﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DBASystem.SMSF.Web.Common;

namespace DBASystem.SMSF.Web.Controllers
{
    [UserAuthorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = TempData["Message"];

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
