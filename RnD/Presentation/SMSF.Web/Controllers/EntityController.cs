﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class EntityController : BaseController
    {
        #region Entity

        [HttpGet]
        public ActionResult Index(int id)
        {
            return Detail(id);
        }

        public ActionResult Detail(int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetEntityDetail);
            var responseModel = base.Execute(requestXml);
            var model = (EntityModel)ModelDeserializer.DeserializeFromXml<EntityModel>(responseModel.ModelXml);

            SessionHelper.SelectedEntity = new KeyValuePair<int, string>(id, "Entity");

            if (Request.IsAjaxRequest())
                return PartialView("_Detail", model);
            else
                return View(model);
        }

        public ActionResult DetailAccountManager(int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetAccountManagerDetail);
            var responseModel = base.Execute(requestXml);
            var model = (AccountManagerModel)ModelDeserializer.DeserializeFromXml<AccountManagerModel>(responseModel.ModelXml);

            return PartialView("_DetailAccountManager", model);
        }

        public ActionResult DetailList(int id)
        {
            ViewBag.ID = id;
            return PartialView("_DetailList");
        }

        public ActionResult GetDetailEntities(int id, [DataSourceRequest] DataSourceRequest request)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = (int)id }, Option.GetEntities);
            var responseModel = base.Execute(requestXml);
            var models = (List<EntityDetailModel>)ModelDeserializer.DeserializeListFromXml<EntityDetailModel>(responseModel.ModelXml);

            return Json(models.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Create(int parentId)
        {
            if (!CommonFunction.IsOptionAllowed(Option.AddEntity))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetCreateView(parentId, EntityTypes.Entity);
        }

        [HttpPost]
        public ActionResult Create(EntityCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;

                if (model.EntityType == EntityTypes.Entity)
                    model.ListAddress = SessionHelper.ListAddresses;

                var requestXml = base.GenerateRequestXml(model, Option.AddEntity);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    var id = ((GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml)).Value;

                    if (model.EntityType == EntityTypes.Entity || model.EntityType == EntityTypes.Group)
                        SessionHelper.SelectedEntity = new KeyValuePair<int, string>(id, "Entity");

                    SessionHelper.EntityTree = null;
                    TempData["Message"] = model.EntityType.ToString() + " added successfully";
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(model);
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult CreateGroup(int parentId)
        {
            if (!CommonFunction.IsOptionAllowed(Option.AddGroup))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetCreateView(parentId, EntityTypes.Group);
        }

        [HttpGet]
        public ActionResult CreateClient(int parentId)
        {
            if (!CommonFunction.IsOptionAllowed(Option.AddClient))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetCreateView(parentId, EntityTypes.Client);
        }

        [HttpGet]
        public ActionResult CreateFund(int parentId)
        {
            if (!CommonFunction.IsOptionAllowed(Option.AddFund))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetCreateView(parentId, EntityTypes.Fund);
        }

        public ActionResult GetCreateView(int parentId, EntityTypes type)
        {
            SessionHelper.ListAddresses = null;
            SessionHelper.ListAccountManagers = null;

            var model = new EntityCreateModel { ParentID = parentId, EntityType = type };

            return View("Create", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (!CommonFunction.IsOptionAllowed(Option.UpdateEntity))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetEditView(id);
        }

        [HttpGet]
        public ActionResult EditGroup(int id)
        {
            if (!CommonFunction.IsOptionAllowed(Option.UpdateGroup))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetEditView(id);
        }

        [HttpGet]
        public ActionResult EditClient(int id)
        {
            if (!CommonFunction.IsOptionAllowed(Option.UpdateClient))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetEditView(id);
        }

        [HttpGet]
        public ActionResult EditFund(int id)
        {
            if (!CommonFunction.IsOptionAllowed(Option.UpdateFund))
                throw new Exception(StatusMessage.AccessDenied.GetDescription());

            return GetEditView(id);
        }

        public ActionResult GetEditView(int id)
        {
            SessionHelper.ListAddresses = null;
            SessionHelper.ListAccountManagers = null;

            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetEntity);
            var responseModel = base.Execute(requestXml);
            var model = (EntityCreateModel)ModelDeserializer.DeserializeFromXml<EntityCreateModel>(responseModel.ModelXml);

            if (model.EntityType == EntityTypes.Entity)
                SessionHelper.ListAddresses = model.ListAddress;

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult Edit(EntityCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = SessionHelper.UserId;
                model.ListAddress = SessionHelper.ListAddresses;
                model.ListUser = null;

                var requestXml = base.GenerateRequestXml(model, Option.UpdateEntity);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    if (model.EntityType == EntityTypes.Entity || model.EntityType == EntityTypes.Group)
                    {
                        SessionHelper.EntityTree = null;
                        return RedirectToAction("Index", new { id = model.ID });
                    }
                    else if (model.EntityType == EntityTypes.Client)
                        return RedirectToAction("ListClients", new { id = model.ParentID });
                    else if (model.EntityType == EntityTypes.Fund)
                        return RedirectToAction("ListFunds", new { id = model.ParentID });
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View("Create", model);
                }
            }

            return View(model);
        }

        public ActionResult Delete(int id)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, int> { Key = id, Value = SessionHelper.UserId }, Option.DeleteEntity);
            var responseModel = base.Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            if (status.Value)
            {
                SessionHelper.EntityTree = null;
                SessionHelper.SelectedEntity = new KeyValuePair<int, string>();

                TempData["Message"] = "Record deleted successfully";
                return RedirectToAction("Index", "Home");
            }
            else
                throw new Exception(responseModel.ResponseMessage);
        }

        public ActionResult ListClients(int id)
        {
            ViewBag.EntityType = EntityTypes.Client;
            return ListEntities(id, EntityTypes.Client);
        }

        public ActionResult ListFunds(int id)
        {
            ViewBag.EntityType = EntityTypes.Fund;
            return ListEntities(id, EntityTypes.Fund);
        }

        public ActionResult ListEntities(int parentId, EntityTypes type)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, EntityTypes> { Key = parentId, Value = type }, type == EntityTypes.Client ? Option.GetClients : Option.GetFunds);
            var responseModel = base.Execute(requestXml);
            var model = (List<EntityModel>)ModelDeserializer.DeserializeListFromXml<EntityModel>(responseModel.ModelXml);

            return View("ListEntities", model);
        }

        #endregion

        #region Address

        [HttpGet]
        public ActionResult GetAddress()
        {
            return PartialView("_ListAddress");
        }

        [HttpGet]
        public ActionResult CreateAddress()
        {
            var model = new AddressCreateModel();
            model.ListTypes = GetAddressTypes();
            model.ListCountires = GetCountries(true);

            if (model.ListCountires.Any())
            {
                var selectedCountry = model.ListCountires.FirstOrDefault(a => a.Selected);
                if (selectedCountry != null)
                {
                    var states = GetStates(Convert.ToInt32(selectedCountry.Value), (int?)null);
                    model.ListState = (List<SelectListItem>)states.Data;
                    model.Country = selectedCountry.Text;
                }
            }

            return PartialView("_CreateAddress", model);
        }

        [HttpPost]
        public ActionResult CreateAddress(AddressCreateModel model)
        {
            var list = SessionHelper.ListAddresses ?? new List<AddressCreateModel>();

            model.CreatedBy = SessionHelper.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusID = Status.Active;

            if (model.Default)
                foreach (var item in list.Where(a => a.TypeID == model.TypeID).ToList())
                    item.Default = false;

            if (model.ID != 0)
            {
                list = list.Where(a => a.ID != model.ID).ToList();
                list.Add(model);
            }
            else
            {
                model.ID = list.Any() ? (list.Max(a => a.ID) + 1) * -1 : -1;
                list.Add(model);
            }
            SessionHelper.ListAddresses = list;

            return Json("success");
        }

        [HttpDelete]
        public void DeleteAddress(int id)
        {
            var list = SessionHelper.ListAddresses;
            if (list.First(a => a.ID == id).ID > 0)
                list.First(a => a.ID == id).StatusID = Status.Deleted;
            else
                list = list.Where(a => a.ID != id).ToList();

            SessionHelper.ListAddresses = list;
        }

        [HttpGet]
        public ActionResult UpdateAddress(int id)
        {
            var model = SessionHelper.ListAddresses.FirstOrDefault(a => a.ID == id);
            model.ListTypes = GetAddressTypes();
            model.ListCountires = GetCountries(false);

            if (model.ListCountires.Any())
            {
                var selectedCountry = model.ListCountires.FirstOrDefault(a => a.Value == model.CountryID.ToString());
                if (selectedCountry != null)
                {
                    var states = GetStates(Convert.ToInt32(selectedCountry.Value), model.StateID);
                    model.ListState = (List<SelectListItem>)states.Data;
                    model.Country = selectedCountry.Text;
                }
            }

            return PartialView("_CreateAddress", model);
        } 

        #endregion

        #region Account Manager

        [HttpGet]
        public ActionResult GetAccountManagers(int? parentEntityId, int? primaryId)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int?> { Value = parentEntityId }, Option.ListEntityAccountManagers);
            var responseModel = base.Execute(requestXml);
            var managers = (List<AccountManagerCreateModel>)ModelDeserializer.DeserializeListFromXml<AccountManagerCreateModel>(responseModel.ModelXml);

            if (primaryId != null)
            {
                foreach (var manager in managers)
                    if (manager.ID == (int)primaryId)
                        manager.Selected = true;
            }

            SessionHelper.ListAccountManagers = managers;

            return PartialView("_ListAccountManager");
        }

        [HttpGet]
        public ActionResult CreateAccountManager(int entityId)
        {
            var model = new AccountManagerCreateModel();
            model.ListGroups = GetGroups();
            model.EntityID = entityId;

            return PartialView("_CreateAccountManager", model);
        }

        [HttpPost]
        public ActionResult CreateAccountManager(AccountManagerCreateModel model)
        {
            var list = SessionHelper.ListAccountManagers ?? new List<AccountManagerCreateModel>();
            if (model.ID == 0)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                model.StatusID = Status.Active;

                var requestXml = base.GenerateRequestXml(model, Option.CreateUser);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    SessionHelper.EntityTree = null;

                    model.ID = ((GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml)).Value;
                    model.Selected = true;
                    list.Add(model);
                    SessionHelper.ListAccountManagers = list;

                    return Json("success");
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return Json("failure");
                }
            }
            else
            {
                model.ModifiedBy = SessionHelper.UserId;
                model.ModifiedOn = DateTime.Now;
                var requestXml = base.GenerateRequestXml(model, Option.UpdateUser);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    SessionHelper.EntityTree = null;

                    model.Selected = true;
                    list = list.Where(a => a.ID != model.ID).ToList();
                    list.Add(model);
                    SessionHelper.ListAccountManagers = list;

                    return Json("success");
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return Json("failure");
                }
            }
        }

        [HttpGet]
        public ActionResult UpdateAccountManager(int id)
        {
            var model = SessionHelper.ListAccountManagers.Where(a => a.ID == id).FirstOrDefault();
            model.ListGroups = GetGroups();

            return PartialView("_CreateAccountManager", model);
        }

        [HttpDelete]
        public void DeleteAccountManager(int id)
        {
            if (id == SessionHelper.UserId)
                throw new Exception("Cannot delete current logged in user");

            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, int> { Key = id, Value = SessionHelper.UserId }, Option.DeleteAccountManager);
            var responseModel = base.Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            if (status.Value)
                SessionHelper.EntityTree = null;
            else
                throw new Exception(responseModel.ResponseMessage);

            SessionHelper.ListAccountManagers = SessionHelper.ListAccountManagers.Where(a => a.ID != id).ToList();
        }

        public ActionResult GetAccountManagersPaged([DataSourceRequest] DataSourceRequest request)
        {
            return Json(SessionHelper.ListAccountManagers.ToDataSourceResult(request));
        }

        #endregion

        #region Contact Info

        [HttpGet]
        public ActionResult CreateContactInfo(int userId)
        {
            var model = new ContactCreateModel();
            model.AccountID = userId;
            model.ListTypes = GetContactTypes();

            return PartialView("_CreateContactInfo", model);
        }

        [HttpPost]
        public ActionResult CreateContactInfo(ContactCreateModel model)
        {
            var list = SessionHelper.ListAccountManagers.Where(a => a.ID == model.AccountID).FirstOrDefault().ListContactInfo;
            if (list == null) list = new List<ContactCreateModel>();

            if (model.ID == 0)
            {
                model.CreatedBy = SessionHelper.UserId;
                model.CreatedOn = DateTime.Now;
                model.StatusID = Status.Active;

                var requestXml = base.GenerateRequestXml(model, Option.AddTelephone);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    model.ID = ((GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml)).Value;
                    list.Add(model);
                    SessionHelper.ListAccountManagers.Where(a => a.ID == model.AccountID).FirstOrDefault().ListContactInfo = list;

                    return Json("success");
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return Json("failure");
                }
            }
            else
            {
                model.ModifiedBy = SessionHelper.UserId;
                model.ModifiedOn = DateTime.Now;

                var requestXml = base.GenerateRequestXml(model, Option.UpdateTelephone);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    list = list.Where(a => a.ID != model.ID).ToList();
                    list.Add(model);
                    SessionHelper.ListAccountManagers.Where(a => a.ID == model.AccountID).FirstOrDefault().ListContactInfo = list;

                    return Json("success");
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return Json("failure");
                }
            }
        }

        [HttpDelete]
        public void DeleteContactInfo(int userId, int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.DeleteTelephone);
            var responseModel = base.Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var list = SessionHelper.ListAccountManagers.Where(a => a.ID == userId).FirstOrDefault().ListContactInfo;
                SessionHelper.ListAccountManagers.Where(a => a.ID == userId).FirstOrDefault().ListContactInfo = list.Where(a => a.ID != id).ToList();
            }
        }

        [HttpGet]
        public ActionResult UpdateContactInfo(int userId, int id)
        {
            var list = SessionHelper.ListAccountManagers.Where(a => a.ID == userId).FirstOrDefault().ListContactInfo;
            var model = list.Where(a => a.ID == id).FirstOrDefault();
            model.ListTypes = GetContactTypes();

            return PartialView("_CreateContactInfo", model);
        } 

        #endregion

        #region Entity Tree

        public ActionResult GetEntityTree()
        {
            if (SessionHelper.EntityTree == null)
            {
                var requestXml = base.GenerateRequestXml(null, Option.EntityTree);

                var responseModel = base.Execute(requestXml);

                var entityTree = (IEnumerable<EntityTreeModel>)ModelDeserializer.DeserializeListFromXml<EntityTreeModel>(responseModel.ModelXml);
                SessionHelper.EntityTree = entityTree;
            }

            return PartialView("_EntityTree", SessionHelper.EntityTree);
        }

        public IEnumerable<EntityTreeModel> LoadEntityTree(int? id)
        {
            var requestXml = base.GenerateRequestXml(id == null ? null : new GenericModel<int> { Value = (int)id }, Option.EntityTree);

            var responseModel = base.Execute(requestXml);

            return (IEnumerable<EntityTreeModel>)ModelDeserializer.DeserializeListFromXml<EntityTreeModel>(responseModel.ModelXml);
        } 

        #endregion

        #region Custom Functions

        public List<SelectListItem> GetAddressTypes()
        {
            var requestXml = base.GenerateRequestXml(null, Option.ListAddressTypes);
            var responseModel = base.Execute(requestXml);
            var addressTypes = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            return addressTypes.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList();
        }

        public List<SelectListItem> GetEntityCategories(int? id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int?> { Value = id }, Option.GetEntityCategories);
            var responseModel = base.Execute(requestXml);
            var entityTypes = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            return entityTypes.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList();
        }

        public List<SelectListItem> GetGroups()
        {
            var groups = new AccountController().GetGroups();

            return groups.Where(a => a.Status == Status.Active).Select(a => new SelectListItem { Value = a.ID.ToString(), Text = a.Title }).ToList();

        }

        public List<SelectListItem> GetContactTypes()
        {
            var requestXml = base.GenerateRequestXml(null, Option.GetTelephoneTypes);
            var responseModel = base.Execute(requestXml);
            var groups = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            return groups.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList();
        }

        public List<SelectListItem> GetCountries(bool showSelected)
        {
            var requestXml = base.GenerateRequestXml(null, Option.ListCountries);
            var responseModel = base.Execute(requestXml);
            var countries = (List<CountryModel>)ModelDeserializer.DeserializeListFromXml<CountryModel>(responseModel.ModelXml);

            if (showSelected)
                return countries.Select(a => new SelectListItem { Value = a.ID.ToString(), Text = a.Name, Selected = a.Default }).ToList();
            else
                return countries.Select(a => new SelectListItem { Value = a.ID.ToString(), Text = a.Name }).ToList();
        }

        public JsonResult GetStates(int countryId, int? selectedStateId)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = countryId }, Option.ListStates);
            var responseModel = base.Execute(requestXml);
            var states = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            if (selectedStateId == null)
                return Json(states.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList(), JsonRequestBehavior.AllowGet);
            else
                return Json(states.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value, Selected = (a.Key == selectedStateId) }).ToList(), JsonRequestBehavior.AllowGet);
        } 

        #endregion
    }
}