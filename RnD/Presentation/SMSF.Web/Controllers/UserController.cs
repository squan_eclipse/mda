﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult Index(int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetAccountManagerDetail);
            var responseModel = base.Execute(requestXml);
            var model = (AccountManagerModel)ModelDeserializer.DeserializeFromXml<AccountManagerModel>(responseModel.ModelXml);

            SessionHelper.SelectedEntity = new KeyValuePair<int, string>(id, "User");

            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int entityId)
        {
            //SessionHelper.ListContacts = null;
            var model = new AccountManagerCreateModel();
            model.EntityID = entityId;

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AccountManagerCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                model.ListContactInfo = SessionHelper.ListContacts;
                model.StatusID = Status.Active;

                var requestXml = base.GenerateRequestXml(model, Option.CreateUser);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    var id = ((GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml)).Value;

                    SessionHelper.SelectedEntity = new KeyValuePair<int, string>(id, "User");

                    SessionHelper.EntityTree = null;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(model);
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            SessionHelper.ListContacts = null;

            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetAccountManager);
            var responseModel = base.Execute(requestXml);
            var model = (AccountManagerCreateModel)ModelDeserializer.DeserializeFromXml<AccountManagerCreateModel>(responseModel.ModelXml);

            SessionHelper.ListContacts = model.ListContactInfo;

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult Edit(AccountManagerCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.ListContactInfo = SessionHelper.ListContacts;
                model.ModifiedBy = SessionHelper.UserId;
                model.ModifiedOn = DateTime.Now;
                var requestXml = base.GenerateRequestXml(model, Option.UpdateUser);
                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    SessionHelper.EntityTree = null;
                    SessionHelper.ListContacts = null;

                    return RedirectToAction("Index", new { id = model.ID });
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(model);
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult CreateContactInfo()
        {
            var model = new ContactCreateModel();
            model.ListTypes = new EntityController().GetContactTypes();

            return PartialView(Url.Content("~/Views/Entity/_CreateContactInfo.cshtml"), model);
        }

        [HttpPost]
        public ActionResult CreateContactInfo(ContactCreateModel model)
        {
            var list = SessionHelper.ListContacts;
            if (list == null) list = new List<ContactCreateModel>();

            model.CreatedBy = SessionHelper.UserId;
            model.CreatedOn = DateTime.Now;

            if (model.ID != 0)
            {
                list = list.Where(a => a.ID != model.ID).ToList();
                list.Add(model);
            }
            else
            {
                model.StatusID = Status.Active;
                model.ID = list.Any() ? (list.Max(a => a.ID) + 1) * -1 : -1;
                list.Add(model);
            }
            SessionHelper.ListContacts = list;

            return Json("success");
        }

        [HttpGet]
        public ActionResult GetContactInfo()
        {
            return PartialView("_ListContactInfo");
        }

        [HttpGet]
        public ActionResult UpdateContactInfo(int id)
        {
            var model = SessionHelper.ListContacts.Where(a => a.ID == id).FirstOrDefault();
            model.ListTypes = new EntityController().GetContactTypes();

            return PartialView(Url.Content("~/Views/Entity/_CreateContactInfo.cshtml"), model);
        }

        [HttpDelete]
        public void DeleteContactInfo(int id)
        {
            SessionHelper.ListContacts = SessionHelper.ListContacts.Where(a => a.ID != id).ToList();
        }

        [HttpGet]
        public JsonResult ValidateLogin(int id, string login)
        {
            if (SessionHelper.ListAccountManagers != null && SessionHelper.ListAccountManagers.Any(a => a.ID != id && a.Login == login))
                return Json("Login Name already exists", JsonRequestBehavior.AllowGet);

            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, string> { Key = id, Value = login }, Option.ValidateLogin);
            var responseModel = base.Execute(requestXml);

            return Json(responseModel.ResponseMessage, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            if (id == SessionHelper.UserId)
                throw new Exception("Cannot delete current logged in user");

            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, int> { Key = id, Value = SessionHelper.UserId }, Option.DeleteAccountManager);
            var responseModel = base.Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            if (status.Value)
            {
                SessionHelper.SelectedEntity = new KeyValuePair<int,string>();
                SessionHelper.EntityTree = null;
                TempData["Message"] = "User deleted successfully";
                return RedirectToAction("Index", "Home");
            }
            else
                throw new Exception(responseModel.ResponseMessage);
        }
    }
}
