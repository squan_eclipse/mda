﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace DBASystem.SMSF.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            if (TempData["message"] != null)
                ViewBag.ErrorMessage = TempData["message"]; 
            // XDocument.Parse(string.Format("{0}{1}{2}", "<Exception>", TempData["message"].ToString(), "</Exception>").ToString(), LoadOptions.PreserveWhitespace);

            return View("Error");
        }
    }
}
