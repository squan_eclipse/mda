﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Web.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Services.SMSFService;
using DBASystem.Web;
using System;
using System.Web.Mvc;
using System.Web;

namespace DBASystem.SMSF.Web.Controllers
{
    [UserAuthorize]
    [CustomHandleError]
    public abstract class BaseController : Controller
    {
        protected ResponseModel Execute(string requestXml)
        {
            var responseXml = new SMSFServiceClient().Execute(requestXml);

            var model = ModelDeserializer.ParseResponseXml(responseXml);

            if (model.Status == ResponseStatus.Error)
                throw new Exception(model.ResponseMessage);

            if (model.Status == ResponseStatus.Failure && model.ResponseMessage == StatusMessage.AccessDenied.GetDescription())
                throw new Exception(model.ResponseMessage);

            return model;
        }

        protected string GenerateRequestXml(IModel model, Option option)
        { 
            return ModelSerializer.GenerateRequestXml(model, option, SessionHelper.UserId, SessionHelper.AuthCode);
        }
    }
}
