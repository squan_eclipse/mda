﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;

namespace DBASystem.SMSF.Web.Controllers
{
    public class AccountController : BaseController
    {
        #region Login
        
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                Session.Add("", null);
                var requestXml = base.GenerateRequestXml(model, Option.Login);

                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    var user = (UserModel)ModelDeserializer.DeserializeFromXml<UserModel>(responseModel.ModelXml);

                    if (user != null)
                    {
                        SessionHelper.UserId = user.ID;
                        SessionHelper.UserName = user.Name;
                        SessionHelper.AuthCode = user.AuthCode;
                        SessionHelper.Name = user.FirstName + " " + user.LastName;
                        SessionHelper.Email = user.Email;

                        SessionHelper.AllowedPages = GetAllowedPages(Option.AllowedPages);
                        SessionHelper.AllowedOptions = GetAllowedPages(Option.AllowedOptions);

                        UserIdentity identity = new UserIdentity(user.ID, user.Name, true);
                        System.Threading.Thread.CurrentPrincipal = new UserPrincipal(identity);

                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(user.Name, true, DateTime.Now.AddHours(24).Minute);

                        string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                        Response.Cookies.Add(authCookie);

                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(model);
                }
            }

            ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return View(model);
        }

        private List<Option> GetAllowedPages(Option option)
        {
            OptionModel optionModel = new OptionModel();
            optionModel.OptionType = OptionType.Page;
            optionModel.UserID = SessionHelper.UserId;

            var requestXml = base.GenerateRequestXml(optionModel, option);
            var responseModel = base.Execute(requestXml);
            return (List<Option>)ModelDeserializer.DeserializeListFromXml<Option>(responseModel.ModelXml);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("Login");
        } 

        #endregion

        #region Group

        [HttpGet]
        public ActionResult ListGroups()
        {
            return View(GetGroups());
        }

        public List<UserGroupModel> GetGroups()
        {
            var requestXml = base.GenerateRequestXml(null, Option.GetUserGroups);
            var responseModel = base.Execute(requestXml);
            var groups = (List<UserGroupModel>)ModelDeserializer.DeserializeListFromXml<UserGroupModel>(responseModel.ModelXml);
            return groups;
        }

        [HttpGet]
        public ActionResult GetGroupList()
        {
            return PartialView("_ListGroups", GetGroups());
        }

        [HttpGet]
        public ActionResult CreateGroup(int? id)
        {
            if (id == null)
                return PartialView("_CreateGroup", new UserGroupModel());
            else
            {
                var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = (int)id }, Option.GetUserGroup);
                var responseModel = base.Execute(requestXml);
                var model = (UserGroupModel)ModelDeserializer.DeserializeFromXml<UserGroupModel>(responseModel.ModelXml);

                return PartialView("_CreateGroup", model);
            }
        }

        [HttpPost]
        public ActionResult CreateGroup(UserGroupModel model)
        {
            string requestXml;

            if (model.ID == 0)
            {
                model.Status = Status.Active;
                requestXml = base.GenerateRequestXml(model, Option.CreateUserGroup);
            }
            else
                requestXml = base.GenerateRequestXml(model, Option.UpdateUserGroup);

            var responseModel = base.Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
                return Json("success");
            else
                return Json(responseModel.StatusMessage);
        }

        [HttpPost]
        public void ChangeGroupStatus(int id, Status status)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, Status> { Key = id, Value = status }, Option.ChangeUserGroupStatus);
            var responseModel = base.Execute(requestXml);
        }

        #endregion

        #region Role

        [HttpGet]
        public ActionResult ListRoles()
        {
            return View(GetRoles());
        }

        public List<RoleModel> GetRoles()
        {
            var requestXml = base.GenerateRequestXml(null, Option.GetRoles);
            var responseModel = base.Execute(requestXml);
            var roles = (List<RoleModel>)ModelDeserializer.DeserializeListFromXml<RoleModel>(responseModel.ModelXml);
            return roles;
        }

        [HttpGet]
        public ActionResult GetRoleList()
        {
            return PartialView("_ListRoles", GetRoles());
        }

        [HttpGet]
        public ActionResult CreateRole(int? id)
        {
            if (id == null)
                return PartialView("_CreateRole", new RoleModel());
            else
            {
                var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = (int)id }, Option.GetRole);
                var responseModel = base.Execute(requestXml);
                var model = (RoleModel)ModelDeserializer.DeserializeFromXml<RoleModel>(responseModel.ModelXml);

                return PartialView("_CreateRole", model);
            }
        }

        [HttpPost]
        public ActionResult CreateRole(RoleModel model)
        {
            string requestXml;

            if (model.ID == 0)
            {
                model.Status = Status.Active;
                requestXml = base.GenerateRequestXml(model, Option.CreateRole);
            }
            else
                requestXml = base.GenerateRequestXml(model, Option.UpdateRole);

            var responseModel = base.Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
                return Json("success");
            else
                return Json(responseModel.StatusMessage);
        }

        [HttpPost]
        public void ChangeRoleStatus(int id, Status status)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, Status> { Key = id, Value = status }, Option.ChangeRoleStatus);
            var responseModel = base.Execute(requestXml);
        }

        #endregion

        #region Group Roles

        public ActionResult ListGroupRoles(int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetUserGroupRoles);
            var responseModel = base.Execute(requestXml);
            var model = (UserGroupRolesModel)ModelDeserializer.DeserializeFromXml<UserGroupRolesModel>(responseModel.ModelXml);

            return View(model);
        }

        public JsonResult AddUserGroupRoles(int groupId, int[] roles)
        {
            var model = new UserGroupRolesModel
            {
                ID = groupId,
                Roles = roles != null && roles.Any() ? roles.Select(a => new RoleModel { ID = a, Status = Status.Active }).ToList() : null
            };

            var requestXml = base.GenerateRequestXml(model, Option.AddUserGroupRoles);
            var responseModel = base.Execute(requestXml);

            return Json(responseModel.StatusMessage.ToString());
        }

        #endregion

        #region Role Options

        public ActionResult ListRoleOptions(int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetUserRoleOptions);
            var responseModel = base.Execute(requestXml);
            var model = (RoleOptionsModel)ModelDeserializer.DeserializeFromXml<RoleOptionsModel>(responseModel.ModelXml);

            return View(model);
        }

        public JsonResult AddRoleOptions(int roleId, int[] options)
        {
            var model = new RoleOptionsModel
            {
                ID = roleId,
                CreateBy = SessionHelper.UserId,
                CreatedOn = DateTime.Now,
                Options = options != null && options.Any() ? options.Select(a => new DictionaryModel<int, string> { Key = a }).ToList() : null
            };

            var requestXml = base.GenerateRequestXml(model, Option.AddUserRoleOptions);
            var responseModel = base.Execute(requestXml);

            return Json(responseModel.StatusMessage.ToString());
        }

        #endregion
    }
}
