﻿using DBASystem.SMSF.Contracts.ViewModels;
using System.Collections.Generic;
using System.Web;
using System;
using DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Web.Common
{
    public static class SessionHelper
    {
        public static bool IsAuthenticated
        {
            get
            {
                return UserId != 0 ? true : false;
            }
        }

        public static int UserId
        {
            get
            {
                return (int?)HttpContext.Current.Session["UserId"] ?? 0;
            }
            set
            {
                HttpContext.Current.Session["UserId"] = value;
            }
        }

        public static Guid? AuthCode
        {
            get
            {

                return (Guid?)HttpContext.Current.Session["AuthCode"];
            }
            set
            {
                HttpContext.Current.Session["AuthCode"] = value;
            }
        }

        public static string UserName
        {
            get
            {
                return (string)HttpContext.Current.Session["UserName"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }

        public static string Name
        {
            get
            {
                return (string)HttpContext.Current.Session["Name"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["Name"] = value;
            }
        }

        public static string Email
        {
            get
            {
                return (string)HttpContext.Current.Session["Email"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["Email"] = value;
            }
        }

        public static IEnumerable<EntityTreeModel> EntityTree
        {
            get
            {
                return (IEnumerable<EntityTreeModel>)HttpContext.Current.Session["EntityTree"];
            }
            set
            {
                HttpContext.Current.Session["EntityTree"] = value;
            }
        }

        public static List<AddressCreateModel> ListAddresses
        {
            get
            {
                return (List<AddressCreateModel>)HttpContext.Current.Session["ListAddresses"];
            }
            set
            {
                HttpContext.Current.Session["ListAddresses"] = value;
            }
        }

        public static List<AccountManagerCreateModel> ListAccountManagers
        {
            get
            {
                return (List<AccountManagerCreateModel>)HttpContext.Current.Session["ListAccountManagers"];
            }
            set
            {
                HttpContext.Current.Session["ListAccountManagers"] = value;
            }
        }

        public static List<ContactCreateModel> ListContacts
        {
            get
            {
                return (List<ContactCreateModel>)HttpContext.Current.Session["ListContacts"];
            }
            set
            {
                HttpContext.Current.Session["ListContacts"] = value;
            }
        }

        public static List<Option> AllowedPages
        {
            get { return (List<Option>)HttpContext.Current.Session["AllowedPages"]; }
            set { HttpContext.Current.Session["AllowedPages"] = value; }
        }

        public static List<Option> AllowedOptions
        {
            get { return (List<Option>)HttpContext.Current.Session["AllowedOptions"]; }
            set { HttpContext.Current.Session["AllowedOptions"] = value; }
        }

        public static KeyValuePair<int, string> SelectedEntity
        {
            get { return HttpContext.Current.Session["SelectedEntity"] != null ? (KeyValuePair<int, string>)HttpContext.Current.Session["SelectedEntity"] : new KeyValuePair<int, string>(); }
            set { HttpContext.Current.Session["SelectedEntity"] = value; }
        }
    }
}