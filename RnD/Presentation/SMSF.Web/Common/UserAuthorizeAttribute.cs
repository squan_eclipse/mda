﻿using System;
using System.Web;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Common
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class UserAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!SessionHelper.IsAuthenticated)
                return false;
            else
                return true;
        }
    }
}