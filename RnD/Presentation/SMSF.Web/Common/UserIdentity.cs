﻿using System.Security.Principal;

namespace DBASystem.SMSF.Web.Common
{
    public class UserIdentity : IIdentity
    {
        #region Variables

        readonly bool isAuthenticated;
        readonly string name;
        readonly int id;

        #endregion

        #region IIdentity Members

        public UserIdentity(int id, string name, bool isAuthenticated)
        {
            this.isAuthenticated = isAuthenticated;
            this.name = name;
            this.id = id;
        }

        public string AuthenticationType
        {
            get { return "Custom"; }
        }

        public bool IsAuthenticated
        {
            get { return isAuthenticated; }
        }

        public string Name
        {
            get { return name; }
        }

        public int ID
        {
            get { return id; }
        }

        #endregion
    }
}