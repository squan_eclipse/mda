﻿USE [DBASystem]
GO



DECLARE @Active	INT
DECLARE @Inactive iNT

SET @Active = (SELECT TOP (1) ID FROM [Status] WHERE Title = 'Active')
SET @Inactive = (SELECT TOP (1) ID FROM [Status] WHERE Title = 'Inactive')

INSERT INTO [DBASystem].[dbo].[Entities]
           ([TypeID],[CategoryID],[ABN],[GST],[TFN],[Name],[AddressID],[Note],[ParentID],[PrimaryAccountManagerID],[StatusID]
           ,[CreatedBy],[CreatedOn],[ModifiedBy],[ModifiedOn])
     VALUES
			(
				1,
				3, 
				'56-456-654-656', 
				null, 
				null, 
				'DBA Holdings', 
				null, null, 
				(SELECT TOP (1) ID FROM Entities WHERE Name = 'SYSTEM'), 
				null, 
				(SELECT TOP (1) ID FROM [Status] WHERE Title = 'Active'),
				1, 
				GETDATE(), 
				null, 
				null
			)
GO
INSERT INTO [DBASystem].[dbo].[Entities]
           ([TypeID],[CategoryID],[ABN],[GST],[TFN],[Name],[AddressID],[Note],[ParentID],[PrimaryAccountManagerID],[StatusID]
           ,[CreatedBy],[CreatedOn],[ModifiedBy],[ModifiedOn])
     VALUES
			(
				1,
				3, 
				'56-457-569-235', 
				null, 
				null, 
				'DBA Advisory', 
				null, 
				null, 
				(SELECT TOP (1) ID FROM Entities WHERE Name = 'DBA Holdings'), 
				null, 
				(SELECT TOP (1) ID FROM [Status] WHERE Title = 'Active'),
				1, 
				GETDATE(), 
				null, 
				null
			)
GO
INSERT INTO [DBASystem].[dbo].[Entities]
           ([TypeID],[CategoryID],[ABN],[GST],[TFN],[Name],[AddressID],[Note],[ParentID],[PrimaryAccountManagerID],[StatusID]
           ,[CreatedBy],[CreatedOn],[ModifiedBy],[ModifiedOn])
     VALUES
			(
				1,
				3, 
				'23-569-865-412', 
				null, 
				null, 
				'DBA System', 
				null, 
				null, 
				(SELECT TOP (1) ID FROM Entities WHERE Name = 'DBA Holdings'), 
				null, 
				(SELECT TOP (1) ID FROM [Status] WHERE Title = 'Active'),
				1, 
				GETDATE(), 
				null, 
				null
			)
GO
INSERT INTO [DBASystem].[dbo].[Entities]
           ([TypeID],[CategoryID],[ABN],[GST],[TFN],[Name],[AddressID],[Note],[ParentID],[PrimaryAccountManagerID],[StatusID]
           ,[CreatedBy],[CreatedOn],[ModifiedBy],[ModifiedOn])
     VALUES
			(
				1,
				3,
				'52-538-654-563', 
				null, 
				null, 
				'DBA PHIL', 
				null, null, 
				(SELECT TOP (1) ID FROM Entities WHERE Name = 'DBA Advisory'), 
				null, 
				(SELECT TOP (1) ID FROM [Status] WHERE Title = 'Active'),
				1, 
				GETDATE(), 
				null, 
				null
			)
GO

INSERT INTO [DBASystem].[dbo].[AccountManagers]
           ([EntityID],[FirstName],[MidName],[LastName],[Email],[StatusID],[CreatedBy],[CreatedOn])
     VALUES
           
		   ((SELECT TOP (1) ID FROM Entities WHERE Name = 'DBA Holdings'), 'Micheal', null, 'Holding', 'micheal@msn.com', 1, 1, GETDATE())
GO

INSERT INTO [DBASystem].[dbo].[Users]
           ([AccountID],[GroupID],[Login],[Password],[CreateDate],[StatusID],[IsSystem],[CreatedBy],[CreatedOn])
     VALUES
           ((SELECT TOP (1) ID FROM [AccountManagers] WHERE [FirstName] = 'Micheal'), 2, 'micheal', 'micheal', GETDATE(), 1, 0, 1, GETDATE())
GO


