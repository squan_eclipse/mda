﻿USE [DBASystem]
GO

/****** Object:  StoredProcedure [dbo].[GetAccountEntities]    Script Date: 08/07/2013 13:57:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAccountEntities]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetAccountEntities]
GO

/****** Object:  StoredProcedure [dbo].[GetEntitiesTree]    Script Date: 08/07/2013 13:57:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEntitiesTree]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetEntitiesTree]
GO

/****** Object:  StoredProcedure [dbo].[GetOrganizationnStrucutre]    Script Date: 08/07/2013 13:57:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetOrganizationnStrucutre]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetOrganizationnStrucutre]
GO

/****** Object:  StoredProcedure [dbo].[InsertEntity]    Script Date: 08/07/2013 13:57:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertEntity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertEntity]
GO

/****** Object:  StoredProcedure [dbo].[IsABNExists]    Script Date: 08/07/2013 13:57:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsABNExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[IsABNExists]
GO

/****** Object:  StoredProcedure [dbo].[ValidateUser]    Script Date: 08/07/2013 13:57:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ValidateUser]
GO

USE [DBASystem]
GO

/****** Object:  StoredProcedure [dbo].[GetAccountEntities]    Script Date: 08/07/2013 13:57:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[GetAccountEntities]
        (
        @accId uniqueidentifier 
        )
AS
	Select EntityID from [dbo].[EntityAccounts] where AccountID = @accId


GO

/****** Object:  StoredProcedure [dbo].[GetEntitiesTree]    Script Date: 08/07/2013 13:57:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetEntitiesTree]
        (
        @EntityId uniqueidentifier 
        )
AS
	WITH ParentChildRels (EntityID, ChildID, HierarchyLevel) AS
	(
	   -- Base case
	   SELECT
		  EntityID,
		  ChildID,
		  1 as HierarchyLevel
	   FROM [dbo].[EntitySubEntities]
   

	   UNION ALL

	   -- Recursive step
	   SELECT
		  ee.EntityID,
		  ee.ChildID,
		  pr.HierarchyLevel + 1 AS HierarchyLevel
	   FROM [dbo].[EntitySubEntities] as ee INNER JOIN ParentChildRels pr ON ee.ChildID = pr.EntityID
	)

	SELECT * FROM ParentChildRels 
	WHERE 
	( EntityID = @EntityId AND HierarchyLevel = 1 ) or 
	( EntityID <> @EntityId )
	ORDER BY HierarchyLevel 

GO

/****** Object:  StoredProcedure [dbo].[GetOrganizationnStrucutre]    Script Date: 08/07/2013 13:57:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetOrganizationnStrucutre]
AS
BEGIN
	SET NOCOUNT ON;
	
	Select
	a.ID, a.Name, a.ParentID, c.Description Type_Description
	from Entities a
	inner join EntityTypes c on a.TypeID = c.ID
	where a.StatusID = 2

END


GO

/****** Object:  StoredProcedure [dbo].[InsertEntity]    Script Date: 08/07/2013 13:57:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[InsertEntity]
	@ParentID		uniqueidentifier,
	@TypeID			uniqueidentifier,
	@ABN			nvarchar(50),
	@GST			nvarchar(50),
	@TFN			nvarchar(50),
	@Name			nvarchar(200),
	@Note			ntext,
	@AddressTypeID	uniqueidentifier,
	@UnitNumber		nvarchar(50),
	@StreetNumber	nvarchar(50),
	@StreetName		nvarchar(50),
	@StreetType		nvarchar(50),
	@City			nvarchar(50),
	@Country		nvarchar(50),
	@PostCode		nvarchar(50)
as
	declare @entityId uniqueidentifier
	set @entityId = NEWID()
	
	INSERT INTO Entities
           (ID,TypeID,ABN,GST,TFN,Name,Deleted,Note)
     VALUES
           (@entityId,@TypeID,@ABN,@GST,@TFN,@Name,0,@Note)
           
    INSERT INTO EntitySubEntities
           (EntityID,ChildID)
     VALUES
           (@ParentID, @entityId)
           
	declare @addressId uniqueidentifier
	set @addressId = NEWID()
           
    INSERT INTO Addresses
           (ID, TypeID, UnitNumber, StreetNumber, StreetName, StreetType, City, Country, PostCode)
     VALUES
           (@addressId, @AddressTypeID, @UnitNumber, @StreetNumber, @StreetName, @StreetType, @City, @Country, @PostCode)

    INSERT INTO EntityAddresses
           (EntityID ,AddressID)
     VALUES
           (@entityId ,@addressId)

	Select @entityId


GO

/****** Object:  StoredProcedure [dbo].[IsABNExists]    Script Date: 08/07/2013 13:57:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[IsABNExists]
	@ABN	nvarchar(50)
as
	Select * from Entities
	Where ABN = @ABN
GO

/****** Object:  StoredProcedure [dbo].[ValidateUser]    Script Date: 08/07/2013 13:57:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ValidateUser]
	(
	@Login nvarchar(max),
	@Password nvarchar(max)
	)
AS
	 SET NOCOUNT ON
	 
	 Select U.AccountID as ID, U.Login, A.FirstName, A.LastName, A.Email
	 From Users U
	 Inner Join AccountManagers A On A.ID = U.AccountID 
	 Where U.Login = @Login And U.Password = @Password

	if @@ROWCOUNT > 0
	Begin 
		SET NOCOUNT OFF
		Update Users Set LastLoginTime = GETDATE()
		Where Login = @Login And Password = @Password
	End
	 
	RETURN


GO

