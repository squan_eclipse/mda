﻿USE [DBASystem]
GO
/****** Object:  Table [dbo].[AddressTypes]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddressTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nchar](10) NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_AddressType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DataTypes]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DataTypes](
	[ID] [int] NOT NULL,
	[AssemblyName] [varchar](100) NOT NULL,
	[Namespace] [varchar](500) NOT NULL,
	[Class] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DataTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Countries]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Countries](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Default] [bit] NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EntityCategories]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityCategories](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_EntityCategories] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[States]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[States](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CountryID] [int] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Code] [nvarchar](10) NULL,
 CONSTRAINT [PK_States] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[StatusID] [int] NOT NULL,
	[IsSystem] [bit] NOT NULL,
 CONSTRAINT [PK_Roles_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TelephoneTypes]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TelephoneTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_TelephoneType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OptionTypes]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OptionTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_OptionTypes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountRoles]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountRoles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [int] NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExecutionActions]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ExecutionActions](
	[ID] [int] NOT NULL,
	[DataTypeID] [int] NOT NULL,
	[ActionName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_ExecutionAction] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EntityTypes]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityTypes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_EntityType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Groups]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Groups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Description] [varchar](500) NULL,
	[StatusID] [int] NOT NULL,
	[IsSystem] [bit] NOT NULL,
 CONSTRAINT [PK_Groups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GroupRoles]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupRoles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
 CONSTRAINT [PK_GroupRoles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entities]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entities](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TypeID] [int] NOT NULL,
	[CategoryID] [int] NULL,
	[ABN] [nvarchar](50) NULL,
	[ACN] [nvarchar](50) NULL,
	[GST] [nvarchar](50) NULL,
	[TFN] [nvarchar](50) NULL,
	[Name] [nvarchar](200) NULL,
	[AddressID] [int] NULL,
	[Note] [ntext] NULL,
	[ParentID] [int] NULL,
	[PrimaryAccountManagerID] [int] NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Entities] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Options]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Options](
	[ID] [int] NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[PageURL] [varchar](50) NULL,
	[NextPageURL] [varchar](50) NULL,
	[TypeID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[ExecutionActionID] [int] NULL,
	[AllowAnonymous] [bit] NOT NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Options] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ActionParams]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ActionParams](
	[ID] [int] NOT NULL,
	[ExecutionActionID] [int] NOT NULL,
	[ParamName] [varchar](100) NOT NULL,
	[DataTypeID] [int] NOT NULL,
 CONSTRAINT [PK_ActionParams] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoleOptions]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleOptions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[OptionID] [int] NOT NULL,
	[IsAssigned] [bit] NULL,
	[IsSystem] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_RoleOptions] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[Data] [xml] NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountManagers]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountManagers](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[FirstName] [nvarchar](100) NULL,
	[MidName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[Email] [varchar](100) NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addresses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[EntityID] [int] NOT NULL,
	[TypeID] [int] NULL,
	[UnitNumber] [nvarchar](50) NULL,
	[StreetNumber] [nvarchar](50) NULL,
	[StreetName] [nvarchar](50) NULL,
	[StreetType] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[StateID] [int] NULL,
	[CountryID] [int] NULL,
	[PostCode] [nvarchar](50) NULL,
	[Default] [bit] NOT NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Funds]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Funds](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Funds] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountManagerRoles]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountManagerRoles](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountManagerID] [int] NULL,
	[AccountRoleID] [int] NULL,
 CONSTRAINT [PK_AccountRoles] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[AccountID] [int] NOT NULL,
	[GroupID] [int] NOT NULL,
	[Login] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
	[AuthCode] [uniqueidentifier] NULL,
	[CreateDate] [datetime] NULL,
	[LastLoginTime] [datetime] NULL,
	[StatusID] [int] NOT NULL,
	[IsSystem] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Users_1] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Telephones]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Telephones](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [int] NOT NULL,
	[TypeID] [int] NULL,
	[CountryCode] [nvarchar](50) NULL,
	[AreaCode] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[StatusID] [int] NOT NULL,
 CONSTRAINT [PK_Telephones] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EntityAddresses]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EntityAddresses](
	[EntityID] [int] NOT NULL,
	[AddressID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Jobs]    Script Date: 09/26/2013 15:33:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jobs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FundID] [int] NOT NULL,
	[AccountManagerID] [int] NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[StatusID] [int] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
 CONSTRAINT [PK_Jobs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_AccountManagers_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagers] ADD  CONSTRAINT [DF_AccountManagers_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_AccountManagers_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagers] ADD  CONSTRAINT [DF_AccountManagers_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_AccountManagers_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagers] ADD  CONSTRAINT [DF_AccountManagers_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_AccountManagers_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagers] ADD  CONSTRAINT [DF_AccountManagers_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Addresses_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses] ADD  CONSTRAINT [DF_Addresses_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_Addresses_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses] ADD  CONSTRAINT [DF_Addresses_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_Addresses_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses] ADD  CONSTRAINT [DF_Addresses_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_Addresses_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses] ADD  CONSTRAINT [DF_Addresses_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Clients_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_Clients_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_Clients_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_Clients_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Clients] ADD  CONSTRAINT [DF_Clients_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Entities_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities] ADD  CONSTRAINT [DF_Entities_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_Entities_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities] ADD  CONSTRAINT [DF_Entities_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_Entities_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities] ADD  CONSTRAINT [DF_Entities_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_Entities_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities] ADD  CONSTRAINT [DF_Entities_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Funds_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Funds] ADD  CONSTRAINT [DF_Funds_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_Funds_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Funds] ADD  CONSTRAINT [DF_Funds_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_Funds_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Funds] ADD  CONSTRAINT [DF_Funds_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_Funds_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Funds] ADD  CONSTRAINT [DF_Funds_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Groups_GroupTitle_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Groups] ADD  CONSTRAINT [DF_Groups_GroupTitle_1]  DEFAULT ('') FOR [Title]
GO
/****** Object:  Default [DF_Groups_IsSystem]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Groups] ADD  CONSTRAINT [DF_Groups_IsSystem]  DEFAULT ((0)) FOR [IsSystem]
GO
/****** Object:  Default [DF_Jobs_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Jobs] ADD  CONSTRAINT [DF_Jobs_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_Jobs_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Jobs] ADD  CONSTRAINT [DF_Jobs_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_Jobs_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Jobs] ADD  CONSTRAINT [DF_Jobs_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_Jobs_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Jobs] ADD  CONSTRAINT [DF_Jobs_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Options_OptionTitle_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_OptionTitle_1]  DEFAULT ('') FOR [Title]
GO
/****** Object:  Default [DF_Options_OptionTypeID_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_OptionTypeID_1]  DEFAULT ((1)) FOR [TypeID]
GO
/****** Object:  Default [DF_Options_ParentOptionID_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_ParentOptionID_1]  DEFAULT ((0)) FOR [ParentID]
GO
/****** Object:  Default [DF_Options_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_Options_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_Options_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_Options_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options] ADD  CONSTRAINT [DF_Options_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_RoleOptions_IsSystem]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[RoleOptions] ADD  CONSTRAINT [DF_RoleOptions_IsSystem]  DEFAULT ((0)) FOR [IsSystem]
GO
/****** Object:  Default [DF_RoleOptions_CreatedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[RoleOptions] ADD  CONSTRAINT [DF_RoleOptions_CreatedBy]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_RoleOptions_CreatedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[RoleOptions] ADD  CONSTRAINT [DF_RoleOptions_CreatedOn]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_RoleOptions_ModifiedBy]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[RoleOptions] ADD  CONSTRAINT [DF_RoleOptions_ModifiedBy]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_RoleOptions_ModifiedOn]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[RoleOptions] ADD  CONSTRAINT [DF_RoleOptions_ModifiedOn]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  Default [DF_Roles_RoleTitle_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_RoleTitle_1]  DEFAULT ('') FOR [Title]
GO
/****** Object:  Default [DF_Roles_IsSystem]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_IsSystem]  DEFAULT ((0)) FOR [IsSystem]
GO
/****** Object:  Default [DF_Users_IsSystem]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_IsSystem]  DEFAULT ((0)) FOR [IsSystem]
GO
/****** Object:  Default [DF_Users_CreatedByUserID_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_CreatedByUserID_1]  DEFAULT ((0)) FOR [CreatedBy]
GO
/****** Object:  Default [DF_Users_CreatedDateTime_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_CreatedDateTime_1]  DEFAULT (getdate()) FOR [CreatedOn]
GO
/****** Object:  Default [DF_Users_LastModifiedByUserID_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_LastModifiedByUserID_1]  DEFAULT ((0)) FOR [ModifiedBy]
GO
/****** Object:  Default [DF_Users_LastModifiedDateTime_1]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users] ADD  CONSTRAINT [DF_Users_LastModifiedDateTime_1]  DEFAULT (getdate()) FOR [ModifiedOn]
GO
/****** Object:  ForeignKey [FK_AccountRoles_Accounts]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagerRoles]  WITH CHECK ADD  CONSTRAINT [FK_AccountRoles_Accounts] FOREIGN KEY([AccountManagerID])
REFERENCES [dbo].[AccountManagers] ([ID])
GO
ALTER TABLE [dbo].[AccountManagerRoles] CHECK CONSTRAINT [FK_AccountRoles_Accounts]
GO
/****** Object:  ForeignKey [FK_AccountRoles_Roles]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagerRoles]  WITH CHECK ADD  CONSTRAINT [FK_AccountRoles_Roles] FOREIGN KEY([AccountRoleID])
REFERENCES [dbo].[AccountRoles] ([ID])
GO
ALTER TABLE [dbo].[AccountManagerRoles] CHECK CONSTRAINT [FK_AccountRoles_Roles]
GO
/****** Object:  ForeignKey [FK_AccountManagers_Entities]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagers]  WITH CHECK ADD  CONSTRAINT [FK_AccountManagers_Entities] FOREIGN KEY([EntityID])
REFERENCES [dbo].[Entities] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AccountManagers] CHECK CONSTRAINT [FK_AccountManagers_Entities]
GO
/****** Object:  ForeignKey [FK_AccountManagers_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountManagers]  WITH CHECK ADD  CONSTRAINT [FK_AccountManagers_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[AccountManagers] CHECK CONSTRAINT [FK_AccountManagers_Status]
GO
/****** Object:  ForeignKey [FK_AccountRoles_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[AccountRoles]  WITH CHECK ADD  CONSTRAINT [FK_AccountRoles_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[AccountRoles] CHECK CONSTRAINT [FK_AccountRoles_Status]
GO
/****** Object:  ForeignKey [FK_ActionParams_DataTypes]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[ActionParams]  WITH CHECK ADD  CONSTRAINT [FK_ActionParams_DataTypes] FOREIGN KEY([DataTypeID])
REFERENCES [dbo].[DataTypes] ([ID])
GO
ALTER TABLE [dbo].[ActionParams] CHECK CONSTRAINT [FK_ActionParams_DataTypes]
GO
/****** Object:  ForeignKey [FK_ActionParams_ExecutionAction]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[ActionParams]  WITH CHECK ADD  CONSTRAINT [FK_ActionParams_ExecutionAction] FOREIGN KEY([ExecutionActionID])
REFERENCES [dbo].[ExecutionActions] ([ID])
GO
ALTER TABLE [dbo].[ActionParams] CHECK CONSTRAINT [FK_ActionParams_ExecutionAction]
GO
/****** Object:  ForeignKey [FK_Addresses_AddressType]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_AddressType] FOREIGN KEY([TypeID])
REFERENCES [dbo].[AddressTypes] ([ID])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_AddressType]
GO
/****** Object:  ForeignKey [FK_Addresses_Countries]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_Countries] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Countries] ([ID])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_Countries]
GO
/****** Object:  ForeignKey [FK_Addresses_Entities]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_Entities] FOREIGN KEY([EntityID])
REFERENCES [dbo].[Entities] ([ID])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_Entities]
GO
/****** Object:  ForeignKey [FK_Addresses_States]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_States] FOREIGN KEY([StateID])
REFERENCES [dbo].[States] ([ID])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_States]
GO
/****** Object:  ForeignKey [FK_Addresses_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Addresses]  WITH CHECK ADD  CONSTRAINT [FK_Addresses_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Addresses] CHECK CONSTRAINT [FK_Addresses_Status]
GO
/****** Object:  ForeignKey [FK_Clients_Entities]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Entities] FOREIGN KEY([EntityID])
REFERENCES [dbo].[Entities] ([ID])
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Entities]
GO
/****** Object:  ForeignKey [FK_Clients_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Clients]  WITH CHECK ADD  CONSTRAINT [FK_Clients_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Clients] CHECK CONSTRAINT [FK_Clients_Status]
GO
/****** Object:  ForeignKey [FK_Entities_Entities]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities]  WITH CHECK ADD  CONSTRAINT [FK_Entities_Entities] FOREIGN KEY([ParentID])
REFERENCES [dbo].[Entities] ([ID])
GO
ALTER TABLE [dbo].[Entities] CHECK CONSTRAINT [FK_Entities_Entities]
GO
/****** Object:  ForeignKey [FK_Entities_EntityCategories]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities]  WITH CHECK ADD  CONSTRAINT [FK_Entities_EntityCategories] FOREIGN KEY([CategoryID])
REFERENCES [dbo].[EntityCategories] ([ID])
GO
ALTER TABLE [dbo].[Entities] CHECK CONSTRAINT [FK_Entities_EntityCategories]
GO
/****** Object:  ForeignKey [FK_Entities_EntityType]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities]  WITH CHECK ADD  CONSTRAINT [FK_Entities_EntityType] FOREIGN KEY([TypeID])
REFERENCES [dbo].[EntityTypes] ([ID])
GO
ALTER TABLE [dbo].[Entities] CHECK CONSTRAINT [FK_Entities_EntityType]
GO
/****** Object:  ForeignKey [FK_Entities_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Entities]  WITH CHECK ADD  CONSTRAINT [FK_Entities_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Entities] CHECK CONSTRAINT [FK_Entities_Status]
GO
/****** Object:  ForeignKey [FK_EntityAddresses_Addresses]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[EntityAddresses]  WITH CHECK ADD  CONSTRAINT [FK_EntityAddresses_Addresses] FOREIGN KEY([AddressID])
REFERENCES [dbo].[Addresses] ([ID])
GO
ALTER TABLE [dbo].[EntityAddresses] CHECK CONSTRAINT [FK_EntityAddresses_Addresses]
GO
/****** Object:  ForeignKey [FK_EntityAddresses_Entities]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[EntityAddresses]  WITH CHECK ADD  CONSTRAINT [FK_EntityAddresses_Entities] FOREIGN KEY([EntityID])
REFERENCES [dbo].[Entities] ([ID])
GO
ALTER TABLE [dbo].[EntityAddresses] CHECK CONSTRAINT [FK_EntityAddresses_Entities]
GO
/****** Object:  ForeignKey [FK_EntityTypes_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[EntityTypes]  WITH CHECK ADD  CONSTRAINT [FK_EntityTypes_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[EntityTypes] CHECK CONSTRAINT [FK_EntityTypes_Status]
GO
/****** Object:  ForeignKey [FK_ExecutionAction_DataTypes]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[ExecutionActions]  WITH CHECK ADD  CONSTRAINT [FK_ExecutionAction_DataTypes] FOREIGN KEY([DataTypeID])
REFERENCES [dbo].[DataTypes] ([ID])
GO
ALTER TABLE [dbo].[ExecutionActions] CHECK CONSTRAINT [FK_ExecutionAction_DataTypes]
GO
/****** Object:  ForeignKey [FK_Funds_Clients]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Funds]  WITH CHECK ADD  CONSTRAINT [FK_Funds_Clients] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Clients] ([ID])
GO
ALTER TABLE [dbo].[Funds] CHECK CONSTRAINT [FK_Funds_Clients]
GO
/****** Object:  ForeignKey [FK_Funds_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Funds]  WITH CHECK ADD  CONSTRAINT [FK_Funds_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Funds] CHECK CONSTRAINT [FK_Funds_Status]
GO
/****** Object:  ForeignKey [FK_GroupRoles_Groups]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[GroupRoles]  WITH CHECK ADD  CONSTRAINT [FK_GroupRoles_Groups] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[GroupRoles] CHECK CONSTRAINT [FK_GroupRoles_Groups]
GO
/****** Object:  ForeignKey [FK_GroupRoles_Roles]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[GroupRoles]  WITH CHECK ADD  CONSTRAINT [FK_GroupRoles_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
ALTER TABLE [dbo].[GroupRoles] CHECK CONSTRAINT [FK_GroupRoles_Roles]
GO
/****** Object:  ForeignKey [FK_Groups_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Groups]  WITH CHECK ADD  CONSTRAINT [FK_Groups_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Groups] CHECK CONSTRAINT [FK_Groups_Status]
GO
/****** Object:  ForeignKey [FK_Jobs_AccountManagers]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Jobs]  WITH CHECK ADD  CONSTRAINT [FK_Jobs_AccountManagers] FOREIGN KEY([AccountManagerID])
REFERENCES [dbo].[AccountManagers] ([ID])
GO
ALTER TABLE [dbo].[Jobs] CHECK CONSTRAINT [FK_Jobs_AccountManagers]
GO
/****** Object:  ForeignKey [FK_Jobs_Funds]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Jobs]  WITH CHECK ADD  CONSTRAINT [FK_Jobs_Funds] FOREIGN KEY([FundID])
REFERENCES [dbo].[Funds] ([ID])
GO
ALTER TABLE [dbo].[Jobs] CHECK CONSTRAINT [FK_Jobs_Funds]
GO
/****** Object:  ForeignKey [FK_Jobs_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Jobs]  WITH CHECK ADD  CONSTRAINT [FK_Jobs_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Jobs] CHECK CONSTRAINT [FK_Jobs_Status]
GO
/****** Object:  ForeignKey [FK_Options_ExecutionAction]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options_ExecutionAction] FOREIGN KEY([ExecutionActionID])
REFERENCES [dbo].[ExecutionActions] ([ID])
GO
ALTER TABLE [dbo].[Options] CHECK CONSTRAINT [FK_Options_ExecutionAction]
GO
/****** Object:  ForeignKey [FK_Options_Options]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options_Options] FOREIGN KEY([ParentID])
REFERENCES [dbo].[Options] ([ID])
GO
ALTER TABLE [dbo].[Options] CHECK CONSTRAINT [FK_Options_Options]
GO
/****** Object:  ForeignKey [FK_Options_OptionTypes]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options_OptionTypes] FOREIGN KEY([TypeID])
REFERENCES [dbo].[OptionTypes] ([ID])
GO
ALTER TABLE [dbo].[Options] CHECK CONSTRAINT [FK_Options_OptionTypes]
GO
/****** Object:  ForeignKey [FK_Options_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Options]  WITH CHECK ADD  CONSTRAINT [FK_Options_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Options] CHECK CONSTRAINT [FK_Options_Status]
GO
/****** Object:  ForeignKey [FK_OptionTypes_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[OptionTypes]  WITH CHECK ADD  CONSTRAINT [FK_OptionTypes_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[OptionTypes] CHECK CONSTRAINT [FK_OptionTypes_Status]
GO
/****** Object:  ForeignKey [FK_RoleOptions_Options]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[RoleOptions]  WITH CHECK ADD  CONSTRAINT [FK_RoleOptions_Options] FOREIGN KEY([OptionID])
REFERENCES [dbo].[Options] ([ID])
GO
ALTER TABLE [dbo].[RoleOptions] CHECK CONSTRAINT [FK_RoleOptions_Options]
GO
/****** Object:  ForeignKey [FK_RoleOptions_Roles]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[RoleOptions]  WITH CHECK ADD  CONSTRAINT [FK_RoleOptions_Roles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[Roles] ([ID])
GO
ALTER TABLE [dbo].[RoleOptions] CHECK CONSTRAINT [FK_RoleOptions_Roles]
GO
/****** Object:  ForeignKey [FK_Roles_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [FK_Roles_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [FK_Roles_Status]
GO
/****** Object:  ForeignKey [FK_States_Countries]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[States]  WITH CHECK ADD  CONSTRAINT [FK_States_Countries] FOREIGN KEY([CountryID])
REFERENCES [dbo].[Countries] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[States] CHECK CONSTRAINT [FK_States_Countries]
GO
/****** Object:  ForeignKey [FK_Telephones_Accounts]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Telephones]  WITH CHECK ADD  CONSTRAINT [FK_Telephones_Accounts] FOREIGN KEY([AccountID])
REFERENCES [dbo].[AccountManagers] ([ID])
GO
ALTER TABLE [dbo].[Telephones] CHECK CONSTRAINT [FK_Telephones_Accounts]
GO
/****** Object:  ForeignKey [FK_Telephones_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Telephones]  WITH CHECK ADD  CONSTRAINT [FK_Telephones_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Telephones] CHECK CONSTRAINT [FK_Telephones_Status]
GO
/****** Object:  ForeignKey [FK_Telephones_TelephoneType]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Telephones]  WITH CHECK ADD  CONSTRAINT [FK_Telephones_TelephoneType] FOREIGN KEY([TypeID])
REFERENCES [dbo].[TelephoneTypes] ([ID])
GO
ALTER TABLE [dbo].[Telephones] CHECK CONSTRAINT [FK_Telephones_TelephoneType]
GO
/****** Object:  ForeignKey [FK_TelephoneTypes_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[TelephoneTypes]  WITH CHECK ADD  CONSTRAINT [FK_TelephoneTypes_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[TelephoneTypes] CHECK CONSTRAINT [FK_TelephoneTypes_Status]
GO
/****** Object:  ForeignKey [FK_Users_AccountManagers]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_AccountManagers] FOREIGN KEY([AccountID])
REFERENCES [dbo].[AccountManagers] ([ID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_AccountManagers]
GO
/****** Object:  ForeignKey [FK_Users_Groups]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Groups] FOREIGN KEY([GroupID])
REFERENCES [dbo].[Groups] ([ID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Groups]
GO
/****** Object:  ForeignKey [FK_Users_Status]    Script Date: 09/26/2013 15:33:14 ******/
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Status] FOREIGN KEY([StatusID])
REFERENCES [dbo].[Status] ([ID])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Status]
GO
