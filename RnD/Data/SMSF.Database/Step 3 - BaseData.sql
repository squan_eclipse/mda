﻿USE [DBASystem]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].[Status] ON
INSERT [dbo].[Status] ([ID], [Title], [Description]) VALUES (1, N'Active', N'Active')
INSERT [dbo].[Status] ([ID], [Title], [Description]) VALUES (2, N'Inactive', N'Inactive')
INSERT [dbo].[Status] ([ID], [Title], [Description]) VALUES (3, N'Deleted', N'Deleted')
SET IDENTITY_INSERT [dbo].[Status] OFF

/****** Object:  Table [dbo].[Roles]    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].[Roles] ON
INSERT [dbo].[Roles] ([ID], [Title], [Description], [StatusID], [IsSystem]) VALUES (2, N'Auditor', N'Auditor', 1, 1)
INSERT [dbo].[Roles] ([ID], [Title], [Description], [StatusID], [IsSystem]) VALUES (3, N'Accountant', N'Accountant', 1, 1)
INSERT [dbo].[Roles] ([ID], [Title], [Description], [StatusID], [IsSystem]) VALUES (4, N'Admin', N'Administrator', 1, 1)
SET IDENTITY_INSERT [dbo].[Roles] OFF

/****** Object:  Table [dbo].EntityCategories    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].EntityCategories ON
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (1, N'System', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (2, N'Club or Society', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (3, N'Company', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (4, N'Individual', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (5, N'Non-profit Organisation', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (6, N'Partnership', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (7, N'SMSF', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (8, N'Sole Trader', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (9, N'Superannuation Fund', 1)
INSERT [dbo].EntityCategories ([ID], [Description], [StatusID]) VALUES (10, N'Trust', 1)
SET IDENTITY_INSERT [dbo].EntityCategories OFF

/****** Object:  Table [dbo].EntityTypes    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].EntityTypes ON
INSERT [dbo].EntityTypes ([ID], [Description], [StatusID]) VALUES (1, N'Entity', 1)
INSERT [dbo].EntityTypes ([ID], [Description], [StatusID]) VALUES (2, N'Group', 1)
INSERT [dbo].EntityTypes ([ID], [Description], [StatusID]) VALUES (3, N'Client', 1)
INSERT [dbo].EntityTypes ([ID], [Description], [StatusID]) VALUES (4, N'Fund', 1)
SET IDENTITY_INSERT [dbo].EntityTypes OFF

/****** Object:  Table [dbo].[Groups]    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].[Groups] ON
INSERT [dbo].[Groups] ([ID], [Title], [Description], [StatusID], [IsSystem]) VALUES (1, N'System Administrators', N'System administrators group', 1, 1)
INSERT [dbo].[Groups] ([ID], [Title], [Description], [StatusID], [IsSystem]) VALUES (2, N'Administrators', N'Entity administrators group', 1, 0)
INSERT [dbo].[Groups] ([ID], [Title], [Description], [StatusID], [IsSystem]) VALUES (3, N'Operators', N'Users perform the business-operations e.g. Accountants, Auditors, Managers etc', 1, 0)
INSERT [dbo].[Groups] ([ID], [Title], [Description], [StatusID], [IsSystem]) VALUES (4, N'Clients', N'Clients group', 1, 0)
SET IDENTITY_INSERT [dbo].[Groups] OFF

/****** Object:  Table [dbo].[[GroupRoles]]    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].[GroupRoles] ON
INSERT [dbo].[GroupRoles] ([ID], [GroupID], [RoleID]) VALUES (1, 1, 4)
SET IDENTITY_INSERT [dbo].[GroupRoles] OFF

/****** Object:  Table [dbo].[OptionTypes]    Script Date: 09/02/2013 11:13:52 ******/
SET IDENTITY_INSERT [dbo].[OptionTypes] ON
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (3, N'Page', N'Page', 1)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (4, N'Menu', N'Main', 1)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (5, N'NextPage', N'Main', 1)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (6, N'Report', N'Main', 1)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (7, N'View', N'Read Only Access', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (8, N'Modify', N'Edit Update', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (9, N'New/Add', N'Add Update', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (10, N'Remove', N'Delete Update', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (11, N'Cancel', N'Cancel', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (12, N'Print', N'Print', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (13, N'Search', N'Search', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (14, N'Maker', N'Maker', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (15, N'Checker', N'Checker', 2)
INSERT [dbo].[OptionTypes] ([ID], [Title], [Description], [StatusID]) VALUES (16, N'CustomAction', N'CustomAction', 2)
SET IDENTITY_INSERT [dbo].[OptionTypes] OFF

/****** Object:  Table [dbo].[[[Entities]]]    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].[Entities] ON
INSERT [dbo].[Entities] ([ID], [TypeID], [CategoryID], [ABN], [ACN], [GST], [TFN], [Name], [AddressID], [Note], [ParentID], [PrimaryAccountManagerID], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, 1, NULL, NULL, NULL, NULL, N'SYSTEM', NULL, NULL, NULL, NULL, 1, 0, CAST(0x0000A22800BD63C8 AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[Entities] OFF

/****** Object:  Table [dbo].[[[[AccountManagers]]]]    Script Date: 08/30/2013 11:25:06 ******/
SET IDENTITY_INSERT [dbo].[AccountManagers] ON
INSERT [dbo].[AccountManagers] ([ID], [EntityID], [FirstName], [MidName], [LastName], [Email], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, N'System', NULL, 'Administrator', 'administrator@dbasystem.com', 1, 0, CAST(0x0000A22800BD63D8 AS DateTime), 0, CAST(0x0000A22800BD63D8 AS DateTime))
SET IDENTITY_INSERT [dbo].[AccountManagers] OFF

/****** Object:  Table [dbo].[Users]    Script Date: 08/30/2013 11:25:06 ******/
INSERT [dbo].[Users] ([AccountID], [GroupID], [Login], [Password], [CreateDate], [LastLoginTime], [StatusID], [IsSystem], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, N'admin', N'admin', CAST(0x0000A22001042E3E AS DateTime), NULL, 1, 1, 0, CAST(0x0000A22001042E3E AS DateTime), 0, CAST(0x0000A22001042E3E AS DateTime))

/****** Object:  Table [dbo].[AddressTypes]    Script Date: 09/02/2013 13:14:34 ******/
SET IDENTITY_INSERT [dbo].[AddressTypes] ON
INSERT [dbo].[AddressTypes] ([ID], [Description], [StatusID]) VALUES (1, N'Business', 1)
INSERT [dbo].[AddressTypes] ([ID], [Description], [StatusID]) VALUES (2, N'Mailing', 1)
INSERT [dbo].[AddressTypes] ([ID], [Description], [StatusID]) VALUES (3, N'PO Box', 1)
SET IDENTITY_INSERT [dbo].[AddressTypes] OFF

/****** Object:  Table [dbo].[TelephoneTypes]    Script Date: 09/02/2013 13:14:34 ******/
SET IDENTITY_INSERT [dbo].[TelephoneTypes] ON
INSERT [dbo].[TelephoneTypes] ([ID], [Type], [StatusID]) VALUES (1, N'Fax', 1)
INSERT [dbo].[TelephoneTypes] ([ID], [Type], [StatusID]) VALUES (2, N'Mobile', 1)
INSERT [dbo].[TelephoneTypes] ([ID], [Type], [StatusID]) VALUES (3, N'Fixed Line', 1)
SET IDENTITY_INSERT [dbo].[TelephoneTypes] OFF

/****** Object:  Table [dbo].[Countries]    Script Date: 09/03/2013 14:11:33 ******/
SET IDENTITY_INSERT [dbo].[Countries] ON
INSERT [dbo].[Countries] ([ID], [Name], [Default]) VALUES (1, N'Australia', 1)
SET IDENTITY_INSERT [dbo].[Countries] OFF

/****** Object:  Table [dbo].[States]    Script Date: 09/03/2013 14:11:33 ******/
SET IDENTITY_INSERT [dbo].[States] ON
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (1, 1, N'Australian Capital Territory', NULL)
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (2, 1, N'New South Wales', NULL)
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (3, 1, N'Northern Territory', NULL)
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (4, 1, N'Queensland', NULL)
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (5, 1, N'South Australia', NULL)
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (6, 1, N'Tasmania', NULL)
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (7, 1, N'Victoria', NULL)
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (8, 1, N'Western Australia', NULL)
SET IDENTITY_INSERT [dbo].[States] OFF