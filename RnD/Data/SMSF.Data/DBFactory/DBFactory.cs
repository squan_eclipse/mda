﻿using System.Data.Entity;
using DBASystem.SMSF.Data.SMSF.Entities;
using Framework.DBContextFactory;
using Framework;

namespace DBASystem.SMSF.Data.Factory
{
    /// <summary>
    /// It provides the singalton context of DB
    /// </summary>
    public sealed class DBFactory : Disposable, IDBFactory
    {
        private SMSFEntities dataContext;

        private DBFactory()
        {
            Database.SetInitializer<SMSFEntities>(null);
        }

        public static IDBFactory Instance
        {
            get 
            {
                return new DBFactory();
            }
        }

        public DbContext DataContext
        {
            get
            {
                if (dataContext == null)
                    dataContext = SMSFEntities.GetDataContext();
                return dataContext;
            }
        }
        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}
