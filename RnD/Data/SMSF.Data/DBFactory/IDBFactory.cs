﻿using System;
using DBASystem.SMSF.Data.SMSF.Entities;

namespace DBASystem.SMSF.Data.Factory
{
    public interface IDBFactory : IDisposable
    {
        SMSFEntities SMSFEntities{get;}
    }
}
