﻿using DBASystem.SMSF.Data.SMSF.Entities;
using Framework.IRepositories;

namespace DBASystem.SMSF.Data.IRepositories
{
    public interface IAccountManagerRespository : IRepositoryBase<AccountManager>
    {
    }
    public interface IAddressRepository : IRepositoryBase<Address>
    {
    }
    public interface IAddressTypeRepository : IRepositoryBase<AddressType>
    {
    }
    public interface IEntityRepository : IRepositoryBase<Entity>
    {
    }
    public interface IEntityTypeRepository : IRepositoryBase<EntityType>
    {
    }
    public interface IEntityCategoryRepository : IRepositoryBase<EntityCategory>
    {
    }
    public interface ITelephoneRepository : IRepositoryBase<Telephone>
    {
    }
    public interface IUserRepository : IRepositoryBase<User>
    {
    }
    public interface IClientRepository : IRepositoryBase<Client>
    {
    }
    public interface ICountryRepository : IRepositoryBase<Country>
    {
    }
    public interface IEntityAddressRepository : IRepositoryBase<EntityAddress>
    {
    }

    public interface IAccountManagerRepository : IRepositoryBase<AccountManager>
    {
    }

    public interface IAccountManagerRoleRepository : IRepositoryBase<AccountManagerRole>
    {
    }
    public interface IAccountRoleRepository : IRepositoryBase<AccountRole>
    {
    }
    public interface IActionParamRepository : IRepositoryBase<ActionParam>
    {
    }
    public interface IExecutionActionRepository : IRepositoryBase<ExecutionAction>
    {
    }
    public interface IDataTypeRepository : IRepositoryBase<DataType>
    {
    }
    public interface IGroupRepository : IRepositoryBase<Group>
    {
    }
    public interface IGroupRoleRepository : IRepositoryBase<GroupRole>
    {
    }
    public interface IRoleRepository : IRepositoryBase<Role>
    {
    }
    public interface IRoleOptionRepository : IRepositoryBase<RoleOption>
    {
    }
   public interface IOptionRepository : IRepositoryBase<Option>
    {
    }
   public interface IOptionTypeRepository : IRepositoryBase<OptionType>
    {
    }
    public interface IStateRepository : IRepositoryBase<State>
    {
    }
    public interface IStatusRepository : IRepositoryBase<Status>
    {
    }
    public interface ITelephoneTypeRepository : IRepositoryBase<TelephoneType>
    {
    }
    public interface IJobRepository : IRepositoryBase<Job>
    {
    }
    public interface IFundRepository : IRepositoryBase<Fund>
    {
    }
}
