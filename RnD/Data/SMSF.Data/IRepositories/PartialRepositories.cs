﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBASystem.SMSF.Data.IRepositories;
using DBASystem.SMSF.Data.SMSF.Entities;

namespace DBASystem.SMSF.Data.Repositories
{

    public partial class UserRepository : SMSFRepositoryBase<User>, IUserRepository
    {
        public UserRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class TelephoneRepository : SMSFRepositoryBase<Telephone>, ITelephoneRepository
    {
        public TelephoneRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class EntityRepository : SMSFRepositoryBase<Entity>, IEntityRepository
    {
        public EntityRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class EntityTypeRepository : SMSFRepositoryBase<EntityType>, IEntityTypeRepository
    {
        public EntityTypeRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class EntityCategoryRepository : SMSFRepositoryBase<EntityCategory>, IEntityCategoryRepository
    {
        public EntityCategoryRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class AddressRepository : SMSFRepositoryBase<Address>, IAddressRepository
    {
        public AddressRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class EntityAddressRepository : SMSFRepositoryBase<EntityAddress>, IEntityAddressRepository
    {
        public EntityAddressRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }

    }
    public partial class AccountManagerRepository : SMSFRepositoryBase<AccountManager>, IAccountManagerRepository
    {
        public AccountManagerRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class AccountManagerRoleRepository : SMSFRepositoryBase<AccountManagerRole>, IAccountManagerRoleRepository
    {
        public AccountManagerRoleRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }
    public partial class AccountRoleRepository : SMSFRepositoryBase<AccountRole>, IAccountRoleRepository
    {
        public AccountRoleRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }
    public partial class AddressTypeRepository : SMSFRepositoryBase<AddressType>, IAddressTypeRepository
    {
        public AddressTypeRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class ActionParamRepository : SMSFRepositoryBase<ActionParam>, IActionParamRepository
    {
        public ActionParamRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class ExecutionActionRepository : SMSFRepositoryBase<ExecutionAction>, IExecutionActionRepository
    {
        public ExecutionActionRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class DataTypeRepository : SMSFRepositoryBase<DataType>, IDataTypeRepository
    {
        public DataTypeRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class ClientRepository : SMSFRepositoryBase<Client>, IClientRepository
    {
        public ClientRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class CountryRepository : SMSFRepositoryBase<Country>, ICountryRepository
    {
        public CountryRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class GroupRepository : SMSFRepositoryBase<Group>, IGroupRepository
    {
        public GroupRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class GroupRoleRepository : SMSFRepositoryBase<GroupRole>, IGroupRoleRepository
    {
        public GroupRoleRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class RoleRepository : SMSFRepositoryBase<Role>, IRoleRepository
    {
        public RoleRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class RoleOptionRepository : SMSFRepositoryBase<RoleOption>, IRoleOptionRepository
    {
        public RoleOptionRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class OptionRepository : SMSFRepositoryBase<Option>, IOptionRepository
    {
        public OptionRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class OptionTypeRepository : SMSFRepositoryBase<OptionType>, IOptionTypeRepository
    {
        public OptionTypeRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class StateRepository : SMSFRepositoryBase<State>, IStateRepository
    {
        public StateRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class StatusRepository : SMSFRepositoryBase<Status>, IStatusRepository
    {
        public StatusRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class TelephoneTypeRepository : SMSFRepositoryBase<TelephoneType>, ITelephoneTypeRepository
    {
        public TelephoneTypeRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class JobRepository : SMSFRepositoryBase<Job>, IJobRepository
    {
        public JobRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }

    public partial class FundRepository : SMSFRepositoryBase<Fund>, IFundRepository
    {
        public FundRepository(SMSFEntities dataContext)
            : base(dataContext)
        {
        }
    }
}
