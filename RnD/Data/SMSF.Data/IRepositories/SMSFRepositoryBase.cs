﻿
using DBASystem.SMSF.Data.Repositories;
using DBASystem.SMSF.Data.SMSF.Entities;
using Framework.Repositories;

namespace DBASystem.SMSF.Data.IRepositories
{
    public class SMSFRepositoryBase<TEntity> : RepositoryBase<TEntity> where TEntity : class
    {
        public SMSFRepositoryBase(SMSFEntities datacontext)
            : base(datacontext)
        {
        }

        protected SMSFEntities SMSFDataContext 
        {
            get
            {
                return this._dataContext as SMSFEntities;
            }
        }

    }
}
