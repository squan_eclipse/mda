﻿using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.IRepositories;
using DBASystem.SMSF.Data.SMSF.Entities;
using DBASystem.SMSF.Data.Factory;
using System.Linq;
using System.Collections.Generic;
using DBASystem.SMSF.Data.Common;
using DBASystem.SMSF.Contracts.Common;
using System;
using DBASystem.SMSF.Contracts.ViewModels;

namespace DBASystem.SMSF.Data.Repositories
{
    public partial class UserRepository : SMSFRepositoryBase<User>, IUserRepository
    {
        public bool IsAllowed(int userID, Guid authCode, DBASystem.SMSF.Contracts.Common.Option option)
        {
            return (from user in SMSFDataContext.Users
                    join gr in SMSFDataContext.GroupRoles
                          on user.GroupID equals gr.GroupID
                    join ro in SMSFDataContext.RoleOptions
                          on gr.RoleID equals ro.RoleID
                          where 
                            user.AccountID == userID
                            &&
                            user.AuthCode == authCode
                            &&
                            ro.OptionID == (int)option
                          select  ro.OptionID ).Any();
        }

        public ActionSignature GetActionSignature(DBASystem.SMSF.Contracts.Common.Option anOption)
        {
            var list =(
                from option in SMSFDataContext.Options

                join execAction in SMSFDataContext.ExecutionActions
                on option.ExecutionActionID equals execAction.ID

                join component in SMSFDataContext.DataTypes
                on execAction.DataTypeID equals component.ID

                join param in SMSFDataContext.ActionParams
                on execAction.ID equals param.ExecutionActionID

                join paramType in SMSFDataContext.DataTypes
                on param.DataTypeID equals paramType.ID

                where
                  option.ID == (int)anOption
                select new ActionSignature
                {
                    ComponentAssemblyName = component.AssemblyName,
                    ComponentNamespace = component.Namespace,
                    ComponentClassName = component.Class,
                    ActionName = execAction.ActionName,
                    ParamName = param.ParamName,
                    ParamTypeAssemblyName = paramType.AssemblyName,
                    ParamTypeNamespace = paramType.Namespace,
                    ParamTypeClassName = paramType.Class 
                }).FirstOrDefault();
            return list;
        }

        public List<DBASystem.SMSF.Contracts.Common.Option> GetAllowedOptionsByOptionType(int UserID, DBASystem.SMSF.Contracts.Common.OptionType optionType)
        {
            return (from user in SMSFDataContext.Users
                    join gr in SMSFDataContext.GroupRoles
                    on user.GroupID equals gr.GroupID
                    join ro in SMSFDataContext.RoleOptions
                    on gr.RoleID equals ro.RoleID
                    join o in SMSFDataContext.Options
                    on ro.OptionID equals o.ID
                    where
                      (DBASystem.SMSF.Contracts.Common.Status)o.StatusID == Contracts.Common.Status.Active
                      &&
                      (user.AccountID == UserID
                      &&
                      ro.IsAssigned == true
                      &&
                      o.TypeID == (int)optionType)
                      ||
                      o.AllowAnonymous
                    select (DBASystem.SMSF.Contracts.Common.Option)ro.OptionID).ToList();
        }
    }
}
