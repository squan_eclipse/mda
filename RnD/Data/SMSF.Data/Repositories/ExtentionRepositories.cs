﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBASystem.SMSF.Data.SMSF.Entities;

namespace DBASystem.SMSF.Data.Repositories
{
    public static class ExtentionRepositories
    {
        public static int GetMaxID(this ActionParamRepository repo)
        {
            int ID = 0;
            var dt = repo.GetAll();
            if (dt.Count() > 0)
                ID = dt.Max(entity => entity.ID);
            return ID;
        }
        public static int Add(this ActionParamRepository repo, int ID, int ParamDataTypeID, string ParamName, int ExecutionActionID)
        {
            ActionParam actionParam = repo.GetLocal(a => a.DataTypeID == ParamDataTypeID && a.ParamName == ParamName && a.ExecutionActionID == ExecutionActionID);
            if (actionParam == null)
            {
                actionParam = new ActionParam
                {
                    ID = ID,
                    DataTypeID = ParamDataTypeID,
                    ParamName = ParamName,
                    ExecutionActionID = ExecutionActionID
                };
                repo.Add(actionParam);
            }
            return actionParam.ID;
        }

        public static int GetMaxID(this DataTypeRepository repo)
        {
            int ID = 0;
            var dt = repo.GetAll();
            if (dt.Count() > 0)
                ID = dt.Max(entity => entity.ID);
            return ID;
        }
        public static int Add(this DataTypeRepository repo, int ID, string assemblyName, string ComponentName, string Namespace )
        {
            DataType dataType = repo.GetLocal(a => a.AssemblyName == assemblyName && a.Class == ComponentName && a.Namespace == Namespace);
            if (dataType == null)
            {
                dataType = new DataType
                {
                    ID = ID,
                    AssemblyName = assemblyName,
                    Class = ComponentName,
                    Namespace = Namespace
                };
                repo.Add(dataType);
            }
            return dataType.ID;
        }

        public static int GetMaxID(this ExecutionActionRepository repo)
        {
            int ID = 0;
            var dt = repo.GetAll();
            if (dt.Count() > 0)
                ID = dt.Max(entity => entity.ID);
            return ID;
        }
        public static int Add(this ExecutionActionRepository repo, int ID, string MethodName, int DataTypeID)
        {
            ExecutionAction executionAction = repo.GetLocal(a => a.ActionName == MethodName && a.DataTypeID == DataTypeID);
            if (executionAction == null)
            {
                executionAction = new ExecutionAction
                {
                    ID = ID,
                    DataTypeID = DataTypeID,
                    ActionName = MethodName
                };
                repo.Add(executionAction);
            }
            return executionAction.ID;
        }
    }
}
