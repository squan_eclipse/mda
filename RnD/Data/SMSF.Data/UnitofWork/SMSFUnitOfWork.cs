﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using common = DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Factory;
using DBASystem.SMSF.Data.IRepositories;
using DBASystem.SMSF.Data.Repositories;
using DBASystem.SMSF.Data.SMSF.Entities;
using Framework.UnitofWork;

namespace DBASystem.SMSF.Data.UnitofWork
{
    public class SMSFUnitOfWork : UnitOfWork<SMSFEntities>
    {
        public SMSFUnitOfWork()
            : base(DBFactory.Instance)
        {
            common.Utility.Logger.Log(NLog.LogLevel.Debug, "Create SMSFUnitofWork");
        }

        public SMSFRepositoryBase<TEntity> GetGenericRepository<TEntity>() where TEntity : class
        {
            return base.GetRepository<SMSFRepositoryBase<TEntity>, TEntity>() as SMSFRepositoryBase<TEntity>;
        }

        public static SMSFUnitOfWork CreateNewInstance
        {
            get
            {
                return new SMSFUnitOfWork();
            }
        }

        #region Public Repositories

        public UserRepository UserRepository
        {
            get
            {
                return base.GetRepository<UserRepository, User>() as UserRepository;
            }
        }

        public EntityRepository EntityRepository
        {
            get
            {
                return base.GetRepository<EntityRepository, Entity>() as EntityRepository;
            }
        }

        public EntityTypeRepository EntityTypeRepository
        {
            get
            {
                return base.GetRepository<EntityTypeRepository, EntityType>() as EntityTypeRepository;
            }
        }

        public EntityCategoryRepository EntityCategoryRepository
        {
            get
            {
                return base.GetRepository<EntityCategoryRepository, EntityCategory>() as EntityCategoryRepository;
            }
        }

        public AddressRepository AddressRepository
        {
            get
            {
                return base.GetRepository<AddressRepository, Address>() as AddressRepository;
            }

        }

        public TelephoneRepository TelephoneRepository
        {
            get
            {
                return base.GetRepository<TelephoneRepository, Telephone>() as TelephoneRepository;
            }
        }

        public EntityAddressRepository EntityAddressRepository
        {
            get
            {
                return base.GetRepository<EntityAddressRepository, EntityAddress>() as EntityAddressRepository;
            }
        }

        public AccountManagerRepository AccountManagerRepository
        {
            get
            {
                return base.GetRepository<AccountManagerRepository, AccountManager>() as AccountManagerRepository;
            }

        }

        public AccountManagerRoleRepository AccountManagerRoleRepository
        {
            get
            {
                return base.GetRepository<AccountManagerRoleRepository, AccountManagerRole>() as AccountManagerRoleRepository;
            }
        }

        public AccountRoleRepository AccountRoleRepository
        {
            get
            {
                return base.GetRepository<AccountRoleRepository, AccountRole>() as AccountRoleRepository;
            }
        }

        public AddressTypeRepository AddressTypeRepository
        {
            get
            {
                return base.GetRepository<AddressTypeRepository, AddressType>() as AddressTypeRepository;
            }
        }

        public ActionParamRepository ActionParamRepository
        {
            get
            {
                return base.GetRepository<ActionParamRepository, ActionParam>() as ActionParamRepository;
            }
        }

        public ExecutionActionRepository ExecutionActionRepository
        {
            get
            {
                return base.GetRepository<ExecutionActionRepository, ExecutionAction>() as ExecutionActionRepository;
            }
        }

        public DataTypeRepository DataTypeRepository
        {
            get
            {
                return base.GetRepository<DataTypeRepository, DataType>() as DataTypeRepository;
            }
        }

        public ClientRepository ClientRepository
        {
            get
            {
                return base.GetRepository<ClientRepository, Client>() as ClientRepository;
            }
        }

        public CountryRepository CountryRepository
        {
            get
            {
                return base.GetRepository<CountryRepository, Country>() as CountryRepository;
            }
        }

        public GroupRepository GroupRepository
        {
            get
            {
                return base.GetRepository<GroupRepository, Group>() as GroupRepository;
            }
        }

        public GroupRoleRepository GroupRoleRepository
        {
            get
            {
                return base.GetRepository<GroupRoleRepository, GroupRole>() as GroupRoleRepository;
            }
        }

        public RoleRepository RoleRepository
        {
            get
            {
                return base.GetRepository<RoleRepository, Role>() as RoleRepository;
            }
        }

        public RoleOptionRepository RoleOptionRepository
        {
            get
            {
                return base.GetRepository<RoleOptionRepository, RoleOption>() as RoleOptionRepository;
            }
        }

        public OptionRepository OptionRepository
        {
            get
            {
                return base.GetRepository<OptionRepository, Option>() as OptionRepository;
            }
        }

        public OptionTypeRepository OptionTypeRepository
        {
            get
            {
                return base.GetRepository<OptionTypeRepository, OptionType>() as OptionTypeRepository;
            }
        }

        public StateRepository StateRepository
        {
            get
            {
                return base.GetRepository<StateRepository, State>() as StateRepository;
            }
        }

        public StatusRepository StatusRepository
        {
            get
            {
                return base.GetRepository<StatusRepository, Status>() as StatusRepository;
            }
        }

        public TelephoneTypeRepository TelephoneTypeRepository
        {
            get
            {
                return base.GetRepository<TelephoneTypeRepository, TelephoneType>() as TelephoneTypeRepository;
            }
        }

        public JobRepository JobRepository
        {
            get
            {
                return base.GetRepository<JobRepository, Job>() as JobRepository;
            }
        }

        public FundRepository FundRepository
        {
            get
            {
                return base.GetRepository<FundRepository, Fund>() as FundRepository;
            }
        }

        #endregion Public Repositories
    }
}
