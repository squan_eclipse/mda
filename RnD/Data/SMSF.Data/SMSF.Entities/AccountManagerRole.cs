//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBASystem.SMSF.Data.SMSF.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class AccountManagerRole
    {
        public int ID { get; set; }
        public Nullable<int> AccountManagerID { get; set; }
        public Nullable<int> AccountRoleID { get; set; }
    
        public virtual AccountRole AccountRole { get; set; }
        public virtual AccountManager AccountManager { get; set; }
    }
}
