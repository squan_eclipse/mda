//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBASystem.SMSF.Data.SMSF.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Country
    {
        public Country()
        {
            this.Addresses = new HashSet<Address>();
            this.States = new HashSet<State>();
        }
    
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Default { get; set; }
    
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<State> States { get; set; }
    }
}
