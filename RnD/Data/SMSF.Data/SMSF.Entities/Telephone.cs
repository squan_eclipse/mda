//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBASystem.SMSF.Data.SMSF.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Telephone
    {
        public int ID { get; set; }
        public int AccountID { get; set; }
        public Nullable<int> TypeID { get; set; }
        public string CountryCode { get; set; }
        public string AreaCode { get; set; }
        public string PhoneNumber { get; set; }
        public int StatusID { get; set; }
    
        public virtual Status Status { get; set; }
        public virtual TelephoneType TelephoneType { get; set; }
        public virtual AccountManager AccountManager { get; set; }
    }
}
