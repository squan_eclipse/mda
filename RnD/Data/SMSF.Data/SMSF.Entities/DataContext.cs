﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBASystem.SMSF.Data.SMSF.Entities
{
    using DBASystem.SMSF.Contracts.Common;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    public partial class SMSFEntities : DbContext
    {
        public static string _localConnectionString = string.Empty;

        public SMSFEntities(string connectionString)
            : base(connectionString)
        {

        }

        public static string LocalConnectionString 
        {
            get
            {
                if (_localConnectionString == string.Empty)
                {
                    _localConnectionString = GetConnectionString();

                    if (_localConnectionString == string.Empty )
                        Utility.Logger.Info("using the default connection string");
                    else
                        Utility.Logger.Info("using the local connection string: {0}", LocalConnectionString);
                }

                return _localConnectionString;
            }
        } 
        
        public static string GetConnectionString()
        {
            try
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings[System.Environment.MachineName].ConnectionString;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static SMSFEntities GetDataContext()
        {
            if (LocalConnectionString == string.Empty)
                return new SMSFEntities();
            else  
                return new SMSFEntities(LocalConnectionString);
        }
    }
}
