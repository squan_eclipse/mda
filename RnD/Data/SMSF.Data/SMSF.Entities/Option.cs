//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DBASystem.SMSF.Data.SMSF.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Option
    {
        public Option()
        {
            this.Options1 = new HashSet<Option>();
            this.RoleOptions = new HashSet<RoleOption>();
        }
    
        public int ID { get; set; }
        public string Title { get; set; }
        public string PageURL { get; set; }
        public string NextPageURL { get; set; }
        public int TypeID { get; set; }
        public Nullable<int> ParentID { get; set; }
        public Nullable<int> ExecutionActionID { get; set; }
        public bool AllowAnonymous { get; set; }
        public int StatusID { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
    
        public virtual ExecutionAction ExecutionAction { get; set; }
        public virtual ICollection<Option> Options1 { get; set; }
        public virtual Option Option1 { get; set; }
        public virtual OptionType OptionType { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<RoleOption> RoleOptions { get; set; }
    }
}
