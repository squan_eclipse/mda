﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBASystem.SMSF.Data.Common
{
    public class ActionSignature
    {
        public string ComponentAssemblyName { get; set; }
        public string ComponentNamespace { get; set; }
        public string ComponentClassName { get; set; }
        public string ActionName { get; set; }
        public string ParamName { get; set; }
        public string ParamTypeAssemblyName { get; set; }
        public string ParamTypeNamespace { get; set; }
        public string ParamTypeClassName { get; set; }
    }
}
