﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Collections.Generic;

using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.UnitofWork;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data.Repositories;
using DBASystem.SMSF.Data.SMSF.Entities;
using c = DBASystem.SMSF.Service.Business.Components;
 
namespace ExecActionExtractionTool
{
    public partial class frmStep1 : Form
    {
        #region DataMembers
        SMSFUnitOfWork uow = SMSFUnitOfWork.CreateNewInstance;
        ExecutionActionRepository execActionRepo;
        ActionParamRepository actionParamRepo;
        DataTypeRepository dataTypeRepo;
        #endregion DataMembers

        public frmStep1()
        {
            InitializeComponent();
            execActionRepo = uow.ExecutionActionRepository;
            actionParamRepo = uow.ActionParamRepository;
            dataTypeRepo = uow.DataTypeRepository;
        }

        private IList<ComponentList> GetServiceComponents()
        {
            Type t = typeof(c.Component);
            return (from type in t.Assembly.GetTypes()
                        let methods = type.GetMethods().Where(m => m.GetCustomAttributes(typeof(ActionAttribute)).Any())
                        where type.Name.Contains("Component")
                    where !type.IsAbstract && type.BaseType == t && type.IsClass
                    select new ComponentList { Component = type, ComponentName = type.GetTypeName(), Methods = methods }).ToList();       
        }

        private void btnLoadComponents_Click(object sender, EventArgs e)
        {
            this.Enabled = false;
            var components = GetServiceComponents();
            int dtID = dataTypeRepo.GetMaxID();
            int eaID = execActionRepo.GetMaxID();
            int apID = actionParamRepo.GetMaxID();
            foreach (var obj in components.ToList())
            {
                foreach (var method in obj.Methods)
                {
                    string assemblyName = Path.GetFileName(obj.Component.Assembly.Location);
                    int dataTypeID = dataTypeRepo.Add(++dtID, assemblyName, obj.ComponentName, obj.Component.Namespace);
                    int execActionID = execActionRepo.Add(++eaID, method.Name, dataTypeID);
                    foreach (var param in method.GetParameters())
                    {
                        string assemblyFile = Path.GetFileName(param.ParameterType.Assembly.Location);
                        int paramDatatTypeID = dataTypeRepo.Add(++dtID, assemblyFile, param.ParameterType.GetTypeName(), param.ParameterType.Namespace);
                        int actionParamID = actionParamRepo.Add(++apID, paramDatatTypeID, param.Name, execActionID);
                    }
                }
            }
            uow.Commit();
            this.Enabled = true;
        }
    }

    public class ComponentList
    {
        public Type Component { get; set; }
        public string ComponentName { get; set; }
        public IEnumerable<MethodInfo> Methods { get; set; }
    }

    public static class ExtensionHelper
    {
        public static string Paramz(this MethodInfo mi)
        {
            return string.Join(",", mi.GetParameters().Select(pa => pa.ParameterType.GetTypeName()).ToArray());
        }

        public  static string GetTypeName(this Type t)
        {
            if (!t.IsGenericType)
                return t.Name;
            string genericTypeName = t.GetGenericTypeDefinition().Name;
            genericTypeName = genericTypeName.Substring(0,
                genericTypeName.IndexOf('`'));
            string genericArgs = string.Join(",",
                t.GetGenericArguments()
                    .Select(ta => GetTypeName(ta)).ToArray());
            return genericTypeName + "<" + genericArgs + ">";
        }
    }
}
