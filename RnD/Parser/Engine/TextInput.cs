﻿using System.Collections;

namespace Parser
{
    public class TextInput :InputAdaptor
    {
        #region "Fields"        
        public string[] words;
        public string newLine = "~N";
        public string doubleSpace = "~D";
        public string wordsData, lastTraceValue;
        public int wordDataIndex, currentWord, doubleSpacesCount, valueDistance, valueGlobalDistance;
        public Hashtable wordsPos = new Hashtable();
        public DataValuesLines _values = new DataValuesLines();
        int _valueWordIndex;

        #endregion
        public static string getTrimLineText(string text)
        {
            for (int i = 0; i < 5; i++)
                text = text.Replace("\r\n\r\n", "\r\n");

            text = text.Replace("  ", "~~");
            return text;
        }
        public override bool Command(string name)
        {
            switch (name)
            {
                case "values-clear":
                    clearValues();
                    break;
                case "values-newline":
                    newValueLine();
                    break;
                case "skip-line":
                    skipLine();
                    break;
                case "reset-line":
                    resetLine();
                    break;
                case "skip-word":
                    currentWord++;
                    break;
             }
            return base.Command(name);
        }
        public override void Load(string inputText)
        {
            for (int i = 0; i < 5; i++)
                inputText = inputText.Replace("\r\n\r\n", "\r\n");
            for (int i = 0; i < 5; i++)
                inputText = inputText.Replace("\n\n", "\n");

            inputText = inputText.Replace("  ", " " + doubleSpace + " ").Replace("\r\n", " " + newLine + " ");

            inputText = inputText.Replace("\n", " " + newLine + " ");

            words = Utils.splitString(inputText, " ");

            wordsData = "";
            for (int i = 0; i < words.Length; i++)
            {
                wordsPos.Add(wordsData.Length, i);
                wordsData = wordsData + words[i] + " ";
            }
            engine.JS.SetValue("values", _values);
            _values.engine = engine;
            
            currentWord = 0;
            wordDataIndex = 0;
        }

        #region "Functions"
        public void skipLine(int c = 1)
        {
            for (int i = 0; i < c; i++)
            {
                while (currentWord < words.Length && words[currentWord] != newLine) { currentWord++; }
                currentWord++;
            }
        }
        public void skipLines(int c)
        {
            skipLine(c);
        }
        public void resetLine()
        {
            while (currentWord > -1 && words[currentWord] != newLine) currentWord--;
            currentWord++;
        }
        public bool endOfLine()
        {
            if (currentWord < words.Length)
                return words[currentWord] == newLine;
            else
                return true;

        }

        public int getValueDistance() { return (valueDistance); }
        public int getWordDistance(int d)
        {
            int dist = 0;
            while (d > -1 && words[d] != newLine)
            {
                dist = dist + words[d].Length;
                d--;
            }
            return dist;
        }

        public int lineEndingWord()
        {
            int i = currentWord;
            while (words[i] != newLine)
                i++;
            return (i);
        }
        public bool endOfFile()
        {
            if (currentWord < words.Length)
                return false;
            else
                return true;

        }
        public void skipDoubleSpaces()
        {
            doubleSpacesCount = 0;
            while (currentWord < words.Length && words[currentWord] == doubleSpace) { currentWord++; doubleSpacesCount++; }

        }
        public string getValue()
        {
            return (lastTraceValue);
        }
        public bool isPageNumber()
        {
            return (System.Text.RegularExpressions.Regex.IsMatch(lastTraceValue, "Page [0-9]"));
        }
        public string getCurrentWord()
        {
            if (currentWord < words.Length)
                return (words[currentWord]);
            else
                return "end-of-file";
        }
        public bool isDoubleSpace()
        {
            if (currentWord < words.Length)
                return words[currentWord] == doubleSpace;
            else
                return true;
        }
        #endregion

        #region "Values"
        public DataValuesLines getValues() { return _values; }
        public DataValuesLines values
        {
            get
            {
                return _values;
            }
        }

        public void clearValues()
        {
            _values.lines.Clear();
        }
        public void newValueLine()
        {
            _values.createNewLine();
        }
        public int valueWordIndex
        {
            get { return _valueWordIndex; }
            set { _valueWordIndex = value; }
        }
        public void pushValue(string value, int distance)
        {
            DataValue dv = _values.currentLine.pushValue(value, distance);
            dv.startingWordIndex = _valueWordIndex;
            dv.endingWordIndex = currentWord - 1;
        }
        #endregion
    }
}