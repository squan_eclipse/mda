﻿using System;
using System.Text;

namespace Parser
{
    public class Engine
    {
        #region "Fields"
        public InputAdaptor inputAdaptor;
        TextInput _text;
        ExcelInput _excel;
        PdfInput _pdf;
        public Exception lastError;
        public bool hasError;

        public System.Text.StringBuilder output = new StringBuilder();
        public Jint.Engine JS;
        public Commands.Command rootCommand = new Commands.Command();
        public Commands.Command currentCommand;
        public Logger Log = new Logger();

        public string outputType;

        #endregion

        #region "Parsing"
        string LoadTemplate(string templateText)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(templateText);

            string inputType = Utils.getAttributeValue(doc.DocumentElement, "input").ToLower();
            outputType = Utils.getAttributeValue(doc.DocumentElement, "output");

            if (inputType.Length == 0) inputType = "text";

            Log.log.Clear();
            output.Clear();

            JS = new Jint.Engine();
            JS.SetValue("parser", this);
            JS.Execute("function xmlize(v){return(v.replace('&','&amp;'));}");

            rootCommand.setNode(doc.DocumentElement, this, rootCommand);

            return inputType;
        }
        InputAdaptor PrepareInput(string type)
        {
            string cName = GetType().Namespace + "." + Utils.titleCase(type) + "Input";
            inputAdaptor = (InputAdaptor)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(cName, true);

            if (inputAdaptor != null)
            {
                inputAdaptor.engine = this;
                if (inputAdaptor.GetType().IsAssignableFrom(typeof(TextInput)))
                {
                    _text = (TextInput)inputAdaptor;
                    JS.SetValue("text", _text);
                }

                if (inputAdaptor.GetType().IsAssignableFrom(typeof(PdfInput)))
                {

                    _text = (TextInput)inputAdaptor;
                    JS.SetValue("text", _text);
                }

                if (inputAdaptor.GetType().IsAssignableFrom(typeof(ExcelInput)))
                {
                    _excel = (ExcelInput)inputAdaptor;
                    JS.SetValue("excel", _excel);
                }
            }
            return inputAdaptor;
        }
        public ExcelInput excel { get { return _excel; } }
        public TextInput text { get { return _text; } }
        public PdfInput pdf { get { return _pdf; } }
        public void Load(string templateText, string inputText)
        {
            try
            {
                PrepareInput(LoadTemplate(templateText)).Load(inputText);
            }
            catch (Exception ex)
            {
                lastError = ex;
                throw ex;
            }
        }
        public void Load(System.IO.Stream stream, string templateText)
        {
            PrepareInput(LoadTemplate(templateText)).Load(stream);
        }
        public void Run()
        {
            lastError = null;
            hasError = false;
            rootCommand.doPerform();
            if (lastError != null) hasError = true;
        }
        #endregion

        #region "Output"
        private static System.Text.RegularExpressions.Regex expressionsParser = new System.Text.RegularExpressions.Regex("#![\\s\\S]*?#", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        public string parseExpressions(string value)
        {
            System.Text.RegularExpressions.MatchCollection mts = expressionsParser.Matches(value);
            try
            {
                foreach (System.Text.RegularExpressions.Match m in mts)
                {
                    string e = m.Value.Replace("#!", "").Replace("#", "");
                    value = value.Replace(m.Value, JS.Execute(e).GetCompletionValue().ToString());
                }
            }
            catch (Exception e)
            {
                lastError = e;
            }
            return (value);
        }
        public void print(string value)
        {
            output.Append(parseExpressions(value));
        }
        public void printLine(string value)
        {
            output.AppendLine(parseExpressions(value));
        }
        #endregion
    }
}
