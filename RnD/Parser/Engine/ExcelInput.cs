﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;


namespace Parser
{

    /*
* Dependency: Office 2007
* OR
* Install 2007 Office System Driver - Data Connectivity Components from http://www.microsoft.com/downloads/en/details.aspx?FamilyID=7554F536-8C28-4598-9B72-EF94E038C891&displaylang=en
*
* Other Notes:
* 1. There is a 64-bit version too. But I am using the 32-bit one.
* 2. For Office 2010. there is a 'Microsoft Access Database Engine 2010 Redistributable' at http://www.microsoft.com/downloads/en/details.aspx?FamilyID=C06B8369-60DD-4B64-A44B-84B371EDE16D
*/

    public class ExcelInput : InputAdaptor
    {
        public DataTable data;
        public DataSet sheets;
        private int _currentRowIndex, _currentColumnIndex, _currentSheetIndex;
        private DataRow _Row;
        private object _value;


        public string sheetName()
        {
            return data.TableName;
        }

        public string value
        {
            get
            {
                return (_value + "");

            }
        }

        public bool nullValue()
        {

            return _value == System.DBNull.Value;

        }

        public int totalRows()
        {
            return data.Rows.Count;
        }

        public int totalColumns()
        {
            return data.Columns.Count - 3;
        }

        public int currentSheetIndex
        {
            get { return _currentSheetIndex; }
            set
            {
                _currentSheetIndex = value;
                if (_currentSheetIndex > -1 && currentSheetIndex < sheets.Tables.Count)
                {
                    setCurrentSheet(_currentSheetIndex);
                }
            }
        }


        public int currentRowIndex
        {
            get { return _currentRowIndex; }
            set
            {
                _currentRowIndex = value;
                if (_currentRowIndex > -1 && _currentRowIndex < data.Rows.Count)
                {
                    _Row = data.Rows[_currentRowIndex];
                    currentColumnIndex = -1;
                }
            }
        }

        public string text(int row, int col)
        {
            return data.Rows[row][col] + "";
        }


        public int currentColumnIndex
        {
            get { return _currentColumnIndex; }
            set
            {
                _currentColumnIndex = value;
                if (_currentColumnIndex > -1 && _currentColumnIndex < totalColumns())
                {
                    _value = _Row[_currentColumnIndex];
                }

            }
        }

        public DataRow Row
        {
            get { return _Row; }
            set { _Row = value; }
        }


        public bool endOfFile()
        {
            return _currentRowIndex > (data.Rows.Count - 1);
        }

        public bool readColumn()
        {
            if (_currentColumnIndex < totalColumns() - 1)
            {
                currentColumnIndex = _currentColumnIndex + 1;
                return true;
            }
            return false;
        }


        public bool jumpRows(int num)
        {
            for (int i = 0; i < num; i++)
                if (!readRow()) return false;

            return true;
        }

        public bool readRow()
        {
            if (_currentRowIndex < data.Rows.Count - 1)
            {
                currentRowIndex = _currentRowIndex + 1;
                return true;
            }
            return false;
        }


        public bool readSheet()
        {
            if (_currentSheetIndex < sheets.Tables.Count - 1)
            {
                currentSheetIndex = _currentSheetIndex + 1;
                return true;
            }
            return false;
        }

        public void resetSheet()
        {
            currentColumnIndex = -1;

        }

        public void resetRow()
        {
            currentRowIndex = -1;
            currentColumnIndex = -1;
        }

        public void resetColumn()
        {
            currentColumnIndex = -1;
        }


        public int totalSheets()
        {
            return sheets.Tables.Count;
        }


        public void setCurrentSheet(int i)
        {
            data = sheets.Tables[i];
            currentColumnIndex = -1;
            _currentRowIndex = -1;
            _value = System.DBNull.Value;
        }

        public string rowStartText
        {
            get { return _Row["row-start-text"] + ""; }
        }

        public int rowStartColumn
        {
            get { return int.Parse(_Row["row-start-column"] + ""); }
        }

        public int rowValueCount
        {
            get { return int.Parse(_Row["row-value-count"] + ""); }
        }


        public void reshapData()
        {
            foreach (DataTable dt in sheets.Tables)
            {
                dt.Columns.Add("row-start-text");
                dt.Columns.Add("row-start-column");
                dt.Columns.Add("row-value-count");
                dt.Columns[dt.Columns.Count - 1].ColumnMapping = MappingType.Hidden;
                dt.Columns[dt.Columns.Count - 2].ColumnMapping = MappingType.Hidden;
                dt.Columns[dt.Columns.Count - 3].ColumnMapping = MappingType.Hidden;
                foreach (DataRow dr in dt.Rows)
                {
                    dr["row-start-column"] = -1;

                    int rowValueCount = 0;
                    for (int i = 0; i < dt.Columns.Count - 3; i++)
                    {
                        if (dr[i] != System.DBNull.Value)
                        {
                            if (rowValueCount == 0)
                            {
                                dr["row-start-text"] = dr[i];
                                dr["row-start-column"] = i;
                            }

                            rowValueCount++;
                            //break;
                        }
                    }
                    dr["row-value-count"] = rowValueCount;
                    dr.AcceptChanges();

                }
                dt.AcceptChanges();

            }


        }

        public override void Load(string inputText)
        {
            Load(inputText, true);
            reshapData();
        }



        public override void Load(System.IO.Stream stream)
        {
        }

        public bool Load(string fileName, bool trimEmptyRows)
        {
            var connectionString =
                string.Format(
                    "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=No;'",
                    fileName);


            OleDbConnection objConn = null;
            DataTable sheetsName = null;
            try
            {
                objConn = new OleDbConnection(connectionString);
                objConn.Open();
                sheets = new DataSet();
                sheetsName = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                var adp = new OleDbDataAdapter("", objConn);
                if (sheetsName != null)
                    foreach (DataRow row in sheetsName.Rows)
                    {
                        string name = row["TABLE_NAME"].ToString();
                        adp.SelectCommand.CommandText = "SELECT * FROM [" + name + "]";

                        adp.Fill(sheets, name);

                        if (trimEmptyRows)
                        {
                            DataTable dt = sheets.Tables[name];
                            var deletedRows = new List<DataRow>();
                            foreach (DataRow dr in dt.Rows)
                            {
                                var vLength = 0;
                                for (int i = 0; i < dt.Columns.Count; i++)
                                {
                                    string v = dr[i] + "";
                                    vLength = vLength + v.Length;
                                }
                                if (vLength == 0) deletedRows.Add(dr);

                            }

                            foreach (DataRow dr in deletedRows)
                                dt.Rows.Remove(dr);

                            deletedRows.Clear();


                            var deletedColums = new List<DataColumn>();
                            foreach (DataColumn col in dt.Columns)
                            {
                                int vLength = 0;
                                foreach (DataRow dr in dt.Rows)
                                {
                                    string v = dr[col.ColumnName] + "";
                                    vLength = vLength + v.Length;
                                }
                                if (vLength == 0) deletedColums.Add(col);
                            }
                            foreach (DataColumn col in deletedColums)
                                dt.Columns.Remove(col);

                        }
                    }
                currentSheetIndex = -1;
            }
            catch (Exception ex)
            {
                engine.lastError = ex;
                return false;
            }
            finally
            {
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (sheetsName != null) sheetsName.Dispose();
            }
            return true;
        }
    }
}
