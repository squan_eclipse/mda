﻿using System;
using Winnovative;

namespace Parser
{
    public class PdfInput : TextInput
    {
        public static string getPDFText(string fileName)
        {
            var pdf = new PdfToTextConverter
            {
                Layout = TextLayout.OriginalLayout,
                MarkPageBreaks = false,
                AddHtmlMetaTags = false,
                StartPageNumber = 1,
                EndPageNumber = 0,
                LicenseKey = "a0BaS1leS1hZS1hFW0tYWkVaWUVSUlJS"
            };
            try
            {
                return pdf.ConvertToText(fileName);
            }
            catch (Exception e)
            {
                return e.Message;

            }
        }

        public static string getPDFText(System.IO.Stream stream)
        {
            var pdf = new PdfToTextConverter
            {
                Layout = TextLayout.OriginalLayout,
                MarkPageBreaks = false,
                AddHtmlMetaTags = false,
                StartPageNumber = 1,
                EndPageNumber = 0,
                LicenseKey = "a0BaS1leS1hZS1hFW0tYWkVaWUVSUlJS"
            };
            try
            {
                return pdf.ConvertToText(stream);
            }
            catch (Exception e)
            {
                return e.Message;

            }
        }

        public override void Load(System.IO.Stream stream)
        {
            string inputText = "";
            var pdf = new PdfToTextConverter
            {
                Layout = TextLayout.OriginalLayout,
                MarkPageBreaks = false,
                AddHtmlMetaTags = false,
                StartPageNumber = 1,
                EndPageNumber = 0,
                LicenseKey = "a0BaS1leS1hZS1hFW0tYWkVaWUVSUlJS"
            };
            try
            {
                inputText = pdf.ConvertToText(stream);
            }
            catch (Exception e)
            {
                inputText = e.Message;

            }
            base.Load(inputText);
        }
        public override void Load(string inputText)
        {
            var pdf = new PdfToTextConverter
            {
                Layout = TextLayout.OriginalLayout,
                MarkPageBreaks = false,
                AddHtmlMetaTags = false,
                StartPageNumber = 1,
                EndPageNumber = 0,
                LicenseKey = "a0BaS1leS1hZS1hFW0tYWkVaWUVSUlJS"

            };
            try
            {
                inputText = inputText.Replace("&", "&amp;");
                inputText = pdf.ConvertToText(inputText);
            }
            catch (Exception e)
            {
                //Winovative internal bug handling
                if (e.Message != "Illegal characters in path.")
                {
                    throw e;
                }
            }
            base.Load(inputText);
        }
    }
}
