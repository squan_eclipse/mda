﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _ValuesPush :ConditionalCommand
    {
        public string content;

        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            content = node.InnerText;
            
        }
        public override bool perform()
        {
            if(content.Length>0)
                engine.text.pushValue(engine.parseExpressions(content), engine.text.valueDistance);            
            else
                engine.text.pushValue(engine.text.lastTraceValue, engine.text.valueDistance);


            if (hasCondition )
            {
                if (isCondition())
                   return performThen();
                else
                   return performElse();
            }


            return true;
        }
    }

}
