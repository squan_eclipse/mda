﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _Exec : ValueCommand
    {
        public string content;

        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            content = node.InnerText.Replace("]]}>", "]]>");
        }
        public override bool perform()
        {

            try
            {
                if (value.Length > 0)
                {
                    engine.JS.Execute(value);
                }
                else if (content.Length > 0)
                {
                    engine.JS.Execute(content);
                }
            }
            catch (Exception e)
            {
                engine.lastError = e;
                return false;
            }

            return true;
        }
    }
}
