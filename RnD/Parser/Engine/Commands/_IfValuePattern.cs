﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _IfValuePattern : PatternCommand
    {
        bool mode;
        public _IfValuePattern(bool _mode = true)
        {
            mode = _mode;
        }

        public override bool perform()
        {

            if (regex.IsMatch(engine.text.lastTraceValue) == mode)
            {
                engine.text.valueDistance = engine.text.getWordDistance(engine.text.currentWord - 1);
               return  performThen();
            }
            else
            {
                return performElse();
            }


        }

    }
}
