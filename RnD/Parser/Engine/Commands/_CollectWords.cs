﻿namespace Parser.Commands
{
    public class _CollectWords :Command
    {
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            isLoopCommand = true;
        }
        public override bool perform()
        {
            engine.text.skipDoubleSpaces();
            engine.text.lastTraceValue = "";
            int sWord = -1;
            string expression = "!text.isDoubleSpace() && !text.endOfLine()";
            while (engine.JS.Execute(expression).GetCompletionValue().AsBoolean() && !breakLoop)
            {
                if (sWord == -1) {
                    engine.text.valueWordIndex = engine.text.currentWord;
                    sWord = engine.text.currentWord; 
                }
                engine.text.lastTraceValue = engine.text.lastTraceValue + engine.text.words[engine.text.currentWord] + " ";
                engine.text.currentWord++;
            }
            engine.text.lastTraceValue = engine.text.lastTraceValue.Trim();
            if (engine.text.lastTraceValue.Length > 0)
            {
                engine.text.valueDistance = engine.text.getWordDistance(sWord - 1);
                engine.Log.Log("  <vl>" + engine.text.lastTraceValue + "</vl>");
               return performThen();
            }
            else return performElse();
        }
    }
}