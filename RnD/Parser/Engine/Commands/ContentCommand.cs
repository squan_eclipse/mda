﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class ContentCommand : Command
    {

        public string content;

        public override void setNode(System.Xml.XmlNode node,Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            content = node.InnerText.Replace("]]}>","]]>");
        }
    }
}
