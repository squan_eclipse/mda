﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _Print : ContentCommand
    {
        public override bool perform()
        {
            if (hasCondition)
            {
                if (isCondition()) engine.printLine(content); 

            }
            else
            {
                engine.printLine(content);
            }
            
            return true;
        }
    }
}
