﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _Switch : ConditionalCommand
    {
        public string computedValue;
        public bool checkPassed;
    
        public bool check(string value)
        {
            
            if (checkPassed) return false;

            if (value == "")
            {
                checkPassed = true;
                return true;
            }

            if (value == computedValue)
            {
                checkPassed = true;
                return true;
            }
            return false;

        }

        public override bool perform()
        {
            checkPassed = false;
            computedValue = engine.JS.Execute(condition).GetCompletionValue().ToString();
            performThen();
            return true;
           
        }
    }
}
