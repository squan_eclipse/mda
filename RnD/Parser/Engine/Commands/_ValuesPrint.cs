﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _ValuesPrint : ContentCommand
    {
        public override bool perform()
        {

            if (content.Length > 0)
            {
                DataValue dv = engine.text.values.currentLine.currentValue;
                string value = content.Replace("[value]", dv.value);
                value = value.Replace("[distance]", dv.distance.ToString());
                engine.printLine(value);
            }
            else
            {
                engine.printLine(engine.text.values.currentLine.currentValue.value);                
            }

            return true;
        }
    }

}
