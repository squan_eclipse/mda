﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _Script : ContentCommand
    {
        public override bool perform()
        {
            try
            {
                engine.JS.Execute(content);
            }
            catch (Exception e)
            {
                engine.lastError = e;
                return false;
            }
            
            return true;
        }
    }
}
