﻿using System.Collections;
namespace Parser.Commands
{
    public class _ValuesNewLine : Command
    {
        Hashtable tags = new Hashtable();
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            foreach (System.Xml.XmlAttribute xa in node.Attributes)
                tags.Add(xa.Name, xa.Value);
        }
        public override bool perform()
        {
            DataValuesLine dl = engine.text.values.createNewLine();
            foreach (string key in tags.Keys)
                dl.tags.define(key, tags[key].ToString());
            return (true);
        }
    }
}
