﻿namespace Parser.Commands
{
    public class _ValuesResolveOverlapping : Command
    {
        int targetColumns,  distanceThreshold;
        
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            targetColumns = int.Parse(node.Attributes["target-columns"].Value);
            distanceThreshold = int.Parse(node.Attributes["distance-threshold"].Value);

        }
        public override bool perform()
        {

            engine.text.values.resolveOverlapping(targetColumns, distanceThreshold);

            return (true);
        }

    }
}
