﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _ValuesEachValue : Command
    {
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            isLoopCommand = true;
        }
        public override bool perform()
        {

            foreach (DataValue dv in engine.text.values.currentLine.values)
            {
                engine.text.values.currentLine.currentValue = dv;
                if (!performThen()) return false;
            }           

            return (true);
        }

    }
}
