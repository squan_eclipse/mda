﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class ValueCommand : Command
    {

        public string value;

        public override void setNode(System.Xml.XmlNode node,Engine e, Command parent)
        {
            base.setNode(node, e, parent);

            value =Utils.getAttributeValue( node,"value");
        }
    }
}
