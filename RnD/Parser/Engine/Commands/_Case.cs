﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _Case : ValueCommand
    {
        public override bool perform()
        {
            if (value.Length > 0)
            {
                if (getParent().asSwitch().check(value))
                    return performThen();

            }
            else if (!getParent().asSwitch().checkPassed)
                return performThen();
            
           return true;
        }
    }
}
