﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class PatternCommand : Command
    {

        public string pattern;
        public Regex regex;

        public override void setNode(System.Xml.XmlNode node,Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            pattern = node.Attributes["pattern"].Value;

            regex = new Regex(pattern);
        }
    }
}
