﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _PatternWord : PatternCommand
    {
        public override bool perform()
        {


            if (flags["SKIP-DOUBLE-SPACES"]) engine.text.skipDoubleSpaces();
            engine.text.valueWordIndex = engine.text.currentWord;
            if (regex.IsMatch(engine.text.words[engine.text.currentWord]))
            {
                engine.text.valueDistance = engine.text.getWordDistance(engine.text.currentWord - 1);
                engine.text.lastTraceValue = engine.text.words[engine.text.currentWord];
                // parser.Log("  <dist>" + parser.getWordDistance() + "</dist>");
                engine.Log.Log("  <vl>" + engine.text.lastTraceValue + "</vl>");
                engine.text.currentWord++;
              return   performThen();
 

            }
            else
            {
                if (flags["SKIP-WORD-ON-FAIL"]) engine.text.currentWord++;
               return  performElse();
            }
        }

    }
}
