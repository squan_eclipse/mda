﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _ValuesEachLine : Command
    {
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            isLoopCommand = true;
        }
        public override bool perform()
        {

            foreach (DataValuesLine dl in engine.text.values.lines)
            {
                engine.text.values.currentLine = dl;
                if (dl.valuesCount > 0) dl.currentValue = dl[0];
                if (!performThen()) return false;
            }           

            return (true);
        }

    }
}
