﻿namespace Parser.Commands
{
    public class _ValuesResolvePadding : Command
    {
        int targetColumns, minColumns, distanceThreshold;
        string paddingText;
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            targetColumns = int.Parse(node.Attributes["target-columns"].Value);
            minColumns = int.Parse(node.Attributes["min-columns"].Value);
            distanceThreshold = int.Parse(node.Attributes["distance-threshold"].Value);
            paddingText = Utils.getAttributeValue(node, "padding-text");
        }
        public override bool perform()
        {
            engine.text.values.resolvePadding(targetColumns, minColumns, distanceThreshold, paddingText);
            return (true);
        }
    }
}
