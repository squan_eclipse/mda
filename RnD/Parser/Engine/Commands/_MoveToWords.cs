﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _MoveToWords : ValueCommand
    {
        public override bool perform()
        {

            engine.text.lastTraceValue = value;

            int wordIndex = engine.text.wordsData.IndexOf(engine.text.lastTraceValue, engine.text.wordDataIndex);

            if (wordIndex > -1)
            {
                engine.text.wordDataIndex = wordIndex + 1;
                if (engine.text.wordsPos.ContainsKey(wordIndex))
                {
                    engine.text.currentWord = (int)engine.text.wordsPos[wordIndex];
                    engine.text.valueWordIndex = engine.text.currentWord;
                    if (flags["SKIP-VALUE"])
                        engine.text.currentWord = engine.text.currentWord + ((engine.text.lastTraceValue.Replace(" " + engine.text.doubleSpace + " ", "").Split(' ').Length));


                   return performThen();

                }
                else
                {
                    return performElse();
                }
            }

            return true;
        }
    }
}
