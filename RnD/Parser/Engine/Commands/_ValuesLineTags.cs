﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _ValuesLineTags : Command
    {

        Hashtable tags = new Hashtable();
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
           
            base.setNode(node, e, parent);
            foreach (System.Xml.XmlAttribute xa in node.Attributes)
                tags.Add(xa.Name, xa.Value);


        }
        public override bool perform()
        {
            DataValuesLine dl = engine.text.values.line;
            foreach (string key in tags.Keys)
                dl.tags.define(key, tags[key].ToString());
                     

            return (true);
        }

    }
}
