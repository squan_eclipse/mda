﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _While : ConditionalCommand
    {
        public int counter;
        public int getCounter() { return counter; }
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            isLoopCommand = true;
        }
        public override bool perform()
        {
            counter = 0;
            
            while (isCondition() && !breakLoop)
            {
               if(!performThen()) return false;
                counter++;
            }

            return (true);
        }

    }
}
