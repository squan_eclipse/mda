﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Parser;
namespace Parser.Commands
{
    public class _CollectWordsUntill : ConditionalCommand
    {
        public override void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            base.setNode(node, e, parent);
            isLoopCommand = true;
        }
        public override bool perform()
        {
            if (flags["SKIP-DOUBLE-SPACES"]) engine.text.skipDoubleSpaces();
            engine.text.lastTraceValue = "";
            int sWord = -1;
            while (isCondition() && !breakLoop)
            {

                if (sWord == -1){
                    engine.text.valueWordIndex = engine.text.currentWord;
                    sWord = engine.text.currentWord;
                }
                engine.text.lastTraceValue = engine.text.lastTraceValue + engine.text.words[engine.text.currentWord] + " ";
                engine.text.currentWord++;
            }

            engine.text.lastTraceValue = engine.text.lastTraceValue.Trim();
            if (engine.text.lastTraceValue.Length > 0)
            {
                engine.text.valueDistance = engine.text.getWordDistance(sWord - 1);
                engine.Log.Log("  <vl>" + engine.text.lastTraceValue + "</vl>");
                return performThen();
            }
            else return performElse();


        }

    }
}
