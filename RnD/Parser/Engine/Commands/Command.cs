﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parser.Commands
{
    public class Command
    {
        public List<Command> childs;

        public bool breakLoop;
        Command thenChilds, elseChilds, parent;
        public FlagesCollection flags = new FlagesCollection();
        public Engine engine;
        public System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();
        public string name;

       

        public bool isLoopCommand;
        public string condition;

        public bool hasCondition;
        public bool isCondition() { return engine.JS.Execute(condition).GetCompletionValue().AsBoolean(); }
       
        public Command searchLoopCommand()
        {
            if (isLoopCommand) return this;
            else if (parent != null) return parent.searchLoopCommand();

            return null;
        }

        public Command searchConditionalCommand()
        {
            if (this.GetType().IsAssignableFrom(typeof(ConditionalCommand))) return this;

            else if (parent != null) return parent.searchConditionalCommand();

            return null;
        }


        public Command getParent() { return parent; }
        public bool hasChilds()
        {
            return childs == null ? false : childs.Count > 0;
        }

        public Command addChild(string name)
        {
            if (childs == null) childs = new List<Command>();
            Command node = null;
          

            string cName = GetType().Namespace + "." + "_" + Utils.titleCase(name.Replace("-", " ")).Replace(" ", "");
            node =(Command)System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(cName,true);

            if (node == null) node = new Command();
            childs.Add(node);
            return (node);
        }
        public virtual void setNode(System.Xml.XmlNode node, Engine e, Command parent)
        {
            thenChilds = this;
            // elseChilds = this;
            engine = e;
            name = node.Name.ToLower();
            this.parent = parent;

            condition = Utils.getAttributeValue(node, "condition");
            hasCondition = condition != "";
            string _flags =Utils.getAttributeValue(node, "flags");
            if (_flags.Length > 0)
            {
                foreach (string f in _flags.Split('|'))
                {
                    if (f.IndexOf(":") > 0)
                    {
                        string[] ff = f.Split(':');
                        flags.Add(ff[0], ff[1]);
                    }
                    else
                        flags.Add(f, "true");
                }
            }
            if (name == "then")
                parent.thenChilds = this;
            else if (name == "else")
                parent.elseChilds = this;

            foreach (System.Xml.XmlNode ch in node.ChildNodes)
            {
                if (ch.NodeType == System.Xml.XmlNodeType.Element) addChild(ch.Name.ToLower()).setNode(ch, e, this);
            }

        }

        public _While asWhile() { return (_While)this; }
        public _If asIf() { return (_If)this; }
        public _CollectWordsUntill asCollectWordsUntill() { return (_CollectWordsUntill)this; }
        public _PatternWord asPatternWord() { return (_PatternWord)this; }
        public _MoveToWords asMoveToWords() { return (_MoveToWords)this; }

        public _Switch asSwitch() { return (_Switch)this; }

        public void breakTheLoop()
        {
            Command cmd = searchLoopCommand();
            breakLoop = true;
            if (cmd != null) cmd.breakLoop = true; 
        }

        public void breakTheCondition()
        {
            Command cmd = searchConditionalCommand();
            breakLoop = true;
            if (cmd != null) cmd.breakLoop = true; 
        }
        public void breakTheCommand()
        {
            Command cmd =getParent();
            breakLoop = true;
            if (cmd != null) cmd.breakLoop = true;
        }

        public virtual bool perform()
        {
            switch (name)
            {               
                case "break-loop":
                    breakTheLoop();
                    break;

                case "break-condition":
                    breakTheCondition();
                    break;

                case "break":
                    breakTheCommand();
                    break;

                default:
                    if (!engine.inputAdaptor.Command(name)) return false;
                    break;
            }
            performChilds();
            return (true);
        }

        public void breakTheLoop(Command cmd)
        {
            breakLoop = true;
            if (cmd.hasChilds())
            {
                foreach (Command ch in cmd.childs)
                    ch.breakTheLoop(ch);
            }
            
        }
        public bool doPerform()
        {
            breakLoop = false;
            engine.currentCommand = this;
            engine.Log.pushLogIndent();
            engine.Log.Log("<" + name + ">");
                   watch.Start();
            Log();
            bool res = true;
            res = perform(); 
            
            
            //try {res= perform(); } catch(Exception e){ engine.lastError=e;  res=false;  }
            
            
            watch.Stop();
            engine.Log.Log("  <tm>" + watch.ElapsedMilliseconds + "</tm>");
            engine.Log.Log("</" + name + ">");

            engine.Log.popLogIndent();
            return (res);
        }

        public virtual void Log()
        {

        }
        public Command getThen()
        {
            if (this.thenChilds != null) return (this.thenChilds);
            return (this);
        }

        public bool performElse()
        {
            if (this.elseChilds != null)  return this.elseChilds.performChilds();

            return true;

        }

        public bool performThen()
        {
            return thenChilds.performChilds();

        }

        public bool performChilds()
        {
            if (hasChilds())
            {
                foreach (Command ch in childs)
                {
                    if (!ch.doPerform()) return false;
                    if (breakLoop || ch.breakLoop) break;
                }
                    

            }
            return true;

        }
    }
}
