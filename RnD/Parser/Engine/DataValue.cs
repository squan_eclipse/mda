﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Parser
{
    public class DataValue
    {
        private TagsCollection _tags = new TagsCollection();

        public TagsCollection tags { get { return _tags; } }
        private string _value;
        private int _distance, _index, _startingWordIndex, _endingWordIndex;
        private DataValue _prev, _next, _top, _bottom;
        private DataValuesLine _line;

        private bool _padded;

        public bool padded { get { return _padded; } set { _padded = value; } }

        public int index
        {
            get { return _index; }
            set { _index = value; }
        }
        public int distance
        {
            get { return _distance; }
            set { _distance = value; }
        }

        public override string ToString()
        {
            return _value;
        }

        public string value
        {
            get { return _value; }
            set { _value = value; }
        }

        public DataValuesLine line
        {
            get { return _line; }
            set { _line = value; }
        }
        public DataValue prev
        {
            get { return _prev; }
            set { _prev = value; }
        }

        public DataValue next
        {
            get { return _next; }
            set { _next = value; }
        }

        public DataValue top
        {
            get { return _top; }
            set { _top = value; }
        }

        public DataValue bottom
        {
            get { return _bottom; }
            set { _bottom = value; }
        }


        public bool hasNext
        {
            get { return _next != null; }
        }

        public bool hasPrev
        {
            get { return _prev != null; }
        }

        public bool hasTop
        {
            get { return _top != null; }
        }

        public bool hasBottom
        {
            get { return _bottom != null; }
        }
        public int startingWordIndex
        {
            get { return _startingWordIndex; }
            set { _startingWordIndex = value; }
        }

        public int endingWordIndex
        {
            get { return _endingWordIndex; }
            set { _endingWordIndex = value; }
        }

        public int distanceDifference(DataValue input)
        {
            return Math.Abs(input.distance - distance);

        }
    }

    public class DataValuesLine
    {
        private TagsCollection _tags = new TagsCollection();
        public TagsCollection tags { get { return _tags; } }

        public List<DataValue> values = new List<DataValue>();
        public int _index, _firstDistnace;
        private DataValuesLine _prev, _next;


        private DataValue _currentValue;
        public int index
        {
            get { return _index; }
            set { _index = value; }
        }

        public int firstDistnace
        {
            get { return _firstDistnace; }
            set { _firstDistnace = value; }
        }

        public string get(string key)
        {
            return (tags.get(key));
        }

        public DataValue currentValue
        {
            get { return _currentValue; }
            set { _currentValue = value; }
        }

        public bool hasNext
        {
            get { return _next != null; }
        }

        public bool hasPrev
        {
            get { return _prev != null; }
        }

        public DataValuesLine prev
        {
            get { return _prev; }
            set { _prev = value; }
        }

        public DataValuesLine next
        {
            get { return _next; }
            set { _next = value; }
        }

        public bool hasValue(string value)
        {
            value = value.ToLower().Trim();
            foreach (DataValue val in values)
                if (val.value.ToLower().Trim() == value) return true;

            return false;

        }
        public int valuesCount { get { return values.Count; } }
        public DataValuesLine(int l)
        {
            _index = l;
        }

        private DataValue createValue(string value, int distance)
        {
            DataValue dv = new DataValue
            {
                line = this,
                value = value,
                distance = distance
            };
            dv.index = values.Count;

            if (dv.index > 0)
            {
                dv.prev = this[dv.index - 1];
                dv.prev.next = dv;

            }
            if (hasPrev)
            {
                if (_prev.values.Count > dv.index)
                {
                    dv.top = prev[dv.index];
                    dv.top.bottom = dv;
                }
            }

            _currentValue = dv;
            return dv;

        }
        public DataValue pushValue(string value, int distance)
        {
            DataValue dv = createValue(value, distance);
            if (values.Count == 0) _firstDistnace = distance;
            values.Add(dv);
            return dv;

        }

        public DataValue insertValue(int index, string value, int distance)
        {
            DataValue dv = createValue(value, distance);
            if (values.Count == 0) _firstDistnace = distance;
            values.Insert(index, dv);
            return dv;
        }

        public DataValue this[int index]
        {
            get
            {
                return values[index];
            }
            set
            {

                values[index] = value;
            }
        }

        public bool _hasPaddings;

        public bool hasPaddings { get { return _hasPaddings; } }


    }

    public class DataValuesLines
    {
        public List<DataValuesLine> lines = new List<DataValuesLine>();
        private DataValuesLine _currentLine;
        private int _index;
        public Engine engine;
        public DataValuesLine getCurrentLine() { return _currentLine; }


        public DataValuesLine line
        {
            get { return _currentLine; }
        }
        public DataValue current
        {
            get { return _currentLine.currentValue; }
        }

        public string value
        {
            get { return _currentLine.currentValue.value; }
        }


        public DataValuesLine currentLine
        {
            get { return _currentLine; }
            set { _currentLine = value; }
        }

        public int index
        {
            get { return _index; }
            set { _index = value; }
        }
        public DataValuesLine createNewLine()
        {
            _currentLine = new DataValuesLine(lines.Count);
            if (_currentLine.index > 0)
            {
                _currentLine.prev = this[_currentLine.index - 1];
                _currentLine.prev.next = _currentLine;
            }
            lines.Add(currentLine);
            return _currentLine;
        }
        public DataValuesLine this[int index]
        {
            get
            {
                return lines[index];
            }
            set
            {

                lines[index] = value;
            }
        }

        public void balanceValues(int distanceThreshold, string paddingText)
        {
            List<int> distances = new List<int>();
            foreach (DataValuesLine ln in lines)
            {
                foreach (DataValue dv in ln.values)
                {
                    int diff = 0;
                    foreach (int d in distances)
                    {
                        diff = Math.Abs(d - dv.distance);
                        if (diff < distanceThreshold)
                        {
                            diff = -1;
                            break;
                        }
                    }
                    if (diff > -1)
                        distances.Add(dv.distance);
                }
            }
            distances = distances.OrderBy(o => o).ToList<int>();

            for (int i = 0; i < distances.Count; i++)
            {
                int d = distances[i];
                foreach (DataValuesLine ln in lines)
                {
                    if (ln.valuesCount != distances.Count)
                    {
                        if (ln.valuesCount > i)
                        {
                            int rDist = d - ln[i].distance;
                            if (Math.Abs(rDist) > distanceThreshold)
                            {
                                if (rDist < 0)
                                    ln.insertValue(i, paddingText, d);
                            }
                        }
                        else
                            ln.pushValue(paddingText, d);
                    }
                }
            }
        }

        public void resolveOverlapping(int targetColumns, int distanceThreshold = 3)
        {

            List<DataValuesLine> targets = new List<DataValuesLine>();
            foreach (DataValuesLine dl in lines)
                if (dl.valuesCount == targetColumns)
                    targets.Add(dl);


            for (int t = 0; t < targets.Count; t++)
            {
                for (int v = 0; v < targets[t].valuesCount; v++)
                {
                    if (v > 0)
                    {
                        if (targets[t][v].padded)
                        {
                            if (!targets[t][v - 1].padded)
                            {
                                DataValue iv = targets[t][v];
                                DataValue ov = targets[t][v - 1];

                                if (ov.endingWordIndex - ov.startingWordIndex > 1)
                                {
                                    iv.value = "";
                                    for (int w = ov.startingWordIndex; w <= ov.endingWordIndex; w++)
                                    {
                                        string ww = engine.text.words[w];
                                        int wd = engine.text.getWordDistance(w - 1);
                                        if (Math.Abs(iv.distance - wd) <= distanceThreshold || wd > iv.distance)
                                        {
                                            iv.value = iv.value + engine.text.words[w] + " ";
                                        }
                                    }
                                }

                                if (iv.value.Length > 0)
                                {
                                    iv.value = iv.value.Trim();
                                    ov.value = ov.value.Replace(iv.value, "");
                                    ov.value = ov.value.Trim();
                                }

                            }

                        }
                    }
                }
            }



        }
        public void resolvePadding(int targetColumns, int minColumns, int distanceThreshold, string paddingText = "0")
        {
            DataValuesLine idealLine = null;
            int[] dist = new int[targetColumns];
            int[] Idealdist = new int[targetColumns];

            foreach (DataValuesLine dl in lines)
            {
                if (dl.valuesCount == targetColumns)
                {
                    idealLine = dl;
                    break;
                }
            }

            try
            {
                //SortedList<int, int> sl = new SortedList<int, int>();
                //int index = 0;
                //foreach (DataValuesLine dl in lines)
                //{
                //    sl.Add(index, dl.valuesCount);
                //    index++;
                //}               
                
                foreach (DataValuesLine dl in lines)
                {
                    if (dl.valuesCount == targetColumns)
                        idealLine = dl;

                    if (idealLine !=null && dl.valuesCount >= minColumns && dl.valuesCount < targetColumns)
                    {
                        for (int i = 0; i < idealLine.values.Count; i++)
                            dist[i] = idealLine[i].distance;

                        bool workDone = true;

                        while (dl.valuesCount < targetColumns && workDone)
                        {
                            workDone = false;
                            for (int i = 0; i < dl.valuesCount; i++)
                            {

                                if (dist[i] > -1)
                                {
                                    int rdist = dist[i] - dl[i].distance;

                                    if (Math.Abs(rdist) > distanceThreshold)
                                    {
                                        if (rdist > 0)
                                        {
                                            workDone = true;
                                            dl._hasPaddings = true;
                                            DataValue dv = dl.insertValue(dl[i].index + 1, paddingText, dist[i]);
                                            dv.padded = true;
                                            dv.startingWordIndex = idealLine[i].startingWordIndex;
                                            dv.endingWordIndex = idealLine[i].endingWordIndex;
                                            for (int d = i; d > -1; d--) dist[d] = -1;
                                            break;
                                        }
                                        else if (rdist < 0)
                                        {
                                            workDone = true;
                                            dl._hasPaddings = true;
                                            DataValue dv = dl.insertValue(i, paddingText, dist[i]);
                                            dv.padded = true;
                                            dv.startingWordIndex = idealLine[i].startingWordIndex;
                                            dv.endingWordIndex = idealLine[i].endingWordIndex;
                                            for (int d = i; d > -1; d--) dist[d] = -1;
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
            catch (Exception ec)
            {
                engine.Log.Log("<error>" + ec.Message + "</error>");
            }
        }
    }
}
