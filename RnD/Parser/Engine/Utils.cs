﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;

namespace Parser
{
    public class Utils
    {
        public static string titleCase(string text)
        {
            return (System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(text));
        }
        public static string getAttributeValue(XmlNode node, string att)
        {
            XmlNode attribute = node.Attributes[att];
            if (attribute != null)
                return (attribute.Value);
            else
                return string.Empty;
        }
        public static string getNodeValue(System.Xml.XmlNode node, string path)
        {

            System.Xml.XmlNode nd = node.SelectSingleNode(path);
            if (nd != null)
            {
                return nd.InnerText;
            }
            return "";
        }

        public static string[] splitString(string text, string delimeter)
        {
            string[] temp = text.Split(delimeter.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            StringCollection st = new StringCollection();

            for (int i = 0; i < temp.Length; i++)
            {
                temp[i] = temp[i].Trim();
                if (temp[i].Length > 0) st.Add(temp[i]);
            }
            temp = new string[st.Count];
            st.CopyTo(temp, 0);
            return (temp);


        }

        public static void saveTextFile(string fileName, string text)
        {
            System.IO.StreamWriter sw = System.IO.File.CreateText(fileName);
            sw.Write(text);
            sw.Flush();
            sw.Close();
            sw.Dispose();
        }

    }
    public class FlagesCollection : Hashtable
    {
        public bool this[string key]
        {
            get
            {
                return ContainsKey(key);
            }
            set
            {

                base[key] = value;
            }
        }
    }


    public class TagsCollection 
    {
        Hashtable values = new Hashtable();

        public void define(string k, string v)
        {
            if (values.ContainsKey(k))
                values[k] = v;
            else
                values.Add(k, v);
        }
        public bool defined(string k)
        {
            return (values.ContainsKey(k));
        }
        public string get(string key)
        {
            return (values[key].ToString());
        }
        public string this[string key]
        {
            get
            {
                return values[key].ToString();
            }
            set
            {

                values[key] = value;
            }
        }
    }


    public class Logger
    {
        public System.Text.StringBuilder log = new StringBuilder();
      
        int logIndent = 0;
        string logSpaces = "                                                                                                                                                                ";
        bool logging=false;
        public void pushLogIndent()
        {
            logIndent++;


        }

        public void popLogIndent()
        {
            logIndent--;
        }
        public void Log(string txt)
        {
            if (logging) log.AppendLine(logSpaces.Substring(0, logIndent * 2) + txt);
        }

    }
}
