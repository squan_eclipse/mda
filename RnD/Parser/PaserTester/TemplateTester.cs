﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data;

namespace PaserTester
{
    public class TemplateTester : Form
    {
        public TemplateTester()
        {
            InitializeComponent();
        }
           private OpenFileDialog ofd;
        private SaveFileDialog sfd;
        private SplitContainer splitContainer1;
        private WebBrowser output;
        private TabControl tb;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private RichTextBox rtx;
        private OpenFileDialog ofdPDF;
        private TabPage tabPage3;
        private RichTextBox rtx2;
        private Label lblPDFFile;
        private Button btnApplyTemplate;
        private Button btnOpenPDF;
        private Button btnSaveAsTemplate;
        private Button btnSaveTemplate;
        private Button btnLoadTemplate;
        private RichTextBox ed;
        private Button btnReloadAndApply;
        private Button btnOpenExcel;
        private TabPage tabExcelData;
        private Panel panel1;

        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnOpenExcel = new System.Windows.Forms.Button();
            this.btnReloadAndApply = new System.Windows.Forms.Button();
            this.lblPDFFile = new System.Windows.Forms.Label();
            this.btnApplyTemplate = new System.Windows.Forms.Button();
            this.btnOpenPDF = new System.Windows.Forms.Button();
            this.btnSaveAsTemplate = new System.Windows.Forms.Button();
            this.btnSaveTemplate = new System.Windows.Forms.Button();
            this.btnLoadTemplate = new System.Windows.Forms.Button();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ed = new System.Windows.Forms.RichTextBox();
            this.tb = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.output = new System.Windows.Forms.WebBrowser();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.rtx = new System.Windows.Forms.RichTextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.rtx2 = new System.Windows.Forms.RichTextBox();
            this.tabExcelData = new System.Windows.Forms.TabPage();
            this.ofdPDF = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tb.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnOpenExcel);
            this.panel1.Controls.Add(this.btnReloadAndApply);
            this.panel1.Controls.Add(this.lblPDFFile);
            this.panel1.Controls.Add(this.btnApplyTemplate);
            this.panel1.Controls.Add(this.btnOpenPDF);
            this.panel1.Controls.Add(this.btnSaveAsTemplate);
            this.panel1.Controls.Add(this.btnSaveTemplate);
            this.panel1.Controls.Add(this.btnLoadTemplate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(947, 69);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnOpenExcel
            // 
            this.btnOpenExcel.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenExcel.Location = new System.Drawing.Point(681, 7);
            this.btnOpenExcel.Name = "btnOpenExcel";
            this.btnOpenExcel.Size = new System.Drawing.Size(74, 23);
            this.btnOpenExcel.TabIndex = 8;
            this.btnOpenExcel.Text = "Open Excel";
            this.btnOpenExcel.UseVisualStyleBackColor = true;
            this.btnOpenExcel.Click += new System.EventHandler(this.btnOpenExcel_Click);
            // 
            // btnReloadAndApply
            // 
            this.btnReloadAndApply.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReloadAndApply.Location = new System.Drawing.Point(423, 7);
            this.btnReloadAndApply.Name = "btnReloadAndApply";
            this.btnReloadAndApply.Size = new System.Drawing.Size(172, 23);
            this.btnReloadAndApply.TabIndex = 6;
            this.btnReloadAndApply.Text = "Reload And Apply Template";
            this.btnReloadAndApply.UseVisualStyleBackColor = true;
            this.btnReloadAndApply.Click += new System.EventHandler(this.btnReloadAndApply_Click);
            // 
            // lblPDFFile
            // 
            this.lblPDFFile.AutoSize = true;
            this.lblPDFFile.Location = new System.Drawing.Point(11, 37);
            this.lblPDFFile.Name = "lblPDFFile";
            this.lblPDFFile.Size = new System.Drawing.Size(0, 13);
            this.lblPDFFile.TabIndex = 5;
            // 
            // btnApplyTemplate
            // 
            this.btnApplyTemplate.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApplyTemplate.Location = new System.Drawing.Point(309, 7);
            this.btnApplyTemplate.Name = "btnApplyTemplate";
            this.btnApplyTemplate.Size = new System.Drawing.Size(97, 23);
            this.btnApplyTemplate.TabIndex = 4;
            this.btnApplyTemplate.Text = "Apply Template";
            this.btnApplyTemplate.UseVisualStyleBackColor = true;
            this.btnApplyTemplate.Click += new System.EventHandler(this.btnApplyTemplate_Click);
            // 
            // btnOpenPDF
            // 
            this.btnOpenPDF.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenPDF.Location = new System.Drawing.Point(601, 7);
            this.btnOpenPDF.Name = "btnOpenPDF";
            this.btnOpenPDF.Size = new System.Drawing.Size(74, 23);
            this.btnOpenPDF.TabIndex = 3;
            this.btnOpenPDF.Text = "Open PDF";
            this.btnOpenPDF.UseVisualStyleBackColor = true;
            this.btnOpenPDF.Click += new System.EventHandler(this.btnOpenPDF_Click);
            // 
            // btnSaveAsTemplate
            // 
            this.btnSaveAsTemplate.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveAsTemplate.Location = new System.Drawing.Point(197, 7);
            this.btnSaveAsTemplate.Name = "btnSaveAsTemplate";
            this.btnSaveAsTemplate.Size = new System.Drawing.Size(104, 23);
            this.btnSaveAsTemplate.TabIndex = 2;
            this.btnSaveAsTemplate.Text = "Save As Template";
            this.btnSaveAsTemplate.UseVisualStyleBackColor = true;
            this.btnSaveAsTemplate.Click += new System.EventHandler(this.btnSaveAsTemplate_Click);
            // 
            // btnSaveTemplate
            // 
            this.btnSaveTemplate.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveTemplate.Location = new System.Drawing.Point(102, 7);
            this.btnSaveTemplate.Name = "btnSaveTemplate";
            this.btnSaveTemplate.Size = new System.Drawing.Size(89, 23);
            this.btnSaveTemplate.TabIndex = 1;
            this.btnSaveTemplate.Text = "Save Template";
            this.btnSaveTemplate.UseVisualStyleBackColor = true;
            this.btnSaveTemplate.Click += new System.EventHandler(this.btnSaveTemplate_Click);
            // 
            // btnLoadTemplate
            // 
            this.btnLoadTemplate.Font = new System.Drawing.Font("Trebuchet MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadTemplate.Location = new System.Drawing.Point(7, 7);
            this.btnLoadTemplate.Name = "btnLoadTemplate";
            this.btnLoadTemplate.Size = new System.Drawing.Size(89, 23);
            this.btnLoadTemplate.TabIndex = 0;
            this.btnLoadTemplate.Text = "Load Template";
            this.btnLoadTemplate.UseVisualStyleBackColor = true;
            this.btnLoadTemplate.Click += new System.EventHandler(this.btnLoadTemplate_Click);
            // 
            // ofd
            // 
            this.ofd.Filter = "Template Files|*.xml";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 69);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ed);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tb);
            this.splitContainer1.Size = new System.Drawing.Size(947, 335);
            this.splitContainer1.SplitterDistance = 165;
            this.splitContainer1.TabIndex = 3;
            // 
            // ed
            // 
            this.ed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ed.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ed.Location = new System.Drawing.Point(0, 0);
            this.ed.Name = "ed";
            this.ed.Size = new System.Drawing.Size(947, 165);
            this.ed.TabIndex = 3;
            this.ed.Text = "";
            this.ed.WordWrap = false;
            // 
            // tb
            // 
            this.tb.Controls.Add(this.tabPage1);
            this.tb.Controls.Add(this.tabPage2);
            this.tb.Controls.Add(this.tabPage3);
            this.tb.Controls.Add(this.tabExcelData);
            this.tb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb.Location = new System.Drawing.Point(0, 0);
            this.tb.Name = "tb";
            this.tb.SelectedIndex = 0;
            this.tb.Size = new System.Drawing.Size(947, 166);
            this.tb.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.output);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(939, 140);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Output";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // output
            // 
            this.output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.output.Location = new System.Drawing.Point(3, 3);
            this.output.MinimumSize = new System.Drawing.Size(20, 20);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(933, 134);
            this.output.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.rtx);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(939, 140);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Text";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // rtx
            // 
            this.rtx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtx.Font = new System.Drawing.Font("Lucida Console", 8.25F);
            this.rtx.Location = new System.Drawing.Point(3, 3);
            this.rtx.Name = "rtx";
            this.rtx.Size = new System.Drawing.Size(933, 134);
            this.rtx.TabIndex = 0;
            this.rtx.Text = "";
            this.rtx.TextChanged += new System.EventHandler(this.rtx_TextChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.rtx2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(939, 140);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Trim Text";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // rtx2
            // 
            this.rtx2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtx2.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtx2.Location = new System.Drawing.Point(0, 0);
            this.rtx2.Name = "rtx2";
            this.rtx2.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.rtx2.Size = new System.Drawing.Size(939, 140);
            this.rtx2.TabIndex = 1;
            this.rtx2.Text = "";
            // 
            // tabExcelData
            // 
            this.tabExcelData.Location = new System.Drawing.Point(4, 22);
            this.tabExcelData.Name = "tabExcelData";
            this.tabExcelData.Size = new System.Drawing.Size(939, 140);
            this.tabExcelData.TabIndex = 4;
            this.tabExcelData.Text = "Excel Data";
            this.tabExcelData.UseVisualStyleBackColor = true;
            // 
            // ofdPDF
            // 
            this.ofdPDF.Filter = "PDF Files|*.pdf";
            // 
            // TemplateTester
            // 
            this.ClientSize = new System.Drawing.Size(947, 404);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Name = "TemplateTester";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tb.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        void ed_KeyUp(object sender, KeyEventArgs e)
        {
            return;
            string tokens = @"(<while|</while)";
            Regex rex = new Regex(tokens);
            MatchCollection mc = rex.Matches(ed.Text);
            int StartCursorPosition = ed.SelectionStart;
            ed.SuspendLayout();
            foreach (Match m in mc)
            {
                int startIndex = m.Index;
                int StopIndex = m.Length;
                ed.Select(startIndex, StopIndex);
                ed.SelectionColor = System.Drawing.Color.Blue;
                ed.SelectionStart = StartCursorPosition;
                ed.SelectionColor = System.Drawing.Color.Black;
            }
        }
        public void saveTemplate(string txt)
        {
            if (templateFileName == null)
            {
                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    templateFileName = sfd.FileName;
                }
            }
            if (templateFileName != null)
            {
                
                Parser.Utils.saveTextFile(templateFileName, txt);
                MessageBox.Show("Template Saved");
                this.Text = templateFileName;
            }
        }
        public void saveAsTemplate(string txt)
        {
            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                templateFileName = sfd.FileName;
                Parser.Utils.saveTextFile(templateFileName, txt);
                MessageBox.Show("Template Saved");
                this.Text = templateFileName;
            }                        
        }
        string templateFileName;
        public string loadTemplate()
        {
            ofd.FileName = "";
            ofd.Filter = "Template Files|*.xml";
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                templateFileName = ofd.FileName;
                this.Text = templateFileName;
                return (System.IO.File.ReadAllText(ofd.FileName));
            }
            return "";
        }
        public string reloadTemplate()
        {
                return (System.IO.File.ReadAllText(templateFileName));
        }
        string PDFText;
        public string openPDF()
        {
            ofdPDF.Filter = "PDF Files|*.pdf";
            ofdPDF.FileName = "";
            if (ofdPDF.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PDFText = Parser.PdfInput.getPDFText(ofdPDF.FileName);
               
               rtx.Text  = PDFText;
               rtx2.Text = Parser.TextInput.getTrimLineText(PDFText);
               tb.SelectedIndex = 1;
               return (ofdPDF.FileName);
            }
            return "";
        }

        public void applyTemplate(string txt)
        {
            Parser.Engine en = new Parser.Engine();
            en.Load(txt,rtx.Text);
            en.Run();
            if (!en.hasError)
            {
                if (en.outputType == "xml")
                {
                    Parser.Utils.saveTextFile(Application.StartupPath + "\\output.xml", en.output.ToString());
                    output.Navigate("file:///" + Application.StartupPath + "\\output.xml");
                }
                else
                {
                    Parser.Utils.saveTextFile(Application.StartupPath + "\\output.html", en.output.ToString());
                    output.Navigate("file:///" + Application.StartupPath + "\\output.html");
                }
                
                tb.SelectedIndex = 0;
            }
            else
            {
                MessageBox.Show(en.lastError.Message, "Error in template");
            }
        }
        
        private void btnLoadTemplate_Click(object sender, EventArgs e)
        {
           ed.Text= loadTemplate();
        }
        private void btnSaveTemplate_Click(object sender, EventArgs e)
        {
            saveTemplate(ed.Text);
        }
        private void btnSaveAsTemplate_Click(object sender, EventArgs e)
        {
            saveAsTemplate(ed.Text);
        }
        private void btnOpenPDF_Click(object sender, EventArgs e)
        {
            lblPDFFile.Text= openPDF();
        }
        private void btnApplyTemplate_Click(object sender, EventArgs e)
        {
            applyTemplate(ed.Text); 
        }
        private void btnReloadAndApply_Click(object sender, EventArgs e)
        {
            ed.Text = reloadTemplate();
            applyTemplate(ed.Text); 
        }
        private void rtx_TextChanged(object sender, EventArgs e)
        {
            //csvLoaded = false;
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {}

        Parser.ExcelInput excel = new Parser.ExcelInput();
        private void btnOpenExcel_Click(object sender, EventArgs e)
        {

            ofdPDF.FileName = "";
            ofdPDF.Filter = "Excel Files|*.xls";
            if (ofdPDF.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                tabExcelData.Controls.Clear();
                if (excel.Load(ofdPDF.FileName,true))                
                {
                    excel.reshapData();
                    rtx.Text = ofdPDF.FileName;                 

                    TabControl tbs = new TabControl();
                    tabExcelData.Controls.Add(tbs);
                    tbs.Dock = DockStyle.Fill;

                    foreach (DataTable dt in excel.sheets.Tables)
                    {
                        TabPage tpage = new TabPage(dt.TableName);

                        tbs.TabPages.Add(tpage);

                        DataGridView grd = new DataGridView();

                        grd.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
                        grd.BorderStyle = System.Windows.Forms.BorderStyle.None;                        
                        grd.ColumnHeadersVisible = false;
                        grd.Dock = System.Windows.Forms.DockStyle.Fill;
                        grd.ReadOnly = true;
                        grd.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Lucida Console", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                        grd.RowTemplate.Height = 20;

                        tpage.Controls.Add(grd);
                        grd.DataSource = dt;
                        grd.AutoResizeColumns();
                        grd.Refresh();

                    }
                
            

                }
                
            
            }
            

        }

    }
}
