﻿using System;
using System.Collections.Generic;

namespace Framework.IRepositories
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(Func<TEntity, Boolean> predicate);
        TEntity GetById(long Id);
        TEntity Get(Func<TEntity, Boolean> where);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetMany(Func<TEntity, bool> where);
        bool Any(Func<TEntity, Boolean> where);
    }
}
