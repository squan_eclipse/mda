﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using Framework.IRepositories;

namespace Framework.Repositories
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected DbContext _dataContext;
        protected readonly IDbSet<TEntity> dbset;

        protected RepositoryBase(DbContext dataContext)
        {
            this._dataContext = dataContext; 
            dbset = this._dataContext.Set<TEntity>();
        }

        public virtual void Add(TEntity entity)
        {
            dbset.Add(entity);
        }
        public virtual void Update(TEntity entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(TEntity entity)
        {
            dbset.Remove(entity);
        }
        public void Delete(Func<TEntity, Boolean> where)
        {
            IEnumerable<TEntity> objects = dbset.Where<TEntity>(where).AsEnumerable();
            foreach (TEntity obj in objects)
                dbset.Remove(obj);
        }
        public virtual TEntity GetById(long id)
        {
            return dbset.Find(id);
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return dbset.ToList();
        }
        public virtual IEnumerable<TEntity> GetMany(Func<TEntity, bool> where)
        {
            return dbset.Where(where).ToList();
        }
        public TEntity Get(Func<TEntity, Boolean> where)
        {
            return dbset.Where(where).FirstOrDefault<TEntity>();
        }

        public TEntity GetLocal(Func<TEntity, Boolean> where)
        {
            return dbset.Local.Where(where).FirstOrDefault<TEntity>();
        }

        public bool Any(Func<TEntity, Boolean> where)
        {
            return dbset.Any(where);
        }
    }
}


