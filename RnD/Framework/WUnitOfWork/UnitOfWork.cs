﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using Framework.DBContextFactory;
using Framework.IRepositories;
using Framework.Repositories;

namespace Framework.UnitofWork
{
    public class UnitOfWork<TContext> : IUnitofWork where TContext : DbContext, new()
    {
        #region DataMemeber
        
        private Dictionary<string, object> _repositories;
        private TContext _dataContext;
        private IDBFactory _dbFactory;
        private bool _disposed;

        #endregion DataMemeber

        #region Constructors
        public UnitOfWork(IDBFactory dbFactory)
        {
            this._dbFactory = dbFactory;
            _dataContext = dbFactory.DataContext as TContext;
        }

        public IRepositoryBase<TEntity> GetRepository<TRepositoryBase, TEntity>()
            where TEntity : class
            where TRepositoryBase : IRepositoryBase<TEntity>
        {
            if (_repositories == null)
                _repositories = new Dictionary<string, object>();

            var repositoryType = typeof(TRepositoryBase);
            var entityType = typeof(TEntity);
            if (repositoryType == null || entityType == null)
                throw new InvalidOperationException(String.Format("No implementation of required Repository was found"));

            var key = repositoryType.Name + "::" + entityType.Name;

            if (_repositories.ContainsKey(key))
                return _repositories[key] as IRepositoryBase<TEntity>;

            try
            {
                var repository = Activator.CreateInstance(repositoryType, this._dataContext);
                if (repository == null )
                    throw new InvalidOperationException(String.Format("Unable to create instance of the required Repository. May be the constructor for the DataContext was not found"));

                _repositories.Add(key, repository);
                return repository as IRepositoryBase<TEntity>;
            }
            catch (Exception ex)
            {                
                throw;
            }
        }

        public virtual IRepositoryBase<TEntity> GetGenericRepository<TEntity>() where TEntity : class
        {
            return this.GetRepository<IRepositoryBase<TEntity>, TEntity>();
        }

        #endregion Constructors

        #region Context
        protected DbContext DataContext
        {
            get { return this._dataContext ?? _dbFactory.DataContext; }
        }

        public void Commit()
        {
            _dataContext.SaveChanges();
        }

        public void RollBack()
        {
            //http://code.msdn.microsoft.com/How-to-undo-the-changes-in-00aed3c4
            //_dataContext.ChangeTracker.Entries().ForEach(entry => entry.State = System.Data.EntityState.Unchanged);
            //foreach (var entry in context.ChangeTracker
            //                 .Entries<YourEntityType>()
            //                 .Where(e => e.State == EntityState.Modified))
            //{
            //    entry.CurrentValues.SetValues(entry.OriginalValues);
            //}

            //a. Undo the Changes in the Context level
            //   It undoes the changes of the all entries.
            //foreach (DbEntityEntry entry in context.ChangeTracker.Entries())
            //{
            //    switch (entry.State)
            //    {
            //        // Under the covers, changing the state of an entity from  
            //        // Modified to Unchanged first sets the values of all  
            //        // properties to the original values that were read from  
            //        // the database when it was queried, and then marks the  
            //        // entity as Unchanged. This will also reject changes to  
            //        // FK relationships since the original value of the FK  
            //        // will be restored. 
            //        case EntityState.Modified:
            //            entry.State = EntityState.Unchanged;
            //            break;
            //        case EntityState.Added:
            //            entry.State = EntityState.Detached;
            //            break;
            //        // If the EntityState is the Deleted, reload the date from the database.   
            //        case EntityState.Deleted:
            //            entry.Reload();
            //            break;
            //        default: break;
            //    }
            //} 

            //b. Undo the Changes in the DbEntities level
            //It undoes the changes of the T type entries.
            //foreach (DbEntityEntry<T> entry in context.ChangeTracker.Entries<T>())
            //{
            //    switch (entry.State)
            //    {
            //        case EntityState.Modified:
            //            entry.State = EntityState.Unchanged;
            //            break;
            //        case EntityState.Deleted:
            //            entry.Reload();
            //            break;
            //        case EntityState.Added:
            //            entry.State = EntityState.Detached;
            //            break;
            //        default: break;


            //    }
            //} 
 
            //c. Undo the Changes in the DbEntity level
            //   It will first get the entry of the entity, and then undoes the changes.
            //    DbEntityEntry entry = context.Entry(entity); 
            //    if (entry != null) 
            //    { 
            //        switch (entry.State) 
            //        { 
            //            case EntityState.Modified: 
            //                entry.State = EntityState.Unchanged; 
            //                break; 
            //            case EntityState.Deleted: 
            //                entry.Reload(); 
            //                break; 
            //            case EntityState.Added: 
            //                entry.State = EntityState.Detached; 
            //                break; 
            //            default: break; 
            //        } 
            //    } 

            //d. Undo the Change in the DbEntity Property level
            //    DbEntityEntry entry = context.Entry(entity); 
            //    if (entry.State == EntityState.Added || entry.State == EntityState.Detached) 
            //    { 
            //        return; 
            //    }  
            //    // Get and Set the Property value by the Property Name. 
            //    object propertyValue = entry.OriginalValues.GetValue<object>(propertyName); 
            //    entry.Property(propertyName).CurrentValue = entry.Property(propertyName).OriginalValue; 



        }
        #endregion Context

        #region Dispose
        public void Dispose()
        {
            Dispose(true);            
        }

        private void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                this._dataContext.Dispose();
                GC.SuppressFinalize(this);
            }
            _disposed = true;
        }
        #endregion Dispose
    }
}
