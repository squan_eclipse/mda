﻿using System;
using Framework.IRepositories;

namespace Framework.UnitofWork
{
    public interface IUnitofWork : IDisposable
    {
        IRepositoryBase<TEntity> GetRepository<TRepositoryBase, TEntity>()
            where TEntity : class
            where TRepositoryBase : IRepositoryBase<TEntity>;

        void Commit();
        void RollBack();
    }
}
