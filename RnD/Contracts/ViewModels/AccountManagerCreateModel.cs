﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class AccountManagerCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        public int EntityID { get; set; }

        [Required(ErrorMessage = "The First Name field is required")]
        public string FirstName { get; set; }

        public string MidName { get; set; }

        [Required(ErrorMessage = "The Last Name field is required")]
        public string LastName { get; set; }

        public bool Selected { get; set; }

        public List<int> ListEntities { get; set; }

        [Required(ErrorMessage = "The Email Address field is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The Group field is required")]
        public int GroupID { get; set; }

        public string Group { get; set; }

        public List<SelectListItem> ListGroups { get; set; }

        [Required(ErrorMessage = "The Login name field is required")]
        [StringLength(50, ErrorMessage = "Login name must be at least 6 characters long", MinimumLength = 6)]
        public string Login { get; set; }

        [Required(ErrorMessage = "The Password field is required")]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = "Password must be at least 6 characters long", MinimumLength = 6)]
        public string Password { get; set; }

        [Required(ErrorMessage = "The Confirm Password field is required")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password",ErrorMessage = "Password doest not match")]
        public string ConfirmPassword { get; set; }

        public List<ContactCreateModel> ListContactInfo { get; set; }

        public Common.Status StatusID { get; set; }
    }

    [XmlRoot("Value")]
    public class AccountManagerModel : IModel
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string MidName { get; set; }

        public string LastName { get; set; }
        
        public string Email { get; set; }

        public string Group { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public List<ContactModel> ListContactInfo { get; set; }
    }
}
