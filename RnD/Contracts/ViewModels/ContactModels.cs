﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class ContactCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        public int AccountID { get; set; }

        [Required(ErrorMessage = "The Contact Type field is required")]
        public int TypeID { get; set; }

        public string Type { get; set; }

        public List<SelectListItem> ListTypes { get; set; }

        public string CountryCode { get; set; }

        public string AreaCode { get; set; }

        [Required(ErrorMessage = "The Phone Number field is required")]
        public string PhoneNumber { get; set; }

        public Status StatusID { get; set; }
    }

    [XmlRoot("Value")]
    public class ContactModel : IModel
    {
        public int ID { get; set; }
                
        public string Type { get; set; }

        public string CountryCode { get; set; }

        public string AreaCode { get; set; }

        public string PhoneNumber { get; set; }
    }
}
