﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    /// <summary>
    /// All model class should implement this interface to create xml.
    /// </summary>
    public interface IModel { }

    [XmlRoot("Value")]
    public class GenericModel<T> : IModel
    {
        public T Value { get; set; }
    }

    [XmlRoot("Value")]
    public class DictionaryModel<TKey, TSource> : IModel
    {
        public TKey Key { get; set; }

        public TSource Value { get; set; }
    }

    public abstract class BaseModel
    {
        public virtual int CreatedBy { get; set; }

        public virtual DateTime CreatedOn { get; set; }

        public virtual int? ModifiedBy { get; set; }

        public virtual DateTime? ModifiedOn { get; set; }
    }

    [DataContract]
    public class RequestModel
    {
        public Option Option{ get; set; }
        public int UserID { get; set; }
        public Guid AuthCode { get; set; }
        public string ModelXml { get; set; }
    }

    [DataContract]
    public class ResponseModel
    {
        public ResponseStatus Status { get; set; }

        public StatusMessage StatusMessage { get; set; }

        public string ResponseMessage { get; set; }

        public string ModelXml { get; set; }

        public string StackTrace { get; set; }
    }
}