﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class EntityTreeModel : IModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int CategoryID { get; set; }

        public string Category { get; set; }

        public List<EntityTreeModel> ChildTree { get; set; }

        public int? ParentID { get; set; }

        public int UserID { get; set; }
    }

    [XmlRoot("Value")]
    public class EntityCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Entity type field is required")]
        public int CategoryID { get; set; }

        public List<SelectListItem> ListCategories { get; set; }

        public string ABN { get; set; }

        public string ACN { get; set; }

        public string GST { get; set; }

        public string TFN { get; set; }

        [Required(ErrorMessage = "The Name field is required")]
        public string Name { get; set; }

        public string Note { get; set; }

        public int? ParentID { get; set; }

        public List<AddressCreateModel> ListAddress { get; set; }

        public List<AccountManagerCreateModel> ListUser { get; set; }

        public EntityTypes EntityType { get; set; }

        public int? PrimaryAccountMangerID { get; set; }
    }

    [XmlRoot("Value")]
    public class EntityModel : BaseModel, IModel
    {
        public int ID { get; set; }

        public string Category { get; set; }

        public EntityTypes EntityType { get; set; }

        public string ABN { get; set; }

        public string ACN { get; set; }

        public string GST { get; set; }

        public string TFN { get; set; }

        public string Name { get; set; }

        public string Note { get; set; }

        public List<AddressModel> Addresses { get; set; }

        public AccountManagerModel PrimaryAccountManger { get; set; }

        public int? PrimaryAccountMangerID { get; set; }
    }

    [XmlRoot("Value")]
    public class EntityDetailModel : BaseModel, IModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Type { get; set; }

        public string PrimaryAccountManger { get; set; }
    }
}