﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;
using DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class ListofAllowedPages
    { 
    
    }

    [XmlRoot("Value")]
    public class OptionModel : IModel
    {
        [Required]
        [Display(Name = "Email Address")]
        public OptionType OptionType{ get; set; }
        [Required]
        [Display(Name = "Email Address")]
        public int UserID{ get; set; }

    }

    [XmlRoot("Value")]
    public class LoginModel : IModel
    {
        [Required]
        [Display(Name = "Login Name")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    [XmlRoot("Value")]
    public class UserModel : IModel
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public Guid AuthCode { get; set; }

        public string Name { get; set; }

        public List<string> Roles { get; set; }
    }

    [XmlRoot("Value")]
    public class UserGroupModel : IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Group Name field is requred")]
        public string Title { get; set; }

        public string Description { get; set; }

        public Status Status { get; set; }

        public bool IsSystem { get; set; }
    }

    [XmlRoot("Value")]
    public class UserGroupRolesModel : IModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<RoleModel> AvailableRoles { get; set; }

        public List<RoleModel> Roles { get; set; }
    }

    [XmlRoot("Value")]
    public class RoleModel : IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Role Name field is requred")]
        public string Title { get; set; }

        public string Description { get; set; }

        public Status Status { get; set; }

        public bool IsSystem { get; set; }
    }

    [XmlRoot("Value")]
    public class RoleOptionsModel : IModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<DictionaryModel<int, string>> AvailableOptions { get; set; }

        public List<DictionaryModel<int, string>> Options { get; set; }

        public int CreateBy { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
