﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class AddressCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Address Type field is required")]
        public int TypeID { get; set; }

        public List<SelectListItem> ListTypes { get; set; }

        public string UnitNumber { get; set; }

        public string StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        [Required(ErrorMessage = "The City field is required")]
        public string City { get; set; }

        public int? StateID { get; set; }

        public string State { get; set; }

        public int? CountryID { get; set; }

        public string PostCode { get; set; }

        public string Type { get; set; }

        public List<SelectListItem> ListCountires { get; set; }

        public string Country { get; set; }

        public List<SelectListItem> ListState { get; set; }

        public bool Default { get; set; }

        public Common.Status StatusID { get; set; }
    }

    [XmlRoot("Value")]
    public class AddressModel : IModel
    {
        public int ID { get; set; }

        public string Type { get; set; }

        public string UnitNumber { get; set; }

        public string StreetNumber { get; set; }

        public string StreetName { get; set; }

        public string StreetType { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostCode { get; set; }

        public string Country { get; set; }

        public bool Default { get; set; }
    }

    [XmlRoot("Value")]
    public class CountryModel : IModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public bool Default { get; set; }
    }

}
