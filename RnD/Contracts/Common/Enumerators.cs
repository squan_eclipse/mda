﻿using System.ComponentModel;

namespace DBASystem.SMSF.Contracts.Common
{
    public enum Status
    {
        Active = 1,
        Inactive = 2,
        Deleted = 3
    }

    public enum ResponseStatus
    {
        Success,
        Failure,
        Error
    }

    public enum Option
    {
        Login = 101,
        LogOut = 102,
        ValidateLogin = 103,

        AddEntity = 301,
        EntityTree = 302,
        ListEntityAccountManagers = 303,
        ListAddressTypes = 304,
        GetEntityCategories = 305,
        GetEntityTypes = 306,
        ListCountries = 307,
        ListStates = 308,
        GetEntityDetail = 309,
        GetAccountManagerDetail = 310,
        DeleteEntity = 311,
        GetEntity = 312,
        UpdateEntity = 313,
        AddGroup = 314,
        AddClient = 315,
        AddFund = 316,
        UpdateGroup = 317,
        UpdateClient = 318,
        UpdateFund = 319,
        GetClients = 320,
        GetFunds = 321,
        GetEntities = 322,

        AllowedPages = 401,
        AllowedOptions = 402,
        GetTelephoneTypes = 404,
        AddTelephone = 405,
        UpdateTelephone = 406,
        DeleteTelephone = 407,

        CreateUser = 501,
        DeleteAccountManager = 502,
        UpdateUser = 503,
        GetAccountManager = 504,

        CreateUserGroup = 601,
        UpdateUserGroup = 602,
        ChangeUserGroupStatus = 603,
        GetUserGroups = 604,
        GetUserGroup = 605,
        GetRoles = 606,
        GetRole = 607,
        CreateRole = 608,
        UpdateRole = 609,
        ChangeRoleStatus = 610,
        GetUserGroupRoles = 611,
        AddUserGroupRoles = 612,
        GetUserRoleOptions = 613,
        AddUserRoleOptions = 614,
    }

    public enum OptionType
    {
        Page = 3,
        Menu = 4,
        NextPage = 5,
        Report = 6,
        View = 7,
        Modify = 8,
        New = 9,
        Remove = 10,
        Cancel = 11,
        Print = 12,
        Search = 13,
        Maker = 14,
        Checker = 15,
        CustomAction = 16
    }

    public enum EntityTypes
    {
        Entity = 1,
        Group = 2,
        [Description("Clients")]
        Client = 3,
        [Description("Funds")]
        Fund = 4
    }

    public enum StreetType
    {
        ALLEY,
        APPROACH,
        ARCADE,
        AVENUE,
        BOULEVARD,
        BROW,
        BYPASS,
        CAUSEWAY,
        CIRCUIT,
        CIRCUS,
        CLOSE,
        COPSE,
        CORNER,
        COVE,
        COURT,
        CRESCENT,
        DRIVE,
        END,
        ESPLANANDE,
        FLAT,
        FREEWAY,
        FRONTAGE,
        GARDENS,
        GLADE,
        GLEN,
        GREEN,
        GROVE,
        HEIGHTS,
        HIGHWAY,
        LANE,
        LINK,
        LOOP,
        MALL,
        MEWS,
        PACKET,
        PARADE,
        PARK,
        PARKWAY,
        PLACE,
        PROMENADE,
        RESERVE,
        RIDGE,
        RISE,
        ROAD,
        ROW,
        SQUARE,
        STREET,
        STRIP,
        TARN,
        TERRACE,
        THOROUGHFARE,
        TRACK,
        TRUNKWAY,
        VIEW,
        VISTA,
        WALK,
        WAY,
        WALKWAY,
        YARD
    }

    public enum StatusMessage
    {
        [Description("")]
        None,
        [Description("Successful")]
        Successful,
        [Description("Failure")]
        Failure,
        [Description("Error")]
        Error,
        [Description("User Not Found")]
        UserNotFound,
        [Description("Login Name Already Exists")]
        LoginNameAlreadyExists,
        [Description("Unexpected Error Occured")]
        UnexpectedErrorOccured,
        [Description("Entity Not Found")]
        EntityNotFound,
        [Description("Invalid Username Or Password")]
        InvalidUsernameOrPassword,
        [Description("Login Name Available")]
        LoginNameAvailable,
        [Description("No Action Signature Found")]
        NoActionSignatureFound,
        [Description("Access Denied")]
        AccessDenied,
        [Description("ABN Already Exists")]
        ABNAlreadyExists,
        [Description("ACN Already Exists")]
        ACNAlreadyExists,
        [Description("GST Already Exists")]
        GSTAlreadyExists,
        [Description("TFN Already Exists")]
        TFNAlreadyExists,
        [Description("Name Already Exists")]
        NameAlreadyExists,
        [Description("User Login Already Exists")]
        UserLoginAlreadyExists,
        [Description("Entity Type Of \"Group\" Not Found")]
        EntityTypeOfGroupNotFound,
        [Description("Unable To Add")]
        UnableToAdd,
        [Description("Unable To Delete")]
        UnableToDelete,
        [Description("Record Added Successfully")]
        RecordAddedSuccessfully,
        [Description("Record Deleted Successfully")]
        RecordDeletedSuccessfully,
        [Description("Group with the same name already exists")]
        UserGroupAlreadyExists,
        [Description("The model proided is either Empty or Invalid")]
        EmptyOrInvalidModel,
        [Description("Unable to update the specified record(s)")]
        UnableToUpdate,
        [Description("Role with the same name already exists")]
        RoleAlreadyExists
    }
}