﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBASystem.SMSF.Contracts.Common 
{
    public static class StringExtensions
    {
        public static string ToValueOrEmpty(this string value)
        {
            return value ?? string.Empty;
        }

        public static string RemoveSpaceToLower(this string str)
        {
            return string.IsNullOrEmpty(str) ? null : str.Replace(" ", string.Empty).ToLower();
        }
    }
}
