﻿using NLog;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.ComponentModel;

namespace DBASystem.SMSF.Contracts.Common 
{
    public class Utility
    {
        private static Logger m_logger = LogManager.GetCurrentClassLogger();

        static public Logger Logger 
        {
            get
            {
                return m_logger;
            }
        }

        static public string GetExceptionDetails(Exception ex)
        {
            return string.Format
                (
                    "Message: {0}\r\nInner Message{1}\r\n", 
                    (ex != null) ? ex.Message : "", 
                    (ex != null) ? ((ex.InnerException != null)? ex.InnerException.Message : "") : ""
                );
        }

        static public string GetComputerName()
        {
            return System.Environment.MachineName;
        }

        public static string EnumValue(Enum e)
        {
            return ((char)e.GetHashCode()).ToString(CultureInfo.InvariantCulture);
        }

        public static T ToEnum<T>(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                throw new ArgumentException("Argument null or empty");
            }
            if (s.Length > 1)
            {
                throw new ArgumentException("Argument length greater than one");
            }
            return (T)Enum.ToObject(typeof(T), s[0]);
        }
    }

    public static class Tracer
    {
        public static DateTime Start(string component, string action, string paramList)
        {
            Utility.Logger.Trace
            (
                string.Format
                (
                    ">> Execute >> Server: {0} >> Thread ID: {1} >> Component: {2} >> Action: {3} >> ParamList: {4}",
                    Utility.GetComputerName(),
                    System.Threading.Thread.CurrentThread.ManagedThreadId,
                    component,
                    action, paramList
                )
            );

            return DateTime.Now;
        }

        public static DateTime End(DateTime time)
        {
            TimeSpan interval = DateTime.Now - time;
            Utility.Logger.Trace
            (
                string.Format
                (
                    ">> Execute >> Server: {0} >> Thread ID: {1} >> Completed: {2} ms>>",
                    Utility.GetComputerName(),
                    System.Threading.Thread.CurrentThread.ManagedThreadId,
                    interval.TotalMilliseconds
                )
            );

            return DateTime.Now;
        }
    }

    public static class EnumHelper
    {
        private static Dictionary<Enum, DescriptionAttribute> _stringValues = new Dictionary<Enum,DescriptionAttribute>();

        public static string GetDescription(this Enum status)
        {
            string output = null;
            Type type = status.GetType();

            //Check first in our cached results...
            if (_stringValues.ContainsKey(status))
                output = (_stringValues[status] as DescriptionAttribute).Description;
            else
            {
                //Look for our 'StringValueAttribute' 
                //in the field's custom attributes
                FieldInfo fi = type.GetField(status.ToString());
                DescriptionAttribute[] attrs =
                   fi.GetCustomAttributes(typeof(DescriptionAttribute),
                                           false) as DescriptionAttribute[];
                if (attrs.Length > 0)
                {
                    _stringValues.Add(status, attrs[0]);
                    output = attrs[0].Description;
                }
                else
                {
                    output = attrs[0].ToString();
                }
            }

            return output;
        }
    }
}
