﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class ComponentMapConfig : ConfigurationSection
    {

        [ConfigurationProperty("component")]
        public ComponentElementColection Component
        {
            get
            {
                return (ComponentElementColection)this["component"];
            }
            set
            { this["component"] = value; }
        }
    }

    public class ComponentElementColection : ConfigurationElementCollection
    {
    
    }

    public class ComponentElement : ConfigurationElement
    {
        [ConfigurationProperty("alias" )]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1, MaxLength = 60)]
        public String Name
        {
            get
            {
                return (String)this["alias"];
            }
            set
            {
                this["alias"] = value;
            }
        }

        [ConfigurationProperty("typeName")]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\ ", MinLength = 1, MaxLength = 60)]
        public String TypeName
        {
            get
            {
                return (String)this["typeName"];
            }
            set
            {
                this["typeName"] = value;
            }
        }

        [ConfigurationProperty("action")]
        public ActionElement Action
        {
            get
            {
                return (ActionElement)this["action"];
            }
            set
            { this["action"] = value; }
        }
    }

    public class ActionElement : ConfigurationElement
    {
        [ConfigurationProperty("alias")]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\", MinLength = 1, MaxLength = 60)]
        public String Name
        {
            get
            {
                return (String)this["alias"];
            }
            set
            {
                this["alias"] = value;
            }
        }

        [ConfigurationProperty("method")]
        [StringValidator(InvalidCharacters = "~!@#$%^&*()[]{}/;'\"|\\ ", MinLength = 1, MaxLength = 60)]
        public String Method
        {
            get
            {
                return (String)this["method"];
            }
            set
            {
                this["method"] = value;
            }
        }

    }
}


namespace DBASystem.SMSF.Service.Business.Components {
    
    
    public sealed class ComponentMapConfig {
        
        private static ComponentMapConfigSection _config;
        
        static ComponentMapConfig() {
            _config = ((ComponentMapConfigSection)(global::System.Configuration.ConfigurationManager.GetSection("ComponentMapConfig")));
        }
        
        private ComponentMapConfig() {
        }
        
        public static ComponentMapConfigSection Config {
            get {
                return _config;
            }
        }
    }
    
    public sealed partial class ComponentMapConfigSection : System.Configuration.ConfigurationElementCollection //System.Configuration.ConfigurationSection {
        
        public ComponentElementCollection this[int i] {
            get {
                return ((ComponentElementCollection)(this.BaseGet(i)));
            }
        }
        
        protected override System.Configuration.ConfigurationElement CreateNewElement() {
            return new ComponentElementCollection();
        }
        
        protected override object GetElementKey(System.Configuration.ConfigurationElement element) {
            return ((ComponentElementCollection)(element)).Alias;
        }
        
        public sealed partial class ComponentElementCollection : System.Configuration.ConfigurationElementCollection {
            
            [System.Configuration.ConfigurationPropertyAttribute("alias", IsRequired=true)]
            public string Alias {
                get {
                    return ((string)(this["alias"]));
                }
                set {
                    this["alias"] = value;
                }
            }
            
            [System.Configuration.ConfigurationPropertyAttribute("typeName", IsRequired=true)]
            public string TypeName {
                get {
                    return ((string)(this["typeName"]));
                }
                set {
                    this["typeName"] = value;
                }
            }
            
            public ActionElement this[int i] {
                get {
                    return ((ActionElement)(this.BaseGet(i)));
                }
            }
            
            protected override System.Configuration.ConfigurationElement CreateNewElement() {
                return new ActionElement();
            }
            
            protected override object GetElementKey(System.Configuration.ConfigurationElement element) {
                return ((ActionElement)(element)).Alias;
            }
            
            public sealed partial class ActionElement : System.Configuration.ConfigurationElement {
                
                [System.Configuration.ConfigurationPropertyAttribute("alias", IsRequired=true)]
                public string Alias {
                    get {
                        return ((string)(this["alias"]));
                    }
                    set {
                        this["alias"] = value;
                    }
                }
                
                [System.Configuration.ConfigurationPropertyAttribute("method", IsRequired=true)]
                public string Method {
                    get {
                        return ((string)(this["method"]));
                    }
                    set {
                        this["method"] = value;
                    }
                }
            }
        }
    }
}
