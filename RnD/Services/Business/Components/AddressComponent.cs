﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data.UnitofWork;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class AddressComponent : Component
    {
        private SMSFUnitOfWork uow = SMSFUnitOfWork.CreateNewInstance;

        public AddressComponent()
            : base()
        {
        }

        [ActionAttribute]
        public string GetCountries(string xml)
        {
            return GetCountries();
        }

        [ActionAttribute]
        public string GetCountries()
        {
            var repository = uow.AddressRepository;
            var countries = uow.CountryRepository.GetAll().Select(a => new CountryModel { ID = a.ID, Name = a.Name, Default = a.Default }).ToList();
            return ModelSerializer.GenerateReponseXml<CountryModel>(countries, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string GetStates(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (model != null)
                return GetStates(model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string GetStates(int countryId)
        {
            var states = uow.StateRepository.GetMany(a => a.CountryID == countryId).Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Name }).ToList();
            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(states, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string GetAddressTypes(string xml)
        {
            return this.GetAddressTypes();
        }

        [ActionAttribute]
        public string GetAddressTypes()
        {
            var types = uow.AddressTypeRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active)
                                                .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();

            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(types, ResponseStatus.Success, StatusMessage.Successful);
        }
    }
}
