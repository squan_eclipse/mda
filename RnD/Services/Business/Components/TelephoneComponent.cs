﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data.SMSF.Entities;
using DBASystem.SMSF.Data.UnitofWork;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class TelephoneComponent : Component
    {
        private SMSFUnitOfWork uow = SMSFUnitOfWork.CreateNewInstance;

        public TelephoneComponent()
            : base()
        {
        }

        [ActionAttribute]
        public string GetTelephoneTypes(string xml)
        {
            return GetTelephoneTypes();
        }

        [ActionAttribute]
        public string GetTelephoneTypes()
        {
            var listPhoneTypes = uow.TelephoneTypeRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active)
                                                            .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Type }).ToList();

            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(listPhoneTypes, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string AddTelephone(string xml)
        {
            var model = (ContactCreateModel)ModelDeserializer.DeserializeFromXml<ContactCreateModel>(xml);
            if(model != null)
                return AddTelephone(model);
            else
                return base.NullModelReference ();
        }

        [ActionAttribute]
        public string AddTelephone(ContactCreateModel model)
        {
            var entity = new Telephone
            {
                AccountID = model.AccountID,
                AreaCode = model.AreaCode,
                CountryCode = model.CountryCode,
                PhoneNumber = model.PhoneNumber,
                StatusID = (int)Contracts.Common.Status.Active,
                TypeID = model.TypeID
            };

            uow.TelephoneRepository.Add(entity);
            uow.Commit();

            if (entity.ID > 0)
                return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = entity.ID }, ResponseStatus.Success, StatusMessage.Successful);
            else
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string UpdateTelephone(string xml)
        {
            var model = (ContactCreateModel)ModelDeserializer.DeserializeFromXml<ContactCreateModel>(xml);
            if (model != null)
                return UpdateTelephone(model);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string UpdateTelephone(ContactCreateModel model)
        {
            var entity = uow.TelephoneRepository.GetById(model.ID);

            entity.AccountID = model.AccountID;
            entity.AreaCode = model.AreaCode;
            entity.CountryCode = model.CountryCode;
            entity.PhoneNumber = model.PhoneNumber;
            entity.StatusID = (int)Contracts.Common.Status.Active;
            entity.TypeID = model.TypeID;

            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string DeleteTelephone(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (model != null)
                return DeleteTelephone(model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string DeleteTelephone(int id)
        {
            var entity = uow.TelephoneRepository.GetById(id);
            uow.TelephoneRepository.Delete(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }
    }
}
