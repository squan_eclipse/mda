﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data.SMSF.Entities;
using DBASystem.SMSF.Data.UnitofWork;
using System;
using System.Linq;
using common = DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Service.Business.Components
{    
    public class UserComponent : Component
    {
        #region Declaration

        private SMSFUnitOfWork uow = SMSFUnitOfWork.CreateNewInstance; 

        #endregion

        #region Constructor
        
        public UserComponent()
            : base()
        {
        }

        #endregion

        #region User

        [ActionAttribute]
        public string ValidateUser(string xml)
        {
            var model = (LoginModel)ModelDeserializer.DeserializeFromXml<LoginModel>(xml);
            if (model != null)
                return this.ValidateUser(model.Email, model.Password);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string ValidateUser(string email, string password)
        {
            var repository = uow.UserRepository;

            StatusMessage responseMessage = StatusMessage.Successful;
            common.ResponseStatus responseStatus;
            UserModel userModel = null;

            var user = uow.UserRepository.Get(a => a.Login == email && a.Password == password && (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == common.Status.Active
                                                    && a.AccountManager != null && (DBASystem.SMSF.Contracts.Common.Status)a.AccountManager.StatusID == common.Status.Active);

            if (user != null)
            {
                if (user.AuthCode == null)
                {
                    user.AuthCode = Guid.NewGuid();
                    uow.Commit();
                }
                responseStatus = common.ResponseStatus.Success;
                userModel = new UserModel
                {
                    Email = user.AccountManager.Email,
                    FirstName = user.AccountManager.FirstName,
                    ID = user.AccountID,
                    LastName = user.AccountManager.LastName,
                    MiddleName = user.AccountManager.MidName,
                    AuthCode = user.AuthCode.Value,
                    Name = user.Login
                };
            }
            else
            {
                responseStatus = common.ResponseStatus.Failure;
                responseMessage = StatusMessage.InvalidUsernameOrPassword;
            }

            return ModelSerializer.GenerateReponseXml(userModel, responseStatus, responseMessage);
        }

        [ActionAttribute]
        public string ValidateLogin(string xml)
        {
            var model = (DictionaryModel<int, string>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, string>>(xml);
            if (model != null)
                return ValidateLogin(model.Key, model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string ValidateLogin(int? id, string login)
        {
            var repository = uow.UserRepository;

            var exist = false;
            if (id != null)
                exist = repository.Any(a => a.AccountID != id && a.Login == login);
            else
                exist = repository.Any(a => a.Login == login);

            if (!exist)
                return ModelSerializer.GenerateReponseXml(null, common.ResponseStatus.Success, StatusMessage.LoginNameAvailable);
            else
                return ModelSerializer.GenerateReponseXml(null, common.ResponseStatus.Failure, StatusMessage.LoginNameAlreadyExists);
        }

        #endregion

        #region Groups

        [ActionAttribute]
        public string GetUserGroups(string xml)
        {
            return this.GetUserGroups();
        }

        [ActionAttribute]
        public string GetUserGroups()
        {
            var groups = uow.GroupRepository.GetAll().Select(a => new UserGroupModel { ID = a.ID, Title = a.Title, Description = a.Description, Status = (DBASystem.SMSF.Contracts.Common.Status)a.StatusID }).ToList();

            return ModelSerializer.GenerateReponseXml<UserGroupModel>(groups, common.ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string GetUserGroup(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetUserGroup(model.Value);
        }

        [ActionAttribute]
        public string GetUserGroup(int id)
        {
            var group = uow.GroupRepository.GetById(id);

            var model = new UserGroupModel
            {
                Description = group.Description,
                ID = group.ID,
                IsSystem = group.IsSystem,
                Status = (common.Status)group.StatusID,
                Title = group.Title
            };
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string CreateUserGroup(string xml)
        {
            var model = (UserGroupModel)ModelDeserializer.DeserializeFromXml<UserGroupModel>(xml);

            return CreateUserGroup(model);
        }

        [ActionAttribute]
        public string CreateUserGroup(UserGroupModel model)
        {
            var groupRepository = uow.GroupRepository;

            if (groupRepository.Any(a => a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.UserGroupAlreadyExists);

            var entity = new Group
            {
                Description = model.Description,
                IsSystem = model.IsSystem,
                StatusID = (int)model.Status,
                Title = model.Title
            };

            groupRepository.Add(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string UpdateUserGroup(string xml)
        {
            var model = (UserGroupModel)ModelDeserializer.DeserializeFromXml<UserGroupModel>(xml);

            return UpdateUserGroup(model);
        }

        [ActionAttribute]
        public string UpdateUserGroup(UserGroupModel model)
        {
            var groupRepository = uow.GroupRepository;

            if (groupRepository.Any(a => a.ID != model.ID && a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.UserGroupAlreadyExists);

            var entity = groupRepository.GetById(model.ID);

            entity.Description = model.Description;
            entity.Title = model.Title;

            groupRepository.Update(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string ChangeUserGroupStatus(string xml)
        {
            var model = (DictionaryModel<int, DBASystem.SMSF.Contracts.Common.Status>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, DBASystem.SMSF.Contracts.Common.Status>>(xml);

            return ChangeUserGroupStatus(model);
        }

        [ActionAttribute]
        public string ChangeUserGroupStatus(DictionaryModel<int, common.Status> model)
        {
            var groupRepository = uow.GroupRepository;

            var entity = groupRepository.GetById(model.Key);

            entity.StatusID = (int)model.Value;

            groupRepository.Update(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        #endregion

        #region Roles

        [ActionAttribute]
        public string GetRoles(string xml)
        {
            return this.GetRoles();
        }

        [ActionAttribute]
        public string GetRoles()
        {
            var roles = uow.RoleRepository.GetAll().Select(a => new RoleModel { ID = a.ID, Title = a.Title, Description = a.Description, Status = (DBASystem.SMSF.Contracts.Common.Status)a.StatusID }).ToList();

            return ModelSerializer.GenerateReponseXml<RoleModel>(roles, common.ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string GetRole(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetRole(model.Value);
        }

        [ActionAttribute]
        public string GetRole(int id)
        {
            var role = uow.RoleRepository.GetById(id);

            var model = new RoleModel
            {
                Description = role.Description,
                ID = role.ID,
                IsSystem = role.IsSystem,
                Status = (common.Status)role.StatusID,
                Title = role.Title
            };
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string CreateRole(string xml)
        {
            var model = (RoleModel)ModelDeserializer.DeserializeFromXml<RoleModel>(xml);

            return CreateRole(model);
        }

        [ActionAttribute]
        public string CreateRole(RoleModel model)
        {
            var roleRepository = uow.RoleRepository;

            if (roleRepository.Any(a => a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.RoleAlreadyExists);

            var entity = new Role
            {
                Description = model.Description,
                IsSystem = model.IsSystem,
                StatusID = (int)model.Status,
                Title = model.Title
            };

            roleRepository.Add(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string UpdateRole(string xml)
        {
            var model = (RoleModel)ModelDeserializer.DeserializeFromXml<RoleModel>(xml);

            return UpdateRole(model);
        }

        [ActionAttribute]
        public string UpdateRole(RoleModel model)
        {
            var roleRepository = uow.RoleRepository;

            if (roleRepository.Any(a => a.ID != model.ID && a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.RoleAlreadyExists);

            var entity = roleRepository.GetById(model.ID);

            entity.Description = model.Description;
            entity.Title = model.Title;

            roleRepository.Update(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        public string ChangeRoleStatus(string xml)
        {
            var model = (DictionaryModel<int, DBASystem.SMSF.Contracts.Common.Status>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, DBASystem.SMSF.Contracts.Common.Status>>(xml);

            return ChangeRoleStatus(model);
        }

        [ActionAttribute]
        public string ChangeRoleStatus(DictionaryModel<int, common.Status> model)
        {
            var roleRepository = uow.RoleRepository;

            var entity = roleRepository.GetById(model.Key);

            entity.StatusID = (int)model.Value;

            roleRepository.Update(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        #endregion

        #region GroupRoles

        [ActionAttribute]
        public string GetUserGroupRoles(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetUserGroupRoles(model.Value);
        }

        [ActionAttribute]
        public string GetUserGroupRoles(int id)
        {
            var group = uow.GroupRepository.GetById(id);

            var model = new UserGroupRolesModel
            {
                Description = group.Description,
                ID = group.ID,
                Title = group.Title,
                Roles = group.GroupRoles.Where(a => (DBASystem.SMSF.Contracts.Common.Status)a.Role.StatusID == common.Status.Active)
                                          .Select(a => new RoleModel
                                          {
                                              Description = a.Role.Description,
                                              ID = a.RoleID,
                                              Title = a.Role.Title,
                                              Status = (common.Status)a.Role.StatusID
                                          }).ToList()
            };

            model.AvailableRoles = uow.RoleRepository.GetMany(a => !model.Roles.Select(x => x.ID).Contains(a.ID) && (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == common.Status.Active)
                                                .Select(a => new RoleModel
                                                {
                                                    Description = a.Description,
                                                    ID = a.ID,
                                                    Title = a.Title,
                                                    Status = (common.Status)a.StatusID
                                                }).ToList();

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string AddUserGroupRoles(string xml)
        {
            var model = (UserGroupRolesModel)ModelDeserializer.DeserializeFromXml<UserGroupRolesModel>(xml);

            return AddUserGroupRoles(model);
        }

        [ActionAttribute]
        public string AddUserGroupRoles(UserGroupRolesModel model)
        {
            var groupRoleRepository = uow.GroupRoleRepository;

            groupRoleRepository.Delete(a => a.GroupID == model.ID);

            foreach (var role in model.Roles)
                groupRoleRepository.Add(new GroupRole { GroupID = model.ID, RoleID = role.ID });

            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        #endregion

        #region RoleOptions

        [ActionAttribute]
        public string GetUserRoleOptions(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetUserRoleOptions(model.Value);
        }

        [ActionAttribute]
        public string GetUserRoleOptions(int id)
        {
            var role = uow.RoleRepository.GetById(id);

            var model = new RoleOptionsModel
            {
                Description = role.Description,
                ID = role.ID,
                Title = role.Title,
                Options = role.RoleOptions.Where(a => (DBASystem.SMSF.Contracts.Common.Status)a.Option.StatusID == common.Status.Active)
                                            .OrderBy(a => a.Option.Title)
                                            .Select(a => new DictionaryModel<int, string>
                                            {
                                                Key = a.Option.ID,
                                                Value = a.Option.Title
                                            }).ToList()
            };

            model.AvailableOptions = uow.OptionRepository.GetMany(a => !model.Options.Select(x => x.Key).Contains(a.ID) && !a.AllowAnonymous
                                                                        && (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == common.Status.Active)
                                                        .OrderBy(a => a.Title)
                                                        .Select(a => new DictionaryModel<int, string>
                                                        {
                                                            Key = a.ID,
                                                            Value = a.Title
                                                        }).ToList();

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string AddUserRoleOptions(string xml)
        {
            var model = (RoleOptionsModel)ModelDeserializer.DeserializeFromXml<RoleOptionsModel>(xml);

            return AddUserRoleOptions(model);
        }

        [ActionAttribute]
        public string AddUserRoleOptions(RoleOptionsModel model)
        {
            var roleOptionRepository = uow.RoleOptionRepository;

            roleOptionRepository.Delete(a => a.RoleID == model.ID);

            foreach (var role in model.Options)
                roleOptionRepository.Add(new RoleOption
                {
                    RoleID = model.ID,
                    OptionID = role.Key,
                    CreatedBy = model.CreateBy,
                    CreatedOn = model.CreatedOn,
                    IsAssigned = true,
                    IsSystem = true
                });

            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        #endregion
    }
}
