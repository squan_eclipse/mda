﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data.Common;
using DBASystem.SMSF.Data.UnitofWork;
using System;
using System.Collections.Generic;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class ValidationComponent : Component
    {
        private Dictionary<Option, ActionSignature> actionSignatureList = new Dictionary<Option, ActionSignature>();

        private SMSFUnitOfWork uow = SMSFUnitOfWork.CreateNewInstance;
        
        [ActionAttribute]
        public bool IsAllowed(int? userID, Guid authCode, Option option)
        {
            //if (option == Option.Login && userID < 1)
            //    return true;

            if (uow.OptionRepository.Any(a => a.ID == (int)option && a.AllowAnonymous))
                return true;

            return uow.UserRepository.IsAllowed(userID.Value, authCode, option);
        }
        
        [ActionAttribute]
        public ActionSignature GetActionSignature(Option option)
        {
            ActionSignature actionSignature = null;
            if (actionSignatureList.ContainsKey(option))
            {
                actionSignature = actionSignatureList[option];
            }
            else
            {
                actionSignature = uow.UserRepository.GetActionSignature(option);
                actionSignatureList.Add(option, actionSignature);
            }
            return actionSignature;
        }

        [ActionAttribute]
        public string GetAllowedOptionsByOptionType(string xml)
        {
            var model = (OptionModel)ModelDeserializer.DeserializeFromXml<OptionModel>(xml);
            if (model != null)
                return this.GetAllowedOptionsByOptionType(model.UserID, model.OptionType);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string GetAllowedOptionsByOptionType(int UserID, OptionType optionType)
        {
            List<Option> list = uow.UserRepository.GetAllowedOptionsByOptionType(UserID, optionType);
            return ModelSerializer.GenerateReponseXml<Option>(list, ResponseStatus.Success, StatusMessage.Successful);
        }
    }
}
