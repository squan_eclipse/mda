﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data.SMSF.Entities;
using DBASystem.SMSF.Data.UnitofWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class EntityComponent : Component
    {
        private SMSFUnitOfWork uow = SMSFUnitOfWork.CreateNewInstance;

        public EntityComponent()
            : base()
        {
        }

        #region Entity

        [ActionAttribute]
        public string AddEntity(string xml)
        {
            var model = (EntityCreateModel)ModelDeserializer.DeserializeFromXml<EntityCreateModel>(xml);
            if (model != null)
                return AddEntity(model);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string AddGroup(string xml)
        {
            return AddEntity(xml);
        }

        [ActionAttribute]
        public string AddClient(string xml)
        {
            return AddEntity(xml);
        }

        [ActionAttribute]
        public string AddFund(string xml)
        {
            return AddEntity(xml);
        }

        [ActionAttribute]
        public string AddEntity(EntityCreateModel model)
        {
            try
            {
                var status = ResponseStatus.Success;
                var message = ValidateEntityOnCreate(model, ref status);

                if (status == ResponseStatus.Success)
                {
                    var entity = AddNewEntity(model);

                    AddAddressesOnCreateEntity(model, entity);

                    uow.Commit();

                    return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = entity.ID }, status, message);
                }

                return ModelSerializer.GenerateReponseXml(null, status, message);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Add Entity

        [ActionAttribute]
        public StatusMessage ValidateEntityOnCreate(EntityCreateModel model, ref ResponseStatus status)
        {
            StatusMessage message = StatusMessage.Successful;

            if (!string.IsNullOrEmpty(model.ABN) && uow.EntityRepository.Any(a => (EntityTypes)a.TypeID == model.EntityType && a.ABN == model.ABN && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            {
                status = ResponseStatus.Failure;
                message = StatusMessage.ABNAlreadyExists;
            }
            //if (!string.IsNullOrEmpty(model.ACN) && uow.EntityRepository.Any(a => a.ACN == model.ACN && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.ACNAlreadyExists;
            //}
            //if (status == ResponseStatus.Success && !string.IsNullOrEmpty(model.GST) && uow.EntityRepository.Any(a => a.GST == model.GST && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.GSTAlreadyExists;
            //}
            //if (status == ResponseStatus.Success && !string.IsNullOrEmpty(model.TFN) && uow.EntityRepository.Any(a => a.TFN == model.TFN && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.TFNAlreadyExists;
            //}
            //if (status == ResponseStatus.Success && uow.EntityRepository.Any(a => a.Name == model.Name && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.NameAlreadyExists;
            //}
            if (status == ResponseStatus.Success && model.ListUser != null && model.ListUser.Any(a => a.ID < 0))
            {
                var logins = model.ListUser.Where(a => a.ID < 0).Select(a => a.Login).ToList();
                if (uow.UserRepository.Any(a => logins.Contains(a.Login) && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
                {
                    status = ResponseStatus.Failure;
                    message = StatusMessage.UserLoginAlreadyExists;
                }
            }

            return message;
        }

        [ActionAttribute]
        public Entity AddNewEntity(EntityCreateModel model)
        {
            var entity = new Entity
            {
                ABN = model.ABN,
                ACN = model.ACN,
                CreatedBy = model.CreatedBy,
                CreatedOn = model.CreatedOn,
                GST = model.GST,
                Name = model.Name,
                Note = model.Note,
                ParentID = model.ParentID,
                StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Active,
                TFN = model.TFN,
                CategoryID = model.CategoryID == 0 ? (int?)null : model.CategoryID,
                TypeID = (int)model.EntityType,
                PrimaryAccountManagerID = model.PrimaryAccountMangerID
            };
            uow.EntityRepository.Add(entity);
            return entity;
        }

        [ActionAttribute]
        public void AddAddressesOnCreateEntity(EntityCreateModel model, Entity entity)
        {
            if (model.ListAddress != null && model.ListAddress.Any())
            {
                foreach (var address in model.ListAddress)
                {
                    AddAddress(entity, address);
                }
            }
        }

        [ActionAttribute]
        public void AddAddress(Entity entity, AddressCreateModel address)
        {
            entity.Addresses.Add(new Address
            {
                City = address.City,
                CountryID = address.CountryID,
                StateID = address.StateID,
                CreatedBy = address.CreatedBy,
                CreatedOn = address.CreatedOn,
                EntityID = entity.ID,
                PostCode = address.PostCode,
                StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Active,
                StreetName = address.StreetName,
                StreetNumber = address.StreetNumber,
                StreetType = address.StreetType,
                TypeID = address.TypeID,
                UnitNumber = address.UnitNumber,
                Default = address.Default
            });
        }

        #endregion

        [ActionAttribute]
        public string UpdateEntity(string xml)
        {
            var model = (EntityCreateModel)ModelDeserializer.DeserializeFromXml<EntityCreateModel>(xml);
            if (model != null)
                return UpdateEntity(model);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string UpdateGroup(string xml)
        {
            return UpdateEntity(xml);
        }

        [ActionAttribute]
        public string UpdateClient(string xml)
        {
            return UpdateEntity(xml);
        }

        [ActionAttribute]
        public string UpdateFund(string xml)
        {
            return UpdateEntity(xml);
        }

        [ActionAttribute]
        public string UpdateEntity(EntityCreateModel model)
        {
            var status = ResponseStatus.Success;
            var message = ValidateEntityOnUpdate(model, ref status);

            if (status == ResponseStatus.Success)
            {
                var entity = uow.EntityRepository.GetById(model.ID);

                entity.ABN = model.ABN;
                entity.ACN = model.ACN;
                entity.ModifiedBy = model.ModifiedBy;
                entity.ModifiedOn = model.ModifiedOn;
                entity.GST = model.GST;
                entity.Name = model.Name;
                entity.Note = model.Note;
                entity.TFN = model.TFN;
                entity.CategoryID = model.CategoryID == 0 ? (int?)null : model.CategoryID;
                entity.PrimaryAccountManagerID = model.PrimaryAccountMangerID;

                UpdateAddressOnUpdateEntity(model, entity);
                uow.Commit();

                if (entity.ID > 0)
                    status = ResponseStatus.Success;
                else
                {
                    status = ResponseStatus.Failure;
                    message = StatusMessage.UnexpectedErrorOccured;
                }
            }

            if (message != StatusMessage.Successful)
                status = ResponseStatus.Failure;

            return ModelSerializer.GenerateReponseXml(null, status, message);
        }

        #region Update Entity

        [ActionAttribute]
        public StatusMessage ValidateEntityOnUpdate(EntityCreateModel model, ref ResponseStatus status)
        {
            StatusMessage message = StatusMessage.Successful;

            if (!string.IsNullOrEmpty(model.ABN) && uow.EntityRepository.Any(a => a.ID != model.ID && (EntityTypes)a.TypeID == model.EntityType && a.ABN == model.ABN && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            {
                status = ResponseStatus.Failure;
                message = StatusMessage.ABNAlreadyExists;
            }
            //if (!string.IsNullOrEmpty(model.ACN) && uow.EntityRepository.Any(a => a.ID != model.ID && a.ACN == model.ACN && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.ACNAlreadyExists;
            //}
            //if (status == ResponseStatus.Success && !string.IsNullOrEmpty(model.GST) && uow.EntityRepository.Any(a => a.ID != model.ID && a.GST == model.GST && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.GSTAlreadyExists;
            //}
            //if (status == ResponseStatus.Success && !string.IsNullOrEmpty(model.TFN) && uow.EntityRepository.Any(a => a.ID != model.ID && a.TFN == model.TFN && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.TFNAlreadyExists;
            //}
            //if (status == ResponseStatus.Success && uow.EntityRepository.Any(a => a.ID != model.ID && a.Name == model.Name && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
            //{
            //    status = ResponseStatus.Failure;
            //    message = StatusMessage.NameAlreadyExists;
            //}
            if (status == ResponseStatus.Success && model.ListUser != null && model.ListUser.Any(a => a.ID < 0))
            {
                var logins = model.ListUser.Where(a => a.ID < 0).Select(a => a.Login).ToList();
                if (uow.UserRepository.Any(a => logins.Contains(a.Login) && (Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active))
                {
                    status = ResponseStatus.Failure;
                    message = StatusMessage.UserLoginAlreadyExists;
                }
            }

            return message;
        }

        [ActionAttribute]
        public void UpdateAddressOnUpdateEntity(EntityCreateModel model, Entity entity)
        {
            if (model.ListAddress != null)
            {
                foreach (var address in model.ListAddress.Where(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Deleted).ToList())
                    entity.Addresses.Remove(uow.AddressRepository.GetById(address.ID));

                foreach (var address in model.ListAddress.Where(a => a.ID > 0 && (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active).ToList())
                {
                    var addressEntity = entity.Addresses.First(a => a.ID == address.ID);

                    addressEntity.City = address.City;
                    addressEntity.CountryID = address.CountryID;
                    addressEntity.StateID = address.StateID;
                    addressEntity.ModifiedBy = address.ModifiedBy;
                    addressEntity.ModifiedOn = address.ModifiedOn;
                    addressEntity.PostCode = address.PostCode;
                    addressEntity.StreetName = address.StreetName;
                    addressEntity.StreetNumber = address.StreetNumber;
                    addressEntity.StreetType = address.StreetType;
                    addressEntity.TypeID = address.TypeID;
                    addressEntity.UnitNumber = address.UnitNumber;
                    addressEntity.Default = address.Default;
                }

                foreach (var address in model.ListAddress.Where(a => a.ID < 0 && (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active).ToList())
                {
                    entity.Addresses.Add(new Address
                    {
                        City = address.City,
                        CountryID = address.CountryID,
                        StateID = address.StateID,
                        CreatedBy = address.CreatedBy,
                        CreatedOn = address.CreatedOn,
                        EntityID = entity.ID,
                        PostCode = address.PostCode,
                        StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Active,
                        StreetName = address.StreetName,
                        StreetNumber = address.StreetNumber,
                        StreetType = address.StreetType,
                        TypeID = address.TypeID,
                        UnitNumber = address.UnitNumber,
                        Default = address.Default
                    });
                }
            }
        }

        #endregion

        [ActionAttribute]
        public string GetEntityDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (model != null)
                return GetEntityDetail(model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string GetEntityDetail(int id)
        {
            var repository = uow.EntityRepository;

            var entity = repository.GetById(id);

            if (entity != null)
            {
                var model = new EntityModel
                {
                    ABN = entity.ABN,
                    ACN = entity.ACN,
                    GST = entity.GST,
                    ID = entity.ID,
                    Name = entity.Name,
                    Note = entity.Note,
                    TFN = entity.TFN,
                    Category = entity.CategoryID == null ? ((EntityTypes)entity.TypeID).ToString() : entity.EntityCategory.Description,
                    EntityType = (EntityTypes)entity.TypeID,
                    PrimaryAccountMangerID = entity.PrimaryAccountManagerID,
                    Addresses = entity.Addresses.Any(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active) ?
                                entity.Addresses.Select(a => new AddressModel
                                {
                                    City = a.City,
                                    Country = a.CountryID != null ? a.Country.Name : "",
                                    Default = a.Default,
                                    ID = a.ID,
                                    PostCode = a.PostCode,
                                    State = a.StateID != null ? a.State.Name : "",
                                    StreetName = a.StreetName,
                                    StreetNumber = a.StreetNumber,
                                    StreetType = a.StreetType,
                                    Type = a.AddressType.Description,
                                    UnitNumber = a.UnitNumber
                                }).ToList() : null
                };

                if (model.PrimaryAccountMangerID != null)
                {
                    var manager = uow.AccountManagerRepository.GetById((int)entity.PrimaryAccountManagerID);
                    if (manager != null)
                    {
                        model.PrimaryAccountManger = new AccountManagerModel
                        {
                            Email = manager.Email,
                            FirstName = manager.FirstName,
                            Group = manager.User.Group.Title,
                            ID = manager.ID,
                            LastName = manager.LastName,
                            Login = manager.User.Login,
                            MidName = manager.MidName,
                            Password = manager.User.Password,
                            ListContactInfo = manager.Telephones.Any(b => (DBASystem.SMSF.Contracts.Common.Status)b.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active) ?
                                              manager.Telephones.Select(b => new ContactModel
                                              {
                                                  AreaCode = b.AreaCode,
                                                  CountryCode = b.CountryCode,
                                                  ID = b.ID,
                                                  PhoneNumber = b.PhoneNumber,
                                                  Type = b.TelephoneType.Type
                                              }).ToList() : null
                        };
                    }
                }

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.EntityNotFound);
        }

        [ActionAttribute]
        public string DeleteEntity(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            if (model != null)
                return DeleteEntity(model.Key, model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string DeleteEntity(int id, int userId)
        {
            var entity = uow.EntityRepository.GetById(id);

            if (entity != null)
            {
                entity.StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Deleted;
                entity.ModifiedBy = userId;
                entity.ModifiedOn = DateTime.Now;

                uow.Commit();

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, StatusMessage.Successful);
            }

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, StatusMessage.UnableToDelete);
        }

        [ActionAttribute]
        public string GetEntity(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (model != null)
                return GetEntity(model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string GetEntity(int id)
        {
            var entity = uow.EntityRepository.GetById(id);

            if (entity != null)
            {
                var model = new EntityCreateModel
                {
                    ABN = entity.ABN,
                    ACN = entity.ACN,
                    GST = entity.GST,
                    ID = entity.ID,
                    Name = entity.Name,
                    Note = entity.Note,
                    TFN = entity.TFN,
                    CategoryID = entity.CategoryID == null ? 0 : (int)entity.CategoryID,
                    EntityType = (EntityTypes)entity.TypeID,
                    ParentID = entity.ParentID,
                    PrimaryAccountMangerID = entity.PrimaryAccountManagerID,
                    ListAddress = entity.Addresses.Any(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active) ?
                                    entity.Addresses.Select(a => new AddressCreateModel
                                    {
                                        CountryID = a.CountryID,
                                        StateID = a.StateID,
                                        TypeID = (int)a.TypeID,
                                        City = a.City,
                                        Country = a.CountryID != null ? a.Country.Name : "",
                                        Default = a.Default,
                                        ID = a.ID,
                                        PostCode = a.PostCode,
                                        State = a.StateID != null ? a.State.Name : "",
                                        StreetName = a.StreetName,
                                        StreetNumber = a.StreetNumber,
                                        StreetType = a.StreetType,
                                        Type = a.AddressType.Description,
                                        UnitNumber = a.UnitNumber,
                                        StatusID = (Contracts.Common.Status)a.StatusID
                                    }).ToList() : null
                };

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.EntityNotFound);
        }

        [ActionAttribute]
        public string GetClients(string xml)
        {
            var model = (DictionaryModel<int, EntityTypes>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, EntityTypes>>(xml);
            return GetEntities(model.Key, model.Value);
        }

        [ActionAttribute]
        public string GetFunds(string xml)
        {
            var model = (DictionaryModel<int, EntityTypes>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, EntityTypes>>(xml);
            return GetEntities(model.Key, model.Value);
        }

        [ActionAttribute]
        public string GetEntities(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetEntities(model.Value);
        }

        private string GetEntities(int parentId)
        {
            var entities = uow.EntityRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active &&
                                                                 (EntityTypes)a.TypeID != EntityTypes.Client && (EntityTypes)a.TypeID != EntityTypes.Fund && a.ParentID == parentId)
                                                .OrderBy(a => a.Name)
                                                .Select(a => new EntityDetailModel
                                                {
                                                    ID = a.ID,
                                                    Name = a.Name,
                                                    Type = a.EntityType.Description,
                                                    PrimaryAccountManger = a.PrimaryAccountManagerID != null ? GetPrimaryAccountManager((int)a.PrimaryAccountManagerID) : null,
                                                    CreatedOn = a.CreatedOn,
                                                    ModifiedOn = a.ModifiedOn
                                                }).ToList();

            var managers = uow.AccountManagerRepository.GetMany(a => a.EntityID == parentId && (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active)
                                                        .Select(a => new EntityDetailModel
                                                        {
                                                            Email = a.Email,
                                                            ID = a.ID,
                                                            Name = a.FirstName + " " + a.LastName,
                                                            Type = "User",
                                                            CreatedOn = a.CreatedOn,
                                                            ModifiedOn = a.ModifiedOn
                                                        }).ToList();

            entities = entities.Concat(managers).ToList();

            return ModelSerializer.GenerateReponseXml(entities, ResponseStatus.Success, StatusMessage.Successful);
        }

        private string GetPrimaryAccountManager(int id)
        {
            var manager = uow.AccountManagerRepository.GetById(id);
            return manager.FirstName + " " + manager.LastName;
        }

        private string GetEntities(int parentId, EntityTypes type)
        {
            var entities = uow.EntityRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active &&
                                                             (EntityTypes)a.TypeID == type && a.ParentID == parentId)
                                                .OrderBy(a => a.Name)
                                                .Select(a => new EntityModel
                                                {
                                                    ABN = a.ABN,
                                                    ACN = a.ACN,
                                                    GST = a.GST,
                                                    ID = a.ID,
                                                    Name = a.Name,
                                                    Note = a.Note,
                                                    TFN = a.TFN,
                                                    EntityType = (EntityTypes)a.TypeID,
                                                    PrimaryAccountMangerID = a.PrimaryAccountManagerID,
                                                    PrimaryAccountManger = a.PrimaryAccountManagerID != null ?
                                                                            uow.AccountManagerRepository.GetMany(x => x.ID == (int)a.PrimaryAccountManagerID)
                                                                                                        .Select(x => new AccountManagerModel
                                                                                                        {
                                                                                                            FirstName = x.FirstName,
                                                                                                            LastName = x.LastName
                                                                                                        }).FirstOrDefault() : null
                                                }).ToList();

            return ModelSerializer.GenerateReponseXml(entities, ResponseStatus.Success, StatusMessage.Successful);
        }

        #endregion

        [ActionAttribute]
        public string GetEntityCategories(string xml)
        {
            var model = (GenericModel<int?>)ModelDeserializer.DeserializeFromXml<GenericModel<int?>>(xml);
            return this.GetEntityCategories(model.Value);
        }

        [ActionAttribute]
        public string GetEntityCategories(int? id)
        {
            List<DictionaryModel<int, string>> types;

            if (id == null)
            {
                types = uow.EntityCategoryRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active && a.ID > 1)
                                                    .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();
            }
            else
            {
                var entity = uow.EntityRepository.GetById((int)id);
                if (entity.ParentID == null)
                    types = uow.EntityCategoryRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active)
                                                        .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();
                else
                    types = uow.EntityCategoryRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == Contracts.Common.Status.Active && a.ID > 1)
                                                        .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();
            }

            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(types, ResponseStatus.Success, StatusMessage.Successful);
        }

        #region Entity Tree
        [ActionAttribute]
        public string GetEntityTree(string xml)
        {
            GenericModel<int> model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            int? id = null;
            if (model != null)
                id = model.Value;
            return this.GetEntityTree(id);
        }

        [ActionAttribute]
        public string GetEntityTree(int? entityId)
        {
            var repository = uow.EntityRepository;
            var accountRepository = uow.AccountManagerRepository;

            var entities = repository.GetMany(a => ((DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active)
                                                    && ((EntityTypes)a.TypeID == EntityTypes.Entity || (EntityTypes)a.TypeID == EntityTypes.Group))
                                        .OrderBy(a => a.Name);

            var models = entities.Select(a => new EntityTreeModel
                                {
                                    ID = a.ID,
                                    Name = a.Name,
                                    Category = (EntityTypes)a.TypeID == EntityTypes.Entity? a.EntityCategory.Description.ToLower(): "group",
                                    ParentID = a.ParentID
                                }).OrderBy(a => a.Name).ToList();

            var mangaers = accountRepository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active)
                                            .Select(a => new EntityTreeModel
                                            {
                                                Name = a.FirstName + " " + a.LastName,
                                                ParentID = a.EntityID,
                                                Category = "user",
                                                UserID = a.ID
                                            }).ToList();

            models = models.Concat<EntityTreeModel>(mangaers).ToList();

            var entitiesTree = entityId == null ? GenerateTree(models.ToList(), entityId) : GenerateDetailTree(models.ToList(), entityId);

            return ModelSerializer.GenerateReponseXml<EntityTreeModel>(entitiesTree, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public List<EntityTreeModel> GenerateTree(List<EntityTreeModel> entities, int? parentId)
        {
            return entities.Where(a => a.ParentID == parentId).Select(a => new EntityTreeModel
            {
                ChildTree = (List<EntityTreeModel>)GenerateTree(entities, a.ID),
                ID = a.ID,
                Name = a.Name,
                Category = a.Category,
                ParentID = a.ParentID,
                UserID = a.UserID
            }).ToList();
        }

        [ActionAttribute]
        public List<EntityTreeModel> GenerateDetailTree(List<EntityTreeModel> entities, int? parentId)
        {
            return entities.Where(a => a.ID == parentId).Select(a => new EntityTreeModel
            {
                ChildTree = (List<EntityTreeModel>)GenerateTree(entities, a.ID),
                ID = a.ID,
                Name = a.Name,
                Category = a.Category,
                ParentID = a.ParentID,
                UserID = a.UserID
            }).ToList();
        } 

        #endregion
    }
}
