﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data.Repositories;
using DBASystem.SMSF.Data.SMSF.Entities;
using DBASystem.SMSF.Data.UnitofWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class AccountManagerComponent : Component
    {
        private SMSFUnitOfWork uow = SMSFUnitOfWork.CreateNewInstance;

        public AccountManagerComponent()
            : base()
        {
        }

        [ActionAttribute]
        public string GetEntityAccountManagers(string xml)
        {
            var model = (GenericModel<int?>)ModelDeserializer.DeserializeFromXml<GenericModel<int?>>(xml);
            if (model != null)
                return GetEntityAccountManagers(model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string GetEntityAccountManagers(int? EntityID)
        {
            AccountManagerRepository repository = uow.AccountManagerRepository;
            IEnumerable<AccountManager> managers;

            if (EntityID == null)
                managers = repository.GetMany(a => (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active);
            else
                managers = repository.GetMany(a => a.EntityID == EntityID.Value && a.User != null && (DBASystem.SMSF.Contracts.Common.Status)a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active);

            var models = managers.Select(a => new AccountManagerCreateModel
                                    {
                                        Login = a.User.Login,
                                        EntityID = a.EntityID,
                                        FirstName = a.FirstName,
                                        ID = a.ID,
                                        LastName = a.LastName,
                                        MidName = a.MidName,
                                        Email = a.Email,
                                        Group = a.User.Group.Title,
                                        GroupID = a.User.GroupID,
                                        StatusID = DBASystem.SMSF.Contracts.Common.Status.Active,
                                        Password = a.User.Password,
                                        ListContactInfo = a.Telephones.Any() ? a.Telephones.Select(x => new ContactCreateModel
                                        {
                                            AccountID = x.AccountID,
                                            AreaCode = x.AreaCode,
                                            CountryCode = x.CountryCode,
                                            ID = x.ID,
                                            PhoneNumber = x.PhoneNumber,
                                            TypeID = (int)x.TypeID,
                                            Type = x.TelephoneType.Type,
                                            StatusID = (Contracts.Common.Status)x.StatusID
                                        }).ToList() : null
                                    }).ToList();

            return ModelSerializer.GenerateReponseXml<AccountManagerCreateModel>(models, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string AddAccountManager(string xml)
        {
            var model = (AccountManagerCreateModel)ModelDeserializer.DeserializeFromXml<AccountManagerCreateModel>(xml);
            if (model != null)
                return AddAccountManager(model);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        private string AddAccountManager(AccountManagerCreateModel model)
        {
            if (uow.UserRepository.Any(a => a.Login == model.Login))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.LoginNameAlreadyExists);

            var entity = new AccountManager
            {
                CreatedBy = model.CreatedBy,
                CreatedOn = model.CreatedOn,
                Email = model.Email,
                EntityID = model.EntityID,
                FirstName = model.FirstName,
                LastName = model.LastName,
                MidName = model.MidName,
                StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Active,
                User = new User
                {
                    CreateDate = model.CreatedOn,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                    GroupID = model.GroupID,
                    IsSystem = false,
                    Login = model.Login,
                    Password = model.Password,
                    StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Active,
                }
            };

            if (model.ListContactInfo != null && model.ListContactInfo.Any())
            {
                foreach (var contact in model.ListContactInfo)
                {
                    var telephone = new DBASystem.SMSF.Data.SMSF.Entities.Telephone
                    {
                        AccountID = entity.ID,
                        AreaCode = contact.AreaCode,
                        CountryCode = contact.CountryCode,
                        PhoneNumber = contact.PhoneNumber,
                        StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Active,
                        TypeID = contact.TypeID
                    };
                    entity.Telephones.Add(telephone);
                }
            }

            uow.AccountManagerRepository.Add(entity);
            uow.Commit();

            if (entity.ID > 0)
                return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = entity.ID }, ResponseStatus.Success, StatusMessage.Successful);
            else
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.UnableToAdd);
        }

        [ActionAttribute]
        public string UpdateAccountManager(string xml)
        {
            var model = (AccountManagerCreateModel)ModelDeserializer.DeserializeFromXml<AccountManagerCreateModel>(xml);
            if (model != null)
                return UpdateAccountManager(model);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string UpdateAccountManager(AccountManagerCreateModel model)
        {
            var repository = uow.AccountManagerRepository;
            var userRepository = uow.UserRepository;

            if (userRepository.Any(a => a.AccountID != model.ID && a.Login == model.Login))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.LoginNameAlreadyExists);

            var entity = uow.AccountManagerRepository.GetById(model.ID);

            if (entity != null)
            {
                entity.ModifiedBy = model.ModifiedBy;
                entity.ModifiedOn = model.ModifiedOn;
                entity.Email = model.Email;
                entity.EntityID = model.EntityID;
                entity.FirstName = model.FirstName;
                entity.LastName = model.LastName;
                entity.MidName = model.MidName;
                entity.User.ModifiedBy = model.ModifiedBy;
                entity.User.ModifiedOn = model.ModifiedOn;
                entity.User.GroupID = model.GroupID;
                entity.User.IsSystem = false;
                entity.User.Login = model.Login;
                entity.User.Password = model.Password;

                if (model.ListContactInfo != null)
                {
                    foreach (var contact in model.ListContactInfo.Where(a => a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Deleted).ToList())
                        entity.Telephones.Remove(uow.TelephoneRepository.GetById(contact.ID));

                    foreach (var contact in model.ListContactInfo.Where(a => a.ID > 0 && a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active).ToList())
                    {
                        var contactEntity = uow.TelephoneRepository.GetById(contact.ID);

                        contactEntity.AreaCode = contact.AreaCode;
                        contactEntity.CountryCode = contact.CountryCode;
                        contactEntity.PhoneNumber = contact.PhoneNumber;
                        contactEntity.TypeID = contact.TypeID;
                    }

                    foreach (var contact in model.ListContactInfo.Where(a => a.ID < 0 && a.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active).ToList())
                    {
                        uow.TelephoneRepository.Add(new Telephone
                        {
                            AccountID = entity.ID,
                            AreaCode = contact.AreaCode,
                            CountryCode = contact.CountryCode,
                            PhoneNumber = contact.PhoneNumber,
                            StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Active,
                            TypeID = contact.TypeID
                        });
                    }
                }
            }

            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, StatusMessage.Successful);
        }

        [ActionAttribute]
        public string GetAccountManagerDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (model != null)
                return GetAccountManagerDetail(model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string GetAccountManagerDetail(int id)
        {
            var repository = uow.AccountManagerRepository;
            var entity = repository.GetById(id);

            if (entity != null)
            {
                var model = new AccountManagerModel
                {
                    Email = entity.Email,
                    FirstName = entity.FirstName,
                    Group = entity.User != null ? entity.User.Group.Title : "",
                    ID = entity.ID,
                    LastName = entity.LastName,
                    Login = entity.User != null ? entity.User.Login : "",
                    MidName = entity.MidName,
                    Password = entity.User != null ? entity.User.Password : "",
                    ListContactInfo = entity.Telephones.Any(b => (DBASystem.SMSF.Contracts.Common.Status)b.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active) ?
                                      entity.Telephones.Select(b => new ContactModel
                                      {
                                          AreaCode = b.AreaCode,
                                          CountryCode = b.CountryCode,
                                          ID = b.ID,
                                          PhoneNumber = b.PhoneNumber,
                                          Type = b.TelephoneType.Type
                                      }).ToList() : null
                };

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.UserNotFound);
        }

        [ActionAttribute]
        public string DeleteAccountManager(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            if (model != null)
                return DeleteAccountManager(model.Key, model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string DeleteAccountManager(int id, int userId)
        {
            var repository = uow.AccountManagerRepository;
            var manager = uow.AccountManagerRepository.GetById(id);

            if (manager != null)
            {
                manager.StatusID = (int)DBASystem.SMSF.Contracts.Common.Status.Deleted;
                manager.ModifiedBy = userId;
                manager.ModifiedOn = DateTime.Now;

                uow.Commit();
                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, StatusMessage.RecordDeletedSuccessfully);
            }
            else
                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Failure, StatusMessage.UnableToDelete);
        }

        [ActionAttribute]
        public string GetAccountManager(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (model != null)
                return GetAccountManager(model.Value);
            else
                return base.NullModelReference();
        }

        [ActionAttribute]
        public string GetAccountManager(int id)
        {
            var repository = uow.AccountManagerRepository;

            var entity = repository.GetById(id);

            if (entity != null)
            {
                var model = new AccountManagerCreateModel
                {
                    Email = entity.Email,
                    FirstName = entity.FirstName,
                    Group = entity.User != null ? entity.User.Group.Title : "",
                    ID = entity.ID,
                    LastName = entity.LastName,
                    Login = entity.User != null ? entity.User.Login : "",
                    MidName = entity.MidName,
                    Password = entity.User != null ? entity.User.Password : "",
                    ConfirmPassword = entity.User != null ? entity.User.Password : "",
                    GroupID = entity.User != null ? entity.User.GroupID : 0,
                    StatusID = (Contracts.Common.Status)entity.StatusID,
                    EntityID = entity.EntityID,
                    ListContactInfo = entity.Telephones.Any(b => (DBASystem.SMSF.Contracts.Common.Status)b.StatusID == DBASystem.SMSF.Contracts.Common.Status.Active) ?
                                      entity.Telephones.Select(b => new ContactCreateModel
                                      {
                                          TypeID = (int)b.TypeID,
                                          AreaCode = b.AreaCode,
                                          CountryCode = b.CountryCode,
                                          ID = b.ID,
                                          PhoneNumber = b.PhoneNumber,
                                          Type = b.TelephoneType.Type,
                                          StatusID = (Contracts.Common.Status)b.StatusID
                                      }).ToList() : null
                };

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, StatusMessage.Successful);
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.UserNotFound);
        }
    }
}
