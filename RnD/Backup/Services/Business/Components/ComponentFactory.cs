﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Service.Business;

namespace DBASystem.SMSF.Service.Business.Components
{

    //A singleton shared among the objects
    public sealed class ComponentFactory
    {
        private static ComponentFactory _instance = new ComponentFactory();
        private Dictionary<Type, Component> componentList = new Dictionary<Type, Component>();

        public static ComponentFactory Instance
        {
            get
            {
                return _instance?? (_instance = new ComponentFactory() );
            }
        }

        public Component GetComponent(string nameSpace, string type)
        {
            Component result = null;
            var t = Type.GetType(nameSpace + "." + type);

            if (t != null)
                if (componentList.ContainsKey(t))
                    result = componentList[t];
                else
                {
                    result = (Component)Activator.CreateInstance(t);
                    componentList.Add(t, result); 
                }
            else
                throw new Exception("Specified Component Type is Invalid");
            return result;
        }

        public MethodInfo GetComponentMethod(Component component, string actionMethod)
        {
            MethodInfo methodInfo = null;
            if (component != null)
            {
                Type componentType = component.GetType();
                methodInfo = componentType.GetMethod(actionMethod, new Type[] { typeof(string) });
                if (methodInfo != null)
                    return methodInfo;
            }
            return methodInfo;
        }

        public object InvokeAction(Component component, string actionMethod, params object[] paramArray)
        {
            object result = null;
            string ModelXml = "";

            if (paramArray.Length > 0)
            {
                ModelXml = paramArray[0] is string ? paramArray[0].ToString() : "";
            }
            
            MethodInfo methodInfo = GetComponentMethod(component, actionMethod);
            
            if (methodInfo != null)
            {
                result = methodInfo.Invoke(component, new string[] { ModelXml });
            }
            return result;
        }

        public object InvokeAction(string nameSpace, string type, string action, params object[] paramArray)
        {
            try
            {
                Component component = this.GetComponent(nameSpace, type);
                return this.InvokeAction(component, action, paramArray);
            }
            catch (Exception ex)
            {
                Utility.Logger.Error("Error in Creating Component Object. Exception: " + Utility.GetExceptionDetails(ex));
                Console.WriteLine(ex);
                return ModelSerializer.GenerateErrorResponseXml(ex);
            }
        }
    }
}

