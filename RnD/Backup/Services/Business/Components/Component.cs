﻿using System.Collections.Generic;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;

namespace DBASystem.SMSF.Service.Business.Components
{
    /// <summary>
    /// This is the base component for the all the business object
    /// </summary>
    public abstract class Component
    {
        protected Component()
        {
        }
        public string NullModelReference()
        {
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Error, StatusMessage.EmptyOrInvalidModel);
        }
    }
}
