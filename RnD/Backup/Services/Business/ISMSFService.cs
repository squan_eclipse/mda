﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DBASystem.SMSF.Service.Business
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISMSFService
    {
        [OperationContract(AsyncPattern = false, IsOneWay = false)]
        string Execute(string value);
    }

    
}
