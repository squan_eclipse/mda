﻿using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Service.Business.Components;
using System.ServiceModel;
using System;
using System.Threading;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data.Common;
using System.Collections.Generic;

namespace DBASystem.SMSF.Service.Business
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.PerCall)]
    public class SMSFService : ISMSFService
    {
        public string Execute(string value)
        {
            try
            {
                var requestModel = ModelDeserializer.ParseRequestXML(value);
                
                if (requestModel == null) return null;

                object result = "";

                ValidationComponent validator = new ValidationComponent();

                if (validator.IsAllowed(requestModel.UserID, requestModel.AuthCode, requestModel.Option))
                {
                    ActionSignature actionSignature = validator.GetActionSignature(requestModel.Option);
                    if (actionSignature != null)
                    {
                        DateTime startTime = Tracer.Start(actionSignature.ComponentAssemblyName + " : " +
                                                        actionSignature.ComponentNamespace + " : " +
                                                        actionSignature.ComponentClassName,
                                                        actionSignature.ActionName,
                                                        actionSignature.ParamName + " => " + requestModel.ModelXml);

                        result = ComponentFactory.Instance.InvokeAction(actionSignature.ComponentNamespace,
                                                                        actionSignature.ComponentClassName,
                                                                        actionSignature.ActionName,
                                                                        requestModel.ModelXml);

                        Tracer.End(startTime);
                    }
                    else
                    {
                        result = ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.NoActionSignatureFound);
                        Utility.Logger.Error("Action Signature not found for the Option ID : " + (int)requestModel.Option);
                    }
                }
                else
                {
                    result = ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, StatusMessage.AccessDenied);
                    Utility.Logger.Error("Access denied. UserID : " + (int)requestModel.UserID + ", OptionID : " + (int)requestModel.Option + " [" + requestModel.Option);
                }
                return result.ToString();
            }

            catch (Exception ex)
            {
                Utility.Logger.Error(Utility.GetExceptionDetails(ex));
                return ModelSerializer.GenerateErrorResponseXml(ex);
            }
        }

    }
}

