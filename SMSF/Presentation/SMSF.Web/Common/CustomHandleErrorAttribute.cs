﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.ServiceModel;
using System.Web.Mvc;
using System.Web.Routing;

namespace DBASystem.SMSF.Web.Common
{
    public class CustomHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            Utility.Logger.Error(Utility.GetExceptionDetails(filterContext.Exception));

            if (filterContext.Exception.GetType() != typeof(SMSFServiceException)) 
            {
                new DBASystem.SMSF.Web.Controllers.HomeController().InsertError(filterContext.Exception);
            }

            // if the request is AJAX return JSON else view.
            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                filterContext.Controller.TempData["message"] = filterContext.Exception.Message;
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        error = true,
                        message = filterContext.Exception.Message
                    }
                };
            }
            else
            {
                RouteValueDictionary lRoutes;
                if (filterContext.Exception.GetType() == typeof(UnauthorizedAccessException))
                    lRoutes = new RouteValueDictionary(new { action = "AccessDenied", controller = "Error" });
                else
                    lRoutes = new RouteValueDictionary(new { action = "Index", controller = "Error" });

                filterContext.Controller.TempData["message"] = filterContext.Exception.Message;
                filterContext.Result = new RedirectToRouteResult(lRoutes);
            }

            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 500;

            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }
    }
}