﻿using System;
using System.Security.Principal;

namespace DBASystem.SMSF.Web.Common
{
    public class UserPrincipal : IPrincipal
    {
        private IIdentity _identity;

        #region IPrincipal Members

        public UserPrincipal(IIdentity identity)
        {
            _identity = identity;
        }

        public IIdentity Identity
        {
            get { return _identity; }
        }

        public bool IsInRole(string role)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}