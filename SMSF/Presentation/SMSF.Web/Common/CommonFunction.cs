﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Common
{
    public enum EumeratorType
    {
        Int,
        String,
        Char
    }

    public static class CommonFunction
    {
        public static IEnumerable<SelectListItem> EnumeratorList<T>(EumeratorType type)
        {
            var attribute = typeof(T).GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
            var title = attribute == null ? "Select" : ((DescriptionAttribute)attribute).Description;
            yield return new SelectListItem { Value = "", Text = title };

            foreach (var item in Enum.GetValues(typeof(T)))
            {
                var fieldInfo = typeof(T).GetField(item.ToString());
                attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                title = attribute == null ? item.ToString() : ((DescriptionAttribute)attribute).Description;
                var value = "";
                switch (type)
                {
                    case EumeratorType.Int:
                        value = ((int)item).ToString();
                        break;
                    case EumeratorType.String:
                        value = item.ToString();
                        break;
                    case EumeratorType.Char:
                        value = ((char)item.GetHashCode()).ToString();
                        break;
                }

                yield return new SelectListItem { Value = value, Text = title };
            }
        }

        public static string GetEnumeratorDescription<T>(string value)
        {
            value = Utility.ToEnum<T>(value).ToString();

            return GetDescription<T>(value);
        }

        public static string GetEnumeratorDescription<T>(T value)
        {
            return GetDescription<T>(value.ToString());
        }

        private static string GetDescription<T>(string value)
        {
            var fieldInfo = typeof(T).GetField(value);
            var attribute = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
            return attribute == null ? value : ((DescriptionAttribute)attribute).Description;
        }

        public static bool IsOptionAllowed(Option option)
        {
            if (SessionHelper.AllowedOptions == null)
                return false;

            return SessionHelper.AllowedOptions.Contains(option);
        }

        public static bool IsPageAllowed(Option option)
        {
            return SessionHelper.AllowedOptions.Contains(option);
        }

        public static string HashPassword(string password)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(password);
            var hash = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}