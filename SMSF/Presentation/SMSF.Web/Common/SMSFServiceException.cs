﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DBASystem.SMSF.Web.Common
{
    public class SMSFServiceException : Exception
    {
        public SMSFServiceException() { }

        public SMSFServiceException(string message) : base(message) { }
    }
}