﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

namespace DBASystem.SMSF.Web.Common
{
    public static class SessionHelper
    {
        public static bool IsAuthenticated
        {
            get
            {
                return UserId != 0;
            }
        }

        public static int UserId
        {
            get
            {
                return (int?)HttpContext.Current.Session["UserId"] ?? 0;
            }
            set
            {
                HttpContext.Current.Session["UserId"] = value;
            }
        }

        public static Guid? AuthCode
        {
            get
            {
                return (Guid?)HttpContext.Current.Session["AuthCode"];
            }
            set
            {
                HttpContext.Current.Session["AuthCode"] = value;
            }
        }

        public static string UserName
        {
            get
            {
                return (string)HttpContext.Current.Session["UserName"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }

        public static string Name
        {
            get
            {
                return (string)HttpContext.Current.Session["Name"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["Name"] = value;
            }
        }

        public static string Email
        {
            get
            {
                return (string)HttpContext.Current.Session["Email"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["Email"] = value;
            }
        }

        public static List<AddressCreateModel> ListAddresses
        {
            get
            {
                return (List<AddressCreateModel>)HttpContext.Current.Session["ListAddresses"];
            }
            set
            {
                HttpContext.Current.Session["ListAddresses"] = value;
            }
        }

        public static List<AccountManagerCreateModel> ListUsers
        {
            get
            {
                return (List<AccountManagerCreateModel>)HttpContext.Current.Session["ListUsers"];
            }
            set
            {
                HttpContext.Current.Session["ListUsers"] = value;
            }
        }

        public static List<ContactCreateModel> ListContacts
        {
            get
            {
                return (List<ContactCreateModel>)HttpContext.Current.Session["ListContacts"];
            }
            set
            {
                HttpContext.Current.Session["ListContacts"] = value;
            }
        }

        public static List<Option> AllowedPages
        {
            get { return (List<Option>)HttpContext.Current.Session["AllowedPages"]; }
            set { HttpContext.Current.Session["AllowedPages"] = value; }
        }

        public static List<Option> AllowedOptions
        {
            get { return (List<Option>)HttpContext.Current.Session["AllowedOptions"]; }
            set { HttpContext.Current.Session["AllowedOptions"] = value; }
        }

        public static KeyValuePair<int, string> SelectedEntity
        {
            get { return HttpContext.Current.Session["SelectedEntity"] != null ? (KeyValuePair<int, string>)HttpContext.Current.Session["SelectedEntity"] : new KeyValuePair<int, string>(); }
            set
            {
                HttpContext.Current.Session["SelectedEntity"] = value;
                if (value.Value == "Entity")
                    SelectedParentEntity = value.Key;
            }
        }

        public static int SelectedParentEntity
        {
            get { return HttpContext.Current.Session["SelectedParentEntity"] != null ? (int)HttpContext.Current.Session["SelectedParentEntity"] : -1; }
            set { HttpContext.Current.Session["SelectedParentEntity"] = value; }
        }

        public static string SelectedMenu
        {
            get
            {
                return (string)HttpContext.Current.Session["SelectedMenu"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["SelectedMenu"] = value;
            }
        }

        public static int EntityID
        {
            get
            {
                return (int?)HttpContext.Current.Session["EntityID"] ?? 0;
            }
            set
            {
                HttpContext.Current.Session["EntityID"] = value;
            }
        }

        public static EntityTypes EntityType
        {
            get
            {
                return (EntityTypes)HttpContext.Current.Session["EntityType"];
            }
            set
            {
                HttpContext.Current.Session["EntityType"] = value;
            }
        }

        public static UserGroup? Group
        {
            get
            {
                return (UserGroup?)HttpContext.Current.Session["Group"];
            }
            set
            {
                HttpContext.Current.Session["Group"] = value;
            }
        }

        public static EntityTreeType? EntityTreeType
        {
            get
            {
                return (EntityTreeType?)HttpContext.Current.Session["EntityTreeType"];
            }
            set
            {
                HttpContext.Current.Session["EntityTreeType"] = value;
            }
        }

        public static Object SessionObject
        {
            get
            {
                return HttpContext.Current.Session["SessionObject"];
            }
            set
            {
                HttpContext.Current.Session["SessionObject"] = value;
            }
        }

        public static List<int> ListTreeParents
        {
            get
            {
                return HttpContext.Current.Session["ListTreeParents"] == null ? new List<int>() : (List<int>)HttpContext.Current.Session["ListTreeParents"];
            }
            set
            {
                HttpContext.Current.Session["ListTreeParents"] = value;
            }
        }

        public static byte[] UserImage
        {
            get
            {
                return HttpContext.Current.Session["UserImage"] == null ? new byte[0] : (byte[])HttpContext.Current.Session["UserImage"];
            }
            set
            {
                HttpContext.Current.Session["UserImage"] = value;
            }
        }

        public static int MaxUploadFileSize
        {
            get
            {
                return HttpContext.Current.Session["MaxUploadFileSize"] == null ? 0 : HttpContext.Current.Session["MaxUploadFileSize"].ToString().ToType<int>();
            }
            set
            {
                HttpContext.Current.Session["MaxUploadFileSize"] = value;
            }
        }

        public static List<string> AllowedFileExtensions
        {
            get
            {
                return HttpContext.Current.Session["AllowedFileExtensions"] == null ? null : (List<string>)HttpContext.Current.Session["AllowedFileExtensions"];
            }
            set
            {
                HttpContext.Current.Session["AllowedFileExtensions"] = value;
            }
        }

        public static string TemplateDataFileUploadStream
        {
            get
            {
                return HttpContext.Current.Session["TemplateFileUpload"] == null ? null : (string)HttpContext.Current.Session["TemplateFileUpload"];
            }
            set
            {
                HttpContext.Current.Session["TemplateFileUpload"] = value;
            }
        }

        public static Guid? UserIdentifier
        {
            get
            {
                return (Guid?)HttpContext.Current.Session["UserIdentifier"];
            }
            set
            {
                HttpContext.Current.Session["UserIdentifier"] = value;
            }
        }

        public static Guid GroupIdentifier
        {
            get
            {
                return (Guid)HttpContext.Current.Session["GroupIdentifier"];
            }
            set
            {
                HttpContext.Current.Session["GroupIdentifier"] = value;
            }
        }

        public static AssociatedEntites Entity
        {
            get { return (AssociatedEntites) HttpContext.Current.Session["Entity"]; }
            set { HttpContext.Current.Session["Entity"] = value; }
        }

        public static AssociatedEntites Client
        {
            get
            {
                return HttpContext.Current.Session["Client"] != null
                    ? (AssociatedEntites) HttpContext.Current.Session["Client"]
                    : null;
            }
            set { HttpContext.Current.Session["Client"] = value; }
        }

        public static bool IsClientUser
        {
            get { return Client != null; }
        }

        public static Dictionary<string, string> ReportParameters
        {
            get
            {
                return (Dictionary<string, string>)HttpContext.Current.Session["ReportParameters"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["ReportParameters"] = value;
            }
        }

        public static string ReportPath
        {
            get
            {
                return (string)HttpContext.Current.Session["ReportPath"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["ReportPath"] = value;
            }
        }

        public static string ReportName
        {
            get
            {
                return (string)HttpContext.Current.Session["ReportName"] ?? null;
            }
            set
            {
                HttpContext.Current.Session["ReportName"] = value;
            }
        }

        public static string VersionNumber
        {
            get
            {
                return (string)HttpContext.Current.Session["VersionNumber"] ?? "";
            }
            set
            {
                HttpContext.Current.Session["VersionNumber"] = value;
            }
        }
    }
}