﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DBASystem.SMSF.Web.Common
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class UserAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (!SessionHelper.IsAuthenticated)
                return false;
            else
                return true;
        }
    }

    [AttributeUsage(AttributeTargets.Method)]
    public class OptionFilter : ActionFilterAttribute
    {
        public Option Option { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!CommonFunction.IsOptionAllowed(Option))
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    filterContext.Result = new JsonResult
                    {
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                        Data = new {status = "error", message = "Access is denied"}
                    };
                }
                else
                {
                    var lRoutes = new RouteValueDictionary(new {action = "AccessDenied", controller = "Error"});

                    filterContext.Controller.TempData["message"] = "AccessDenied";
                    filterContext.Result = new RedirectToRouteResult(lRoutes);
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}