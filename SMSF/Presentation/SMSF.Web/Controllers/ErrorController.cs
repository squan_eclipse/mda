﻿using DBASystem.SMSF.Contracts.Common;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    [AllowAnonymous]
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.StatusCode = 500;

            if (TempData["message"] != null)
                ViewBag.ErrorMessage = TempData["message"];

            return View("Error");
        }

        public ActionResult PageNotFound()
        {
            Utility.Logger.Error(string.Format("Error occured whilte trying to access page {0}, Page not found", Url.Content(Request["aspxerrorpath"])));

            ViewBag.StatusCode = 404;
            return View("Error");
        }

        public ActionResult AccessDenied()
        {
            ViewBag.StatusCode = 403;
            return View("Error");
        }

        public ActionResult WorkflowNotAvailable()
        {
            return View("WorkflowNotAvailable");
        }
    }
}
