﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class ActivityController : BaseController
    {
        [OptionFilter(Option = Option.GetActivities)]
        public ActionResult Index()
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = SessionHelper.EntityID }, Option.GetActivities);
            var responseModel = base.Execute(requestXml);
            var model = (List<ActivityCreateModel>)ModelDeserializer.DeserializeListFromXml<ActivityCreateModel>(responseModel.ModelXml);

            return View(model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.AddActivity)]
        public ActionResult AddActivity(ActivityCreateModel model)
        {
            if (model.Dispositions == null || model.Dispositions.Count == 0)
                return Json(new { status = "error", error = "Please add disposition for this activity" });

            model.EntityID = SessionHelper.EntityID;
            model.CreatedBy = SessionHelper.UserId;
            model.CreatedOn = DateTime.Now;

            var requestXml = base.GenerateRequestXml(model, Option.AddActivity);
            var responseModel = base.Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var newModel = (ActivityCreateModel)ModelDeserializer.DeserializeFromXml<ActivityCreateModel>(responseModel.ModelXml);
                return Json(new { status = "success", model = newModel });
            }
            else
                return Json(new { status = "error", error = responseModel.StatusMessage });
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateActivity)]
        public ActionResult UpdateActivity(ActivityCreateModel model)
        {
            System.Threading.Thread.Sleep(1000);
            if (model.Dispositions == null || model.Dispositions.Count == 0)
                return Json(new { status = "error", error = "Please add disposition for this activity" });

            model.EntityID = SessionHelper.EntityID;
            model.ModifiedBy = SessionHelper.UserId;
            model.ModifiedOn = DateTime.Now;

            var requestXml = base.GenerateRequestXml(model, Option.UpdateActivity);
            var responseModel = base.Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var newModel = (ActivityCreateModel)ModelDeserializer.DeserializeFromXml<ActivityCreateModel>(responseModel.ModelXml);
                return Json(new { status = "success", model = newModel });
            }
            else
                return Json(new { status = "error", error = responseModel.StatusMessage });
        }

        [HttpDelete]
        [OptionFilter(Option = Option.DeleteActivity)]
        public ActionResult DeleteActivity(int id)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, int> { Key = id, Value = SessionHelper.UserId }, Option.DeleteActivity);
            var responseModel = base.Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
                return Json(new { status = "success" });
            else
                return Json(new { status = "error", error = responseModel.StatusMessage });
        }
    }
}
