﻿using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    [UserAuthorize]
    public class InvestmentController : Controller
    {
        //
        // GET: /Investment/

        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Firms";

            ViewBag.EntityType = (int)DBASystem.SMSF.Contracts.Common.EntityTypes.IFM;
            ViewBag.ControllerName = "Firm";

            return View();
        }

        //
        // GET: /Investment/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Investment/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Investment/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Investment/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Investment/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Investment/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Investment/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
