﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class ReportsController : BaseController
    {
        //
        // GET: /Reports/

        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Reports";

            GetFirms();
            GetAccounts();
            GetFunds();
            GetAccountManagers();
            GetJobStatus();

            return View();
        }

        public ActionResult Report1(string id)
        {
            SessionHelper.SelectedMenu = "Reports";
            ViewBag.ReportTitle = id;
            return View();
        }

        public ActionResult ViewAccountsReport(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Accounts Management Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            reportParameters.Add("abn", !string.IsNullOrEmpty(form["ABN"]) ? form["ABN"] : null);
            reportParameters.Add("acn", !string.IsNullOrEmpty(form["ACN"]) ? form["ACN"] : null);
            reportParameters.Add("gst", !string.IsNullOrEmpty(form["GST"]) ? form["GST"] : null);
            reportParameters.Add("tfn", !string.IsNullOrEmpty(form["TFN"]) ? form["TFN"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/Accounts";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public ActionResult ViewJobDetialsReport(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Job Processing Details Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            reportParameters.Add("fundId", !string.Equals(form["Funds"], "Select Fund") ? form["Funds"] : null);
            reportParameters.Add("managerId", !string.Equals(form["AccountManagers"], "Select User") ? form["AccountManagers"] : null);
            reportParameters.Add("jobstatus", !string.Equals(form["JobStatus"], "Select Process State") ? form["JobStatus"] : null);
            reportParameters.Add("startdate", !string.IsNullOrEmpty(form["StartDate"]) ? form["StartDate"] : null);
            reportParameters.Add("enddate", !string.IsNullOrEmpty(form["EndDate"]) ? form["EndDate"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/JobProcessingDetails";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public ActionResult ViewJobSummaryReport(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Job Processing Summary Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            reportParameters.Add("fundId", !string.Equals(form["Funds"], "Select Fund") ? form["Funds"] : null);
            reportParameters.Add("jobstatus", !string.Equals(form["JobStatus"], "Select Process State") ? form["JobStatus"] : null);
            reportParameters.Add("startdate", !string.IsNullOrEmpty(form["StartDateS"]) ? form["StartDateS"] : null);
            reportParameters.Add("enddate", !string.IsNullOrEmpty(form["EndDateS"]) ? form["EndDateS"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/JobProcessingSummary";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public ActionResult ViewJobGraphReports(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Job Summary Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            //reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            //reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            //reportParameters.Add("fundId", !string.Equals(form["Funds"], "Select Fund") ? form["Funds"] : null);
            //reportParameters.Add("jobstatus", !string.Equals(form["JobStatus"], "Select Process State") ? form["JobStatus"] : null);
            //reportParameters.Add("startdate", !string.IsNullOrEmpty(form["StartDateS"]) ? form["StartDateS"] : null);
            //reportParameters.Add("enddate", !string.IsNullOrEmpty(form["EndDateS"]) ? form["EndDateS"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/SummaryReport";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public ActionResult ViewUsersManagementReport(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Users Management Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            reportParameters.Add("startdate", !string.IsNullOrEmpty(form["StartDateU"]) ? form["StartDateU"] : null);
            reportParameters.Add("enddate", !string.IsNullOrEmpty(form["EndDateU"]) ? form["EndDateU"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/UsersManagement";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public ActionResult ViewFundManagementReport(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Fund Management Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            reportParameters.Add("fundId", !string.Equals(form["Funds"], "Select Fund") ? form["Funds"] : null);
            reportParameters.Add("startdate", !string.IsNullOrEmpty(form["StartDateF"]) ? form["StartDateF"] : null);
            reportParameters.Add("enddate", !string.IsNullOrEmpty(form["EndDateF"]) ? form["EndDateF"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/FundManagement";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public ActionResult ViewAccountManagementReport(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Account Management Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            reportParameters.Add("startdate", !string.IsNullOrEmpty(form["StartDateA"]) ? form["StartDateA"] : null);
            reportParameters.Add("enddate", !string.IsNullOrEmpty(form["EndDateA"]) ? form["EndDateA"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/AccountManagement";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public ActionResult ViewFirmManagementReport(FormCollection form)
        {
            SessionHelper.SelectedMenu = "Reports";
            const string reportName = "Firm Management Report";
            ViewBag.ReportTitle = reportName;

            var reportParameters = new Dictionary<string, string>();
            reportParameters.Add("firmId", !string.Equals(form["Firms"], "Select Firm") ? form["Firms"] : null);
            reportParameters.Add("accountId", !string.Equals(form["Accounts"], "Select Account") ? form["Accounts"] : null);
            reportParameters.Add("fundId", !string.Equals(form["Funds"], "Select Fund") ? form["Funds"] : null);
            reportParameters.Add("startdate", !string.IsNullOrEmpty(form["StartDateFirm"]) ? form["StartDateFirm"] : null);
            reportParameters.Add("enddate", !string.IsNullOrEmpty(form["EndDateFirm"]) ? form["EndDateFirm"] : null);

            SessionHelper.ReportParameters = reportParameters;
            SessionHelper.ReportPath = "/DBAReporting/FirmManagement";
            SessionHelper.ReportName = reportName;

            return Redirect("../Reporting/ReportsViewer.aspx");
        }

        public string GetReportServerUrl()
        {
            var requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = SettingHeads.ReportServer }, Option.GetSetting);
            ResponseModel responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var reportServerModel = (ReportServerSettingModel)ModelDeserializer.DeserializeFromXml<ReportServerSettingModel>(responseModel.ModelXml);
                return reportServerModel.ReportServerUrl;
            }
            else
                return "";
        }

        public ActionResult AuditReports(int id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = SettingHeads.Sharepoint }, Option.GetSetting);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var sharepointSettings = (SharePointSettingModel)ModelDeserializer.DeserializeFromXml<SharePointSettingModel>(responseModel.ModelXml);
                ViewBag.JobId = id;
                return PartialView("_AuditReports", sharepointSettings.ReportTemplatePath);
            }
            return PartialView("_AuditReports", "");
        }

        public ActionResult GenrateAuditReports(string reportName, string folderPath, int jobId)
        {
            var requestXml = GenerateRequestXml(new AuditReportGenerateModel() { TemplateName = reportName, FolderPath = folderPath, JobId = jobId }, Option.AuditReports);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var reportFile = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(responseModel.ModelXml);
                var filedata = Convert.FromBase64String(reportFile.Value);
                var stream = new MemoryStream(filedata);
                var fileObject = File(stream.ToArray(), "application/octet-stream", reportName);
                return fileObject;
            }
            return View("Error");
        }

        #region Private Functions

        private void GetFirms()
        {
            var searchModel = new SearchFilterModel { UserID = SessionHelper.UserId, EntityType = EntityTypes.Entity };
            var requestXml = GenerateRequestXml(searchModel, Option.GetFirms);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<FirmModels>(responseModel.ModelXml);
            model = model.Where(p => p.CategoryID != (int)EntityCategories.System).Select(q => q).ToList();

            ViewBag.Firms = new SelectList(model, "ID", "Name");
        }

        private void GetAccounts()
        {
            var searchModel = new SearchFilterModel { UserID = SessionHelper.UserId, EntityType = EntityTypes.Client };
            var requestXml = GenerateRequestXml(searchModel, Option.GetAccounts);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<FirmModels>(responseModel.ModelXml);

            ViewBag.Accounts = new SelectList(model, "ID", "Name");
        }

        private void GetFunds()
        {
            var searchModel = new SearchFilterModel { UserID = SessionHelper.UserId, EntityType = EntityTypes.Fund };
            var requestXml = GenerateRequestXml(searchModel, Option.GetFunds);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<FirmModels>(responseModel.ModelXml);

            ViewBag.Funds = new SelectList(model, "ID", "Name");
        }

        private void GetAccountManagers()
        {
            var searchModel = new SearchFilterModel { UserID = SessionHelper.UserId, EntityType = EntityTypes.User };
            var requestXml = GenerateRequestXml(searchModel, Option.GetAccountManagers);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<AccountManagerViewModel>(responseModel.ModelXml);

            ViewBag.AccountManagers = new SelectList(model, "ID", "Name");
        }

        private void GetJobStatus()
        {
            var items = new List<SelectListItem>
                        {
                            new SelectListItem {Text = "Completed", Value = "2"},
                            new SelectListItem {Text = "In Progress", Value = "1"}
                        };

            ViewBag.JobStatus = items;
        }

        #endregion Private Functions
    }
}