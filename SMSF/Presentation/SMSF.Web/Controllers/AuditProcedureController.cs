﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DBASystem.SMSF.Web.Controllers
{
    public class AuditProcedureController : BaseController
    {
        #region AuditProcedure
        
        //[OptionFilter(Option = Option.GetAuditProcedureTabs)]
        public JsonResult GetAuditProcedureTabs(ManageWorkPaperModel manageWorkPaperModel)
        {
            //var requestXml = GenerateRequestXml(null, Option.GetAuditProcedureTabs);
            var requestXml = GenerateRequestXml(manageWorkPaperModel, Option.GetAuditProcedureTabs);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var models = ModelDeserializer.DeserializeListFromXml<AuditProcedureTabViewModel>(responseModel.ModelXml);
                models = models.OrderBy(a => a.TabOrder).ToList();
                return Json(models, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<AuditProcedureTabViewModel>(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAuditProcedureTabContents(ManageWorkPaperModel manageWorkPaperModel)
        {
            var requestXml = GenerateRequestXml(manageWorkPaperModel, Option.GetAuditProcedureTabContents);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var models = ModelDeserializer.DeserializeListFromXml<AuditProcedureTabContentViewModel>(responseModel.ModelXml);
                if (SessionHelper.IsClientUser)
                    models = models.Where(x => x.IsViewable).ToList();

                return Json(models, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<AuditProcedureTabContentViewModel>(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAuditProcedureTabContent(ManageWorkPaperModel manageWorkPaperModel)
        {
            var requestXml = GenerateRequestXml(manageWorkPaperModel, Option.GetAuditProcedureTabContent);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var models = ModelDeserializer.DeserializeListFromXml<AuditProcedureTabContentViewModel>(responseModel.ModelXml);
                models = models.OrderBy(a => a.Order).ToList();
                return Json(models, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<AuditProcedureTabContentViewModel>(), JsonRequestBehavior.AllowGet);
        }

        //[OptionFilter(Option = Option.GetAuditProcedures)]
        public ActionResult GetAuditProcedures(int jobId, int tabId)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> { Key = tabId, Value = jobId }, Option.GetAuditProcedures);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var models = ModelDeserializer.DeserializeFromXml<AuditProcedureTabContentViewModel>(responseModel.ModelXml);
                return Json(models, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<AuditProcedureTabContentViewModel>(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateAuditProcedureTabContent(AuditProcedureTabContentViewModel viewModel)
        {
            var requestXml = GenerateRequestXml(viewModel, Option.UpdateAuditProcedureTabContent);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region AuditProcedure Report & Summary

        [OptionFilter(Option = Option.GetProcedureTestSummary)]
        public ActionResult GetProcedureTestSummary(int jobId)
        {
            var model = new DictionaryModel<int, int> { Key = jobId };
            var requestXml = GenerateRequestXml(model, Option.GetProcedureTestSummary);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var models = ModelDeserializer.DeserializeListFromXml<TestSummaryViewModel>(responseModel.ModelXml);
                return Json(models, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<TestSummaryViewModel>(), JsonRequestBehavior.AllowGet);
        }

        [OptionFilter(Option = Option.UpdateAuditProcedureAnswer)]
        public string SaveTestSummary(TestSummaryAnswerViewModel model)
        {
            var requestXml = GenerateRequestXml(model, Option.SaveTestSummary);
            var responseModel = Execute(requestXml);

            if (responseModel.Status != ResponseStatus.Success)
            {
                Response.StatusCode = 500;
            }
            return responseModel.StatusMessage;
        }

        public ActionResult GetInitialAuditQuries(int jobId)
        {
            //For Comments by JobID
            var dicModel = new DictionaryModel<int, int> { Key = jobId, Value = 0 };
            var requestXml = GenerateRequestXml(dicModel, Option.GetProcedureComments);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var models = ModelDeserializer.DeserializeListFromXml<AuditProcedureCommentViewModel>(responseModel.ModelXml);
                return Json(models, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<AuditProcedureCommentViewModel>(), JsonRequestBehavior.AllowGet);
        }

        [OptionFilter(Option = Option.UpdateAuditProcedureAnswer)]
        public string SaveAuditProcess(AuditProcedureAnswerViewModel model)
        {
            var requestXml = GenerateRequestXml(model, Option.UpdateAuditProcedureAnswer);
            var responseModel = Execute(requestXml);

            if (responseModel.Status != ResponseStatus.Success)
            {
                Response.StatusCode = 500;
            }
            return responseModel.StatusMessage;
        }

        #endregion

        #region AuditProcedure - Popups
        public ActionResult GetProcedureComments(int jobId, int procedureId)
        {
            var model = new DictionaryModel<int, int> { Key = jobId, Value = procedureId };
            var requestXml = GenerateRequestXml(model, Option.GetProcedureComments);
            var responseModel = Execute(requestXml);

            List<AuditProcedureCommentViewModel> models = null;
            if (responseModel.Status == ResponseStatus.Success)
            {
                models = ModelDeserializer.DeserializeListFromXml<AuditProcedureCommentViewModel>(responseModel.ModelXml);
            }

            ViewBag.JobID = jobId;
            ViewBag.AuditProcedureID = procedureId;

            return PartialView("~/Views/Job/AuditProcedurePopups/_ProcedureComments.cshtml", models);
        }

        [OptionFilter(Option = Option.GetProcedureOmlForManagerReview)]
        public ActionResult GetProcedureOmlForManagerReview(int jobId)
        {
            var dictionaryModel = new DictionaryModel<int, int> { Key = jobId };
            var requestXml = GenerateRequestXml(dictionaryModel, Option.GetProcedureOmlForManagerReview);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                List<AuditProcedureOmlViewModel> models = ModelDeserializer.DeserializeListFromXml<AuditProcedureOmlViewModel>(responseModel.ModelXml);
                return Json(models, JsonRequestBehavior.AllowGet);
            }
            return Json(new List<AuditProcedureOmlViewModel>(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetProcedureOml(int jobId, int procedureId)
        {
            var model = new DictionaryModel<int, int> { Key = jobId, Value = procedureId };
            var requestXml = GenerateRequestXml(model, Option.GetProcedureOml);
            var responseModel = Execute(requestXml);

            List<AuditProcedureOmlViewModel> models = null;
            if (responseModel.Status == ResponseStatus.Success)
            {
                models = ModelDeserializer.DeserializeListFromXml<AuditProcedureOmlViewModel>(responseModel.ModelXml);
            }

            string json = new JavaScriptSerializer().Serialize(Json(models).Data);
            ViewBag.JobId = jobId;
            ViewBag.AuditProcedureId = procedureId;
            ViewBag.OMLsJson = json;

            return PartialView("~/Views/Job/AuditProcedurePopups/_ProcedureOML.cshtml");

        }

        [OptionFilter(Option = Option.AddProcedureComments)]
        public void SaveProcedureComments(AuditProcedureCommentViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedBy = SessionHelper.UserId;
                var requestXml = GenerateRequestXml(model, Option.AddProcedureComments);
                var responseModel = Execute(requestXml);

                if (responseModel.Status != ResponseStatus.Success)
                {
                    Response.StatusCode = 500;
                }
            }
        }

        [OptionFilter(Option = Option.AddEditProcedureOml)]
        public void SaveProcedureOml(AuditProcedureOmlViewModel model)
        {
            if (ModelState.IsValid)
            {
                var requestXml = GenerateRequestXml(model, Option.AddEditProcedureOml);
                var responseModel = Execute(requestXml);

                if (responseModel.Status != ResponseStatus.Success)
                {
                    Response.StatusCode = 500;
                }
            }
        }

        #endregion

        #region Workpaper Attachments

        public ActionResult GetProcedureAttachments(int jobId, int procedureId)
        {
            ViewBag.JobId = jobId;
            ViewBag.ProcedureID = procedureId;

            return PartialView("~/Views/Job/AuditProcedurePopups/_ProcedureAttachments.cshtml");
        }
        public ActionResult DownloadFile(int attachmentDetailId)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = attachmentDetailId }, Option.DownloadAttachmentFile);
            var responseModel = Execute(requestXml);
            var file = (DictionaryModel<string, string>)ModelDeserializer.DeserializeFromXml<DictionaryModel<string, string>>(responseModel.ModelXml);

            var filedata = Convert.FromBase64String(file.Value);
            var stream = new MemoryStream(filedata);

            return File(stream.ToArray(), "application/octet-stream", file.Key);
        }
        public void DeleteFile(int attachmentDetailId)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = attachmentDetailId }, Option.DeleteAuditProcedureAttachmentFile);
            Execute(requestXml);
        }
        public JsonResult GetFiles(int jobId, int procedureId)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> { Key = jobId, Value = procedureId }, Option.GetAuditProcedureAttachments);
            var responseModel = Execute(requestXml);
            var models = (AuditProcedureAttachmentViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureAttachmentViewModel>(responseModel.ModelXml);

            if(models != null)
            { 
                var publicUrlExipry = System.Configuration.ConfigurationManager.AppSettings["SharePointPublicUrlExpiry"].ToType<int>();
                models.Files = models.Files.Select(x => { x.Document.PublicUrlExipry = publicUrlExipry; return x; }).ToList();
            }
            return Json(models, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Save(List<HttpPostedFileBase> files, int jobId, int procedureId)
        {
            if (SessionHelper.MaxUploadFileSize == 0 || SessionHelper.AllowedFileExtensions == null)
            {
                var requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = SettingHeads.Sharepoint }, Option.GetSetting);
                var responseModel = Execute(requestXml);
                if (responseModel.Status == ResponseStatus.Success)
                {
                    var sharePointSetting = ((SharePointSettingModel)ModelDeserializer.DeserializeFromXml<SharePointSettingModel>(responseModel.ModelXml));
                    SessionHelper.MaxUploadFileSize = sharePointSetting.MaximumFileSize;
                    SessionHelper.AllowedFileExtensions = sharePointSetting.AllowedFileTypeList;
                }
            }
            var model = new AuditProcedureAttachmentViewModel { JobId = jobId, AuditProcedureId = procedureId };

            var filesUploadFailed = new List<string>();
            foreach (var file in files)
            {
                byte[] data;
                using (var stream = file.InputStream)
                {
                    var ms = stream as MemoryStream;
                    if (ms == null)
                    {
                        ms = new MemoryStream();
                        stream.CopyTo(ms);
                    }
                    data = ms.ToArray();
                }

                var extensionAllowed = SessionHelper.AllowedFileExtensions.Any(p => p.Contains(Path.GetExtension(file.FileName)) || p.Contains(Path.GetExtension(file.FileName).Replace(".", "")));

                if (((data.Length / 1024) / 1024) <= SessionHelper.MaxUploadFileSize && extensionAllowed)
                    model.Files.Add(new AttachmentInfo
                    {
                        Document = new DocumentModel() {Name = file.FileName},
                        FileData = Convert.ToBase64String(data)
                    });

                else if (((data.Length / 1024) / 1024) > SessionHelper.MaxUploadFileSize)
                    filesUploadFailed.Add(file.FileName + "<span class=globalerror>[File size exceed.]</span><br/>");
                else if (!extensionAllowed)
                    filesUploadFailed.Add(file.FileName + "<span class=globalerror>[File type not allowed.]</span><br/>");
            }

            var request = GenerateRequestXml(model, Option.AddEditAuditProcedureAttachments);
            var response = Execute(request);

            if (filesUploadFailed.Count == 0)
                Response.StatusCode = (int)HttpStatusCode.OK;
            else
                Response.StatusCode = (int)HttpStatusCode.NotAcceptable;

            if (response.Status != ResponseStatus.Success)
            {
                return Json(new { status = false, fileName = "", message = response.StatusMessage });
            }
            return
                Json(
                    new
                    {
                        status = filesUploadFailed.Count == 0,
                        fileName = string.Join(",", filesUploadFailed),
                        message = string.Empty
                    });
        }

        #endregion

        #region Manage AuditProc

        public ActionResult GetAuditProcedureEditContents(int id, string mode)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetAuditProcedureEditContents);
            var responseModel = Execute(requestXml);
            
            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (AuditProcedureViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureViewModel>(responseModel.ModelXml);
                
                if (model.AnswerControlType != null)
                    GetAnswerControl((int)model.AnswerControlType);
                else
                    GetAnswerControl(0);

                if (mode == "Add")
                {
                    BindOptionDetailsList(null, model);
                    return PartialView("~/Views/AuditProcedure/Manage/_AuditProcedureAddContents.cshtml", model);
                }
                else
                {
                    BindOptionDetailsList(id, model);
                    return PartialView("~/Views/AuditProcedure/Manage/_AuditProcedureEditContents.cshtml", model);
                }

            }
            return PartialView("~/Views/AuditProcedure/Manage/_AuditProcedureEditContents.cshtml", null);
        }

        public ActionResult GetAuditProcedureAddContents(int tabContentid)
        {
            var model = new AuditProcedureViewModel {AuditProcedureTabContentId = tabContentid};
            BindOptionDetailsList(null, model);

            if (model.AnswerControlType != null)
                GetAnswerControl((int) model.AnswerControlType);
            else
                GetAnswerControl(0);

            return PartialView("~/Views/AuditProcedure/Manage/_AuditProcedureAddContents.cshtml", model);
        }

        public JsonResult UpdateAuditProcedureTab(AuditProcedureTabViewModel viewModel)
        {
            var requestXml = GenerateRequestXml(viewModel, Option.UpdateAuditProcedureTab);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteAuditProcedureTab(ManageWorkPaperModel manageWorkPaperModel)
        {
            var requestXml = GenerateRequestXml(manageWorkPaperModel, Option.DeleteAuditProcedureTab);
            var responseModel = Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);

        }
        public JsonResult DeleteAuditProcedureTabContent(ManageWorkPaperModel manageWorkPaperModel)
        {
            var requestXml = GenerateRequestXml(manageWorkPaperModel, Option.DeleteAuditProcedureTabContent);
            var responseModel = Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);

        }
        public JsonResult AddAuditProcedureTab(AuditProcedureTabViewModel viewModel)
        {
            var requestXml = GenerateRequestXml(viewModel, Option.AddAuditProcedureTab);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddAuditProcedureTabContent(AuditProcedureTabContentViewModel viewModel)
        {
            var requestXml = GenerateRequestXml(viewModel, Option.AddAuditProcedureTabContent);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        //[OptionFilter(Option = Option.UpdateAuditProcedureEditContents)]
        public JsonResult UpdateAuditProcedureEditContents(AuditProcedureViewModel viewModel)
        {
            var requestXml = GenerateRequestXml(viewModel, Option.UpdateAuditProcedureEditContents);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveWorkpaperTemplate(WorkpaperTemplateModel workpaperTemplateModel)
        {
            workpaperTemplateModel.CreatedBy = SessionHelper.UserId;
            var requestXml = GenerateRequestXml(workpaperTemplateModel, Option.SaveWorkpaperTemplate);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult SaveWorkpaperTemplateAsXml(WorkpaperTemplateModel workpaperTemplateModel)
        {
            workpaperTemplateModel.FileName = workpaperTemplateModel.FileName.Contains(".xml") ? workpaperTemplateModel.FileName : workpaperTemplateModel.FileName + ".xml";
            var requestXml = GenerateRequestXml(workpaperTemplateModel, Option.SaveWorkpaperTemplateAsXML);
            var responseModel = Execute(requestXml);

            var file = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(responseModel.ModelXml);
            var filedata = Convert.FromBase64String(file.Value);
            var stream = new MemoryStream(filedata);
            return File(stream.ToArray(), "application/xml", workpaperTemplateModel.FileName);
        }

        public JsonResult UploadWorkpaperTemplate(HttpPostedFileBase file, string folderpath)
        {
            var startTime = DateTime.Now;

            string fileUploadFailed = "";
            var extensionAllowed = string.Equals(Path.GetExtension(file.FileName).ToUpper(), ".XML");
            
            if (!extensionAllowed)
            {
                fileUploadFailed = JsonConvert.SerializeObject(new { file = file.FileName, message = "File type not allowed, Please upload template with '.xml' extension" });
            }
            else
            {
                var fileName = Path.GetFileName(file.FileName);
                var filePath = Path.Combine(Server.MapPath("~/Templates"), fileName);
                file.SaveAs(filePath);

                var requestXml = GenerateRequestXml(new DictionaryModel<string, string> { Key = filePath, Value = folderpath }, Option.LoadWorkpaperTemplateFromXml);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    var model =
                        (GenericModel<bool>)
                            ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                    return Json(new { model, Time = DateTime.Now.Subtract(startTime).ToString(@"hh\:mm\:ss") }, JsonRequestBehavior.AllowGet);
                }
                else if (responseModel.Status == ResponseStatus.Warning)
                {
                    fileUploadFailed = JsonConvert.SerializeObject(new { file = file.FileName, message = responseModel.ResponseMessage });
                    Response.StatusCode = (int)HttpStatusCode.NotAcceptable;
                    return Json(new { status = "error", fileuploadfailed = fileUploadFailed, Time = DateTime.Now.Subtract(startTime).ToString(@"hh\:mm\:ss") });
                }
                return Json(null, JsonRequestBehavior.AllowGet);
            }

            Response.StatusCode = (int)HttpStatusCode.NotAcceptable;
            return Json(new { status = "error", fileuploadfailed = fileUploadFailed, Time = DateTime.Now.Subtract(startTime).ToString(@"hh\:mm\:ss") });
        }

        public JsonResult LoadWorkpaperTemplate(WorkpaperTemplateModel workpaperTemplateModel)
        {
            var requestXml = GenerateRequestXml(workpaperTemplateModel, Option.LoadWorkpaperTemplate);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //[OptionFilter(Option = Option.UpdateAuditProcedureEditContents)]
        public JsonResult AddAuditProcedure(AuditProcedureViewModel viewModel)
        {
            var requestXml = GenerateRequestXml(viewModel, Option.AddAuditProcedure);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OptionFilter(Option = Option.DeleteFirm)]
        public JsonResult DeleteAuditProcedure(int id)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> { Key = id, Value = SessionHelper.UserId }, Option.DeleteAuditProcedure);
            var responseModel = Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);

        }


        #region Private Functions

        private void GetAnswerControl(int selectedValue)
        {
            var answerControlTypeList = Enum.GetValues(typeof (AnswerControlType)).Cast<object>().ToDictionary(item => (int) item, item => item.ToString());
            answerControlTypeList.Remove(2); //Remove TextBox Option
            ViewBag.AnswerControlTypeList = selectedValue != 0 ? new SelectList(answerControlTypeList, "Key", "Value", selectedValue) : new SelectList(answerControlTypeList, "Key", "Value");
        }

        private void BindOptionDetailsList(int? id, AuditProcedureViewModel viewModel)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int?> { Value = id }, Option.GetAuditProcedureOptionDetail);
            var responseModel = Execute(requestXml);
            var model = (List<AuditProcedureOptionDetailViewModel>)ModelDeserializer.DeserializeListFromXml<AuditProcedureOptionDetailViewModel>(responseModel.ModelXml);

            viewModel.AuditProcedureOptionDetail = model.ToList();
            if (viewModel.AuditProcedureOption != null && id != null)
                viewModel.AuditProcedureOptionDetailSelected = model.Where(x => viewModel.AuditProcedureOption.Any(a => a.AuditProcedureOptionDetailId == x.Id)).ToList();
        }

        #endregion

        #endregion
    }
}
