﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace DBASystem.SMSF.Web.Controllers
{
    public class AccountsController : BaseController
    {
        public ActionResult HasMultipleAccess()
        {
            var requestXml =
                GenerateRequestXml(
                    new DictionaryModel<int, EntityTypes> { Key = SessionHelper.UserId, Value = EntityTypes.Client },
                    Option.HasMultipleAccess);
            var responseModel = Execute(requestXml);
            var models = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

            return models.Value == 0
                ? RedirectToAction("Index")
                : RedirectToAction("Detail", new {id = Utility.EncryptQueryString(models.Value)});
        }

        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Accounts";
            ViewBag.ControllerName = "Accounts";
            ViewBag.EntityType = (int)EntityTypes.Client;

            if (TempData["serachData"] != null)
            {
                var model = (SearchFilterModel)TempData["serachData"];

                ViewBag.Firm = string.IsNullOrEmpty(model.Firm) ? "" : model.Firm;
                ViewBag.Account = string.IsNullOrEmpty(model.Name) ? "" : model.Name;
                ViewBag.ABN = string.IsNullOrEmpty(model.ABN) ? "" : model.ABN;
                ViewBag.AccountManager = string.IsNullOrEmpty(model.AccountManager) ? "" : model.AccountManager;
                ViewBag.PrimaryContact = string.IsNullOrEmpty(model.PrimaryContact) ? "" : model.PrimaryContact;
                ViewBag.DateFrom = model.DateFrom == null ? "" : Convert.ToDateTime(model.DateFrom).ToShortDateString();
                ViewBag.DateTo = model.DateTo == null ? "" : Convert.ToDateTime(model.DateTo).ToShortDateString();
            }
            else
            {
                ViewBag.Firm = "";
                ViewBag.Account = "";
                ViewBag.ABN = "";
                ViewBag.AccountManager = "";
                ViewBag.PrimaryContact = "";
                ViewBag.DateFrom = "";
                ViewBag.DateTo = "";
            }

            return View();
        }

        public ActionResult Detail(string id)
        {
            id = Utility.DecryptQueryString(id);
            SessionHelper.SelectedMenu = "Accounts";

            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(id) }, Option.GetAccountDetail);
            var responseModel = Execute(requestXml);
            var model = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);

            return AccountDetailView(model);
        }

        private ActionResult AccountDetailView(FirmDetailModels model)
        {
            return View(@"~/Views/Firm/Detail.cshtml", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.AddAccount)]
        public ActionResult Create(string pId)
        {
            ViewBag.EncryptedID = pId;
            pId = Utility.DecryptQueryString(pId);

            SessionHelper.SelectedMenu = "Accounts";
            SessionHelper.ListAddresses = null;
            SessionHelper.ListUsers = null;

            var model = new FirmCreateModel { ParentID = Convert.ToInt32(pId), EntityType = EntityTypes.Client };
            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.AddAccount)]
        public ActionResult CreateAccount(FirmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                model.ListUser = SessionHelper.ListUsers;
                model.ListAddress = SessionHelper.ListAddresses;

                var requestXml = GenerateRequestXml(model, Option.AddAccount);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var detailModel = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);
                    if (responseModel.Status == ResponseStatus.Warning)
                    {
                        ViewBag.WarningMessage = responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage + ",however " + model.EntityType.ToString() + " has been created successfully , please contact your administrator."; ;
                    }
                    ViewBag.BackURL = Url.Content("~/Accounts/Detail/" + model.ParentID);
                    return RedirectToAction("Detail", new { id = Utility.EncryptQueryString(detailModel.ID) });
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(@"~/Views/Firm/Create.cshtml", model);
                }
            }

            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateAccount)]
        public ActionResult UpdateAccount(FirmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = SessionHelper.UserId;
                model.ListUser = SessionHelper.ListUsers;
                model.ListAddress = SessionHelper.ListAddresses;

                var requestXml = GenerateRequestXml(model, Option.UpdateAccount);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var reponseModel = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);
                    if (responseModel.Status == ResponseStatus.Warning)
                    {
                        ViewBag.WarningMessage = responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage + ",however " + model.EntityType.ToString() + " has been updated successfully , please contact your administrator."; ;
                    }

                    return AccountDetailView(reponseModel);
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(@"~/Views/Firm/Create.cshtml", model);
                }
            }
            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.UpdateAccount)]
        public ActionResult EditAccount(string accountId)
        {
            ViewBag.EncryptedID = accountId;
            accountId = Utility.DecryptQueryString(accountId);

            SessionHelper.ListAddresses = null;
            SessionHelper.ListUsers = null;
            SessionHelper.SelectedMenu = "Accounts";
            SessionHelper.ListAddresses = null;
            SessionHelper.ListUsers = null;

            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(accountId) }, Option.GetFirm);
            var responseModel = Execute(requestXml);
            var model = (FirmCreateModel)ModelDeserializer.DeserializeFromXml<FirmCreateModel>(responseModel.ModelXml);

            //if (model.EntityType == EntityTypes.Entity)
            if (model.EntityType == EntityTypes.Entity || model.EntityType == EntityTypes.Client)
                SessionHelper.ListAddresses = model.ListAddress;

            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        public JsonResult IndexSearch(SearchFilterModel searchModel)
        {
            searchModel.UserID = SessionHelper.UserId;
            searchModel.EntityType = EntityTypes.Client;

            var requestXml = GenerateRequestXml(searchModel, Option.GetAccounts);
            var responseModel = Execute(requestXml);
            var model = (List<FirmModels>)ModelDeserializer.DeserializeListFromXml<FirmModels>(responseModel.ModelXml);

            return
                Json(
                    new
                    {
                        data = model,
                        message = responseModel.Status == ResponseStatus.Success ? "" : responseModel.ResponseMessage
                    });
        }

        public ActionResult IndexSearchWithFilter(string searchModelstring)
        {
            var searchModel = JsonConvert.DeserializeObject<SearchFilterModel>(searchModelstring, new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy" });
            TempData["serachData"] = searchModel;

            return RedirectToAction("Index");
        }
    }
}