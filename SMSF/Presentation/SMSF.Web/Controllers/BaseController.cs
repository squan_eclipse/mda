﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.Common.Encryption;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Services.SMSFService;
using DBASystem.SMSF.Web.Common;
using System;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    [UserAuthorize]
    [CustomHandleError]
    public abstract class BaseController : Controller
    {
        protected ResponseModel Execute(string requestXml)
        {
            //Encrypting Message Before Send Message
            requestXml = AesEncryption.Encrypt(requestXml, true);   

            var responseXml = new SMSFServiceClient().Execute(requestXml);

            //Decryting the Response XML
            responseXml = AesEncryption.Decrypt(responseXml, true);

            var model = ModelDeserializer.ParseResponseXml(responseXml);

            if (model.Status == ResponseStatus.Error)
            {
                throw new SMSFServiceException(model.ResponseMessage);
            }
            else if (model.Status == ResponseStatus.Failure && model.ResponseMessage == Resource_EN.Error_AccessDenied)
            {
                var e = new UnauthorizedAccessException(model.ResponseMessage);
                throw e;
            }

            return model;
        }

        protected string GenerateRequestXml(IModel model, Option option)
        { 
            return ModelSerializer.GenerateRequestXml(model, option, SessionHelper.UserId, SessionHelper.AuthCode);
        }
    }
}
