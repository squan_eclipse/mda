﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class SettingController : BaseController
    {
        public ActionResult Index(string Id)
        {
            ViewBag.IsPrimaryAccountManager = GetUserProfile(SessionHelper.UserId).IsPrimaryAccountManager;
            SessionHelper.SelectedMenu = "Settings";

            return View();
        }

        public ActionResult UserProfile()
        {
            ViewBag.IsPrimaryAccountManager = GetUserProfile(SessionHelper.UserId).IsPrimaryAccountManager;
            ViewBag.InitialTab = SettingHeads.UserProfile.ToString();
            SessionHelper.SelectedMenu = "Settings";
            return View("Index");
        }

        public ActionResult SystemSettings()
        {
            ViewBag.InitialTab = (SessionHelper.Group == UserGroup.Clients || SessionHelper.Group == UserGroup.Operators)
                ? SettingHeads.UserProfile.ToString()
                : SettingHeads.System.ToString();

            ViewBag.IsPrimaryAccountManager = GetUserProfile(SessionHelper.UserId).IsPrimaryAccountManager;
            SessionHelper.SelectedMenu = "Settings";
            return View("Index");
        }

        public ActionResult UserGroups()
        {
            return View();
        }

        public void GetAccounts()
        {
            var searchModel = new SearchFilterModel { UserID = SessionHelper.UserId, EntityType = EntityTypes.Client };
            var requestXml = GenerateRequestXml(searchModel, Option.GetAccounts);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<FirmModels>(responseModel.ModelXml);

            ViewBag.ClientAccounts = new SelectList(model, "ID", "Name");
        }

        public ActionResult GetSettings(SettingHeads settingHead)
        {
            var requestXml = "";
            ResponseModel responseModel;
            GetAccounts();
            switch (settingHead)
            {
                case SettingHeads.System:

                    requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = settingHead }, Option.GetSetting);
                    responseModel = Execute(requestXml);
                    if (responseModel.Status == ResponseStatus.Success)
                    {
                        var systemSettings = (SystemSettingsModel)ModelDeserializer.DeserializeFromXml<SystemSettingsModel>(responseModel.ModelXml);
                        return PartialView("_SystemSettings", systemSettings);
                    }
                    break;

                case SettingHeads.Sharepoint:

                    requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = settingHead }, Option.GetSetting);
                    responseModel = Execute(requestXml);
                    if (responseModel.Status == ResponseStatus.Success)
                    {
                        var sharepointSettings = (SharePointSettingModel)ModelDeserializer.DeserializeFromXml<SharePointSettingModel>(responseModel.ModelXml);
                        return PartialView("_SharepointSetting", sharepointSettings);
                    }
                    break;

                case SettingHeads.UserProfile:

                    var model = GetUserProfile(SessionHelper.UserId);
                    if (model.IsPrimaryAccountManager)
                        SessionHelper.ListAddresses = model.ListAddress;

                    SessionHelper.ListContacts = model.ListContact;

                    model.PasswordPolicy.ValidationExpression = "/" + model.PasswordPolicy.ValidationExpression + "/";
                    model.ListAddress = null;
                    return PartialView("_UserProfile", model);

                case SettingHeads.Enittes:
                    var objModel = GetUserProfile(SessionHelper.UserId);
                    if (objModel.IsPrimaryAccountManager)
                        SessionHelper.ListAddresses = objModel.ListAddress;

                    objModel.ListAddress = null;
                    return PartialView("_EntitySettings");

                case SettingHeads.Logs:
                    return PartialView("_Logs");

                case SettingHeads.DefaultDocuments:
                    requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = SettingHeads.Sharepoint }, Option.GetSetting);
                    responseModel = Execute(requestXml);
                    if (responseModel.Status == ResponseStatus.Success)
                    {
                        var sharepointSettings = (SharePointSettingModel)ModelDeserializer.DeserializeFromXml<SharePointSettingModel>(responseModel.ModelXml);
                        ViewBag.JobDefaultDocumentPath = sharepointSettings.JobDefaultDocumentsPath;
                    }

                    return PartialView("_DefaultDocuments");

                case SettingHeads.ReportsTemplates:
                    requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = SettingHeads.Sharepoint }, Option.GetSetting);
                    responseModel = Execute(requestXml);
                    if (responseModel.Status == ResponseStatus.Success)
                    {
                        var sharepointSettings = (SharePointSettingModel)ModelDeserializer.DeserializeFromXml<SharePointSettingModel>(responseModel.ModelXml);
                        ViewBag.ReportTemplatePath = sharepointSettings.ReportTemplatePath;
                    }

                    return PartialView("_FinalReportTemplates");
                case SettingHeads.ReportServer:
                    requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = settingHead }, Option.GetSetting);
                    responseModel = Execute(requestXml);
                    if (responseModel.Status == ResponseStatus.Success)
                    {
                        var reportServerModel = (ReportServerSettingModel)ModelDeserializer.DeserializeFromXml<ReportServerSettingModel>(responseModel.ModelXml);
                        return PartialView("_ReportServer", reportServerModel);
                    }
                    break;

                case SettingHeads.Groups:
                    requestXml = base.GenerateRequestXml(null, Option.GetUserGroups);
                    responseModel = base.Execute(requestXml);
                    var groups = (List<UserGroupModel>)ModelDeserializer.DeserializeListFromXml<UserGroupModel>(responseModel.ModelXml);
                    return PartialView("_UserGroups", groups);

                case SettingHeads.Roles:
                    return PartialView("_UserRolesList", GetRoles());
            }

            return View("Index");
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateEmailSettings)]
        public ActionResult UpdateEmailSettings(EmailSettingsModel model)
        {
            model.ModifiedBy = SessionHelper.UserId;
            var requestXml = GenerateRequestXml(new GenericModel<EmailSettingsModel> { Value = model }, Option.UpdateEmailSettings);
            var responseModel = Execute(requestXml);

            return Json(new { success = responseModel.Status == ResponseStatus.Success });
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdatePasswordSettings)]
        public ActionResult UpdatePasswordSettings(PasswordSettingsModel model)
        {
            model.ModifiedBy = SessionHelper.UserId;
            var requestXml = GenerateRequestXml(new GenericModel<PasswordSettingsModel> { Value = model }, Option.UpdatePasswordSettings);
            var responseModel = Execute(requestXml);

            return Json(new { success = responseModel.Status == ResponseStatus.Success });
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateSessionSettings)]
        public ActionResult UpdateSessionSettings(SessionSettingModel model)
        {
            model.ModifiedBy = SessionHelper.UserId;
            var requestXml = GenerateRequestXml(new GenericModel<SessionSettingModel> { Value = model }, Option.UpdateSessionSettings);
            var responseModel = Execute(requestXml);

            return Json(new { success = responseModel.Status == ResponseStatus.Success });
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateSharepointSetting)]
        public ActionResult UpdateSharePointSetting(SharePointSettingModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedBy = SessionHelper.UserId;
                var requestXml = GenerateRequestXml(new GenericModel<SharePointSettingModel> { Value = model }, Option.UpdateSharepointSetting);
                var responseModel = Execute(requestXml);
                if (responseModel.Status == ResponseStatus.Success)
                {
                    SessionHelper.MaxUploadFileSize = model.MaximumFileSize;
                    SessionHelper.AllowedFileExtensions = model.AllowedFileTypeList;
                }
                return Json(new { success = responseModel.Status == ResponseStatus.Success });
            }
            return Json(new { success = false });
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateVersionSettings)]
        public ActionResult UpdateVersionSettings(VersionSettingModel model)
         {
            model.ModifiedBy = SessionHelper.UserId;
            model.ModifiedOn = DateTime.Now;

            var requestXml = GenerateRequestXml(new GenericModel<VersionSettingModel> { Value = model }, Option.UpdateVersionSettings);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                SessionHelper.VersionNumber = model.VersionNumber;
                return Json(new { success = responseModel.Status == ResponseStatus.Success });
            }

            return Json(new { success = false });
        }

        public JsonResult GetNotifications(NotificationTypes? type, int page, int count, int days)
        {
            var requestXml = GenerateRequestXml(new NotificationGetModel { Type = type, UserID = SessionHelper.UserId, Count = count, Days = days, IsSystem = false, Page = page }, Option.GetNotifications);
            var responseModel = Execute(requestXml);
            var model = (NotificationModel)ModelDeserializer.DeserializeFromXml<NotificationModel>(responseModel.ModelXml);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpDelete]
        public void DeleteNotifications(int? Id, int? days)
        {
            var requestXml = GenerateRequestXml(new NotificationDeleteModel { ID = Id, Days = days, EntityID = (Id == null ? SessionHelper.EntityID : (int?)null) }, Option.DeleteNotifications);
            var responseModel = Execute(requestXml);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateReportServerSettings)]
        public ActionResult UpdateReportServerSettings(ReportServerSettingModel model)
        {
            try
            {
                model.ModifiedBy = SessionHelper.UserId;
                if (!model.SetPassword)
                    model.Password = null;

                var requestXml = GenerateRequestXml(model, Option.UpdateReportServerSettings);
                var responseModel = Execute(requestXml);

                return Json(new { status = "success" });
            }
            catch (Exception e)
            {
                return Json(new { status = "error", message = e.Message });
            }
        }

        #region Group

        [HttpGet]
        [OptionFilter(Option = Option.GetUserGroups)]
        public List<UserGroupModel> GetGroups()
        {
            var requestXml = base.GenerateRequestXml(null, Option.GetUserGroups);
            var responseModel = base.Execute(requestXml);

            var groups = (List<UserGroupModel>)ModelDeserializer.DeserializeListFromXml<UserGroupModel>(responseModel.ModelXml);
            return groups;
        }

        [HttpGet]
        public ActionResult GetGroupList()
        {
            return PartialView("_ListGroups", GetGroups());
        }

        [HttpGet]
        public ActionResult CreateGroup(int? id)
        {
            if (id == null)
                return PartialView("_CreateGroup", new UserGroupModel());
            else
            {
                var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = (int)id }, Option.GetUserGroup);
                var responseModel = base.Execute(requestXml);
                var model = (UserGroupModel)ModelDeserializer.DeserializeFromXml<UserGroupModel>(responseModel.ModelXml);
                return PartialView("_CreateGroup", model);
            }
        }

        [HttpPost]
        public ActionResult CreateGroup(UserGroupModel model)
        {
            string requestXml;

            if (model.ID == 0)
            {
                model.Status = Status.Active;
                requestXml = base.GenerateRequestXml(model, Option.CreateUserGroup);
            }
            else
                requestXml = base.GenerateRequestXml(model, Option.UpdateUserGroup);

            var responseModel = base.Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
                return Json("success");
            else
                return Json(responseModel.StatusMessage);
        }

        [HttpPost]
        public void ChangeGroupStatus(int id, Status status)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, Status> { Key = id, Value = status }, Option.ChangeUserGroupStatus);
            var responseModel = base.Execute(requestXml);
        }

        #endregion Group

        #region Group Roles

        public ActionResult ListGroupRoles(int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetUserGroupRoles);
            var responseModel = base.Execute(requestXml);
            var model = (UserGroupRolesModel)ModelDeserializer.DeserializeFromXml<UserGroupRolesModel>(responseModel.ModelXml);

            return PartialView("_ListGroupRoles", model);
        }

        public JsonResult AddUserGroupRoles(int groupId, int[] roles)
        {
            var model = new UserGroupRolesModel
            {
                ID = groupId,
                Roles = roles != null && roles.Any() ? roles.Select(a => new RoleModel { ID = a, Status = Status.Active }).ToList() : null
            };

            var requestXml = base.GenerateRequestXml(model, Option.AddUserGroupRoles);
            var responseModel = base.Execute(requestXml);

            return Json(responseModel.StatusMessage.ToString());
        }

        #endregion Group Roles

        #region Role

        public List<RoleModel> GetRoles()
        {
            var requestXml = base.GenerateRequestXml(null, Option.GetRoles);
            var responseModel = base.Execute(requestXml);
            var roles = (List<RoleModel>)ModelDeserializer.DeserializeListFromXml<RoleModel>(responseModel.ModelXml);
            return roles;
        }

        [HttpGet]
        public ActionResult GetRoleList()
        {
            return PartialView("_ListRoles", GetRoles());
        }

        [HttpGet]
        public ActionResult CreateRole(int? id)
        {
            if (id == null)
                return PartialView("_CreateRole", new RoleModel());
            else
            {
                var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = (int)id }, Option.GetRole);
                var responseModel = base.Execute(requestXml);
                var model = (RoleModel)ModelDeserializer.DeserializeFromXml<RoleModel>(responseModel.ModelXml);

                return PartialView("_CreateRole", model);
            }
        }

        [HttpPost]
        public ActionResult CreateRole(RoleModel model)
        {
            string requestXml;

            if (model.ID == 0)
            {
                model.Status = Status.Active;
                requestXml = base.GenerateRequestXml(model, Option.CreateRole);
            }
            else
                requestXml = base.GenerateRequestXml(model, Option.UpdateRole);

            var responseModel = base.Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
                return Json("success");
            else
                return Json(responseModel.StatusMessage);
        }

        [HttpPost]
        public void ChangeRoleStatus(int id, Status status)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, Status> { Key = id, Value = status }, Option.ChangeRoleStatus);
            var responseModel = base.Execute(requestXml);
        }

        #endregion Role

        #region Role Options

        public ActionResult ListRoleOptions(int id)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetUserRoleOptions);
            var responseModel = base.Execute(requestXml);
            var model = (RoleOptionsModel)ModelDeserializer.DeserializeFromXml<RoleOptionsModel>(responseModel.ModelXml);

            return PartialView("_ListRoleOptions", model);
        }

        public List<TreeViewItemModel> GetOptionTree(List<ClientTreeViewItemModel> optionTree)
        {
            return optionTree.Select(a => new TreeViewItemModel
            {
                Id = a.id.ToString(),
                Text = a.text,
                Checked = a.selected,
                Expanded = a.items.Any(x => x.selected),
                Items = a.items != null ? GetOptionTree(a.items) : null
            }).ToList();
        }

        public JsonResult AddRoleOptions(int roleId, int[] options)
        {
            var model = new RoleOptionsModel
            {
                ID = roleId,
                CreateBy = SessionHelper.UserId,
                CreatedOn = DateTime.Now,
                SelectedOptions = options != null && options.Any() ? options.Select(a => a).ToList() : null
            };

            var requestXml = base.GenerateRequestXml(model, Option.AddUserRoleOptions);
            var responseModel = base.Execute(requestXml);

            return Json(responseModel.StatusMessage.ToString());
        }

        public ActionResult GetOptionTree()
        {
            var list = new List<DBASystem.SMSF.Contracts.ViewModels.ClientTreeViewItemModel>();
            var childList = new List<DBASystem.SMSF.Contracts.ViewModels.ClientTreeViewItemModel>();

            childList.Add(new DBASystem.SMSF.Contracts.ViewModels.ClientTreeViewItemModel { id = 101, text = "Validate User" });
            childList.Add(new DBASystem.SMSF.Contracts.ViewModels.ClientTreeViewItemModel { id = 102, text = "Log Out" });

            list.Add(new DBASystem.SMSF.Contracts.ViewModels.ClientTreeViewItemModel { id = 100, text = "Login", hasChildren = true, items = childList, expanded = true });

            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion Role Options

        #region Private Functions

        private UserProfileModel GetUserProfile(int id)
        {
            var userProfilerequestXml = GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetUserProfile);
            var userProfileresponseModel = Execute(userProfilerequestXml);
            var model = (UserProfileModel)ModelDeserializer.DeserializeFromXml<UserProfileModel>(userProfileresponseModel.ModelXml);
            return model;
        }

        #endregion Private Functions
    }
}