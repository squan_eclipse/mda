﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class JobController : BaseController
    {
        public ActionResult HasMultipleAccess()
        {
            var requestXml =
                GenerateRequestXml(
                    new GenericModel<int> {Value = SessionHelper.UserId},
                    Option.HasMultipleJobAccess);
            var responseModel = Execute(requestXml);
            var models =
                (GenericModel<int>) ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

            return models.Value == 0
                ? RedirectToAction("Index")
                : RedirectToAction("Detail", new {id = Utility.EncryptQueryString(models.Value)});
        }

        public ActionResult Index(int? statusId)
        {
            SessionHelper.SelectedMenu = "Jobs";
            
            ViewBag.EntityType = (int) EntityTypes.Job;
            ViewBag.ControllerName = "Job";
            ViewBag.StatusID = statusId ?? 0;

            if (TempData["serachData"] != null)
            {
                var model = (SearchFilterModel) TempData["serachData"];
                ViewBag.Firm = string.IsNullOrEmpty(model.Firm) ? "" : model.Firm;
                ViewBag.Account = string.IsNullOrEmpty(model.Account) ? "" : model.Account;
                ViewBag.ABN = string.IsNullOrEmpty(model.ABN) ? "" : model.ABN;
                ViewBag.AccountManager = string.IsNullOrEmpty(model.AccountManager) ? "" : model.AccountManager;
                ViewBag.PrimaryContact = string.IsNullOrEmpty(model.PrimaryContact) ? "" : model.PrimaryContact;
                ViewBag.DateFrom = model.DateFrom == null ? "" : Convert.ToDateTime(model.DateFrom).ToShortDateString();
                ViewBag.DateTo = model.DateTo == null ? "" : Convert.ToDateTime(model.DateTo).ToShortDateString();
                ViewBag.StatusList = JsonConvert.SerializeObject(GetJobFlowStatusList());
            }
            else
            {
                ViewBag.Firm = string.Empty;
                ViewBag.Account = string.Empty;
                ViewBag.ABN = string.Empty;
                ViewBag.AccountManager = string.Empty;
                ViewBag.PrimaryContact = string.Empty;
                ViewBag.DateFrom = string.Empty;
                ViewBag.DateTo = string.Empty;
                ViewBag.StatusList = JsonConvert.SerializeObject(GetJobFlowStatusList());
            }

            return View();
        }

        public ActionResult Overdue()
        {
            SessionHelper.SelectedMenu = "Jobs";

            ViewBag.EntityType = (int) EntityTypes.Job;
            ViewBag.ControllerName = "Job";
            
            return View();
        }

        [OptionFilter(Option = Option.GetWorkpaperMenu)]
        public ActionResult GetWorkpaperMenu(ManageWorkPaperModel manageWorkPaperModel)
        {
            //For Selected Menu
            var requestXml = GenerateRequestXml(manageWorkPaperModel, Option.GetWorkpaperMenu);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var model = (WorkpaperMenuViewModel)ModelDeserializer.DeserializeFromXml<WorkpaperMenuViewModel>(responseModel.ModelXml);

                if (manageWorkPaperModel.ContentType.HasValue)
                {
                    model.WorkPapers = model.WorkPapers.Where(x => x.ContentType == manageWorkPaperModel.ContentType).ToList();
                }

                if(SessionHelper.IsClientUser)
                {
                    model.WorkPapers = model.WorkPapers.Where(x => x.IsViewable).ToList();
                }

                if (manageWorkPaperModel.AccountId != 0)
                    ViewBag.AccountId = manageWorkPaperModel.AccountId;

                return PartialView("_WorkpaperMenu", model);
            }

            return PartialView("_WorkpaperMenu", null);
        }

        public JsonResult IndexSearch(SearchFilterModel searchModel)
        {
            searchModel.EntityID = SessionHelper.EntityID;
            searchModel.EntityType = EntityTypes.Job;
            searchModel.UserID = SessionHelper.UserId;
            ViewBag.EntityType = (int) EntityTypes.Job;

           
            var requestXml = GenerateRequestXml(searchModel, Option.GetJobs);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<JobsModel>(responseModel.ModelXml);

            return
                Json(
                    new
                    {
                        data = model,
                        message = responseModel.Status == ResponseStatus.Success ? "" : responseModel.ResponseMessage
                    });
        }

        public ActionResult IndexSearchWithFilter(string searchModelstring)
        {
            var searchModel = JsonConvert.DeserializeObject<SearchFilterModel>(searchModelstring,
                new JsonSerializerSettings {DateFormatString = "dd/MM/yyyy"});
            TempData["serachData"] = searchModel;

            return RedirectToAction("Index", "Job");
        }

        private List<JobFlowStaus> GetJobFlowStatusList()
        {
            var jobStates = Enum.GetValues(typeof (JobWorkflowStatus))
            .Cast<JobWorkflowStatus>()
                .Select(v => new JobFlowStaus() {value = (int) v, text = v.GetDescription()})
            .ToList();
            jobStates.Add(new JobFlowStaus() {selected = true, text = "ALL", value = 0});
            return jobStates.OrderBy(p => p.value).ToList();
           
        }

        [HttpGet]
        public JsonResult GetJobFlowStates()
        {
            return Json(JsonConvert.SerializeObject(GetJobFlowStatusList()), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(string id)
        {
            id = Utility.DecryptQueryString(id);
            //For Selected Menu
            SessionHelper.SelectedMenu = "Jobs";

            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(id) }, Option.GetJobInfo);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var model =
                    (JobCreateModel) ModelDeserializer.DeserializeFromXml<JobCreateModel>(responseModel.ModelXml);
                return View(model);
            }
            return View(new JobCreateModel());
        }

        [OptionFilter(Option = Option.AddJob)]
        public ActionResult Create(string pId)
        {
            ViewBag.EncryptedID = pId;
            pId = Utility.DecryptQueryString(pId);
            var model = new JobCreateModel {FundId = Convert.ToInt32(pId)};
            return View("Create", model);
        }

        private List<AccountManagerSelect> GetJobManagerList(int fundId)
        {
            var model = new GenericModel<int?>();
            model.Value = fundId;

            var requestXml = GenerateRequestXml(model, Option.GetJobManagers);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var managers =
                    (List<AccountManagerSelect>)
                        ModelDeserializer.DeserializeListFromXml<AccountManagerSelect>(responseModel.ModelXml);
                return managers;
            }
            return new List<AccountManagerSelect>();
        }

        private List<JobRolesModel> GetJobRoleList(int fundId)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int>{Value = fundId}, Option.GetJobRoles);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
                return
                    (List<JobRolesModel>)
                        ModelDeserializer.DeserializeListFromXml<JobRolesModel>(responseModel.ModelXml);

            return new List<JobRolesModel>();
        }

        [HttpGet]
        public ActionResult GetJobManagers(int parentEntityId, string selectedManagers = null)
        {
            var accountManagers = GetJobManagerList(parentEntityId);
            var selectView = new AccountManagerSelectView();

            selectView.Managers =
                accountManagers.Select(
                    p => new AccountManager() {AccountManagerName = p.AccountManagerName, Email = p.Email, Id = p.ID})
                    .ToList();
            selectView.Roles = GetJobRoleList(parentEntityId);
            selectView.SelectedManagers = String.IsNullOrEmpty(selectedManagers)
                ? new List<AccountManagerSelect>()
                : JsonConvert.DeserializeObject<List<AccountManagerSelect>>(selectedManagers);

            return PartialView("_ListJobManagers", selectView);
        }

        public ActionResult GetJobtManagersForList([DataSourceRequest] DataSourceRequest request)
        {
            return Json(((List<AccountManagerSelect>) SessionHelper.SessionObject).ToDataSourceResult(request),
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobInfo(int id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> {Value = id}, Option.GetJobInfo);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var model =
                    (JobCreateModel) ModelDeserializer.DeserializeFromXml<JobCreateModel>(responseModel.ModelXml);
                model.EntityType = 6;

                //return model;
                return Json(model, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [OptionFilter(Option = Option.UpdateJob)]
        public ActionResult EditJob(string jobId)
        {
            ViewBag.EncryptedID = jobId;
            jobId = Utility.DecryptQueryString(jobId);
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(jobId) }, Option.GetJobInfo);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var model =
                    (JobCreateModel) ModelDeserializer.DeserializeFromXml<JobCreateModel>(responseModel.ModelXml);
              

                return View("Create", model);
            }

            return HttpNotFound();
        }

        private int GetRoleID(int managerID, string[] roles)
        {
            foreach (string role in roles)
            {
                string extractedManagerID = role.Split(',')[0];
                if (managerID.ToString() == extractedManagerID)
                    return int.Parse(role.Split(',')[1]);
            }
            return 0;
        }

        [HttpPost]
        [OptionFilter(Option = Option.AddJob)]
        public JsonResult AddJob(JobCreateModel model)
        {
            var jobManagers = Request.Form.GetValues("jobManagers");

            if (ModelState.IsValid)
            {
                var selectedManagers = jobManagers == null
                    ? new List<AccountManagerSelect>()
                    : JsonConvert.DeserializeObject<List<AccountManagerSelect>>(jobManagers[0]);
                if (selectedManagers != null)
                    model.SelectedJobManager = selectedManagers.Where(q => q.Selected).Select(p => p).ToList();

                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                var requestXml = GenerateRequestXml(model, Option.AddJob);
                var responseModel = Execute(requestXml);
                if (responseModel.Status == ResponseStatus.Warning)
                {
                    responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage +
                                                    ", however job has been created successfully , please contact your administrator.";
                    TempData["WarningMessage"] = responseModel.ResponseMessage;
                }

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var jobId =
                        (GenericModel<int>)
                            ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);
                    return Json(new {Status = ResponseStatus.Success.ToString(), JobId = Utility.EncryptQueryString(jobId.Value) });
                }

                ModelState.AddModelError("", responseModel.ResponseMessage);
                var errors = new List<object>();
                errors.Add(new {error = responseModel.ResponseMessage});
                return Json(new {status = "Error", ResponseMessage = errors});
            }
            else
            {
                var errors = new List<object>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(new {error = error.ErrorMessage});
                    }
                }
                return Json(new {status = "Error", ResponseMessage = errors});
            }
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateJob)]
        public JsonResult UpdateJob(JobCreateModel model)
        {
            var jobManagers = Request.Form.GetValues("jobManagers");

            if (ModelState.IsValid)
            {
                var selectedManagers = jobManagers == null
                    ? new List<AccountManagerSelect>()
                    : JsonConvert.DeserializeObject<List<AccountManagerSelect>>(jobManagers[0]);
                model.SelectedJobManager = selectedManagers.Where(q => q.Selected).Select(p => p).ToList();

                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = SessionHelper.UserId;
                var requestXml = GenerateRequestXml(model, Option.UpdateJob);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Warning)
                {
                    responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage +
                                                    ", however job has been updated successfully , please contact your administrator.";
                    TempData["WarningMessage"] = responseModel.ResponseMessage;
                }

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    return Json(new { Status = ResponseStatus.Success.ToString(), JobId = Utility.EncryptQueryString(model.ID) });
                }

                return Json(new {Status = "Error", ResponseMessage = responseModel.ResponseMessage});
            }
            else
            {
                var errors = new List<object>();
                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(new {error = error.ErrorMessage});
                    }
                }
                return Json(new {status = "Error", ResponseMessage = errors});
            }
        }

        [HttpGet]
        [OptionFilter(Option = Option.Deletejob)]
        public JsonResult Delete(int id)
        {
            var requestXml =
                GenerateRequestXml(new DictionaryModel<int, int> {Key = id, Value = SessionHelper.UserId},
                    Option.Deletejob);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Warning)
            {
                responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage +
                                                ", however job has been deleted successfully , please contact your administrator.";
                TempData["WarningMessage"] = responseModel.ResponseMessage;
            }

            if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
            {
                return Json(new {Status = ResponseStatus.Success.ToString(), JobId = id}, JsonRequestBehavior.AllowGet);
            }

            return Json(new {Status = "Error", ResponseMessage = responseModel.ResponseMessage},
                JsonRequestBehavior.AllowGet);
        }

        [OptionFilter(Option = Option.WorkPaper)]
        public ActionResult WorkPaper(string jobId)
        {
            jobId = Utility.DecryptQueryString(jobId);
            //For Selected Menu
            SessionHelper.SelectedMenu = "Jobs";

            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(jobId) }, Option.WorkPaper);
            var responseModel = Execute(requestXml);
            var model = (JobModel)ModelDeserializer.DeserializeFromXml<JobModel>(responseModel.ModelXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                return View(model);
            }

            return View(new JobModel());
        }

        [OptionFilter(Option = Option.WorkPaper)]
        public ActionResult ManageWorkPaper(int accountId)
        {
            if (accountId != 0)
            {
                ViewBag.AccountId = accountId;
                GetWorkpaperTemplates(accountId);
            }

            return View(new JobModel());
        }

        private void GetWorkpaperTemplates(int accountId)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<string, int> { Key = "AccountId", Value = accountId }, Option.GetWorkpaperTemplates);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<WorkpaperTemplateModel>(responseModel.ModelXml);

            ViewBag.WorkpaperTemplates = new SelectList(model, "ID", "Name");
        }

         [OptionFilter(Option = Option.WorkPaper)]
        public ActionResult UpdateWorkPaper(int accountId)
         {
             if (accountId != 0)
                 ViewBag.AccountId = accountId;
             
             return View();
        }
           
        public ActionResult UpdateWorkPaperContent(int accountId, int tabId)
        {
            ViewBag.AccountId = accountId;
            ViewBag.AuditProcTabId = tabId;
             return View();
         }

        public ActionResult AuditReports(string jobId)
        {
            var id = Utility.DecryptQueryString(jobId);
            return View(id.ToType<int>());
        }

        #region Work Logs

        private JobWorkLogAccountManagerModel GetWorkLogAccountManager(int jobId)
        {
            var requestXml =
                GenerateRequestXml(new DictionaryModel<int, int> {Key = jobId, Value = SessionHelper.UserId},
                    Option.GetWorkLogAccountManagers);
            var responseModel = Execute(requestXml);
            var model =
                (JobWorkLogAccountManagerModel)
                    ModelDeserializer.DeserializeFromXml<JobWorkLogAccountManagerModel>(responseModel.ModelXml);

            if (SessionHelper.Group != UserGroup.Administrators)
            {
                var managers = model.AccountManagers.Where(a => a.Key == SessionHelper.UserId).ToList();
                model.AccountManagers = managers;
            }

            return model;
        }

        public ActionResult WorkLog(string jobId)
        {
            jobId = Utility.DecryptQueryString(jobId);
            return View(GetWorkLogAccountManager(Convert.ToInt32(jobId)));
        }

        [OptionFilter(Option = Option.GetWorkLogs)]
        public JsonResult GetWorkLogs(int jobId, int userId)
        {
            if (userId == 0)
            {
                if (SessionHelper.Group != UserGroup.Administrators)
                    userId = SessionHelper.UserId;
            }
            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> {Key = jobId, Value = userId},
                Option.GetWorkLogs);
            var responseModel = Execute(requestXml);
            var models = ModelDeserializer.DeserializeListFromXml<JobWorkLogModel>(responseModel.ModelXml);

            return Json(models, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddWorkLog(int jobId)
        {
            var model = GetWorkLogAccountManager(jobId);

            ViewBag.AccountManagerID = 
                model.AccountManagers.Select(a => new SelectListItem {Value = a.Key.ToString(), Text = a.Value})
                    .ToList();

            return PartialView("_AddWorkLog", new JobWorkLogModel {JobID = jobId});
        }

        public ActionResult UpdateWorkLog(JobWorkLogModel workLog)
        {
            var model = GetWorkLogAccountManager(workLog.JobID);

            ViewBag.AccountManagerID =
                model.AccountManagers.Select(
                    a =>
                        new SelectListItem
                        {
                            Value = a.Key.ToString(),
                            Text = a.Value,
                            Selected = (workLog.AccountManagerID == a.Key)
                        }).ToList();

            return PartialView("_AddWorkLog", workLog);
        }

        [HttpPost]
        public ActionResult AddWorkLog(JobWorkLogModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.ID == 0)
                    {
                        model.CreatedBy = SessionHelper.UserId;
                        model.CreatedOn = DateTime.Now;

                        var requestXml = GenerateRequestXml(model, Option.AddWorkLog);
                        var responseModel = Execute(requestXml);
                        var id =
                            (GenericModel<int>)
                                ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

                        model.ID = id.Value;
                        return Json(new {status = "success", model, action = "added"});
                    }
                    else
                    {
                        model.ModifiedBy = SessionHelper.UserId;
                        model.ModifiedOn = DateTime.Now;

                        var requestXml = GenerateRequestXml(model, Option.UpdateWorkLog);
                        var responseModel = Execute(requestXml);
                        return Json(new {status = "success", model, action = "updated"});
                    }
                }
            }
            catch (Exception e)
            {
                return Json(new {status = "error", error = e.Message});
            }

            return Json(new {status = "error", error = "Model state is not valid"});
        }

        [HttpPost]
        public ActionResult DeleteWorkLog(int id)
        {
            try
            {
                var requestXml = GenerateRequestXml(new GenericModel<int> {Value = id}, Option.DeleteWorkLog);
                var responseModel = Execute(requestXml);

                return Json(new {status = "success"});
            }
            catch (Exception e)
            {
                return Json(new {status = "error", error = e.Message});
            }
        }

        #endregion

        #region Charts

        public JsonResult GetScheduledJobs(int months)
        {
            var startDate = DateTime.Now.AddMonths(months * -1);
            var endDate = DateTime.Now;

            var requestXml =
                GenerateRequestXml(
                    new JobGraphModel
                    {
                        UserID = SessionHelper.UserId,
                        Group = SessionHelper.Group,
                        StartDate = startDate,
                        EndDate = endDate
                    }, Option.GetScheduledJobs);

            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<JobGraphModel>(responseModel.ModelXml);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCompletedJobs(int months)
        {
            var startDate = DateTime.Now.AddMonths(months * -1);
            var endDate = DateTime.Now;

            var requestXml =
                GenerateRequestXml(
                    new JobGraphModel
                    {
                        UserID = SessionHelper.UserId,
                        Group = SessionHelper.Group,
                        StartDate = startDate,
                        EndDate = endDate
                    },
                    Option.GetCompletedJobs);

            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<JobGraphModel>(responseModel.ModelXml);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetJobToBeCompleted(DateTime startDate, DateTime endDate)
        {
            var requestXml =
                GenerateRequestXml(
                    new JobGraphModel
                    {
                        UserID = SessionHelper.UserId,
                        Group = SessionHelper.Group,
                        StartDate = startDate,
                        EndDate = endDate
                    },
                    Option.GetJobToBeCompleted);
            var responseModel = Execute(requestXml);
            var model = ModelDeserializer.DeserializeListFromXml<JobGraphModel>(responseModel.ModelXml);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Comments

        public ActionResult Comments(string id)
        {
            id = Utility.DecryptQueryString(id);
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(id) }, Option.WorkPaper);
            var responseModel = Execute(requestXml);
            var model = (JobModel)ModelDeserializer.DeserializeFromXml<JobModel>(responseModel.ModelXml);

            requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(id) }, Option.GetAuditProcedureCommentsByJob);
            responseModel = Execute(requestXml);
            var comments = ModelDeserializer.DeserializeListFromXml<AuditProcedureCommentViewModel>(responseModel.ModelXml);
            ViewBag.Comments = comments;

            return View(model);
        }

        [HttpPost]
        public JsonResult AddComments(int jobId, int auditprocedureId, int? parentId, string comment)
        {
            try
            {
                var auditprocedureComment = new AuditProcedureCommentViewModel
                {
                    AccountManagerName = SessionHelper.Name,
                    AuditProcedureId = auditprocedureId,
                    Comment = comment,
                    CreatedBy = SessionHelper.UserId,
                    CreatedOn = DateTime.Now,
                    JobId = jobId,
                    ParentID = parentId
                };
                var requestXml = GenerateRequestXml(auditprocedureComment, Option.AddProcedureComments);
                var responseModel = Execute(requestXml);
                var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

                auditprocedureComment.Id = model.Value;

                return Json(new {status = "success", model = auditprocedureComment}, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                return Json(new {status = "error", message = e.Message});
            }
        }

        #endregion
    }
}