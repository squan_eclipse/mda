﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class NotesController : BaseController
    {
        public ActionResult IndexNotes(int typeID, int parentID)
        {
            var model = new NoteModel { ParentId = parentID, Type = (EntityGenericType)typeID };
            return PartialView("_Notes", model);
        }

        public JsonResult IndexNotesUpdate(NotesModel notesModel)
        {
            notesModel.ModifiedOn = DateTime.Now;
            notesModel.ModifiedBy = SessionHelper.UserId;
            if (notesModel.ID == 0)
            {
                notesModel.CreatedOn = DateTime.Now;
                notesModel.CreatedBy = SessionHelper.UserId;
            }
            var requestXml = base.GenerateRequestXml(notesModel, Option.AddUpdateNotes);
            var responseModel = base.Execute(requestXml);
            var model = (NotesModel)ModelDeserializer.DeserializeFromXml<NotesModel>(responseModel.ModelXml);

            return Json(notesModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNotes(int ParentID, int typeID)
        {
            List<NotesModel> lst = new List<NotesModel>();
            var requestXml = base.GenerateRequestXml(new NotesGenericModel() { ParentID = ParentID, TypeId = typeID }, Option.GetNotes);
            var responseModel = base.Execute(requestXml);
            var model = (List<NotesModel>)ModelDeserializer.DeserializeListFromXml<NotesModel>(responseModel.ModelXml);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #region Entity Notes

        [HttpPost]
        [OptionFilter(Option = Option.AddFirmNotes)]
        public JsonResult AddEntityNotes(NoteModel notesModel)
        {
            return AddNote(notesModel, Option.AddFirmNotes);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateFirmNotes)]
        public JsonResult UpdateEntityNotes(NoteModel notesModel)
        {
            return UpdateNote(notesModel, Option.UpdateFirmNotes);
        }

        [HttpPost]
        [OptionFilter(Option = Option.DeleteFirmNotes)]
        public JsonResult DeleteEntityNote(int id)
        {
            return DeleteNote(id, Option.DeleteFirmNotes);
        }

        [HttpGet]
        [OptionFilter(Option = Option.AddFirmNotes)]
        public ActionResult CreateEntityNote(int id)
        {
            var model = new NoteModel { ParentId = id, Type = EntityGenericType.Entity };
            return PartialView("_Notes", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.UpdateFirmNotes)]
        public ActionResult EditEntityNote(int id)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, EntityGenericType> { Key = id, Value = EntityGenericType.Entity }, Option.GetFirmNotes);
            var responseModel = base.Execute(requestXml);
            var model = (NoteModel)ModelDeserializer.DeserializeFromXml<NoteModel>(responseModel.ModelXml);
            return PartialView("_Notes", model);
        }

        #endregion Entity Notes

        #region Job Notes

        [HttpPost]
        [OptionFilter(Option = Option.AddJobNotes)]
        public JsonResult AddJobNotes(NoteModel notesModel)
        {
            return AddNote(notesModel, Option.AddJobNotes);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateJobNotes)]
        public JsonResult UpdateJobNotes(NoteModel notesModel)
        {
            return UpdateNote(notesModel, Option.UpdateJobNotes);
        }

        [HttpPost]
        [OptionFilter(Option = Option.DeleteJobNotes)]
        public JsonResult DeleteJobNote(int id)
        {
            return DeleteNote(id, Option.DeleteJobNotes);
        }

        [HttpGet]
        [OptionFilter(Option = Option.AddJobNotes)]
        public ActionResult CreateJobNote(int id)
        {
            var model = new NoteModel { ParentId = id, Type = EntityGenericType.Job };
            return PartialView("_Notes", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.UpdateJobNotes)]
        public ActionResult EditJobNote(int id)
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<int, EntityGenericType> { Key = id, Value = EntityGenericType.Job }, Option.GetJobNotes);
            var responseModel = base.Execute(requestXml);
            var model = (NoteModel)ModelDeserializer.DeserializeFromXml<NoteModel>(responseModel.ModelXml);
            return PartialView("_Notes", model);
        }

        #endregion Job Notes

        #region Private Methods

        private JsonResult AddNote(NoteModel notesModel, Option option)
        {
            if (!ModelState.IsValid) return Json(new { });

            notesModel.CreatedOn = DateTime.Now;
            notesModel.CreatedBy = SessionHelper.UserId;

            var requestXml = base.GenerateRequestXml(notesModel, option);
            var responseModel = base.Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var newNoteId =
                    (GenericModel<int>)
                        ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);
                var viewModel = new NotesViewModel()
                {
                    Date = (DateTime)notesModel.CreatedOn,
                    Id = newNoteId.Value,
                    Note = notesModel.Note
                };
                return Json(new { status = responseModel.Status.ToString(), Note = viewModel },
                    JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = responseModel.Status, responseMessage = responseModel.ResponseMessage });
        }

        private JsonResult UpdateNote(NoteModel notesModel, Option option)
        {
            if (!ModelState.IsValid) return Json(new { });

            notesModel.ModifiedOn = DateTime.Now;
            notesModel.ModifiedBy = SessionHelper.UserId;
            if (notesModel.ID == 0)
            {
                notesModel.CreatedOn = DateTime.Now;
                notesModel.CreatedBy = SessionHelper.UserId;
            }

            var requestXml = base.GenerateRequestXml(notesModel, option);
            var responseModel = base.Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                //var newNoteId = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);
                var viewModel = new NotesViewModel()
                {
                    Date = (DateTime)notesModel.ModifiedOn,
                    Id = notesModel.ID,
                    Note = notesModel.Note
                };
                return Json(new { status = responseModel.Status.ToString(), Note = viewModel }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = responseModel.Status.ToString() }, JsonRequestBehavior.AllowGet);
        }

        private JsonResult DeleteNote(int id, Option option)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = id }, option);
            var responseModel = base.Execute(requestXml);
            return Json(new { status = responseModel.Status.ToString() }, JsonRequestBehavior.AllowGet);
        }

        #endregion Private Methods
    }
}