﻿using System.Web.Routing;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class FirmController : BaseController
    {
        #region Firm

        public ActionResult HasMultipleAccess()
        {
            var requestXml =
                GenerateRequestXml(
                    new DictionaryModel<int, EntityTypes> { Key = SessionHelper.UserId, Value = EntityTypes.Entity },
                    Option.HasMultipleAccess);
            var responseModel = Execute(requestXml);
            var models = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

            return models.Value == 0
                ? RedirectToAction("Index")
                : RedirectToAction("Detail", new {id = Utility.EncryptQueryString(models.Value)});
        }

        [HttpGet]
        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Firms";

            ViewBag.EntityType = (int)EntityTypes.IFM;
            ViewBag.ControllerName = "Firm";

            return View();
        }

        [HttpGet]
        public ActionResult Detail(string id)
        {
            id = Utility.DecryptQueryString(id);
            SessionHelper.SelectedMenu = "Firms";
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(id) }, Option.GetFirmDetail);
            var responseModel = Execute(requestXml);
            var models = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);
            return FirmDetailView(models);
        }

        private ActionResult FirmDetailView(FirmDetailModels model)
        {
            SessionHelper.ListUsers = null;
            SessionHelper.ListAddresses = null;
            return View("Detail", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.AddFirm)]
        public ActionResult Create(string pId)
        {
            ViewBag.EncryptedID = pId;
            pId = Utility.DecryptQueryString(pId);

            SessionHelper.ListUsers = null;
            SessionHelper.ListAddresses = null;
            SessionHelper.SelectedMenu = "Firms";

            var model = new FirmCreateModel { ParentID = Convert.ToInt32(pId), EntityType = EntityTypes.Entity };
            return View(model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.AddFirm)]
        public ActionResult CreateFirm(FirmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                model.ListUser = SessionHelper.ListUsers;
                model.ListAddress = SessionHelper.ListAddresses;

                var requestXml = GenerateRequestXml(model, Option.AddFirm);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var reponseModel = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);

                    if (responseModel.Status == ResponseStatus.Warning)
                    {
                        ViewBag.WarningMessage = responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage + ",however " + model.EntityType.ToString() + " has been created successfully , please contact your administrator.";
                    }
                    ViewBag.BackURL = Url.Content("~/Firm/Detail/" + model.ParentID);
                    return RedirectToAction("Detail", new {id = Utility.EncryptQueryString(reponseModel.ID)});
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View("Create", model);
                }
            }

            return View("Create", model);
        }

        public JsonResult IndexSearch(SearchFilterModel searchModel)
        {
            searchModel = searchModel ?? new SearchFilterModel();
            
            
            searchModel.EntityID = SessionHelper.EntityID;
            searchModel.UserID = SessionHelper.UserId;
            searchModel.EntityType = EntityTypes.Entity;

            var requestXml = GenerateRequestXml(searchModel, Option.GetFirms);
            var responseModel = Execute(requestXml);
            var model = (List<FirmModels>)ModelDeserializer.DeserializeListFromXml<FirmModels>(responseModel.ModelXml);

            return
                Json(
                    new
                    {
                        data = model,
                        message = responseModel.Status == ResponseStatus.Success ? "" : responseModel.ResponseMessage
                    },JsonRequestBehavior.AllowGet);
        }
        
        #endregion

        #region Group

        [HttpGet]
        [OptionFilter(Option = Option.GetGroupDetail)]
        public ActionResult GroupDetail(int id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetGroupDetail);
            var responseModel = Execute(requestXml);
            var models = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);
            return FirmDetailView(models);
        }

        [HttpGet]
        [OptionFilter(Option = Option.AddGroup)]
        public ActionResult AddGroup(string parentId)
        {
            parentId = Utility.DecryptQueryString(parentId);
            var model = new FirmCreateModel { ParentID = Convert.ToInt32(parentId), EntityType = EntityTypes.Group };
            return View("Create", model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.AddGroup)]
        public ActionResult CreateGroup(FirmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                model.ListUser = SessionHelper.ListUsers;

                if (model.EntityType == EntityTypes.Entity)
                    model.ListAddress = SessionHelper.ListAddresses;

                var requestXml = GenerateRequestXml(model, Option.AddGroup);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var reponseModel = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);

                    if (responseModel.Status == ResponseStatus.Warning)
                    {
                        ViewBag.WarningMessage = responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage + ",however " + model.EntityType.ToString() + " has been created successfully , please contact your administrator."; ;
                    }
                    ViewBag.BackURL = Url.Content("~/Firm/Detail/" + model.ParentID);
                    return View("Detail", reponseModel);
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View("Create", model);
                }
            }

            return View("Create", model);
        } 

        #endregion

        #region Contact Info

        [HttpGet]
        public ActionResult CreateContactInfo(int userId)
        {
            var model = new ContactCreateModel();
            model.AccountManagerID = userId;
            model.ListTypes = GetContactTypes();

            return PartialView("_CreateContactInfo", model);
        }

        [HttpPost]
        public ActionResult CreateContactInfo(ContactCreateModel model)
        {
            var list = SessionHelper.ListUsers.Where(a => a.ID == model.AccountManagerID).FirstOrDefault().ListContactInfo;
            if (list == null) list = new List<ContactCreateModel>();

            if (model.ID == 0)
            {
                model.CreatedBy = SessionHelper.UserId;
                model.CreatedOn = DateTime.Now;
                model.StatusID = Status.Active;
                model.ID = list.Any() ? (list.Max(a => a.ID) + 1) * -1 : -1;
            }
            else
            {
                model.ModifiedBy = SessionHelper.UserId;
                model.ModifiedOn = DateTime.Now;
                list = list.Where(a => a.ID != model.ID).ToList();
            }

            list.Add(model);
            SessionHelper.ListUsers.Where(a => a.ID == model.AccountManagerID).FirstOrDefault().ListContactInfo = list;

            return Json("success");
        }

        [HttpDelete]
        public void DeleteContactInfo(int userId, int id)
        {
            var list = SessionHelper.ListUsers.Where(a => a.ID == userId).FirstOrDefault().ListContactInfo;
            list.First(a => a.ID == id).StatusID = Status.Deleted;
            SessionHelper.ListUsers.Where(a => a.ID == userId).FirstOrDefault().ListContactInfo = list;
        }

        [HttpGet]
        public ActionResult UpdateContactInfo(int userId, int id)
        {
            var list = SessionHelper.ListUsers.Where(a => a.ID == userId).FirstOrDefault().ListContactInfo;
            var model = list.Where(a => a.ID == id).FirstOrDefault();
            model.ListTypes = GetContactTypes();

            return PartialView("_CreateContactInfo", model);
        }

        public JsonResult GetCountryCode(string text)
        {
            var requestXml = GenerateRequestXml(new GenericModel<string> { Value = text }, Option.GetCountryCode);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                return Json(ModelDeserializer.DeserializeListFromXml<DictionaryModel<string, string>>(responseModel.ModelXml), JsonRequestBehavior.AllowGet);
            }
            else
            {
                ModelState.AddModelError("", responseModel.ResponseMessage);
                return Json("failure");
            }
        }

        #endregion Contact Info

        #region Address

        [HttpGet]
        public JsonResult GetAddress()
        {
            return
                Json(
                    SessionHelper.ListAddresses != null
                        ? SessionHelper.ListAddresses.Where(a => a.StatusID != Status.Deleted)
                        : new List<AddressCreateModel>(),
                    JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateAddress()
        {
            var model = new AddressCreateModel();
            model.ListTypes = GetAddressTypes();
            model.ListCountires = GetCountries(true);

            if (model.ListCountires.Any())
            {
                var selectedCountry = model.ListCountires.FirstOrDefault(a => a.Selected);
                if (selectedCountry != null)
                {
                    var states = GetStates(Convert.ToInt32(selectedCountry.Value), (int?)null);
                    model.ListState = (List<SelectListItem>)states.Data;
                    model.Country = selectedCountry.Text;
                }
            }

            return PartialView("_CreateAddress", model);
        }

        [HttpPost]
        public ActionResult CreateAddress(AddressCreateModel model)
        {
            var list = SessionHelper.ListAddresses ?? new List<AddressCreateModel>();

            model.CreatedBy = SessionHelper.UserId;
            model.CreatedOn = DateTime.Now;
            model.StatusID = Status.Active;

            if (model.Default)
                foreach (var item in list.Where(a => a.TypeID == model.TypeID).ToList())
                    item.Default = false;

            if (model.ID != 0)
            {
                list = list.Where(a => a.ID != model.ID).ToList();
                list.Add(model);
            }
            else
            {
                //model.ID = list.Any() ? (list.Max(a => a.ID) + 1) * -1 : -1;
                model.ID = list.Any(p => p.ID < 0) ? (list.Where(p => p.ID < 0).Min(p => p.ID)) - 1 : -1;
                list.Add(model);
            }
            SessionHelper.ListAddresses = list;

            return Json("success");
        }

        [HttpDelete]
        public void DeleteAddress(int id)
        {
            var list = SessionHelper.ListAddresses;
            if (list.First(a => a.ID == id).ID > 0)
                list.First(a => a.ID == id).StatusID = Status.Deleted;
            else
                list = list.Where(a => a.ID != id).ToList();

            SessionHelper.ListAddresses = list;
        }

        [HttpGet]
        public ActionResult UpdateAddress(int id)
        {
            var model = SessionHelper.ListAddresses.FirstOrDefault(a => a.ID == id);
            model.ListTypes = new List<SelectListItem>(GetAddressTypes());
            model.ListCountires = new List<SelectListItem>(GetCountries(false));

            if (model.ListCountires.Any())
            {
                var selectedCountry = model.ListCountires.FirstOrDefault(a => a.Value == model.CountryID.ToString());
                if (selectedCountry != null)
                {
                    var states = GetStates(Convert.ToInt32(selectedCountry.Value), model.StateID);
                    model.ListState = new List<SelectListItem>((List<SelectListItem>) states.Data);
                    model.Country = selectedCountry.Text;
                }
            }

            return PartialView("_CreateAddress", model);
        }

        #region Country

        [HttpPost]
        public JsonResult AddCountry(string name, string code)
        {
            try
            {
                var requestXml = GenerateRequestXml(new DictionaryModel<string, string> {Key = name, Value = code}, Option.AddCountry);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Failure)
                    return Json(new {status = "error", message = responseModel.ResponseMessage});

                var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

                return Json(new { status = "success", ID = model.Value });
            }
            catch (Exception e)
            {
                return Json(new {status = "error", message = e.Message});
            }
        }

        [HttpPost]
        public JsonResult AddState(int countryId, string name)
        {
            try
            {
                var requestXml = GenerateRequestXml(new DictionaryModel<int, string> { Key = countryId, Value = name }, Option.AddState);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Failure)
                    return Json(new { status = "error", message = responseModel.ResponseMessage });

                var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

                return Json(new { status = "success", ID = model.Value });
            }
            catch (Exception e)
            {
                return Json(new { status = "error", message = e.Message });
            }
        }

        #endregion

        #endregion Address

        #region Account Manager

        [HttpGet]
        public ActionResult GetAccountManagers(int? parentEntityId, int? primaryId)
        {
            if (SessionHelper.ListUsers == null)
            {
                if (parentEntityId != null)
                {
                    var requestXml = GenerateRequestXml(new GenericModel<int?> { Value = parentEntityId }, Option.GetFirmAccountManager);
                    var responseModel = Execute(requestXml);
                    var managers = (List<AccountManagerCreateModel>)ModelDeserializer.DeserializeListFromXml<AccountManagerCreateModel>(responseModel.ModelXml);

                    if (primaryId != null)
                    {
                        foreach (var manager in managers)
                            if (manager.ID == (int)primaryId)
                                manager.Selected = true;
                    }

                    SessionHelper.ListUsers = managers;
                }
            }

            return PartialView("_ListAccountManager");
        }

        [HttpGet]
        public ActionResult CreateAccountManager(int entityId)
        {
            var titleList = Enum.GetNames(typeof(UserTitles)).Select(p => new SelectListItem { Value = p, Text = p }).ToList();
            titleList.Insert(0, new SelectListItem { Text = "-- Select --", Value = "" });
            var model = new AccountManagerCreateModel();
            model.User = new UserCreateModel();
            model.User.ListGroups = GetGroups();
            model.EntityID = entityId;
            model.User.GroupID = 0;

            return PartialView("_CreateAccountManager", model);
        }

        [HttpPost]
        public ActionResult CreateAccountManager(AccountManagerCreateModel model)
        {
            var list = SessionHelper.ListUsers ?? new List<AccountManagerCreateModel>();

            if (list.Any())
            {
                foreach (var item in list)
                    item.Selected = false;
            }

            model.Status = Status.Active;
            model.Selected = true;
            if (!model.HasLogin)
                model.User = null;
            else
                if (!string.IsNullOrEmpty(model.User.Password))
                    model.User.Password = CommonFunction.HashPassword(model.User.Password);

            if (model.ID == 0)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                model.ID = list.Any(a => a.ID < 0) ? (list.Where(a => a.ID < 0).Max(a => a.ID) - 1) : -1;
            }
            else if (model.ID > 0)
            {
                model.ModifiedBy = SessionHelper.UserId;
                model.ModifiedOn = DateTime.Now;
                list = list.Where(a => a.ID != model.ID).ToList();
            }

            list.Add(model);
            SessionHelper.ListUsers = list;

            return Json("success");
        }

        [HttpGet]
        public ActionResult UpdateAccountManager(int id)
        {
            var model = SessionHelper.ListUsers.Where(a => a.ID == id).FirstOrDefault();
            if (model.User == null) model.User = new UserCreateModel();
            model.User.ListGroups = GetGroups();

            return PartialView("_CreateAccountManager", model);
        }

        [HttpDelete]
        public void DeleteAccountManager(int id)
        {
            if (id == SessionHelper.UserId)
                throw new UnauthorizedAccessException(Resource_EN.Error_CannotDeleteLoggedInUser);

            SessionHelper.ListUsers.First(a => a.ID == id).Status = Status.Deleted;
        }

        [HttpPost]
        public void SelectAccountManager(int id)
        {
            var list = SessionHelper.ListUsers ?? new List<AccountManagerCreateModel>();

            foreach (var item in list)
            {
                if (item.ID == id)
                    item.Selected = true;
                else
                    item.Selected = false;
            }

            SessionHelper.ListUsers = list;
        }

        public ActionResult GetAccountManagersPaged([DataSourceRequest] DataSourceRequest request)
        {
            return Json(SessionHelper.ListUsers.Where(p => p.Status == Status.Active).Select(q => q).ToList().ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion Account Manager

        #region Edit

        [HttpGet]
        [OptionFilter(Option = Option.UpdateFirm)]
        public ActionResult EditFirm(string firmId)
        {
            ViewBag.EncryptedID = firmId;
            SessionHelper.SelectedMenu = "Firms";
            return GetEditView(Convert.ToInt32(Utility.DecryptQueryString(firmId)));
        }

        [HttpGet]
        [OptionFilter(Option = Option.UpdateGroup)]
        public ActionResult EditGroup(int groupId)
        {
            return GetEditView(groupId);
        }

        private ActionResult GetEditView(int id)
        {
            SessionHelper.ListAddresses = null;
            SessionHelper.ListUsers = null;

            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetFirm);
            var responseModel = Execute(requestXml);
            var model = (FirmCreateModel)ModelDeserializer.DeserializeFromXml<FirmCreateModel>(responseModel.ModelXml);

            if (model.EntityType == EntityTypes.Entity || model.EntityType == EntityTypes.Client)
                SessionHelper.ListAddresses = model.ListAddress;

            return View("Create", model);
        }

        private ActionResult Edit(FirmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = SessionHelper.UserId;
                model.ListAddress = SessionHelper.ListAddresses;
                model.ListUser = SessionHelper.ListUsers;
                
                string requestXml = "";
                if (model.EntityType == EntityTypes.Entity)
                {
                    requestXml = GenerateRequestXml(model, Option.UpdateFirm);
                }
                else if (model.EntityType == EntityTypes.Group)
                {
                    requestXml = GenerateRequestXml(model, Option.UpdateGroup);
                }
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var detailModel = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);
                    if (responseModel.Status == ResponseStatus.Warning)
                    {
                        ViewBag.WarningMessage = responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage + ",however " + model.EntityType.ToString() + " has been updated successfully , please contact your administrator."; ;
                    }
                    return FirmDetailView(detailModel);
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View("Create", model);
                }
            }

            return View(model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateFirm)]
        public ActionResult UpdateFirm(FirmCreateModel model)
        {
            return Edit(model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateGroup)]
        public ActionResult UpdateGroup(FirmCreateModel model)
        {
            return Edit(model);
        }

        #endregion Edit

        #region Delete

        [HttpGet]
        [OptionFilter(Option = Option.DeleteFirm)]
        public JsonResult DeleteFirm(int id)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> { Key = id, Value = SessionHelper.UserId }, Option.DeleteFirm);
            var responseModel = Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            return
                Json(
                    status.Value
                        ? new {result = status.Value, ResponseMessage = ""}
                        : new {result = status.Value, ResponseMessage = responseModel.ResponseMessage},
                    JsonRequestBehavior.AllowGet);
        }

        #endregion Delete

        #region Custom Functions

        public List<SelectListItem> GetAddressTypes()
        {
            var requestXml = GenerateRequestXml(null, Option.ListAddressTypes);
            var responseModel = Execute(requestXml);
            var addressTypes = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            return addressTypes.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList();
        }

        public List<SelectListItem> GetEntityCategories(int? id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int?> { Value = id }, Option.GetFirmCategories);
            var responseModel = Execute(requestXml);
            var entityTypes = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            return entityTypes.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList();
        }

        public List<SelectListItem> GetGroups()
        {
            var groups = new SettingController().GetGroups();
            var list = groups.Where(a => a.Status == Status.Active).Select(a => new SelectListItem { Value = a.ID.ToString(), Text = a.Title }).ToList();

            list.Insert(0, new SelectListItem { Text = "-- Select --", Value = "" });
            return list;
        }

        public List<SelectListItem> GetContactTypes()
        {
            var requestXml = GenerateRequestXml(null, Option.GetTelephoneTypes);
            var responseModel = Execute(requestXml);
            var groups = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            return groups.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList();
        }

        public List<SelectListItem> GetCountries(bool showSelected)
        {
            var requestXml = GenerateRequestXml(null, Option.ListCountries);
            var responseModel = Execute(requestXml);
            var countries = (List<CountryModel>)ModelDeserializer.DeserializeListFromXml<CountryModel>(responseModel.ModelXml);

            if (showSelected)
                return countries.Select(a => new SelectListItem { Value = a.ID.ToString(), Text = a.Name, Selected = a.Default }).ToList();
            else
                return countries.Select(a => new SelectListItem { Value = a.ID.ToString(), Text = a.Name }).ToList();
        }

        public JsonResult GetStates(int countryId, int? selectedStateId)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = countryId }, Option.ListStates);
            var responseModel = Execute(requestXml);
            var states = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            if (selectedStateId == null)
                return Json(states.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList(), JsonRequestBehavior.AllowGet);
            else
                return Json(states.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value, Selected = (a.Key == selectedStateId) }).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Add(EntityGenericType type, int parentId, string previousUrl)
        {
            TempData["PreviousUrl"] = previousUrl;

            switch (type)
            {
                case EntityGenericType.Entity:
                    return RedirectToAction("Create", new { pId = Utility.EncryptQueryString(parentId) });
                case EntityGenericType.Client:
                    return RedirectToAction("Create", "Accounts", new { pId = Utility.EncryptQueryString(parentId) });
                case EntityGenericType.Fund:
                    return RedirectToAction("Create", "Fund", new { pId = Utility.EncryptQueryString(parentId) });
                case EntityGenericType.Group:
                    return RedirectToAction("AddGroup", new { pId = Utility.EncryptQueryString(parentId) });
                case EntityGenericType.Job:
                    return RedirectToAction("Create", "Job", new { pId = Utility.EncryptQueryString(parentId) });
                case EntityGenericType.User:
                    return RedirectToAction("Create", "User", new { eId = Utility.EncryptQueryString(parentId) });
            }

            return new HttpNotFoundResult();
        }

        public ActionResult Update(EntityGenericType type, int id, string previousUrl)
        {
            TempData["PreviousUrl"] = previousUrl;

            switch (type)
            {
                case EntityGenericType.Entity:
                    return RedirectToAction("EditFirm", new { firmId = Utility.EncryptQueryString(id) });
                case EntityGenericType.Client:
                    return RedirectToAction("EditAccount", "Accounts", new { accountId = Utility.EncryptQueryString(id) });
                case EntityGenericType.Fund:
                    return RedirectToAction("EditFund", "Fund", new { fundId = Utility.EncryptQueryString(id) });
                case EntityGenericType.Group:
                    return RedirectToAction("EditGroup", new { groupId = Utility.EncryptQueryString(id) });
                case EntityGenericType.Job:
                    return RedirectToAction("EditJob", "Job", new { jobId = Utility.EncryptQueryString(id) });
                case EntityGenericType.User:
                    return RedirectToAction("Edit", "User", new { userId = Utility.EncryptQueryString(id) });
            }

            return new HttpNotFoundResult();
        }

        public JsonResult GetParents(EntityTypes type, bool showSystemEntity = true)
        {
            var requestXml =
                GenerateRequestXml(
                    new DictionaryModel<EntityTypes, int> { Key = type, Value = SessionHelper.UserId }, Option.GetParents);
            var responseModel = Execute(requestXml);

            var model = ModelDeserializer.DeserializeListFromXml<ParentFirmModel>(responseModel.ModelXml);
            if(!showSystemEntity)
                model.RemoveAll(p => p.ID == 1 && p.Type == EntityTypes.Entity);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion Custom Functions
    }
}