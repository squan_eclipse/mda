﻿using System.Globalization;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Ionic.Zip;

namespace DBASystem.SMSF.Web.Controllers
{
    public class ImportsController : BaseController
    {
        public string GetTemplateXML(int id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetTemplate);
            var responseModel = Execute(requestXml);
            var templates = (TemplateModel)ModelDeserializer.DeserializeFromXml<TemplateModel>(responseModel.ModelXml);
            return templates.TemplateText;
        }

        [OptionFilter(Option = Option.WorkPaper)]
        public ActionResult Index(string jobId)
        {
            jobId = Utility.DecryptQueryString(jobId);
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(jobId) }, Option.WorkPaper);
            var responseModel = Execute(requestXml);
            var model = (JobModel)ModelDeserializer.DeserializeFromXml<JobModel>(responseModel.ModelXml);
            return View(model);
        }

        public JsonResult GetTemplateProviders()
        {
            var templateProviders = new List<DictionaryModel<int, string>>();
            var requestXml = GenerateRequestXml(new GenericModel<int?>(), Option.GetTemplateProviders);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
                templateProviders = ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);
            return Json(templateProviders, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTemplateCategories(string templateProviderId)
        {
            var templateCategories = new List<DictionaryModel<int, string>>();

            if (string.IsNullOrEmpty(templateProviderId))
                templateProviderId = "1";

            var requestXml = GenerateRequestXml(
                new GenericModel<int?> { Value = Convert.ToInt32(templateProviderId) }, Option.GetTemplateCategories);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
                templateCategories =
                    ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);
            return Json(templateCategories, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTemplatesBy(string templateCategoryId)
        {
            var templates = new List<DictionaryModel<int, string>>();

            if (string.IsNullOrEmpty(templateCategoryId))
                templateCategoryId = "12";

            var requestXml = GenerateRequestXml(new GenericModel<int?> { Value = Convert.ToInt32(templateCategoryId) }, Option.GetTemplatesBy);
            var responseModel = Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
                templates = ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);
            return Json(templates, JsonRequestBehavior.AllowGet);
        }

        public string GetUploadedFileString(bool isPartial, JobsImportedDataModel jobsImportedDataModel,
            string templateXml, Stream fileBase)
        {
            string output;
            try
            {
                var template = templateXml;
                var engine = new Parser.Engine();
                engine.Load(fileBase, template);
                engine.Run();

                output = !engine.hasError
                    ? engine.output.ToString()
                    : string.Format("<b>Error in parsing= [{0}]</b>", engine.lastError);

                var httpPostedFileBase = Request.Files[0];
                if (httpPostedFileBase != null) ViewBag.FileName = httpPostedFileBase.FileName;
                SessionHelper.TemplateDataFileUploadStream = output;
            }
            catch (Exception e)
            {
                output = string.Format("<b>Error in parsing= [{0}]</b>", e.Message);
            }

            ViewBag.output = output;
            return ViewBag.output;
        }

        public JsonResult Parse(List<HttpPostedFileBase> fileUploader, int templateId, int jobId)
        {
            string output;
            var base64File = string.Empty;
            int templateTypeId = 0;
            try
            {
                if (jobId == 0) // single file
                {
                    base64File = Parsing(fileUploader, templateId);
                    output = ConvertXmlToJson(SessionHelper.TemplateDataFileUploadStream);
                }
                else // zip file
                {
                    string fileName = Path.GetFileName(fileUploader[0].FileName);
                    var guid = Guid.NewGuid();
                    var pathZip = Path.Combine(string.Format("{0}{1}_{2}", Server.MapPath("~/App_Data/"), guid, fileName));
                    string pathExtracted;
                    ///////////////////////////////////////////////////////////////
                    //1. Save in App_Data
                    ///////////////////////////////////////////////////////////////
                    fileUploader[0].SaveAs(pathZip);
                    ///////////////////////////////////////////////////////////////
                    //2. Extract
                    ///////////////////////////////////////////////////////////////
                    using (ZipFile zip = ZipFile.Read(pathZip))
                    {
                        pathExtracted = string.Format("{0}/{1}_ExtractFiles", Server.MapPath("~/App_Data"), guid);
                        zip.ExtractAll(pathExtracted, ExtractExistingFileAction.DoNotOverwrite);
                    }
                    ///////////////////////////////////////////////////////////////
                    //3. Iterate and Process Individual Files
                    ///////////////////////////////////////////////////////////////
                    var files = Directory.EnumerateFiles(pathExtracted);
                    var sb = new StringBuilder();

                    foreach (var file in files)
                    {
                        fileName = file.Substring(file.LastIndexOf("\\", StringComparison.Ordinal) + 1);

                        switch (fileName.Split('.')[1].ToString())
                        {
                            case "xls":
                                templateTypeId = GetBGLTemplateID(fileName, templateTypeId);
                                break;

                            case "pdf":
                                templateTypeId = GetDesktopSuperTemplateID(fileName.Split('.')[0].ToLower(), templateTypeId);
                                break;
                        }


                        if (templateTypeId == 0)
                        {
                            if (sb.Length == 0)
                            {
                                sb.AppendLine("Following file(s) have FAILED to import. <hr>");
                            }
                            sb.AppendLine(string.Format("<li> {0} </li>", fileName));
                            continue;
                        }
                        var templateXml = GetTemplateXML(templateTypeId);

                        var engine = new Parser.Engine();
                        engine.Load(templateXml, file);
                        engine.Run();

                        output = engine.output.ToString();
                        SessionHelper.TemplateDataFileUploadStream = output;

                        var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
                        base64File = fileStream.ToBase64String();

                        Save(jobId.ToString(CultureInfo.InvariantCulture), templateTypeId.ToString(CultureInfo.InvariantCulture), fileName, base64File);
                        templateTypeId = 0;
                    }

                    if (sb.Length > 0)
                    {
                        sb.Insert(0, "<list>");
                        sb.AppendLine("</list><hr>");
                        sb.AppendLine("<b>Details:</b>  Either File Name(s) are incorrect Or Templates are not available for them.");
                    }

                    output = sb.ToString();
                    output = output == string.Empty ? "Zip file is successfully imported." : output;
                    ///////////////////////////////////////////////////////////////
                    //4. Delete Files
                    ///////////////////////////////////////////////////////////////
                    System.IO.File.Delete(pathZip);
                    Directory.Delete(pathExtracted, true);
                }
            }
            catch (Exception ex)
            {
                return
                    Json(
                        new
                        {
                            status = "error",
                            message = "Unable to parse the uploaded file.<br />" + ex.Message
                        }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = false, fileName = fileUploader[0].FileName, message = output, Base64File = base64File }, JsonRequestBehavior.AllowGet);
        }

        private int GetDesktopSuperTemplateID(string fileName, int templateTypeId)
        {
            if (fileName.Contains("cash flow") || fileName.Contains("cashflow") || fileName.Contains("cashflows") ||
                fileName.Contains("statement of cashflows"))
                templateTypeId = (int) ImportTemplate.DSStatementOfCashflows;

            if (fileName.Contains("financials") || fileName.Contains("financial position") ||
                fileName.Contains("statement of financial position"))
                templateTypeId = (int) ImportTemplate.DSStatementOfFinancialPosition;

            if (fileName.Contains("general ledger") || fileName.Contains("ledger"))
                templateTypeId = (int)ImportTemplate.DSGeneralLedger;
            
            if (fileName.Contains("investment income") || fileName.Contains("investment income summary"))
                templateTypeId = (int)ImportTemplate.DSInvestmentIncomeSummary;

            if (fileName.Contains("investment movement"))
                templateTypeId = (int)ImportTemplate.DSInvestmentMovement;

            if (fileName.Contains("investment summary"))
                templateTypeId = (int)ImportTemplate.DSInvestmentSummary;

            if (fileName.Contains("operating statement"))
                templateTypeId = (int)ImportTemplate.DSOperatingStatement;

            if (fileName.Contains("realised cgt"))
                templateTypeId = (int)ImportTemplate.DSRealisedCGT;

            if (fileName.Contains("tax reconciliation"))
                templateTypeId = (int)ImportTemplate.DSTaxReconciliation;

            if (fileName.Contains("trial balance"))
                templateTypeId = (int)ImportTemplate.DSTrialBalance;

            if (fileName.Contains("unrealised cgt"))
                templateTypeId = (int)ImportTemplate.DSUnrealisedCGT;

            return templateTypeId;
        }

        private int GetBGLTemplateID(string fileName, int templateTypeId)
        {
            switch (fileName.Split('.')[0].ToLower())
            {
                case "cash flow":
                case "cashflow":
                case "cashflows":
                    templateTypeId = (int)ImportTemplate.BGLStatementCashflows;
                 break;
                case "balance sheet":
                 templateTypeId = (int)ImportTemplate.BGLStatementFinancialPosition;
                    break;
                case "investment disposal":
                case "investment disposals":
                    templateTypeId = (int)ImportTemplate.BGLInvestmentDisposals;
                    break;
                case "investment summary":
                    templateTypeId = (int)ImportTemplate.BGLInvestmentSummary;
                    break;
                case "notes to financial statement":
                case "notes to fs":
                    templateTypeId = (int)ImportTemplate.BGLNotesFinancialStatement;
                    break;
                case "operating statement":
                    templateTypeId = (int)ImportTemplate.BGLOperatingStatement;
                    break;
                case "income statement":
                    templateTypeId = (int)ImportTemplate.BGLIncomeStatement;
                    break;
                case "investment income":
                    templateTypeId = (int)ImportTemplate.BGLInvestmentIncome;
                    break;
                case "investment movement":
                    templateTypeId = (int)ImportTemplate.BGLInvestmentMovement;
                    break;
                case "member statement":
                    templateTypeId = (int)ImportTemplate.BGLMemberStatement;
                    break;
            }
            return templateTypeId;
        }

        public string Parsing(List<HttpPostedFileBase> fileUploader, int templateId)
        {
            var templateXml = GetTemplateXML(templateId);
            var output = string.Empty;
            string base64File = string.Empty;

            switch (fileUploader[0].ContentType)
            {
                case "application/pdf":
                    var fileStream = fileUploader[0].InputStream.CopyStream();
                    if (templateXml.LastIndexOf("<template") > 0)
                    {
                        templateXml = templateXml.Split(new string[] {"<template"}, StringSplitOptions.None)[2];
                        templateXml = string.Format("<template {0}", templateXml);
                    }
                    output = GetUploadedFileString(true, null, templateXml, fileStream);
                    base64File = fileStream.ToBase64String();
                    break;

                case "application/vnd.ms-excel":
                case "application/octet-stream":
                    var fileName = Path.GetFileName(fileUploader[0].FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/"), Guid.NewGuid() + fileName);
                    fileUploader[0].SaveAs(path);

                    var engine = new Parser.Engine();
                    if (templateXml.LastIndexOf("<template") > 0)
                    {
                        templateXml = templateXml.Split(new string[] {"<template"}, StringSplitOptions.None)[1];
                        templateXml = string.Format("<template {0}", templateXml);
                    }
                    engine.Load(templateXml, path);
                    engine.Run();
                    output = engine.output.ToString();
                    System.IO.File.Delete(path);
                    base64File = fileUploader[0].InputStream.ToBase64String();
                    break;
            }

            SessionHelper.TemplateDataFileUploadStream = output;
            return base64File;
        }

        private static string HtmlTable(string xml)
        {
            var sbOutPut = new StringBuilder();
            sbOutPut.Append(xml);

            #region Header

            sbOutPut.Replace("report>", "table>");
            sbOutPut.Replace("header>", "thead>");
            sbOutPut.Replace("header>", "thead>");
            sbOutPut.Replace("main-heading>", "tr>");
            sbOutPut.Replace("second-heading>", "tr>");
            sbOutPut.Replace("columns>", "tr>");
            sbOutPut.Replace("column>", "th>");

            #endregion Header

            #region Body

            sbOutPut.Replace("data>", "tbody>");
            sbOutPut.Replace("row>", "tr>");
            sbOutPut.Replace("item-heading>", "td>");
            sbOutPut.Replace("item>", "td>");
            sbOutPut.Replace("item-total>", "td>");

            #endregion Body

            return sbOutPut.ToString();
        }

        [ValidateInput(false)]
        public string Save(string jobId, string templateId, string fileName, string base64File)
        {
            var jobsImportedDataModel = new JobsImportedDataModel { UploadFileInfo = new Contracts.ViewModels.FileInfo { FileName = fileName, FileData = base64File }, JobId = Convert.ToInt32(jobId), TemplateId = Convert.ToInt32(templateId), XMLData = SessionHelper.TemplateDataFileUploadStream, CreatedOn = DateTime.Now, CreatedBy = SessionHelper.UserId };

            var requestXml = GenerateRequestXml(jobsImportedDataModel, Option.AddUpdateJobsImportedData);
            var responseModel = Execute(requestXml);
            ViewBag.Status = responseModel.Status == ResponseStatus.Success ? 1 : 0;
            return "Success";
        }

        public ActionResult JobImportReview(int id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = id }, Option.GetJobInfo);
            var responseModel = Execute(requestXml);
            var model = (JobCreateModel)ModelDeserializer.DeserializeFromXml<JobCreateModel>(responseModel.ModelXml);
            return View(model);
        }

        public ActionResult GetJobFiles(int jobId)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = jobId }, Option.GetJobFiles);
            var responseModel = Execute(requestXml);
            var models = ModelDeserializer.DeserializeListFromXml<JobImportedDataViewModel>(responseModel.ModelXml);

            return Json(models, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetJobImportedFiles(int jobId)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> {Value = jobId}, Option.GetJobsImportedFiles);
            var responseModel = Execute(requestXml);
            if (responseModel.Status != ResponseStatus.Success)
                return Json(new JobImportedFileViewModel(), JsonRequestBehavior.AllowGet);
            var model = ModelDeserializer.DeserializeListFromXml<JobImportedFileViewModel>(responseModel.ModelXml);

            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult BalanceSheetReview(string id)
        {
            var jobId = Utility.DecryptQueryString(id);
            return View(GetImportData(jobId.ToType<int>(), new List<int>{(int)ImportTemplate.BGLStatementFinancialPosition,(int)ImportTemplate.CSFinancialPosition,(int)ImportTemplate.SF360BalanceSheet}));
        }

        public ActionResult IncomeStatementReview(string id)
        {
            var jobId = Utility.DecryptQueryString(id);
            return View(GetImportData(jobId.ToType<int>(), new List<int>{(int)ImportTemplate.BGLIncomeStatement}));
        }

        private JobImportedDataViewModel GetImportData(int jobId, List<int> templeteTypes)
        {
            string joined = string.Join(",", templeteTypes.ToArray());
            var requestXml = GenerateRequestXml(new DictionaryModel<int, List<int>> { Key = jobId, Value = templeteTypes}, Option.GetJobImportData);
            var responseModel = Execute(requestXml);

            var model = (JobImportedDataViewModel)ModelDeserializer.DeserializeFromXml<JobImportedDataViewModel>(responseModel.ModelXml);
            if (model != null)
            {
                model.JsonData = ConvertXmlToJson(model.XmlData).Replace("'", "\'");
                model.JsonData = model.JsonData.Replace("'", "");
                model.XmlData = String.Empty;
                model.JobId = jobId;
                return model;
            }
            model = new JobImportedDataViewModel();
            model.JobId = jobId;
            return model;
        }

        #region

        public ActionResult GetJobsImportedData(int jobId, int tabId, int jobsImportedDataId)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> { Key = jobId, Value = jobsImportedDataId }, Option.GetJobsImportedData);
            var responseModel = Execute(requestXml);
            if (responseModel.Status != ResponseStatus.Success)
                return Json(new JobImportedDataViewModel(), JsonRequestBehavior.AllowGet);
            var model = (JobImportedDataViewModel)ModelDeserializer.DeserializeFromXml<JobImportedDataViewModel>(responseModel.ModelXml);
            model.JsonData = ConvertXmlToJson(model.XmlData);
            model.XmlData = "";
            int publicUrlExipry = System.Configuration.ConfigurationManager.AppSettings["SharePointPublicUrlExpiry"].ToType<int>();
            model.SourceFile.PublicUrlExipry = publicUrlExipry;

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        private static string ConvertXmlToJson(string xml)
        {
            var doc = new XmlDocument();
            doc.LoadXml(xml.Replace("&", "&amp;"));
            return Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
        }

        #endregion
    }
}