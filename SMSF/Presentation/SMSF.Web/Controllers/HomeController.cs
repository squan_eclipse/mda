﻿using System.Collections.Generic;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using System;
using System.Configuration;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class CarModel
    {
        public int Year { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public decimal Price { get; set; }
        public int Id { get; set; }
    }

    public class CarRepository
    {
        private IList<CarModel> _cars;
        public CarRepository()
        {
            InitializeList();
        }

        private void InitializeList()
        {
            _cars = new List<CarModel>();
            _cars.Add(new CarModel { Id = 0, Year = 2000, Make = "Hyundai", Model = "Elantra", Color = "Bluish", Price = 6000m });
            _cars.Add(new CarModel { Id = 1, Year = 2001, Make = "Hyundai", Model = "Sonata", Color = "Greenish", Price = 16000m });
            _cars.Add(new CarModel { Id = 2, Year = 2002, Make = "Toyota", Model = "Corolla", Color = "Grayish", Price = 3000m });
            _cars.Add(new CarModel { Id = 3, Year = 2003, Make = "Toyota", Model = "Yaris", Color = "Egg White", Price = 26000m });
            _cars.Add(new CarModel { Id = 4, Year = 2004, Make = "Honda", Model = "CRV", Color = "Incandescent Yellow", Price = 1000m });
            _cars.Add(new CarModel { Id = 5, Year = 2005, Make = "Honda", Model = "Accord", Color = "Dark Black (?)", Price = 3000m });
            _cars.Add(new CarModel { Id = 6, Year = 2000, Make = "Honda", Model = "Accord", Color = "Red (white stripes)", Price = 9000m });
            _cars.Add(new CarModel { Id = 7, Year = 2002, Make = "Kia", Model = "Sedona", Color = "Black", Price = 6000m });
            _cars.Add(new CarModel { Id = 8, Year = 2004, Make = "Fiat", Model = "One", Color = "White", Price = 5000m });
            _cars.Add(new CarModel { Id = 9, Year = 2005, Make = "BMW", Model = "M3", Color = "Orangey", Price = 62000m });
            _cars.Add(new CarModel { Id = 10, Year = 2008, Make = "BMW", Model = "X5", Color = "Coco", Price = 8000m });

        }

        public IList<CarModel> All()
        {
            return _cars;
        }
    }

    [UserAuthorize]
    public class HomeController : BaseController
    {
        public JsonResult GetCars()
        {
            CarRepository repository = new CarRepository();

            var carList = repository.All();
            return Json(carList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index(string type)
        {
            SessionHelper.SelectedMenu = string.IsNullOrEmpty(type) ? "Dashboard" : type;
            ViewBag.Message = TempData["Message"];

            return View();
        }

        public ActionResult NotImplemented(string item)
        {
            SessionHelper.SelectedMenu = item;
            ViewBag.Message = "This page is not implemented yet";

            return View();
        }
        
        public ActionResult Settings()
        {
            SessionHelper.SelectedMenu = "Settings";
            return View();
        }

        public void InsertError(Exception ex)
        {
            var message = ex.Message;
            var messageDetail = ex.InnerException != null ? ex.InnerException.Message : null;

            var requestXml = base.GenerateRequestXml(new ErrorModel { Message = message, MessageDetail = messageDetail, UserID = SessionHelper.UserId }, Option.AddError);
            var responseModel = base.Execute(requestXml);
        }
    }
}
