﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class FundController : BaseController
    {
        public ActionResult HasMultipleAccess()
        {
            var requestXml =
                GenerateRequestXml(
                    new DictionaryModel<int, EntityTypes> { Key = SessionHelper.UserId, Value = EntityTypes.Fund },
                    Option.HasMultipleAccess);
            var responseModel = Execute(requestXml);
            var models = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

            return models.Value == 0
                ? RedirectToAction("Index")
                : RedirectToAction("Detail", new {id = Utility.EncryptQueryString(models.Value)});
        }

        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Funds";

            ViewBag.EntityType = (int)EntityTypes.Fund;
            ViewBag.ControllerName = "Fund";

            if (TempData["serachData"] != null)
            {
                var model = (SearchFilterModel)TempData["serachData"];

                ViewBag.Firm = string.IsNullOrEmpty(model.Firm) ? "" : model.Firm;
                ViewBag.Account = string.IsNullOrEmpty(model.Account) ? "" : model.Account;
                ViewBag.ABN = string.IsNullOrEmpty(model.ABN) ? "" : model.ABN;
                ViewBag.AccountManager = string.IsNullOrEmpty(model.AccountManager) ? "" : model.AccountManager;
                ViewBag.PrimaryContact = string.IsNullOrEmpty(model.PrimaryContact) ? "" : model.PrimaryContact;
                ViewBag.DateFrom = model.DateFrom == null ? "" : Convert.ToDateTime(model.DateFrom).ToShortDateString();
                ViewBag.DateTo = model.DateTo == null ? "" : Convert.ToDateTime(model.DateTo).ToShortDateString();
            }
            else
            {                
                ViewBag.Firm = "";
                ViewBag.Account = "";
                ViewBag.ABN = "";
                ViewBag.AccountManager = "";
                ViewBag.PrimaryContact = "";
                ViewBag.DateFrom = "";
                ViewBag.DateTo = "";
            }

            return View();
        }

        public ActionResult Detail(string id)
        {
            id = Utility.DecryptQueryString(id);
            SessionHelper.SelectedMenu = "Funds";

            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(id) }, Option.GetFundDetail);
            var responseModel = Execute(requestXml);
            var model = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);

            return FundDetailView(model);
        }

        private ActionResult FundDetailView(FirmDetailModels model)
        {
            return View(@"~/Views/Firm/Detail.cshtml", model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateFund)]
        public ActionResult UpdateFund(FirmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.ModifiedOn = DateTime.Now;
                model.ModifiedBy = SessionHelper.UserId;

                var requestXml = GenerateRequestXml(model, Option.UpdateFund);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var reponseModel = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);
                    if (responseModel.Status == ResponseStatus.Warning)
                    {
                        ViewBag.WarningMessage = responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage + ",however " + model.EntityType.ToString() + " has been updated successfully , please contact your administrator."; ;
                    }
                    return FundDetailView(reponseModel);
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(@"~/Views/Firm/Create.cshtml", model);
                }
            }
            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.UpdateFund)]
        public ActionResult EditFund(string fundId)
        {
            ViewBag.EncryptedID = fundId;
            fundId = Utility.DecryptQueryString(fundId);

            SessionHelper.SelectedMenu = "Funds";
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(fundId) }, Option.GetFirm);
            var responseModel = Execute(requestXml);
            var model = (FirmCreateModel)ModelDeserializer.DeserializeFromXml<FirmCreateModel>(responseModel.ModelXml);

            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.AddFund)]
        public ActionResult Create(string pId)
        {
            ViewBag.EncryptedID = pId;
            pId = Utility.DecryptQueryString(pId);

            SessionHelper.SelectedMenu = "Funds";
            SessionHelper.ListAddresses = null;
            SessionHelper.ListUsers = null;

            var model = new FirmCreateModel { ParentID = Convert.ToInt32(pId), EntityType = EntityTypes.Fund };
            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateFund)]
        public ActionResult CreateFund(FirmCreateModel model)
        {
            if (ModelState.IsValid)
            {
                model.CreatedOn = DateTime.Now;
                model.CreatedBy = SessionHelper.UserId;
                model.ListUser = SessionHelper.ListUsers;

                var requestXml = GenerateRequestXml(model, Option.AddFund);
                var responseModel = Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success || responseModel.Status == ResponseStatus.Warning)
                {
                    var detailModel = (FirmDetailModels)ModelDeserializer.DeserializeFromXml<FirmDetailModels>(responseModel.ModelXml);
                    if (responseModel.Status == ResponseStatus.Warning)
                    {
                        ViewBag.WarningMessage = responseModel.ResponseMessage = "<b>[Warning]</b><br />" + responseModel.ResponseMessage + ",however " + model.EntityType.ToString() + " has been created successfully , please contact your administrator."; ;
                    }
                    return RedirectToAction("Detail", new { id = Utility.EncryptQueryString(detailModel.ID) });
                }
                else
                {
                    ModelState.AddModelError("", responseModel.ResponseMessage);
                    return View(@"~/Views/Firm/Create.cshtml", model);
                }
            }

            return View(@"~/Views/Firm/Create.cshtml", model);
        }

        public JsonResult IndexSearch(SearchFilterModel searchModel)
        {
            searchModel.UserID = SessionHelper.UserId;
            searchModel.EntityType = EntityTypes.Fund;

            var requestXml = GenerateRequestXml(searchModel, Option.GetFunds);
            var responseModel = Execute(requestXml);
            var model = (List<FirmModels>)ModelDeserializer.DeserializeListFromXml<FirmModels>(responseModel.ModelXml);

            return
                Json(
                    new
                    {
                        data = model,
                        message = responseModel.Status == ResponseStatus.Success ? "" : responseModel.ResponseMessage
                    });
        }

        public ActionResult IndexSearchWithFilter(string searchModelstring)
        {
            var searchModel = JsonConvert.DeserializeObject<SearchFilterModel>(searchModelstring, new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy" });
            TempData["serachData"] = searchModel;

            return RedirectToAction("Index", "Fund");
        }

        [HttpGet]
        public JsonResult GetFundJobs(int id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int>() { Value = id }, Option.GetFundJobs);
            var responseModel = Execute(requestXml);

            var model = (List<FundsJobView>)ModelDeserializer.DeserializeListFromXml<FundsJobView>(responseModel.ModelXml);

            return Json(new { Jobs = model, Status = responseModel.Status.ToString(), Message = responseModel.Status == ResponseStatus.Success ? "" : responseModel.ResponseMessage }, JsonRequestBehavior.AllowGet);
        }
    }
}