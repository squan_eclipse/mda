﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class DataFeedController : Controller
    {
        //
        // GET: /DataFeed/

        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Firms";

            ViewBag.EntityType = (int)EntityTypes.IFM;
            ViewBag.ControllerName = "Firm";
            return View();
        }

        //
        // GET: /DataFeed/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /DataFeed/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DataFeed/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /DataFeed/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /DataFeed/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /DataFeed/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /DataFeed/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
