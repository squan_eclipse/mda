﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace DBASystem.SMSF.Web.Controllers
{
    public class AccountController : BaseController
    {
        #region Login
        
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, bool? hashPassword)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public JsonResult Login(LoginModel model, string returnUrl, bool? hashPassword)
        {
            if (ModelState.IsValid)
            {                
                if (hashPassword != true)
                    model.Password = CommonFunction.HashPassword(model.Password);

                var requestXml = base.GenerateRequestXml(model, Option.Login);

                var responseModel = base.Execute(requestXml);

                if (responseModel.Status == ResponseStatus.Success)
                {
                    if (responseModel.StatusMessage == Resource_EN.Error_MultipleUsersFound)
                    {
                        var users = (List<UserModel>)ModelDeserializer.DeserializeListFromXml<UserModel>(responseModel.ModelXml);

                        return Json(new { status = "multipleusersfound", users = users });
                    }
                    else
                    {
                        var user = (UserModel)ModelDeserializer.DeserializeFromXml<UserModel>(responseModel.ModelXml);

                        if (user != null)
                        {
                            AuthenticateUser(user);

                            return Json(new { status = "success" });
                            //return RedirectToAction("Index", "Home");
                        }
                    }
                }
                else
                {
                    //ModelState.AddModelError("", responseModel.ResponseMessage);
                    return Json(new { status = "failure", error = responseModel.ResponseMessage});
                    //return View(model);
                }
            }

            //ModelState.AddModelError("", "The user name or password provided is incorrect.");
            return Json(new { status = "error", error = Resource_EN.Error_InvalidUsernameOrPassword });
            //return View(model);
        }

        private void AuthenticateUser(UserModel user)
        {
            Session.Timeout = user.SystemSettings.SessionSetting.MaxSessionTimeout;
            SessionHelper.UserId = user.ID;
            SessionHelper.UserName = user.Name;
            SessionHelper.AuthCode = user.AuthCode;
            SessionHelper.Name = user.FirstName + " " + user.LastName;
            SessionHelper.Email = user.Email;
            SessionHelper.EntityID = user.Entity.EntityID;
            SessionHelper.EntityType = user.EntityType;
            SessionHelper.Group = user.Group;
            SessionHelper.AllowedPages = GetAllowedPages(Option.AllowedPages);
            SessionHelper.AllowedOptions = GetAllowedPages(Option.AllowedOptions);
            SessionHelper.SelectedParentEntity = user.Entity.EntityID;
            SessionHelper.UserImage = user.UserImage == null ? null : Convert.FromBase64String(user.UserImage);
            SessionHelper.UserIdentifier = user.UserIdentifier;
            SessionHelper.GroupIdentifier = user.GroupIdentifier;
            SessionHelper.Entity = user.Entity;
            SessionHelper.Client = user.Client;

            SessionHelper.VersionNumber = user.SystemSettings.VersionSetting.VersionNumber;

            var identity = new UserIdentity(user.ID, user.Name, true);
            System.Threading.Thread.CurrentPrincipal = new UserPrincipal(identity);

            var authTicket = new FormsAuthenticationTicket(user.Name, true, DateTime.Now.AddHours(24).Minute);

            var encryptedTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
            Response.Cookies.Add(authCookie);
        }

        [HttpPost]
        [AllowAnonymous]
        public void LoginUser(UserModel user)
        {
            AuthenticateUser(user);
            var requestXml = base.GenerateRequestXml(new GenericModel<int> { Value = user.ID }, Option.UpdateLastLogin);
            var responseModel = base.Execute(requestXml);
        }

        private List<Option> GetAllowedPages(Option option)
        {
            OptionModel optionModel = new OptionModel();
            optionModel.OptionType = OptionType.Page;
            optionModel.UserID = SessionHelper.UserId;

            var requestXml = base.GenerateRequestXml(optionModel, option);
            var responseModel = base.Execute(requestXml);
            return (List<Option>)ModelDeserializer.DeserializeListFromXml<Option>(responseModel.ModelXml);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return RedirectToAction("Login");
        } 

        #endregion

        #region ForgotPassowrd
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword(string EmailId, int EntityID, string Password)
        {
            string baseUrl = Request.Url.GetLeftPart(UriPartial.Authority);
            var requestXml = base.GenerateRequestXml(new GenericModel<ForgotPasswordModel> { Value = new ForgotPasswordModel { BaseUrl= baseUrl, EmailAddress= EmailId,EntityID= EntityID, NewPassword = CommonFunction.HashPassword(Password)  } }, Option.SendForgotPassEmail);
            var responseModel = base.Execute(requestXml);

            return Json(responseModel.ResponseMessage.ToString(),JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetAssociatedEnties(string EmailId)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<string> { Value = EmailId }, Option.GetUerAssociatedEntites);
            var responseModel = base.Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
            {
                var Entites = (List<AssociatedEntites>)ModelDeserializer.DeserializeListFromXml<AssociatedEntites>(responseModel.ModelXml);

                return Json(Entites, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;

                return Json(new { message = responseModel.ResponseMessage}, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public ActionResult PasswordReset(string authCode)
        {
            ViewBag.Status = ResetPassword(authCode);
            return View();
        }

        private string ResetPassword(string authID)
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<string> { Value = authID }, Option.ResetPassword);
            var responseModel = base.Execute(requestXml);
            if (responseModel.Status == ResponseStatus.Success)
                return "Success";
            else
                return responseModel.ResponseMessage;
        }

        #endregion

        #region Common Function
        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetPasswordPolicy()
        {
            var requestXml = base.GenerateRequestXml(new GenericModel<SettingTypes> { Value = SettingTypes.PasswordSettings }, Option.GetSettingByType);
            var responseModel = base.Execute(requestXml);
            var passwordRegex = (GenericModel<PasswordPolicyModel>)ModelDeserializer.DeserializeFromXml<GenericModel<PasswordPolicyModel>>(responseModel.ModelXml);
            return Json(passwordRegex, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
