﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class UserController : BaseController
    {
        public ActionResult HasMultipleAccess()
        {
            var requestXml =
                GenerateRequestXml(
                    new GenericModel<int> { Value = SessionHelper.UserId },
                    Option.HasMultipleUserAccess);
            var responseModel = Execute(requestXml);
            var models = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);

            return models.Value == 0
                ? RedirectToAction("Index")
                : RedirectToAction("Detail", new {id = Utility.EncryptQueryString(models.Value)});
        }

        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Users";

            ViewBag.EntityType = (int)EntityTypes.User;
            ViewBag.ControllerName = "User";
            if (TempData["serachUserData"] != null)
            {
                SearchFilterModel model = (SearchFilterModel)TempData["serachUserData"];

                ViewBag.Firm = string.IsNullOrEmpty(model.Firm) ? "" : model.Firm;
                ViewBag.Account = string.IsNullOrEmpty(model.Account) ? "" : model.Account;
                ViewBag.DateFrom = model.DateFrom == null ? "" : Convert.ToDateTime(model.DateFrom).ToShortDateString();
                ViewBag.DateTo = model.DateTo == null ? "" : Convert.ToDateTime(model.DateTo).ToShortDateString();
            }
            else
            {
                ViewBag.Firm = "";
                ViewBag.Account = "";
                ViewBag.DateFrom = "";
                ViewBag.DateTo = "";
            }

            return View();
        }

        public JsonResult IndexSearch(SearchFilterModel searchModel)
        {
            searchModel.EntityID = SessionHelper.EntityID;
            searchModel.UserID = SessionHelper.UserId;
            searchModel.EntityType = EntityTypes.User;

            var requestXml = GenerateRequestXml(searchModel, Option.GetAccountManagers);
            var responseModel = Execute(requestXml);
            var managers = (List<AccountManagerViewModel>)ModelDeserializer.DeserializeListFromXml<AccountManagerViewModel>(responseModel.ModelXml);

            return
                Json(
                    new
                    {
                        data = managers,
                        message = responseModel.Status == ResponseStatus.Success ? "" : responseModel.ResponseMessage
                    });
        }

        public ActionResult IndexSearchWithFilter(string searchModelstring)
        {
            var searchModel = JsonConvert.DeserializeObject<SearchFilterModel>(searchModelstring, new JsonSerializerSettings { DateFormatString = "dd/MM/yyyy" });
            TempData["serachUserData"] = searchModel;

            return RedirectToAction("Index", "User");
        }

        public ActionResult Detail(string id)
        {
            id = Utility.DecryptQueryString(id);
            SessionHelper.SelectedMenu = "Users";
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(id) }, Option.GetAccountManagerDetail);
            var responseModel = Execute(requestXml);
            var model = (AccountManagerDetailModel)ModelDeserializer.DeserializeFromXml<AccountManagerDetailModel>(responseModel.ModelXml);
            return View("Detail", model);
        }

        [HttpGet]
        [OptionFilter(Option = Option.CreateAccountManager)]
        public ActionResult Create(string eId)
        {
            ViewBag.EncryptedID = eId;
            eId = Utility.DecryptQueryString(eId);
            SessionHelper.SelectedMenu = "Users";
            SessionHelper.ListContacts = null;

            var managerCreateModel = new AccountManagerCreateModel {EntityID = Convert.ToInt32(eId)};

            return View(managerCreateModel);
        }

        [HttpPost]
        [OptionFilter(Option = Option.CreateAccountManager)]
        public ActionResult Create(AccountManagerCreateModel model)
        {
            model.ListContactInfo = SessionHelper.ListContacts;
            model.CreatedOn = DateTime.Now;
            model.CreatedBy = SessionHelper.UserId;
            model.Status = Status.Active;

            if (!model.HasLogin)
                model.User = null;
            else
            {
                if (model.User.GroupID == 0)
                {
                    ModelState.AddModelError("", Resource_EN.Error_PleaseSelectGroup);
                    return View(model);
                }

                if (!string.IsNullOrEmpty(model.User.Password))
                    model.User.Password = CommonFunction.HashPassword(model.User.Password);
            }

            var requestXml = GenerateRequestXml(model, Option.CreateAccountManager);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var newUserid = ((GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml)).Value;
                return RedirectToAction("Detail", new {id = Utility.EncryptQueryString(newUserid)});
            }
            else
            {
                ModelState.AddModelError("", responseModel.ResponseMessage); // showing httml without render
                return View(model);
            }
        }

        [HttpGet]
        [OptionFilter(Option = Option.UpdateAccountManager)]
        public ActionResult Edit(string userId)
        {
            ViewBag.EncryptedID = userId;
            userId = Utility.DecryptQueryString(userId);
            SessionHelper.SelectedMenu = "Users";
            SessionHelper.ListContacts = null;

            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = Convert.ToInt32(userId) }, Option.GetAccountManager);
            var responseModel = Execute(requestXml);
            var model = (AccountManagerCreateModel)ModelDeserializer.DeserializeFromXml<AccountManagerCreateModel>(responseModel.ModelXml);

            SessionHelper.ListContacts = model.ListContactInfo;

            return View("Create", model);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UpdateAccountManager)]
        public ActionResult Edit(AccountManagerCreateModel model)
        {
            model.ListContactInfo = SessionHelper.ListContacts;
            model.ModifiedBy = SessionHelper.UserId;
            model.ModifiedOn = DateTime.Now;

            if (!model.HasLogin)
                model.User = null;
            else
                if (!string.IsNullOrEmpty(model.User.Password))
                    model.User.Password = CommonFunction.HashPassword(model.User.Password);

            var requestXml = GenerateRequestXml(model, Option.UpdateAccountManager);
            var responseModel = Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var detailModel = (AccountManagerDetailModel)ModelDeserializer.DeserializeFromXml<AccountManagerDetailModel>(responseModel.ModelXml);
                return RedirectToAction("Detail", new { id = Utility.EncryptQueryString(detailModel.ID) });
            }
            else
            {
                ModelState.AddModelError("", responseModel.ResponseMessage);
                return View("Create", model);
            }
        }

        [OptionFilter(Option = Option.DeleteUser)]
        public JsonResult Delete(int id)
        {
            if (id == SessionHelper.UserId)
                return Json(new { result = false, ResponseMessage = Resource_EN.Error_CannotDeleteLoggedInUser }, JsonRequestBehavior.AllowGet);

            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> { Key = id, Value = SessionHelper.UserId }, Option.DeleteUser);
            var responseModel = Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);

            return
                Json(
                    status.Value
                        ? new { result = status.Value, ResponseMessage = "" }
                        : new { result = status.Value, ResponseMessage = responseModel.ResponseMessage },
                    JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [OptionFilter(Option = Option.UnlockUser)]
        public JsonResult Unlock(int id)
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = id }, Option.UnlockUser);
            var responseModel = Execute(requestXml);
            
            return Json(new { result = responseModel.Status.ToString(), ResponseMessage = responseModel.ResponseMessage },
                    JsonRequestBehavior.AllowGet);
        }

        #region Contact Info

        [HttpGet]
        public ActionResult CreateContactInfo()
        {
            var model = new ContactCreateModel();
            model.ListTypes = GetContactTypes();

            return PartialView("_CreateContactInfo", model);
        }

        [HttpPost]
        public ActionResult CreateContactInfo(ContactCreateModel model)
        {
            var list = SessionHelper.ListContacts;
            if (list == null) list = new List<ContactCreateModel>();

            model.CreatedBy = SessionHelper.UserId;
            model.CreatedOn = DateTime.Now;

            if (model.ID != 0)
            {
                list = list.Where(a => a.ID != model.ID).ToList();
                list.Add(model);
            }
            else
            {
                model.StatusID = Status.Active;
                model.ID = list.Any() ? (list.Max(a => a.ID) + 1) * -1 : -1;
                list.Add(model);
            }
            SessionHelper.ListContacts = list;

            return Json("success");
        }

        [HttpPost]
        public ActionResult UpdateContactInfo(ContactCreateModel model)
        {
            var list = SessionHelper.ListContacts;
            if (list == null) list = new List<ContactCreateModel>();

            model.CreatedBy = SessionHelper.UserId;
            model.CreatedOn = DateTime.Now;

            list = list.Where(a => a.ID != model.ID).ToList();
            list.Add(model);

            SessionHelper.ListContacts = list;

            return Json("success");
        }

        [HttpGet]
        public ActionResult GetContactInfo()
        {
            return PartialView("_ListContactInfo");
        }

        [HttpGet]
        public ActionResult UpdateContactInfo(int id)
        {
            var model = SessionHelper.ListContacts.Where(a => a.ID == id).FirstOrDefault();
            model.ListTypes = GetContactTypes();
            ViewBag.Mode = "Edit";
            return PartialView("_CreateContactInfo", model);
        }

        [HttpDelete]
        public void DeleteContactInfo(int id)
        {
            foreach (var contact in SessionHelper.ListContacts.Where(contact => contact.ID == id))
            {
                contact.StatusID = Status.Deleted;
            }
        }

        public ActionResult GetContactInfoPaged([DataSourceRequest] DataSourceRequest request)
        {
            return Json(SessionHelper.ListContacts.Where(a=>a.StatusID != Status.Deleted).ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region UserProfile

        [OptionFilter(Option = Option.GetUserProfile)]
        public ActionResult UserProfile()
        {
            var requestXml = GenerateRequestXml(new GenericModel<int> { Value = SessionHelper.UserId }, Option.GetUserProfile);
            var responseModel = Execute(requestXml);
            var model = (UserProfileModel)ModelDeserializer.DeserializeFromXml<UserProfileModel>(responseModel.ModelXml);

            if (model.IsPrimaryAccountManager)
                SessionHelper.ListAddresses = model.ListAddress;
            model.ListAddress = null;

            return View(model);
        }

        public ActionResult UpdatePersonalInformation(UserProfileModel model)
        {
            model.UserDetail.ID = SessionHelper.UserId;
            model.ListAddress = SessionHelper.ListAddresses;
            model.ModifiedBy = SessionHelper.UserId;
            model.ListContact = SessionHelper.ListContacts;
            

            if (model.UpdatePassword)
            {
                model.ChangePassword.CurrentPassword = CommonFunction.HashPassword(model.ChangePassword.CurrentPassword);
                model.ChangePassword.NewPassword = CommonFunction.HashPassword(model.ChangePassword.NewPassword);
            }
            var requestXml = GenerateRequestXml(new GenericModel<UserProfileModel> { Value = model }, Option.UpdatePersonalInfo);
            var responseModel = Execute(requestXml);
            var status = (GenericModel<bool>)ModelDeserializer.DeserializeFromXml<GenericModel<bool>>(responseModel.ModelXml);
            
            if (responseModel.Status == ResponseStatus.Success && model.UpdateProfilePic)
                SessionHelper.UserImage = Convert.FromBase64String(model.ProfilePicBase64);

            if (responseModel.Status == ResponseStatus.Success && model.UpdatePersonalInfo)
            {
                SessionHelper.Name = model.UserDetail.FirstName + " " + model.UserDetail.MidName + (String.IsNullOrEmpty(model.UserDetail.MidName) ? "" : " ") + model.UserDetail.LastName;
                //var tree = SessionHelper.EntityTree.Flatten(a => a.ChildTree).ToList();
                //var item = tree.FirstOrDefault(a => a.UserID == model.UserDetail.ID && a.Category == "user");

                //if (item != null)
                //    item.Name = SessionHelper.Name;

                //SessionHelper.EntityTree = tree.Tree(null).ToList();
            }

            return Json(new { success = responseModel.Status == ResponseStatus.Success, responseMessage = responseModel.ResponseMessage });
        }

        public ActionResult UpdateEntityAddress()
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<int, List<AddressCreateModel>>() { Key = SessionHelper.UserId, Value = SessionHelper.ListAddresses }, Option.UpdateEntityAddress);
            var responseModel = Execute(requestXml);
            return Json(new { success = responseModel.Status == ResponseStatus.Success, responseMessage = responseModel.ResponseMessage });
        }

        public ActionResult GetProfileImage()
        {
            byte[] image = SessionHelper.UserImage;
            if (image.Length != 0)
                return File(image, "image/jpg");
            else
                return File(Url.Content("~/Images/profile_pic.png"), "image/jpg");
        }

        public ActionResult GetProfileImageJson()
        {
            byte[] image = SessionHelper.UserImage;
            if (image.Length != 0)
                return Json(new { base64image = Convert.ToBase64String(image) }, JsonRequestBehavior.AllowGet);
            else
                return File(Url.Content("~/Images/profile_pic.png"), "image/jpg");
        }

        #endregion UserProfile

        #region Common

        public List<SelectListItem> GetContactTypes()
        {
            var requestXml = GenerateRequestXml(null, Option.GetTelephoneTypes);
            var responseModel = Execute(requestXml);
            var groups = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            var groupList = groups.Select(a => new SelectListItem { Value = a.Key.ToString(), Text = a.Value }).ToList();
            groupList.Insert(0, new SelectListItem { Text = "-- Select --", Value = null });
            return groupList;
        }

        [HttpGet]
        public JsonResult ValidateEmail(int id, int entityId, string email)
        {
            if (SessionHelper.ListUsers != null && SessionHelper.ListUsers.Any(a => a.ID != id && a.Email == email))
                return Json(Resource_EN.Error_LoginNameAlreadyExists, JsonRequestBehavior.AllowGet);

            var requestXml = GenerateRequestXml(new AccountManangerEmailValidation { Email = email, EntityID = entityId, ID = (id == 0 ? (int?)null : id) }, Option.ValidateEmail);
            var responseModel = Execute(requestXml);
            return Json(responseModel.ResponseMessage, JsonRequestBehavior.AllowGet);
        } 

        #endregion
    }
}
