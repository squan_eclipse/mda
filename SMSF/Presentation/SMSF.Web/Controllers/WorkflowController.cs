﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Web.Common;
using Newtonsoft.Json;
using OptimaJet.Workflow.Designer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DBASystem.SMSF.Web.Controllers
{
    public class WorkflowController : BaseController
    {
        [OptionFilter(Option = Option.GetWorkflow)]
        public ActionResult Index()
        {
            SessionHelper.SelectedMenu = "Workflow";
            return View();
        }

        [HttpPost]
        public ActionResult Create(WorkflowCreateModel model)
        {
            return View(model);
        }

        public JsonResult GetWorkflow()
        {
            var requestXml = base.GenerateRequestXml(null, Option.GetWorkflow);
            var responseModel = base.Execute(requestXml);
            var workflows = (List<DictionaryModel<int, string>>)ModelDeserializer.DeserializeListFromXml<DictionaryModel<int, string>>(responseModel.ModelXml);

            return Json(workflows, JsonRequestBehavior.AllowGet);
        }

        [OptionFilter(Option = Option.GetWorkflowTemplate)]
        public ActionResult GetTemplate(WorkflowCreateModel createModel)
        {
            var requestXml = base.GenerateRequestXml(createModel, Option.GetWorkflowTemplate);
            var responseModel = base.Execute(requestXml);
            var model = (WorkflowTemplateModel)ModelDeserializer.DeserializeFromXml<WorkflowTemplateModel>(responseModel.ModelXml);

            return PartialView("_Template", model);
        }

        public ActionResult GetPendingProcess()
        {
            return View();
        }

        public ActionResult ListPendingProcesses()
        {
            var requestXml = base.GenerateRequestXml(new DictionaryModel<Guid, Guid> { Key = (Guid)SessionHelper.UserIdentifier, Value = SessionHelper.GroupIdentifier }, Option.GetWorkflowPendingProcesses);
            var responseModel = base.Execute(requestXml);
            var model = (List<WorkflowPendingProcessModel>)ModelDeserializer.DeserializeListFromXml<WorkflowPendingProcessModel>(responseModel.ModelXml);

            if (model == null)
                model = new List<WorkflowPendingProcessModel>();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPendingProcessDetail(int type, Guid processId)
        {
            var requestXml = base.GenerateRequestXml(new WorkflowPendingProcessViewModel { ProcessId = processId, TypeId = type, userId = (Guid)SessionHelper.UserIdentifier }, Option.GetWorkflowPendingProcessDetail);
            var responseModel = base.Execute(requestXml);
            var model = (WorkflowPendingProcessDetailModel)ModelDeserializer.DeserializeFromXml<WorkflowPendingProcessDetailModel>(responseModel.ModelXml);
            model.ViewUrl = String.IsNullOrEmpty(model.ViewUrl) ? "" : UrlHelper.GenerateContentUrl(model.ViewUrl, this.HttpContext);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WorkflowProcessExecuteCommand(int type, Guid processId, string command)
        {
            var requestXml = base.GenerateRequestXml(new WorkflowPendingProcessViewModel { ProcessId = processId, TypeId = type, userId = (Guid)SessionHelper.UserIdentifier, Command = command }, Option.WorkflowProcessExecuteCommand);
            var responseModel = base.Execute(requestXml);
            var status = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(responseModel.ModelXml);

            return Json(status.Value, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveWorkflow(WorkflowTemplateModel model, int firmId)
        {
            model.UserId = SessionHelper.UserId;

            var requestXml = base.GenerateRequestXml(model, Option.UpdateWorkflowProcess);
            var responseModel = base.Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
            {
                var templateId = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(responseModel.ModelXml);
                return Json(new { Success = true, Message = responseModel.StatusMessage, TemplateId = templateId.Value });
            }
            else
                return Json(new { success = false, message = responseModel.StatusMessage });
        }

        [HttpPost]
        public ActionResult SaveWorkflowTemplete(WorkflowTemplateModel model)
        {
            model.UserId = SessionHelper.UserId;
            model.EntityId = SessionHelper.EntityID;
            var requestXml = base.GenerateRequestXml(model, Option.UpdateWorkflowProcess);
            var responseModel = base.Execute(requestXml);

            if (responseModel.Status == ResponseStatus.Success)
                return Json(new { success = true, message = responseModel.StatusMessage });
            else
                return Json(new { success = false, message = responseModel.StatusMessage });
        }

        public JsonResult GetProcessCommands(int type, int id, string[] activityNames)
        {
            var requestXml = base.GenerateRequestXml(new WorkflowPendingProcessViewModel { ProcessIdentity = id, TypeId = type, userId = (Guid)SessionHelper.UserIdentifier }, Option.GetWorkflowPendingProcessDetail);
            var responseModel = base.Execute(requestXml);
            var model = (WorkflowPendingProcessDetailModel)ModelDeserializer.DeserializeFromXml<WorkflowPendingProcessDetailModel>(responseModel.ModelXml);

            if (!activityNames.ToList().Contains(model.CurrentActivity ?? "") || model.Commands.Count == 0)
            {
                return Json(new { Model = "", Response = new { Status = ResponseStatus.Failure.ToString(), Message = responseModel.StatusMessage, FlowIsAvailable = false } }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Model = model, Response = new { Status = responseModel.Status.ToString(), Message = responseModel.StatusMessage, FlowIsAvailable = true } }, JsonRequestBehavior.AllowGet);
        }

        internal WorkflowPendingProcessDetailModel GetJobProcessCommands(Workflows type, int id)
        {
            var requestXml = base.GenerateRequestXml(new WorkflowPendingProcessViewModel { ProcessIdentity = id, TypeId = (int)type, userId = (Guid)SessionHelper.UserIdentifier }, Option.GetWorkflowPendingProcessDetail);
            var responseModel = base.Execute(requestXml);
            var model = (WorkflowPendingProcessDetailModel)ModelDeserializer.DeserializeFromXml<WorkflowPendingProcessDetailModel>(responseModel.ModelXml);

            return model;
        }

        public ActionResult _WorkflowCommands()
        {
            return View();
        }

        public ActionResult GetWorkflowGraph(WorkflowCreateModel createModel)
        {
            var allowedFirms = new FirmController().GetParents(EntityTypes.Entity, false);
            if (!((List<ParentFirmModel>)allowedFirms.Data).Any(p => p.ID == createModel.FirmId))
            {
                return null;
            }

            var requestXml = GenerateRequestXml(createModel, Option.GetWorkFlowGraph);
            var responseModel = Execute(requestXml);
            var model = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(responseModel.ModelXml);

            var graphSettings = GetWorkFlowGraphSettings();
            var graph = Generator.Create(graphSettings).LoadScheme(model.Value);
            graph.GetDefaultStyle();
            var graphHtml = MvcHtmlString.Create(graph.GetHtml().ToString().Replace("<b>Results:</b>", ""));
            ViewBag.TemplateId = createModel.TemplateId;
            return View("_WorkflowGraph", graphHtml);
        }

        [HttpGet]
        public JsonResult GetFirmWorkflowTempletesList(int workflowId, int firmId)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<int, int> { Key = firmId, Value = workflowId }, Option.GetWorkflowTemplateList);
            var responseModel = Execute(requestXml);
            var model = (List<WorkflowSelectModel>)ModelDeserializer.DeserializeListFromXml<WorkflowSelectModel>(responseModel.ModelXml);
            var selectedTempleteId = model.Where(p => p.IsDefault).Select(q => q.Id).FirstOrDefault();

            return Json(new { templates = model, selectedValue = selectedTempleteId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SetDefaultWorkFlow(int workflowId, int firmId, int workflowTypeId)
        {
            var requestXml = GenerateRequestXml(new WorkflowCreateModel() { WorkflowTypeId = workflowTypeId, TemplateId = workflowId, FirmId = firmId }, Option.SetDefaultWorkFlow);
            var responseModel = Execute(requestXml);
            return Json(new { Success = responseModel.Status == ResponseStatus.Success }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SaveWorkflowGraph(string activities, int templateId)
        {
            var paramList = JsonConvert.DeserializeObject<List<GraphDeserializeModel>>(activities);

            Dictionary<string, string> paramDictionary = paramList.ToDictionary(pair => pair.Key, pair => pair.Value.ToString());
            var designerModel = new GraphDesignerModel()
            {
                DesignerX = paramDictionary.GetValueOrDefault<int, string, string>("WFDesigner_DesignerX"),
                DesignerY = paramDictionary.GetValueOrDefault<int, string, string>("WFDesigner_DesignerY"),
                DesignerScale = paramDictionary.GetValueOrDefault<int, string, string>("WFDesigner_DesignerScale"),
                WorkflowTemplateId = templateId
            };
            var activityKeys =
                 paramDictionary.Where(p => p.Key.Contains("Name") && p.Key.Contains("WFDesigner_Activity"))
                     .ToDictionary(p => p.Value, p => p.Key.Replace("Name", ""));

            foreach (KeyValuePair<string, string> activity in activityKeys)
            {
                designerModel.ActivityPostions.Add(new ActivityGraphPostion()
                {
                    LocationX = paramDictionary.GetValueOrDefault<int, string, string>(activity.Value + "DesignerX"),
                    LocationY = paramDictionary.GetValueOrDefault<int, string, string>(activity.Value + "DesignerY"),
                    Name = activity.Key
                });
            }

            var requestXml = GenerateRequestXml(designerModel, Option.SaveWorkFlowGraph);
            var responseModel = Execute(requestXml);
            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        #region Common

        private GeneratorSettings GetWorkFlowGraphSettings()
        {
            var graphSettings = new GeneratorSettings()
            {
                ReadOnly = true,
                LoadingImageUrl = "",
                GraphVisible = true,
                GraphWidth = 1200,
                GraphHeight = 600
            };
            return graphSettings;
        }

        #endregion Common
    }
}