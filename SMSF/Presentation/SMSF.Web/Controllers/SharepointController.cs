﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.Common.Encryption;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Service.Sharepoint;
using DBASystem.SMSF.Web.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace DBASystem.SMSF.Web.Controllers
{
    public class SharepointController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDocuments(string folder)
        {
            ViewBag.Folder = folder;
            return PartialView("_Documents");
        }

        public JsonResult GetFiles(string folderPath)
        {
            //var requestXml = GenerateRequestXml(new GenericModel<string> { Value = folderPath }, Option.GetFiles);
            //var responseModel = Execute(requestXml);
            //var models = (List<DocumentModel>)ModelDeserializer.DeserializeListFromXml<DocumentModel>(responseModel.ModelXml);

            //if (responseModel.Status == ResponseStatus.Success)
            //{
            //    int publicUrlExipry = System.Configuration.ConfigurationManager.AppSettings["SharePointPublicUrlExpiry"].ToType<int>();
            //    models = models.Select(x => { x.PublicUrlExipry = publicUrlExipry; return x; }).ToList();

            //    return Json(models, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    return Json(null, JsonRequestBehavior.AllowGet);
            //}

            return Json(null, JsonRequestBehavior.AllowGet);
        
        }

        public ActionResult DownloadFile(string filePath, bool decrypt = true)
        {
            if (decrypt)
            {
                filePath = filePath.Replace("[Plus]", "+");
                filePath = AesEncryption.Decrypt(filePath);
            }
            var fname = Path.GetFileName(filePath);
            var dir = Path.GetDirectoryName(filePath) + @"\";

            var requestXml = GenerateRequestXml(new DictionaryModel<string, string> { Key = dir, Value = fname }, Option.DownloadFile);
            var responseModel = Execute(requestXml);
            var file = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(responseModel.ModelXml);

            var filedata = Convert.FromBase64String(file.Value);
            var stream = new MemoryStream(filedata);
            var fileObject = File(stream.ToArray(), "application/octet-stream", fname);
            return fileObject;
        }

        public ActionResult Save(List<HttpPostedFileBase> files, string folderpath)
        {
            if (SessionHelper.MaxUploadFileSize == 0 || SessionHelper.AllowedFileExtensions == null)
            {
                var requestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = SettingHeads.Sharepoint },
                    Option.GetSetting);
                var responseModel = Execute(requestXml);
                if (responseModel.Status == ResponseStatus.Success)
                {
                    var sharePointSetting =
                        ((SharePointSettingModel)
                            ModelDeserializer.DeserializeFromXml<SharePointSettingModel>(responseModel.ModelXml));
                    SessionHelper.MaxUploadFileSize = sharePointSetting.MaximumFileSize;
                    SessionHelper.AllowedFileExtensions = sharePointSetting.AllowedFileTypeList;
                }
            }
            var model = new FileModel();

            var filesUploadFailed = new List<string>();
            var message = "";
            foreach (var file in files)
            {
                byte[] data;
                using (var stream = file.InputStream)
                {
                    var ms = stream as MemoryStream;
                    if (ms == null)
                    {
                        ms = new MemoryStream();
                        stream.CopyTo(ms);
                    }
                    data = ms.ToArray();
                }

                var extensionAllowed =
                    SessionHelper.AllowedFileExtensions.Any(
                        p =>
                            p.Contains(Path.GetExtension(file.FileName)) ||
                            p.Contains(Path.GetExtension(file.FileName).Replace(".", "")));

                if (((data.Length / 1024) / 1024) < SessionHelper.MaxUploadFileSize && extensionAllowed)
                    model.Files.Add(new Contracts.ViewModels.FileInfo
                    {
                        FileData = Convert.ToBase64String(data),
                        FileName = file.FileName
                    });

                else if (((data.Length / 1024) / 1024) > SessionHelper.MaxUploadFileSize)
                    filesUploadFailed.Add("{ 'file': '" + file.FileName + "', 'message': 'File size exceed' }");

                else if (!extensionAllowed)
                    filesUploadFailed.Add(
                        JsonConvert.SerializeObject(new { file = file.FileName, message = "File type not allowed" }));
            }

            if (model.Files.Any())
            {
                model.FolderPath = folderpath;
                var request = GenerateRequestXml(model, Option.UploadFiles);
                var response = Execute(request);

                if (filesUploadFailed.Count == 0 && response.Status == ResponseStatus.Success)
                {
                    Response.StatusCode = (int)HttpStatusCode.OK;
                    return Json(new { status = "success" });
                }

                message = response.ResponseMessage;
            }

            Response.StatusCode = (int)HttpStatusCode.NotAcceptable;
            return Json(new { status = "error", message, filesUploadFailed });
        }

        public void DeleteFile(string name, string filePath)
        {
            var requestXml = GenerateRequestXml(new DictionaryModel<string, string> { Key = name, Value = filePath }, Option.DeleteFile);
            var responseModel = Execute(requestXml);
        }

        [AllowAnonymous]
        public ActionResult ViewFile(string file)
        {
            //  fileinfo is complete path of sharepoint file and date to expire the request.
            //  URL from Office Web App escapes plus sign in return url '(+)' is replaced by '[Plus]' DocumentModel
            file = file.Replace("[Plus]", "+");
            var fileInfo = AesEncryption.Decrypt(file).Split('|');
            if (DateTime.Parse(fileInfo[1]) >= DateTime.Now)
            {
                return DownloadFile(fileInfo[0], false);
            }
            else
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return null;
            }
        }

        #region MyRegion

        public JsonResult GetAllItems(string folderPath)
        {
            var list = GetDM.GetFolderAllItems(folderPath);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public void AddNewFolder(string name, string parentFolder)
        {
            GetDM.AddFolder(name, parentFolder);
        }

        public void DeleteFolder(string name, string filePath)
        {
            GetDM.DeleteFolder(name, filePath);
        }

        private DocumentManagement GetDM
        {
            get
            {
                var SharepointrequestXml = GenerateRequestXml(new GenericModel<SettingHeads> { Value = SettingHeads.Sharepoint }, Option.GetSetting);
                var SharepointresponseModel = Execute(SharepointrequestXml);
                var sharepointSettings = (SharePointSettingModel)ModelDeserializer.DeserializeFromXml<SharePointSettingModel>(SharepointresponseModel.ModelXml);

                return new DocumentManagement(sharepointSettings.SharePointUrl, sharepointSettings.UserName, sharepointSettings.Password);
            }
        }

        #endregion MyRegion
    }
}