﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using DBASystem.SMSF.Web.Common;
using DBASystem.SMSF.Web.Controllers;
using Microsoft.Reporting.WebForms;


namespace DBASystem.SMSF.Web.Reporting
{
    public partial class ReportsViewer : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                var rc = new ReportsController();
                rvDBA.ServerReport.ReportServerUrl = new Uri(rc.GetReportServerUrl());
                rvDBA.ServerReport.ReportPath = SessionHelper.ReportPath;
                rvDBA.ServerReport.DisplayName = SessionHelper.ReportName;

                var reportParameters = SessionHelper.ReportParameters;

                foreach (var item in reportParameters)
                {
                    rvDBA.ServerReport.SetParameters(
                        new List<ReportParameter>
                        {
                            new ReportParameter
                                (item.Key, item.Value == string.Empty ? null : item.Value)
                        });
                }
            }
        }
    }
}