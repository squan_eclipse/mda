﻿function VisualDesigner(settings) {

    var me = this;

    if (settings == undefined) {
        settings = new Object();
    }

    if (settings.Container == undefined) {
        settings.Container = 'container';
    }

    if (settings.Prefix == undefined) {
        settings.Prefix = 'WFDesigner_';
    }

    if (settings.StateText == undefined) {
        settings.StateText = 'State';
    }

    if (settings.Width == undefined) {
        settings.Width = 1024;
    }

    if (settings.Height == undefined) {
        settings.Height = 768;
    }

    if (settings.DefaultActivityWidth == undefined) {
        settings.DefaultActivityWidth = 150;
    }

    if (settings.DefaultActivityHeight == undefined) {
        settings.DefaultActivityHeight = 75;
    }


    if (settings.DefaultActivityColor == undefined) {
        settings.DefaultActivityColor = '#66B922';
    }

    if (settings.DefaultInitialActivityColor == undefined) {
        settings.DefaultInitialActivityColor = '#4679B6';
    }

    if (settings.DefaultFinalActivityColor == undefined) {
        settings.DefaultFinalActivityColor = '#CF0056';
    }

    if (settings.DefaultMoveStep == undefined) {
        settings.DefaultMoveStep = 10;
    }

    this.Settings = settings;

    this.Settings.ContainerStage = this.Settings.Container + '_stage';

    this.Settings.ContainerProperties = this.Settings.Container + '_properties';


    $('#' + this.Settings.Container).append('<div id=\'' + this.Settings.ContainerStage + '\'></div>');
    
   // this.Settings.Width = "100%";
    //this.Settings.Height = "100%" ;
    this.Stage = new Kinetic.Stage({
        container: this.Settings.ContainerStage,
        width: this.Settings.Width,
        height:this.Settings.Height,
        stroke: 'black',
    });

    this.BackgroundLayer = new Kinetic.Layer();

    this.CLayer = new Kinetic.Layer();

    this.TLayer = new Kinetic.Layer();

    this.ALayer = new Kinetic.Layer();

    this.TAPLayer = new Kinetic.Layer();

    this.Stage.add(this.TLayer);

    this.Stage.add(this.ALayer);

    this.Stage.add(this.TAPLayer);

    this.Stage.add(this.CLayer);

    this.Stage.add(this.BackgroundLayer);

    this.BackgroundLayer.setZIndex(0);

    this.ALayer.setZIndex(1);

    this.TLayer.setZIndex(2);

    this.TAPLayer.setZIndex(3);

    this.CLayer.setZIndex(4);

    this.AllActivities = new Array();

    this.AllTransitions = new Array();

    //Background
    var bgImage = new Image();
    bgImage.onload = function() {
        me.RectBG.setFillPatternImage(bgImage);
        me.BackgroundLayer.batchDraw();
    };
    bgImage.src = '/images/grid.gif';

    this.RectBG = new Kinetic.Rect({
        x: 0,
        y: 0,
        width: 2000,
        height: 2000,
        draggable: true,
        dragBoundFunc: function(pos) {
            var x = pos.x - pos.x % (settings.DefaultMoveStep * me.BackgroundLayer.getScaleX());
            var y = pos.y - pos.y % (settings.DefaultMoveStep * me.BackgroundLayer.getScaleY());

            me.GraphLayerSetOffset(-x / me.BackgroundLayer.getScaleX(), -y / me.BackgroundLayer.getScaleY());
            return { x: x, y: y };
        }
    });

    this.BackgroundLayer.add(this.RectBG);
    this.BackgroundLayer.batchDraw();
    this.RectBG.on('click', function() { me.DeselectAll(); });

    //Load icons
    this.ImageCreateTransitionAndActivity = new Image();
    this.ImageCreateTransitionAndActivity.src = '/images/designer.createat.png';

    this.ImageCreateTransition = new Image();
    this.ImageCreateTransition.src = '/images/designer.createt.png';

    this.ImageDeleteActivity = new Image();
    this.ImageDeleteActivity.src = '/images/designer.delete.png';
    //Creating Graph ToolBar
    if (showGraphToolBar)
        {
    this.ImageToolbarAdd = new Image();
    this.ImageToolbarAdd.onload = function() {
        me.cImageToolbarAdd = new Kinetic.Image({
            x: 20,
            y: 10,
            image: me.ImageToolbarAdd,
            width: 32,
            height: 32,
            strokeWidth: 0
        });

        me.cImageToolbarAdd.on('click', function() { me.CreateActivity(); });
        me.CLayer.add(me.cImageToolbarAdd);
        CTooltip(me.CLayer, me.cImageToolbarAdd, 'Create');
        me.CLayer.batchDraw();
    };
    this.ImageToolbarAdd.src = '/images/designer.tb.add.png';

    //************************************************************************Delete Button 
        
        this.ImageToolbarDelete = new Image();
        this.ImageToolbarDelete.onload = function() {
            me.cImageToolbarDelete = new Kinetic.Image({
                x: 62,
                y: 10,
                image: me.ImageToolbarDelete,
                width: 32,
                height: 32,
                strokeWidth: 0
            });

            me.cImageToolbarDelete.on('click', function() { me.DeleteSelected(); });
            me.CLayer.add(me.cImageToolbarDelete);
            CTooltip(me.CLayer, me.cImageToolbarDelete, 'Delete');
            me.CLayer.batchDraw();
        };
        this.ImageToolbarDelete.src = '/images/designer.tb.delete.png';
    
    //========================================================================Delete Button
    //************************************************************************Copy Button 
    
    this.ImageToolbarCopy = new Image();
    this.ImageToolbarCopy.className = 'toolBarButton';
    this.ImageToolbarCopy.id = "toolbarCopy";
    this.ImageToolbarCopy.onload = function() {
        me.cImageToolbarCopy = new Kinetic.Image({
            x: 104,
            y: 10,
            image: me.ImageToolbarCopy,
            width: 32,
            height: 32,
            strokeWidth: 0,
        });

        me.cImageToolbarCopy.on('click', function() { me.CopySelected(); });
        me.id = 'Farrukh';
        me.CLayer.add(me.cImageToolbarCopy);
        CTooltip(me.CLayer, me.cImageToolbarCopy, 'Copy selected activity');
        me.CLayer.batchDraw();
    };
    this.ImageToolbarCopy.src = '/images/designer.tb.copy.png';
    

    this.ImageToolbarCopyProps = new Image();
    this.ImageToolbarCopyProps.onload = function() {
        me.cImageToolbarCopyProps = new Kinetic.Image({
            x: 146,
            y: 10,
            image: me.ImageToolbarCopyProps,
            width: 32,
            height: 32,
            strokeWidth: 0
        });

        me.cImageToolbarCopyProps.on('click', function() { me.CopySelectedProperties(); });
        me.CLayer.add(me.cImageToolbarCopyProps);
        CTooltip(me.CLayer, me.cImageToolbarCopyProps, 'Copy properties');
        me.CLayer.batchDraw();
    };
    this.ImageToolbarCopyProps.src = '/images/designer.tb.copyprops.png';

    this.ImageToolbarCopyPropsActive = new Image();
    this.ImageToolbarCopyPropsActive.src = '/images/designer.tb.copypropsactive.png';

    this.ImageToolbarZoomIn = new Image();
    this.ImageToolbarZoomIn.onload = function() {
        me.cImageToolbarZoomIn = new Kinetic.Image({
            x: 200,
            y: 10,
            image: me.ImageToolbarZoomIn,
            width: 32,
            height: 32,
            strokeWidth: 0
        });

        me.cImageToolbarZoomIn.on('click', function() { me.GraphLayerScale(0.1); });
        me.CLayer.add(me.cImageToolbarZoomIn);
        CTooltip(me.CLayer, me.cImageToolbarZoomIn, 'Zoom In');
        me.CLayer.batchDraw();
    };
    this.ImageToolbarZoomIn.src = '/images/designer.tb.zoomIn.png';

    this.ImageToolbarZoomOut = new Image();
    this.ImageToolbarZoomOut.onload = function() {
        me.cImageToolbarZoomOut = new Kinetic.Image({
            x: 240,
            y: 10,
            image: me.ImageToolbarZoomOut,
            width: 32,
            height: 32,
            strokeWidth: 0
        });

        me.cImageToolbarZoomOut.on('click', function() { me.GraphLayerScale(-0.1); });
        me.CLayer.add(me.cImageToolbarZoomOut);
        CTooltip(me.CLayer, me.cImageToolbarZoomOut, 'Zoom Out');
        me.CLayer.batchDraw();
    };
    this.ImageToolbarZoomOut.src = '/images/designer.tb.zoomOut.png';

    this.ImageToolbarZoomNorm = new Image();
    this.ImageToolbarZoomNorm.onload = function() {
        me.cImageToolbarZoomNorm = new Kinetic.Image({
            x: 280,
            y: 10,
            image: me.ImageToolbarZoomNorm,
            width: 32,
            height: 32,
            strokeWidth: 0
        });

        me.cImageToolbarZoomNorm.on('click', function() { me.GraphLayerScaleNorm(); });
        me.CLayer.add(me.cImageToolbarZoomNorm);
        CTooltip(me.CLayer, me.cImageToolbarZoomNorm, 'Zoom and position default set');
        me.CLayer.batchDraw();
    };
    this.ImageToolbarZoomNorm.src = '/images/designer.tb.zoomnorm.png';

    this.ImageToolbarSaveAsImage = new Image();
    this.ImageToolbarSaveAsImage.onload = function() {
        me.cImageToolbarSaveAsImage = new Kinetic.Image({
            x: 330,
            y: 10,
            image: me.ImageToolbarSaveAsImage,
            width: 32,
            height: 32,
            strokeWidth: 0
        });

        me.cImageToolbarSaveAsImage.on('click', function() { me.SaveAsImage(); });
        me.CLayer.add(me.cImageToolbarSaveAsImage);
        CTooltip(me.CLayer, me.cImageToolbarSaveAsImage, 'Save as Image');
        me.CLayer.batchDraw();
    };
    this.ImageToolbarSaveAsImage.src = '/images/designer.tb.saveasimage.png';
}

//Draw process
    this.Draw = function (data) {

        this.TLayer.destroyChildren();
        this.ALayer.destroyChildren();
        this.TAPLayer.destroyChildren();

        var activities = data.activity;
        var transitions = data.transition;

        var sx = 10;

        var sy = 50;

        this.AllActivities = new Array();
        this.AllTransitions = new Array();

        for (var i = 0; i < activities.length; i++) {

            var actX, actY;

            if (activities[i].designerx != '' && activities[i].designery != '') {
                actX = Number(activities[i].designerx);
                actY = Number(activities[i].designery);
            }
            else {
                actX = sx;
                actY = sy;

                sy += this.Settings.DefaultActivityHeight + 20;
                sx += this.Settings.DefaultActivityWidth + 20;
            }
            
            var newActivity = new CActivity({ x: actX, y: actY, activity: activities[i], designer: this }); // this.createActivity(sx, sy, activities[i]);
            this.AllActivities.push(newActivity);
            newActivity.Draw();
            newActivity.Sync();
        }


        for (var i = 0; i < transitions.length; i++) {
            var transition = transitions[i];
            var fromActivity = this.findActivityControlByName(transition.from);
            var toActivity = this.findActivityControlByName(transition.to);

            if (fromActivity == undefined || toActivity == undefined)
                continue;

            var b = 0;
            if (transition.designerbending != '') {
                b = transition.designerbending;
            }

            var newTransition = new CTransition({ from: fromActivity, to: toActivity, designer: this, transition: transition, bending: b});
            this.AllTransitions.push(newTransition);

            newTransition.DrawTransition();
            newTransition.DrawActivePoint();
            newTransition.DrawTouchPoints();
        }



        this.ALayer.batchDraw();
        this.TLayer.batchDraw();
        this.TAPLayer.batchDraw();
    };

    this.AutoUpdate = function () {
        var func = this.Settings.Prefix + 'GetDataObject';
        this.Draw(window[func]());
    };

   this.GraphLayerSetOffset = function (x, y) {
        this.ALayer.setOffset(x,y);
        this.TLayer.setOffset(x, y);
        this.TAPLayer.setOffset(x, y);

        this.Stage.batchDraw();
   }

   this.GraphLayerScale = function (a) {
       this.ALayer.setScale(this.ALayer.getScale().x + a, this.ALayer.getScale().y + a);
       this.TLayer.setScale(this.TLayer.getScale().x + a, this.TLayer.getScale().y + a);
       this.TAPLayer.setScale(this.TAPLayer.getScale().x + a, this.TAPLayer.getScale().y + a);
       this.BackgroundLayer.setScale(this.BackgroundLayer.getScale().x + a, this.BackgroundLayer.getScale().y + a);
       this.redrawAll();
   }

   this.GraphLayerScaleNorm = function (a) {
       this.ALayer.setScale(1, 1);
       this.TLayer.setScale(1, 1);
       this.TAPLayer.setScale(1, 1);
       this.BackgroundLayer.setScale(1, 1);
       
       this.ALayer.setOffset(0, 0);
       this.TLayer.setOffset(0, 0);
       this.TAPLayer.setOffset(0, 0);
       this.RectBG.setPosition(0, 0);

       this.redrawAll();
   }
    
   this.findActivityControlByName = function (name) {

        if (name == undefined)
            return undefined;
        
        for (var i = 0; i < this.AllActivities.length; i++) {
            if (this.AllActivities[i].GetName() == name)
                return this.AllActivities[i];
        }

        return undefined;
    };
    
    this.findActivityControlById = function (id) {

        if (id == undefined)
            return undefined;

        for (var i = 0; i < this.AllActivities.length; i++) {
            if (this.AllActivities[i].GetId() == id)
                return this.AllActivities[i];
        }

        return undefined;
    };
    
    this.findTransitionControlById = function (id) {

        if (id == undefined)
            return undefined;

        for (var i = 0; i < this.AllTransitions.length; i++) {
            if (this.AllTransitions[i].GetId() == id)
                return this.AllTransitions[i];
        }

        return undefined;
    };

    this.deleteActivity = function(activity) {
        var n = new Array();
        for (var i = 0; i < this.AllActivities.length; i++) {
            if (this.AllActivities[i].GetId() != activity.GetId())
                n.push(this.AllActivities[i]);
        }

        this.AllActivities = n;

        this.deleteActivityInTableView(activity);
    };

    this.deleteTransition = function (transition) {
        var n = new Array();
        for (var i = 0; i < this.AllTransitions.length; i++) {
            if (this.AllTransitions[i].GetId() != transition.GetId())
                n.push(this.AllTransitions[i]);
        }

        this.AllTransitions = n;

        this.deleteTransitionInTableView(transition);
    };
    
    this.deleteTransitionInTableView = function (transition) {
        var controls = $('input[value$="' + transition.transition.designerid + '"]');
        if (controls.length == 0) {
            alert('Transition not found!');
            return;
        }
        controls.parent().remove();
    }

    this.deleteActivityInTableView = function (activity) {
        var controls = $('input[value$="' + activity.activity.designerid + '"]');
        if (controls.length == 0) {
            alert('Activity not found!');
            return;
        }

        controls.parent().remove();
        var arrayUpdate = this.Settings.Prefix + "ActivityArrayUpdate";
        window[arrayUpdate]();
    };

    this.ShowProperties = function (c, type) {
        if (type == 'a') {
            var controls = $('input[value$="' + c.designerid + '"]');
            if (controls.length == 0) {
                alert('Activity not found!');
                return;
            }
            controls.parent().modal({ persist: true, onClose: function () { me.AutoUpdate(); } });
        }
        else {
            var controls = $('input[value$="' + c.designerid + '"]');
            if (controls.length == 0) {
                alert('Transition not found!');
                return;
            }
            controls.parent().modal({ persist: true, onClose: function () { me.AutoUpdate(); } });
        }
    };
    
    this.addOnALayer = function(ctrl) {
        this.ALayer.add(ctrl);
    };
    
    this.addOnTLayer = function (ctrl) {
        this.TLayer.add(ctrl);
    };
    
    this.addOnTAPLayer = function (ctrl) {
        this.TAPLayer.add(ctrl);
    };
    
    this.redrawTransitions = function() {
        this.TLayer.batchDraw();
        this.TAPLayer.batchDraw();
    };
    


    this.redrawActivities = function() {
        this.ALayer.batchDraw();
    };

    this.redrawObjects = function() {
        this.TLayer.batchDraw();
        this.ALayer.batchDraw();
    };
    
    this.redrawAll = function () {
        this.Stage.batchDraw();
    };

    
    this.DeselectAll = function() {

        for (var i = 0; i < this.AllActivities.length; i++) {
            var a = this.AllActivities[i];
            a.Deselect();
        }
        
        for (var i = 0; i < this.AllTransitions.length; i++) {
            var t = this.AllTransitions[i];
            t.Deselect();
        }
    };

    this.getIntersectingActivity = function (point) {

        for (var i = 0; i < this.AllActivities.length; i++) {
            var a = this.AllActivities[i];
            var rectPos = a.rectangle.getAbsolutePosition();
            var xl = rectPos.x;
            var yl = rectPos.y;
            var xr = xl + a.rectangle.getWidth() * me.ALayer.getScaleX();
            var yr = yl + a.rectangle.getHeight() * me.ALayer.getScaleX();
            if (point.x >= xl && point.x < xr && point.y >= yl && point.y < yr) {
                return a;
            }

        }

        return undefined;
    };

    this.CreateActivity = function() {
        var na = new Object();
        na.name = 'Activity_' + this._nameCounter;
        na.designerid = DesignerCommon.createUUID();
        
        var nac = new CActivity({ x: 100, y: 50, activity: na, designer: this });
        this.AllActivities.push(nac);
                
        nac.Draw();

        nac.CreateInTableView();
        this.redrawAll();
        
        this._nameCounter++;
    };

    this.DeleteSelected = function () {
        
        var selected = this.GetSelected();
        if (selected != undefined) {

            if (confirm('Are you sure you want to delete selected item Farrukh?')) {
                selected.Delete();
                this.redrawAll();
            }
        }
    };
    
    this.CopySelected = function() {
        var s = this.GetSelected();

        if (s == undefined || s.activity == undefined)
            return;

        var newO = s.CloneTo(s.getX() + 200, s.getY());

        newO.SetName('Activity_' + this._nameCounter);
        newO.SetId(DesignerCommon.createUUID());

        this.AllActivities.push(newO);
        newO.Draw();
        newO.CreateInTableView();

        this.redrawActivities();
        
        this._nameCounter++;
    };

    this._actiivitytocopy = undefined;

    this._transitiontocopy = undefined;

    this._copypropactive = false;

    this.CopySelectedProperties = function() {
        if (this._copypropactive) {
            this._actiivitytocopy = undefined;
            this._transitiontocopy = undefined;
            this.cImageToolbarCopyProps.setImage(this.ImageToolbarCopyProps);
            this._copypropactive = false;
            
            this.CLayer.batchDraw();
        } else {
            var s = this.GetSelected();

            if (s == undefined)
                return;
        
            if (s.activity != undefined) {
                this._actiivitytocopy = s;
            }
            else if (s.transition != undefined) {
                this._transitiontocopy = s;
            } else {
                return;
            }
            
            this.cImageToolbarCopyProps.setImage(this.ImageToolbarCopyPropsActive);
            this._copypropactive = true;

            this.CLayer.batchDraw();
        }
    };

    this.GetSelected = function() {

        for (var i = 0; i < this.AllActivities.length; i++) {
            var a = this.AllActivities[i];
            if (a.selected) {
                return a;
            }
        }

        for (var i = 0; i < this.AllTransitions.length; i++) {
            var t = this.AllTransitions[i];
            if (t.selected) {
                return t;
            }
        }

        return undefined;
    };

    this.createTransitionAndActivity = function(activity) {
        var sx = activity.getX() + activity.rectangle.attrs.width * 2;
        var sy = activity.getY();
        var nac = activity.CloneTo(sx, sy);
        nac.SetName('Activity_' + this._nameCounter);
        nac.SetId(DesignerCommon.createUUID());
        nac.activity.isinitial = false;
        nac.activity.isfinal = false;
        
        this.AllActivities.push(nac);
        nac.Draw();
        nac.CreateInTableView();
        this._createTransition(activity, nac);

        this.redrawAll();

        this._nameCounter++;

    };

    this.SelectChanged = function (obj) {
        if (this._copypropactive) {
            this.copyProperties(obj);
        }
    };

    this.copyProperties = function (c) {
        if (this._actiivitytocopy != undefined && c.activity != undefined) {
            c.CopyPropsFrom(this._actiivitytocopy.activity);
            this.CopySelectedProperties();
            this.AutoUpdate();
        }
        else if (this._transitiontocopy != undefined && c.transition != undefined) {
            c.CopyPropsFrom(this._transitiontocopy.transition);
            this.CopySelectedProperties();
            this.AutoUpdate();
        }
    };

    this._createTransition = function(from, to) {
        var nt = new Object();
        nt.name = from.GetName() + '_' + to.GetName() + '_' + from.dependentTransitions.length;
        nt.designerid = DesignerCommon.createUUID();
        nt.triggertype = 'Auto';
        nt.conditiontype = 'Always';
        nt.classifier = 'NotSpecified';
        var ntc = new CTransition({ from: from, to: to, designer: this, transition: nt });
        this.AllTransitions.push(ntc);
        ntc.Draw();
        ntc.CreateInTableView();
    };

    this.createTransition = function(activity) {
        var me = this;
        var xs = activity.getX() + activity.rectangle.attrs.width;
        var ys = activity.getY() + activity.rectangle.attrs.height/2;

        var tt = new CTransitionTemp({ x: xs, y: ys, designer: this });
        
        tt.Draw(xs+10,ys);

        this.redrawTransitions();

        var onMousemove = function(e) {
            var p = me.CorrectPossition(this.getMousePosition(), me.TLayer);
            p.x = p.x;
            p.y = p.y;

            tt.Redraw(p.x, p.y);
            me.TLayer.batchDraw();
        };

        var onMousedown = function (e) {

            var pos = this.getMousePosition();
            var to = me.getIntersectingActivity({ x: pos.x, y: pos.y });
            if (to != undefined) {
                me._createTransition(activity, to);
            }
            tt.Delete();
            me.Stage.off('mousemove', onMousemove);
            me.Stage.off('mousedown', onMousedown);
            me.redrawTransitions();
        };

        this.Stage.on('mousemove', onMousemove);

        this.Stage.on('mousedown', onMousedown);

    };

    this._nameCounter = 0;

    this.GetData = function() {
        var ts = new Array();
        var as = new Array();
        for (var i = 0; i < this.AllActivities.length; i++) {
            var a = this.AllActivities[i];
            as.push(a);
        }

        for (var i = 0; i < this.AllTransitions.length; i++) {
            var t = this.AllTransitions[i];
            ts.push(t);
        }

        return { activity: as, transition: ts };

    };

    this.CorrectPossition = function (e, layer) {

        if (layer.getScaleX() == 0 || layer.getScaleY() == 0)
            return { x: 0, y: 0 };

        return {
            x: e.x / layer.getScaleX() + layer.getOffsetX(),
            y: e.y / layer.getScaleY() + layer.getOffsetY()
        }
    };

    this.SaveAsImage = function () {

        var size = { x1: 10000, y1: 10000, x2: -10000, y2: -10000 };

        for (var i = 0; i < this.AllActivities.length; i++) {
            var a = this.AllActivities[i];
            var rectPos = a.rectangle.getAbsolutePosition();
            var xl = rectPos.x;
            var yl = rectPos.y;
            var xr = xl + a.rectangle.getWidth() * me.ALayer.getScaleX();
            var yr = yl + a.rectangle.getHeight() * me.ALayer.getScaleX();

            if (xl < size.x1) size.x1 = xl;
            if (yl < size.y1) size.y1 = yl;

            if (xr > size.x2) size.x2 = xr;
            if (yr > size.y2) size.y2 = yr;

        }

        this.CLayer.hide();

        var stageSize = this.Stage.getSize();
        this.Stage.setSize(-this.ALayer.getOffsetX() + size.x2 + size.x1 + 40, -this.ALayer.getOffsetY() + size.y2 + size.y1 + 40);
        this.Stage.batchDraw();

        this.Stage.toDataURL({
            x: size.x1 - 10,
            y: size.y1 - 10,
            width: size.x2 - size.x1 + 40,
            height: size.y2 - size.y1 + 40,
            callback: function (dataUrl) {
                me.Stage.setSize(stageSize);
                me.CLayer.show();
                me.Stage.batchDraw();
                window.open(dataUrl);
            }
        });
    };

    this.GetDesignerSettings = function (prefix) {
        var data = new Array();
        data.push({ name: prefix + 'X', value: this.RectBG.getX() });
        data.push({ name: prefix + 'Y', value: this.RectBG.getY() });
        data.push({ name: prefix + 'Scale', value: this.BackgroundLayer.getScaleX() });
        return data;
    }
    this.SetDesignerSettings = function (x, y, scale) {       
        if (scale != '' && scale != undefined)
            this.GraphLayerScale(scale - this.BackgroundLayer.getScaleX());

        if (x != '' && y != '' && x != undefined && y != undefined) {
            this.RectBG.setX(x);
            this.RectBG.setY(y);
            this.GraphLayerSetOffset(-x, -y);
        }
    }
}


function CActivity(parameters) {
    
    this.designer = parameters.designer;
    this.x = parameters.x;
    this.y = parameters.y;
    this.activity = parameters.activity;

    this.GetId = function() {
        return this.activity.designerid;
    };

    this.SetId = function(v) {
        this.activity.designerid = v;
    };

    this.GetName = function() {
        return this.activity.name;
    };

    this.SetName = function(v) {
        this.activity.name = v;
    };

    this.control = undefined;
    this.rectangle = undefined;
    this.text = undefined;
    this.deleteButton = undefined;
    this.createTransitionAndActivityButton = undefined;
    this.createTransitionButton = undefined;
    this.selected = false;


    this.dependentTransitions = new Array();

    this.getX = function() {
        return this.rectangle.attrs.x + this.control.attrs.x;
    };


    this.getY = function() {
        return this.rectangle.attrs.y + this.control.attrs.y;
    };

        this.Draw = function () {

        var settings = this.designer.Settings;

        this.control = new Kinetic.Group({
            x: parameters.x,
            y: parameters.y,
            rotationDeg: 0,
            draggable: true,
            dragBoundFunc: function (pos) {                
                return {
                    x: pos.x - pos.x % (settings.DefaultMoveStep * me.designer.ALayer.getScaleX()),
                    y: pos.y - pos.y % (settings.DefaultMoveStep * me.designer.ALayer.getScaleY()),
                };
            }
        });


        this.rectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: this.designer.Settings.DefaultActivityWidth,
            height: this.designer.Settings.DefaultActivityHeight,
            stroke: 'black',
            strokeWidth: 1,
            fill: '#F2F2F2',
            shadowOpacity: 0.5
        });

        this.text = new Kinetic.Text({
            x: 2,
            y: 2,
            text: this.activity.name,
            fontSize: 12,
            fontFamily: 'Calibri',
            fontStyle: 'bold',
            fill: 'black'
        });

        this.text.setX((this.rectangle.attrs.width - this.text.getWidth()) / 2);
        this.text.setY((this.rectangle.attrs.height - this.text.getHeight()) / 2 - 6);
         
        if (this.activity.state == undefined)
            this.activity.state = '';

        this.stateText = new Kinetic.Text({
            x: 2,
            y: 2,
            text: me.designer.Settings.StateText + ': ' + this.activity.state,
            fontSize: 10,
            fontFamily: 'Calibri',
            fill: 'black'
        });
        this.stateText.setX((this.rectangle.attrs.width - this.stateText.getWidth()) / 2);
        this.stateText.setY(this.text.getY() + 15);

        this.deleteButton = new Kinetic.Image({
            x: this.rectangle.attrs.width - 13,
            y: 3,
            image: this.designer.ImageDeleteActivity,
            width: 10,
            height: 10,
        });

        this.createTransitionAndActivityButton = new Kinetic.Image({
            x: this.rectangle.attrs.width - 13,
            y: this.rectangle.attrs.height - 26,
            image: this.designer.ImageCreateTransitionAndActivity,
            width: 10,
            height: 10
        });

        this.createTransitionButton = new Kinetic.Image({
            x: this.rectangle.attrs.width - 13,
            y: this.rectangle.attrs.height - 13,
            image: this.designer.ImageCreateTransition,
            width: 10,
            height: 10
        });

        this.control.add(this.rectangle);
        this.control.add(this.text);
        this.control.add(this.stateText);
            
        if (showActivityDelete)
            this.control.add(this.deleteButton);
        if (showAcitvityCreate)
            this.control.add(this.createTransitionAndActivityButton);
        if (showTransactionAdd)
            this.control.add(this.createTransitionButton);
            
        
        this.control.on('dragend', this.Sync);
        this.control.on('dragmove', this._onMove);
        this.control.on('click', this._onClick);
        this.control.on('dblclick', this._onDblClick);
        if (showActivityDelete)
            this.deleteButton.on('click', this._onDelete);
        if (showAcitvityCreate)
            this.createTransitionAndActivityButton.on('click', this._onCreateTransitionAndActivity);
        if (showTransactionAdd)
            this.createTransitionButton.on('click', this._onCreateTransition);
            

        this.designer.addOnALayer(this.control);
    };

    this.Delete = function() {

        this.control.destroy();
        this.designer.deleteActivity(this);

        var todel = new Array();
        for (var i = 0; i < this.dependentTransitions.length; i++) {
            todel.push(this.dependentTransitions[i]);
        }

        for (var i = 0; i < todel.length; i++) {
            todel[i].Delete();
        }
    };

    this.Select = function() {
        this.rectangle.setStrokeWidth(4);
        this.rectangle.setOpacity(0.5);
        this.rectangle.setStroke('yellow');
        this.selected = true;

        this.designer.SelectChanged(this);
    };

    this.Deselect = function() {
        this.rectangle.setStrokeWidth(1);
        this.rectangle.setOpacity(1);
        this.rectangle.setStroke('black');
        this.selected = false;
    };

    var me = this;

    this._onMove = function() {

        if (me.dependentTransitions.length < 1)
            return;

        for (var i = 0; i < me.dependentTransitions.length; i++) {
            var t = me.dependentTransitions[i];
            t.Redraw();
        }

        me.designer.redrawTransitions();
    };

    this._onClick = function() {
        me.designer.DeselectAll();
        me.Select();
        me.designer.redrawObjects();
    };

    this._onDblClick = function () {
        me.designer.DeselectAll();
        me.Select();
        me.designer.redrawObjects();

        me.designer.ShowProperties(me.activity, 'a');
    };

    this._onDelete = function () {
        if (confirm('Are you sure you want to delete selected item?')) {
            me.Delete();
            me.designer.redrawAll();
        }
    };

    this._onCreateTransitionAndActivity = function() {
        me.designer.createTransitionAndActivity(me);
    };

    this._onCreateTransition = function() {
        me.designer.createTransition(me);
    };

    this.RegisterTransition = function(cTransition) {
        var f = false;

        for (var i = 0; i < this.dependentTransitions.length; i++) {
            if (this.dependentTransitions[i].GetId() == cTransition.GetId()) {
                f = true;
                break;
            }
        }

        if (!f)
            this.dependentTransitions.push(cTransition);
    };

    this.UnregisterTransition = function(cTransition) {
        var f = false;
        var nt = new Array();
        for (var i = 0; i < this.dependentTransitions.length; i++) {
            if (this.dependentTransitions[i].GetId() != cTransition.GetId()) {
                nt.push(this.dependentTransitions[i]);
            }
        }
        this.dependentTransitions = nt;
    };

    this.CloneTo = function(x, y) {
        var na = jQuery.extend({}, this.activity);
        return new CActivity({ activity: na, x: x, y: y, designer: this.designer });
    };
    
    this.CopyPropsFrom = function (a) {
        var nt = jQuery.extend({}, a);
        var id = this.GetId();
        var name = this.GetName();
        this.activity = nt;
        this.SetId(id);
        this.SetName(name);

        var tempActivityIndex = WFDesigner_ActivityIndex;

        var cntrl = $('input[value$="' + this.GetId() + '"]');
        var row = cntrl.closest('tr');
        var rowprev = row.prev();
        var rownext = row.next();

        var currIndex = cntrl[0].id.replace('DesignerId', 'CurrentIndex');
        WFDesigner_ActivityIndex = $('input#' + currIndex)[0].value;        

        this.designer.deleteActivityInTableView(this);
        this.CreateInTableView();

        var row = $('input[value$="' + this.GetId() + '"]').closest('tr');
        if (rowprev.length != 0) {
            row.insertAfter(rowprev);
        } else if (rownext.length != 0) {
            row.insertBefore(rownext);
        }        

        WFDesigner_ActivityIndex = tempActivityIndex;
    };

    this.Sync = function () {
        var id = me.GetId();
        var pos = me.control.getPosition();

        var controls = $('input[value$="' + id + '"]');
        if (controls.length == 0) {
            alert('Activity not found!');
            return;
        }

        var posXItemId = controls[0].id.replace('DesignerId', 'DesignerX');
        $('input#' + posXItemId)[0].value = pos.x;

        var posYItemId = controls[0].id.replace('DesignerId', 'DesignerY');
        $('input#' + posYItemId)[0].value = pos.y;
    };
    
    this.CreateInTableView = function() {
        var pos = this.control.getPosition();
        var method = this.designer.Settings.Prefix + 'OnActivityCreate';
        window[method](this.GetName(), this.activity.state,
            this.activity.isinitial, this.activity.isfinal, this.activity.isforsetstate, this.activity.isautoschemeupdate,
            this.activity.imp, this.activity.peimp, this.GetId(), pos.x, pos.y);
    }
}

function CTransition(parameters) {

    this.from = parameters.from;
    this.to = parameters.to;

    this.setFrom = function(activity) {
        this.from = activity;
        this.transition.from = activity.GetName();

        var controls = $('input[value$="' + this.GetId() + '"]');
        if (controls.length == 0) {
            alert('Transition not found!');
            return;
        }
        var itemId = controls[0].id.replace('DesignerId', 'From');
        $('input#' + itemId).val(activity.GetName());
    };
    
    this.setTo = function (activity) {
        this.to = activity;
        this.transition.to = activity.GetName();

        var controls = $('input[value$="' + this.GetId() + '"]');
        if (controls.length == 0) {
            alert('Transition not found!');
            return;
        }
        var itemId = controls[0].id.replace('DesignerId', 'To');
        $('input#' + itemId).val(activity.GetName());
    };

    
    this.designer = parameters.designer;
    this.transition = parameters.transition;

    this.GetId = function () {
        return this.transition.designerid;
    };

    this.SetId = function (v) {
        this.transition.designerid = v;
    };
    
    this.GetName = function () {
        return this.transition.name;
    };

    this.SetName = function (v) {
        this.transition.name = v;
    };

    this.control = undefined;
    this.arrow = undefined;
    this.line = undefined;
    if (parameters.bending == undefined) {
        this.bending = 0;
    }
    else {
        this.bending = parameters.bending;
    }

    this.from.RegisterTransition(this);
    this.to.RegisterTransition(this);

    this.CopyPropsFrom = function(t) {
        var nt = jQuery.extend({}, t);
        var id = this.GetId();
        var name = this.GetName();
        this.transition = nt;
        this.SetId(id);
        this.SetName(name);

        this.setTo(this.to);
        this.setFrom(this.from);

        var tempTransitionIndex = WFDesigner_TransitionIndex;

        var cntrl = $('input[value$="' + this.GetId() + '"]');
        var row = cntrl.closest('tr');
        var rowprev = row.prev();
        var rownext = row.next();

        var currIndex = cntrl[0].id.replace('DesignerId', 'CurrentIndex');
        WFDesigner_TransitionIndex = $('input#' + currIndex)[0].value;

        this.designer.deleteTransitionInTableView(this);
        this.CreateInTableView();

        var row = $('input[value$="' + this.GetId() + '"]').closest('tr');
        if (rowprev.length != 0) {
            row.insertAfter(rowprev);
        } else if (rownext.length != 0) {
            row.insertBefore(rownext);
        }

        WFDesigner_TransitionIndex = tempTransitionIndex;
    };
   
    this.DrawTransition = function (fixedstartpoit, fixedendpoint) {
        var bending = this.bending;
        var me = this;

        var delta = 50;
        var fromRec = this.from.rectangle;
        var toRec = this.to.rectangle;

        var fromx, fromy, tox, toy;


        fromx = this.from.getX();
        fromy = this.from.getY();


        tox = this.to.getX();
        toy = this.to.getY();

        var ascx = fromx + fromRec.attrs.width / 2;
        var ascy = fromy + fromRec.attrs.height / 2;
        var aecx = tox + toRec.attrs.width / 2;
        var aecy = toy + toRec.attrs.height / 2;
        var xs, ys, xe, ye;

        if (aecx >= ascx - delta - toRec.attrs.width / 2 && aecx <= ascx + delta + toRec.attrs.width / 2 && aecy >= ascy) {
            xs = ascx;
            ys = fromy + fromRec.attrs.height;
            xe = aecx;
            ye = toy;
        } else if (aecx >= ascx - delta - toRec.attrs.width / 2 && aecx <= ascx + delta + toRec.attrs.width / 2 && aecy < ascy) {
            xs = ascx;
            ys = fromy;
            xe = aecx;
            ye = toy + toRec.attrs.height;
        } else if (aecx < ascx) {
            xs = fromx;
            ys = ascy;
            xe = tox + toRec.attrs.width;
            ye = aecy;
        } else {
            xs = fromx + fromRec.attrs.width;
            ys = fromy + fromRec.attrs.height / 2;
            xe = tox;
            ye = toy + toRec.attrs.height / 2;
        }

        if (fixedstartpoit != undefined) {
            xs = fixedstartpoit.x;
            ys = fixedstartpoit.y;
        }


        if (fixedendpoint != undefined) {
            xe = fixedendpoint.x;
            ye = fixedendpoint.y;
        }


        //Calculates the center point
        var lineAngle = (Math.atan2(ye - ys, xe - xs));

        var lineLength = this._getLineLength(xs, ys, xe, ye);

        var cbp = this._getCBPoint(xs, ys, xe, ye, lineAngle, lineLength, bending);

        var xcb = cbp.x; var ycb = cbp.y;
        this.control = new Kinetic.Group({
            x: 0,
            y: 0,
            rotationDeg: 0
        });
        
        var tension = 0.5;


        var angle = Math.atan2(ye - ycb, xe - xcb);

        var add = Math.PI / 10 * (bending > 0 ? 1 : bending < 0 ? -1 : 0);


        if (Math.abs(lineAngle) >= Math.PI / 2)
            add = -add;

        angle += add;

        var classifier = this.transition.classifier == undefined ? 'notspecified' : this.transition.classifier.toLowerCase();
        var colour = classifier == 'notspecified' ? 'gray' : classifier == 'direct' ? 'green' : 'red';
        this.arrow = DesignerCommon.createArrowByAngle(xe, ye, angle, 15,colour);

        this.start = { x: xs, y: ys };
        this.end = { x: xe, y: ye };
        this.middle = { x: xcb, y: ycb };

        this.angle = angle;
        this.lineAngle = lineAngle;
        

        this.line = new Kinetic.Spline({
            points: [xs, ys, xcb, ycb, xe, ye],
            stroke: colour,
            strokeWidth: 1,
            lineCap: 'round',
            lineJoin: 'round',
            tension: tension
        });
        
        this.control.add(this.line);

        this.control.add(this.arrow);

        this.designer.addOnTLayer(this.control);

        this.line.on('click', function () {
            me.designer.DeselectAll();
            me.Select();
            me.designer.redrawObjects();
        });

        if (this.selected) {
            this.Select();
        }

    };

    this.start = undefined;
    this.end = undefined;
    this.middle = undefined;
    this.angle = undefined;
    this.lineAngle = undefined;

    this.activePoint = undefined;
    this.touchpoints = [];

    this._getCBPoint = function(xs,ys,xe,ye,angle,length, bending) {
        
        var lineAngle = angle;

        var lineLength = length;

        var k1;

        if (lineAngle < Math.PI / 2 && lineAngle > 0) {
            k1 = 1;
        } else if (lineAngle >= Math.PI / 2) {
            k1 = -1;
        } else if (lineAngle <= 0 && lineAngle > -Math.PI / 2) {
            k1 = -1;
        } else {
            k1 = 1;
        }

        k1 = (lineAngle > 0 ? 1 : -1) * k1;

        var lineBendLength = bending * lineLength * k1;

        var xc = (xs + xe) / 2;
        var yc = (ys + ye) / 2;


        var xcb = xc - lineBendLength * (Math.cos(Math.PI / 2 + lineAngle));

        var ycb = yc - lineBendLength * (Math.sin(Math.PI / 2 + lineAngle));

        return { x: xcb, y: ycb };

    };

    this.DrawActivePoint = function() {
        var activePoint = this._createActivePoint(this.start.x, this.start.y, this.end.x, this.end.y, this.middle.x, this.middle.y, this.control);
        this.designer.addOnTAPLayer(activePoint);
        this.activePoint = activePoint;
    };

    this.DrawTouchPoints = function () {
        var tpShift = this._getLineLength(this.start.x, this.start.y, this.end.x, this.end.y) * 0.1;
        var touchPoint1 = this._createTouchPoint(this.start.x, this.start.y, tpShift, this.angle, this.lineAngle, this.control);
        var touchPoint2 = this._createTouchPoint(this.end.x, this.end.y, tpShift, this.angle, this.lineAngle, this.control, true);

        this.designer.addOnTAPLayer(touchPoint1);
        this.designer.addOnTAPLayer(touchPoint2);
        this.touchpoints = [touchPoint1, touchPoint2];
    };

    this.Draw = function (fixedstartpoit, fixedendpoint) {
        this.DrawTransition(fixedstartpoit, fixedendpoint);
        this.DrawActivePoint();
        this.DrawTouchPoints();
    };

    this.Redraw = function (fixedstartpoit, fixedendpoint) {
        this.RedrawTransition(fixedstartpoit, fixedendpoint);
        this.RedrawActivePoint();
        this.RedrawTouchPoints();
    };
    

    this.RedrawTransition = function (fixedstartpoit, fixedendpoint) {
        this.control.destroy();
        this.DrawTransition(fixedstartpoit, fixedendpoint);
    };

    this.RedrawTouchPoints = function () {
       for (var i = 0; i < this.touchpoints.length; i++) {
            this.touchpoints[i].destroy();
        }
        this.DrawTouchPoints();
    };

    this.RedrawActivePoint = function() {
        if (this.activePoint.ToolTip != undefined) {
            this.activePoint.ToolTip.destroy();
        }
        this.activePoint.destroy();
        this.DrawActivePoint();
    };

    this.DeleteTouchPoint = function(isend) {
        for (var i = 0; i < this.touchpoints.length; i++) {
            if (this.touchpoints[i].isend == isend) {
                this.touchpoints[i].destroy();
            }
        }
    };

    this.Delete = function() {
        this.from.UnregisterTransition(this);
        this.to.UnregisterTransition(this);
        this.control.destroy();

        if (this.activePoint.ToolTip != undefined) {
            this.activePoint.ToolTip.destroy();
        }
        this.activePoint.destroy();
        for (var i = 0; i < this.touchpoints.length; i++) {
            this.touchpoints[i].destroy();
        }
        this.designer.deleteTransition(this);
    };

    this.Select = function() {
        this.oldstroke = this.line.getStroke();
        this.line.setStroke('yellow');
        this.arrow.setStroke('yellow');
        this.arrow.setFill('yellow');
        this.line.setStrokeWidth(3);
        this.selected = true;

        this.designer.SelectChanged(this);
    };

    this.Deselect = function() {
        this.line.setStrokeWidth(1);
        if (this.oldstroke != undefined) {
            this.line.setStroke(this.oldstroke);
            this.arrow.setStroke(this.oldstroke);
            this.arrow.setFill(this.oldstroke);
        }
        this.selected = false;
    };
    
    this._createTouchPoint = function (x, y, len, angle, lineAngle, cTransition, isend) {

        var me = this;

        var angleGrad = lineAngle * 180 / Math.PI;

        var cTouchPoint = new Kinetic.Group({
            x: x,
            y: y,
            rotationDeg: angleGrad,
            draggable: true
        });

        cTouchPoint.isend = isend;

        var circle = new Kinetic.Circle({
            x: (isend ? -1 : 1) * len * Math.cos(-(angle - lineAngle)),
            y: len * Math.sin(-(angle - lineAngle)),
            radius: 5,
            fill: 'lightgray'

        });

        cTouchPoint.add(circle);

        cTouchPoint.transition = cTransition;



        var redraw = function () {
            var position = me.designer.CorrectPossition(circle.getAbsolutePosition(), me.designer.TLayer);
            if (me.oldbending == undefined)
                me.oldbending = me.bending;
            me.bending = 0;

            var e = position;

            if (isend) {
                me.RedrawTransition(undefined, e);
            } else {
                me.RedrawTransition(e,undefined);
            }
            me.RedrawActivePoint();
            me.DeleteTouchPoint(!isend);
            me.designer.redrawTransitions();
        };

        cTouchPoint.on('dragmove', function () {
            redraw();
        });

        cTouchPoint.on('dragend', function () {

            var position = circle.getAbsolutePosition();
            var activity = me.designer.getIntersectingActivity(position);
            

            if (activity != undefined) {
                me.oldbending = undefined;
                me.bending = 0;
                if (isend) {
                    me.to.UnregisterTransition(me);
                    me.setTo(activity);
                    me.to.RegisterTransition(me);

                } else {
                    me.from.UnregisterTransition(me);
                    me.setFrom(activity);
                    me.from.RegisterTransition(me);
                }

                me.Redraw();
                me.Sync();
            } else {
                me.bending = me.oldbending;
                me.oldbending = undefined;
                me.Redraw();
            }
        });

        return cTouchPoint;

    };

    this._createActivePoint = function (x1, y1, x2, y2, x, y, cTransition) {

        var me = this;

        var cActivePoint = new Kinetic.Group({
            x: x,
            y: y,
            rotationDeg: 0,
            draggable: true
        });

        var circle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 15,
            fill: 'lightgray'

        });

        cActivePoint.add(circle);

        var textvalue = '';
        
        if (this.transition.triggertype.toLowerCase() == 'auto') {
            textvalue += 'A';
        }
        else if (this.transition.triggertype.toLowerCase() == 'command') {
            textvalue += 'C';
        }

        if (this.transition.conditiontype.toLowerCase() == 'always') {
            textvalue += 'A';
        }
        else if (this.transition.conditiontype.toLowerCase() == 'action') {
            textvalue += 'C';
        }
        else if (this.transition.conditiontype.toLowerCase() == 'otherwise') {
            textvalue += 'O';
        }
        
        var text = new Kinetic.Text({
            x: -12,
            y: -11,
            text: textvalue,
            fontSize: 20,
            fontFamily: 'Calibri',
            fill: 'black',
            fontStyle: 'bold'
        });
        
        
        cActivePoint.add(text);

        cActivePoint.transition = cTransition;

        var redraw = function (d, r) {
            var position = cActivePoint.getPosition();
            if (r)
                me.bending = 0;
            else {
                me.bending = me._getBendingKoeff(x1, y1, x2, y2, position.x, position.y);
            }
            
            
            if (Math.abs(me.bending) < 0.07)
                me.bending = 0;
            
            me.RedrawTransition();
            me.RedrawTouchPoints();

            if(d){
                me.RedrawActivePoint();
                me.Sync();
            }

            me.designer.redrawTransitions();
        };

        cActivePoint.on('click', function () {
            me.designer.DeselectAll();
            me.Select();
            me.designer.redrawObjects();
        });

        cActivePoint.on('dblclick', function () {
            me.designer.DeselectAll();
            me.Select();
            me.designer.redrawObjects();

            me.designer.ShowProperties(me.transition, 't');
        });

        cActivePoint.on('dragmove', function () {
            redraw(false);
        });

        cActivePoint.on('dragend', function () {
            redraw(true);
        });
        
        CTooltip(me.designer.TLayer, cActivePoint, this.transition.triggertype + ' ' + this.transition.conditiontype, true);

        return cActivePoint;
    };
    
    this._getLineLength = function (x1, y1, x2, y2) {
        return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
    };

    this._getBendingKoeff = function (x1, y1, x2, y2, xap, yap) {

        var a = y1 - y2;
        var b = x2 - x1;
        var c = (x1 * y2 - x2 * y1);

        if (b <= 0) {
            a = -a;
            b = -b;
            c = -c;
        }

        var yapn = -(c + a * xap) / b;

        var sign = yapn < yap ? -1 : 1;

        var len1 = this._getLineLength(x1, y1, x2, y2);
        var xc = (x1 + x2) / 2;
        var yc = (y1 + y2) / 2;
        var len2 = this._getLineLength(xc, yc, xap, yap);
        var bending = len2 / len1 * sign;
        return bending;
    };

    this.Sync = function () {
        var id = this.GetId();
        
        var controls = $('input[value$="' + id + '"]');
        if (controls.length == 0) {
            alert('Transition not found!');
            return;
        }

        var posBendingItemId = controls[0].id.replace('DesignerId', 'DesignerBending');
        $('input#' + posBendingItemId)[0].value = this.bending;
    };

    this.CreateInTableView = function () {
        var method = this.designer.Settings.Prefix + 'OnTransitionCreate';
        window[method](this.GetName(), this.from.GetName(), this.to.GetName(), this.transition.classifier, '', this.transition.triggertype, '', this.transition.conditiontype, '', '', this.GetId(), this.bending);
    }
}

function CTransitionTemp(parameters) {
    this.x = parameters.x;
    this.y = parameters.y;
    this.designer = parameters.designer;
    this.control = undefined;
    
    this.Draw = function(xe, ye) {
        this.control = new Kinetic.Group({
            x: 0,
            y: 0,
            rotationDeg: 0
        });
        var line = new Kinetic.Polygon({
            points: [this.x, this.y, xe, ye],
            stroke: 'red',
            strrokeWidth: 1
        });
        var arrow = DesignerCommon.createArrow(this.x, this.y, xe, ye, 20);

        this.control.add(line);
        this.control.add(arrow);

        this.designer.addOnTLayer(this.control);

    };
    
    this.Redraw = function(xe, ye) {
        this.control.destroy();
        this.Draw(xe, ye);
    };

    this.Delete = function() {
        this.control.destroy();
    };
}

var DesignerCommon = {
    createArrow: function(fromx, fromy, tox, toy, headlen) {
        var angle = Math.atan2(toy - fromy, tox - fromx);
        var line = new Kinetic.Polygon({
            points: [tox - headlen * Math.cos(angle - Math.PI / 6),
                toy - headlen * Math.sin(angle - Math.PI / 6),
                tox,
                toy,
                tox - headlen * Math.cos(angle + Math.PI / 6),
                toy - headlen * Math.sin(angle + Math.PI / 6)],
            fill: 'red'
        });

        return line;
    },

    createArrowByAngle: function (x, y, angle, headlen, colour) {
        if (colour == undefined)
            colour = 'red';
        var line = new Kinetic.Polygon({
            points: [x - headlen * Math.cos(angle - Math.PI / 6),
                y - headlen * Math.sin(angle - Math.PI / 6),
                x,
                y,
                x - headlen * Math.cos(angle + Math.PI / 6),
                y - headlen * Math.sin(angle + Math.PI / 6)],
            fill: colour
        });

        return line;
    },
    
    createUUID : function() {

    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
}
};

function CTooltip(layer, obj, text, includeInObj) {
    var me = this;

    obj.on('mouseover', function ()  {
        if (obj.ToolTip != undefined) {
            return;
        }

        var tooltip = new Kinetic.Label({
            x: obj.getX() + obj.getWidth() / 2,
            y: obj.getY() + 30,
            opacity: 0.75
        });

        tooltip.add(new Kinetic.Tag({
            fill: 'black',
            pointerDirection: 'up',
            pointerWidth: 10,
            pointerHeight: 10,
            lineJoin: 'round',
            shadowColor: 'black',
            shadowBlur: 10,
            shadowOffset: 10,
            shadowOpacity: 0.5
        }));

        tooltip.add(new Kinetic.Text({
            text: text,
            fontFamily: 'Calibri',
            fontSize: 12,
            padding: 5,
            fill: 'white'
        }));

        if (includeInObj) {
            tooltip.attrs.x = 0;
            tooltip.attrs.y = 15;
            obj.add(tooltip);
        }
        else {
            layer.add(tooltip);
        }
                
        layer.batchDraw();
        obj.ToolTip = tooltip;
    });
    obj.on('mouseleave', function () {
        me.Destroy(obj);
        layer.batchDraw();
    });

    this.Destroy = function (obj) {
        if (obj.ToolTip != undefined) {
            obj.ToolTip.destroy();
            obj.ToolTip = undefined;
        }
    };
}
