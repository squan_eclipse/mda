﻿
//Use to set date in kendo ui grid called from js.
$.date = function (dateObject) {
    var re = /-?\d+/;
    var m = re.exec(dateObject);
    var d = new Date(parseInt(m[0]));
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date == '01/01/1' ? '' : date;
};

//Use to set date
$.dateTime = function (dateObject) {
    var re = /-?\d+/;
    var m = re.exec(dateObject);
    var d = new Date(parseInt(m[0]));
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var hour = d.getHours();
    var minute = d.getMinutes();
    var second = d.getSeconds();

    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year + " " + pad(hour, 2) + ":" + pad(minute, 2) + ":" + pad(second, 2);

    return date;
}

//Custom alert box
function doAlert(msg) {
    if ($('#dvAlert') != null)
        $('#dvAlert').remove();

    $('<div/>', { id: 'dvAlert' }).appendTo('body');
    $('<div/>', { id: 'dvAlertMessage' }).appendTo('#dvAlert');
    $('<div/>', { id: 'dvAlertButtons' }).appendTo('#dvAlert');
    $('<input/>', { id: 'btnAlertOK', type: 'button', value: 'OK' }).appendTo('#dvAlertButtons');

    $('#dvAlert').dialog({
        title: 'DBA System',
        width: 370,
        height: 'auto',
        modal: true,
        resizable: false,
    });

    var alertBox = $("#dvAlert");
    alertBox.find("#dvAlertMessage").html(msg);
    alertBox.find("#btnAlertOK").unbind().click(function () {
        alertBox.dialog('close');
    });
    $("#dvAlert").dialog("option", "position", "center");
}

//Confirmation dialog
function doConfirm(msg, yesFn, noFn) {
    if ($('#dvConfirm') != null)
        $('#dvConfirm').remove();

    $('<div/>', { id: 'dvConfirm' }).appendTo('body');
    $('<div/>', { id: 'dvConfirmMessage' }).appendTo('#dvConfirm');
    $('<div/>', { id: 'dvConfirmButtons' }).appendTo('#dvConfirm');
    $('<input/>', { id: 'btnConfirmYes', type: 'button', value: 'Yes' }).appendTo('#dvConfirmButtons');
    $('#dvConfirmButtons').append(' ');
    $('<input/>', { id: 'btnConfirmYes', type: 'button', value: 'No' }).appendTo('#dvConfirmButtons');

    $('#dvConfirm').dialog({
        title: 'DBA System',
        width: 370,
        height: 'auto',
        modal: true,
        resizable: false,
    });
    var confirmBox = $("#dvConfirm");
    confirmBox.find("#dvConfirmMessage").html(msg);
    confirmBox.find("#btnConfirmYes,#btnConfirmNo").unbind().click(function () {
        confirmBox.dialog('close').remove();
    });
    confirmBox.find("#btnConfirmYes").click(yesFn);
    confirmBox.find("#btnConfirmNo").click(noFn).focus();
    $("#dvConfirm").dialog("option", "position", "center");
}

//Set the message in the status bar.
function setStatusBarMessage(message) {
    $('footer div').html(message);
}

//Check if the object is null or empty.
function isNullOrEmpty(obj) {
    return obj == null || obj == '';
}

//Check if the return view is login view or not
function isSessionExpired(view, redirectUrl) {
    if (view.toString().indexOf('frmLogin') > 0)
        window.location.href = redirectUrl;
}

//Set the list of parents of the select item in tree view
function setSelectedItemParent(a) {
    var listId = [];
    $.each($(a).parents('li'), function (index, li) {
        listId[index] = $(li).attr('id');
    });

    $('#txtSelectedItemParents').val(listId.toString());
}

//Add padding to a string
function pad(str, max) {
    str = str.toString();
    while (str.length < max) {
        str = '0' + str;
    }
    return str;
}

//Set date as pretty date format
function prettyDate(date_str) {
    var re = /-?\d+/;
    var m = re.exec(date_str);
    var d = new Date(parseInt(m[0]));
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var hour = d.getHours();
    var minute = d.getMinutes();
    var second = d.getSeconds();

    var date = year + '-' + pad(month, 2) + '-' + pad(day, 2) + 'T' + pad(hour, 2) + ':' + pad(minute, 2) + ':' + pad(second, 2) + 'Z';

    var time = ('' + date).replace(/-/g, "/").replace(/[TZ]/g, " ");
    var seconds = (new Date - new Date(time)) / 1000;
    var token = 'ago',
        list_choice = 1;
    if (seconds < 0) {
        seconds = Math.abs(seconds);
        token = 'from now';
        list_choice = 2;
    }
    var i = 0,
        format;
    while (format = time_formats[i++]) if (seconds < format[0]) {
        if (typeof format[2] == 'string') return format[list_choice];
        else return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
    }
    return time;
};

//Hide buttons and show loading image.
function showLoader(elementName) {
    if (elementName == '') return;

    if (elementName.indexOf('#') == -1) elementName = '#' + elementName;
    if ($(elementName).length == 0) return;

    $(elementName).find('input').hide();
    $(elementName).find('img').show();
}

//Hide loading image and show buttons.
function hideLoader(elementName) {
    if (elementName == '') return;

    if (elementName.indexOf('#') == -1) elementName = '#' + elementName;
    if ($(elementName).length == 0) return;

    $(elementName).find('input').show();
    $(elementName).find('img').hide();
}

//Time format for pretty date
var time_formats = [
    [60, 'just now', 1], // 60 
    [120, '1 minute ago', '1 minute from now'], // 60*2
    [3600, 'minutes', 60], // 60*60, 60
    [7200, '1 hour ago', '1 hour from now'], // 60*60*2 
    [86400, 'hours', 3600], // 60*60*24, 60*60 
    [172800, 'yesterday', 'tomorrow'], // 60*60*24*2 
    [604800, 'days', 86400], // 60*60*24*7, 60*60*24 
    [1209600, 'last week', 'next week'], // 60*60*24*7*4*2 
    [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7 
    [4838400, 'last month', 'next month'], // 60*60*24*7*4*2 
    [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4 
    [58060800, 'last year', 'next year'], // 60*60*24*7*4*12*2 
    [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12 
    [5806080000, 'last century', 'next century'], // 60*60*24*7*4*12*100*2 
    [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
];