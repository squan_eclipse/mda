(function($){
	$.fn.multiAccordion = function(openedArray, onClass){
		var element = this;
		element.find('.'+onClass).addClass('be4');
		
		element.find('ul').hide();
		
		// Open the specified submenus
		if(openedArray){
			for (i=0;i<=openedArray.length - 1;i++){
				openedArray[i] = openedArray[i] - 1;
				element.children('li:eq('+openedArray[i]+')').children('ul').show();
			};
		};
		
		var allClickable = element.find('a');
		
		allClickable.each(function(i){
			var subul = $(this).parent().find('ul:eq(0)');
			$(this).click(function(){
				if(subul.is(':visible')){
					isVisible(this, subul);
				} else {
					isNotVisible(this, subul);
				};
				if(subul.length == 0){
					return true;
				} else {
					return false;
				};
			});
		});
		
		function isVisible(clicked, toHide){
			toHide.slideUp(function(){
				if(!$(clicked).hasClass('be4')){
					$(clicked).removeClass(onClass);
				};
			});
			$(clicked).parent().find('ul').slideUp();
		};
		
		function isNotVisible(clicked, toOpen){
			$(clicked).parent().parent().find('.on:not(.be4)').removeClass('on');
			$(clicked).addClass(onClass);
			$(clicked).parent().parent().find('ul').slideUp();
			toOpen.slideDown();
			
		};
	};
})(jQuery);