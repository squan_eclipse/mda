﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimaJet.Workflow.Designer
{
    public class ValidationResult
    {
        public StringBuilder ErrorMsg = new StringBuilder();
        public StringBuilder ErrorScript = new StringBuilder();
        public StringBuilder SyncScript = new StringBuilder();

        public bool IsValid
        {
            get
            {
                return ErrorMsg.Length == 0;
            }
        }

        public string GetHtml()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(ErrorMsg.ToString());
            if (ErrorScript.Length > 0)
            {
                sb.Append("<script>");
                sb.Append(ErrorScript);
                sb.Append("</script>");
            }

            if (SyncScript.Length > 0)
            {
                sb.Append("<script>");
                sb.Append(SyncScript);
                sb.Append("</script>");
            }

            return sb.ToString();
        }

        #region Formed \ Generate error message & scripts
        public void FormedErrorMessage(string sectionName, string msg, string contolId = "")
        {
            GenerateErrorMsg(sectionName, msg);

            if (!string.IsNullOrWhiteSpace(contolId))
                GenerateErrorScript(contolId, msg);
        }

        private void GenerateErrorMsg(string sectionName, string error)
        {
            if(string.IsNullOrEmpty(sectionName))
                ErrorMsg.AppendLine(string.Format("<b>{1}</b><br/>", sectionName, error));
            else
                ErrorMsg.AppendLine(string.Format("<b>{0}</b> : {1}<br/>", sectionName, error));
        }

        private void GenerateErrorScript(string controlId, string msg)
        {
            ErrorScript.AppendLine(string.Format("$('#{0}').addClass('field-validation-error');", controlId));
            ErrorScript.AppendLine(string.Format("$('#{0}').after(\"<span class='span-validation-error' title='{1}'></span>\");", controlId, msg));
        }
        #endregion
    }
}
