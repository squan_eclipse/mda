﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OptimaJet.Workflow.Core.Runtime;
using OptimaJet.Workflow.Core.Model;
using OptimaJet.Workflow.Core.Builder;
using System.Xml.Linq;
using System.Web.Mvc;
using System.Web;

namespace OptimaJet.Workflow.Designer
{
    public enum GeneratorBlockName: int
    {
        Activity = 0,
        Transition,
        Actor,
        Command,
        Action,
        Parameter,
        Localization
    }
    public class Generator
    {
        WorkflowRuntime _runtime;
        ProcessInstance _processInstance;
        ProcessDefinition _processDef;
        ValidationResult validator = null;
        private static string[] BlockNames = new string[] { "Activity", "Transition", "Actor", "Command", "Action", "Parameter", "Localization" };
        public string TabUIPanelId = string.Empty;
        
        GeneratorSettings _settings = new GeneratorSettings();
        private string CallbackResFunc = string.Empty;
        public bool IsValid
        {
            get
            {
                if (validator == null)
                    return true;
                return validator.IsValid;
            }
        }

        public string ValidationErrors
        {
            get { return validator == null ? "" : validator.ErrorMsg.ToString(); }
        }
        public GeneratorSettings Settings
        {
            get
            {
                return _settings;
            }
        }

        public ProcessDefinition ProcessDefinition
        {
            get
            {
                return _processDef;
            }
        }

        public string ProcessName
        {
            get
            {
                return _processDef == null ? null: _processDef.Name;
            }
        }

        #region Constructors
        public Generator(WorkflowRuntime wfRuntime, GeneratorSettings settings)
        {
            _runtime = wfRuntime;
            _settings = settings;
        }

        public static Generator Create(WorkflowRuntime wfRuntime, GeneratorSettings settings)
        {
            var g = new Generator(wfRuntime, settings);
            return g;
        }

        public static Generator Create(GeneratorSettings settings)
        {
            WorkflowRuntime wfRuntime = new WorkflowRuntime(new Guid("{8D38DB8F-F3D5-4F26-A989-4FDD40F32D9D}"))
                .WithBuilder(new WorkflowBuilder<XElement>(null, new Core.Parser.XmlWorkflowParser(), null));                

            return Create(wfRuntime, settings);
        }
       
        #endregion

        #region LoadScheme
        public Generator LoadScheme(string wfSchema)
        {
            validator = null;
            var parser = (_runtime.Builder as WorkflowBuilder<XElement>).GetParser();

            if (string.IsNullOrWhiteSpace(wfSchema))
            {
                _processDef = new ProcessDefinition();
            }
            else
            {
                _processDef = parser.Parse(XElement.Parse(wfSchema));
            }

            return this;
        }

        public Generator LoadScheme(ProcessDefinition pd)
        {
            _processDef = pd;
            return this;
        }

        public Generator LoadScheme(ProcessInstance pi)
        {
            _processInstance = pi;
            LoadScheme(pi.ProcessScheme);
            return this;
        }
        #endregion

        #region HTML
        public MvcHtmlString GetHtml(bool generateParentDiv = true)
        {
            
            var sb = new StringBuilder();

            string headDiv = Settings.GetControlId("HeadDiv");
            string parentDiv = Settings.GetControlId("ParentDiv");
            string processNameId = Settings.GetControlId("ProcessName");
            if(generateParentDiv)
                sb.AppendFormat("<div id='{0}'>", parentDiv);

            sb.AppendFormat("<div id='{0}'>", headDiv);

            WriteParams(sb, processNameId, parentDiv);
         
            sb.AppendFormat("<br />");
            WriteTabs(sb);
            if(Settings.GraphVisible)
                WriteGraph(sb, parentDiv);

            WriteActivities(sb, BlockNames[(int)GeneratorBlockName.Activity]);
            WriteTransition(sb, BlockNames[(int)GeneratorBlockName.Transition]);
            WriteActor(sb, BlockNames[(int)GeneratorBlockName.Actor]);
            WriteCommand(sb, BlockNames[(int)GeneratorBlockName.Command]);
            WriteAction(sb, BlockNames[(int)GeneratorBlockName.Action]);
            WriteParameter(sb, BlockNames[(int)GeneratorBlockName.Parameter]);
            WriteLocalization(sb, BlockNames[(int)GeneratorBlockName.Localization]);

            sb.AppendFormat("</div>");

            WriteAddintionalScript(sb, parentDiv, headDiv, BlockNames);
            WriteValidationResult(sb);            

            if (generateParentDiv)
                sb.AppendFormat("</div>");

            return MvcHtmlString.Create(sb.ToString());
        }

        private void WriteActivities(StringBuilder sb, string name)
        {
            var parameters = new[]
            {
                new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityImpOrderText, Name = "Order" },                
                new HtmlTemplateColumnInfo()
                { 
                    Caption = Settings.ActivityImpActionText, 
                    Name = "Name", 
                    Type = HtmlTemplateColumnType.ComboBox,
                    TypeDic = _processDef.Actions.OrderBy(c=>c.Name).ToDictionary(c=>c.Name, c=>c.Name),
                    ComboBoxIsShowEmpty = true
                }
            };

            var columnsInfo = new List<HtmlTemplateColumnInfo>()
                    {
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityNameText, Name = "Name",  SubGroupName = Settings.ActivityCheckParamsText, TrakingChanging = true  },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityStateText, Name = "State", SubGroupName = Settings.ActivityCheckParamsText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityIsInitialText, Name = "IsInitial", Type = HtmlTemplateColumnType.CheckBox, SubGroupName = Settings.ActivityCheckParamsText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityIsFinalText, Name = "IsFinal", Type = HtmlTemplateColumnType.CheckBox, SubGroupName = Settings.ActivityCheckParamsText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityIsForSetStateText, Name = "IsForSetState", Type = HtmlTemplateColumnType.CheckBox, SubGroupName = Settings.ActivityCheckParamsText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityIsAutoSchemeUpdateText, Name = "IsAutoSchemeUpdate", Type = HtmlTemplateColumnType.CheckBox, SubGroupName = Settings.ActivityCheckParamsText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityImplementationText, Name = "Imp",  SubGroupName = Settings.ActivityImplementationGroupText,
                            Type = HtmlTemplateColumnType.ChildTable, 
                            Childs = parameters
                        },            
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActivityPreExecutionImplementationText, Name = "PEImp", SubGroupName = Settings.ActivityImplementationGroupText,
                            Type = HtmlTemplateColumnType.ChildTable, 
                            Childs = parameters
                        },
                        new HtmlTemplateColumnInfo(){ Caption = "DesignerId", Name = "DesignerId", Type = HtmlTemplateColumnType.Hidden},
                        new HtmlTemplateColumnInfo(){ Caption = "DesignerX", Name = "DesignerX", Type = HtmlTemplateColumnType.Hidden},
                        new HtmlTemplateColumnInfo(){ Caption = "DesignerY", Name = "DesignerY", Type = HtmlTemplateColumnType.Hidden}
     
                    };

             WriteBlockDefault(sb, name, columnsInfo.ToArray(), true);

            sb.Append("<script>");

            foreach (var item in _processDef.Activities)
            {
                sb.Append(HtmlTemplate.CreateRowScript(Settings, name, new object[]{
                        item.Name, 
                        item.State,
                        item.IsInitial ? 1 : 0,
                        item.IsFinal ? 1 : 0,
                        item.IsForSetState ? 1 : 0,
                        item.IsAutoSchemeUpdate ? 1 : 0,
                        item.Implementation.Select(c=> new object[]{ c.Order, c.Name}).ToArray(),
                        item.PreExecutionImplementation.Select(c=> new object[]{ c.Order, c.Name}).ToArray(),
                        item.DesignerSettings.IdIsEmpty ? DesignerSettings.GenerateNewDesignerId() : item.DesignerSettings.Id,
                        item.DesignerSettings.X,
                        item.DesignerSettings.Y
                    }));
            }

            sb.Append("</script>");
            sb.AppendFormat("</div>");    
        }

        private void WriteTransition(StringBuilder sb, string name)
        {
            var restTypeDic = new Dictionary<string, string>();
            restTypeDic.Add(RestrictionType.Allow.ToString(), Settings.RestrictionTypeAllowText);
            restTypeDic.Add(RestrictionType.Restrict.ToString(), Settings.RestrictionTypeRestrictText);

            var triggerTypeDic = new Dictionary<string, string>();
            triggerTypeDic.Add(TriggerType.Command.ToString(), Settings.TriggerTypeCommandText);
            triggerTypeDic.Add(TriggerType.Auto.ToString(), Settings.TriggerTypeAutoText);
            triggerTypeDic.Add(TriggerType.Timer.ToString(), Settings.TriggerTypeTimerText);
            

            var classifierDic = new Dictionary<string, string>();
            classifierDic.Add(TransitionClassifier.Direct.ToString(), Settings.TransitionClassifierDirectText);
            classifierDic.Add(TransitionClassifier.Reverse.ToString(), Settings.TransitionClassifierReverseText);
            classifierDic.Add(TransitionClassifier.NotSpecified.ToString(), Settings.TransitionClassifierNotSpecifiedText);

            var conditionTypeDic = new Dictionary<string, string>();
            conditionTypeDic.Add(ConditionType.Always.ToString(), Settings.TransitionConditionTypeAlwaysText);
            conditionTypeDic.Add(ConditionType.Action.ToString(), Settings.TransitionConditionTypeActionText);
            conditionTypeDic.Add(ConditionType.Otherwise.ToString(), Settings.TransitionConditionTypeOtherwiseText);

            var resultOnPEDic = new Dictionary<string, string>();
            resultOnPEDic.Add("", "Null");
            resultOnPEDic.Add("True", "True");
            resultOnPEDic.Add("False", "False");


            var columnsInfo = new List<HtmlTemplateColumnInfo>()
                {
                    new HtmlTemplateColumnInfo() {Caption = Settings.TransitionNameText, Name = "Name", SubGroupName = Settings.TransitionFromToText},
                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionFromText,
                            Name = "From",
                            SubGroupName = Settings.TransitionFromToText,
                            Type = HtmlTemplateColumnType.ComboBoxDynamic,
                            ArrayName = "Activity"                            
                        },
                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionToText,
                            Name = "To",
                            SubGroupName = Settings.TransitionFromToText,
                            Type = HtmlTemplateColumnType.ComboBoxDynamic,
                            ArrayName = "Activity"   
                        },

                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionClassifierText,
                            Name = "Classifier",
                            SubGroupName = Settings.TransitionFromToText,
                            Type = HtmlTemplateColumnType.ComboBox,
                            TypeDic = classifierDic
                        },

                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionRestrictionsText,
                            Name = "Rest",
                            Type = HtmlTemplateColumnType.ChildTable,
                            Childs = new HtmlTemplateColumnInfo[]
                                {
                                    new HtmlTemplateColumnInfo()
                                        {
                                            Caption = Settings.TransitionRestrictionsTypeText,
                                            Name = "Type",
                                            Type = HtmlTemplateColumnType.ComboBox,
                                            TypeDic = restTypeDic
                                        },
                                    new HtmlTemplateColumnInfo()
                                        {
                                            Caption = Settings.TransitionRestrictionsActorText,
                                            Name = "Actor",
                                            Type = HtmlTemplateColumnType.ComboBox,
                                            TypeDic = _processDef.Actors.OrderBy(c => c.Name).ToDictionary(c => c.Name, c => c.Name),
                                            ComboBoxIsShowEmpty = true
                                        }
                                }
                        },
                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionTriggerTypeText,
                            Name = "TriggerType",
                            SubGroupName = Settings.TransitionTriggersConditionGroupText,
                            Type = HtmlTemplateColumnType.ComboBox,
                            TypeDic = triggerTypeDic
                        },
                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionTriggerCommandText,
                            Name = "TriggerCommand",
                            SubGroupName = Settings.TransitionTriggersConditionGroupText,
                            Type = HtmlTemplateColumnType.ComboBox,
                            TypeDic = _processDef.Commands.OrderBy(c => c.Name).ToDictionary(c => c.Name, c => c.Name),
                            ComboBoxIsShowEmpty = true
                        },

                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionConditionTypeText,
                            Name = "ConditionType",
                            SubGroupName = Settings.TransitionTriggersConditionGroupText,
                            Type = HtmlTemplateColumnType.ComboBox,
                            TypeDic = conditionTypeDic
                        },

                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionConditionActionText,
                            Name = "ConditionAction",
                            SubGroupName = Settings.TransitionTriggersConditionGroupText,
                            Type = HtmlTemplateColumnType.ComboBox,
                            TypeDic = _processDef.Actions.OrderBy(c => c.Name).ToDictionary(c => c.Name, c => c.Name),
                            ComboBoxIsShowEmpty = true
                        },

                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.TransitionResultOnPreExecutionText,
                            Name = "ResultOnPreExecution",
                            SubGroupName = Settings.TransitionTriggersConditionGroupText,
                            Type = HtmlTemplateColumnType.ComboBox,
                            TypeDic = resultOnPEDic
                        },
                    new HtmlTemplateColumnInfo() {Caption = "DesignerId", Name = "DesignerId", Type = HtmlTemplateColumnType.Hidden},
                    new HtmlTemplateColumnInfo() {Caption = "DesignerBending", Name = "DesignerBending", Type = HtmlTemplateColumnType.Hidden}
                };

            WriteBlockDefault(sb, name, columnsInfo.ToArray());

            sb.Append("<script>");

            foreach (var item in _processDef.Transitions)
            {
                sb.Append(HtmlTemplate.CreateRowScript(Settings, name, new object[]{
                        item.Name, 
                        item.From == null ? string.Empty : item.From.Name,
                        item.To == null ? string.Empty : item.To.Name,
                        item.Classifier,
                        item.Restrictions.Select(c=> new object[]{ c.Type, (c.Actor == null ? string.Empty : c.Actor.Name) }).ToArray(),
                        
                        item.Trigger == null ? string.Empty : item.Trigger.Type.ToString(),
                        item.Trigger == null? string.Empty : item.Trigger.NameRef,
                        
                        item.Condition == null ? string.Empty : item.Condition.Type.ToString(),
                        (item.Condition == null || item.Condition.Action == null) ? string.Empty : item.Condition.Action.Name,
                        item.Condition == null ? string.Empty : item.Condition.ResultOnPreExecution.ToString(),
                         item.DesignerSettings.IdIsEmpty ? DesignerSettings.GenerateNewDesignerId() : item.DesignerSettings.Id,
                        item.DesignerSettings.Bending
                    }));
            }

            sb.Append("</script>");
            sb.AppendFormat("</div>");    
        }

        private void WriteActor(StringBuilder sb, string name)
        {
            var typeDic = new Dictionary<string, string>();
            typeDic.Add("Identity", Settings.ActorTypeIdentityText);
            typeDic.Add("InRole", Settings.ActorTypeInRoleText);
            typeDic.Add("ExecuteRule", Settings.ActorTypeExecuteRuleText);

            var columnsInfo = new List<HtmlTemplateColumnInfo>()
                    {
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActorNameText, CaptionStyle = "width:250px", Name = "Name" },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActorTypeText, Name = "Type", 
                            Type = HtmlTemplateColumnType.ComboBox, 
                            TypeDic = typeDic
                        },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActorValueText, Name = "Value", CaptionStyle= "width:400px" }
                    };

            WriteBlockDefault(sb, name, columnsInfo.ToArray());

            sb.Append("<script>");

           
            foreach (var item in _processDef.Actors)
            {
                object[] obj = null;
                if (item is ActorDefinitionIsIdentity)
                {
                    obj = new object[]{ item.Name, "Identity", (((ActorDefinitionIsIdentity)item).IdentityId) };
                }
                else if (item is ActorDefinitionIsInRole)
                {
                  obj = new object[]{ item.Name, "InRole", (((ActorDefinitionIsInRole)item).RoleId) };
                }
                else if (item is ActorDefinitionExecuteRule)
                {
                 obj = new object[]{ item.Name, "ExecuteRule", (((ActorDefinitionExecuteRule)item).RuleName) };
                }

                sb.Append(HtmlTemplate.CreateRowScript(Settings, name, obj));
            }

            sb.Append("</script>");
            sb.AppendFormat("</div>");
        }

        private void WriteCommand(StringBuilder sb, string name)
        {
            HtmlTemplateColumnInfo[] parameters = new HtmlTemplateColumnInfo[]
            {
                new HtmlTemplateColumnInfo()
                { 
                    Caption = Settings.CommandParameterNameText, 
                    Name = "Name", 
                    Type = HtmlTemplateColumnType.ComboBox,
                    TypeDic = _processDef.Parameters.OrderBy(c=>c.Name).ToDictionary(c=>c.Name, c=>c.Name),
                    ComboBoxIsShowEmpty = true

                }
            };

            var columnsInfo = new List<HtmlTemplateColumnInfo>()
                    {
                        new HtmlTemplateColumnInfo(){ Caption = Settings.CommandNameText, CaptionStyle = "width:250px", Name = "Name" },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.CommandParameterText, Name = "IP", 
                            Type = HtmlTemplateColumnType.ChildTable, 
                            Childs = parameters
                        }            
                    };


            WriteBlockDefault(sb, name, columnsInfo.ToArray());

            sb.Append("<script>");

            foreach (var item in _processDef.Commands)
            {
                sb.Append(HtmlTemplate.CreateRowScript(Settings, name, new object[]{
                        item.Name, 
                        item.InputParameters.Select(c=> c.Value == null ? string.Empty : c.Value.Name).ToArray()
                    }));
            }

            sb.Append("</script>");
            sb.AppendFormat("</div>");    
        }

        private void WriteAction(StringBuilder sb, string name)
        {
            var parameters = new HtmlTemplateColumnInfo[]
            {
                new HtmlTemplateColumnInfo(){ Caption = Settings.ActionParametrOrderText, Name = "Order" },
                new HtmlTemplateColumnInfo()
                { 
                    Caption = Settings.ActionParametrNameText, 
                    Name = "Name", 
                    Type = HtmlTemplateColumnType.ComboBox,
                    TypeDic = _processDef.Parameters.OrderBy(c=>c.Name).ToDictionary(c=>c.Name, c=>c.Name),
                    ComboBoxIsShowEmpty = true
                }
            };

            var columnsInfo = new List<HtmlTemplateColumnInfo>()
                    {
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActionNameText, Name = "Name", SubGroupName = Settings.ActionMainParametersText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActionTypeText, Name = "Type", SubGroupName = Settings.ActionMainParametersText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActionMethodNameText, Name = "MethodName", SubGroupName = Settings.ActionMainParametersText },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActionInputParametersText, Name = "IP", SubGroupName = Settings.ActionParametersGroupText,
                            Type = HtmlTemplateColumnType.ChildTable, 
                            Childs = parameters
                        },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.ActionOutputParametersText, Name = "OP", SubGroupName = Settings.ActionParametersGroupText,
                            Type = HtmlTemplateColumnType.ChildTable, 
                            Childs = parameters
                        }            
                    };
            
            WriteBlockDefault(sb, name, columnsInfo.ToArray());

            sb.Append("<script>");

           foreach (var item in _processDef.Actions)
           {
                    sb.Append(HtmlTemplate.CreateRowScript(Settings, name, new object[]{
                        item.Name, 
                        item.FullTypeName, 
                        item.MethodName,
                        item.InputParameters.Select(c=> new object[]{ c.Order, c.Name }).ToArray(),
                        item.OutputParameters.Select(c=> new object[]{ c.Order, c.Name }).ToArray()
                    }));
                }          

            sb.Append("</script>");
            sb.AppendFormat("</div>");          
        }

        private void WriteParameter(StringBuilder sb, string name)
        {
            var purposeDic = new Dictionary<string, string>();
            purposeDic.Add(ParameterPurpose.Temporary.ToString(), Settings.ParameterPurposeTemporaryText);
            purposeDic.Add(ParameterPurpose.Persistence.ToString(), Settings.ParameterPurposePersistenceText);
            purposeDic.Add(ParameterPurpose.System.ToString(), Settings.ParameterPurposeSystemText);

            var columnsInfo = new List<HtmlTemplateColumnInfo>()
                {
                    new HtmlTemplateColumnInfo() {Caption = Settings.ParameterNameText, CaptionStyle = "width:250px", Name = "Name"},
                    new HtmlTemplateColumnInfo() {Caption = Settings.ParameterTypeText, Name = "Type", DefaultValue = "System.String"},
                    new HtmlTemplateColumnInfo()
                        {
                            Caption = Settings.ParameterPurposeText,
                            Name = "Purpose",
                            Type = HtmlTemplateColumnType.ComboBox,
                            TypeDic = purposeDic
                        },
                    new HtmlTemplateColumnInfo() {Caption = Settings.ParameterSerializedDefaultValueText, Name = "SerializedDefaultValue"}
                };

            if (_processInstance != null)
                columnsInfo.Add(new HtmlTemplateColumnInfo() {Caption = Settings.ParameterValueText, Name = "Value"});


            WriteBlockDefault(sb, name, columnsInfo.ToArray());

            sb.Append("<script>");

            if (_processInstance == null)
            {
                foreach (var item in _processDef.Parameters)
                {
                    sb.Append(HtmlTemplate.CreateRowScript(Settings,
                                                           name,
                                                           new object[]
                                                               {
                                                                   item.Name,
                                                                   item.Type,
                                                                   item.Purpose,
                                                                   item.SerializedDefaultValue
                                                               }));
                }
            }
            else
            {
                foreach (var item in _processInstance.ProcessParameters)
                {
                    sb.Append(HtmlTemplate.CreateRowScript(Settings,
                                                           name,
                                                           new[]
                                                               {
                                                                   item.Name,
                                                                   item.Type,
                                                                   item.Purpose,
                                                                   item.SerializedDefaultValue,
                                                                   item.Value
                                                               }));
                }
            }

            sb.Append("</script>");
            sb.AppendFormat("</div>");
        }

        private void WriteLocalization(StringBuilder sb, string name)
        {
            var typeDic = new Dictionary<string, string>();
            typeDic.Add(LocalizeType.Command.ToString(), Settings.LocalizationTypeCommandText);
            typeDic.Add(LocalizeType.State.ToString(), Settings.LocalizationTypeStateText);
            typeDic.Add(LocalizeType.Parameter.ToString(), Settings.LocalizationTypeParameterText);

            HtmlTemplateColumnInfo[] columnsInfo = new HtmlTemplateColumnInfo[]
                    {
                        new HtmlTemplateColumnInfo(){ Caption = Settings.LocalizationObjectNameText, CaptionStyle = "width:250px", Name = "ObjectName" },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.LocalizationTypeText, CaptionStyle = "width:100px", Name = "Type", 
                            Type = HtmlTemplateColumnType.ComboBox, 
                            TypeDic = typeDic
                        },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.LocalizationIsDefaultText, Name = "IsDefault", Type = HtmlTemplateColumnType.CheckBox },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.LocalizationCultureText, CaptionStyle = "width:100px", Name = "Culture", DefaultValue = Settings.LocalizationCultureDefault },
                        new HtmlTemplateColumnInfo(){ Caption = Settings.LocalizationValueText, CaptionStyle = "width:250px", Name = "Value" },
                    };

            WriteBlockDefault(sb, name, columnsInfo);

            sb.Append("<script>");
            foreach (var item in _processDef.Localization)
            {
                sb.Append(HtmlTemplate.CreateRowScript(Settings, name, new object[]{
                    item.ObjectName, 
                    item.Type, 
                    item.IsDefault ? 1 : 0, 
                    item.Culture, 
                    item.Value
                }));
            }
            sb.Append("</script>");             
            sb.AppendFormat("</div>");
        }

        private void WriteBlockDefault(StringBuilder sb, string name, HtmlTemplateColumnInfo[] columnsInfo, bool createArrayColl = false)
        {
            sb.Append(HtmlTemplate.StartDiv(Settings, name));
            sb.Append(HtmlTemplate.Table(Settings, name, columnsInfo));
            sb.Append("<script>");
            sb.Append(HtmlTemplate.OnCreate(Settings, name, columnsInfo, false, createArrayColl));
            sb.Append(HtmlTemplate.OnCopyPaste(Settings, name, columnsInfo));
            sb.Append(HtmlTemplate.OnDelete(Settings, name, false, createArrayColl));
            sb.Append(HtmlTemplate.GetParameter(Settings, name, columnsInfo));
            sb.Append("</script>");           
        }

        private void WriteAddintionalScript(StringBuilder sb, string parentDiv, string headDiv, string[] blockNames)
        {
            sb.Append("<script>");

            #region GetDesignerDataArray
            sb.AppendFormat(@"
function {0}GetDataArray() {{
var prefix = '{0}';
var data = new Array();
", Settings.PrefixId);

            sb.AppendFormat(@"
var processDefName = $('#{1}');
data.push({{ name: prefix + 'processName', value: processDefName[0].value }});
", Settings.PrefixId, Settings.GetControlId("ProcessName"));

            foreach (string s in blockNames)
                sb.AppendFormat(@"
data = data.concat({0}Get{1}());", Settings.PrefixId, s);

            sb.AppendFormat(@"return data;}}");
            #endregion

            #region GetDesignerDataObject
            ///
            sb.AppendFormat(@"
function {0}GetDataObject() {{
var result = new Object();
", Settings.PrefixId);

            foreach (string s in blockNames)
                sb.AppendFormat(@"
var obj = {0}Get{1}AsObject();
result['{1}'.toLowerCase()] = obj;

", Settings.PrefixId, s);

            sb.AppendFormat(@"return result;}}");
            #endregion

            #region ValidateAndSync
            sb.AppendFormat(@"
function {0}ValidateAndSync(uiPanelId) {{
        
        var data = {0}GetDataArray();
        
        var dataArray = {0}app.GetDesignerSettings('{0}' + 'Designer');
        dataArray.push({{ name: '{0}TabUIIndex', value: {0}tabUIIndex }});
        dataArray.push({{ name: '{0}TabUIPanelId', value: uiPanelId }});        
        data = data.concat(dataArray);        

        $('#{1}').mask('{2}');
        $('#{1}').load('{3}', data,
            function () {{
                $('#{1}').unmask();
            }});        
    }}
", Settings.PrefixId, parentDiv, Settings.ValidationText, Settings.UrlValidateAndSync);
            #endregion

            #region ValidateAndSave
            sb.AppendFormat(@"
function {0}ValidateAndSave(funcRes) {{
        var data = {0}GetDataArray();
        
        var dataArray = {0}app.GetDesignerSettings('{0}' + 'Designer');
        dataArray.push({{ name: '{0}TabUIIndex', value: {0}tabUIIndex }});
        dataArray.push({{ name: '{0}CallbackResFunc', value: funcRes }});        
        data = data.concat(dataArray);        
        
        $('#{1}').mask('{2}');
        $('#{1}').load('{3}', data,
            function () {{
                $('#{1}').unmask();
            }});        
    }}
", Settings.PrefixId, parentDiv, Settings.SaveText, Settings.UrlValidateAndSave);
            #endregion

            sb.AppendFormat(@"
$('#{0}').tabs();", headDiv);

            if (string.IsNullOrWhiteSpace(TabUIPanelId))
            {
                sb.AppendFormat(@"
var {0}tabUIIndex = 0;", Settings.PrefixId);
            }
            else
            {
                sb.AppendFormat(@"
$('#{0}').tabs('select', '#{1}');
", headDiv, TabUIPanelId);
            }

            if (!Settings.ReadOnly)
            {
                sb.AppendFormat(@"
$('#{1}').bind( 'tabsselect', function(event, ui) {{
        {0}ValidateAndSync(ui.panel.id);
}})
", Settings.PrefixId, headDiv);
            }

            if (Settings.GraphVisible)
            {
                sb.AppendFormat(@"
if({0}app == undefined){{
    var {0}app = new VisualDesigner({{Container : '{0}graph', Prefix: '{0}', Width: '{1}', Height: '{2}'}});
}}
else{{
     {0}app = new VisualDesigner({{Container : '{0}graph', Prefix: '{0}', Width: '{1}', Height: '{2}'}});
}}

{0}app.Draw({0}GetDataObject());
{0}app.SetDesignerSettings({3}, {4}, {5}); 
", Settings.PrefixId, Settings.GraphWidth, Settings.GraphHeight,
 string.IsNullOrWhiteSpace(_processDef.DesignerSettings.X) ? "undefined" : _processDef.DesignerSettings.X,
 string.IsNullOrWhiteSpace(_processDef.DesignerSettings.Y) ? "undefined" : _processDef.DesignerSettings.Y,
 string.IsNullOrWhiteSpace(_processDef.DesignerSettings.Scale) ? "undefined" : _processDef.DesignerSettings.Scale);
            }

            sb.Append("</script>");
        }

        private void WriteValidationResult(StringBuilder sb)
        {
            if (!string.IsNullOrEmpty(CallbackResFunc) && CallbackResFunc != "undefined")
            {
                sb.AppendFormat(@"
<script>
{0}({1});
</script>", CallbackResFunc, validator == null || validator.IsValid ? "1" : "0");
            }

            string resultDivId = Settings.GetControlId("ResultDiv");
            if (!Settings.ReadOnly)
            {
                sb.Append("<br />");
                sb.AppendFormat("<b>{0}:</b>", Settings.ResultText);
                sb.AppendFormat("<div id='{0}'>", resultDivId);
                if (validator != null)
                {
                    sb.AppendFormat(validator.GetHtml());
                }
                sb.AppendFormat("</div>");
            }
        }

        public string GetValidatorText()
        {
            if (validator == null)
                return null;
            return validator.GetHtml();
        }

        private void WriteGraph(StringBuilder sb, string parentDiv)
        {
            sb.AppendFormat("<div id='{0}tabGraph'>", Settings.PrefixId);
            sb.AppendFormat("<div style='overflow:auto;'><div id='{0}graph' class='base' style='width:{1}px;height:{2}px;' ></div></div></div>", 
                Settings.PrefixId, Settings.GraphWidth + 20, Settings.GraphHeight + 20);
        }

        private void WriteTabs(StringBuilder sb)
        {
            sb.AppendFormat("<ul>");
            if(Settings.GraphVisible)
                sb.AppendFormat("<li><a href='#{0}tabGraph'>{1}</a></li>", Settings.PrefixId, Settings.GraphText);
            sb.AppendFormat("<li><a href='#{0}tabActivity'>{1}</a></li>", Settings.PrefixId, Settings.ActivityText);
            sb.AppendFormat("<li><a href='#{0}tabTransition'>{1}</a></li>", Settings.PrefixId, Settings.TransitionText);
            sb.AppendFormat("<li><a href='#{0}tabActor'>{1}</a></li>", Settings.PrefixId, Settings.ActorText);
            sb.AppendFormat("<li><a href='#{0}tabCommand'>{1}</a></li>", Settings.PrefixId, Settings.CommandText);
            sb.AppendFormat("<li><a href='#{0}tabAction'>{1}</a></li>", Settings.PrefixId, Settings.ActionText);
            sb.AppendFormat("<li><a href='#{0}tabParameter'>{1}</a></li>", Settings.PrefixId, Settings.ParameterText);
            sb.AppendFormat("<li><a href='#{0}tabLocalization'>{1}</a></li>", Settings.PrefixId, Settings.LocalizationText);
            sb.AppendFormat("</ul>");         
        }

        private void WriteParams(StringBuilder sb, string processNameId, string parentDiv)
        {
            sb.AppendFormat(@"<table class='{0}'>
        <tbody>
            <tr>
                <td style='width:150px'><b>{1}</b></td>
                <td><input id='{2}' style='width:100%' value='{3}' {4}/></td>            
            </tr>",
                Settings.TableStyleName,
             Settings.ProcessNameText,
             processNameId,
             _processDef == null ? string.Empty : _processDef.Name,
             Settings.ReadOnly ? "readonly='readonly'" : string.Empty);
            
            sb.AppendFormat(@"</tbody></table>");            
        }

        #endregion
    
        public bool Validate()
        {
            validator = new ValidationResult();

            if(string.IsNullOrWhiteSpace(_processDef.Name))
            {
                validator.FormedErrorMessage(string.Empty, Settings.ErrorFieldIsRequired(Settings.ProcessNameText), Settings.GetControlId("ProcessName"));
            }

            #region Activities
            if (_processDef.Activities.Count == 0)
            {
                validator.FormedErrorMessage(Settings.ActivityText, Settings.ErrorAddObjectTypeToProcess(Settings.ActivityText));
            }

            List<string> activityNameList = new List<string>();
            for(int i =0; i < _processDef.Activities.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_processDef.Activities[i].Name))
                    validator.FormedErrorMessage(Settings.ActivityText, Settings.ErrorFieldIsRequired(Settings.ActivityNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Activity], i));

                if (activityNameList.Contains(_processDef.Activities[i].Name))
                {
                    validator.FormedErrorMessage(Settings.ActivityText, Settings.ErrorFieldMustBeUnique(Settings.ActivityNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Activity], i));
                }
                else
                {
                    activityNameList.Add(_processDef.Activities[i].Name);
                }

                for (int j = 0; j < _processDef.Activities[i].Implementation.Count; j++)
                {
                    if (_processDef.Activities[i].Implementation[j].ActionDefinition == null)
                    {
                        validator.FormedErrorMessage(Settings.ActivityText, Settings.ErrorFieldIsRequired(Settings.ActivityImpActionText), string.Format("{0}{1}_{2}_Imp_{3}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Activity], i, j));
                    }                    
                }

                for (int j = 0; j < _processDef.Activities[i].PreExecutionImplementation.Count; j++)
                {
                    if (_processDef.Activities[i].PreExecutionImplementation[j].ActionDefinition == null)
                    {
                        validator.FormedErrorMessage(Settings.ActivityText, Settings.ErrorFieldIsRequired(Settings.ActivityImpActionText), string.Format("{0}{1}_{2}_PEImp_{3}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Activity], i, j));
                    }
                }
            }

            if (_processDef.Activities.Count(c => c.IsInitial) != 1)
                validator.FormedErrorMessage(Settings.ActivityText, Settings.ErrorActivityIsInitialCountText);
                        
            #endregion

            #region Transition
            if (_processDef.Transitions.Count == 0)
            {
                validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorAddObjectTypeToProcess(Settings.TransitionText));
            }

            List<string> transNameList = new List<string>();
            for (int i = 0; i < _processDef.Transitions.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_processDef.Transitions[i].Name))
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldIsRequired(Settings.TransitionNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));
                
                if (_processDef.Transitions[i].From == null)
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldIsRequired(Settings.TransitionFromText), string.Format("{0}{1}_{2}_From", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));

                if (_processDef.Transitions[i].To == null)
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldIsRequired(Settings.TransitionToText), string.Format("{0}{1}_{2}_To", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));

                if (_processDef.Transitions[i].From != null && _processDef.Transitions[i].To != null &&
                    _processDef.Transitions[i].From.Name == _processDef.Transitions[i].To.Name)
                {
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldNotEquals(Settings.TransitionFromText, Settings.TransitionToText), string.Format("{0}{1}_{2}_From", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldNotEquals(Settings.TransitionToText, Settings.TransitionFromText), string.Format("{0}{1}_{2}_To", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));
                }

                if (transNameList.Contains(_processDef.Transitions[i].Name))
                {
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldMustBeUnique(Settings.TransitionNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));
                }
                else
                {
                    transNameList.Add(_processDef.Transitions[i].Name);
                }

                if (_processDef.Transitions[i].Trigger != null && 
                    _processDef.Transitions[i].Trigger.Type == TriggerType.Command &&
                    _processDef.Transitions[i].Trigger.Command == null)
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldIsRequired(Settings.TransitionTriggerCommandText), string.Format("{0}{1}_{2}_TriggerCommand", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));


                if (_processDef.Transitions[i].Condition != null &&
                   _processDef.Transitions[i].Condition.Type == ConditionType.Action &&
                   _processDef.Transitions[i].Condition.Action == null)
                    validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldIsRequired(Settings.TransitionConditionActionText), string.Format("{0}{1}_{2}_ConditionAction", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i));

                List<RestrictionDefinition> restList = _processDef.Transitions[i].Restrictions as List<RestrictionDefinition>;
                for (int j = 0; j < restList.Count; j++)
                {
                    if (restList[j].Actor == null)
                    {
                        validator.FormedErrorMessage(Settings.TransitionText, Settings.ErrorFieldIsRequired(Settings.TransitionRestrictionsActorText), string.Format("{0}{1}_{2}_Rest_{3}_Actor", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Transition], i, j));
                    }
                }
            }
            #endregion

            #region Actor
            List<string> actorNameList = new List<string>();
            for (int i = 0; i < _processDef.Actors.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_processDef.Actors[i].Name))
                    validator.FormedErrorMessage(Settings.ActorText, Settings.ErrorFieldIsRequired(Settings.ActorNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Actor], i));

                if (_processDef.Actors[i] is ActorDefinitionExecuteRule && string.IsNullOrWhiteSpace(((ActorDefinitionExecuteRule)_processDef.Actors[i]).RuleName) ||
                    _processDef.Actors[i] is ActorDefinitionIsInRole && string.IsNullOrWhiteSpace(((ActorDefinitionIsInRole)_processDef.Actors[i]).RoleId))
                    validator.FormedErrorMessage(Settings.ActorText, Settings.ErrorFieldIsRequired(Settings.ActorValueText), string.Format("{0}{1}_{2}_Value", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Actor], i));

                if (actorNameList.Contains(_processDef.Actors[i].Name))
                {
                    validator.FormedErrorMessage(Settings.ActorText, Settings.ErrorFieldMustBeUnique(Settings.ActorNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Actor], i));
                }
                else
                {
                    actorNameList.Add(_processDef.Actors[i].Name);
                }
            }                      
            #endregion

            #region Command
            List<string> commandNameList = new List<string>();
            for (int i = 0; i < _processDef.Commands.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_processDef.Commands[i].Name))
                    validator.FormedErrorMessage(Settings.CommandText, Settings.ErrorFieldIsRequired(Settings.CommandNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Command], i));

                if (commandNameList.Contains(_processDef.Commands[i].Name))
                {
                    validator.FormedErrorMessage(Settings.CommandText, Settings.ErrorFieldMustBeUnique(Settings.CommandNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Command], i));
                }
                else
                {
                    commandNameList.Add(_processDef.Commands[i].Name);
                }
            }
            #endregion

            #region Actions
            List<string> actionNameList = new List<string>();
            for (int i = 0; i < _processDef.Actions.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_processDef.Actions[i].Name))
                    validator.FormedErrorMessage(Settings.ActionText, Settings.ErrorFieldIsRequired(Settings.ActionNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Action], i));

                if (string.IsNullOrWhiteSpace(_processDef.Actions[i].FullTypeName))
                    validator.FormedErrorMessage(Settings.ActionText, Settings.ErrorFieldIsRequired(Settings.ActionTypeText), string.Format("{0}{1}_{2}_Type", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Action], i));

                if (string.IsNullOrWhiteSpace(_processDef.Actions[i].MethodName))
                    validator.FormedErrorMessage(Settings.ActionText, Settings.ErrorFieldIsRequired(Settings.ActionMethodNameText), string.Format("{0}{1}_{2}_MethodName", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Action], i));


                if (actionNameList.Contains(_processDef.Actions[i].Name))
                {
                    validator.FormedErrorMessage(Settings.ActionText, Settings.ErrorFieldMustBeUnique(Settings.ActionNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Action], i));
                }
                else
                {
                    actionNameList.Add(_processDef.Actions[i].Name);
                }
            }
            #endregion

            #region Parameters
            List<string> paramNameList = new List<string>();
            for (int i = 0; i < _processDef.Parameters.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_processDef.Parameters[i].Name))
                    validator.FormedErrorMessage(Settings.ParameterText, Settings.ErrorFieldIsRequired(Settings.ParameterNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Parameter], i));

                if (paramNameList.Contains(_processDef.Parameters[i].Name))
                {
                    validator.FormedErrorMessage(Settings.ParameterText, Settings.ErrorFieldMustBeUnique(Settings.ParameterNameText), string.Format("{0}{1}_{2}_Name", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Parameter], i));
                }
                else
                {
                    paramNameList.Add(_processDef.Parameters[i].Name);
                }
            }
            #endregion

            #region Localization
            for (int i = 0; i < _processDef.Localization.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(_processDef.Localization[i].ObjectName))
                    validator.FormedErrorMessage(Settings.LocalizationText, Settings.ErrorFieldIsRequired(Settings.LocalizationObjectNameText), string.Format("{0}{1}_{2}_ObjectName", Settings.PrefixId, BlockNames[(int)GeneratorBlockName.Localization], i));
            }
            #endregion

            return validator.IsValid;
        }


        public void LoadFromParams(Dictionary<string, string> pars)
        {
            validator = null;
            _processDef = new ProcessDefinition();
            _processDef.Name = pars[string.Format("{0}processName", Settings.PrefixId)];
            
            string designerXParName = string.Format("{0}DesignerX", Settings.PrefixId);
            string designerYParName = string.Format("{0}DesignerY", Settings.PrefixId);
            string designerScaleParName = string.Format("{0}DesignerScale", Settings.PrefixId);

            if(pars.ContainsKey(designerXParName))
                _processDef.DesignerSettings.X = pars[designerXParName];
            
            if (pars.ContainsKey(designerYParName))
                _processDef.DesignerSettings.Y = pars[designerYParName];

            if (pars.ContainsKey(designerScaleParName))
                _processDef.DesignerSettings.Scale = pars[designerScaleParName];

            string uiIndexParName = string.Format("{0}TabUIPanelId", Settings.PrefixId);
            
            if(pars.ContainsKey(uiIndexParName))
                TabUIPanelId = pars[uiIndexParName];
            
            string funcRes = string.Format("{0}CallbackResFunc", Settings.PrefixId);
            if (pars.ContainsKey(funcRes))
                 CallbackResFunc = pars[funcRes];
            
            #region Fill blocks
            List<string> tmp = new List<string>();
            for (int step = 0; step < 4; step++)
            {
                foreach (var item in pars)
                {
                    string prefix = GetKeyToName(item.Key);
                    if (tmp.Contains(prefix))
                        continue;

                    var paramColl = pars.Where(c => c.Key.Contains(prefix)).ToList();
                    if (prefix.Contains(BlockNames[(int)GeneratorBlockName.Localization]))
                    {
                        LoadLocalizationFromParams(prefix, paramColl);
                    }
                    else if (prefix.Contains(BlockNames[(int)GeneratorBlockName.Parameter]))
                    {
                        LoadParameterFromParams(prefix, paramColl);
                    }
                    else if (prefix.Contains(BlockNames[(int)GeneratorBlockName.Actor]))
                    {
                        LoadActorFromParams(prefix, paramColl);
                    }
                    else
                    {
                        if (step < 1)
                            continue;

                        if (prefix.Contains(BlockNames[(int)GeneratorBlockName.Action]))
                        {
                            LoadActionFromParams(prefix, paramColl);
                        }
                        else if (prefix.Contains(BlockNames[(int)GeneratorBlockName.Command]))
                        {
                            LoadCommandFromParams(prefix, paramColl);
                        }
                        else
                        {
                            if (step < 2)
                                continue;

                            if (prefix.Contains(BlockNames[(int)GeneratorBlockName.Activity]))
                            {
                                LoadActivityFromParams(prefix, paramColl);
                            }
                            else
                            {
                                if (step < 3)
                                    continue;

                                if (prefix.Contains(BlockNames[(int)GeneratorBlockName.Transition]))
                                {
                                    LoadTransitionFromParams(prefix, paramColl);
                                }
                            }
                        }
                    }                    

                    tmp.Add(prefix);
                }
            }
            #endregion

        }

        private void LoadCommandFromParams(string prefix, List<KeyValuePair<string, string>> paramColl)
        {
            string name = string.Empty;
            Dictionary<string, ParameterDefinition> par = new Dictionary<string,ParameterDefinition>();
        
            foreach (var p in paramColl)
            {
                int index = p.Key.IndexOf('.');
                if (index <= 0)
                    continue;

                if (p.Key == string.Format("{0}.Name", prefix))
                {
                    name = p.Value;
                }
                else if (p.Key.Contains(string.Format("{0}.IP", prefix)))
                {
                    foreach (var parameterName in p.Value.Split(','))
                    {
                        var parameter = _processDef.Parameters.FirstOrDefault(c => c.Name == parameterName);
                        if (parameter != null)
                            par.Add(parameter.Name, parameter);
                    }
                }
            }

            var item = CommandDefinition.Create(name);

            foreach (var p in par)
                item.InputParameters.Add(p.Key, p.Value);

            _processDef.Commands.Add(item);
        }

        private void LoadTransitionFromParams(string prefix, List<KeyValuePair<string, string>> paramColl)
        {
            string name = string.Empty;
            string classifier = string.Empty;
            ActivityDefinition from = null;
            ActivityDefinition to = null;
            string triggerType = string.Empty;
            CommandDefinition triggerCommand = null;
            string conditionType = string.Empty;
            ActionDefinition conditionAction = null;
            string resultOnPreExecution = string.Empty;
            List<RestrictionDefinition> rest = new List<RestrictionDefinition>();

            string designerId = string.Empty;
            string designerBending = string.Empty;

            foreach (var p in paramColl)
            {
                int index = p.Key.IndexOf('.');
                if (index <= 0)
                    continue;

                if (p.Key == string.Format("{0}.Name", prefix))
                {
                    name = p.Value;
                }
                else if (p.Key == string.Format("{0}.Classifier", prefix))
                {
                    classifier = p.Value;
                }
                else if (p.Key == string.Format("{0}.From", prefix))
                {
                    from = _processDef.Activities.FirstOrDefault(c => c.Name == p.Value);
                }
                else if (p.Key == string.Format("{0}.To", prefix))
                {
                    to = _processDef.Activities.FirstOrDefault(c => c.Name == p.Value);
                }
                else if (p.Key == string.Format("{0}.TriggerType", prefix))
                {
                    triggerType = p.Value;
                }
                else if (p.Key == string.Format("{0}.TriggerCommand", prefix))
                {
                    triggerCommand = _processDef.Commands.FirstOrDefault(c => c.Name == p.Value);
                }
                else if (p.Key == string.Format("{0}.ConditionType", prefix))
                {
                    conditionType = p.Value;
                }
                else if (p.Key == string.Format("{0}.ConditionAction", prefix))
                {
                    conditionAction =  _processDef.Actions.FirstOrDefault(c => c.Name == p.Value);
                }
                else if (p.Key == string.Format("{0}.ResultOnPreExecution", prefix))
                {
                    resultOnPreExecution = p.Value;
                }
                else if (p.Key.Contains(string.Format("{0}.Rest", prefix)))
                {
                    string[] values = p.Value.Split(',');
                    for (int i = 0; i < values.Length / 2; i++)
                    {
                        int k = i * 2;
                        var actor = _processDef.Actors.FirstOrDefault(c => c.Name == values[k+ 1]);
                        rest.Add(RestrictionDefinition.Create(values[k], actor));
                    }                    
                }
                else if (p.Key == string.Format("{0}.DesignerId", prefix))
                {
                    designerId = p.Value;
                }
                else if (p.Key == string.Format("{0}.DesignerBending", prefix))
                {
                    designerBending = p.Value;
                }
            }

            TriggerDefinition trigger = TriggerDefinition.Create(triggerType);
            if(trigger.Type == TriggerType.Command)
            {
                (trigger as CommandTriggerDefinition).Command = triggerCommand;
            }

            ConditionDefinition condition = ConditionDefinition.Create(conditionType, conditionAction, resultOnPreExecution);
            
            var item = TransitionDefinition.Create(name, classifier, from, to, trigger, condition);
            (item.Restrictions as List<RestrictionDefinition>).AddRange(rest);
            item.DesignerSettings.Id = designerId;
            item.DesignerSettings.Bending = designerBending;

            _processDef.Transitions.Add(item);
        }

        private void LoadActionFromParams(string prefix, List<KeyValuePair<string, string>> paramColl)
        {
            string name = string.Empty;
            string type = string.Empty;
            string methodName = string.Empty;
            var ip = new List<ParameterDefinitionForAction>();
            var op = new List<ParameterDefinitionForAction>();

            foreach (var p in paramColl)
            {
                int index = p.Key.IndexOf('.');
                if (index <= 0)
                    continue;

                if (p.Key == string.Format("{0}.Name", prefix))
                {
                    name = p.Value;
                }
                else if (p.Key == string.Format("{0}.Type", prefix))
                {
                    type = p.Value;
                }
                else if (p.Key == string.Format("{0}.MethodName", prefix))
                {
                     methodName = p.Value;
                }              
                else if (p.Key.Contains(string.Format("{0}.IP", prefix)))
                {
                    string[] values = p.Value.Split(',');
                    
                    for(int i=0; i < values.Length / 2 ;i++)
                    {
                        int k = i * 2;
                        var parameter = _processDef.Parameters.FirstOrDefault(c => c.Name == values[k + 1]);
                        ip.Add(ParameterDefinitionForAction.Create(parameter, values[k]));
                    }
                }
                else if (p.Key.Contains(string.Format("{0}.OP", prefix)))
                {
                    string[] values = p.Value.Split(',');

                    for (int i = 0; i < values.Length / 2; i++)
                    {
                        int k = i * 2;
                        var parameter = _processDef.Parameters.FirstOrDefault(c => c.Name == values[k + 1]);
                        op.Add(ParameterDefinitionForAction.Create(parameter, values[k]));
                    }
                }
            }

            var item = ActionDefinition.Create(name, type, methodName);
            
            (item.InputParameters as List<ParameterDefinitionForAction>).AddRange(ip);
            (item.OutputParameters as List<ParameterDefinitionForAction>).AddRange(op);
            _processDef.Actions.Add(item);
        }

        private void LoadActorFromParams(string prefix, List<KeyValuePair<string, string>> paramColl)
        {           
            string name = string.Empty;
            string type = string.Empty;
            string value = string.Empty;

            foreach (var p in paramColl)
            {
                int index = p.Key.IndexOf('.');
                if (index <= 0)
                    continue;


                if (p.Key == string.Format("{0}.Name", prefix))
                {
                    name = p.Value;
                }
                else if (p.Key == string.Format("{0}.Type", prefix))
                {
                    type = p.Value;
                }
                else if (p.Key == string.Format("{0}.Value", prefix))
                {
                    value = p.Value;
                }              
            }

            ActorDefinition item = null;
            switch(type)
            {
                case "ExecuteRule":
                    item = ActorDefinition.CreateRule(name, value);
                    break;
                case "InRole":
                    item = ActorDefinition.CreateIsInRole(name, value);
                    break;
                case "Identity":
                    Guid tryValue = Guid.Empty;
                    Guid.TryParse(value, out tryValue);
                    item = ActorDefinition.CreateIsIdentity(name, tryValue.ToString());
                    break;
            }

            _processDef.Actors.Add(item);
        }

        private void LoadParameterFromParams(string prefix, List<KeyValuePair<string, string>> paramColl)
        {
            string name = string.Empty; 
            string type = string.Empty; 
            string purpose = string.Empty;
            string serializedValue = string.Empty;

            foreach (var p in paramColl)
            {
                int index = p.Key.IndexOf('.');
                if (index <= 0)
                    continue;
                
                if (p.Key == string.Format("{0}.Name", prefix))
                {
                    name = p.Value;
                }
                else if (p.Key == string.Format("{0}.Type", prefix))
                {
                    type = p.Value;
                }
                else if (p.Key == string.Format("{0}.Purpose", prefix))
                {
                    purpose = p.Value;
                }
                else if (p.Key == string.Format("{0}.SerializedDefaultValue", prefix))
                {
                    serializedValue = p.Value;
                }                
            }

            _processDef.Parameters.Add(ParameterDefinition.Create(name, type, purpose, serializedValue));
        }

        private void LoadLocalizationFromParams(string prefix, List<KeyValuePair<string, string>> paramColl)
        {
            string objectName = string.Empty; 
            string type = string.Empty; 
            string culture = string.Empty; 
            string value = string.Empty;
            string isDefault = string.Empty;

            foreach (var p in paramColl)
            {
                int index = p.Key.IndexOf('.');
                if (index <= 0)
                    continue;

                if (p.Key == string.Format("{0}.ObjectName", prefix))
                {
                    objectName = p.Value;
                }
                else if (p.Key == string.Format("{0}.Type", prefix))
                {
                    type = p.Value;
                }
                else if (p.Key == string.Format("{0}.Culture", prefix))
                {
                    culture = p.Value;
                }
                else if (p.Key == string.Format("{0}.Value", prefix))
                {
                    value = p.Value;
                }
                else if (p.Key == string.Format("{0}.IsDefault", prefix))
                {
                    isDefault = p.Value;
                }
            }

            _processDef.Localization.Add(LocalizeDefinition.Create(objectName, type, culture, value, isDefault));
        }

        private void LoadActivityFromParams(string prefix, List<KeyValuePair<string, string>> paramColl)
        {
            string name = string.Empty;
            string state = null;
            string isInitial = string.Empty;
            string isFinal = string.Empty;
            string isForSetState = string.Empty;
            string isAutoSchemeUpdate = string.Empty;
            var imp = new List<ActionDefinitionForActivity>();
            var peimp = new List<ActionDefinitionForActivity>();

            string designerId = string.Empty;
            string designerX = string.Empty;
            string designerY = string.Empty;

            foreach (var p in paramColl)
            {
                int index = p.Key.IndexOf('.');
                if (index <= 0)
                    continue;
                                
                if (p.Key == string.Format("{0}.Name", prefix))
                {
                    name = p.Value;
                }
                else if (p.Key == string.Format("{0}.State", prefix) && !string.IsNullOrEmpty(p.Value))
                {
                    state = p.Value;
                }
                else if (p.Key == string.Format("{0}.IsInitial", prefix))
                {
                    isInitial = p.Value;
                }
                else if (p.Key == string.Format("{0}.IsFinal", prefix))
                {
                    isFinal = p.Value;
                }
                else if (p.Key == string.Format("{0}.IsForSetState", prefix))
                {
                    isForSetState = p.Value;
                }
                else if (p.Key == string.Format("{0}.IsAutoSchemeUpdate", prefix))
                {
                    isAutoSchemeUpdate = p.Value;
                }
                else if (p.Key.Contains(string.Format("{0}.Imp", prefix)))
                {
                    string[] values = p.Value.Split(',');

                    for (int i = 0; i < values.Length / 2; i++)
                    {
                        int k = i * 2;
                        var action = _processDef.Actions.FirstOrDefault(c => c.Name == values[k + 1]);
                        imp.Add(ActionDefinitionForActivity.Create(action, values[k]));
                    }
                }
                else if (p.Key.Contains(string.Format("{0}.PEImp", prefix)))
                {
                    string[] values = p.Value.Split(',');
                    for (int i = 0; i < values.Length / 2; i++)
                    {
                        int k = i * 2;
                        var action = _processDef.Actions.FirstOrDefault(c => c.Name == values[k + 1]);
                        peimp.Add(ActionDefinitionForActivity.Create(action, values[k]));
                    }
                }
                else if (p.Key == string.Format("{0}.DesignerId", prefix))
                {
                    designerId = p.Value;
                }
                else if (p.Key == string.Format("{0}.DesignerX", prefix))
                {
                    designerX = p.Value;
                }
                else if (p.Key == string.Format("{0}.DesignerY", prefix))
                {
                    designerY = p.Value;
                }
            }

            var item = ActivityDefinition.Create(name, state, isInitial, isFinal, isForSetState, isAutoSchemeUpdate);
            item.Implementation.AddRange(imp);
            item.PreExecutionImplementation.AddRange(peimp);

            item.DesignerSettings.Id = designerId;
            item.DesignerSettings.X = designerX;
            item.DesignerSettings.Y = designerY;

            _processDef.Activities.Add(item);
        }

        private string GetKeyToName(string name)
        {
            int startIndex = 0;//Settings.PrefixId.Length;
            int endIndex = name.IndexOf('.');

            string res;

            if (endIndex > startIndex)
                res = name.Substring(startIndex, endIndex - startIndex);
            else
                res = string.Empty;

            return res;
        }

        public string GetScheme()
        {
            return _processDef == null ? string.Empty : _processDef.Serialize();           
        }

        public void FormedErrorMessage(string sectionName, string msg, string contolId = "")
        {
            if (validator == null)
                validator = new ValidationResult();

            validator.FormedErrorMessage(sectionName, msg, contolId);
        }
        
        public MvcHtmlString GetDefaultStyle()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(@"
            <style>
    div.base {{
		overflow: hidden;
		white-space: nowrap;
		font-family: Arial;
		font-size: 8pt;
	}}

    .{0}dragRow{{
        background-color:#FFC310;
    }}

    .{0}RowCopy{{
        background-color:#FFFFCC;
    }}

    table.{0}Table {{
        border-top: 1px solid #E7E7E7;
        border-left: 1px solid #E7E7E7;
        border-bottom: 1px solid #E7E7E7;
        border-spacing: 0px;
        width: 100%;
    }}
    
    table.{0}Table td.ColumnChecked {{
        text-align: center; 
    }}
    
    table.{0}Table th{{
    padding-left: 5px;   
    padding-right: 5px;
    border-right: 1px solid #E7E7E7;
    }}
    
    table.{0}Table td{{
    padding-left: 5px;   
    padding-right: 5px;
    border-top: 1px solid #E7E7E7; 
    border-right: 1px solid #E7E7E7;         
    }}
    
    table.{0}Table td.columnTree{{
        padding-left: 20px;   
    }}
        
    table.{0}Table tr:hover td,
    table.{0}Table tr.even:hover td.active,
    table.{0}Table tr.odd:hover td.active {{
        background: #CCCCCC;
    }}
    
    .span-validation-error {{
        color: #FF0033;
        background:url('{1}'); left top; 
        width:16px; 
        height:16px;
        display: inline-block;
    }}

    .field-validation-error {{
        border-color:#FF0033;
    }}  
    
    div#{0}ResultDiv{{
	    background:#FBFBFB;
	    border:1px solid #CCC;
	    margin-bottom:10px;
	    padding:10px;
	    width:98%;
	    max-height:300px;
	    overflow: auto;
    }}
</style>", Settings.PrefixId, Settings.ImageError);


            return new MvcHtmlString(sb.ToString());
        }
    }
}
