﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace OptimaJet.Workflow.Designer
{
    internal enum HtmlTemplateColumnType
    {
        Input,
        CheckBox,
        TextArea,
        ComboBox,
        ChildTable,
        Hidden,
        ComboBoxDynamic
    }

    internal class HtmlTemplateColumnInfo
    {
        public string Caption;
        public string CaptionStyle;
        public string Name;
        public string DefaultValue;
        public HtmlTemplateColumnType Type = HtmlTemplateColumnType.Input;
        public Dictionary<string, string> TypeDic;
        public bool ComboBoxIsShowEmpty = false;
        public HtmlTemplateColumnInfo[] Childs;
        public string SubGroupName;

        public bool Hidden
        {
            get { return Type == HtmlTemplateColumnType.Hidden; }
        }

        public string ArrayName { get; set; }

        public bool TrakingChanging { get; set; }
    }

    internal class HtmlTemplate
    {
        public static string StartDiv(GeneratorSettings settings, string name)
        {
            return string.Format("<div id='{0}tab{1}'>", settings.PrefixId, name);
        }

        public static string Table(GeneratorSettings settings, string name, HtmlTemplateColumnInfo[] columns)
        {
            //buttons
            var par = new StringBuilder();
            foreach (var col in columns)
            {
                if(par.Length > 0)
                    par.Append(",");
                par.AppendFormat("'{0}'", HttpUtility.HtmlEncode(col.DefaultValue));
            }
            var buttons = new StringBuilder();

            if (!settings.ReadOnly)
            {
                buttons.AppendFormat(@"<a class='btnAdd' href=""javascript:{0}On{1}Create({3})"">{2}</a> | ", settings.PrefixId, name, settings.CreateText, par.ToString());
                buttons.AppendFormat(@"<a class='btnPaste' href=""javascript:{0}On{1}PasteFromBuffer()"">{2}</a>", settings.PrefixId, name, settings.PasteFromBufferText);
            }

            var sb = new StringBuilder();
            sb.Append(buttons);
            sb.AppendFormat(@"<table id='{0}{1}' class='{2}'><thead><tr>", settings.PrefixId, name, settings.TableStyleName);

            var groups = new List<string>();
            //.Where(c=>!c.Hidden)
            foreach (var col in columns.Where(c => !c.Hidden))
            {
                if(string.IsNullOrEmpty(col.SubGroupName))
                    sb.AppendFormat("<th style='{0}'>{1}</th>", col.CaptionStyle, col.Caption);
                else if(!groups.Contains(col.SubGroupName))
                {
                    sb.AppendFormat("<th style='{0}'>{1}</th>", col.CaptionStyle, col.SubGroupName);
                    groups.Add(col.SubGroupName);
                }
            }

            if (!settings.ReadOnly)
                sb.AppendFormat("<th></th>");

            sb.AppendFormat(@"</tr></thead></table>");
            sb.Append(buttons);
            return sb.ToString();

        }

        internal static string OnCreate(GeneratorSettings settings, string name, HtmlTemplateColumnInfo[] columnsInfo, bool isChild = false, bool createArrayColl = false)
        {
            var refColumnList = new List<HtmlTemplateColumnInfo>();
            StringBuilder sb = new StringBuilder();

            if (isChild)
            {
                sb.AppendFormat(@"var  {0}{1}Index = new Array();", settings.PrefixId, name);
            }
            else
            {
                sb.AppendFormat(@"var  {0}{1}Index = 0;", settings.PrefixId, name);
            }

            foreach (var col in columnsInfo.Where(c => c.Type == HtmlTemplateColumnType.ChildTable))
            {
                string oldPrefix = settings.PrefixId;
                settings.PrefixId = string.Format("{0}{1}_", settings.PrefixId, name);
                sb.AppendFormat("{0}", OnDelete(settings, col.Name, true));
                sb.AppendFormat("{0}", OnCreate(settings, col.Name, col.Childs, true));
                settings.PrefixId = oldPrefix;
            }

            if (createArrayColl)
            {
                sb.AppendFormat(@"var {0}{1}Array = new Array();
function {0}{1}ArrayUpdate(){{
    {0}{1}Array.splice(0, {0}{1}Array.length);
    var data = {0}Get{1}AsObject();
    for(var i = 0; i < data.length; i++){{
        {0}{1}Array.push(data[i].name);
    }}
}}
", settings.PrefixId, name);
                
            }

            sb.AppendFormat(@"
function {0}On{1}Create(", settings.PrefixId, name);

            if (isChild)
            {
                sb.AppendFormat("index");
                if (columnsInfo.Length > 0)
                    sb.Append(", ");
            }

            for (int i = 0; i < columnsInfo.Length; i++)
            {
                if (i > 0)
                    sb.AppendFormat(", ");
                sb.Append(columnsInfo[i].Name);
            }
                        
            if (isChild)
            {
                sb.AppendFormat(@") {{
    if({0}{1}Index[index] == undefined)
           {0}{1}Index[index] = 0;
    var valuePrefix = '{0}' + index + '_{1}_' + {0}{1}Index[index] + '_';
    var rowSource = ""<tr id='"" + valuePrefix + ""Row'>"";", settings.PrefixId, name);
            }
            else
            {
                sb.AppendFormat(@") {{
    var valuePrefix = '{0}{1}_' + {0}{1}Index + '_';
    var rowSource = ""<tr id='"" + valuePrefix + ""Row'>"";", settings.PrefixId, name);
                sb.AppendFormat(@"rowSource += ""<input type='hidden' id='"" + valuePrefix + ""CurrentIndex' value='"" + {0}{1}Index + ""' ></input>"";", settings.PrefixId, name);
            }            

            string groupName = string.Empty;
            foreach (var col in columnsInfo)
            {
                #region groupName
                if (!string.IsNullOrWhiteSpace(col.SubGroupName))
                {
                    if (col.SubGroupName == groupName)
                    {
                        sb.AppendFormat(@"rowSource += ""</tr><tr><td><label for='""+ valuePrefix +""{0}'>{1}</label></td>"";", col.Name, col.Caption);
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(groupName))
                        {
                            sb.AppendFormat(@"rowSource += ""</tr></table></td>"";");
                        }

                        sb.AppendFormat(@"rowSource += ""<td><table class='{2}' ><tr><td><label for='""+ valuePrefix +""{0}'>{1}</label></td>"";", col.Name, col.Caption, settings.TableStyleName);
                    }
                }
                else if (!string.IsNullOrWhiteSpace(groupName))
                {
                    sb.AppendFormat(@"rowSource += ""</tr></table></td>"";");
                }
                #endregion

                #region Column
                if (col.Type == HtmlTemplateColumnType.Input)
                {
                    sb.AppendFormat(@"
if({0} == undefined)  {0} = '';
rowSource += ""<td><input style='width:100%' id='"" + valuePrefix + ""{0}' value='"" + {0} + ""' ></input></td>"";", col.Name);
                }
                else if (col.Type == HtmlTemplateColumnType.CheckBox)
                {
                    sb.AppendFormat(@"
rowSource += ""<td class='ColumnChecked'><input type='checkbox'  id='"" + valuePrefix + ""{0}'""", col.Name);
                    sb.AppendFormat(@"
if({0} == '1') rowSource += ""checked='checked'"";
rowSource += ""></input></td>"";", col.Name);
                }
                else if (col.Type == HtmlTemplateColumnType.TextArea)
                {
                    sb.AppendFormat(@"
if({0} == undefined)  {0} = '';
rowSource += ""<td><textarea style='width:100%' id='"" + valuePrefix + ""{0}' value='"" + {0} + ""' ></textarea></td>"";", col.Name);
                }
                else if (col.Type == HtmlTemplateColumnType.ComboBox)
                {
                    if (col.TypeDic != null)
                    {
                        sb.AppendFormat(@"
rowSource += ""<td><select id='"" + valuePrefix + ""{0}' >"";", col.Name);

                        if (col.ComboBoxIsShowEmpty)
                        {
                            sb.AppendFormat(@"
rowSource += ""<option value=''></option>"";");
                        }

                        foreach (var item in col.TypeDic)
                        {
                            sb.AppendFormat(@"
rowSource += ""<option value='{0}' "";
if({1} == '{0}') rowSource += 'selected';
rowSource += "">{2}</option>"";", item.Key, col.Name, item.Value);
                        }
                        sb.AppendFormat(@"
rowSource += ""</select></td>"";");
                    }
                    else
                    {
                        sb.AppendFormat(@"rowSource += ""<td><input style='width:100%' id='"" + valuePrefix + ""{0}' value='"" + {0} + ""' ></input></td>"";", col.Name);

                        refColumnList.Add(col);
                    }
                }
                else if (col.Type == HtmlTemplateColumnType.ComboBoxDynamic)
                {
                    sb.AppendFormat(@"rowSource += ""<td><input id='"" + valuePrefix + ""{0}'  class='{1}' style='width:80%' value='"" + {0} + ""' ></input></td>"";", col.Name, col.ArrayName);                   
                }
                else if (col.Type == HtmlTemplateColumnType.ChildTable)
                {
                    sb.AppendFormat(@"rowSource += ""<td>{0}</td>"";", ChildTable(settings, name, col.Name, col.Childs));
                }
                else if (col.Type == HtmlTemplateColumnType.Hidden)
                {
                    sb.AppendFormat(@"
if({0} == undefined)  {0} = '';
rowSource += ""<input style='width:100%' type='hidden' id='"" + valuePrefix + ""{0}' value='"" + {0} + ""' ></input>"";", col.Name);
                }
                #endregion

                groupName = col.SubGroupName;
            }

            #region groupName final
            if (!string.IsNullOrWhiteSpace(groupName))
            {
                sb.AppendFormat(@"rowSource += ""</tr></table></td>"";");
                groupName = string.Empty;
            }
            #endregion

            if (!settings.ReadOnly)
            {
                if (isChild)
                {
                    sb.AppendFormat(@"
rowSource += ""<td><a href='javascript:{0}On{1}Delete("" + index + "",""+ {0}{1}Index[index] +"")'>{2}</a></td>"";",
                        settings.PrefixId, name, settings.DeleteText);
                }
                else
                {
                    sb.AppendFormat(@"
rowSource += ""<td class='Commands'><a href='javascript:{0}On{1}CopyInBuffer(""+ {0}{1}Index +"")'>{2}</a> | <a href='javascript:{0}On{1}Delete(""+ {0}{1}Index +"")'>{3}</a></td>"";",
                        settings.PrefixId, name, settings.CopyInBufferText, settings.DeleteText);
                }
            }

            if (isChild)
            {
                sb.AppendFormat(@"rowSource += ""</tr>"";
$('#{0}{1}_' + index).append(rowSource);", settings.PrefixId, name);
            }
            else
            {
                sb.AppendFormat(@"rowSource += ""</tr>"";
$('#{0}{1}').append(rowSource);
$('#{0}{1}').tableDnD({{
    onDragClass: '{0}dragRow'  
}});
", settings.PrefixId, name);
            }

            foreach (var col in columnsInfo.Where(c => c.Type == HtmlTemplateColumnType.ChildTable))
            {
                sb.AppendFormat(@" 
if({2} != undefined && {2}.length > 0) for(var i=0;i < {2}.length; i++) {0}{1}_On{2}Create({0}{1}Index", settings.PrefixId, name, col.Name);

                if (col.Childs.Length == 1)
                {
                    sb.AppendFormat(", {0}[i]", col.Name);
                }
                else
                {
                    for (int i = 0; i < col.Childs.Length; i++)
                        sb.AppendFormat(", {0}[i][{1}]", col.Name, i);
                }
                sb.AppendFormat(");");
            }

            if (settings.ReadOnly)
            {
                sb.AppendFormat(@"$('#' + valuePrefix + 'Row input').attr('readonly', 'readonly');
$('#' + valuePrefix + 'Row select').attr('disabled', 'disabled');
$('#' + valuePrefix + 'Row input').attr('disabled', 'disabled');");
            }

            if (isChild)
            {
                sb.AppendFormat(@"
{0}{1}Index[index]++;", settings.PrefixId, name);
            }
            else
            {
                sb.AppendFormat(@"
{0}{1}Index++;", settings.PrefixId, name);
            }


            if (createArrayColl && columnsInfo.Length > 0)
            {
                sb.AppendFormat(@"{0}{1}Array.push({2});", settings.PrefixId, name, columnsInfo[0].Name);
            }

            foreach (var col in columnsInfo)
            {
                if (col.Type == HtmlTemplateColumnType.ComboBoxDynamic)
                {
                    sb.AppendFormat(@"$('#' + valuePrefix + '{0}').momboBox({{data:{1}{2}Array}});", col.Name, settings.PrefixId, col.ArrayName);
                }

                if (col.TrakingChanging)
                {
                    sb.AppendFormat(@"var tmp = $('#' + valuePrefix + '{0}');
tmp.data('value', tmp.val());
tmp.change(function(){{
var prevValue = $(this).data('value');

$('input[value$=' + prevValue + '].{2}').val( $(this).val());
$(this).data('value', $(this).val());

{1}{2}ArrayUpdate();
}});", col.Name, settings.PrefixId, name);
                }
            }            
            
            sb.AppendFormat(@"}} ");
            
            return sb.ToString();
        }

        private static string ChildTable(GeneratorSettings settings, string parentName, string name, HtmlTemplateColumnInfo[] columns)
        {
        
            StringBuilder buttons = new StringBuilder();

            if (!settings.ReadOnly)
            {
                buttons.AppendFormat(@"<a class='btnAdd' href='javascript:{0}{1}_On{2}Create("" + {0}{1}Index + "")' >{3}</a>",
                    settings.PrefixId, parentName, name, settings.CreateText);
            }

            StringBuilder sb = new StringBuilder();

            sb.AppendFormat(@"<table id='{0}{1}_{2}_"" + {0}{1}Index + ""' class='{3}'><thead><tr>", settings.PrefixId, parentName, name, settings.TableStyleName);

            foreach (var col in columns)
                sb.AppendFormat("<th style='{0}'>{1}</th>", col.CaptionStyle, col.Caption);
            sb.AppendFormat("<th></th></tr></thead>");

            sb.AppendFormat("");
            sb.AppendFormat("</table>");

            

            sb.Append(buttons);
            return sb.ToString();
        }

        internal static string OnCopyPaste(GeneratorSettings settings, string name, HtmlTemplateColumnInfo[] columnsInfo)
        {
            var sb = new StringBuilder();
            sb.AppendFormat(@"
var {0}{1}Buffer;
function {0}On{1}CopyInBuffer(index){{
        $('#{0}{1} tr').removeClass('{2}');    
        {0}{1}Buffer = {0}Get{1}Row(index);
        $('#{0}{1}_' + index + '_Row').addClass('{2}'); 
}}", settings.PrefixId, name, settings.RowCopyStyleName);

            sb.AppendFormat(@"function {0}On{1}PasteFromBuffer(){{ 
    if({0}{1}Buffer != undefined){{
           {0}On{1}Create(",settings.PrefixId, name);

            int index = 0;
            for (int i = 0; i < columnsInfo.Length; i++)
            {
                if (i > 0)
                    sb.Append(",");
               
                sb.AppendFormat("{0}{1}Buffer[{2}].value", settings.PrefixId, name, index);
                index++;
            }
            
            sb.AppendFormat(@");}}}}", settings.PrefixId, name);

            return sb.ToString();
        }

        internal static string OnDelete(GeneratorSettings settings, string name, bool isChild = false, bool createArrayColl = false)
        {
            var sb = new StringBuilder();
            if (isChild)
            {
                sb.AppendFormat(@"function {0}On{1}Delete(index, indexChild){{
    var rowName = '#{0}' + index + '_{1}_' +  indexChild + '_Row';", settings.PrefixId, name);

            }
            else
            {
                sb.AppendFormat(@"function {0}On{1}Delete(index){{
    var rowName = '#{0}{1}_' + index + '_Row';", settings.PrefixId, name);
            }

            sb.AppendFormat(@"
if(confirm('{0}')){{
$(rowName).remove(); ", settings.ConfirmDeleteText);

           if(createArrayColl)
           {
               sb.AppendFormat(@"{0}{1}ArrayUpdate();", settings.PrefixId, name);
           }

            sb.AppendFormat("}}}}");
            return sb.ToString();
        }

        internal static string GetParameter(GeneratorSettings settings, string name, HtmlTemplateColumnInfo[] columnsInfo)
        {
            var sb = new StringBuilder();
            sb.AppendFormat(@"
function {0}Get{1}RowByRowNumber(rowNumber) {{
    var item = $('#{0}{1}')[0].childNodes[1].childNodes;
    if(item.length <= rowNumber) return;

    var i = item[rowNumber].id.replace('{0}{1}_', '').replace('_Row', '');
    return {0}Get{1}Row(i);
}}", settings.PrefixId, name);

            sb.AppendFormat(@"
function {0}Get{1}Row(i) {{
    var res = new Array();
    var valuePrefix = '{0}{1}_' + i + '_';   
    var parPrefix = '{0}{1}' + '[' + i + '].';
    ", settings.PrefixId, name);
            for (int i = 0; i < columnsInfo.Length; i++)
            {
                if (columnsInfo[i].Type == HtmlTemplateColumnType.CheckBox)
                {
                    #region CheckBox
                    if (i == 0)
                    {
                        sb.AppendFormat(@"
var {0} = $('#' + valuePrefix + '{0}');
if({0}.length == 0) return undefined; 
res.push({{name: parPrefix + '{0}',         value:  {0}[0].checked }}); ", columnsInfo[i].Name);
                    }
                    else
                    {
                        sb.AppendFormat(@"
var {0} = $('#' + valuePrefix + '{0}');
res.push({{name: parPrefix + '{0}', value:  {0}[0].checked }});", columnsInfo[i].Name);
                    }
                    #endregion
                }
                else if (columnsInfo[i].Type == HtmlTemplateColumnType.ChildTable)
                {
                    #region ChildTable
                    sb.AppendFormat(@"
var {2}Array = new Array();
for (var j = 0; j < {0}{1}_{2}Index[i]; j++) {{
    var {2}ArrayChild = new Array();
    var valueSubPrefix = valuePrefix + '{2}_' + j + '_';
    var parSubPrefix = parPrefix + '{2}[' + j + '].';
", settings.PrefixId, name, columnsInfo[i].Name);

                    for(int j=0; j<columnsInfo[i].Childs.Length;j++)
                    {
                        var col = columnsInfo[i].Childs[j];
                        sb.AppendFormat(@"    
var sChild{0} = $('#' + valueSubPrefix + '{0}');", col.Name);

                        if (j == 0)
                        {
                            sb.AppendFormat(@"                      
if(sChild{0}.length == 0) continue;", col.Name);
                        }

                        sb.AppendFormat(@"
{0}ArrayChild.push( sChild{1}[0].value );", columnsInfo[i].Name, col.Name);
                    }

                    sb.AppendFormat(@"
{0}Array.push({0}ArrayChild);
}}
res.push({{name: parPrefix + '{0}',       value: {0}Array  }});
", columnsInfo[i].Name);
                    #endregion
                }
                else
                {
                    #region input
                    if (i == 0)
                    {
                        sb.AppendFormat(@"
var {0} = $('#' + valuePrefix + '{0}');
if({0}.length == 0) return undefined; 
res.push({{name: parPrefix + '{0}',         value:  {0}[0].value }}); ", columnsInfo[i].Name);
                    }
                    else
                    {
                        sb.AppendFormat(@"
var {0} = $('#' + valuePrefix + '{0}');
res.push({{name: parPrefix + '{0}', value:  {0}[0].value }});", columnsInfo[i].Name);
                    }
                    #endregion
                }
            }   
            sb.AppendFormat(@"
return res;
}}

function {0}Get{1}() {{
    var data = new Array();
    for (var i = 0; i < {0}{1}Index; i++) {{
            var tmp = {0}Get{1}RowByRowNumber(i);
            if (tmp == undefined || tmp.length == 0) continue;            

            data = data.concat(tmp);
        }}
    return data;
}}

function {0}Get{1}AsObject() {{
    var data = new Array();
    for (var i = 0; i < {0}{1}Index; i++) {{
            var tmp = {0}Get{1}RowByRowNumber(i);
            if (tmp == undefined || tmp.length == 0) continue;
            var item = new Object();
            for (var j = 0; j < tmp.length; j++) {{
                var regex = /{0}{1}\[\d{{1,}}\]./i;
                var property = tmp[j].name.replace(regex,'').toLowerCase();
                item[property] = tmp[j].value;
            }}
            data.push(item);
        }}
    return data;
}}
", settings.PrefixId, name);
            return sb.ToString();
        }

        internal static string CreateRowScript(GeneratorSettings settings, string name, object[] values)
        {
            var sb = new StringBuilder();
            sb.AppendFormat(@"
{0}On{1}Create(", settings.PrefixId, name);

            sb.Append(GetFuncParameters(values));            
            
            sb.AppendFormat(");");
            return sb.ToString();            
        }

        private static string GetFuncParameters(object[] values)
        {
            var sb = new StringBuilder();
            for (int i = 0; i < values.Length; i++)
            {
                if (i > 0)
                    sb.Append(",");

                if (values[i] is object[])
                {
                    sb.AppendFormat("[");
                    sb.Append(GetFuncParameters((object[])values[i]));
                    sb.AppendFormat("]");
                }
                else
                {
                    sb.AppendFormat("'{0}'", HttpUtility.HtmlEncode(values[i]));
                }
            }

            return sb.ToString();
        }
    }
}
