﻿using OptimaJet.Workflow.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimaJet.Workflow.Designer
{
    public interface IGraph
    {
        string GenerateGraph(ProcessDefinition processDef, GeneratorSettings settings);
    }
}
