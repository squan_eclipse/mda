﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimaJet.Workflow.Designer
{
    public class GeneratorSettings
    {
        public string TransitionClassifierDirectStyle = "strokeColor=green";
        public string TransitionClassifierReverseStyle = "strokeColor=red";
        public string[] Сolor = new string[] { "#83027F", "#66B922", "#808913", "#CF0056", "#4679B6" };
        public int MarginX = 20;

        public string PrefixId = "WFDesigner_";
        public string TableStyleName = "WFDesigner_Table";
        public string RowCopyStyleName = "WFDesigner_RowCopy";  

        public string JQueryLoadMaskCssPath;
        public string JQueryLoadMaskSrcPath;

        public bool ReadOnly = false;
        public bool GraphVisible = true;
        public int GraphWidth = 1000;
        public int GraphHeight = 1000;

        public string LoadingImageUrl = "images/loading.gif";
        public string LocalizationCultureDefault = "en-US";

        public string GetControlId(string name)
        {
            return PrefixId + name;
        }
        public string ImageError = "images/error.png";

        public string ProcessNameText = "Process name";
        public string ResultText = "Results";
        public string ActiveProcessSavingIsNotAvailable = "Active process saving is not available!";
        public string GraphText = "Graph";
        public string LocalizationText = "Localization";
        public string ParameterText = "Parameter";
        public string ActionText = "Action";
        public string CommandText = "Command";
        public string ActorText = "Actor";
        public string TransitionText = "Transition";
        
        public string AutogenerateGraphText = "Autogenerate graph";
        public string CreateText = "Create";
        public string DeleteText = "Delete";
        public string PasteFromBufferText = "Paste from buffer";
        public string CopyInBufferText = "Copy in buffer";        

        public string ConfirmDeleteText = "Are you sure you want to delete?";
        public string LocalizationObjectNameText = "ObjectName";
        public string LocalizationTypeText = "Type";
        public string LocalizationTypeCommandText = "Command";
        public string LocalizationTypeParameterText = "Parameter";
        public string LocalizationTypeStateText = "State";
        public string LocalizationIsDefaultText = "IsDefault";
        public string LocalizationCultureText = "Culture";
        public string LocalizationValueText = "Value";
        public string ParameterNameText = "Name";
        public string ParameterTypeText = "Type";
        public string ParameterPurposeText = "Purpose";
        public string ParameterValueText = "Value";
        public string ParameterSerializedDefaultValueText = "SerializedDefaultValue";
        public string ParameterPurposePersistenceText = "Persistence";
        public string ParameterPurposeTemporaryText = "Temporary";
        public string ParameterPurposeSystemText = "System";
        public string ActionNameText = "Name";
        public string ActionTypeText = "Type";
        public string ActionMethodNameText = "Method";
        public string ActionInputParametersText = "Input";
        public string ActionOutputParametersText = "Output";
        public string ActionParametrNameText = "Name";
        public string ActionParametrOrderText = "Order";
        public string ActionParametersGroupText = "Parameters";
        public string CommandNameText = "Name";
        public string CommandParameterText = "Parameters";
        public string CommandParameterNameText = "Name";
        public string ActorNameText = "Name";
        public string ActorTypeText = "Type";
        public string ActorValueText = "Value";

        public string ActivityText = "Activity";
        public string ActivityImpOrderText = "Order";
        public string ActivityImpActionText = "Action";
        public string ActorTypeIdentityText = "Identity";
        public string ActorTypeInRoleText = "InRole";
        public string ActorTypeExecuteRuleText = "ExecuteRule";
        public string ActivityNameText = "Name";
        public string ActivityStateText = "State";
        public string ActivityCheckParamsText = "Check params";
        public string ActivityIsAutoSchemeUpdateText = "IsAutoScheme";
        public string ActivityIsForSetStateText = "IsForSetState";
        public string ActivityIsFinalText = "IsFinal";
        public string ActivityIsInitialText = "IsInitial";
        public string ActivityImplementationGroupText = "Implementation group";
        public string ActivityImplementationText = "Implementation";
        public string ActivityPreExecutionImplementationText = "Pre Execution Implementation";
        public string TransitionNameText = "Name";
        public string TransitionFromToText = "From To";
        public string TransitionRestrictionsText = "Restrictions";
        public string TransitionTriggersConditionGroupText = "Triggers/Conditions";
        public string TransitionFromText = "From";
        public string TransitionToText = "To";
        public string TransitionRestrictionsTypeText = "Type";
        public string TransitionRestrictionsActorText = "Actor";
        public string RestrictionTypeAllowText = "Allow";
        public string RestrictionTypeRestrictText = "Restrict";
        public string TransitionTriggerTypeText = "Type";
        public string TransitionTriggerCommandText = "Command";
        public string TransitionClassifierText = "Classifier";
        public string TransitionClassifierDirectText = "Direct";
        public string TransitionClassifierReverseText = "Reverse";
        public string TransitionClassifierNotSpecifiedText = "NotSpecified";
        public string TransitionConditionTypeOtherwiseText = "Otherwise";
        public string TransitionConditionTypeActionText = "Action";
        public string TransitionConditionTypeAlwaysText = "Always";
        public string TransitionConditionTypeText = "Type";
        public string TransitionConditionActionText = "Action";
        public string TransitionResultOnPreExecutionText = "Result on PreExecution";
        public string TriggerTypeCommandText = "Command";
        public string TriggerTypeAutoText = "Auto";
        public string TriggerTypeTimerText = "Timer";
        public string ActionMainParametersText = "Main parameters";
        public string ValidationText = "Validation...";
        public string SaveText = "Saving...";
        public string UrlValidateAndSync = "/Designer/ValidateAndSync";
        public string UrlValidateAndSave = "/Designer/ValidateAndSave";
        public string UrlAutogenerateGraph = "/Designer/AutogenerateGraph";
        public string AutogenerateGraphProcessText = "Generating...";

        #region Erros
        public string ErrorFieldIsRequiredText = "Field {0} is required";
        public string ErrorFieldIsRequired(string columnName)
        {
            return string.Format(ErrorFieldIsRequiredText, columnName);
        }

        public string ErrorActivityIsInitialCountText = "One element must be marked flag IsInitial";

        public string ErrorAddObjectTypeToProcessText = "Add {0} to process";
        public string ErrorAddObjectTypeToProcess(string name)
        {
            return string.Format(ErrorAddObjectTypeToProcessText, name);
        }

        public string ErrorFieldMustBeUniqueText = "Field {0} must be unique";
        public string ErrorFieldMustBeUnique(string columnName)
        {
            return string.Format(ErrorFieldMustBeUniqueText, columnName);
        }

        public string ErrorFieldNotEqualsText = "Field {0} must not equals {1}";
               
        internal string ErrorFieldNotEquals(string field, string value)
        {
            return string.Format(ErrorFieldNotEqualsText, field, value);
        }
        #endregion                 
    }
}
