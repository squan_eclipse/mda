﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using OptimaJet.Common;
using OptimaJet.Workflow.Core.Fault;
using OptimaJet.Workflow.Core.Model;
using OptimaJet.Workflow.Core.Persistence;

namespace OptimaJet.Workflow.DbPersistence
{
    public sealed class DbPersistenceProvider : DbProvider, IPersistenceProvider
    {
        public DbPersistenceProvider(string connectionString) : base(connectionString)
        {
        }

        public void InitializeProcess(ProcessInstance processInstance)
        {
            using (var scope = PredefinedTransactionScopes.ReadCommittedSupressedScope)
            {
                using (var context = CreateContext())
                {
                    var oldProcess = context.WorkflowProcessInstances.SingleOrDefault(wpi => wpi.Id == processInstance.ProcessId);
                    if (oldProcess != null)
                        throw new ProcessAlredyExistsException();
                    var newProcess = new WorkflowProcessInstance
                                         {
                                             Id = processInstance.ProcessId,
                                             SchemeId = processInstance.SchemeId,
                                             ActivityName = processInstance.ProcessScheme.InitialActivity.Name,
                                             StateName = processInstance.ProcessScheme.InitialActivity.State
                                         };
                    context.WorkflowProcessInstances.InsertOnSubmit(newProcess);
                    context.SubmitChanges();
                }
                scope.Complete();
            }
        }


        public void BindProcessToNewScheme(ProcessInstance processInstance)
        {
            BindProcessToNewScheme(processInstance,false);
        }

        public void BindProcessToNewScheme(ProcessInstance processInstance, bool resetIsDeterminingParametersChanged)
        {
            using (var scope = PredefinedTransactionScopes.ReadCommittedSupressedScope)
            {

                using (var context = CreateContext())
                {
                    var oldProcess =
                        context.WorkflowProcessInstances.SingleOrDefault(wpi => wpi.Id == processInstance.ProcessId);
                   if (oldProcess == null)
                        throw new ProcessNotFoundException();
                    oldProcess.SchemeId = processInstance.SchemeId;
                    if (resetIsDeterminingParametersChanged)
                        oldProcess.IsDeterminingParametersChanged = false;
                    context.SubmitChanges();
                }
                scope.Complete();
            }
        }

        public void FillProcessParameters(ProcessInstance processInstance)
        {
            processInstance.AddParameters(GetProcessParameters(processInstance.ProcessId, processInstance.ProcessScheme));
        }

        public void FillPersistedProcessParameters(ProcessInstance processInstance)
        {
            processInstance.AddParameters(GetPersistedProcessParameters(processInstance.ProcessId, processInstance.ProcessScheme));
        }

        public void FillSystemProcessParameters(ProcessInstance processInstance)
        {
            processInstance.AddParameters(GetSystemProcessParameters(processInstance.ProcessId, processInstance.ProcessScheme));
        }

        public void SavePersistenceParameters(ProcessInstance processInstance)
        {
            var parametersToPersistList =
              processInstance.ProcessParameters.Where(ptp => ptp.Purpose == ParameterPurpose.Persistence).Select(ptp => new { Parameter = ptp, SerializedValue = SerializeParameter(ptp.Value) })
                  .ToList();
            var persistenceParameters = processInstance.ProcessScheme.PersistenceParameters.ToList();
            
            using (var scope = PredefinedTransactionScopes.ReadUncommittedSupressedScope)
            {
                using (var context = CreateContext())
                {
                   var persistedParameters =
                      context.WorkflowProcessInstancePersistences.Where(
                          wpip =>
                          wpip.ProcessId == processInstance.ProcessId &&
                          persistenceParameters.Select(pp => pp.Name).Contains(wpip.ParameterName)).ToList();

                    foreach (var parameterDefinitionWithValue in parametersToPersistList)
                    {
                        WorkflowProcessInstancePersistence persistence =
                            persistedParameters.SingleOrDefault(
                                pp => pp.ParameterName == parameterDefinitionWithValue.Parameter.Name);
                        {
                            if (persistence == null)
                            {
                                if (parameterDefinitionWithValue.SerializedValue != null)
                                {
                                    persistence = new WorkflowProcessInstancePersistence()
                                                      {
                                                          Id = Guid.NewGuid(),
                                                          ParameterName = parameterDefinitionWithValue.Parameter.Name,
                                                          ProcessId = processInstance.ProcessId,
                                                          Value = parameterDefinitionWithValue.SerializedValue
                                                      };
                                    context.WorkflowProcessInstancePersistences.InsertOnSubmit(persistence);
                                }
                            }
                            else 
                            {
                                if (parameterDefinitionWithValue.SerializedValue != null)
                                 persistence.Value = parameterDefinitionWithValue.SerializedValue;
                                else
                                    context.WorkflowProcessInstancePersistences.DeleteOnSubmit(persistence);
                            }
                        }
                    }

                    context.SubmitChanges();
                }

                scope.Complete();
            }
        }

        public void SetWorkflowIniialized(ProcessInstance processInstance)
        {
            using (var scope = PredefinedTransactionScopes.SerializableSupressedScope)
            {
                using (var context = CreateContext())
                {
                    var instanceStatus = context.WorkflowProcessInstanceStatus.SingleOrDefault(wpis => wpis.Id == processInstance.ProcessId);
                    if (instanceStatus == null)
                    {
                        instanceStatus = new WorkflowProcessInstanceStatus()
                                        {
                                            Id = processInstance.ProcessId,
                                            Lock = Guid.NewGuid(),
                                            Status = ProcessStatus.Initialized.Id
                                        };

                        context.WorkflowProcessInstanceStatus.InsertOnSubmit(instanceStatus);

                    }
                    else
                    {
                        instanceStatus.Status = ProcessStatus.Initialized.Id;
                    }

                    context.SubmitChanges();
                }

                scope.Complete();
            }
        }

        public void SetWorkflowIdled(ProcessInstance processInstance)
        {
            SetCustomStatus(processInstance.ProcessId,ProcessStatus.Idled);
        }

        public void SetWorkflowRunning(ProcessInstance processInstance)
        {
            using (var scope = PredefinedTransactionScopes.SerializableSupressedScope)
            {
                using (var context = CreateContext())
                {
                    var instanceStatus = context.WorkflowProcessInstanceStatus.SingleOrDefault(wpis => wpis.Id == processInstance.ProcessId);
                    if (instanceStatus == null)
                        throw new StatusNotDefinedException();
                    instanceStatus.Lock = Guid.NewGuid();
                    instanceStatus = context.WorkflowProcessInstanceStatus.SingleOrDefault(wpis => wpis.Id == processInstance.ProcessId);
                    if (instanceStatus == null)
                        throw new StatusNotDefinedException();

                    if (instanceStatus.Status == ProcessStatus.Running.Id)
                        throw new ImpossibleToSetStatusException();
                    instanceStatus.Status = ProcessStatus.Running.Id;

                    context.SubmitChanges();
                }

                scope.Complete();
            }
        }

        public void SetWorkflowFinalized(ProcessInstance processInstance)
        {
            SetCustomStatus(processInstance.ProcessId, ProcessStatus.Finalized);
        }

        public void SetWorkflowTerminated(ProcessInstance processInstance, ErrorLevel level, string errorMessage)
        {
            SetCustomStatus(processInstance.ProcessId, ProcessStatus.Terminated);
        }

        public void ResetWorkflowRunning()
        {
            using (var context = CreateContext())
            {
                context.spWorkflowProcessResetRunningStatus();
            }
        }

        public void UpdatePersistenceState(ProcessInstance processInstance, TransitionDefinition transition)
        {
                using (var context = CreateContext())
                {
                    var history = new WorkflowProcessTransitionHistory()
                                                                   {
                                                                       ActorIdentityId = Guid.NewGuid(),
                                                                       //TODO ACTOR
                                                                       ExecutorIdentityId = Guid.NewGuid(),
                                                                       Id = Guid.NewGuid(),
                                                                       IsFinalised = false,
                                                                       //TODO Зачем на м финализед тут????
                                                                       ProcessId = processInstance.ProcessId,
                                                                       FromActivityName = transition.From.Name,
                                                                       FromStateName = transition.From.State,
                                                                       ToActivityName = transition.To.Name,
                                                                       ToStateName = transition.To.State,
                                                                       TransitionClassifier =
                                                                           transition.Classifier.ToString(),
                                                                       TransitionTime = DateTime.Now
                                                                       //TODO возможно время нужно тоже передавать другим способом
                                                                   };
                    context.WorkflowProcessTransitionHistories.InsertOnSubmit(history);
                    context.SubmitChanges();
                }

        }

        public bool IsProcessExists(Guid processId)
        {
            using (var context = CreateContext())
            {
                return context.WorkflowProcessInstances.Count(wpi => wpi.Id == processId) > 0;
            }
        }

        public ProcessStatus GetInstanceStatus(Guid processId)
        {
            using (var context = CreateContext())
            {
                var instance = context.WorkflowProcessInstanceStatus.FirstOrDefault(wpis => wpis.Id == processId);
                if (instance == null)
                    return ProcessStatus.NotFound;
                var status = ProcessStatus.All.SingleOrDefault(ins => ins.Id == instance.Status);
                if (status == null)
                    return ProcessStatus.Unknown;
                return status;
            }
        }


        private void SetCustomStatus (Guid processId, ProcessStatus status)
        {
            using (var scope = PredefinedTransactionScopes.SerializableSupressedScope)
            {
                using (var context = CreateContext())
                {
                    var instanceStatus = context.WorkflowProcessInstanceStatus.SingleOrDefault(wpis => wpis.Id == processId);
                    if (instanceStatus == null)
                        throw new StatusNotDefinedException();
                    instanceStatus.Status = status.Id;

                    context.SubmitChanges();
                }

                scope.Complete();
            }
        }

        private IEnumerable<ParameterDefinitionWithValue> GetProcessParameters(Guid processId, ProcessDefinition processDefinition)
        {
            var parameters = new List<ParameterDefinitionWithValue>(processDefinition.Parameters.Count());
            parameters.AddRange(GetPersistedProcessParameters(processId,processDefinition));
            parameters.AddRange(GetSystemProcessParameters(processId, processDefinition));
            return parameters;
        }

        private IEnumerable<ParameterDefinitionWithValue> GetSystemProcessParameters(Guid processId,
            ProcessDefinition processDefinition)
        {
            var processInstance = GetProcessInstance(processId);

            var systemParameters =
                processDefinition.Parameters.Where(p => p.Purpose == ParameterPurpose.System).ToList();

            List<ParameterDefinitionWithValue> parameters;
            parameters = new List<ParameterDefinitionWithValue>(systemParameters.Count())
            {
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterProcessId.Name),
                    processId),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterPreviousState.Name),
                    (object) processInstance.PreviousState),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterCurrentState.Name),
                    (object) processInstance.StateName),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterPreviousStateForDirect.Name),
                    (object) processInstance.PreviousStateForDirect),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterPreviousStateForReverse.Name),
                    (object) processInstance.PreviousStateForReverse),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterPreviousActivity.Name),
                    (object) processInstance.PreviousActivity),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterCurrentActivity.Name),
                    (object) processInstance.ActivityName),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterPreviousActivityForDirect.Name),
                    (object) processInstance.PreviousActivityForDirect),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterPreviousActivityForReverse.Name),
                    (object) processInstance.PreviousActivityForReverse),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterProcessName.Name),
                    (object) processDefinition.Name),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterSchemeId.Name),
                    (object) processInstance.SchemeId),
                ParameterDefinition.Create(
                    systemParameters.Single(sp => sp.Name == DefaultDefinitions.ParameterIsPreExecution.Name),
                    false)
            };
            return parameters;
        }

        private IEnumerable<ParameterDefinitionWithValue> GetPersistedProcessParameters (Guid processId, ProcessDefinition processDefinition)
        {
            var persistenceParameters = processDefinition.PersistenceParameters.ToList();
            var parameters = new List<ParameterDefinitionWithValue>(persistenceParameters.Count());
            
            List<WorkflowProcessInstancePersistence> persistedParameters;
            using (PredefinedTransactionScopes.ReadUncommittedSupressedScope)
            {
                using (var context = CreateContext())
                {
                    persistedParameters =
                        context.WorkflowProcessInstancePersistences.Where(
                            wpip =>
                            wpip.ProcessId == processId &&
                            persistenceParameters.Select(pp => pp.Name).Contains(wpip.ParameterName)).ToList();
                }
            }

            foreach (var persistedParameter in persistedParameters)
            {
                var parameterDefinition = persistenceParameters.Single(p => p.Name == persistedParameter.ParameterName);
                parameters.Add(ParameterDefinition.Create(parameterDefinition, DeserializeParameter(persistedParameter.Value, parameterDefinition.Type)));
            }

            return parameters;
        }

        private object DeserializeParameter (string serializedValue, Type parameterType)
        {
            var serializer = new XmlSerializer(parameterType);
       
            using (var reader = new StringReader(serializedValue))
            {
                return serializer.Deserialize(reader);
            }
        }

        public static string SerializeParameter(object value)
        {
            if (value == null)
                return null;

            var serializer = new XmlSerializer(value.GetType());

            using (var writer = new StringWriter())
            {
                serializer.Serialize(writer,value);
                return writer.ToString();
            }
        }

        private WorkflowProcessInstance GetProcessInstance (Guid processId)
        {
            using (PredefinedTransactionScopes.ReadCommittedSupressedScope)
            {
                using (var context = CreateContext())
                {
                    var processInstance = context.WorkflowProcessInstances.SingleOrDefault(wpi => wpi.Id == processId);
                    if (processInstance == null)
                        throw new ProcessNotFoundException();
                    return processInstance;
                }
            }
        }
    }
}
