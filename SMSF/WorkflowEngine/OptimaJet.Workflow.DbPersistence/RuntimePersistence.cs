﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using OptimaJet.Common;
using OptimaJet.Workflow.Core.Persistence;
using OptimaJet.Workflow.Core.Runtime;

namespace OptimaJet.Workflow.DbPersistence
{
    public class RuntimePersistence : DbProvider, IRuntimePersistence
    {
        public RuntimePersistence(string connectionStringName) : base(connectionStringName)
        {
        }

        /// <summary>
        /// Provides saving of timers in runtime with runtimeId
        /// </summary>
        /// <param name="runtimeId">Id of runtime</param>
        /// <param name="timer">Runtime timer to save</param>
        public void SaveTimer(Guid runtimeId,RuntimeTimer timer)
        {
            string serializedTimer;
            var serializer = new DataContractSerializer(typeof(Dictionary<TimerKey, DateTime>));

            using (var stringWriter = new StringWriter())
            {
                using (var writer = XmlWriter.Create(stringWriter))
                {
                    serializer.WriteObject(writer, timer.Timers);
                    writer.Flush();
                    serializedTimer = stringWriter.ToString();
                }
            }

            using (var scope = PredefinedTransactionScopes.ReadUncommittedSupressedScope)
            {
                using (var context = CreateContext())
                {
                    var runtimeinfo = context.WorkflowRuntimes.FirstOrDefault(wr => wr.RuntimeId == runtimeId);
                    if (runtimeinfo == null)
                    {
                        runtimeinfo = new WorkflowRuntime { RuntimeId = runtimeId, Timer = serializedTimer };
                        context.WorkflowRuntimes.InsertOnSubmit(runtimeinfo);
                    }
                    else
                    {
                        runtimeinfo.Timer = serializedTimer;
                    }

                    context.SubmitChanges();
                }

                scope.Complete();
            }
        }

        /// <summary>
        /// Provides loading of timers from persistence store
        /// </summary>
        /// <param name="runtimeId">Id of runtime</param>
        /// <returns>Timer that was saved in persistence store or new empty timer</returns>
        public RuntimeTimer LoadTimer(Guid runtimeId)
        {
            string serializedTimer;

            using (var scope = PredefinedTransactionScopes.ReadUncommittedSupressedScope)
            {
                using (var context = CreateContext())
                {
                    var runtimeinfo = context.WorkflowRuntimes.FirstOrDefault(wr => wr.RuntimeId == runtimeId);
                    if (runtimeinfo == null)
                    {
                        runtimeinfo = new WorkflowRuntime {RuntimeId = runtimeId, Timer = string.Empty};
                        context.WorkflowRuntimes.InsertOnSubmit(runtimeinfo);
                    }
                    serializedTimer = runtimeinfo.Timer;
                    context.SubmitChanges();
                }

                scope.Complete();
            }

            if (string.IsNullOrEmpty(serializedTimer))
                return new RuntimeTimer();

            var serializer = new DataContractSerializer(typeof(Dictionary<TimerKey, DateTime>));

            using (var stringReader = new StringReader(serializedTimer))
            {
                using (var reader = XmlReader.Create(stringReader))
                {
                    return new RuntimeTimer(serializer.ReadObject(reader) as IDictionary<TimerKey, DateTime>);
                }
            }
        }
    }
}
