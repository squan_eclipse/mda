﻿IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WorkflowProcessScheme'))
BEGIN
CREATE TABLE [dbo].[WorkflowProcessScheme](
	[Id] [uniqueidentifier] NOT NULL,
	[Scheme] [ntext] NOT NULL,
	[DefiningParameters] [ntext] NOT NULL,
	[DefiningParametersHash] [nvarchar](1024) NOT NULL,
	[ProcessName] [nvarchar](max) NOT NULL,
    [IsObsolete] [bit] NOT NULL DEFAULT (0),
 CONSTRAINT [PK_WorkflowProcessScheme] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END
GO


IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WorkflowProcessInstance'))
BEGIN
CREATE TABLE [dbo].[WorkflowProcessInstance](
	[Id] [uniqueidentifier] NOT NULL,
	[StateName] [nvarchar](max) NOT NULL,
	[ActivityName] [nvarchar](max) NOT NULL,
	[SchemeId] [uniqueidentifier] NULL,
	[PreviousState] [nvarchar](max) NULL,
	[PreviousStateForDirect] [nvarchar](max) NULL,
	[PreviousStateForReverse] [nvarchar](max) NULL,
	[PreviousActivity] [nvarchar](max) NULL,
	[PreviousActivityForDirect] [nvarchar](max) NULL,
	[PreviousActivityForReverse] [nvarchar](max) NULL,
	[IsDeterminingParametersChanged] [bit] NOT NULL,
 CONSTRAINT [PK_WorkflowProcessInstance_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[WorkflowProcessInstance] ADD  DEFAULT ((0)) FOR [IsDeterminingParametersChanged]

END
GO


IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WorkflowProcessInstancePersistence'))
BEGIN
CREATE TABLE [dbo].[WorkflowProcessInstancePersistence](
	[Id] [uniqueidentifier] NOT NULL,
	[ProcessId] [uniqueidentifier] NOT NULL,
	[ParameterName] [nvarchar](max) NOT NULL,
	[Value] [ntext] NOT NULL,
 CONSTRAINT [PK_WorkflowProcessInstancePersistence] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WorkflowProcessTransitionHistory'))
BEGIN
CREATE TABLE [dbo].[WorkflowProcessTransitionHistory](
	[Id] [uniqueidentifier] NOT NULL,
	[ProcessId] [uniqueidentifier] NOT NULL,
	[ExecutorIdentityId] [uniqueidentifier] NOT NULL,
	[ActorIdentityId] [uniqueidentifier] NOT NULL,
	[FromActivityName] [nvarchar](max) NOT NULL,
	[ToActivityName] [nvarchar](max) NOT NULL,
	[ToStateName] [nvarchar](max) NULL,
	[TransitionTime] [datetime] NOT NULL,
	[TransitionClassifier] [nvarchar](max) NOT NULL,
	[IsFinalised] [bit] NOT NULL,
	[FromStateName] [nvarchar](max) NULL,
 CONSTRAINT [PK_WorkflowProcessTransitionHistory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WorkflowProcessInstanceStatus'))
BEGIN
CREATE TABLE [dbo].[WorkflowProcessInstanceStatus](
	[Id] [uniqueidentifier] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Lock] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_WorkflowProcessInstanceStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF ( EXISTS (select * from sys.triggers where name = 'tWorkflowProcessTransitionHistoryInsert'))
BEGIN
	DROP TRIGGER  tWorkflowProcessTransitionHistoryInsert
END
GO

CREATE TRIGGER [dbo].[tWorkflowProcessTransitionHistoryInsert]
   ON  [dbo].[WorkflowProcessTransitionHistory]
   AFTER INSERT
AS 
BEGIN
	BEGIN TRAN 
		UPDATE [dbo].[WorkflowProcessInstance] 
			SET 
				StateName = COALESCE(i.ToStateName,StateName), 
				ActivityName = i.ToActivityName,
				PreviousActivity = i.FromActivityName,
				PreviousState = COALESCE(i.FromStateName,StateName),
				PreviousActivityForDirect = CASE WHEN i.TransitionClassifier = 'Direct' THEN i.FromActivityName ELSE PreviousActivityForDirect END,
				PreviousActivityForReverse = CASE WHEN i.TransitionClassifier = 'Reverse' THEN i.FromActivityName ELSE PreviousActivityForReverse END,
				PreviousStateForDirect = CASE WHEN i.TransitionClassifier = 'Direct' THEN COALESCE(i.FromStateName,PreviousStateForDirect) ELSE PreviousStateForDirect END,
				PreviousStateForReverse = CASE WHEN i.TransitionClassifier = 'Reverse' THEN COALESCE(i.FromStateName,PreviousStateForReverse) ELSE PreviousStateForReverse END
				FROM inserted i INNER JOIN [dbo].[WorkflowProcessInstance] wpi ON i.ProcessId = wpi.Id
				     
	COMMIT TRAN	
END


GO

IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WorkflowRuntime'))
BEGIN

CREATE TABLE [dbo].[WorkflowRuntime](
	[RuntimeId] [uniqueidentifier] NOT NULL,
	[Timer] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_WorkflowRuntime] PRIMARY KEY CLUSTERED 
(
	[RuntimeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'spWorkflowProcessResetRunningStatus')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN

DROP PROCEDURE [dbo].[spWorkflowProcessResetRunningStatus]

END 

GO

CREATE PROCEDURE [dbo].[spWorkflowProcessResetRunningStatus]
AS
BEGIN
	UPDATE [dbo].[WorkflowProcessInstanceStatus] SET [WorkflowProcessInstanceStatus].[Status] = 2 WHERE [WorkflowProcessInstanceStatus].[Status] = 1
END
GO

-- Simple schemestorage
IF (NOT EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'WorkflowScheme'))
BEGIN
CREATE TABLE [dbo].[WorkflowScheme](
 [Code] [nvarchar](256) NOT NULL,
 [Scheme] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_WorkflowScheme] PRIMARY KEY CLUSTERED 
(
 [Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'DropWorkflowProcess')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN

DROP PROCEDURE [dbo].[DropWorkflowProcess]

END 

GO


CREATE PROCEDURE [dbo].[DropWorkflowProcess] 
	@id uniqueidentifier
AS
BEGIN
	BEGIN TRAN
	
	DELETE FROM dbo.WorkflowProcessInstance WHERE Id = @id
	DELETE FROM dbo.WorkflowProcessInstanceStatus WHERE Id = @id
	DELETE FROM dbo.WorkflowProcessInstancePersistence  WHERE ProcessId = @id
	
	COMMIT TRAN
END
GO

IF TYPE_ID(N'IdsTableType') IS NULL 
BEGIN
CREATE TYPE IdsTableType AS TABLE 
( Id uniqueidentifier );
END
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'DropWorkflowProcesses')
                    AND type IN ( N'P', N'PC' ) ) 
BEGIN

DROP PROCEDURE [dbo].[DropWorkflowProcesses]

END 

GO

CREATE PROCEDURE [dbo].[DropWorkflowProcesses] 
	@Ids  IdsTableType	READONLY
AS	
BEGIN
    BEGIN TRAN
	
	DELETE dbo.WorkflowProcessInstance FROM dbo.WorkflowProcessInstance wpi  INNER JOIN @Ids  ids ON wpi.Id = ids.Id 
	DELETE dbo.WorkflowProcessInstanceStatus FROM dbo.WorkflowProcessInstanceStatus wpi  INNER JOIN @Ids  ids ON wpi.Id = ids.Id 
	DELETE dbo.WorkflowProcessInstanceStatus FROM dbo.WorkflowProcessInstancePersistence wpi  INNER JOIN @Ids  ids ON wpi.ProcessId = ids.Id 
	

	COMMIT TRAN
END
GO








