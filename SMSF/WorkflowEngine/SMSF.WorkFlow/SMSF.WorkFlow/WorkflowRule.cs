﻿using System;
using System.Collections.Generic;
using System.Linq;
using OptimaJet.Workflow.Core.Runtime;

namespace SMSF.WorkFlow
{
    public class WorkflowRule : IWorkflowRuleProvider
    {
        private Dictionary<string, Func<Guid, Guid, bool>> _funcs = new Dictionary<string, Func<Guid, Guid, bool>>();

        private Dictionary<string, Func<Guid, IEnumerable<Guid>>> _getIdentitiesFuncs = new Dictionary<string, Func<Guid, IEnumerable<Guid>>>();

        public WorkflowRule ()
        {
        //    _funcs.Add("IsDocumentAuthor", IsDocumentAuthor);
        //    _funcs.Add("IsAuthorsBoss", IsAuthorsBoss);
        //    _funcs.Add("IsDocumentController", IsDocumentController);
        //    _getIdentitiesFuncs.Add("IsDocumentAuthor", GetDocumentAuthor);
        //    _getIdentitiesFuncs.Add("IsDocumentController", GetDocumentController);
        //    _getIdentitiesFuncs.Add("IsAuthorsBoss", GetAuthorsBoss);
        }


        public bool CheckRule(Guid processId, Guid identityId, string ruleName)
        {
            return _funcs.ContainsKey(ruleName) && _funcs[ruleName].Invoke(processId, identityId);
        }

        public bool CheckRule(Guid processId, Guid identityId, string ruleName, IDictionary<string, string> parameters)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Guid> GetIdentitiesForRule(Guid processId, string ruleName)
        {
            return !_getIdentitiesFuncs.ContainsKey(ruleName) ? new List<Guid> {} : _getIdentitiesFuncs[ruleName].Invoke(processId);
        }

        public IEnumerable<Guid> GetIdentitiesForRule(Guid processId, string ruleName, IDictionary<string, string> parameters)
        {
            throw new NotImplementedException();
        }
    }
}
