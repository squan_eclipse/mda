﻿using OptimaJet.Workflow.Core.Builder;
using OptimaJet.Workflow.Core.Bus;
using OptimaJet.Workflow.Core.Parser;
using OptimaJet.Workflow.Core.Persistence;
using OptimaJet.Workflow.Core.Runtime;
using OptimaJet.Workflow.DbPersistence;
using SMSF.WorkFlow.Actions;
using System;
using System.Configuration;
using System.Xml.Linq;
using WorkflowRuntime = OptimaJet.Workflow.Core.Runtime.WorkflowRuntime;

namespace SMSF.WorkFlow
{
    #region Sample Workflow Initialization

    public class WorkflowInit
    {
        private string connectionString = "";
        private int entityId;

        public WorkflowInit(int entityId, string connectionString)
        {
            connectionString = ConfigurationManager.ConnectionStrings["WF"].ConnectionString;
        }

        public static WorkflowInit GetInstance(int entityID,string connectionString)
        {

            return new WorkflowInit(entityID,connectionString);
        }

        private IWorkflowBuilder GetDefaultBuilder()
        {
            var generator = new DbXmlWorkflowGenerator(connectionString);

            var builder = new WorkflowBuilder<XElement>(generator,
                new XmlWorkflowParser(),
                new DbSchemePersistenceProvider(connectionString)
                ).WithDefaultCache();
            return builder;
        }

        private WorkflowBuilder<XElement> GetWorkflowBuilderXElement()
        {
            return GetDefaultBuilder() as WorkflowBuilder<XElement>;
        }

        private WorkflowRuntime _runtime;

        public WorkflowRuntime Runtime
        {
            get
            {
                if (_runtime == null)
                {
                    var connectionString = ConfigurationManager.ConnectionStrings["WF"].ConnectionString;
                    var builder = GetDefaultBuilder();

                    _runtime = new WorkflowRuntime(Guid.NewGuid())
                        .WithBuilder(builder)
                        .WithBus(new NullBus())
                        .WithRoleProvider(new WorkflowRole())
                        .WithRuleProvider(new WorkflowRule())
                        .WithRuntimePersistance(new RuntimePersistence(connectionString))
                        .WithPersistenceProvider(new DbPersistenceProvider(connectionString))
                        .SwitchAutoUpdateSchemeBeforeGetAvailableCommandsOn()
                        .Start();

                    _runtime.ProcessStatusChanged += new EventHandler<ProcessStatusChangedEventArgs>(_runtime_ProcessStatusChanged);
                }

                return _runtime;
            }
        }

        private void _runtime_ProcessStatusChanged(object sender, ProcessStatusChangedEventArgs e)
        {
            if (e.NewStatus != ProcessStatus.Idled && e.NewStatus != ProcessStatus.Finalized)
                return;

            if (string.IsNullOrEmpty(e.ProcessName))
                return;

            WorkflowActions.DeleteEmptyPreHistory(e.ProcessId);
            _runtime.PreExecuteFromCurrentActivity(e.ProcessId);
        }
    }

    #endregion Sample Workflow Initialization
}