﻿//using System;
//using System.Collections.Concurrent;
//using System.IO;
//using System.Reactive.Concurrency;
//using System.Reactive.Linq;

//namespace OptimaJet.Common.IO
//{

//    public class FileSystemMonitorSettings
//    {
//        public bool MonitorSubfolders { get; set; }
//        public string Filter { get; set; }

//        public WatcherChangeTypes Actions { get; set; }

//        public TimeSpan BufferInterval { get; set; }

//        public FileSystemMonitorSettings()
//        {
//            Actions = WatcherChangeTypes.Changed | WatcherChangeTypes.Created | WatcherChangeTypes.Deleted | WatcherChangeTypes.Renamed;
//            BufferCount = 10;
//            BufferInterval = new TimeSpan(0, 1, 0);
//        }

//        public int BufferCount { get; set; }
//    }

//    public class FileSystemEventEventArgs : EventArgs
//    {
//        public string FilePath { get; set; }
//    }

//    public sealed class FileSystemMonitor
//    {
//        private IDisposable _observer;
//        readonly FileSystemWatcher _watcher;
//        private readonly FileSystemMonitorSettings _settings;
//        private volatile object _lock = new object();

//        public event EventHandler<FileSystemEventEventArgs> OnFileSystemEvent;
//        private event EventHandler<EventArgs> OnFileSystemEventInternal;
//        private readonly ConcurrentBag<FileSystemEventEventArgs> _eventsBag = new ConcurrentBag<FileSystemEventEventArgs>();

//        public FileSystemMonitor(string directoryPath, FileSystemMonitorSettings settings)
//        {
//            _watcher = new FileSystemWatcher(directoryPath) {IncludeSubdirectories = settings.MonitorSubfolders, Filter = settings.Filter};

//            if ((settings.Actions & WatcherChangeTypes.Changed) == WatcherChangeTypes.Changed)
//            {
//                _watcher.Changed += WatcherChanged;
//            }
//            if ((settings.Actions & WatcherChangeTypes.Created) == WatcherChangeTypes.Created)
//            {
//                _watcher.Created += WatcherCreated;
//            }
//            if ((settings.Actions & WatcherChangeTypes.Deleted) == WatcherChangeTypes.Deleted)
//            {
//                _watcher.Deleted += WatcherDeleted;
//            }
//            if ((settings.Actions & WatcherChangeTypes.Renamed) == WatcherChangeTypes.Renamed)
//            {
//                _watcher.Renamed += WatcherRenamed;
//            }

            
//            _settings = settings;

//        }

//        public void Start ()
//        {
//            _watcher.EnableRaisingEvents = true;
//            Resume();
//        }

//        public void Stop()
//        {
//            Pause();
//            _watcher.EnableRaisingEvents = true;
            
//        }

//        public void StartMonitor()
//        {
//            _watcher.EnableRaisingEvents = true;
//        }

//        public void StopMonitor()
//        {
//            _watcher.EnableRaisingEvents = false;
//        }

//        public void Pause()
//        {
//            lock (_lock)
//            {
//                if (_observer != null)
//                    _observer.Dispose();
//                _observer = null;
//            }
//        }

//        public void Resume()
//        {
//            lock (_lock)
//            {
//                _observer = Observable.FromEventPattern<EventArgs>(p => OnFileSystemEventInternal += p, p => OnFileSystemEventInternal -= p)
//                    .Buffer(_settings.BufferInterval, _settings.BufferCount,Scheduler.ThreadPool).Subscribe(e =>
//                                                                                           {
//                                                                                               FileSystemEventEventArgs args;
//                                                                                               while (_eventsBag.TryTake(out args))
//                                                                                               {
//                                                                                                   OnFileSystemEvent(this, args);
//                                                                                               }

//                                                                                           }
//                                                                                       ,
//                                                                                       e => Logger.Log.Error("Ошибка мониторинга файловой системы", e)
//                    );

//            }
//        }
      
//        private void WatcherRenamed(object sender, RenamedEventArgs e)
//        {
//            ProcessEvent(e);
//        }

//        private void WatcherDeleted(object sender, FileSystemEventArgs e)
//        {
//            ProcessEvent(e);
//        }

//        private void WatcherCreated(object sender, FileSystemEventArgs e)
//        {
//            ProcessEvent(e);
//        }

//        void WatcherChanged(object sender, FileSystemEventArgs e)
//        {
//            ProcessEvent(e);
//        }

//        private void ProcessEvent(FileSystemEventArgs e)
//        {
//            _eventsBag.Add(GetFileSystemEventEventArgs(e));
//            if (OnFileSystemEventInternal != null)
//                OnFileSystemEventInternal(null, EventArgs.Empty);
//        }



//        private static FileSystemEventEventArgs GetFileSystemEventEventArgs(FileSystemEventArgs e)
//        {
//            return new FileSystemEventEventArgs {FilePath = e.FullPath};
//        }
//    }
//}
