﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimaJet.Common
{
    public sealed class StringToDictionaryConverter
    {
        public bool IgnoreNull { get; private set; }
        public char ItemsSplitBy { get; set; }
        public char KeyValueSplitBy { get; set; }
        public List<string> KeyRemove { get; set; }
        public List<string> ValueRemove { get; set; }

        public StringToDictionaryConverter(bool ignoreNull, char itemsSplitBy, char keyValueSplitBy, List<string> keyRemove, List<string> valueRemove)
        {
            IgnoreNull = ignoreNull;
            ItemsSplitBy = itemsSplitBy;
            KeyValueSplitBy = keyValueSplitBy;
            KeyRemove = keyRemove;
            ValueRemove = valueRemove;
        }

        public Dictionary<string, string> Convert(string value)
        {
            var ret = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(value))
                return ret;

            foreach (string param in value.Split(ItemsSplitBy))
            {
                string[] tmp = param.Split(KeyValueSplitBy);

                if (tmp.Length == 2)
                {

                    var val = tmp[1];

                    foreach (var remove in ValueRemove)
                    {
                        val = val.Replace(remove, "");
                    }


                    var key = tmp[0];

                    foreach (var remove in KeyRemove)
                    {
                        key = key.Replace(remove, "");
                    }

                    if (!IgnoreNull || !string.IsNullOrEmpty(value))
                        ret.Add(key, val);
                }
            }
            return ret;
        }

        public static StringToDictionaryConverter Default
        {
            get { return new StringToDictionaryConverter(false, '\n', '=', new List<string> { "@" }, new List<string> { "\r" }); }
        }

        public static StringToDictionaryConverter DefaultIgnoreNull
        {
            get { return new StringToDictionaryConverter(true, '\n', '=', new List<string> { "@" }, new List<string> { "\r" }); }
        }

    }
}
