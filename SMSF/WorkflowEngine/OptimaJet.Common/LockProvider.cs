﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimaJet.Common
{
    public sealed class LockProvider<T>
    {
        private Dictionary<T, object> _wfLocks = new Dictionary<T, object>();

        private volatile object _lock = new object();

        public object GetLockObject(T instanceId)
        {
            if (_wfLocks.ContainsKey(instanceId))
                return _wfLocks[instanceId];
            lock (_lock)
            {
                if (_wfLocks.ContainsKey(instanceId))
                    return _wfLocks[instanceId];
                else
                {
                    var wflock = new object();
                    _wfLocks.Add(instanceId, wflock);
                    return wflock;
                }
            }
        }
    }
}
