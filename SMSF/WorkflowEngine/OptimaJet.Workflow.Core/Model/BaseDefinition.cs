﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimaJet.Workflow.Core.Model
{
    public abstract class BaseDefinition
    {
        public BaseDefinition()
        {
            DesignerSettings = new DesignerSettings();
        }

        public virtual string Name { get; set; }

        public DesignerSettings DesignerSettings { get; set; }
    }
}
