﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OptimaJet.Workflow.Core.Model
{
    public class DesignerSettings
    {
        public string Id { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Bending { get; set; }
        public string Scale { get; set; }

        public bool IdIsEmpty
        {
            get { return string.IsNullOrEmpty(Id); }
        }


        public static DesignerSettings Empty
        {
            get
            {
                return new DesignerSettings();
            }
        }

        public static string GenerateNewDesignerId()
        {
            return Guid.NewGuid().ToString("N");
        }

    }
}
