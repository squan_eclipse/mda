﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using OptimaJet.Workflow.Core.Model;

namespace OptimaJet.Workflow.Core.Runtime
{
    [Serializable]
    [DataContract]
    public class CommandParameter
    {
        [DataMember]
        public string ParameterName { get; internal set; }

        [DataMember]
        public string LocalizedName { get; internal set; }

        [DataMember]
        public object Value { get; set; }

        public Type Type { get { return Type.GetType(TypeName); } }

        [DataMember]
        public string TypeName { get; internal set; }
     
    }

    [Serializable]
    [DataContract]
    public sealed class WorkflowCommand //: IEqualityComparer<WorkflowCommand>
    {
       

        [DataMember]
        public Guid ProcessId { get; private set; }

        [DataMember]
        public string ValidForActivityName { get; private set; }

        [DataMember]
        public string ValidForStateName { get; private set; }


        public IEnumerable<Guid> Identities
        {
            get { return IdentitiesList; }
        }

         private  List<Guid> IdentitiesList = new List<Guid>(); 
        
        [DataMember]
        public string CommandName { get;  private set; }

        [DataMember]
        public string LocalizedName { get; private set; }

        [DataMember] public IEnumerable<CommandParameter> Parameters = new List<CommandParameter>();

        public object GetParameter(string name)
        {
            var parameter = Parameters.SingleOrDefault(p => p.ParameterName == name);
            if (parameter == null)
                return null;
            return parameter.Value;
        }

        public void SetParameter(string name, object value)
        {
            var parameter = Parameters.SingleOrDefault(p => p.ParameterName == name);
            if (parameter == null)
                throw new InvalidOperationException();
            //TODO Сделать проверку приводимости типов аналогичной динамик мэпперу
            //if (parameter.Type != value.GetType())
            //    throw new InvalidOperationException();
            parameter.Value = value;
        }

        public static WorkflowCommand Create(Guid processId, TransitionDefinition transitionDefinition,ProcessDefinition processDefinition)
        {
            if (transitionDefinition.Trigger.Type != TriggerType.Command || transitionDefinition.Trigger.Command == null)
                throw new InvalidOperationException();

            var parametrs = new List<CommandParameter>(transitionDefinition.Trigger.Command.InputParameters.Count);
            parametrs.AddRange(
                transitionDefinition.Trigger.Command.InputParameters.Select(
                    p =>
                    new CommandParameter
                        {
                            ParameterName = p.Key,
                            TypeName = p.Value.Type.AssemblyQualifiedName,
                            LocalizedName = processDefinition.GetLocalizedParameterName(p.Key, CultureInfo.CurrentCulture),
                            Value = null
                        }));

            return new WorkflowCommand
                       {
                           CommandName = transitionDefinition.Trigger.Command.Name,
                           LocalizedName = processDefinition.GetLocalizedCommandName(transitionDefinition.Trigger.Command.Name,CultureInfo.CurrentCulture),
                           Parameters = parametrs,
                           ProcessId = processId,
                           ValidForActivityName = transitionDefinition.From.Name,
                           ValidForStateName = transitionDefinition.From.State
                       };
        }

        public bool Validate ()
        {
            return Parameters.All(parameter => parameter.Value != null);
        }

        public void AddIdentity (Guid identityId)
        {
            if (!IdentitiesList.Contains(identityId))
                IdentitiesList.Add(identityId);
        }

        //public bool Equals(WorkflowCommand x, WorkflowCommand y)
        //{
        //    if (x == null && y == null)
        //        return true;

        //    if (x == null || y == null)
        //        return false;

        //    return x.CommandName == y.CommandName;
        //}

        //public int GetHashCode(WorkflowCommand obj)
        //{
        //    int hCode = bx.Height ^ bx.Length ^ bx.Width;
        //    return hCode.GetHashCode();
        //}
    }
}
