﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using common = DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Service.Business.Components
{    
    public class UserComponent : Component
    {
        #region Declaration

        private readonly UnitOfWork _uow = new UnitOfWork();
        private readonly IRepository<Users> _repository;
        private readonly IRepository<Settings> _settingRepository;
        private readonly IRepository<UserForgotPassword> _forgotPasswordRepository;

        #endregion

        #region Constructor
        
        public UserComponent()
        {
            _repository = _uow.Repository<Users>();
            _settingRepository = _uow.Repository<Settings>();
            _forgotPasswordRepository = _uow.Repository<UserForgotPassword>();
        }

        #endregion

        #region User
        
        public string ValidateUser(string xml)
        {
            var model = (LoginModel)ModelDeserializer.DeserializeFromXml<LoginModel>(xml);
            return model != null ? ValidateUser(model.Email, model.Password) : NullModelReference();
        }

        private string ValidateUser(string email, string password)
        {
            var responseMessage = "Successful";
            var responseStatus = ResponseStatus.Success;
            UserModel userModel = null;
            
            int maxRetriesAllowed;
            var loginSettings = _settingRepository.GetAll()
                                                 .Where(p => ((common.SettingTypes)p.SettingTypeID == common.SettingTypes.PasswordSettings && p.Description == "MaxPasswordRetries") ||
                                                             ((common.SettingTypes)p.SettingTypeID == common.SettingTypes.SessionSettings && p.Description == "MaxSessionTimeOut") ||
                                                             ((common.SettingTypes)p.SettingTypeID == common.SettingTypes.VersionSettings && p.Description == "VersionNumber")
                                                        )
                                                 .ToDictionary( t => t.Description, t => t.SettingValue );

            Int32.TryParse(loginSettings.ContainsKey("MaxPasswordRetries") ? loginSettings["MaxPasswordRetries"] : "0", out maxRetriesAllowed);

            var systemSettings = new SystemSettingsModel
            {
                SessionSetting =
                {
                    MaxSessionTimeout = loginSettings.ContainsKey("MaxSessionTimeOut")
                        ? Convert.ToInt32(loginSettings["MaxSessionTimeOut"])
                        : 20
                },
                VersionSetting = {VersionNumber = loginSettings.GetValue("VersionNumber")}
            };

            var users = _repository.GetAll()
                                  .Where(a => a.Email.ToLower() == email.ToLower() && a.Password == password && a.StatusID == Status.Active 
                                                && a.AccountManager.StatusID == Status.Active
                                                && (a.FailedPasswordAttemptCount ?? 0) < maxRetriesAllowed)
                                  .ToList();
            
            if (users.Any())
            {
                if (users.Count() > 1)
                {
                    #region multiple login found
                    var listUsers = users.Select(user => new UserModel
                        {
                            Email = user.AccountManager.Email,
                            FirstName = user.AccountManager.FirstName,
                            ID = user.ID,
                            LastName = user.AccountManager.LastName,
                            MiddleName = user.AccountManager.MidName,
                            Name = user.Email,
                            Group = (UserGroup)user.GroupID,
                            EntityType = (common.EntityTypes)user.AccountManager.Entity.TypeID,
                            UserImage = user.UserImage!= null ? Convert.ToBase64String(user.UserImage) : "",
                            SystemSettings = systemSettings,
                            Client = SetClient(user.AccountManager.Entity),
                            Entity = SetEntity(user.AccountManager.Entity),
                            UserIdentifier = user.Identifier,
                            GroupIdentifier = user.Group.Identifier,
                            
                        }).ToList();

                    foreach (var u in users)
                    {
                        u.FailedPasswordAttemptCount = null;
                        u.FailedPasswordAttemptStart = null;
                    }
                    _uow.Commit();
                    return ModelSerializer.GenerateReponseXml<UserModel>(listUsers, responseStatus, Resource_EN.Error_MultipleUsersFound);
                    #endregion
                }
                else
                {
                    #region single Login found
                    var user = users.First();
                    if (user != null)
                    {
                        responseStatus = ResponseStatus.Success;
                        userModel = new UserModel
                        {
                            Email = user.AccountManager.Email,
                            FirstName = user.AccountManager.FirstName,
                            ID = user.ID,
                            LastName = user.AccountManager.LastName,
                            MiddleName = user.AccountManager.MidName,
                            Name = user.Email,
                            Group = (UserGroup)user.GroupID,
                            EntityType = (common.EntityTypes)user.AccountManager.Entity.TypeID,
                            UserImage = user.UserImage != null ? Convert.ToBase64String(user.UserImage) : "",
                            SystemSettings = systemSettings,
                            Client = SetClient(user.AccountManager.Entity),
                            Entity = SetEntity(user.AccountManager.Entity),
                            UserIdentifier = user.Identifier,
                            GroupIdentifier = user.Group.Identifier
                        };
                    }
                    user.LastLoginTime = DateTime.Now;
                    _uow.Commit();
                    #endregion
                }
            }
            else
            {
                #region if user not found on login

                var userAccounts =
                    _repository.GetAll()
                        .FirstOrDefault(
                            a =>
                                a.Email == email && a.StatusID == Status.Active &&
                                a.AccountManager.StatusID == Status.Active);

                if (userAccounts != null)
                {
                    //Checking if account is locked , i.e. if maximum password retries have been used 

                    if ((userAccounts.FailedPasswordAttemptCount ?? 0) >= maxRetriesAllowed)
                    {
                        responseMessage = Resource_EN.Error_UserAccountLocked;
                        responseStatus = ResponseStatus.Failure;
                        return ModelSerializer.GenerateReponseXml(null, responseStatus, responseMessage);
                    }

                    if (userAccounts.FailedPasswordAttemptCount == null)
                        userAccounts.FailedPasswordAttemptStart = DateTime.Now;

                    userAccounts.FailedPasswordAttemptCount = userAccounts.FailedPasswordAttemptCount == null ? 1 : userAccounts.FailedPasswordAttemptCount + 1;

                    _uow.Commit();
                }
                responseStatus = ResponseStatus.Failure;
                responseMessage = Resource_EN.Error_InvalidUsernameOrPassword;
                #endregion
            }

            return ModelSerializer.GenerateReponseXml(userModel, responseStatus, responseMessage);
        }

        public string UpdateLastLogin(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return UpdateLastLogin(model.Value);
        }

        private string UpdateLastLogin(int userID)
        {
            var responseMessage = "Successful";
            var responseStatus = ResponseStatus.Success;

            var user = _repository.GetById(userID);
            if (user != null)
            {
                user.LastLoginTime = DateTime.Now;
                _uow.Commit();
            }
            else
            {
                responseMessage = "User Not found";
                responseStatus = ResponseStatus.Failure;  
            }
            return ModelSerializer.GenerateReponseXml(null, responseStatus, responseMessage);

        }

        private AssociatedEntites SetClient(Entities entity)
        {
            if (entity == null) return null;

            return (common.EntityTypes) entity.TypeID == common.EntityTypes.Client
                ? new AssociatedEntites {EntityID = entity.ID, EntityName = entity.Name}
                : null;
        }

        private AssociatedEntites SetEntity(Entities entity)
        {
            if (entity == null) return null;

            return (common.EntityTypes) entity.TypeID == common.EntityTypes.Client
                ? SetEntity(entity.ParentEntity)
                : ((common.EntityTypes) entity.TypeID == common.EntityTypes.Entity
                    ? new AssociatedEntites {EntityID = entity.ID, EntityName = entity.Name}
                    : null);
        }

        #endregion

        #region forgotpassword
        public string GetUerAssociatedEntites(string xml) 
        {
            var userEmail = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(xml);
            var entities = _repository.GetAll().Where(p => p.Email == userEmail.Value && p.StatusID == Status.Active).Select(m => new AssociatedEntites { EntityID = m.AccountManager.Entity.ID, EntityName = m.AccountManager.Entity.Name }).ToList();

            return entities.Count != 0
                ? ModelSerializer.GenerateReponseXml<AssociatedEntites>(entities, ResponseStatus.Success, "Successful")
                : ModelSerializer.GenerateReponseXml<AssociatedEntites>(null, ResponseStatus.Failure,
                    Resource_EN.Error_UserNotFound);
        }

        public string SendForgotPassEmail(string xml)
        {
            var forgotPassParams = (GenericModel<ForgotPasswordModel>)ModelDeserializer.DeserializeFromXml<GenericModel<ForgotPasswordModel>>(xml);
            var uniqueToken = Guid.NewGuid();
            var user =
                _repository.GetAll()
                    .FirstOrDefault(
                        p =>
                            p.Email == forgotPassParams.Value.EmailAddress &&
                            p.AccountManager.EntityID == forgotPassParams.Value.EntityID);

            if (user != null)
            {
                var ufp = new UserForgotPassword
                {
                    UserID = user.ID,
                    CreatedOn = DateTime.Now,
                    ForgotPasswordToken = uniqueToken,
                    NewPassword = forgotPassParams.Value.NewPassword,
                    StatusID = Status.Active
                };
                _forgotPasswordRepository.Add(ufp);
            }
            _uow.Commit();
            var body = "To reset your password click the following link<br />" +
                       "<a href='" + forgotPassParams.Value.BaseUrl + "/Account/PasswordReset?authCode=" + uniqueToken +
                       "'>Reset Password</a>";

            return !SendEmail(forgotPassParams.Value.EmailAddress, body, true, "DBA Password Reset", MailPriority.High)
                ? ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Failure")
                : ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }
        public string ResetPassword(string xml)
        {
            var resetPassParams = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(xml);
            var thisUser = (
                from a in _uow.Repository<UserForgotPassword>().GetAll()
                join b in _uow.Repository<Users>().GetAll() on a.UserID equals b.ID
                // && p.StatusID == Status.Active
                where
                    a.ForgotPasswordToken == new Guid(resetPassParams.Value)
                    && a.StatusID == Status.Active
                    && b.StatusID == Status.Active
                select (new { ForgotPassDetail = a, ResetUser = b })
                ).FirstOrDefault();
            
            if (thisUser!= null)
            {
                // 24 hours passed after requesting the new password ... 
                if (DateTime.Now > thisUser.ForgotPassDetail.CreatedOn.AddHours(24))
                {
                    return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_ResetKeyExpired);
                }
                thisUser.ResetUser.ModifiedOn = DateTime.Now;
                thisUser.ResetUser.Password = thisUser.ForgotPassDetail.NewPassword;
                thisUser.ResetUser.FailedPasswordAttemptCount = null;
                thisUser.ResetUser.FailedPasswordAttemptStart = null;
                var allActiveRequests = _forgotPasswordRepository.GetAll().Where(p => p.UserID == thisUser.ResetUser.ID && p.StatusID == Status.Active);
                foreach (var request in allActiveRequests)
                {
                    request.StatusID = Status.Inactive;
                }
                _uow.Commit();
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_ResetKeyUsedOrNotFound);
        }
        #endregion

        #region Groups
        
        public string GetUserGroups(string xml)
        {
            return GetUserGroups();
        }
        
        public string GetUserGroups()
        {
            var groups = _uow.Repository<Groups>().GetAll().Select(a => new UserGroupModel { ID = a.ID, Title = a.Title, Description = a.Description, Status = a.StatusID }).ToList();

            return ModelSerializer.GenerateReponseXml<UserGroupModel>(groups, ResponseStatus.Success, "Successful");
        }
        
        public string GetUserGroup(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetUserGroup(model.Value);
        }
        
        public string GetUserGroup(int id)
        {
            var group = _uow.Repository<Groups>().GetById(id);

            var model = new UserGroupModel
            {
                Description = group.Description,
                ID = group.ID,
                IsSystem = group.IsSystem,
                Status = @group.StatusID,
                Title = group.Title
            };
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }
        
        public string CreateUserGroup(string xml)
        {
            var model = (UserGroupModel)ModelDeserializer.DeserializeFromXml<UserGroupModel>(xml);

            return CreateUserGroup(model);
        }
        
        public string CreateUserGroup(UserGroupModel model)
        {
            var groupRepository = _uow.Repository<Groups>();

            if (groupRepository.Any(a => a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_UserGroupAlreadyExists);

            var entity = new Groups
            {
                Description = model.Description,
                IsSystem = model.IsSystem,
                StatusID = model.Status,
                Title = model.Title
            };

            groupRepository.Add(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }
        
        public string UpdateUserGroup(string xml)
        {
            var model = (UserGroupModel)ModelDeserializer.DeserializeFromXml<UserGroupModel>(xml);

            return UpdateUserGroup(model);
        }
        
        public string UpdateUserGroup(UserGroupModel model)
        {
            var groupRepository = _uow.Repository<Groups>();

            if (groupRepository.Any(a => a.ID != model.ID && a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_UserGroupAlreadyExists);

            var entity = groupRepository.GetById(model.ID);

            entity.Description = model.Description;
            entity.Title = model.Title;

            groupRepository.Update(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }
        
        public string ChangeUserGroupStatus(string xml)
        {
            var model = (DictionaryModel<int, Status>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, Status>>(xml);

            return ChangeUserGroupStatus(model);
        }
        
        public string ChangeUserGroupStatus(DictionaryModel<int, Status> model)
        {
            var groupRepository = _uow.Repository<Groups>();

            var entity = groupRepository.GetById(model.Key);

            entity.StatusID = model.Value;

            groupRepository.Update(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        #endregion

        #region Roles
        
        public string GetRoles(string xml)
        {
            return GetRoles();
        }
        
        public string GetRoles()
        {
            var roles = _uow.Repository<Roles>().GetAll().Select(a => new RoleModel { ID = a.ID, Title = a.Title, Description = a.Description, Status = a.StatusID }).ToList();

            return ModelSerializer.GenerateReponseXml<RoleModel>(roles, ResponseStatus.Success, "Successful");
        }
        
        public string GetRole(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetRole(model.Value);
        }
        
        public string GetRole(int id)
        {
            var role = _uow.Repository<Roles>().GetById(id);

            var model = new RoleModel
            {
                Description = role.Description,
                ID = role.ID,
                IsSystem = role.IsSystem,
                Status = role.StatusID,
                Title = role.Title
            };
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }
        
        public string CreateRole(string xml)
        {
            var model = (RoleModel)ModelDeserializer.DeserializeFromXml<RoleModel>(xml);

            return CreateRole(model);
        }
        
        public string CreateRole(RoleModel model)
        {
            var roleRepository = _uow.Repository<Roles>();

            if (roleRepository.Any(a => a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_RoleAlreadyExists);

            var entity = new Roles
            {
                Description = model.Description,
                IsSystem = model.IsSystem,
                StatusID = model.Status,
                Title = model.Title
            };

            roleRepository.Add(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }
        
        public string UpdateRole(string xml)
        {
            var model = (RoleModel)ModelDeserializer.DeserializeFromXml<RoleModel>(xml);

            return UpdateRole(model);
        }
        
        public string UpdateRole(RoleModel model)
        {
            var roleRepository = _uow.Repository<Roles>();

            if (roleRepository.Any(a => a.ID != model.ID && a.Title == model.Title))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_RoleAlreadyExists);

            var entity = roleRepository.GetById(model.ID);

            entity.Description = model.Description;
            entity.Title = model.Title;

            roleRepository.Update(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        public string ChangeRoleStatus(string xml)
        {
            var model = (DictionaryModel<int, Status>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, Status>>(xml);

            return ChangeRoleStatus(model);
        }
        
        public string ChangeRoleStatus(DictionaryModel<int, Status> model)
        {
            var roleRepository = _uow.Repository<Roles>();

            var entity = roleRepository.GetById(model.Key);

            entity.StatusID = model.Value;

            roleRepository.Update(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        #endregion

        #region GroupRoles
                
        public string GetUserGroupRoles(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetUserGroupRoles(model.Value);
        }
        
        public string GetUserGroupRoles(int id)
        {
            var group = _uow.Repository<Groups>().GetById(id);

            var model = new UserGroupRolesModel
            {
                Description = group.Description,
                ID = group.ID,
                Title = group.Title,
                Roles = group.GroupRoles.Where(a => a.Role.StatusID == Status.Active)
                                          .Select(a => new RoleModel
                                          {
                                              Description = a.Role.Description,
                                              ID = a.RoleID,
                                              Title = a.Role.Title,
                                              Status = a.Role.StatusID
                                          }).ToList()
            };

            var roles = model.Roles.Select(x => x.ID).ToArray<int>();
            model.AvailableRoles = _uow.Repository<Roles>().GetAll().Where(a => ! roles.Contains(a.ID) && a.StatusID == Status.Active)
                                                .Select(a => new RoleModel
                                                {
                                                    Description = a.Description,
                                                    ID = a.ID,
                                                    Title = a.Title,
                                                    Status = a.StatusID
                                                }).ToList();

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }
        
        public string AddUserGroupRoles(string xml)
        {
            var model = (UserGroupRolesModel)ModelDeserializer.DeserializeFromXml<UserGroupRolesModel>(xml);

            return AddUserGroupRoles(model);
        }
        
        public string AddUserGroupRoles(UserGroupRolesModel model)
        {
            var groupRoleRepository = _uow.Repository<GroupRoles>();

            var groupRoles = groupRoleRepository.GetAll().Where(a => a.GroupID == model.ID);
            foreach (var roles in groupRoles)
                groupRoleRepository.Delete(groupRoleRepository.GetById(roles.ID));

            foreach (var role in model.Roles)
                groupRoleRepository.Add(new GroupRoles { GroupID = model.ID, RoleID = role.ID });

            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        #endregion

        #region RoleOptions
        
        public string GetUserRoleOptions(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);

            return GetUserRoleOptions(model.Value);
        }
        
        public string GetUserRoleOptions(int id)
        {
            var role = _uow.Repository<Roles>().GetById(id);

            var model = new RoleOptionsModel
            {
                Description = role.Description,
                ID = role.ID,
                Title = role.Title,
                SelectedOptions = role.RoleOptions.Where(a => a.Option.StatusID == Status.Active)
                                                .Select(a => a.Option.ID).ToList(),
                OptionsTree = _uow.Repository<Options>().GetAll().Where(a => a.ParentID == null && a.StatusID == Status.Active)
                                                        .OrderBy(a => a.ID)
                                                        .Select(a => new ClientTreeViewItemModel
                                                        {
                                                            id = a.ID,
                                                            text = a.Title
                                                        }).ToList()
            };

            foreach (var item in model.OptionsTree)
                item.items = LoadChildOptions(item.id, model.SelectedOptions);

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        private List<ClientTreeViewItemModel> LoadChildOptions(int parentId, List<int> selectedOptions)
        {
            return _uow.Repository<Options>().GetAll().Where(a => a.ParentID == parentId && a.StatusID == Status.Active && !a.AllowAnonymous)
                                                    .OrderBy(a => a.Title)
                                                    .AsEnumerable()
                                                    .Select(a => new ClientTreeViewItemModel
                                                    {
                                                        id = a.ID,
                                                        text = a.Title,
                                                        selected = selectedOptions.Contains(a.ID),
                                                        items = _uow.Repository<Options>().Any(x => x.ParentID == a.ID) ? LoadChildOptions(a.ID, selectedOptions) : null
                                                    }).ToList();
        }
        
        public string AddUserRoleOptions(string xml)
        {
            var model = (RoleOptionsModel)ModelDeserializer.DeserializeFromXml<RoleOptionsModel>(xml);

            return AddUserRoleOptions(model);
        }
        
        public string AddUserRoleOptions(RoleOptionsModel model)
        {
            var roleOptionRepository = _uow.Repository<RoleOptions>();

            var roleOptions = roleOptionRepository.GetAll().Where(a => a.RoleID == model.ID);
            foreach (var option in roleOptions)
            {
                roleOptionRepository.Delete(roleOptionRepository.GetById(option.ID));
            }
            var maxId = roleOptionRepository.GetAll().Max(p => p.ID)+1;
            
            foreach (var option in model.SelectedOptions)
                roleOptionRepository.Add(new RoleOptions
                {
                    ID = maxId++,
                    RoleID = model.ID,
                    OptionID = option,
                    CreatedBy = model.CreateBy,
                    CreatedOn = model.CreatedOn,
                    IsAssigned = true,
                    IsSystem = true
                });

            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        #endregion

        #region CommonFunction
        /// <summary>
        /// Send's Email with respect to settings in App.config 
        /// <para>
        ///  Note From Address will be overriden
        /// </para>
        /// </summary>
        /// <param name="mail"></param>
        /// <returns></returns>
        private bool SendEmail(MailMessage mail)
        {
            var emailSettings = new CommonComponent().GetSettingByType<EmailSettingsModel>();

                var client = new SmtpClient
                {
                    Host = emailSettings.SMTPHost,
                    Timeout = 15000,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailSettings.SMTPUserName, emailSettings.SMTPPassword),
                    EnableSsl = emailSettings.SMTPRequireSSL == "true"
                };

                mail.From = new MailAddress(emailSettings.SMTPFromAddress);
               
            if (emailSettings.SMTPPort != 0)
                {
                client.Port = emailSettings.SMTPPort;
                }
                client.Send(mail);
                return true;           
        }

        private bool SendEmail(string toEmailAddress, string body, bool bodyIsHtml, string subject, MailPriority priority)
        {
            var emailSettings = new CommonComponent().GetSettingByType<EmailSettingsModel>();
             var mail = new MailMessage(emailSettings.SMTPFromAddress,toEmailAddress,subject,body)
            {
                Priority = priority,
                IsBodyHtml = bodyIsHtml
            };
            var client = new SmtpClient
            {
                Host = emailSettings.SMTPHost,
                Timeout = 15000,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailSettings.SMTPUserName, emailSettings.SMTPPassword),
                EnableSsl = emailSettings.SMTPRequireSSL == "true"
            };

            if (emailSettings.SMTPPort != 0)
            {
                client.Port = emailSettings.SMTPPort;
            }
            client.Send(mail);
            return true;
        }

        #endregion

        #region Profile

        public string UpdatePersonalInfo(string xml)
        {
            var model = ((GenericModel<UserProfileModel>)ModelDeserializer.DeserializeFromXml<GenericModel<UserProfileModel>>(xml)).Value;
            Tuple<ResponseStatus,string> responseStatus = UpdatePersonalInfo(model);
            
            return ModelSerializer.GenerateReponseXml(null, responseStatus.Item1, responseStatus.Item2);
        }

        private Tuple<ResponseStatus,string> UpdatePersonalInfo(UserProfileModel model)
        {
            var responseStatus = new Tuple<ResponseStatus, string>(ResponseStatus.Success, "Successful");
            var thisUser = _uow.Repository<AccountManagers>().GetById(model.UserDetail.ID);
            bool doUpdate = false;
            if (thisUser != null)
            {
                if (model.UpdatePersonalInfo)
                {
                    thisUser.FirstName = model.UserDetail.FirstName;
                    thisUser.LastName = model.UserDetail.LastName;
                    thisUser.MidName = model.UserDetail.MidName;
                    thisUser.ModifiedBy = model.ModifiedBy;
                    thisUser.ModifiedOn = DateTime.Now;
                    doUpdate = true;
                }
                if (model.UpdatePassword)
                {
                    if (thisUser.User.Password == model.ChangePassword.CurrentPassword)
                    {
                        thisUser.User.Password = model.ChangePassword.NewPassword;
                        thisUser.User.ModifiedBy = model.ModifiedBy;
                        thisUser.User.ModifiedOn = DateTime.Now;
                        doUpdate = true;
                    }
                    else
                    {
                        responseStatus = new Tuple<ResponseStatus, string>(ResponseStatus.Failure, Resource_EN.Error_InvalidCurrentPassword);
                        return responseStatus;
                    }
           
                }
                if (model.UpdateProfilePic)
                {
                    if (IsValidImage(Convert.FromBase64String(model.ProfilePicBase64)))
                    {
                        thisUser.User.UserImage = Convert.FromBase64String(model.ProfilePicBase64);
                        thisUser.User.ModifiedBy = model.ModifiedBy;
                        thisUser.User.ModifiedOn = DateTime.Now;
                        doUpdate = true;
                    }
                }
                #region Contact information

                if (model.ListContact != null)
                {
                    foreach (var contact in model.ListContact.Where(a => a.StatusID == Status.Deleted).ToList())
                    {
                        var contactEntity = _uow.Repository<Telephones>().GetById(contact.ID);
                        contactEntity.StatusID = Status.Deleted;
                    }

                    foreach (var contact in model.ListContact.Where(a => a.ID > 0 && a.StatusID == Status.Active).ToList())
                    {
                        var contactEntity = _uow.Repository<Telephones>().GetById(contact.ID);

                        contactEntity.AreaCode = contact.AreaCode;
                        contactEntity.CountryCode = contact.CountryCode;
                        contactEntity.PhoneNumber = contact.PhoneNumber;
                        contactEntity.TypeID = contact.TypeID;
                    }

                    foreach (var contact in model.ListContact.Where(a => a.ID < 0 && a.StatusID == Status.Active).ToList())
                    {
                        _uow.Repository<Telephones>().Add(new Telephones
                        {
                            AccountManagerID = thisUser.ID,
                            AreaCode = contact.AreaCode,
                            CountryCode = contact.CountryCode,
                            PhoneNumber = contact.PhoneNumber,
                            StatusID = Status.Active,
                            TypeID = contact.TypeID
                        });
                    }

                    doUpdate = true;
                }

                #endregion Contact information


                if (doUpdate)
            _uow.Commit();
            }
            else
                responseStatus = new Tuple<ResponseStatus, string>(ResponseStatus.Failure, "Failure");

            return responseStatus;
        }

        public string UpdateEntityAddress(string xml)
        {
            var model =(DictionaryModel<int,List<AddressCreateModel>>) ModelDeserializer.DeserializeFromXml<DictionaryModel<int,List<AddressCreateModel>>>(xml);
            return UpdateEntityAddress(model.Key, model.Value);
        }

        private string UpdateEntityAddress(int entityId,List<AddressCreateModel> addressList)
        {
            var entity = _uow.Repository<Entities>().GetById(entityId);
            if (entity == null) return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Failure");

            new FirmComponent().UpdateAddressOnUpdateEntity(addressList, entity);
            _uow.Commit();
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        public string GetUserProfile(string xml)
        {
            var userId = ((GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml)).Value;
            return GetUserProfile(userId);
        }

        public string Unlock(string xml)
        {
            var userId = ((GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml)).Value;
            var user = _uow.Repository<Users>().GetById(userId);
            if (user == null) return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Failure");

            user.FailedPasswordAttemptCount = 0;
            user.FailedPasswordAttemptStart = null;
            _uow.Repository<Users>().Update(user);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");

            
        }

        private string GetUserProfile(int userId)
        {
            var thisUser = _uow.Repository<AccountManagers>().GetById(userId);

            if (thisUser == null)
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "EmptyOrInvalidModel");

            var userProfile = new UserProfileModel
            {
                IsPrimaryAccountManager = thisUser.Entity.PrimaryAccountManagerID == thisUser.ID,
                UserDetail = new AccountManagerDetailModel { FirstName = thisUser.FirstName, LastName = thisUser.LastName, MidName = thisUser.MidName, Email = thisUser.Email },
                PasswordPolicy = new CommonComponent().GetSettingByType<PasswordPolicyModel>(),
                ListContact = thisUser.Telephones.Where(c=> c.StatusID == Status.Active).Select(c => new ContactCreateModel {
                                            TypeID = c.TypeID,
                                            AreaCode = c.AreaCode,
                                            CountryCode = c.CountryCode,
                                            ID = c.ID,
                                            PhoneNumber = c.PhoneNumber,
                                            Type = c.TelephoneType.Name,
                                            StatusID = c.StatusID
                                        }).ToList()
            };

            if (userProfile.IsPrimaryAccountManager)
            {
                userProfile.ListAddress = thisUser.Entity.Addresses.Any(a => a.StatusID == Status.Active) ?
                    thisUser.Entity.Addresses.Where(a => a.StatusID == Status.Active)
                        .Select ( a => new AddressCreateModel
                        {
                            CountryID = a.CountryID,
                            StateID = a.StateID,
                            TypeID = a.TypeID,
                            City = a.City,
                            Country = a.CountryID != null ? a.Country.Name : "",
                            Default = a.Default,
                            ID = a.ID,
                            PostCode = a.PostCode,
                            State = a.StateID != null ? a.State.Name : "",
                            StreetName = a.StreetName,
                            StreetNumber = a.StreetNumber,
                            StreetType = a.StreetType,
                            Type = a.AddressType.Description,
                            UnitNumber = a.UnitNumber,
                            StatusID = a.StatusID
                        }).ToList() : null;
            }
            return ModelSerializer.GenerateReponseXml(userProfile, ResponseStatus.Success, "Successful");
        }

        private bool IsValidImage(byte[] bytes)
        {
            try
            {
                using (var ms = new MemoryStream(bytes))
                    Image.FromStream(ms);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
