﻿using System.CodeDom;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class NoteComponent : Component
    {
        private readonly UnitOfWork _uow = new UnitOfWork();
        private readonly IRepository<Notes> _notesRepository;
        private readonly IRepository<JobNotes> _jobNotesRepository;

        public NoteComponent()
        {
            _notesRepository = _uow.Repository<Notes>();
            _jobNotesRepository = _uow.Repository<JobNotes>();
        }

        public string AddUpdateNotes(string parameter)
        {
            var model = (NotesModel)ModelDeserializer.DeserializeFromXml<NotesModel>(parameter);
            if (model.TypeID == (int)EntityGenericType.Job)
            {
                JobNotes jobNotes;
                if (model.ID == 0)
                {
                    jobNotes = new JobNotes
                    {
                        CreatedBy = model.CreatedBy,
                        CreatedOn = model.CreatedOn,
                        JobID = model.JobID,
                        ModifiedBy = model.ModifiedBy,
                        ModifiedOn = model.ModifiedOn,
                        Note = model.Note
                    };
                    _jobNotesRepository.Add(jobNotes);
                    model.ID = jobNotes.ID;
                }
                else
                {
                    jobNotes = _jobNotesRepository.GetById(model.ID);
                    jobNotes.ModifiedBy = model.ModifiedBy;
                    jobNotes.ModifiedOn = model.ModifiedOn;
                    jobNotes.Note = model.Note;
                    _jobNotesRepository.Update(jobNotes);
                }
            }
            else
            {
                Notes note;
                if (model.ID == 0)
                {
                    note = new Notes
                    {
                        CreatedBy = model.CreatedBy,
                        CreatedOn = model.CreatedOn,
                        EntityID = model.EntityID,
                        ModifiedBy = model.ModifiedBy,
                        ModifiedOn = model.ModifiedOn,
                        Note = model.Note
                    };
                    _notesRepository.Add(note);
                    model.ID = note.ID;
                }
                else
                {
                    note = _notesRepository.GetById(model.ID);
                    note.ModifiedBy = model.ModifiedBy;
                    note.ModifiedOn = model.ModifiedOn;
                    note.Note = model.Note;
                    _notesRepository.Update(note);
                }
            }
            _uow.Commit();
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        public string GetNotes(string parameters)
        {
            var notesGenerics = (NotesGenericModel)ModelDeserializer.DeserializeFromXml<NotesGenericModel>(parameters);
           
            return ModelSerializer.GenerateReponseXml(GetNotes(notesGenerics.ParentID,(EntityGenericType)notesGenerics.TypeId), ResponseStatus.Success, "Successful");
        }

        private List<NotesModel> GetNotes(int parentId,EntityGenericType type)
        {
            List<NotesModel> noteModel;
            if (type == EntityGenericType.Job)
            {
                noteModel = _jobNotesRepository.GetAll()
                    .Where(model => model.JobID == parentId)
                    .Select(a => new NotesModel
                    {
                        CreatedBy = a.CreatedBy,
                        CreatedOn = a.CreatedOn,
                        ID = a.ID,
                        JobID = a.JobID,
                        ModifiedBy = a.ModifiedBy,
                        ModifiedOn = a.ModifiedOn,
                        Note = a.Note,
                        TypeID = (int)EntityGenericType.Job
                    })
                    .ToList();
            }
            else
            {
                noteModel = _notesRepository.GetAll().Where(model => model.EntityID == parentId)
                    .Select(a => new NotesModel
                    {
                        CreatedBy = a.CreatedBy,
                        CreatedOn = a.CreatedOn,
                        ID = a.ID,
                        EntityID = a.EntityID,
                        ModifiedBy = a.ModifiedBy,
                        ModifiedOn = a.ModifiedOn,
                        Note = a.Note,
                        TypeID = (int)EntityGenericType.Job
                    }).ToList();
            }
            return noteModel;
        }

        #region Entity Notes [Firm/Account/Fund]

        public string AddEntityNotes(string parameter)
        {
            var model = (NoteModel)ModelDeserializer.DeserializeFromXml<NoteModel>(parameter);
            return AddNote(model, EntityGenericType.Entity);
        }

        public string UpdateEntityNotes(string parameter)
        {
            var model = (NoteModel)ModelDeserializer.DeserializeFromXml<NoteModel>(parameter);
            return UpdateNote(model, EntityGenericType.Entity);
        }

        public string DeleteEntityNotes(string parameter)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameter);
            return DeleteNote(model.Value, EntityGenericType.Entity);
        }

        #endregion Entity Notes [Firm/Account/Fund]

        #region Job Notes

        public string AddJobNotes(string parameter)
        {
            var model = (NoteModel)ModelDeserializer.DeserializeFromXml<NoteModel>(parameter);
            return AddNote(model, EntityGenericType.Job);
        }

        public string UpdateJobNotes(string parameter)
        {
            var model = (NoteModel)ModelDeserializer.DeserializeFromXml<NoteModel>(parameter);
            return UpdateNote(model, EntityGenericType.Job);
        }

        public string DeleteJobNotes(string parameter)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameter);
            return DeleteNote(model.Value, EntityGenericType.Job);
        }

        #endregion Job Notes

        public string GetNote(string parameters)
        {
            var notesDictionary = (DictionaryModel<int,EntityGenericType>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int,EntityGenericType>>(parameters);
            return GetNote(notesDictionary.Key, notesDictionary.Value);
        }

        #region Notes CRUD

        private string AddNote(NoteModel model, EntityGenericType type)
        {
            int noteId = 0 ;
            switch (type)
            {
                case EntityGenericType.Job:
                    var jobNote = new JobNotes()
                    {
                        CreatedBy = model.CreatedBy,
                        CreatedOn = model.CreatedOn,
                        JobID = model.ParentId,
                        ModifiedBy = model.ModifiedBy,
                        ModifiedOn = model.ModifiedOn,
                        Note = model.Note
                    };
                    _jobNotesRepository.Add(jobNote);
                    _uow.Commit();
                    noteId = jobNote.ID;
                    break;

                case EntityGenericType.Entity:
                    var entityNote = new Notes()
                    {
                        CreatedBy = model.CreatedBy,
                        CreatedOn = model.CreatedOn,
                        EntityID = model.ParentId,
                        ModifiedBy = model.ModifiedBy,
                        ModifiedOn = model.ModifiedOn,
                        Note = model.Note
                    };
                    _notesRepository.Add(entityNote);
                    _uow.Commit();
                    noteId = entityNote.ID;
                    break;
            }
            var test = new GenericModel<int> { Value = 1};

            return ModelSerializer.GenerateReponseXml(new GenericModel<int>{Value = noteId}, ResponseStatus.Success, "Successful");
        }

        private string UpdateNote(NoteModel model, EntityGenericType type)
        {
            switch (type)
            {
                case EntityGenericType.Job:
                    var jobNote = _jobNotesRepository.GetById(model.ID);
                    jobNote.ModifiedBy = model.ModifiedBy;
                    jobNote.ModifiedOn = model.ModifiedOn;
                    jobNote.Note = model.Note;
                    _uow.Commit();
                    break;

                case EntityGenericType.Entity:

                    var entityNote = _notesRepository.GetById(model.ID);
                    entityNote.ModifiedBy = model.ModifiedBy;
                    entityNote.ModifiedOn = model.ModifiedOn;
                    entityNote.Note = model.Note;
                    _uow.Commit();
                    break;
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        private string DeleteNote(int id, EntityGenericType type)
        {
            switch (type)
            {
                case EntityGenericType.Job:
                    var jobNote = _jobNotesRepository.GetById(id);
                    _jobNotesRepository.Delete(jobNote);
                    _uow.Commit();
                    break;

                case EntityGenericType.Entity:
                    var entityNote = _notesRepository.GetById(id);
                    _notesRepository.Delete(entityNote);
                    _uow.Commit();
                    break;
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        private string GetNote(int id, EntityGenericType type)
        {
            var model = new NoteModel();
            switch (type)
            {
                case EntityGenericType.Job:
                    var jobNote = _jobNotesRepository.GetById(id);
                    model.ID = jobNote.ID;
                    model.ParentId = jobNote.JobID;
                    model.Type = EntityGenericType.Job;
                    model.Note = jobNote.Note;
                    break;

                case EntityGenericType.Entity:

                    var entityNote = _notesRepository.GetById(id);
                    model.ID = entityNote.ID;
                    model.ParentId = entityNote.EntityID;
                    model.Type = EntityGenericType.Entity;
                    model.Note = entityNote.Note;
                    
                    break;
            }

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        #endregion Notes CRUD
    }
}