﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Extensions;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;

using Common = DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class FirmComponent : Component
    {
        private UnitOfWork _uow;
        private IRepository<Entities> _repository;

        public FirmComponent()
        {
            _uow = new UnitOfWork();
            _repository = _uow.Repository<Entities>();
        }

        #region Get List

        public string GetAccounts(string xml)
        {
            var model = (SearchFilterModel)ModelDeserializer.DeserializeFromXml<SearchFilterModel>(xml);
            return GetEntities(Common.EntityTypes.Client, model);
        }

        public string GetFirms(string xml)
        {
            var model = (SearchFilterModel)ModelDeserializer.DeserializeFromXml<SearchFilterModel>(xml);
            return GetEntities(Common.EntityTypes.Entity, model);
        }

        public string GetFunds(string xml)
        {
            var model = (SearchFilterModel)ModelDeserializer.DeserializeFromXml<SearchFilterModel>(xml);
            return GetEntities(Common.EntityTypes.Fund, model);
        }

        public string GetEntities(Common.EntityTypes type, SearchFilterModel model)
        {
            var entity = _uow.Repository<AccountManagers>().GetById(model.UserID).Entity;
            var entities = GetChildEntities(type, entity);

            var query = ApplyFilter(entities, type, model);
            if (query.Any())
            {
                entities = query.ToList();

                var count = query.Count();
                //var query = ApplyFilter(_repository.GetAll(), type, model);
                //var count = query.Count();
                //var entities = count > 1000
                //                ? query.Take(1000).AsEnumerable()
                //                : query.AsEnumerable();

                //var list = entities.Where(a => CheckParent(a, model.UserID));

                var models = query.Select(a => ToModel(a)).ToList();

                return ModelSerializer.GenerateReponseXml<FirmModels>(models,
                    count > 1000 ? ResponseStatus.Warning : ResponseStatus.Success,
                    count > 100 ? count.ToString() : "Successful");
            }

            return ModelSerializer.GenerateReponseXml(new List<FirmModels>(), ResponseStatus.Success, "Successful");
        }

        private List<Entities> GetChildEntities(Common.EntityTypes type, Entities entity)
        {
            var list = new List<Entities>();

            if (type == Common.EntityTypes.Entity)
            {
                if ((Common.EntityTypes) entity.TypeID == Common.EntityTypes.Entity ||
                    (Common.EntityTypes) entity.TypeID == Common.EntityTypes.Group)
                    list.Add(entity);
            }
            else
            {
                if ((Common.EntityTypes) entity.TypeID == type)
                    list.Add(entity);
            }

            foreach (var item in entity.ChildEntities.Where(a => a.StatusID == Status.Active))
            {
                list.AddRange(GetChildEntities(type, item));
            }

            return list;
        }

        private FirmModels ToModel(Entities entity)
        {
            if (entity == null) return null;

            var model = new FirmModels
            {
                ID = entity.ID,
                Name = entity.Name,
                CreationDate = entity.CreatedOn,
                LastModifiedDate = entity.ModifiedOn,
                Type = (Common.EntityTypes) entity.TypeID,
                ABN = entity.ABN,
                Path = entity.GetFolderPath(),
                CategoryID = entity.CategoryID,
                HasChild =
                    (Common.EntityTypes) entity.TypeID == Common.EntityTypes.Fund
                        ? entity.Jobs.Any(a => a.StatusID == Status.Active)
                        : entity.ChildEntities.Any(a => a.StatusID == Status.Active)
            };
            if (model.Type == Common.EntityTypes.Fund)
            {
                model.Account = entity.ParentID != null ? entity.ParentEntity.Name : "";
                model.Firm = entity.ParentEntity.ParentEntity.Name;
                model.AccountManager = GetAccountManager(entity.ParentEntity);
            }
            else
            {
                model.Firm = entity.ParentID != null ? entity.ParentEntity.Name : "";
                model.AccountManager = GetAccountManager(entity);
            }

            return model;
        }

        private string GetAccountManager(Entities entity)
        {
            if (entity != null && entity.PrimaryAccountManagerID != null)
            {
                var manager = entity.AccountManagers.FirstOrDefault(a => a.ID == entity.PrimaryAccountManagerID);
                if (manager != null)
                    return manager.FirstName + " " + manager.LastName;
            }

            return "";
        }

        private IEnumerable<Entities> ApplyFilter(IEnumerable<Entities> entities, Common.EntityTypes type, SearchFilterModel model)
        {
            entities = entities.Where(a => a.StatusID == Status.Active);

            entities = type == Common.EntityTypes.Entity ? entities.Where(a => (Common.EntityTypes)a.TypeID == Common.EntityTypes.Entity || (Common.EntityTypes)a.TypeID == Common.EntityTypes.Group) : entities.Where(a => (Common.EntityTypes)a.TypeID == type);

            if (!string.IsNullOrEmpty(model.Name))
                entities = entities.Where(a => a.Name.ToLower().Contains(model.Name.ToLower()));

            if (!string.IsNullOrEmpty(model.ABN))
                entities = entities.Where(a => a.ABN != null && a.ABN.ToLower().Contains(model.ABN.ToLower()));

            if (model.DateFrom != null)
                entities = entities.Where(a => a.CreatedOn >= model.DateFrom.Value);

            if (model.DateTo != null)
                entities = entities.Where(a => a.CreatedOn <= model.DateTo.Value);

            if (!string.IsNullOrEmpty(model.AccountManager))
            {
                entities = from a in entities
                           join b in _uow.Repository<AccountManagers>().GetAll() on a.PrimaryAccountManagerID equals b.ID
                           where a.PrimaryAccountManagerID != null && (b.FirstName.ToLower().Contains(model.AccountManager.ToLower()) || b.LastName.ToLower().Contains(model.AccountManager.ToLower()))
                           select a;
            }

            if (!string.IsNullOrEmpty(model.PrimaryContact))
            {
                entities = from a in entities
                           join b in _uow.Repository<AccountManagers>().GetAll() on a.PrimaryAccountManagerID equals b.ID
                           where a.PrimaryAccountManagerID != null && (b.FirstName.ToLower().Contains(model.PrimaryContact.ToLower()) || b.LastName.ToLower().Contains(model.PrimaryContact.ToLower()))
                           select a;
            }

            if (model.EntityType == Common.EntityTypes.Fund)
            {
                if (!string.IsNullOrEmpty(model.Firm))
                    entities = entities.Where(a => a.ParentEntity.ParentEntity.Name.ToLower().Contains(model.Firm.ToLower()));

                if (!string.IsNullOrEmpty(model.Account))
                    entities = entities.Where(a => a.ParentEntity.Name.ToLower().Contains(model.Account.ToLower()));
            }
            if (model.EntityType == Common.EntityTypes.Client)
            {
                if (!string.IsNullOrEmpty(model.Firm))
                    entities = entities.Where(a => a.ParentEntity.Name.ToLower().Contains(model.Firm.ToLower()));
            }

            return entities;
        }

        public string HasMultipleAccess(string xml)
        {
            var model = (DictionaryModel<int, Common.EntityTypes>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, Common.EntityTypes>>(xml);
            return HasMultipleAccess(model.Key, model.Value);
        }

        private string HasMultipleAccess(int userId, Common.EntityTypes type)
        {
            var list = new List<int>();

            var entity = _uow.Repository<AccountManagers>().GetById(userId).Entity;

            if (type == Common.EntityTypes.Entity)
            {
                if ((Common.EntityTypes) entity.TypeID == Common.EntityTypes.Client)
                    entity = entity.ParentEntity;

                list.Add(entity.ID);
                list.AddRange(entity.ChildEntities.Where(
                    a =>
                        (Common.EntityTypes) a.TypeID == type ||
                        (Common.EntityTypes) a.TypeID == Common.EntityTypes.Group &&
                        a.StatusID == Status.Active).Select(a => a.ID).ToList());
            }
            if (type == Common.EntityTypes.Client)
            {
                if ((Common.EntityTypes) entity.TypeID == Common.EntityTypes.Client)
                    list.Add(entity.ID);

                if (list.Count == 0)
                {
                    list.AddRange(
                        entity.ChildEntities.Where(
                            a => (Common.EntityTypes) a.TypeID == type && a.StatusID == Status.Active)
                            .Select(a => a.ID)
                            .ToList());

                    if (list.Count == 0 &&
                        entity.ChildEntities.Count(a => ((Common.EntityTypes) a.TypeID == Common.EntityTypes.Entity ||
                                                         (Common.EntityTypes) a.TypeID == Common.EntityTypes.Group) &&
                                                        a.StatusID == Status.Active) > 0)
                    {
                        list = GetAccountCount(entity);
                    }
                }
            }
            if (type == Common.EntityTypes.Fund)
            {
                list = GetFundCount(entity);
            }

            var id = list.Count == 1 ? list[0] : 0; // Check if only one id exists in the list then return the id else return 0.

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> {Value = id},
                ResponseStatus.Success, "Successful");
        }

        private List<int> GetFundCount(Entities entity)
        {
            var list = new List<int>();

            foreach (
                var item in
                    entity.ChildEntities.Where(a => a.StatusID == Status.Active))
            {
                if ((Common.EntityTypes) item.TypeID == Common.EntityTypes.Fund)
                    list.Add(item.ID);

                if ((Common.EntityTypes) item.TypeID == Common.EntityTypes.Client)
                    list.AddRange(
                        item.ChildEntities.Where(a => (Common.EntityTypes) a.TypeID == Common.EntityTypes.Fund)
                            .Select(a => a.ID));

                if (list.Count > 1)
                    return list;

                if ((Common.EntityTypes) item.TypeID == Common.EntityTypes.Entity ||
                    (Common.EntityTypes) item.TypeID == Common.EntityTypes.Group)
                    list.AddRange(GetFundCount(item));
            }

            return list;
        }

        private List<int> GetAccountCount(Entities entity)
        {
            var list = new List<int>();

            var entities =
                entity.ChildEntities.Where(a => ((Common.EntityTypes) a.TypeID == Common.EntityTypes.Entity ||
                                                 (Common.EntityTypes) a.TypeID == Common.EntityTypes.Group) &&
                                                a.StatusID == Status.Active);
            foreach (var item in entities)
            {
                list.AddRange(
                    item.ChildEntities
                        .Where(
                            a =>
                                (Common.EntityTypes) a.TypeID == Common.EntityTypes.Client &&
                                a.StatusID == Status.Active)
                        .Select(a => a.ID)
                    );

                if (list.Count > 1)
                    return list;

                if (item.ChildEntities.Any(a => (Common.EntityTypes) a.TypeID == Common.EntityTypes.Entity ||
                                                (Common.EntityTypes) a.TypeID == Common.EntityTypes.Group &&
                                                a.StatusID == Status.Active))
                {
                    list.AddRange(GetAccountCount(item));
                }
            }

            return list;
        }

        public string GetParents(string xml)
        {
            var model = (DictionaryModel<Common.EntityTypes, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<Common.EntityTypes, int>>(xml);
            return GetParents(model.Key, model.Value);
        }

        private string GetParents(Common.EntityTypes type, int userId)
        {
            var entities = _repository.GetAll().Where(a => a.StatusID == Status.Active);
            entities = type == Common.EntityTypes.Entity ? entities.Where(a => (Common.EntityTypes)a.TypeID == Common.EntityTypes.Entity || (Common.EntityTypes)a.TypeID == Common.EntityTypes.Group) : entities.Where(a => (Common.EntityTypes)a.TypeID == type);
            
            var list = entities.AsEnumerable().Where(entity => CheckParent(entity, userId));

            var models = list.Select(a => new ParentFirmModel
            {
                ID = a.ID,
                Name = a.Name,
                Parent = a.ParentEntity != null ? a.ParentEntity.Name : "",
                Type = (Common.EntityTypes)a.TypeID,
                CategoryID = a.CategoryID
            }).ToList();

            return ModelSerializer.GenerateReponseXml<ParentFirmModel>(models, ResponseStatus.Success, "Successful");
        }

        private bool CheckParent(Entities entity, int userId)
        {
            if ((Common.EntityTypes)entity.TypeID == Common.EntityTypes.Fund)
                return CheckParent(entity.ParentEntity, userId);

            if (entity.AccountManagers.Any(a => a.ID == userId))
                return true;

            if (entity.ParentEntity != null)
                return CheckParent(entity.ParentEntity, userId);

            return false;
        }

        #endregion Get List

        #region Get Detail

        public string GetAccountDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetEntityDetail(model.Value, Common.EntityTypes.Client);
        }

        public string GetFirmDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetEntityDetail(model.Value, Common.EntityTypes.Entity);
        }

        public string GetGroupDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetEntityDetail(model.Value, Common.EntityTypes.Group);
        }

        public string GetFundDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetEntityDetail(model.Value, Common.EntityTypes.Fund);
        }

        private string GetEntityDetail(int id, Common.EntityTypes type)
        {
            var repository = _uow.Repository<Entities>();
            var entity = repository.GetById(id);
            if ((Common.EntityTypes)entity.TypeID != type)
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Not found");
            var model = GetEntityDetail(entity, type);

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        private FirmDetailModels GetEntityDetail(Entities entity, Common.EntityTypes type)
        {
            var userRepository = _uow.Repository<AccountManagers>();

            #region Main Model

            var model = new FirmDetailModels
            {
                ABN = entity.ABN,
                ACN = entity.ACN,
                GST = entity.GST,
                TFN = entity.TFN,
                ID = entity.ID,
                Name = entity.Name,
                ParentName = entity.ParentID == null
                    ? ""
                    : (type == Common.EntityTypes.Fund ? entity.ParentEntity.ParentEntity.Name : entity.ParentEntity.Name),
                Type = (Common.EntityTypes) entity.TypeID,
                FolderPath = entity.GetFolderPath(),
                ManagedAccount = type == Common.EntityTypes.Fund ? entity.ParentEntity.Name : "",
                HasChild =
                    (Common.EntityTypes) entity.TypeID == Common.EntityTypes.Fund
                        ? entity.Jobs.Any(a => a.StatusID == Status.Active)
                        : entity.ChildEntities.Any(a => a.StatusID == Status.Active),
                Category = entity.EntityCategory != null ? entity.EntityCategory.Description : ""
            };

            if (model.Type != Common.EntityTypes.Entity)
                model.Category = model.Type == Common.EntityTypes.Fund ? "Fund" : "Account";

            #endregion Main Model

            #region Addresses

            if (entity.Addresses.Any(a => a.StatusID == Status.Active))
            {
                model.Addresses = entity.Addresses.Where(q => q.StatusID == Status.Active).Select(a => new AddressModel
                {
                    ID = a.ID,
                    City = a.City,
                    Country = a.CountryID != null ? a.Country.Name : "",
                    State = a.StateID != null? a.State.Name: "",
                    PostCode = a.PostCode,
                    StreetName = a.StreetName,
                    StreetNumber = a.StreetNumber,
                    UnitNumber = a.UnitNumber,
                    Type = a.AddressType.Description,
                    StreetType = a.StreetType,
                    Default = a.Default
                }).ToList();
            }

            #endregion Addresses

            #region Notes

            if (entity.Notes.Any())
            {
                model.Notes = entity.Notes.Select(a => new NotesViewModel
                {
                    Note = a.Note,
                    Date = a.ModifiedOn ?? a.CreatedOn,
                    Id = a.ID,
                    CreatedBy = userRepository.GetAll().Where(p => p.ID == (a.ModifiedBy ?? a.CreatedBy)).Select(q => q.FirstName + " " + q.LastName).FirstOrDefault()
                }).ToList();
            }

            #endregion Notes

            #region Users

            if (type != Common.EntityTypes.Group && type != Common.EntityTypes.Fund)
            {
                if (entity.AccountManagers.Any(a => a.StatusID == Status.Active))
                {
                    model.AccountManagers = entity.AccountManagers.Select(a => new AccountManagerDetailModel
                    {
                        Email = a.Email,
                        FirstName = a.FirstName,
                        ID = a.ID,
                        LastName = a.LastName,
                        MidName = a.MidName,
                        ListContactInfo = a.Telephones.Any(b => b.StatusID == Status.Active) ?
                                            a.Telephones.Select(x => new ContactModel
                                            {
                                                AreaCode = x.AreaCode,
                                                CountryCode = x.CountryCode,
                                                ID = x.ID,
                                                PhoneNumber = x.PhoneNumber,
                                                Type = x.TelephoneType.Name
                                            }).ToList() : null,
                        UserDetail = a.User != null ? new UserDetailModel
                                    {
                                        Email = a.User.Email,
                                        Group = a.User.Group.Title,
                                        ID = a.User.ID,
                                    } : null
                    }).ToList();
                }
            }

            #endregion Users

            return model;
        }

        public string GetFirm(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (model != null)
                return GetFirm(model.Value);

            return NullModelReference();
        }

        private string GetFirm(int id)
        {
            var repository = _uow.Repository<Entities>();
            var entity = repository.GetById(id);

            if (entity != null)
            {
                var model = new FirmCreateModel
                {
                    ABN = entity.ABN,
                    ACN = entity.ACN,
                    GST = entity.GST,
                    ID = entity.ID,
                    Name = entity.Name,
                    Note = entity.Note,
                    TFN = entity.TFN,
                    CategoryID = entity.CategoryID == null ? 0 : (int)entity.CategoryID,
                    EntityType = (Common.EntityTypes)entity.TypeID,
                    ParentID = entity.ParentID,
                    PrimaryAccountMangerID = entity.PrimaryAccountManagerID,
                    ListAddress = entity.Addresses.Any(a => a.StatusID == Status.Active) ?
                                    entity.Addresses.Select(a => new AddressCreateModel
                                    {
                                        CountryID = a.CountryID,
                                        StateID = a.StateID,
                                        TypeID = a.TypeID,
                                        City = a.City,
                                        Country = a.CountryID != null ? a.Country.Name : "",
                                        Default = a.Default,
                                        ID = a.ID,
                                        PostCode = a.PostCode,
                                        State = a.StateID != null ? a.State.Name : "",
                                        StreetName = a.StreetName,
                                        StreetNumber = a.StreetNumber,
                                        StreetType = a.StreetType,
                                        Type = a.AddressType.Description,
                                        UnitNumber = a.UnitNumber,
                                        StatusID = a.StatusID
                                    }).ToList() : null
                };

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_EntityNotFound);
        }

        #endregion Get Detail

        #region CRUD

        #region Add Entity

        public string AddAccount(string xml)
        {
            return AddFirm(xml);
        }

        public string AddFirm(string xml)
        {
            var model = (FirmCreateModel)ModelDeserializer.DeserializeFromXml<FirmCreateModel>(xml);
            if (model != null)
            {
                string response = AddEntity(model);
                return response;
            }

            return NullModelReference();
        }

        public string AddFund(string xml)
        {
            return AddFirm(xml);
        }

        public string AddGroup(string xml)
        {
            return AddFirm(xml);
        }

        private void AddAccountManager(Entities entity, AccountManagerCreateModel model)
        {
            if (_uow.Repository<AccountManagers>().Any(a => a.Email == model.Email))
                return;

            var identifier = Guid.NewGuid();

            var manager = new AccountManagers
            {
                EntityID = model.EntityID,
                CreatedBy = model.CreatedBy,
                CreatedOn = model.CreatedOn,
                ModifiedBy = model.CreatedBy,
                ModifiedOn = model.CreatedOn,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                MidName = model.MidName,
                StatusID = Status.Active,
                Title = model.Title,
                Identifier = identifier,
                User = model.User == null ? null :
                        new Users
                        {
                            CreatedBy = model.CreatedBy,
                            CreatedOn = model.CreatedOn,
                            GroupID = model.User.GroupID,
                            Email = model.Email,
                            Password = model.User.Password,
                            StatusID = Status.Active,
                            Identifier = identifier,
                        }
            };

            if (model.ListContactInfo != null && model.ListContactInfo.Any())
            {
                foreach (var contact in model.ListContactInfo)
                {
                    var telephone = new Telephones
                    {
                        AccountManagerID = manager.ID,
                        AreaCode = contact.AreaCode,
                        CountryCode = contact.CountryCode,
                        PhoneNumber = contact.PhoneNumber,
                        StatusID = Status.Active,
                        TypeID = contact.TypeID
                    };
                    manager.Telephones.Add(telephone);
                }
            }

            entity.AccountManagers.Add(manager);
        }

        private void AddAddress(Entities entity, AddressCreateModel address)
        {
            entity.Addresses.Add(new Addresses
            {
                City = address.City,
                CountryID = address.CountryID,
                StateID = address.StateID,
                CreatedBy = address.CreatedBy,
                CreatedOn = address.CreatedOn,
                EntityID = entity.ID,
                PostCode = address.PostCode,
                StatusID = Status.Active,
                StreetName = address.StreetName,
                StreetNumber = address.StreetNumber,
                StreetType = address.StreetType,
                TypeID = address.TypeID,
                UnitNumber = address.UnitNumber,
                Default = address.Default
            });
        }

        private void AddAddressesOnCreateEntity(FirmCreateModel model, Entities entity)
        {
            if (model.ListAddress != null && model.ListAddress.Any())
            {
                foreach (var address in model.ListAddress)
                {
                    AddAddress(entity, address);
                }
            }
        }

        private string AddEntity(FirmCreateModel model)
        {
            try
            {
                var status = ResponseStatus.Success;
                var message = ValidateEntityOnCreate(model, ref status);

                if (status == ResponseStatus.Success)
                {
                    var entity = AddNewEntity(model);

                    AddAddressesOnCreateEntity(model, entity);

                    if (model.ListUser != null && model.ListUser.Any(a => a.Status == Status.Active && a.ID < 0))
                    {
                        foreach (var user in model.ListUser.Where(a => a.Status == Status.Active && a.ID < 0))
                            AddAccountManager(entity, user);
                    }

                    _uow.Commit();
                    _uow = new UnitOfWork();
                    entity = _uow.Repository<Entities>().GetById(entity.ID);
                    model.ID = entity.ID;

                    if (entity.AccountManagers != null && entity.AccountManagers.Any())
                    {
                        foreach (var user in entity.AccountManagers)
                        {
                            if (model.ListUser != null)
                            {
                                var u = model.ListUser.FirstOrDefault(a => a.Email == user.Email);
                                if (u != null)
                                    u.ID = user.ID;
                            }
                        }
                    }

                    UpdatePrimaryAccountManager(entity.ID, model.ListUser);

                    if (model.EntityType == Common.EntityTypes.Entity)
                        new NotificationComponent().InsertNotification(NotificationTypes.Info, "New firm " + model.Name + " registered", null, "Entity", entity.ID, false, model.CreatedBy);
                    else if (model.EntityType == Common.EntityTypes.Client)
                        new NotificationComponent().InsertNotification(NotificationTypes.Info, "New account " + model.Name + " registered", null, "Client", entity.ID, false, model.CreatedBy);
                    var responseModel = GetEntityDetail(entity, model.EntityType);
                    var resposne = CreateEntityFolder(entity, entity.CreatedBy);

                    if (resposne.Status == ResponseStatus.Failure)
                    {
                        _uow.Dispose();
                        return ModelSerializer.GenerateReponseXml(responseModel, ResponseStatus.Warning, resposne.StatusMessage);
                    }

                    return ModelSerializer.GenerateReponseXml(responseModel, status, message);
                }

                return ModelSerializer.GenerateReponseXml(null, status, message);
            }
            catch (DbEntityValidationException dbEx)
            {
                StringBuilder errorDetail = new StringBuilder();
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorDetail.Append(String.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage));
                        Console.WriteLine(@"Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new Exception(errorDetail.ToString());
            }
        }

        private Entities AddNewEntity(FirmCreateModel model)
        {
            IRepository<Entities> repository = _uow.Repository<Entities>();

            #region Client Detail and fund Detail Deprecated

            //ClientDetail clientDetail = null;
            //FundDetail fundDetail = null;
            //if (model.EntityType == Common.EntityTypes.Client)
            //{
            //    clientDetail = new ClientDetail()
            //    {
            //        Contract = new Data.Models.Contracts() { ID = model.Clientdetail.ContractID, Description = model.Clientdetail.Contract.Description, StatusID = model.Clientdetail.Contract.StatusID },
            //        ID = model.ID
            //    };
            //    IRepository<ClientDetail> repositoryCD = uow.Repository<ClientDetail>();
            //    repositoryCD.Add(clientDetail);
            //}
            //else if (model.EntityType == Common.EntityTypes.Fund)
            //{
            //    fundDetail = new FundDetail()
            //    {
            //        ID = model.ID,
            //        IsDirectDebit = model.FundDetail.IsDirectDebit,
            //        IsPreviousClient = model.FundDetail.IsPreviousClient,
            //        MainContactID = model.FundDetail.MainContactID,
            //        OwnerID = model.FundDetail.OwnerID,
            //        TrusteeID = model.FundDetail.TrusteeID,
            //        CreatedBy = model.CreatedBy,
            //        CreatedOn = model.CreatedOn
            //    };
            //    IRepository<FundDetail> repositoryFD = uow.Repository<FundDetail>();
            //    repositoryFD.Add(fundDetail);
            //}

            #endregion Client Detail and fund Detail Deprecated

            var entity = new Entities
            {
                ABN = model.ABN,
                ACN = model.ACN,
                CreatedBy = model.CreatedBy,
                CreatedOn = model.CreatedOn,
                ModifiedBy = model.CreatedBy,
                ModifiedOn = model.CreatedOn,
                GST = model.GST,
                Name = model.Name,
                Note = model.Note,
                ParentID = model.ParentID,
                StatusID = Status.Active,
                TFN = model.TFN,
                CategoryID = model.CategoryID == 0 ? (int?)null : model.CategoryID,
                TypeID = (int)model.EntityType,
                PrimaryAccountManagerID = model.PrimaryAccountMangerID,
                Identifier = Guid.NewGuid()
            };
            repository.Add(entity);
            return entity;
        }

        private ResponseModel CreateEntityFolder(Entities entity, int userId)
        {
            var folderList = entity.GetFolderHierarchy();
            string folderName = folderList[0];
            folderList.RemoveAt(0);
            folderList.Reverse();
            string rootFolder = String.Join("/", folderList);
            return new DocumentComponent(userId).AddFolder(folderName, rootFolder);
        }

        private string ValidateEntityOnCreate(FirmCreateModel model, ref ResponseStatus status)
        {
            IRepository<Entities> repository = _uow.Repository<Entities>();
            var message = "Successful";

            if (!string.IsNullOrEmpty(model.ABN) && repository.Any(a => (Common.EntityTypes)a.TypeID == model.EntityType && a.ABN == model.ABN && a.StatusID == Status.Active))
            {
                status = ResponseStatus.Failure;
                message = Resource_EN.Error_ABNAlreadyExists;
            }
            else if (repository.GetAll().Where(p => p.Name == model.Name && p.ParentID == model.ParentID && p.StatusID == Status.Active).Any())
            {
                status = ResponseStatus.Failure;
                message = Resource_EN.Error_EntityAlreadyExist;
                return message;
            }
            return message;
        }

        #endregion Add Entity

        #region Update

        #region Update Entity

        private string UpdateEntity(FirmCreateModel model)
        {
            var status = ResponseStatus.Success;
            var message = ValidateEntityOnUpdate(model, ref status);
            var responseModel = new FirmDetailModels();
            if (status == ResponseStatus.Success)
            {
                var entity = _repository.GetById(model.ID);

                bool renameFolder = entity.Name != model.Name;
                string oldFolderName = renameFolder ? entity.Name : "";

                entity.ABN = model.ABN;
                entity.ACN = model.ACN;
                entity.ModifiedBy = model.ModifiedBy;
                entity.ModifiedOn = model.ModifiedOn;
                entity.GST = model.GST;
                entity.ModifiedBy = model.ModifiedBy;
                entity.ModifiedOn = model.ModifiedOn;
                entity.Name = model.Name;
                entity.Note = model.Note;
                entity.TFN = model.TFN;
                entity.CategoryID = model.CategoryID == 0 ? (int?)null : model.CategoryID;

                UpdateAddressOnUpdateEntity(model.ListAddress, entity);

                if (model.ListUser != null && model.ListUser.Any())
                {
                    if (model.ListUser.Any(a => a.Status == Status.Active && a.ID < 0)) // New managers
                    {
                        foreach (var user in model.ListUser.Where(a => a.Status == Status.Active && a.ID < 0))
                            AddAccountManager(entity, user);
                    }
                    else if (model.ListUser.Any(a => a.ID > 0)) // Existing managers to be updated
                    {
                        foreach (var user in model.ListUser.Where(a => a.ID > 0))
                            UpdateAccountManager(user);
                    }
                }

                _uow.Commit();

                if (entity.AccountManagers != null && entity.AccountManagers.Any())
                {
                    foreach (var user in entity.AccountManagers)
                    {
                        if (model.ListUser != null)
                        {
                            var u = model.ListUser.FirstOrDefault(a => a.Email == user.Email);
                            if (u != null)
                                u.ID = user.ID;
                        }
                    }
                }

                UpdatePrimaryAccountManager(entity.ID, model.ListUser);

                if (entity.ID > 0)
                    status = ResponseStatus.Success;
                else
                {
                    status = ResponseStatus.Failure;
                    message = Resource_EN.Error_UnexpectedErrorOccured;
                }
                _uow.Dispose();
                _uow = new UnitOfWork();
                _repository = _uow.Repository<Entities>();
                entity = _repository.GetById(model.ID);
                responseModel = GetEntityDetail(entity, model.EntityType);
                _uow.Dispose();
                if (renameFolder)
                {
                    var response = RenameEntityFolder(entity, oldFolderName, model.ModifiedBy == null ? model.CreatedBy : (int)model.ModifiedBy);
                    if (response.Status == ResponseStatus.Failure)
                        return ModelSerializer.GenerateReponseXml(responseModel, ResponseStatus.Warning, response.StatusMessage);
                }
            }

            if (message != "Successful")
            {
                status = ResponseStatus.Failure;
            }

            return ModelSerializer.GenerateReponseXml(responseModel, status, message);
        }

        public string UpdateFirm(string xml)
        {
            var model = (FirmCreateModel)ModelDeserializer.DeserializeFromXml<FirmCreateModel>(xml);
            return model != null ? UpdateEntity(model) : NullModelReference();
        }

        public string UpdateGroup(string xml)
        {
            return UpdateFirm(xml);
        }

        public string UpdateAccount(string xml)
        {
            return UpdateFirm(xml);
        }

        public string UpdateFund(string xml)
        {
            return UpdateFirm(xml);
        }

        public string ValidateEntityOnUpdate(FirmCreateModel model, ref ResponseStatus status)
        {
            var message = "Successful";

            if (!string.IsNullOrEmpty(model.ABN) && _repository.Any(a => a.ID != model.ID && (Common.EntityTypes)a.TypeID == model.EntityType && a.ABN == model.ABN && a.StatusID == Status.Active))
            {
                status = ResponseStatus.Failure;
                message = Resource_EN.Error_ABNAlreadyExists;
            }

            return message;
        }

        public void UpdateAddressOnUpdateEntity(List<AddressCreateModel> listAddress, Entities entity)
        {
            if (listAddress != null)
            {
                foreach (var address in listAddress.Where(a => a.StatusID == Status.Deleted).ToList())
                {
                    entity.Addresses.Single(a => a.ID == address.ID).StatusID = Status.Deleted;
                }

                foreach (var address in listAddress.Where(a => a.ID > 0 && a.StatusID == Status.Active).ToList())
                {
                    var addressEntity = entity.Addresses.First(a => a.ID == address.ID);

                    addressEntity.City = address.City;
                    addressEntity.CountryID = address.CountryID;
                    addressEntity.StateID = address.StateID;
                    addressEntity.ModifiedBy = address.ModifiedBy;
                    addressEntity.ModifiedOn = address.ModifiedOn;
                    addressEntity.PostCode = address.PostCode;
                    addressEntity.StreetName = address.StreetName;
                    addressEntity.StreetNumber = address.StreetNumber;
                    addressEntity.StreetType = address.StreetType;
                    addressEntity.TypeID = address.TypeID;
                    addressEntity.UnitNumber = address.UnitNumber;
                    addressEntity.Default = address.Default;
                }

                foreach (var address in listAddress.Where(a => a.ID < 0 && a.StatusID == Status.Active).ToList())
                {
                    entity.Addresses.Add(new Addresses
                    {
                        City = address.City,
                        CountryID = address.CountryID,
                        StateID = address.StateID,
                        CreatedBy = address.CreatedBy,
                        CreatedOn = address.CreatedOn,
                        EntityID = entity.ID,
                        PostCode = address.PostCode,
                        StatusID = Status.Active,
                        StreetName = address.StreetName,
                        StreetNumber = address.StreetNumber,
                        StreetType = address.StreetType,
                        TypeID = address.TypeID,
                        UnitNumber = address.UnitNumber,
                        Default = address.Default
                    });
                }
            }
        }

        private void UpdateAccountManager(AccountManagerCreateModel model)
        {
            if (_uow.Repository<AccountManagers>().Any(a => a.ID != model.ID && a.Email == model.Email))
                return;

            var manager = _uow.Repository<AccountManagers>().GetById(model.ID);

            if (manager != null)
            {
                manager.ModifiedBy = model.ModifiedBy;
                manager.ModifiedOn = model.ModifiedOn;
                manager.EntityID = model.EntityID;
                manager.Email = model.Email;
                manager.FirstName = model.FirstName;
                manager.LastName = model.LastName;
                manager.MidName = model.MidName;
                manager.StatusID = model.Status;

                #region Contact information

                if (model.ListContactInfo != null)
                {
                    foreach (var contact in model.ListContactInfo.Where(a => a.StatusID == Status.Deleted).ToList())
                        manager.Telephones.Remove(_uow.Repository<Telephones>().GetById(contact.ID));

                    foreach (var contact in model.ListContactInfo.Where(a => a.ID > 0 && a.StatusID == Status.Active).ToList())
                    {
                        var contactEntity = _uow.Repository<Telephones>().GetById(contact.ID);

                        contactEntity.AreaCode = contact.AreaCode;
                        contactEntity.CountryCode = contact.CountryCode;
                        contactEntity.PhoneNumber = contact.PhoneNumber;
                        contactEntity.TypeID = contact.TypeID;
                    }

                    foreach (var contact in model.ListContactInfo.Where(a => a.ID < 0 && a.StatusID == Status.Active).ToList())
                    {
                        _uow.Repository<Telephones>().Add(new Telephones
                        {
                            AccountManagerID = manager.ID,
                            AreaCode = contact.AreaCode,
                            CountryCode = contact.CountryCode,
                            PhoneNumber = contact.PhoneNumber,
                            StatusID = Status.Active,
                            TypeID = contact.TypeID
                        });
                    }
                }

                #endregion Contact information

                #region User

                if (model.User != null)
                {
                    if (manager.User == null)
                    {
                        manager.User = new Users();
                        manager.User.CreatedOn = model.ModifiedOn.Value;
                        manager.User.CreatedBy = model.ModifiedBy.Value;
                        manager.User.StatusID = Status.Active;
                        manager.User.Password = model.User.Password;
                    }
                    else
                    {
                        manager.User.ModifiedBy = model.ModifiedBy;
                        manager.User.ModifiedOn = model.ModifiedOn;
                    }

                    manager.User.StatusID = model.Status;
                    manager.User.Email = model.Email;
                    manager.User.GroupID = model.User.GroupID;
                }
                else
                {
                    if (manager.User != null)
                        _uow.Repository<Users>().Delete(manager.User);
                }

                #endregion User
            }
        }

        #endregion Update Entity

        private ResponseModel RenameEntityFolder(Entities entity, string oldName, int userID)
        {
            var folderList = entity.GetFolderHierarchy();
            string folderName = folderList[0];
            folderList.RemoveAt(0);
            folderList.Reverse();
            string rootFolder = String.Join("/", folderList);
            return new DocumentComponent(userID).RenameFolder(oldName, folderName, rootFolder);
        }

        #endregion Update

        #region Delete

        public string DeleteFirm(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            return model != null ? DeleteFirm(model.Key, model.Value) : NullModelReference();
        }

        private string DeleteFirm(int id, int userId)
        {
            var entity = _repository.GetById(id);

            if (entity != null)
            {
                entity.StatusID = Status.Deleted;
                entity.ModifiedBy = userId;
                entity.ModifiedOn = DateTime.Now;
                DeleteEntityFolder(entity, userId);
                _uow.Commit();

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
            }

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, Resource_EN.Error_UnableToDelete);
        }

        private void DeleteEntityFolder(Entities entity, int userId)
        {
            var folderList = entity.GetFolderHierarchy();
            var folderName = folderList[0];
            folderList.RemoveAt(0);
            folderList.Reverse();
            var rootFolder = String.Join("/", folderList);
            new DocumentComponent(userId).DeleteFolder(rootFolder, folderName);
        }

        #endregion Delete

        #endregion CRUD

        #region Common Functions

        private void UpdatePrimaryAccountManager(int entityId, List<AccountManagerCreateModel> listUser)
        {
            if (listUser != null && listUser.Any(a => a.Status == Status.Active))
            {
                var u = listUser.FirstOrDefault(a => a.Selected);
                if (u != null)
                {
                    var manager = _uow.Repository<AccountManagers>().GetAll().FirstOrDefault(a => a.FirstName == u.FirstName && a.LastName == u.LastName && a.EntityID == entityId);
                    if (manager != null)
                    {
                        var e = _repository.GetById(entityId);
                        e.PrimaryAccountManagerID = manager.ID;
                        _uow.Commit();
                    }
                }
            }
        }

        #endregion Common Functions
        
        #region Entity Category

        public string GetFirmCategories(string xml)
        {
            var model = (GenericModel<int?>)ModelDeserializer.DeserializeFromXml<GenericModel<int?>>(xml);
            return this.GetFirmCategories(model.Value);
        }

        private string GetFirmCategories(int? id)
        {
            List<DictionaryModel<int, string>> types;

            if (id == null)
            {
                types = _uow.Repository<DBASystem.SMSF.Data.Models.EntityCategories>().GetAll().Where(a => a.StatusID == Status.Active && a.ID > 1)
                                                    .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();
            }
            else
            {
                var entity = _repository.GetById((int)id);
                if (entity.ParentID == null)
                    types = _uow.Repository<DBASystem.SMSF.Data.Models.EntityCategories>().GetAll().Where(a => a.StatusID == Status.Active)
                                                        .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();
                else
                    types = _uow.Repository<DBASystem.SMSF.Data.Models.EntityCategories>().GetAll().Where(a => a.StatusID == Status.Active && a.ID > 1)
                                                        .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();
            }

            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(types, ResponseStatus.Success, "Successful");
        }

        #endregion Entity Category
    }
}