﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using DBASystem.SMSF.Service.Business.WorkflowHelper.Common;
using DBASystem.SMSF.Service.Business.WorkflowHelper.Workflow;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class WorkflowComponent : Component
    {
        private UnitOfWork uow = new UnitOfWork();

        public string GetWorkflows(string xml)
        {
            return GetAllWorkflows();
        }

        private string GetAllWorkflows()
        {
            var workflows = uow.Repository<Workflow>().GetAll().Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Name }).ToList();

            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(workflows, ResponseStatus.Success, "Successful");
        }

        public string GetWorkflowTemplate(string xml)
        {
            var model = (WorkflowCreateModel)ModelDeserializer.DeserializeFromXml<WorkflowCreateModel>(xml);
            return GetWorkflowTemplate(model.FirmId, model.TemplateId, model.WorkflowTypeId);
        }

        private string GetWorkflowTemplate(int entityId, int workflowTempleteId, int workflowTypeId)
        {
            var model = new WorkflowTemplateModel();
            var workflowProcess = new Process();
            var selectedWorkflow = uow.Repository<WorkflowMap>().GetById(workflowTempleteId);
            workflowProcess.Load(selectedWorkflow.Workflow.Scheme);


            model.EntityId = entityId;
            model.Actors = uow.Repository<Actors>().GetAll().Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Name }).ToList();
            model.Activities = uow.Repository<Activities>().GetAll().Where(a => a.WorkflowID == workflowTypeId).Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Name }).ToList();
            model.RestrictionTypes = ((TransitionRestrictionType[])Enum.GetValues(typeof(TransitionRestrictionType))).Select(a => new DictionaryModel<int, string> { Key = (int)a, Value = a.ToString() }).ToList();
            model.AvailableCommands = uow.Repository<Commands>().GetAll().Where(p => !p.EntityId.HasValue || p.EntityId == 1).Select(a => new TransitionCommandModel() { ID = a.ID, Name = a.Name }).ToList();
            model.AvailableTransitions = uow.Repository<Transitions>().GetAll()
                                                                      .Where(a => a.WorkflowID == workflowTypeId && (!a.EntityId.HasValue || a.EntityId == entityId)).AsEnumerable()
                                                                      .Select(a => new WorkflowTransitionsModel
                                                                      {
                                                                          CommandType = a.CommandType,
                                                                          FromActivity =  a.FromActivity.Name,
                                                                          FromActivityID = a.FromActivityID,
                                                                          ID = a.ID,
                                                                          Name = a.Name,
                                                                          ToActivity =  a.ToActivity.Name ,
                                                                          ToActivityID = a.ToActivityID,
                                                                          Restrictions = a.CommandType == TransitionCommandType.Auto ? null :
                                                                                            a.TransitionActors.Select(q => new WorkflowRestrictionsModel
                                                                                                                          {
                                                                                                                              ActorID = q.ActorID,
                                                                                                                              Actor = q.Actor.Name,
                                                                                                                              ID = q.ID,
                                                                                                                              TypeID = (int)q.RestrictionType,
                                                                                                                              Type = q.RestrictionType.ToString()
                                                                                                                          }).ToList()
                                                                      }).ToList();

          


            model.Transitions = workflowProcess.Transitions.Select(a => new WorkflowTransitionsModel
            {
                CommandType =
                    (TransitionCommandType) Enum.Parse(typeof (TransitionCommandType), a.Triggers.Trigger.Type, true),
                FromActivity = a.From,
                FromActivityID = a.FromActivityID.ToType<int>(), 
                ID = a.ID.ToType<int>(),
                Name = a.Name,
                ToActivity = a.To,
                ToActivityID = a.ToActivityID.ToType<int>(), 
                Restrictions = a.Restrictions.Select(q => new WorkflowRestrictionsModel
                {
                    ActorID = q.ActorID.ToType<int>(),
                    Actor = q.NameRef,
                    ID = q.ID.ToType<int>(),
                    TypeID = q.TypeID.ToType<int>(),
                    Type = q.Type
                }).ToList()
            }).ToList();
            model.Name = selectedWorkflow.Name;
            model.Id = selectedWorkflow.ID;
            model.IsSystemDefault = !selectedWorkflow.EntityId.HasValue;
            model.WorkflowTypeId = selectedWorkflow.WorkFlowTypeId;
            model.IsDefault = selectedWorkflow.IsActive && selectedWorkflow.EntityId.HasValue; //workflowsSelect.Count() == 1 || workflowsSelect.Any(p => p.ID != model.ID && p.IsActive);
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        public string GetWorkflowPendingProcesses(string xml)
        {
            var model = (DictionaryModel<Guid, Guid>)ModelDeserializer.DeserializeFromXml<DictionaryModel<Guid, Guid>>(xml);
            return GetWorkflowPendingProcesses(model.Key, model.Value);
        }

        private string GetWorkflowPendingProcesses(Guid userId, Guid groupId)
        {
            var models = new List<WorkflowPendingProcessModel>();
            var clientUser = uow.Repository<Users>().GetAll().FirstOrDefault(p => p.Identifier == userId && p.AccountManager.Entity.EntityType.ID == (int)DBASystem.SMSF.Contracts.Common.EntityTypes.Client);
            if (clientUser == null)
            {
                var jobList = (
                           from a in uow.WorkflowRepository<WorkflowTransitionUsers>().GetAll()
                           join b in uow.WorkflowRepository<WorkflowTransactionHistory>().GetAll() on a.TransactionId equals b.Id
                           join c in uow.Repository<AccountManagerJobs>().GetAll() on b.ProcessIdentity equals c.Job.Identifier

                           where c.AccountManager.User.Identifier == userId
                                 && c.JobRole.Identifier == a.UserID
                                 && b.TransitionTime == null
                                 && b.Command != "Auto"
                               && b.WorkflowId == (int)Workflows.JobProcess
                           group b by new { b.ProcessIdentity, b.WorkflowId, b.WorkFlow.Name, c.Job.Title } into g
                           select new WorkflowPendingProcessModel
                             {
                                 Name = g.Key.Title,
                                 ProcessId = g.Key.ProcessIdentity,
                                 WorkflowName = g.Key.Name,
                                 WorkflowTypeId = g.Key.WorkflowId
                             }
                         ).ToList();
                models.AddRange(jobList);
            }
            else
            {
                var jobList = (
                           from a in uow.WorkflowRepository<WorkflowTransitionUsers>().GetAll()
                           join b in uow.WorkflowRepository<WorkflowTransactionHistory>().GetAll() on a.TransactionId equals b.Id
                           join c in uow.Repository<Jobs>().GetAll() on b.ProcessIdentity equals c.Identifier

                           where c.Fund.ParentID == clientUser.AccountManager.EntityID
                                 && b.TransitionTime == null
                                 && b.Command != "Auto"
                                 && b.WorkflowId == (int)Workflows.JobProcess
                                 && a.UserID == new Guid("D2BE6DFA-27E1-4DAE-A79A-496FE899EE95")
                           group b by new { b.ProcessIdentity, b.WorkflowId, b.WorkFlow.Name, c.Title } into g
                           select new WorkflowPendingProcessModel
                    {
                        Name = g.Key.Title,
                        ProcessId = g.Key.ProcessIdentity,
                        WorkflowName = g.Key.Name,
                        WorkflowTypeId = g.Key.WorkflowId
                    }
                         ).ToList();
                models.AddRange(jobList);
            }

            return ModelSerializer.GenerateReponseXml<WorkflowPendingProcessModel>(models.Any() ? models : null, ResponseStatus.Success, "Successful");
        }

        public string GetWorkflowPendingProcessDetail(string xml)
        {
            var model = (WorkflowPendingProcessViewModel)ModelDeserializer.DeserializeFromXml<WorkflowPendingProcessViewModel>(xml);
            var type = (Workflows)model.TypeId;

            //All view model which are integrated with workflow does not have
            //identifier(Guid) , handling int identy here

            return model.ProcessIdentity != 0
                ? GetWorkflowPendingProcessDetail(type, model.ProcessIdentity, model.userId)
                : GetWorkflowPendingProcessDetail(type, model.ProcessId, model.userId);
        }

        private string GetWorkflowPendingProcessDetail(Workflows type, Guid processId, Guid userId)
        {
            var workflow = WorkflowInit.GetWorkFlowInstance(type, processId);
            var activityUrl = GetActivtyUrl(workflow.CurrentActivity, type, processId);
            var model = new WorkflowPendingProcessDetailModel();

            if (!String.IsNullOrEmpty(activityUrl))
            {
                model.ViewUrl = activityUrl;
                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
            }

            var commands = workflow.GetAvailableCommands(userId).Select(a => new DictionaryModel<string, string> { Key = a.Key, Value = a.Value }).ToList();

            model.Commands = commands;

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        private string GetWorkflowPendingProcessDetail(Workflows type, int processIdentity, Guid userId)
        {
            WorkflowInit workflow = null;
            var model = new WorkflowPendingProcessDetailModel();
            var responseStatus = ResponseStatus.Success;
            var responseMessage = "Successful";
            switch (type)
            {
                case Workflows.JobProcess:
                    var jobIdentifier = uow.Repository<Jobs>().GetById(processIdentity).Identifier;

                    if (jobIdentifier != null)
                    {
                        workflow = WorkflowInit.GetWorkFlowInstance(type, (Guid)jobIdentifier);
                        if (!workflow.ProcessFlowExists)
                        {
                            responseStatus = ResponseStatus.Failure;
                            responseMessage = "Job process not initiated properly,pleasse contact your administrator.";
                        }
                        model.Identifier = (Guid)jobIdentifier;
                    }
                    else
                    {
                        responseStatus = ResponseStatus.Failure;
                        responseMessage = "Job not initiated properly, please contact your administrator ";
                    }
                    break;
            }

            if (responseStatus != ResponseStatus.Success)
                return ModelSerializer.GenerateReponseXml(model, responseStatus, responseMessage);

            var commands = workflow.GetAvailableCommands(userId).Select(a => new DictionaryModel<string, string> { Key = a.Key, Value = a.Value }).ToList();
            model.CurrentActivity = workflow.CurrentActivity;
            model.Commands = commands;
            return ModelSerializer.GenerateReponseXml(model, responseStatus, responseMessage);
        }

        private string GetActivtyUrl(string activityName, Workflows workflowType, Guid processIdentifier)
        {
            var activity = uow.Repository<Activities>().GetAll().First(p => p.WorkflowID == (int)workflowType && p.Name == activityName);
            if (String.IsNullOrEmpty(activity.ActivityView)) return "";
            var job = uow.Repository<Jobs>().GetAll().First(p => p.Identifier == processIdentifier);

            return activity.ActivityView.Replace("[ID]", Utility.EncryptQueryString(job.ID.ToString()));
        }

        private string GetActivtyUrl(string activityName, Workflows workflowType, int processIdentifier)
        {
            var activity = uow.Repository<Activities>().GetAll().First(p => p.WorkflowID == (int)workflowType && p.Name == activityName);
            return !String.IsNullOrEmpty(activity.ActivityView)
                ? activity.ActivityView.Replace("[ID]", processIdentifier.ToString())
                : "";
        }

        public string WorkflowProcessExecuteCommand(string xml)
        {
            var model = (WorkflowPendingProcessViewModel)ModelDeserializer.DeserializeFromXml<WorkflowPendingProcessViewModel>(xml);
            var type = (Workflows)model.TypeId;

            return WorkflowProcessExecuteCommand(type, model.ProcessId, model.Command, model.userId);
        }

        private string WorkflowProcessExecuteCommand(Workflows type, Guid processId, string command, Guid userId)
        {
            var workflow = WorkflowInit.GetWorkFlowInstance(type, processId);
            var status = workflow.ExecuteCommand(command, userId, processId);

            return ModelSerializer.GenerateReponseXml(new GenericModel<string> { Value = status }, ResponseStatus.Success, "Successful");
        }

        public string UpdateWorkflowProcess(string xml)
        {
            var model = (WorkflowTemplateModel)ModelDeserializer.DeserializeFromXml<WorkflowTemplateModel>(xml);
            return UpdateWorkflowProcess(model);
        }

        private string UpdateWorkflowProcess(WorkflowTemplateModel model)
        {
            var workflowProcess = LoadSchemaFromDB(model);

            var validationMessages = workflowProcess.Validatate();
            if (validationMessages.Count > 0)
            {
                var sb = new StringBuilder();
                foreach (var message in validationMessages)
                {
                    sb.Append(message);
                }

                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, sb.ToString());
            }
            var transitionRepository = uow.Repository<Transitions>();

            #region Add new Transitions

            foreach (var transition in model.Transitions)
            {
                if (transition.ID < 0)
                {
                    var newTransition = new Transitions()
                    {
                        WorkflowID = model.WorkflowTypeId,
                        Name = transition.Name,
                        FromActivityID = transition.FromActivityID,
                        ToActivityID = transition.ToActivityID,
                        Classifier = TransitionClassifier.Direct,
                        CommandType = TransitionCommandType.Command,
                        ActionType = TransitionActionType.Always,
                        PreExecutionResult = 0,
                        EntityId = model.EntityId,
                        CreatedBy = model.UserId,
                        CreatedOn = DateTime.Now,
                    };
                    if (transition.Command.ID == -1)
                    {
                        newTransition.Command = new Commands()
                        {
                            Name = transition.Command.Name,
                            EntityId = newTransition.EntityId
                        };
                    }
                    else
                    {
                        newTransition.CommandID = transition.Command.ID;
                    }

                    transitionRepository.Add(newTransition);
                    uow.Commit();

                    var transactionObj = workflowProcess.Transitions.FirstOrDefault(p => p.ID == transition.ID.ToString());
                    transactionObj.ID = newTransition.ID.ToString();

                    if (transition.Command.ID < 0)
                    {
                        var commandObj = workflowProcess.Commands.FirstOrDefault(p => p.ID == transition.Command.ID.ToString());
                        commandObj.ID = newTransition.Command.ID.ToString();
                    }
                }
            }

            #endregion Add new Transitions

            WorkflowMap workflowMap;
            var workflowMapRepository = uow.Repository<WorkflowMap>();
            if (model.Id == -1)
            {
                workflowMap = new WorkflowMap();
                workflowMap.EntityId = model.EntityId;
                workflowMap.WorkFlowTypeId = model.WorkflowTypeId;
                workflowMap.Workflow = new WorkflowProcessSchemes();
                workflowMap.Workflow.Id = Guid.NewGuid();

                workflowMap.Workflow.IsObsolete = false;
                workflowMap.Workflow.ProcessName = workflowMap.Workflow.Id.ToString();

                workflowMap.Workflow.DefiningParameters = "{}";
                workflowMap.Workflow.DefiningParametersHash = "r4ztHEDMTwYwDqoEyePFlg==";
                workflowMap.Workflow.Scheme = workflowProcess.ToXml();
                workflowMap.IsActive = model.IsDefault;
                workflowMap.Name = model.Name;
                workflowMap.ModifiedBy = workflowMap.CreatedBy = model.UserId;
                workflowMap.ModifiedOn = workflowMap.CreatedOn = DateTime.Now;

                workflowMapRepository.Add(workflowMap);
            }
            else
            {
                workflowMap = workflowMapRepository.GetAll().FirstOrDefault(p => p.EntityId == model.EntityId && p.ID == model.Id);
                workflowMap.ModifiedBy = model.UserId;
                workflowMap.ModifiedOn = DateTime.Now;
                workflowMap.Workflow.Scheme = workflowProcess.ToXml();
                workflowMap.IsActive = model.IsDefault;
                workflowMap.Name = model.Name;
            }

            var processSchemeRepository = uow.WorkflowRepository<WorkflowSchemes>();
            var processScheme = processSchemeRepository.Get(p => p.Code == workflowMap.WorkFlowId.ToString());
            if (processScheme == null)
            {
                processScheme = new WorkflowSchemes();
                processScheme.Code = workflowMap.Workflow.ProcessName;
                processScheme.Scheme = workflowMap.Workflow.Scheme;
                processScheme.Id = Guid.NewGuid();

                processSchemeRepository.Add(processScheme);
            }
            else
            {
                processScheme.Scheme = workflowMap.Workflow.Scheme;
            }

            if (model.IsDefault)
            {
                var activeTemlates = workflowMapRepository.GetAll()
                    .Where(p => p.IsActive && p.WorkFlowTypeId == model.WorkflowTypeId && p.EntityId == model.EntityId && p.ID != model.Id).FirstOrDefault();
                if (activeTemlates != null)
                    activeTemlates.IsActive = false;
            }

            uow.Commit();

            #region Delete Transitions

            var activeTransitions = workflowProcess.Transitions.Select(p => Convert.ToInt32(p.ID)).ToList();
            var deletedTransitions = transitionRepository.GetAll()
                .Where(
                    p => p.EntityId == model.EntityId && p.StatusID == Status.Active && !activeTransitions.Contains(p.ID)).ToList();
            if (deletedTransitions.Any())
            {
                foreach (var transition in deletedTransitions)
                {
                    transition.StatusID = Status.Deleted;
                    transition.ModifiedBy = model.UserId;
                    transition.ModifiedOn = DateTime.Now;
                }

                uow.Commit();
            }

            #endregion Delete Transitions

            return ModelSerializer.GenerateReponseXml(new GenericModel<int>() { Value = workflowMap.ID }, ResponseStatus.Success, "Workflow updated successfully");
        }

        private Process LoadSchemaFromDB(WorkflowTemplateModel model)
        {
            var unitOfWork = new UnitOfWork();

            var workflowTemplateProcess = new Process();
            if (model.Id > 0)
            {
                var selectedWorkflow = unitOfWork.Repository<WorkflowMap>().GetById(model.Id);
                workflowTemplateProcess.Load(selectedWorkflow.Workflow.Scheme);
            }
            else
            {
                var selectedWorkflow =
                    unitOfWork.Repository<WorkflowMap>()
                        .GetAll()
                        .FirstOrDefault(p => !p.EntityId.HasValue && p.WorkFlowTypeId == model.WorkflowTypeId);
                workflowTemplateProcess.Load(selectedWorkflow.Workflow.Scheme);
            }


            var workflowProcess = new Process();
            var actors = model.Transitions.SelectMany(q => q.Restrictions.Select(p => p.ActorID)).ToList();
            var availableActors = unitOfWork.Repository<Actors>().GetAll().Where(actor => actors.Contains(actor.ID)).AsEnumerable().Select(p => new { p.MethodName, p.ID, p.Name }).ToList();
            var availableActivities = unitOfWork.Repository<Activities>().GetAll().Where(activity => activity.WorkflowID == model.WorkflowTypeId).ToList();

            #region Get Work flow Detail

            var workflowDetail =

                unitOfWork.Repository<Workflow>().GetAll().Where(a => a.ID == model.WorkflowTypeId).AsEnumerable()
                    .Select(a => new
                    {
                        WorkFlowName = a.Name,
                        WorkflowID = a.ID,
                        wfActivities =
                            a.Activities.Where(
                                p => model.Transitions.Any(m => m.ToActivityID == p.ID || m.FromActivityID == p.ID))
                                .Select(p =>
                                    new
                                    {
                                        p.ID,
                                        p.IsAutoScheme,
                                        p.IsFinal,
                                        p.IsForSetState,
                                        p.IsInitial,
                                        State = p.State.Name,
                                        p.StateID,
                                        p.Name,
                                        Actions = p.ActivityActions
                                            .Select(m => new
                                            {
                                                m.ActionID,
                                                m.IsPreExecution,
                                                m.Action.Name,
                                                m.Action.Method,
                                                MethodType = m.Action.Type,
                                                InputParameters =
                                                    m.Action.ActionParameters.Where(r => !r.IsOutput)
                                                        .Select(
                                                            q =>
                                                                new
                                                                {
                                                                    q.ID,
                                                                    q.Parameter.Name,
                                                                    q.ParameterID,
                                                                    q.Parameter.Purpose,
                                                                    q.Parameter.SerializedDefaultValue,
                                                                    q.Parameter.Type,
                                                                    q.Order
                                                                })
                                                        .ToList(),
                                                OutputParameters =
                                                    m.Action.ActionParameters.Where(r => r.IsOutput)
                                                        .Select(
                                                            q =>
                                                                new
                                                                {
                                                                    q.ID,
                                                                    q.Parameter.Name,
                                                                    q.ParameterID,
                                                                    q.Parameter.Purpose,
                                                                    q.Parameter.SerializedDefaultValue,
                                                                    q.Parameter.Type,
                                                                    q.Order
                                                                })
                                                        .ToList()
                                            }).ToList()
                                    }).ToList(),
                        wfTransitions = a.Transitions.Where(p => model.Transitions.Any(q => p.ID == q.ID))
                            .Select(m => new
                            {
                                #region Select Transition

                                m.Name,
                                ID = m.ID.ToString(),
                                Classifier = m.Classifier.ToString(),
                                FromActivity =
                                    a.Activities.FirstOrDefault(
                                        r => model.Transitions.Any(q => q.ID == m.ID && q.FromActivityID == r.ID)),
                                //m.FromActivity.Name,
                                ToActivity =
                                    a.Activities.FirstOrDefault(
                                        r => model.Transitions.Any(q => q.ID == m.ID && q.ToActivityID == r.ID)),
                                TriggerCommandName = m.Command == null ? null : m.Command.Name,
                                TriggerCommandType = m.CommandType.ToString(),
                                TriggerCommandId = m.Command == null ? null : m.Command.ID.ToString(),
                                TriggerCommandParameters =
                                    m.Command == null
                                        ? null
                                        : m.Command.CommandParameters.Select(
                                            q =>
                                                new
                                                {
                                                    q.Parameter.Name,
                                                    q.ParameterID,
                                                    q.Parameter.Purpose,
                                                    q.Parameter.SerializedDefaultValue,
                                                    q.Parameter.Type,
                                                }).ToList(),
                                ConditionActionName = m.Action == null ? null : m.Action.Name,
                                ConditionActionID = m.Action == null ? null : m.Action.ID.ToString(),
                                CondtionActionType = m.ActionType.ToString(),
                                ConditionActionMethodType = m.Action == null ? null : m.Action.Type.ToString(),
                                ConditionActionInputParameters =
                                    m.Action == null
                                        ? null
                                        : m.Action.ActionParameters.Where(r => !r.IsOutput)
                                            .Select(
                                                q =>
                                                    new
                                                    {
                                                        q.ID,
                                                        q.Parameter.Name,
                                                        q.ParameterID,
                                                        q.Parameter.Purpose,
                                                        q.Parameter.SerializedDefaultValue,
                                                        q.Parameter.Type,
                                                        q.Order
                                                    })
                                            .ToList(),
                                ConditionActionOutputParameters =
                                    m.Action == null
                                        ? null
                                        : m.Action.ActionParameters.Where(r => r.IsOutput)
                                            .Select(
                                                q =>
                                                    new
                                                    {
                                                        q.ID,
                                                        q.Parameter.Name,
                                                        q.ParameterID,
                                                        q.Parameter.Purpose,
                                                        q.Parameter.SerializedDefaultValue,
                                                        q.Parameter.Type,
                                                        q.Order
                                                    })
                                            .ToList(),
                                ConditionActionMethod = m.Action == null ? null : m.Action.Method,

                                ActionPreExceutionResult = "false",
                                TActors =
                                    model.Transitions.Where(p => p.ID == m.ID)
                                        .Select(
                                            p =>
                                                p.Restrictions.Select(
                                                    r =>
                                                        new
                                                        {
                                                            r.ActorID,
                                                            r.TypeID,
                                                            TypeName = (TransitionRestrictionType)r.TypeID,
                                                            r.ID
                                                        }).ToList())
                                        .FirstOrDefault()

                                #endregion Select Transition
                            }).ToList(),
                    }).FirstOrDefault();

            #region Select Newly Added Transition

            var newTransactionsList =
            model.Transitions.Where(p => p.ID < 0).Select(m => new ProcessTransition
            {
                Name = m.Name,
                ID = m.ID.ToString(),
                Classifier = TransitionClassifier.Direct.ToString(),
                From = availableActivities.FirstOrDefault(r => model.Transitions.Any(q => q.ID == m.ID && q.FromActivityID == r.ID)).Name,
                FromActivityID = m.FromActivityID.ToString(),
                To = availableActivities.FirstOrDefault(r => model.Transitions.Any(q => q.ID == m.ID && q.ToActivityID == r.ID)).Name,
                ToActivityID = m.ToActivityID.ToString(),
                Conditions = new Conditions { Condition = new ConditionDef() { NameRef = null, Type = TransitionActionType.Always.ToString(), ResultOnPreExecution = "false" } },
                Triggers = new Triggers { Trigger = new TriggersDef() { NameRef = m.Command.Name, Type = TransitionCommandType.Command.ToString() } },
                Restrictions = m.Restrictions.Select(r => new Restriction() { ID = r.ID.ToString(), TypeID = r.TypeID.ToString(), NameRef = availableActors.SingleOrDefault(actor => actor.ID == r.ActorID).Name, ActorID = r.ActorID.ToString(), Type = ((TransitionRestrictionType)r.TypeID).ToString() }).ToList()
            }).ToList();

            #endregion Select Newly Added Transition

            #endregion Get Work flow Detail

            workflowProcess.ID = workflowDetail.WorkflowID.ToString();
            workflowProcess.Name = workflowDetail.WorkFlowName;

            #region Add Transactions

            var transactionList = workflowDetail.wfTransitions.Select(m => new ProcessTransition
            {
                Classifier = m.Classifier,
                From = m.FromActivity.Name,
                FromActivityID = m.FromActivity.ID.ToString(),
                To = m.ToActivity.Name,
                ToActivityID = m.ToActivity.ID.ToString(),
                Name = m.Name,
                ID = m.ID,
                Conditions = new Conditions { Condition = new ConditionDef() { NameRef = m.ConditionActionName, Type = m.CondtionActionType, ResultOnPreExecution = m.ActionPreExceutionResult } },
                Triggers = new Triggers { Trigger = new TriggersDef() { NameRef = m.TriggerCommandName, Type = m.TriggerCommandType } },
                Restrictions = m.TActors.Select(r => new Restriction() { ID = r.ID.ToString(), TypeID = r.TypeID.ToString(), NameRef = availableActors.SingleOrDefault(actor => actor.ID == r.ActorID).Name, ActorID = r.ActorID.ToString(), Type = r.TypeName.ToString() }).ToList()
            }).ToList();
            transactionList.AddRange(newTransactionsList);

            workflowProcess.Transitions = transactionList;

            #endregion Add Transactions

            #region Add Activities

            var activityList = workflowDetail.wfActivities.Select(p => new ProcessActivity()
            {
                ID = p.ID,
                Implementation =
                    p.Actions.Where(m => !m.IsPreExecution)
                        .Select(r => new ActivityActionRef { NameRef = r.Name, Order = 1 })
                        .ToList(),
                PreExecutionImplementation =
                    p.Actions.Where(m => m.IsPreExecution)
                        .Select(r => new ActivityActionRef { NameRef = r.Name, Order = 1 })
                        .ToList(),
                IsAutoSchemeUpdate = p.IsAutoScheme ? "true" : "false",
                IsFinal = p.IsFinal.ToString(),
                IsInitial = p.IsInitial.ToString(),
                IsForSetState = p.IsForSetState.ToString(),
                State = p.State.ToString(),
                Name = p.Name,
                Designer = workflowTemplateProcess.GetActivityDesign(p.ID)
            }).ToList();

            

            workflowProcess.Activities = activityList;

            #region Set Graph Presentation

            var axisVlaues = new Point(activityList.Max(p => p.Designer.X) + 50, 0);
            var axisIncreament = new Point(200, 150);
            const int xAxisMaxLimit = 1000;
            const int xAxisMinLimit = 0;

            // ProcessActivity currentActivity = activityList.Single(p => p.IsInitial == "true");

            foreach (var currentActivity in activityList.Where(p=> p.Designer.X == -1 &&  p.Designer.Y == -1).ToList())
            {
                currentActivity.Designer.X = axisVlaues.X;
                currentActivity.Designer.Y = axisVlaues.Y;
                axisVlaues.X += axisIncreament.X;
                if (axisVlaues.X >= xAxisMaxLimit)
                {
                    axisVlaues.Y += axisIncreament.Y;
                    axisVlaues.X = xAxisMinLimit;
                }
            }

            #endregion Set Graph Presentation

            #endregion Add Activities

            #region Add Actors

            var actorList = availableActors.Select(p => new ProcessActor() { ID = p.ID.ToString(), Name = p.Name, Rule = new ProcessActorRule { RuleName = p.MethodName } }).ToList();
            workflowProcess.Actors = actorList;

            #endregion Add Actors

            #region Add Actions

            var actionList = workflowDetail.wfActivities.SelectMany(p => p.Actions.Select(m => new ProcessAction()
            {
                ID = m.ActionID.ToString(),
                ExecuteMethod = new ProcessActionExecuteMethod()
                {
                    MethodName = m.Method,
                    Type = m.MethodType,
                    InputParameters = m.InputParameters
                    .Select(r => new ParameterRef
                    {
                        NameRef = r.Name,
                        Order = r.Order
                    }).ToList(),
                    OutputParameters = m.OutputParameters
                   .Select(r => new ParameterRef
                   {
                       NameRef = r.Name,
                       Order = r.Order
                   }).ToList()
                },
                Name = m.Name
            }).ToList()).Union(

            workflowDetail.wfTransitions.Where(c => c.ConditionActionID != null).Select(q => new ProcessAction()
            {
                ID = q.ConditionActionID,
                ExecuteMethod = new ProcessActionExecuteMethod()
                {
                    MethodName = q.ConditionActionMethod,
                    Type = q.ConditionActionMethodType,
                    InputParameters = q.ConditionActionInputParameters
                    .Select(r => new ParameterRef
                    {
                        NameRef = r.Name,
                        Order = r.Order
                    }).ToList(),
                    OutputParameters = q.ConditionActionOutputParameters
                   .Select(r => new ParameterRef
                   {
                       NameRef = r.Name,
                       Order = r.Order
                   }).ToList()
                },
                Name = q.ConditionActionName
            })).ToList();
            var test = actionList.GroupBy(cust => cust.ID).Select(grp => grp.First()).ToList();
            workflowProcess.Actions = test;

            #endregion Add Actions

            #region Add Parameters

            var parameterList = workflowDetail.wfActivities.SelectMany(p => p.Actions)
                                                        .SelectMany(q => q.InputParameters)
                                                        .Where(r => r.Purpose != ParameterPurpose.System)
                                                        .Select(s => new ProcessParameters { ID = s.ParameterID.ToString(), Name = s.Name, Purpose = s.Purpose.ToString(), Type = s.Type })
             .Union(
             workflowDetail.wfActivities.SelectMany(p => p.Actions)
                                                       .SelectMany(q => q.OutputParameters)
                                                       .Where(r => r.Purpose != ParameterPurpose.System)
                                                       .Select(s => new ProcessParameters { ID = s.ParameterID.ToString(), Name = s.Name, Purpose = s.Purpose.ToString(), Type = s.Type }).ToList()
                   )
            .Union(
            workflowDetail.wfTransitions.Where(q => q.ConditionActionOutputParameters != null)
                                                      .SelectMany(p => p.ConditionActionOutputParameters)
                                                      .Where(r => r.Purpose != ParameterPurpose.System)
                                                      .Select(s => new ProcessParameters { ID = s.ParameterID.ToString(), Name = s.Name, Purpose = s.Purpose.ToString(), Type = s.Type }).ToList()
                  )
            .Union(
            workflowDetail.wfTransitions.Where(q => q.ConditionActionOutputParameters != null)
                                                   .SelectMany(p => p.ConditionActionInputParameters)
                                                   .Where(r => r.Purpose != ParameterPurpose.System)
                                                   .Select(s => new ProcessParameters { ID = s.ParameterID.ToString(), Name = s.Name, Purpose = s.Purpose.ToString(), Type = s.Type }).ToList()
                 )
                 .Union(
                   workflowDetail.wfTransitions.Where(q => q.TriggerCommandParameters != null)
                                                   .SelectMany(p => p.TriggerCommandParameters)
                                                   .Where(r => r.Purpose != ParameterPurpose.System)
                                                   .Select(s => new ProcessParameters { ID = s.ParameterID.ToString(), Name = s.Name, Purpose = s.Purpose.ToString(), Type = s.Type }).ToList()
                 )

                 .GroupBy(grp => new { grp.Name }).Select(g => g.First()).ToList();

            workflowProcess.Parameters = parameterList;

            #endregion Add Parameters

            #region Add Commands

            var commandList = workflowDetail.wfTransitions.Where(p => p.TriggerCommandId != null).Select(q => new ProcessCommand { ID = q.TriggerCommandId, Name = q.TriggerCommandName, InputParameters = q.TriggerCommandParameters.Select(r => new CommandParameterRef { Name = r.Name, NameRef = r.Name }).ToList() }).ToList();
            var newTransitionCommands = model.Transitions.Where(p => p.ID < 0).Select(q => new ProcessCommand { ID = q.Command.ID.ToString(), Name = q.Command.Name, }).ToList();
            commandList.AddRange(newTransitionCommands);
            commandList = commandList.GroupBy(grp => new { grp.Name }).Select(g => g.First()).ToList();
            workflowProcess.Commands = commandList;

            #endregion Add Commands

            return workflowProcess;
        }

        public string GetWorkflowGraph(string xml)
        {
            var model = (WorkflowCreateModel)ModelDeserializer.DeserializeFromXml<WorkflowCreateModel>(xml);
            return GetWorkflowGraph(model.FirmId, (Workflows)model.WorkflowTypeId, model.TemplateId);
        }

        private string GetWorkflowGraph(int entityId, Workflows workflowType, int workflowTemplateId)
        {
            var selectedWorkflow = uow.Repository<WorkflowMap>().GetById(workflowTemplateId);
            var processScheme =
                new UnitOfWork().WorkflowRepository<WorkflowProcessSchemes>()
                    .GetAll()
                    .FirstOrDefault(p => p.Id == selectedWorkflow.WorkFlowId);
            if (processScheme != null)
            {
                return ModelSerializer.GenerateReponseXml(new GenericModel<string> { Value = processScheme.Scheme },
                    ResponseStatus.Success, "Successful");
            }
            return ModelSerializer.GenerateReponseXml(new GenericModel<string> { Value = "" }, ResponseStatus.Failure,
                "Successful");
        }

        public string GetWorkflowTempleList(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            return GetWorkflowTempleList(model.Key, model.Value);
        }

        private string GetWorkflowTempleList(int firmId, int workflowTypeId)
        {
            var templateList = new UnitOfWork().Repository<WorkflowMap>()
                .GetAll()
                .Where(p => (p.WorkFlowTypeId == workflowTypeId && (p.EntityId == firmId || !p.EntityId.HasValue)))
                .Select(q => q).OrderBy(r => r.Name).ToList();

            var defaultTemplate = templateList.Where(p => p.IsActive && p.EntityId.HasValue).FirstOrDefault() ?? templateList.Where(p => !p.EntityId.HasValue).FirstOrDefault();
            var models =
            templateList.Select(
                p => new WorkflowSelectModel() { Id = p.ID, IsDefault = p.ID == defaultTemplate.ID, Name = p.Name })
                .ToList();
            return ModelSerializer.GenerateReponseXml(models, ResponseStatus.Success, "Success");
        }

        public string SetDefaultWorkFlow(string xml)
        {
            var model = (WorkflowCreateModel)ModelDeserializer.DeserializeFromXml<WorkflowCreateModel>(xml);
            return SetDefaultWorkFlow(model.FirmId, model.TemplateId, model.WorkflowTypeId);
        }

        private string SetDefaultWorkFlow(int firmId, int workflowId, int workflowTypeId)
        {
            var workflowMapRepository = uow.Repository<WorkflowMap>();
            var templates = workflowMapRepository.GetAll().Where(p => p.EntityId == firmId && (p.ID == workflowId || (p.IsActive && p.WorkFlowTypeId == workflowTypeId))).ToList();
            if (templates.Any())
            {
                foreach (var template in templates)
                {
                    if (template.ID == workflowId)
                        template.IsActive = true;
                    else
                        template.IsActive = false;
                    workflowMapRepository.Update(template);
                }
            }
            uow.Commit();
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Success");
        }

        public string SaveWorkFlowGraph(string xml)
        {
            var model = (GraphDesignerModel)ModelDeserializer.DeserializeFromXml<GraphDesignerModel>(xml);
            return SaveWorkFlowGraph(model);
        }

        private string SaveWorkFlowGraph(GraphDesignerModel model)
        {
            var workflowMapReposiotry = uow.Repository<WorkflowMap>();

            var workflow = workflowMapReposiotry.GetById(model.WorkflowTemplateId).Workflow;



            var workflowProcess = new Process();
            workflowProcess.Load(workflow.Scheme);
            foreach (var activity in model.ActivityPostions)
            {
                var t = workflowProcess.Activities.FirstOrDefault(p => p.Name == activity.Name);
                t.Designer.X = activity.LocationX;
                t.Designer.Y = activity.LocationY;
            }
            workflow.Scheme = workflowProcess.ToXml();
            uow.Commit();
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Success");
        }

        internal List<JobRolesModel> GetWorkFlowActors(WorkflowSelectModel model)
        {
            var unitOfWork = new UnitOfWork();

            var workflowProcess = new Process();
            var selectedWorkflow = unitOfWork.Repository<WorkflowMap>().GetById(model.Id);
            workflowProcess.Load(selectedWorkflow.Workflow.Scheme);
            var actorIds=workflowProcess.Actors.Select(p => p.ID.ToType<int>()).ToList();
            var roles = unitOfWork.Repository<AccountManagerJobRoles>().GetAll().Where(a => a.IsActive && actorIds.Contains(a.ID))
               .Select(a => new JobRolesModel
               {
                   ID = a.ID,
                   RoleName = a.Name
               }).ToList();

            return roles;
        }
    }
}