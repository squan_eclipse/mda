﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Extensions;
using DBASystem.SMSF.Data.Models;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class ImportComponent : Component
    {
        private UnitOfWork uow = new UnitOfWork();

        public string GetTemplateProviders(string parameters)
        {
            var templateProviders =
                uow.Repository<TemplateProvider>()
                    .GetAll()
                    .Where(q => q.StatusID == Status.Active)
                    .Select(a => new DictionaryModel<int, string> {Key = a.ID, Value = a.Name})
                    .ToList();
            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(templateProviders,
                ResponseStatus.Success, "Successful");
        }

        public string GetTemplateCategories(string parameters)
        {
            var templateProviderID =
                ((GenericModel<int>) ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameters)).Value;

            var templateCategories =
                uow.Repository<TemplateCategories>()
                    .GetAll()
                    .Where(q => q.TemplateProviderID == templateProviderID && q.StatusID == Status.Active)
                    .Select(a => new DictionaryModel<int, string> {Key = a.ID, Value = a.Name})
                    .OrderBy(q => q.Value)
                    .ToList();
            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(templateCategories,
                ResponseStatus.Success, "Successful");
        }

        public string GetTemplates(string parameters)
        {
            var templateCategoryID =
                ((GenericModel<int>) ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameters)).Value;

            var templates =
                uow.Repository<Templates>()
                    .GetAll()
                    .Where(q => q.CategoryID == templateCategoryID)
                    .Select(a => new DictionaryModel<int, string> {Key = a.ID, Value = a.Name})
                    .ToList();
            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(templates, ResponseStatus.Success,
                "Successful");
        }

        public string GetImportTemplates(string parameters)
        {
            var importTemp =
                uow.Repository<Templates>().GetAll().Select(a => new TemplateModel {ID = a.ID, Name = a.Name}).ToList();
            return ModelSerializer.GenerateReponseXml<TemplateModel>(importTemp, ResponseStatus.Success, "Successful");
        }

        public string GetTemplate(string parameters)
        {
            var templateID =
                ((GenericModel<int>) ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameters)).Value;
            var importTemp = uow.Repository<Templates>().GetById(templateID);
            var templateModel = new TemplateModel()
            {
                CategoryID = importTemp.CategoryID,
                ID = importTemp.ID,
                Name = importTemp.Name,
                TemplateText = importTemp.TemplateText
            };
            return ModelSerializer.GenerateReponseXml(templateModel, ResponseStatus.Success, "Successful");
        }

        #region For Getting Imported Data

        public string GetJobsImportedData(string xml)
        {
            var content =
                (DictionaryModel<int, int>) ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            return GetJobsImportedData(content.Key, content.Value);
        }

        internal string GetJobsImportedData(int jobID, int jobsImportedDataId)
        {
            var context = uow.Repository<JobImportedData>();
            JobImportedData jobImportedData = context.GetById(jobsImportedDataId);

            var jobImportedDataViewModel = ToJobImportDataViewModel(jobImportedData);

            return ModelSerializer.GenerateReponseXml(jobImportedDataViewModel, ResponseStatus.Success, "Successful");
        }

        public string GetJobsImportedFiles(string xml)
        {
            var content =
                (DictionaryModel<string, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<string, int>>(xml);
            return GetJobsImportedFiles(content.Value);
        }

        private string GetJobsImportedFiles(int jobId)
        {
            var model =
                uow.Repository<JobImportedData>()
                    .GetAll()
                    .Where(x => x.JobId == jobId)
                    .AsEnumerable()
                    .Select(a => new JobImportedFileViewModel
                    {
                        Category = a.Template.Category.Name,
                        Template = a.Template.Name,
                        FileName = a.Filename,
                        CreatedOn = a.CreatedOn,
                    }).ToList();

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }


        public JobImportedDataViewModel ToJobImportDataViewModel(JobImportedData model)
        {

            return model == null
                ? null
                : new JobImportedDataViewModel()
                {
                    JobId = model.JobId,
                    TemplateId = model.TemplateId,
                    ControlType = model.ControlType,
                    CreatedBy = model.CreatedBy,
                    FileName = model.Filename,
                    XmlData = model.XmlData,
                    CreatedOn = model.CreatedOn,
                    SourceFile =  new DocumentModel(){ ServerRelativeUrl = model.Job.GetFolderPath() + new JobComponent().ImportFilesFolderName +"/" + model.Filename}
                    
                };
        }

        #endregion
    }
}
