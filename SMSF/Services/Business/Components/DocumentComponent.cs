﻿using System.Collections.Generic;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Service.Sharepoint;
using System;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class DocumentComponent : Component
    {
        #region Declaration

        private DocumentManagement documentMgmt;
        private int currentUser;
        private string errorIdentifier = "";
        private SharePointSettingModel sharePointSetting;

        #endregion Declaration

        #region Constructor

        public DocumentComponent(int userId)
        {
            sharePointSetting = new CommonComponent().GetSettingByType<SharePointSettingModel>();
            documentMgmt = new DocumentManagement(sharePointSetting.SharePointUrl, sharePointSetting.UserName, sharePointSetting.Password);
            currentUser = userId;
        }

        public DocumentComponent()
        {
            sharePointSetting = new CommonComponent().GetSettingByType<SharePointSettingModel>();
            documentMgmt = new DocumentManagement(sharePointSetting.SharePointUrl, sharePointSetting.UserName, sharePointSetting.Password);
            currentUser = 1; // if user id is not available then simulate as System user
        }

        #endregion Constructor

        #region Folder Operations

        /// <summary>
        ///  Creates a new folder if parentFolder name is emtpy folder will be created in Root folder
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parentFolder"></param>
        public ResponseModel AddFolder(string name, string parentFolder)
        {
            return AddFolder(name, parentFolder, new List<string>());
        }

        public ResponseModel AddFolder(string name, string parentFolder,List<string> subFolderList)
        {
            var response = new ResponseModel();
            try
            {
                documentMgmt.AddFolder(name, parentFolder,subFolderList);
                response.ResponseMessage = parentFolder + "/" + name + "/";
                response.Status = ResponseStatus.Success;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = ResponseStatus.Failure;
                var componentMessage = "Unable to add folder " + name + (String.IsNullOrEmpty(parentFolder) ? "" : " in " + parentFolder);
                AddNotificationOnError(ex, componentMessage);
                response.Status = ResponseStatus.Failure;
                response.StatusMessage = errorIdentifier + componentMessage;
                return response;
            }
        }

        /// <summary>
        ///  Renames an existing folder
        /// </summary>
        /// <param name="name"></param>
        /// <param name="newName"></param>
        /// <param name="parentFolder"></param>
        public ResponseModel RenameFolder(string name, string newName, string parentFolder)
        {
            var response = new ResponseModel();
            try
            {
                documentMgmt.RenameFolder(name, newName, parentFolder);
                response.Status = ResponseStatus.Success;
                return response;
            }
            catch (Exception ex)
            {
                var componentMessage = "Unable to rename folder " + name + " to " + newName + " in " + parentFolder;
                AddNotificationOnError(ex, componentMessage);
                response.Status = ResponseStatus.Failure;
                response.StatusMessage = errorIdentifier + componentMessage;
                return response;
            }
        }

        public ResponseModel MoveFolder(string sourceFolder, string destinationFolder)
        {
            var response = new ResponseModel();
            try
            {
                documentMgmt.MoveFolder(sourceFolder, destinationFolder);
                response.Status = ResponseStatus.Success;
                return response;
            }
            catch (Exception ex)
            {
                response.Status = ResponseStatus.Failure;
                var componentMessage = "Unable to move folder from" + sourceFolder + " to " + destinationFolder;
                AddNotificationOnError(ex, componentMessage);
                response.Status = ResponseStatus.Failure;
                response.ResponseMessage = errorIdentifier + componentMessage;
                return response;
            }
        }

        /// <summary>
        ///  Marks a folder as deleted i.e FolderName - Deleted
        /// </summary>
        /// <param name="rootFolder"></param>
        /// <param name="folderName"></param>
        public ResponseModel DeleteFolder(string rootFolder, string folderName)
        {
            var response = new ResponseModel();
            try
            {
                documentMgmt.DeleteFolder(folderName, rootFolder);
                return response;
            }
            catch (Exception ex)
            {
                response.Status = ResponseStatus.Failure;
                var componentMessage = "Unable to delete folder " + folderName + " from " + rootFolder;
                AddNotificationOnError(ex, componentMessage);
                response.Status = ResponseStatus.Failure;
                response.StatusMessage = errorIdentifier + componentMessage;
                return response;
            }
        }

        #endregion Folder Operations

        #region Files

        public string GetFiles(string xml)
        {
            var model = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(xml);
            return GetSharepointFiles(model.Value);
        }

        private string GetSharepointFiles(string folderPath)
        {
            try
            {
                var files = documentMgmt.GetFiles(folderPath);
                return ModelSerializer.GenerateReponseXml<DocumentModel>(files, ResponseStatus.Success, "Successful");
            }
            catch (Exception ex)
            {
                var componentMessage = "Unable to get files from" + folderPath;
                AddNotificationOnError(ex, componentMessage);
                return ModelSerializer.GenerateReponseXml<DocumentModel>(null, ResponseStatus.Failure, errorIdentifier + componentMessage);
            }
        }

        public string DeleteFile(string xml)
        {
            var model = (DictionaryModel<string, string>)ModelDeserializer.DeserializeFromXml<DictionaryModel<string, string>>(xml);
            return DeleteFile(model.Key, model.Value);
        }

        internal string DeleteFile(string name, string filePath)
        {
            try
            {
                documentMgmt.DeleteFile(name, filePath);
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
            }
            catch (Exception ex)
            {
                var componentMessage = "Unable to delete file " + name + " from " + filePath;
                AddNotificationOnError(ex, componentMessage);
                return ModelSerializer.GenerateReponseXml<DocumentModel>(null, ResponseStatus.Failure, errorIdentifier + componentMessage);
            }
        }

        public string DownloadFile(string xml)
        {
            var model = (DictionaryModel<string, string>)ModelDeserializer.DeserializeFromXml<DictionaryModel<string, string>>(xml);
            return DownloadFile(model.Key, model.Value);
        }

        internal string DownloadFile(string folderPath, string fileName)
        {
            try
            {
                var fileData = Convert.ToBase64String(documentMgmt.DownloadFile(folderPath, fileName));
                return ModelSerializer.GenerateReponseXml(new GenericModel<string> { Value = fileData }, ResponseStatus.Success, "Successful");
            }
            catch (Exception ex)
            {
                var componentMessage = "File " + fileName + " not found at " + folderPath;
                AddNotificationOnError(ex, componentMessage);
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, errorIdentifier + componentMessage);
            }
        }

        public ResponseModel DownloadFileInternal(string folderPath, string fileName)
        {
            var response = new ResponseModel();
            try
            {
                var fileData = Convert.ToBase64String(documentMgmt.DownloadFile(folderPath, fileName));
                response.Status = ResponseStatus.Success;
                response.ModelXml = fileData;
                return response;
            }
            catch (Exception ex)
            {
                var componentMessage = "File " + fileName + " not found at " + folderPath;
                AddNotificationOnError(ex, componentMessage);
                response.Status = ResponseStatus.Failure;
                response.ResponseMessage = componentMessage;
                return response;
            }
        }

        public string UploadFiles(string xml)
        {
            var model = (FileModel)ModelDeserializer.DeserializeFromXml<FileModel>(xml);
            var response = UploadFiles(model);

            return ModelSerializer.GenerateReponseXml(null, response.Status, response.ResponseMessage);
        }

        internal ResponseModel UploadFiles(FileModel files)
        {
            var response = new ResponseModel();
            try
            {
                documentMgmt.UploadFiles(files);
                response.Status = ResponseStatus.Success;
                response.ResponseMessage = "Successful";
                return response;
            }
            catch (Exception ex)
            {
                var componentMessage = "Unable to upload files in" + files.FolderPath;
                AddNotificationOnError(ex, componentMessage);
                response.Status = ResponseStatus.Failure;
                response.ResponseMessage = componentMessage;
                return response;
            }
        }

        public ResponseModel CopyFiles(string destinationUrl)
        {
            var sourceUrl = sharePointSetting.JobDefaultDocumentsPath;

            try
            {
                documentMgmt.CopyFiles(sourceUrl, destinationUrl);
            }
            catch (Exception ex)
            {
                var message = string.Format("Unable to copy default documents, source url '{0}' destination url '{1}'",
                    sourceUrl, destinationUrl);

                AddNotificationOnError(ex, message);
                return new ResponseModel {Status = ResponseStatus.Failure, ResponseMessage = message};
            }

            return new ResponseModel {Status = ResponseStatus.Success};
        }

        #endregion Files

        #region Helper Methods

        private void AddNotificationOnError(Exception ex, string componentMessage)
        {
            Utility.Logger.Error(Utility.GetExceptionDetails(ex));
            var messageDetail = ex.InnerException != null ? ex.InnerException.Message : null;
            new NotificationComponent().InsertNotification(NotificationTypes.Error, ex.Message + "[" + componentMessage + "]", messageDetail, null, null, false, currentUser);
        }

        #endregion Helper Methods
    }
}