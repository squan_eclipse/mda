﻿using DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Service.Business.Components
{
    public sealed class ComponentFactory
    {
        public object InvokeAction(Option option, string parameters)
        {
            object result = null;
            switch (option)
            {
                case Option.AddAccount:
                    result = new FirmComponent().AddAccount(parameters);
                    break;

                case Option.AddFirm:
                    result = new FirmComponent().AddFirm(parameters);
                    break;

                case Option.AddError:
                    result = new NotificationComponent().AddError(parameters);
                    break;

                case Option.AddFund:
                    result = new FirmComponent().AddFund(parameters);
                    break;

                case Option.AddGroup:
                    result = new FirmComponent().AddGroup(parameters);
                    break;

                case Option.AddTelephone:
                    result = new TelephoneComponent().AddTelephone(parameters);
                    break;

                case Option.AddUserGroupRoles:
                    result = new UserComponent().AddUserGroupRoles(parameters);
                    break;

                case Option.AddUserRoleOptions:
                    result = new UserComponent().AddUserRoleOptions(parameters);
                    break;

                case Option.AllowedOptions:
                case Option.AllowedPages:
                    result = new ValidationComponent().GetAllowedOptionsByOptionType(parameters);
                    break;
                    //case Option.AllowedPages:
                    //    break;
                case Option.ChangeRoleStatus:
                    result = new UserComponent().ChangeRoleStatus(parameters);
                    break;

                case Option.ChangeUserGroupStatus:
                    result = new UserComponent().ChangeUserGroupStatus(parameters);
                    break;

                case Option.CreateAccountManager:
                    result = new AccountManagerComponent().AddAccountManager(parameters);
                    break;

                case Option.CreateRole:
                    result = new UserComponent().CreateRole(parameters);
                    break;

                case Option.CreateUserGroup:
                    result = new UserComponent().CreateUserGroup(parameters);
                    break;

                case Option.DeleteTelephone:
                    result = new TelephoneComponent().DeleteTelephone(parameters);
                    break;

                case Option.DeleteUser:
                    result = new AccountManagerComponent().DeleteAccountManager(parameters);
                    break;

                case Option.GetAccountManager:
                    result = new AccountManagerComponent().GetAccountManager(parameters);
                    break;

                case Option.GetAccountManagerDetail:
                    result = new AccountManagerComponent().GetAccountManagerDetail(parameters);
                    break;

                case Option.GetCountryCode:
                    result = new TelephoneComponent().GetCountryCode(parameters);
                    break;

                case Option.GetFirm:
                    result = new FirmComponent().GetFirm(parameters);
                    break;

                case Option.GetFirmAccountManager:
                    result = new AccountManagerComponent().GetFirmAccountManager(parameters);
                    break;

                //case Option.GetEntityTypes:
                //    result = new AccountManagerComponent().GetEntityAccountManager(parameters);
                //    break;

                case Option.GetRole:
                    result = new UserComponent().GetRole(parameters);
                    break;

                case Option.GetRoles:
                    result = new UserComponent().GetRoles(parameters);
                    break;

                case Option.GetTelephoneTypes:
                    result = new TelephoneComponent().GetTelephoneTypes(parameters);
                    break;

                case Option.GetUserGroup:
                    result = new UserComponent().GetUserGroup(parameters);
                    break;

                case Option.GetUserGroups:
                    result = new UserComponent().GetUserGroups(parameters);
                    break;

                case Option.GetUserGroupRoles:
                    result = new UserComponent().GetUserGroupRoles(parameters);
                    break;

                case Option.GetUserRoleOptions:
                    result = new UserComponent().GetUserRoleOptions(parameters);
                    break;

                case Option.ListAddressTypes:
                    result = new AddressComponent().GetAddressTypes(parameters);
                    break;

                case Option.ListCountries:
                    result = new AddressComponent().GetCountries(parameters);
                    break;

                case Option.ListStates:
                    result = new AddressComponent().GetStates(parameters);
                    break;

                case Option.AddCountry:
                    result = new AddressComponent().AddCountry(parameters);
                    break;

                case Option.AddState:
                    result = new AddressComponent().AddState(parameters);
                    break;

                case Option.Login:
                    result = new UserComponent().ValidateUser(parameters);
                    break;
                    //  case Option.LogOut:
                    //  result = new UserComponent().LogOff(parameters);
                    //  break;
                case Option.UpdateAccountManager:
                    result = new AccountManagerComponent().UpdateAccountManager(parameters);
                    break;

                case Option.UpdateAccount:
                    result = new FirmComponent().UpdateAccount(parameters);
                    break;

                case Option.UpdateFirm:
                    result = new FirmComponent().UpdateFirm(parameters);
                    break;

                case Option.UpdateFund:
                    result = new FirmComponent().UpdateFund(parameters);
                    break;

                case Option.UpdateGroup:
                    result = new FirmComponent().UpdateGroup(parameters);
                    break;

                case Option.UpdateRole:
                    result = new UserComponent().UpdateRole(parameters);
                    break;

                case Option.UpdateTelephone:
                    result = new TelephoneComponent().UpdateTelephone(parameters);
                    break;

                case Option.UpdateUserGroup:
                    result = new UserComponent().UpdateUserGroup(parameters);
                    break;

                case Option.ValidateEmail:
                    result = new AccountManagerComponent().ValidateEmail(parameters);
                    break;

                case Option.SendForgotPassEmail:
                    result = new UserComponent().SendForgotPassEmail(parameters);
                    break;

                case Option.ResetPassword:
                    result = new UserComponent().ResetPassword(parameters);
                    break;

                case Option.GetUerAssociatedEntites:
                    result = new UserComponent().GetUerAssociatedEntites(parameters);
                    break;

                case Option.GetNotifications:
                    result = new NotificationComponent().GetNotifications(parameters);
                    break;
                //case Option.GetJobList:
                //    result = new JobComponent().GetJobList(parameters);
                //    break;

                case Option.GetJobManagers:
                    result = new JobComponent().GetJobManagers(parameters);
                    break;

                case Option.SaveWorkpaperTemplate:
                    result = new AuditProcedureComponent().SaveWorkpaperTemplate(parameters);
                    break;

                case Option.SaveWorkpaperTemplateAsXML:
                    result = new AuditProcedureComponent().SaveWorkpaperTemplateAsXml(parameters);
                    break;

                case Option.LoadWorkpaperTemplateFromXml:
                    result = new AuditProcedureComponent().LoadWorkpaperTemplateFromXml(parameters);
                    break;

                case Option.GetWorkpaperTemplates:
                    result = new AuditProcedureComponent().GetWorkpaperTemplates(parameters);
                    break;

                case Option.LoadWorkpaperTemplate:
                    result = new AuditProcedureComponent().LoadWorkpaperTemplate(parameters);
                    break;

                case Option.GetWorkpaperMenu:
                    result = new AuditProcedureComponent().GetWorkpaperMenu(parameters);
                    break;

                case Option.WorkPaper:
                    result = new JobComponent().GetJob(parameters);
                    break;

                case Option.AddJob:
                    result = new JobComponent().AddJob(parameters);
                    break;

                case Option.GetJobInfo:
                    result = new JobComponent().GetJobDetail(parameters);
                    break;

                case Option.UpdateJob:
                    result = new JobComponent().UpdateJob(parameters);
                    break;

                case Option.Deletejob:
                    result = new JobComponent().Deletejob(parameters);
                    break;

                case Option.GetSetting:
                    result = new SettingComponent().GetSetting(parameters);
                    break;

                case Option.UpdateEmailSettings:
                    result = new SettingComponent().UpdateEmailSettings(parameters);
                    break;

                case Option.UpdatePasswordSettings:
                    result = new SettingComponent().UpdatePasswordSettings(parameters);
                    break;

                case Option.UpdateSessionSettings:
                    result = new SettingComponent().UpdateSessionSettings(parameters);
                    break;

                case Option.UpdateUserPassword:
                    result = new SettingComponent().UpdateUserPassword(parameters);
                    break;

                case Option.UpdateVersionSettings:
                    result = new SettingComponent().UpdateVersionSettings(parameters);
                    break;

                case Option.GetSettingByType:
                    result = new SettingComponent().GetSettingByType(parameters);
                    break;

                case Option.UpdatePersonalInfo:
                    result = new UserComponent().UpdatePersonalInfo(parameters);
                    break;

                case Option.GetUserProfile:
                    result = new UserComponent().GetUserProfile(parameters);
                    break;

                case Option.UpdateEntityAddress:
                    result = new UserComponent().UpdateEntityAddress(parameters);
                    break;

                case Option.UpdateSharepointSetting:
                    result = new SettingComponent().UpdateSharepointSettings(parameters);
                    break;

                case Option.UpdateReportServerSettings:
                    result = new SettingComponent().UpdateReportServerSettings(parameters);
                    break;

                case Option.GetFiles:
                    result = new DocumentComponent().GetFiles(parameters);
                    break;

                case Option.DeleteFile:
                    result = new DocumentComponent().DeleteFile(parameters);
                    break;

                case Option.DownloadFile:
                    result = new DocumentComponent().DownloadFile(parameters);
                    break;

                case Option.UploadFiles:
                    result = new DocumentComponent().UploadFiles(parameters);
                    break;
                case Option.GetAuditProcedureTabs:
                    result = new AuditProcedureComponent().GetAuditProcedureTabs(parameters);
                    break;

                case Option.GetAuditProcedureTabContents:
                    result = new AuditProcedureComponent().GetAuditProcedureTabContents(parameters);
                    break;
                case Option.GetAuditProcedureTabContent:
                    result = new AuditProcedureComponent().GetAuditProcedureTabContent(parameters);
                    break;
                case Option.UpdateAuditProcedureTabContent:
                    result = new AuditProcedureComponent().UpdateAuditProcedureTabContent(parameters);
                    break;
                    
                case Option.DeleteAuditProcedure:
                    result = new AuditProcedureComponent().DeleteAuditProcedure(parameters);
                    break;
                case Option.AddAuditProcedure:
                    result = new AuditProcedureComponent().AddAuditProcedure(parameters);
                    break;
                //case Option.GetAuditProcedures:
                //    result = new AuditProcedureComponent().GetAuditProcedures(parameters);
                //    break;

                case Option.DeleteNotifications:
                    result = new NotificationComponent().DeleteNotifications(parameters);
                    break;

                case Option.GetWorkflow:
                    result = new WorkflowComponent().GetWorkflows(parameters);
                    break;

                case Option.GetWorkflowTemplate:
                    result = new WorkflowComponent().GetWorkflowTemplate(parameters);
                    break;

                case Option.GetImportTemplates:
                    result = new ImportComponent().GetImportTemplates(parameters);
                    break;

                case Option.GetTemplate:
                    result = new ImportComponent().GetTemplate(parameters);
                    break;

                case Option.GetWorkflowPendingProcesses:
                    result = new WorkflowComponent().GetWorkflowPendingProcesses(parameters);
                    break;

                case Option.GetWorkflowPendingProcessDetail:
                    result = new WorkflowComponent().GetWorkflowPendingProcessDetail(parameters);
                    break;

                case Option.WorkflowProcessExecuteCommand:
                    result = new WorkflowComponent().WorkflowProcessExecuteCommand(parameters);
                    break;

                case Option.UpdateWorkflowProcess:
                    result = new WorkflowComponent().UpdateWorkflowProcess(parameters);
                    break;

                case Option.GetWorkflowTemplateList :
                    result = new WorkflowComponent().GetWorkflowTempleList(parameters);
                    break;
                case Option.SetDefaultWorkFlow:
                    result = new WorkflowComponent().SetDefaultWorkFlow(parameters);
                    break;
                case Option.SaveWorkFlowGraph:
                    result = new WorkflowComponent().SaveWorkFlowGraph(parameters);
                    break;
                case Option.GetNotes:
                    result = new NoteComponent().GetNotes(parameters);
                    break;

                case Option.AddUpdateNotes:
                    result = new NoteComponent().AddUpdateNotes(parameters);
                    break;

                case Option.UpdateAuditProcedureAnswer:
                    result = new AuditProcedureComponent().AddUpdateAuditProcedureAnswer(parameters);
                    break;

                case Option.GetTemplateProviders:
                    result = new ImportComponent().GetTemplateProviders(parameters);
                    break;

                case Option.GetTemplateCategories:
                    result = new ImportComponent().GetTemplateCategories(parameters);
                    break;

                case Option.GetTemplatesBy:
                    result = new ImportComponent().GetTemplates(parameters);
                    break;

                case Option.GetJobRoles:
                    result = new JobComponent().GetJobRoles(parameters);
                    break;

                case Option.GetJobsImportedData:
                    result = new ImportComponent().GetJobsImportedData(parameters);
                    break;

                case Option.AddUpdateJobsImportedData:
                    result = new JobComponent().AddUpdateJobsImportedData(parameters);
                    break;

                case Option.GetProcedureComments:
                    result = new AuditProcedureComponent().GetProcedureComments(parameters);
                    break;

                case Option.GetAuditProcedureCommentsByJob:
                    result = new AuditProcedureComponent().GetAuditProcedureCommentsByJob(parameters);
                    break;

                case Option.AddProcedureComments:
                    result = new AuditProcedureComponent().AddProcedureComments(parameters);
                    break;

                case Option.GetProcedureOmlForManagerReview:
                    result = new AuditProcedureComponent().GetProcedureOMLsByJobID(parameters);
                    break;

                case Option.GetProcedureOml:
                    result = new AuditProcedureComponent().GetProcedureOMLs(parameters);
                    break;

                case Option.GetProcedureTestSummary:
                    result = new AuditProcedureComponent().GetProcedureTestSummary(parameters);
                    break;
                case Option.SaveTestSummary:
                    result = new AuditProcedureComponent().SaveTestSummary(parameters);
                    break;
                case Option.AddEditProcedureOml:
                    result = new AuditProcedureComponent().AddEditProcedureOMLs(parameters);
                    break;

                case Option.GetAuditProcedureAttachments:
                    result = new AuditProcedureComponent().GetProcedureAttachment(parameters);
                    break;
                case Option.GetAuditProcedureOptionDetail:
                    result = new AuditProcedureComponent().GetAuditProcedureOptionDetail(parameters);
                    break;

                case Option.AddEditAuditProcedureAttachments:
                    result = new AuditProcedureComponent().AddEditAuditProcedureAttachments(parameters);
                    break;

                case Option.DeleteAuditProcedureAttachmentFile:
                    result = new AuditProcedureComponent().DeleteAuditProcedureAttachmentFile(parameters);
                    break;
                case Option.GetAuditProcedureEditContents:
                    result = new AuditProcedureComponent().GetAuditProcedureEditContents(parameters);
                    break;
                case Option.UpdateAuditProcedureEditContents:
                    result = new AuditProcedureComponent().UpdateAuditProcedureEditContents(parameters);
                    break;
                case Option.UpdateAuditProcedureTab:
                    result = new AuditProcedureComponent().UpdateAuditProcedureTab(parameters);
                    break;
                case Option.AddAuditProcedureTab:
                    result = new AuditProcedureComponent().AddAuditProcedureTab(parameters);
                    break;
                case Option.AddAuditProcedureTabContent:
                    result = new AuditProcedureComponent().AddAuditProcedureTabContent(parameters);
                    break;
                case Option.DeleteAuditProcedureTab:
                    result = new AuditProcedureComponent().DeleteAuditProcedureTab(parameters);
                    break;
                case Option.DeleteAuditProcedureTabContent:
                    result = new AuditProcedureComponent().DeleteAuditProcedureTabContent(parameters);
                    break;
                case Option.DownloadAttachmentFile:
                    result = new AuditProcedureComponent().DownloadAuditProcedureAttachmentFile(parameters);
                    break;

                case Option.GetJobFiles:
                    result = new JobComponent().GetJobFiles(parameters);
                    break;

                case Option.GetFirms:
                    result = new FirmComponent().GetFirms(parameters);
                    break;

                case Option.GetAccounts:
                    result = new FirmComponent().GetAccounts(parameters);
                    break;

                case Option.GetFunds:
                    result = new FirmComponent().GetFunds(parameters);
                    break;

                case Option.GetFirmDetail:
                    result = new FirmComponent().GetFirmDetail(parameters);
                    break;

                case Option.GetAccountDetail:
                    result = new FirmComponent().GetAccountDetail(parameters);
                    break;

                case Option.GetFundDetail:
                    result = new FirmComponent().GetFundDetail(parameters);
                    break;

                case Option.GetAccountManagers:
                    result = new AccountManagerComponent().GetAccountManagers(parameters);
                    break;

                case Option.UpdateLastLogin:
                    result = new UserComponent().UpdateLastLogin(parameters);
                    break;

                case Option.GetGroupDetail:
                    result = new FirmComponent().GetGroupDetail(parameters);
                    break;

                case Option.GetJobs:
                    result = new JobComponent().GetJobs(parameters);
                    break;

                case Option.GetJobImportData:
                    result = new JobComponent().GetJobImportData(parameters);
                    break;

                case Option.GetJobsImportedFiles:
                    result = new ImportComponent().GetJobsImportedFiles(parameters);
                    break;

                case Option.AddFirmNotes:
                    result = new NoteComponent().AddEntityNotes(parameters);
                    break;

                case Option.UpdateFirmNotes:
                    result = new NoteComponent().UpdateEntityNotes(parameters);
                    break;

                case Option.DeleteFirmNotes:
                    result = new NoteComponent().DeleteEntityNotes(parameters);
                    break;

                case Option.AddJobNotes:
                    result = new NoteComponent().AddJobNotes(parameters);
                    break;

                case Option.UpdateJobNotes:
                    result = new NoteComponent().UpdateJobNotes(parameters);
                    break;

                case Option.DeleteJobNotes:
                    result = new NoteComponent().DeleteJobNotes(parameters);
                    break;

                case Option.GetFirmNotes:
                    result = new NoteComponent().GetNote(parameters);
                    break;

                case Option.GetJobNotes:
                    result = new NoteComponent().GetNote(parameters);
                    break;

                case Option.HasMultipleAccess:
                    result = new FirmComponent().HasMultipleAccess(parameters);
                    break;

                case Option.HasMultipleJobAccess:
                    result = new JobComponent().HasMultipleJobAccess(parameters);
                    break;

                case Option.GetFundJobs:
                    result = new JobComponent().GetFundJobs(parameters);
                    break;

                case Option.HasMultipleUserAccess:
                    result = new AccountManagerComponent().HasMultipleUserAccess(parameters);
                    break;

                case Option.GetParents:
                    result = new FirmComponent().GetParents(parameters);
                    break;

                case Option.GetFirmCategories:
                    result = new FirmComponent().GetFirmCategories(parameters);
                    break;

                case Option.DeleteFirm:
                    result = new FirmComponent().DeleteFirm(parameters);
                    break;

                case Option.UnlockUser:
                    result = new UserComponent().Unlock(parameters);
                    break;
                case Option.GetJobFlowStatusList:
                    result = new JobComponent().GetJobFlowStatusList(parameters);
                    break;
                case Option.GetWorkFlowGraph:
                    result = new WorkflowComponent().GetWorkflowGraph(parameters);
                    break;

                case Option.GetScheduledJobs:
                    result = new JobComponent().GetScheduledJobs(parameters);
                    break;

                case Option.GetCompletedJobs:
                    result = new JobComponent().GetCompletedJobs(parameters);
                    break;

                case Option.GetJobToBeCompleted:
                    result = new JobComponent().GetJobToBeCompleted(parameters);
                    break;

                    #region Job Work Log

                case Option.GetWorkLogs:
                    result = new JobComponent().GetWorkLogs(parameters);
                    break;

                case Option.GetWorkLogAccountManagers:
                    result = new JobComponent().GetWorkLogAccountManagers(parameters);
                    break;

                case Option.AddWorkLog:
                    result = new JobComponent().AddWorkLog(parameters);
                    break;

                case Option.UpdateWorkLog:
                    result = new JobComponent().UpdateWorkLog(parameters);
                    break;

                case Option.DeleteWorkLog:
                    result = new JobComponent().DeleteWorkLog(parameters);
                    break;

                    #endregion

                #region Reports
                case Option.AuditReports:
                    result = new ReportComponent().GenerateAuditReports(parameters);
                    break;
                #endregion


            }
            return result;
        }
    }
}
