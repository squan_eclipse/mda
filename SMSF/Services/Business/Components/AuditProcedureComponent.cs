﻿using System.Collections.ObjectModel;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Extensions;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using FileInfo = DBASystem.SMSF.Contracts.ViewModels.FileInfo;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class AuditProcedureComponent : Component
    {
        private UnitOfWork uow = new UnitOfWork();
        private IRepository<Jobs> repository;
        private IRepository<AuditProcedureAnswer> answerRepository;
        private IRepository<AuditProcedureComment> commentsRepository;
        private IRepository<AuditProcedureOml> omlRepository;
        private IRepository<TestSummary> testSummary;
        private IRepository<TestSummaryAnswer> testSummaryAnswer;
        private IRepository<AuditProcedureAttachment> attachmentsRepository;
        private List<AuditProcedureOption> _auditProcedureOption;
        private List<AuditProcedureOptionDetail> _auditProcedureOptionDetail;
        private List<AuditProcedureColumn> _auditProcedureColumn;
        private IRepository<AccountManagers> _accountmanageRepository; 

        public AuditProcedureComponent()
        {
            repository = uow.Repository<Jobs>();
            answerRepository = uow.Repository<AuditProcedureAnswer>();
            commentsRepository = uow.Repository<AuditProcedureComment>();
            omlRepository = uow.Repository<AuditProcedureOml>();
            testSummary = uow.Repository<TestSummary>();
            attachmentsRepository = uow.Repository<AuditProcedureAttachment>();
            testSummaryAnswer = uow.Repository<TestSummaryAnswer>();
            _accountmanageRepository = uow.Repository<AccountManagers>();
        }

        #region AuditProcedures Section

        public string GetWorkpaperMenu(string xml)
        {
            var jobMainMenuRequestedIds = (ManageWorkPaperModel)ModelDeserializer.DeserializeFromXml<ManageWorkPaperModel>(xml);
            return GetWorkpaperMenu(jobMainMenuRequestedIds);
        }
        
        public string GetWorkpaperMenu(ManageWorkPaperModel manageWorkPaperModel)
        {
            //For Workpaper Tabs

            if (manageWorkPaperModel.AccountId == 0)
                manageWorkPaperModel.AccountId = uow.Repository<Entities>().GetById(uow.Repository<Jobs>().GetById(manageWorkPaperModel.JobId).FundID).ParentID ?? 0;

            var auditProcTabsByEntity = uow.Repository<EntityAuditProcedureTab>().GetAll().Where(a => a.EntityID == manageWorkPaperModel.AccountId).ToList();

            if (auditProcTabsByEntity.Count == 0)
            {
                AddEntityAuditProcedureTab(manageWorkPaperModel.AccountId);
            }

            var tabViewModel = uow.Repository<AuditProcedureTab>()
                .GetAll()
                .Where(a => a.IsActive && a.EntityAuditProcedureTabs.Any(e => e.EntityID == manageWorkPaperModel.AccountId))
                .Select(x => new AuditProcedureTabViewModel
                {
                    Id = x.ID,
                    Caption = x.Caption,
                    ContentType = x.ContentType,
                    IsViewable = x.IsViewable
                }).ToList();

            var model = new WorkpaperMenuViewModel { WorkPapers = tabViewModel };
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }
        
        private void AddEntityAuditProcedureTab(int accountId)
        {
            var auditProcTabs = uow.Repository<AuditProcedureTab>().GetAll().Where(a => a.IsActive && a.EntityId == null);
            foreach (var auditProcedureTab in auditProcTabs)
            {
                AddEntityAuditProcedureTab(auditProcedureTab.ID, accountId);
            }
        }

        private void AddEntityAuditProcedureTab(int auditProcTabId, int accountId)
        {
            var entityAuditProcedureTab = new EntityAuditProcedureTab
            {
                AuditProcedureTabID = auditProcTabId,
                EntityID = accountId
            };
            uow.Repository<EntityAuditProcedureTab>().Add(entityAuditProcedureTab);
            uow.Commit();
        }

        public string GetAuditProcedureTabs(string xml)
        {
            var viewModel = (ManageWorkPaperModel)ModelDeserializer.DeserializeFromXml<ManageWorkPaperModel>(xml);
            var tabs = uow.Repository<AuditProcedureTab>().GetAll().Where(a => a.IsActive && a.EntityAuditProcedureTabs.Any(e => e.EntityID == viewModel.AccountId))
                 .Select(t => new AuditProcedureTabViewModel
                 {
                     Id = t.ID,
                     Caption = t.Caption,
                     EntityId = t.EntityId,
                     TabOrder = t.TabOrder,
                     IsActive = t.IsActive,
                     ContentType = WorkpaperContentType.AuditProcedure,
                     IsViewable = t.IsViewable
                 }).ToList();

            return ModelSerializer.GenerateReponseXml(tabs, ResponseStatus.Success, "Successfull");
        }

        public string UpdateAuditProcedureTab(string xml)
        {
            var viewModel = (AuditProcedureTabViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureTabViewModel>(xml);
            if (viewModel != null)
            {
                var model = uow.Repository<AuditProcedureTab>().GetById(viewModel.Id);
                if (model != null)
                {
                    model.Caption = viewModel.Caption;
                    model.EntityId = viewModel.EntityId;
                    model.TabOrder = viewModel.TabOrder;
                    model.IsActive = viewModel.IsActive;
                    model.ContentType = viewModel.ContentType.Value;
                    model.IsViewable = viewModel.IsViewable;
                    uow.Repository<AuditProcedureTab>().Update(model);
                    uow.Commit();

                    return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successfull");
                }
            }

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, "Error");
        }

        public string AddAuditProcedureTabContent(string xml)
        {
            var model = (AuditProcedureTabContentViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureTabContentViewModel>(xml);
            return model != null ? AddAuditProcedureTabContent(model) : NullModelReference();
        }

        private string AddAuditProcedureTabContent(AuditProcedureTabContentViewModel viewModel)
        {
            if (viewModel != null)
            {
                var auditProcedureTabContent = new AuditProcedureTabContent
                                               {
                                                   ID = uow.Repository<AuditProcedureTabContent>().GetAll().Max(x => x.ID) + 1,
                                                   AuditProcedureTabId = viewModel.AuditProcedureTabId,
                                                   Caption = viewModel.Caption,
                                                   Order = viewModel.Order,
                                                   IsActive = viewModel.IsActive,
                                                   ContentType = viewModel.ContentType,
                                                   ControlType = viewModel.ControlType,
                                                   IsViewable = viewModel.IsViewable,
                                                   EntityId = viewModel.EntityId
                                               };
                uow.Repository<AuditProcedureTabContent>().Add(auditProcedureTabContent);
                uow.Commit();

                var newAuditProcedureTabContentId = auditProcedureTabContent.ID;
                AddEntityAuditProcedureTabContent(newAuditProcedureTabContentId, viewModel.EntityId);

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successfull");
            }
            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, "Error");
        }

        private void AddEntityAuditProcedureTabContent(int auditProcTabContentId, int accountId)
        {
            var entityAuditProcedureTabContent = new EntityAuditProcedureTabContent
                                                 {
                                                     AuditProcedureTabContentID = auditProcTabContentId,
                                                     EntityID = accountId
                                                 };
            uow.Repository<EntityAuditProcedureTabContent>().Add(entityAuditProcedureTabContent);
            uow.Commit();
        }

        public string AddAuditProcedureTab(string xml)
        {
            var model = (AuditProcedureTabViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureTabViewModel>(xml);
            return model != null ? AddAuditProcedureTab(model) : NullModelReference();
        }

        private string AddAuditProcedureTab(AuditProcedureTabViewModel viewModel)
        {
            if (viewModel != null)
            {
                var auditProcedureTab = new AuditProcedureTab
                                        {
                                            ID = uow.Repository<AuditProcedureTab>().GetAll().Max(x => x.ID) + 1,
                                            Caption = viewModel.Caption,
                                            EntityId = viewModel.EntityId,
                                            TabOrder = viewModel.TabOrder,
                                            IsActive = viewModel.IsActive,
                                            IsViewable = viewModel.IsViewable,
                                            ContentType = viewModel.ContentType.Value
                                        };
                uow.Repository<AuditProcedureTab>().Add(auditProcedureTab);
                uow.Commit();

                var auditProcTabId = auditProcedureTab.ID;
                if (viewModel.EntityId != null)
                {
                    var accountId = (int) viewModel.EntityId;
                    AddEntityAuditProcedureTab(auditProcTabId, accountId);
                }

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successfull");
            }
            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, "Error");
        }
        
        public string DeleteAuditProcedureTab(string xml)
        {
            var model = (ManageWorkPaperModel)ModelDeserializer.DeserializeFromXml<ManageWorkPaperModel>(xml);
            return model != null ? DeleteAuditProcedureTab(model) : NullModelReference();
        }

        public string DeleteAuditProcedureTabContent(string xml)
        {
            var model = (ManageWorkPaperModel)ModelDeserializer.DeserializeFromXml<ManageWorkPaperModel>(xml);
            return model != null ? DeleteAuditProcedureTabContent(model) : NullModelReference();
        }

        private string DeleteAuditProcedureTab(ManageWorkPaperModel model)
        {
            var entity = uow.Repository<EntityAuditProcedureTab>().GetAll().FirstOrDefault(e => e.AuditProcedureTabID == model.AuditProcTabId && e.EntityID == model.AccountId);
            if (entity != null)
            {
                uow.Repository<EntityAuditProcedureTab>().Delete(entity);
                uow.Commit();

                var auditProcTabContents = uow.Repository<EntityAuditProcedureTabContent>().GetAll().Where(e => e.EntityID == model.AccountId).ToList();
                DeleteEntityAuditProcedureTabContents(auditProcTabContents);

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
            }
            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, Resource_EN.Error_UnableToDelete);

        }

        private string DeleteAuditProcedureTabContent(ManageWorkPaperModel model)
        {

            var entity = uow.Repository<EntityAuditProcedureTabContent>().GetAll().FirstOrDefault(e => e.AuditProcedureTabContentID == model.AuditProcTabContentId && e.EntityID == model.AccountId);
            if (entity != null)
            {
                uow.Repository<EntityAuditProcedureTabContent>().Delete(entity);
                uow.Commit();
                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
            }
            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, Resource_EN.Error_UnableToDelete);


            //var entity = uow.Repository<AuditProcedureTabContent>().GetById(model.AuditProcTabId);
            //if (entity != null)
            //{
            //    entity.IsActive = false;
            //    uow.Commit();
            //    return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
            //}
            //return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, Resource_EN.Error_UnableToDelete);
        }

        public string GetAuditProcedureEditContents(string xml)
        {
            var query = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            if (query != null)
            {
                var model = uow.Repository<AuditProcedure>().GetById(query.Value);
                var viewModel = new AuditProcedureViewModel
                {
                    Id = model.ID,
                    AuditProcedureTabContentId = model.AuditProcedureTabContentId,
                    Title = model.Title,
                    Attachments = model.Attachments,
                    Oml = model.Oml,
                    Comments = model.Comments,
                    ControlType = model.ControlType,
                    AnswerControlType = model.AnswerControlType,
                    Checkbox = model.Checkbox,
                    AuditProcedureOption = model.AuditProcedureOption.Select(x => new AuditProcedureOptionViewModel
                    {
                        Id = x.ID,
                        AuditProcedureId = x.AuditProcedureId,
                        AuditProcedureOptionDetailId = x.AuditProcedureOptionDetailId
                    }).ToList(),
                    ParentId = model.ParentId
                };

                return ModelSerializer.GenerateReponseXml(viewModel, ResponseStatus.Success, "Successfull");
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Error, "Error");
        }

        public string UpdateAuditProcedureEditContents(string xml)
        {
            var viewModel = (AuditProcedureViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureViewModel>(xml);
            return viewModel != null ? UpdateAuditProcedureEditContents(viewModel) : NullModelReference();
        }

        private string UpdateAuditProcedureEditContents(AuditProcedureViewModel viewModel)
        {
            var model = uow.Repository<AuditProcedure>().GetById(viewModel.Id);
            if (model != null)
            {
                model.Title = viewModel.Title;
                model.AnswerControlType = viewModel.AnswerControlType;
                uow.Repository<AuditProcedure>().Update(model);
                uow.Commit();

                if (viewModel.IsOptionUpdated)
                    UpdateAuditProcedureOption(viewModel.Id, viewModel.OptionDetailIds);

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successfull");
            }

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, "Error");
        }

        public string AddAuditProcedure(string xml)
        {
            var model = (AuditProcedureViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureViewModel>(xml);
            return model != null ? AddAuditProcedure(model) : NullModelReference();
        }

        private string AddAuditProcedure(AuditProcedureViewModel model)
        {
            var auditProcedureId = !uow.Repository<AuditProcedure>().GetAll().Any() ? 1 : uow.Repository<AuditProcedure>().GetAll().Max(x => x.ID) + 1;

            var entity = new AuditProcedure
            {
                ID = auditProcedureId,
                Title = model.Title,
                AuditProcedureTabContentId = model.AuditProcedureTabContentId,
                ControlType = model.ControlType,
                ParentId = model.ParentId,
                AnswerControlType = model.AnswerControlType,
                StatusID = Status.Active
            };

            uow.Repository<AuditProcedure>().Add(entity);
            uow.Commit();

            AddAuditProcedureOption(auditProcedureId, model.OptionDetailIds);

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successfull");
        }

        private void AddAuditProcedureOption(int auditProcedureId, IEnumerable<string> optionDetailIds)
        {
            foreach (var optionDetailId in optionDetailIds)
            {
                var optionId = !uow.Repository<AuditProcedureOption>().GetAll().Any() ? 1 : uow.Repository<AuditProcedureOption>().GetAll().Max(x => x.ID) + 1;

                var entity = new AuditProcedureOption
                {
                    AuditProcedureId = auditProcedureId,
                    AuditProcedureOptionDetailId = int.Parse(optionDetailId),
                    ID = optionId
                };

                uow.Repository<AuditProcedureOption>().Add(entity);
                uow.Commit();
            }
        }

        private void UpdateAuditProcedureOption(int auditProcedureId, IEnumerable<string> optionDetailIds)
        {
            var entities = uow.Repository<AuditProcedureOption>().GetAll().Where(a => a.AuditProcedureId == auditProcedureId);

            foreach (var entity in entities)
            {
                DeleteAuditProcedureOption(entity);
            }

            AddAuditProcedureOption(auditProcedureId, optionDetailIds);
        }

        private string DeleteAuditProcedureOption(AuditProcedureOption entity)
        {
            if (entity != null)
            {
                uow.Repository<AuditProcedureOption>().Delete(entity);
                uow.Commit();
                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
            }
            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, Resource_EN.Error_UnableToDelete);
        }

        public string DeleteAuditProcedure(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            return model != null ? DeleteAuditProcedure(model.Key) : NullModelReference();
        }

        private string DeleteAuditProcedure(int id)
        {
            var entity = uow.Repository<AuditProcedure>().GetById(id);
            if (entity != null)
            {
                entity.StatusID = Status.Deleted;
                uow.Commit();
                DeleteAuditProcedureRec(entity.Childs.ToList());

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
            }
            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, Resource_EN.Error_UnableToDelete);
        }

        private void DeleteAuditProcedureRec(List<AuditProcedure> child)
        {
            foreach (var auditProcedure in child)
            {
                auditProcedure.StatusID = Status.Deleted;
                uow.Commit();

                if (auditProcedure.Childs.Any())
                {
                    DeleteAuditProcedureRec(auditProcedure.Childs.ToList());
                }

            }
        }

        public string UpdateAuditProcedureTabContent(string xml)
        {
            var viewModel = (AuditProcedureTabContentViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureTabContentViewModel>(xml);
            if (viewModel != null)
            {
                var model = uow.Repository<AuditProcedureTabContent>().GetById(viewModel.Id);
                if (model != null)
                {
                    model.Caption = viewModel.Caption;
                    model.Order = viewModel.Order;
                    model.IsActive = viewModel.IsActive;
                    model.ContentType = viewModel.ContentType;
                    model.ControlType = viewModel.ControlType;
                    model.IsViewable = viewModel.IsViewable;
                    uow.Repository<AuditProcedureTabContent>().Update(model);
                    uow.Commit();
                    return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successfull");
                }
            }

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = false }, ResponseStatus.Error, "Error");
        }

        public string GetAuditProcedureTabContent(string xml)
        {
            var model = (ManageWorkPaperModel) ModelDeserializer.DeserializeFromXml<ManageWorkPaperModel>(xml);
            if (model != null)
                return GetAuditProcedureTabContent(model);
            return NullModelReference();
        }
        public string GetAuditProcedureTabContents(string xml)
        {

             var model = (ManageWorkPaperModel)ModelDeserializer.DeserializeFromXml<ManageWorkPaperModel>(xml);
            if (model != null)
                return GetAuditProcedureTabContents(model);
            return NullModelReference();
        }
        private string GetAuditProcedureTabContents(ManageWorkPaperModel manageWorkPaperModel)
        {
            List<AuditProcedureTabContentViewModel> tabsVm;
            if (manageWorkPaperModel.AuditProcTabId == 50)
            {
                //For Import Data Tab
                tabsVm = uow.Repository<JobImportedData>().GetAll().Where(p => p.JobId == manageWorkPaperModel.JobId).Select(jid =>
                    new AuditProcedureTabContentViewModel
                    {
                        Id = jid.ID,
                        Caption = jid.Template.Name,
                        ContentType = WorkpaperContentType.Import,
                        //ControlType = jid.ControlType,
                        TemplateId = jid.TemplateId,
                        Order = 0,
                        IsActive = true,
                    }).ToList();
            }
            else
            {
                _auditProcedureOption = uow.Repository<AuditProcedureOption>().GetAll().ToList();
                _auditProcedureOptionDetail = uow.Repository<AuditProcedureOptionDetail>().GetAll().ToList();
                _auditProcedureColumn = uow.Repository<AuditProcedureColumn>().GetAll().ToList();

                if (manageWorkPaperModel.AccountId == 0)
                    manageWorkPaperModel.AccountId = uow.Repository<Entities>().GetById(uow.Repository<Jobs>().GetById(manageWorkPaperModel.JobId).FundID).ParentID ?? 0;

                var auditProcTabContentByEntity = uow.Repository<EntityAuditProcedureTabContent>()
                    .GetAll()
                    .Where(a => a.EntityID == manageWorkPaperModel.AccountId && a.AuditProcedureTabContent.AuditProcedureTabId == manageWorkPaperModel.AuditProcTabId).ToList();


                if (auditProcTabContentByEntity.Count == 0)
                {
                    AddEntityAuditProcedureTabContent(manageWorkPaperModel, manageWorkPaperModel.AccountId);
                }

                tabsVm = uow.Repository<AuditProcedureTabContent>()
                .GetAll()
                .Where(tc => tc.AuditProcedureTabId == manageWorkPaperModel.AuditProcTabId && tc.EntityAuditProcedureTabContents.Any(e => e.EntityID == manageWorkPaperModel.AccountId))
                    .OrderBy(ob => ob.Order)
                    .ThenBy(tb => tb.Caption)
                    .AsEnumerable().Select(ToTabContentsViewModel).ToList();

                var auditProcedureAnswer = uow.Repository<AuditProcedureAnswer>().GetAll().Where(p => p.JobId == manageWorkPaperModel.JobId).ToList();

                //For Answers Mapping
                foreach (var item in tabsVm)
                {
                    SetProcedureAnswer(manageWorkPaperModel.JobId, item.AuditProcedure, auditProcedureAnswer);
                }
            }

            return ModelSerializer.GenerateReponseXml(tabsVm, ResponseStatus.Success, "Successfull");

        }

        private void AddEntityAuditProcedureTabContent(ManageWorkPaperModel model, int accountId)
        {
            var tabContents = uow.Repository<AuditProcedureTabContent>().GetAll().Where(tc => tc.AuditProcedureTabId == model.AuditProcTabId && tc.EntityId == null);
            foreach (var tabContent in tabContents)
            {
                AddEntityAuditProcedureTabContent(tabContent.ID, accountId);
            }
        }

        private string GetAuditProcedureTabContent(ManageWorkPaperModel manageWorkPaperModel)
        {
            //For Import Data Tab
            var tabsVm = uow.Repository<AuditProcedureTabContent>()
                .GetAll()
                .Where(p => p.AuditProcedureTabId == manageWorkPaperModel.AuditProcTabId && p.IsActive && p.EntityAuditProcedureTabContents.Any(e => e.EntityID == manageWorkPaperModel.AccountId))
                .Select(jid =>
                    new AuditProcedureTabContentViewModel
                    {
                        Id = jid.ID,
                        Caption = jid.Caption,
                        Order = jid.Order,
                        IsActive = true,
                        AuditProcedureTabId = jid.AuditProcedureTabId,
                        ContentType = jid.ContentType,
                        ControlType = jid.ControlType,
                        IsViewable = jid.IsViewable
                    }).ToList();
            return ModelSerializer.GenerateReponseXml(tabsVm, ResponseStatus.Success, "Successfull");
        }

        #endregion AuditProcedures Section

        #region Workpaper Template

        public string SaveWorkpaperTemplate(string xml)
        {
            var model = (WorkpaperTemplateModel)ModelDeserializer.DeserializeFromXml<WorkpaperTemplateModel>(xml);
            return model != null ? SaveWorkpaperTemplate(model) : NullModelReference();
        }
        private string SaveWorkpaperTemplate(WorkpaperTemplateModel model)
        {
            var auditProcTabs = uow.Repository<EntityAuditProcedureTab>().GetAll().Where(e => e.EntityID == model.AccountId);
            var auditProcTabContents = uow.Repository<EntityAuditProcedureTabContent>().GetAll().Where(e => e.EntityID == model.AccountId);

            //Adding New Template
            var auditProcedureTemplate = new AuditProcedureTemplate
            {
                Name = model.Name,
                EntityID = model.AccountId,
                CreatedOn = DateTime.Now,
                CreatedBy = model.CreatedBy
            };
            uow.Repository<AuditProcedureTemplate>().Add(auditProcedureTemplate);
            uow.Commit();

            int templateId = uow.Repository<AuditProcedureTemplate>().GetAll().Max(x => x.ID);

            //Adding TabTemplate Details
            foreach (var auditProcTab in auditProcTabs)
            {
                var apTabTemplateDetail = new AuditProcedureTabTemplateDetail
                {
                    AuditProcedureTabID = auditProcTab.AuditProcedureTabID,
                    AuditProcedureTemplateID = templateId
                };
                uow.Repository<AuditProcedureTabTemplateDetail>().Add(apTabTemplateDetail);
                uow.Commit();
            }

            //Adding TabContentTemplate Details
            foreach (var auditProcTabContent in auditProcTabContents)
            {
                var apTabContentTemplateDetail = new AuditProcedureTabContentTemplateDetail
                {
                    AuditProcedureTabContentID = auditProcTabContent.AuditProcedureTabContentID,
                    AuditProcedureTemplateID = templateId
                };
                uow.Repository<AuditProcedureTabContentTemplateDetail>().Add(apTabContentTemplateDetail);
                uow.Commit();
            }

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
        }
        public string SaveWorkpaperTemplateAsXml(string xml)
        {
            var model = (WorkpaperTemplateModel)ModelDeserializer.DeserializeFromXml<WorkpaperTemplateModel>(xml);
            return model != null ? SaveWorkpaperTemplateAsXml(model) : NullModelReference();
        }
        private string SaveWorkpaperTemplateAsXml(WorkpaperTemplateModel model)
        {
            var auditProcTabs = uow.Repository<AuditProcedureTab>()
                    .GetAll()
                    .Where(apt => apt.IsActive && apt.EntityAuditProcedureTabs.Any(a => a.EntityID == model.AccountId))
                    .Select(aptt => new AuditProcedureTabTemplates
                    {
                        ID = aptt.ID,
                        Caption = aptt.Caption,
                        ContentType = (int)aptt.ContentType,
                        EntityId = aptt.EntityId,
                        IsActive = aptt.IsActive,
                        IsViewable = aptt.IsViewable,
                        TabOrder = aptt.TabOrder,
                        AuditProcedureTabContents = aptt.AuditProcedureTabContent
                            .Where(b => b.EntityAuditProcedureTabContents.Any(c => c.EntityID == model.AccountId))
                            .Select(aptct => new AuditProcedureTabContentTemplates
                            {
                                ID = aptct.ID,
                                AuditProcedureTabId = aptct.AuditProcedureTabId,
                                Caption = aptct.Caption,
                                ContentType = (int)aptct.ContentType,
                                ControlType = (int?)aptct.ControlType,
                                EntityId = aptct.EntityId,
                                IsActive = aptct.IsActive,
                                IsViewable = aptct.IsViewable,
                                Order = aptct.Order,
                                TemplateId = aptct.TemplateId,
                                AuditProcedures = aptct.AuditProcedure
                                    .Where(d => d.AuditProcedureTabContent.EntityAuditProcedureTabContents.Any(e => e.EntityID == model.AccountId))
                                    .Select(apts => new AuditProcedureTemplates
                                    {
                                        ID = apts.ID,
                                        Title = apts.Title,
                                        ParentId = apts.ParentId,
                                        Attachments = apts.Attachments,
                                        StatusID = (int)apts.StatusID,
                                        ControlType = (int?)apts.ControlType,
                                        AnswerControlType = (int?)apts.AnswerControlType,
                                        Oml = apts.Oml,
                                        Checkbox = apts.Checkbox,
                                        Comments = apts.Comments,
                                        AuditProcedureTabContentId = apts.AuditProcedureTabContentId,
                                        AuditProcedureOption = apts.AuditProcedureOption
                                            .Select(apoptions => new AuditProcedureOptionTemplates
                                            {
                                                ID = apoptions.ID,
                                                AuditProcedureId = apoptions.AuditProcedureId,
                                                AuditProcedureOptionDetailId = apoptions.AuditProcedureOptionDetailId,
                                                AuditProcedureOptionDetail = new AuditProcedureOptionDetailTemplates
                                                {
                                                    ID = apoptions.ID,
                                                    Field = apoptions.AuditProcedureOptionDetail.Field,
                                                    Value = apoptions.AuditProcedureOptionDetail.Value
                                                }
                                            }).ToList()
                                    }).ToList()

                            }).ToList()
                    }).ToList();
            
            var serializer = new XmlSerializer(typeof(List<AuditProcedureTabTemplates>));
            var stream = new MemoryStream();
            serializer.Serialize(stream, auditProcTabs);
            var fileData = Convert.ToBase64String(stream.ToArray());
            stream.Flush();
            stream.Close();

            return ModelSerializer.GenerateReponseXml(new GenericModel<string> { Value = fileData }, ResponseStatus.Success, "Successful");
        }
        public string LoadWorkpaperTemplateFromXml(string xml)
        {
            var model = (DictionaryModel<string, string>)ModelDeserializer.DeserializeFromXml<DictionaryModel<string, string>>(xml);
            return model != null ? LoadWorkpaperTemplateFromXml(model.Key, model.Value) : NullModelReference();
        }
        private string LoadWorkpaperTemplateFromXml(string fileName, string filePath)
        {
            //Loading XML to Object
            List<AuditProcedureTabTemplates> auditProcTabsTemplate;

            try
            {
                var serializer = new XmlSerializer(typeof (List<AuditProcedureTabTemplates>));
                using (XmlReader reader = XmlReader.Create(fileName))
                {
                    auditProcTabsTemplate = (List<AuditProcedureTabTemplates>) serializer.Deserialize(reader);
                    reader.Close();
                }
            }
            catch (Exception)
            {
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Warning, "Xml template file is empty or corrupt");
            }

            try
            {

                var auditProcTabRepository = uow.Repository<AuditProcedureTab>();
                var auditProcTabContentRepository = uow.Repository<AuditProcedureTabContent>();
                var auditProcedureRepository = uow.Repository<AuditProcedure>();
                var auditProcOptionRepository = uow.Repository<AuditProcedureOption>();

                #region Removing Old Data

                DeleteAuditProcedureTabContentTemplateDetails();
                DeleteAuditProcedureTabTemplateDetails();
                DeleteAuditProcedureTemplates();
                DeleteEntityAuditProcedureTabContents();
                DeleteEntityAuditProcedureTabs();
                DeleteAuditProcedureTabContents(auditProcTabContentRepository);
                DeleteAuditProcedureTabs(auditProcTabRepository);

                uow.Commit();

                #endregion

                #region Adding New Data

                foreach (var auditProcTabTemplate in auditProcTabsTemplate)
                {
                    var auditProcedureTab = new AuditProcedureTab
                    {
                        ID = auditProcTabTemplate.ID,
                        Caption = auditProcTabTemplate.Caption,
                        TabOrder = auditProcTabTemplate.TabOrder,
                        IsActive = auditProcTabTemplate.IsActive,
                        IsViewable = auditProcTabTemplate.IsViewable,
                        ContentType = (WorkpaperContentType) auditProcTabTemplate.ContentType
                    };
                    auditProcTabRepository.Add(auditProcedureTab);

                    foreach (var auditProcTabContentTemplate in auditProcTabTemplate.AuditProcedureTabContents)
                    {
                        AddAuditProcedureTabContent(auditProcTabContentTemplate, auditProcTabContentRepository);
                        foreach (var auditProcTemplate in auditProcTabContentTemplate.AuditProcedures)
                        {
                            AddAuditProcedure(auditProcTemplate, auditProcedureRepository);

                            foreach (var auditProcOption in auditProcTemplate.AuditProcedureOption)
                            {
                                AddAuditProcedureOption(auditProcOption, auditProcOptionRepository);
                            }
                        }
                    }
                }

                uow.Commit();

                #endregion

                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> {Value = true}, ResponseStatus.Success, "Successful");
            }
            catch (Exception)
            {
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Warning, "Xml template file is empty or corrupt");
            }
        }


        private void AddAuditProcedureTabContent(AuditProcedureTabContentTemplates auditProcTabContentTemplate, IRepository<AuditProcedureTabContent> auditProcedureTabContentRepository)
        {
            var auditProcedureTabContent = new AuditProcedureTabContent
            {
                ID = auditProcTabContentTemplate.ID,
                AuditProcedureTabId = auditProcTabContentTemplate.AuditProcedureTabId,
                Caption = auditProcTabContentTemplate.Caption,
                Order = auditProcTabContentTemplate.Order,
                IsActive = auditProcTabContentTemplate.IsActive,
                ContentType = (WorkpaperContentType) auditProcTabContentTemplate.ContentType,
                ControlType = (ControlType?) auditProcTabContentTemplate.ControlType,
                IsViewable = auditProcTabContentTemplate.IsViewable
            };
            auditProcedureTabContentRepository.Add(auditProcedureTabContent);
        }
        private void AddAuditProcedure(AuditProcedureTemplates auditProcedureTemplate, IRepository<AuditProcedure> auditProcedureRepository)
        {
            var auditProcedure = new AuditProcedure
            {
                AnswerControlType = (ControlType?)auditProcedureTemplate.AnswerControlType,
                AuditProcedureTabContentId = auditProcedureTemplate.AuditProcedureTabContentId,
                Checkbox = auditProcedureTemplate.Checkbox,
                Comments = auditProcedureTemplate.Comments,
                ControlType = (ControlType?)auditProcedureTemplate.ControlType,
                ID = auditProcedureTemplate.ID,
                Oml = auditProcedureTemplate.Oml,
                ParentId = auditProcedureTemplate.ParentId,
                StatusID = (Status)auditProcedureTemplate.StatusID,
                Title = auditProcedureTemplate.Title,
                Attachments = auditProcedureTemplate.Attachments
            };
            auditProcedureRepository.Add(auditProcedure);
        }
        private void AddAuditProcedureOption(AuditProcedureOptionTemplates auditProcOptionTemplate, IRepository<AuditProcedureOption> auditProcOptionRepository)
        {
            var auditProcedureOption = new AuditProcedureOption
            {
                ID = auditProcOptionTemplate.ID,
                AuditProcedureId = auditProcOptionTemplate.AuditProcedureId,
                AuditProcedureOptionDetailId = auditProcOptionTemplate.AuditProcedureOptionDetailId
            };
            auditProcOptionRepository.Add(auditProcedureOption);
        }
        private void DeleteAuditProcedureTabs(IRepository<AuditProcedureTab> auditProcTabRepository)
        {
            foreach (var auditProcedureTab in auditProcTabRepository.GetAll())
            {
                auditProcTabRepository.Delete(auditProcedureTab);
            }
        }
        private void DeleteAuditProcedureTabContents(IRepository<AuditProcedureTabContent> auditProcTabContentRepository)
        {
            foreach (var auditProcedureTabContent in auditProcTabContentRepository.GetAll())
            {
                auditProcTabContentRepository.Delete(auditProcedureTabContent);
            }
        }
        private void DeleteEntityAuditProcedureTabs()
        {
            var entityAuditProcedureTabRepository = uow.Repository<EntityAuditProcedureTab>();
            foreach (var entityAuditProcedureTab in entityAuditProcedureTabRepository.GetAll())
            {
                entityAuditProcedureTabRepository.Delete(entityAuditProcedureTab);
            }
        }
        private void DeleteEntityAuditProcedureTabs(List<EntityAuditProcedureTab> entityAuditProcTabs)
        {
            var entityAuditProcedureTabRepository = uow.Repository<EntityAuditProcedureTab>();
            foreach (var entityAuditProcedureTab in entityAuditProcTabs)
            {
                entityAuditProcedureTabRepository.Delete(entityAuditProcedureTab);
            }

            uow.Commit();
        }
        private void DeleteEntityAuditProcedureTabContents()
        {
            var entityAuditProcedureTabContentRepository = uow.Repository<EntityAuditProcedureTabContent>();
            foreach (var entityAuditProcedureTabContent in entityAuditProcedureTabContentRepository.GetAll())
            {
                entityAuditProcedureTabContentRepository.Delete(entityAuditProcedureTabContent);
            }
        }
        private void DeleteEntityAuditProcedureTabContents(List<EntityAuditProcedureTabContent> entityAuditProcTabContents)
        {
            var entityAuditProcedureTabContentRepository = uow.Repository<EntityAuditProcedureTabContent>();
            foreach (var entityAuditProcedureTabContent in entityAuditProcTabContents)
            {
                entityAuditProcedureTabContentRepository.Delete(entityAuditProcedureTabContent);
            }

            uow.Commit();
        }
        private void DeleteAuditProcedureTabTemplateDetails()
        {
            var tabTemplateDetailRepository = uow.Repository<AuditProcedureTabTemplateDetail>();
            foreach (var apTabTemplateDetail in tabTemplateDetailRepository.GetAll())
            {
                tabTemplateDetailRepository.Delete(apTabTemplateDetail);
            }
        }
        private void DeleteAuditProcedureTabContentTemplateDetails()
        {
            var tabContentTemplateDetailRepository = uow.Repository<AuditProcedureTabContentTemplateDetail>();
            foreach (var apTabContentTemplateDetail in tabContentTemplateDetailRepository.GetAll())
            {
                tabContentTemplateDetailRepository.Delete(apTabContentTemplateDetail);
            }
        }
        private void DeleteAuditProcedureTemplates()
        {
            var templateRepository = uow.Repository<AuditProcedureTemplate>();
            foreach (var apTemplate in templateRepository.GetAll())
            {
                templateRepository.Delete(apTemplate);
            }
        }
        public string GetWorkpaperTemplates(string xml)
        {
            var model = (DictionaryModel<string, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<string, int>>(xml);
            return model != null ? GetWorkpaperTemplates(model.Value) : NullModelReference();
        }
        private string GetWorkpaperTemplates(int accountId)
        {
            var viewModel = uow.Repository<AuditProcedureTemplate>().GetAll().Where(x => x.EntityID == accountId)
                 .AsEnumerable().Select(t => new WorkpaperTemplateModel
                 {
                     ID = t.ID,
                     Name = t.Name,
                     CreatedBy = t.CreatedBy,
                     CreatedOn = t.CreatedOn,
                     AccountId = t.EntityID
                 }).ToList();

            return ModelSerializer.GenerateReponseXml(viewModel, ResponseStatus.Success, "Successfull");
        }
        public string LoadWorkpaperTemplate(string xml)
        {
            var model = (WorkpaperTemplateModel)ModelDeserializer.DeserializeFromXml<WorkpaperTemplateModel>(xml);
            return model != null ? LoadWorkpaperTemplate(model) : NullModelReference();
        }
        private string LoadWorkpaperTemplate(WorkpaperTemplateModel model)
        {
            var auditProcTabs = uow.Repository<EntityAuditProcedureTab>().GetAll().Where(e => e.EntityID == model.AccountId).ToList();
            var auditProcTabContents = uow.Repository<EntityAuditProcedureTabContent>().GetAll().Where(e => e.EntityID == model.AccountId).ToList();
            var auditProcTabTemplates = uow.Repository<AuditProcedureTabTemplateDetail>().GetAll().Where(e => e.AuditProcedureTemplateID == model.ID);
            var auditProcTabContentTemplates = uow.Repository<AuditProcedureTabContentTemplateDetail>().GetAll().Where(e => e.AuditProcedureTemplateID == model.ID);

            //Remove EntityAuditProcedureTabContent
            DeleteEntityAuditProcedureTabContents(auditProcTabContents);

            //Remove EntityAuditProcedureTab
            DeleteEntityAuditProcedureTabs(auditProcTabs);

            //Moving data from Template to EntityAuditProcedureTab
            foreach (var auditProcTabTemplate in auditProcTabTemplates)
            {
                AddEntityAuditProcedureTab(auditProcTabTemplate.AuditProcedureTabID, model.AccountId);
            }

            //Moving data from Template to EntityAuditProcedureTabContent
            foreach (var auditProcTabContentTemplate in auditProcTabContentTemplates)
            {
                AddEntityAuditProcedureTabContent(auditProcTabContentTemplate.AuditProcedureTabContentID, model.AccountId);
            }

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Successful");
        }

        #endregion

        #region Audit Procedure Answer Section

        public string AddUpdateAuditProcedureAnswer(string parameters)
        {
            var model = (AuditProcedureAnswerViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureAnswerViewModel>(parameters);
            return model != null ? AddEditAuditProcedureAnswer(model) : NullModelReference();
        }

        public string AddEditAuditProcedureAnswer(AuditProcedureAnswerViewModel model)
        {
            var auditProcedureAnswer = Get(model.JobId, model.AuditProcedureId);

            if (auditProcedureAnswer == null)
            {
                //Insert Operation
                auditProcedureAnswer = new AuditProcedureAnswer
                {
                    JobId = model.JobId,
                    AuditProcedureId = model.AuditProcedureId,
                    IsChecked = model.IsChecked,
                    Text = model.Text
                };

                answerRepository.Add(auditProcedureAnswer);
            }
            else
            {
                //only one value from IsChecked and Text can be not null i.e. come from AJAX client.
                if (model.Text == null)
                    auditProcedureAnswer.IsChecked = model.IsChecked;
                else
                    auditProcedureAnswer.Text = model.Text;
            }

            uow.Commit();
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Success");
        }

        public AuditProcedureAnswerViewModel ToAuditProcedureAnswerViewModel(AuditProcedureAnswer model)
        {
            return model == null ? null : new AuditProcedureAnswerViewModel
            {
                AuditProcedureId = model.AuditProcedureId,
                Id = model.ID,
                IsChecked = model.IsChecked,
                JobId = model.JobId,
                Text = model.Text
            };
        }

        #endregion Audit Procedure Answer Section

        #region Audit Procedure Comments Section

        public string GetProcedureComments(string parameters)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(parameters);
            var comments = GetProcedureComments(model.Key, model.Value);
            return ModelSerializer.GenerateReponseXml(comments, ResponseStatus.Success, "Success");
        }

        private List<AuditProcedureCommentViewModel> GetProcedureComments(int jobId, int procedureId)
        {
            var query = commentsRepository.GetAll().Where(c => c.JobId == jobId);
            if (procedureId > 0)
            {
                query = query.Where(c => c.AuditProcedureId == procedureId && c.ParentID == null);
            }

            List<AuditProcedureCommentViewModel> comments = query.OrderByDescending(a => a.CreatedOn).Select(ToModel).ToList();
            //comments = query.OrderByDescending(o => o.ID).Select(
            //    c => new AuditProcedureCommentViewModel()
            //    {
            //        Id = c.ID,
            //        Comment = c.Text,
            //    }).ToList();

            return comments;
        }

        public string AddProcedureComments(string parameters)
        {
            var model = (AuditProcedureCommentViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureCommentViewModel>(parameters);
            return AddProcedureComments(model);
        }

        private string AddProcedureComments(AuditProcedureCommentViewModel model)
        {
            var entity = new AuditProcedureComment
            {
                JobId = model.JobId,
                AuditProcedureId = model.AuditProcedureId,
                Text = model.Comment,
                CreatedBy = model.CreatedBy,
                CreatedOn = DateTime.Now,
                ParentID = model.ParentID
            };

            commentsRepository.Add(entity);
            uow.Commit();

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> {Value = entity.ID}, ResponseStatus.Success,
                "Success");
        }

        public string GetAuditProcedureCommentsByJob(string xml)
        {
            var model = (GenericModel<int>) ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetAuditProcedureCommentsByJob(model.Value);
        }

        private string GetAuditProcedureCommentsByJob(int jobId)
        {
            var auditprocedures =
                (from a in commentsRepository.GetAll().Where(a => a.JobId == jobId && a.ParentID == null)
                    group a by new {a.AuditProcedureId, AuditProcedure = a.AuditProcedure.Title, a.JobId}
                    into g
                    select new AuditProcedureCommentViewModel
                    {
                        AuditProcedureId = g.Key.AuditProcedureId,
                        AuditProcedure = g.Key.AuditProcedure,
                        JobId =  g.Key.JobId,
                    }).ToList();


            var comments =
                commentsRepository.GetAll()
                    .Where(a => a.JobId == jobId && a.ParentID == null)
                    .ToList();

            foreach (var ap in auditprocedures)
            {
                ap.ChildComments =
                    comments.Where(a => a.AuditProcedureId == ap.AuditProcedureId)
                        .OrderByDescending(a => a.CreatedOn)
                        .Select(ToModel)
                        .ToList();
            }

            return ModelSerializer.GenerateReponseXml(auditprocedures, ResponseStatus.Success, "Successful");
        }
        
        #endregion

        #region Audit Procedure OML Section

        public string GetProcedureOMLsByJobID(string parameters)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(parameters);
            var comments = GetProcedureOMLsByJobID(model.Key);
            return ModelSerializer.GenerateReponseXml(comments, ResponseStatus.Success, "Success");
        }

        private List<AuditProcedureOmlViewModel> GetProcedureOMLsByJobID(int jobID)
        {
            var comments = omlRepository.GetAll().Where(c => c.JobId == jobID).OrderByDescending(o => o.ID).Select(
                c => new AuditProcedureOmlViewModel
                {
                    Id = c.ID,
                    AuditProcedureId = c.AuditProcedureId,
                    CustomOption = c.CustomOption,
                    JobId = c.JobId,
                    Item = c.Item,
                    StaffComments = c.StaffComments,
                    Initials = c.Initials,
                    IsCleared = c.IsCleared,
                    IsManagerApproved = c.IsManagerApproved,
                }
                ).ToList();

            return comments;
        }

        public string GetProcedureOMLs(string parameters)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(parameters);
            var comments = GetProcedureOMLs(model.Key, model.Value);
            return ModelSerializer.GenerateReponseXml(comments, ResponseStatus.Success, "Success");
        }
        private List<AuditProcedureOmlViewModel> GetProcedureOMLs(int jobID, int procedureID)
        {
            var comments = omlRepository.GetAll().Where(c => c.AuditProcedureId == procedureID && c.JobId == jobID).OrderByDescending(o => o.ID).Select(
                c => new AuditProcedureOmlViewModel
                {
                    Id = c.ID,
                    CustomOption = c.CustomOption
                }
                ).ToList();

            return comments;
        }

        public string AddEditProcedureOMLs(string parameters)
        {
            var model = (AuditProcedureOmlViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureOmlViewModel>(parameters);
            AddEditProcedureOMLs(model);
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Success");
        }
        private void AddEditProcedureOMLs(AuditProcedureOmlViewModel model)
        {
            if (model == null || model.Id == 0)
            {
                //For New Entry
                var entity = new AuditProcedureOml
                {
                    JobId = model.JobId,
                    AuditProcedureId = model.AuditProcedureId,
                    CustomOption = model.CustomOption,
                };
                omlRepository.Add(entity);
            }
            else
            {
                //For Update Entry
                var getModel = omlRepository.GetById(model.Id);
                if (getModel != null)
                {
                    getModel.Item = model.Item;
                    getModel.StaffComments = model.StaffComments;
                    getModel.Initials = model.Initials;
                    getModel.IsCleared = model.IsCleared;
                    getModel.IsManagerApproved = model.IsManagerApproved;

                }
            }
            uow.Commit();
        }


        public string GetProcedureTestSummary(string parameters)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(parameters);
            var comments = GetProcedureTestSummary(model.Key);
            return ModelSerializer.GenerateReponseXml(comments, ResponseStatus.Success, "Success");
        }
        public string SaveTestSummary(string parameters)
        {
            var model = (TestSummaryAnswerViewModel)ModelDeserializer.DeserializeFromXml<TestSummaryAnswerViewModel>(parameters);
            return model != null ? SaveTestSummary(model) : NullModelReference();
        }

        public string SaveTestSummary(TestSummaryAnswerViewModel model)
        {
            var testSummaryA = testSummaryAnswer.GetAll().FirstOrDefault(a => a.TestSummaryId == model.TestSummaryId && a.JobId == model.JobId);
            if (testSummaryA == null)
            {
                //Insert Operation
                var newTestSummary = new TestSummaryAnswer
                {
                    JobId = model.JobId,
                    TestSummaryId = model.TestSummaryId,
                    IsCompleted = model.IsCompleted
                };
                testSummaryAnswer.Add(newTestSummary);
            }
            else
            {
                //only one value from IsChecked and Text can be not null i.e. come from AJAX client.
                testSummaryA.IsCompleted = model.IsCompleted;
            }

            uow.Commit();
            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Success");
        }

        private List<TestSummaryViewModel> GetProcedureTestSummary(int jobID)
        {
            var modelVm = testSummary.GetAll().Select(
                ts => new TestSummaryViewModel
                {
                    Id = ts.ID,
                    //JobId = ts.JobId,
                    TestName = ts.TestName,
                    Description = ts.Description,
                    //IsCompleted = ts.IsCompleted,
                    TestSummaryComment = ts.TestSummaryComment.Select(tsc => new TestSummaryCommentViewModel
                    {
                        Id = tsc.ID,
                        TestSummaryId = tsc.TestSummaryId,
                        TestComments = tsc.TestComments,
                    }).ToList(),
                }).ToList();

            //For Test Summary Answer
            var summaryAnswer = uow.Repository<TestSummaryAnswer>().GetAll().Where(x => x.JobId == jobID).ToList();
            SetSummaryAnswer(modelVm, summaryAnswer);

            return modelVm;
        }

        private void SetSummaryAnswer(List<TestSummaryViewModel> modelVm, List<TestSummaryAnswer> testSummaryAns)
        {
            modelVm.ForEach(x => x.Answer = ToTestSummaryAnswerViewModel((testSummaryAns.Find(sa => sa.TestSummaryId == x.Id))));

        }

        #endregion Audit Procedure OML Section

        #region Audit Procedure Attachments

        public string GetProcedureAttachment(string parameters)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(parameters);
            var attachments = GetProcedureAttachment(model.Key, model.Value);

            return ModelSerializer.GenerateReponseXml(attachments, ResponseStatus.Success, "Success");
        }

        private AuditProcedureAttachmentViewModel GetProcedureAttachment(int jobID, int procedureID)
        {
            var attachmentDetail = attachmentsRepository.GetAll().Where(c => c.AuditProcedureId == procedureID && c.JobId == jobID).OrderByDescending(o => o.ID);
            var attachment  = attachmentDetail.ToList().Select(

                c => new AuditProcedureAttachmentViewModel
                {
                   
                    Files = c.AuditProcedureAttachmentDetail.Select(f => new AttachmentInfo
                    {
                        Document = new DocumentModel { Name = f.FileName, ServerRelativeUrl = c.Job.GetFolderPath() + @"\Audit Procedures\" + f.ID.ToString() + "_" + f.FileName },
                        Id = f.ID,
                        
                    }).ToList()
                }
                ).FirstOrDefault();

            return attachment;
        }

        public string AddEditAuditProcedureAttachments(string parameters)
        {
            var model = (AuditProcedureAttachmentViewModel)ModelDeserializer.DeserializeFromXml<AuditProcedureAttachmentViewModel>(parameters);
            return AddEditAuditProcedureAttachments(model);
        }

        private string AddEditAuditProcedureAttachments(AuditProcedureAttachmentViewModel model)
        {
            var obj = GetAttachmentModel(model.JobId, model.AuditProcedureId);

            if (obj == null)
            {
                obj = new AuditProcedureAttachment
                {
                    AuditProcedureId = model.AuditProcedureId,
                    JobId = model.JobId,
                    Job = uow.Repository<Jobs>().GetById(model.JobId)
                };

                attachmentsRepository.Add(obj);
            }

            IRepository<AuditProcedureAttachmentDetail> attachmentDetailRepository = uow.Repository<AuditProcedureAttachmentDetail>();

            var fm = new FileModel { FolderPath = obj.GetFolderPath() };

            foreach (AttachmentInfo attachment in model.Files)
            {
                var ai = new AuditProcedureAttachmentDetail
                {
                    AuditProcedureAttachmentId = obj.ID,
                    FileName = attachment.Document.Name
                };
                attachmentDetailRepository.Add(ai);

                uow.Commit();

                fm.Files.Add(new FileInfo
                {
                    FileName = ai.GetSharepointFileName(),
                    FileData = attachment.FileData
                });
            }

            var dc = new DocumentComponent(1);
            var response = dc.UploadFiles(fm);

            return ModelSerializer.GenerateReponseXml(null, response.Status, response.ResponseMessage);
        }

        public AuditProcedureAttachment GetAttachmentModel(int jobID, int procedureID)
        {
            AuditProcedureAttachment model = attachmentsRepository.GetAll().FirstOrDefault(a => a.JobId == jobID && a.AuditProcedureId == procedureID);
            return model;
        }

        public string DeleteAuditProcedureAttachmentFile(string parameters)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameters);
            DeleteAuditProcedureAttachmentFile(model.Value);
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Success");
        }

        private void DeleteAuditProcedureAttachmentFile(int attachmentDetailID)
        {
            IRepository<AuditProcedureAttachmentDetail> attachmentDetailRepository = uow.Repository<AuditProcedureAttachmentDetail>();
            AuditProcedureAttachmentDetail model = attachmentDetailRepository.GetById(attachmentDetailID);

            model.Attachment = attachmentsRepository.GetById(model.AuditProcedureAttachmentId);
            model.Attachment.Job = uow.Repository<Jobs>().GetById(model.Attachment.JobId);

            var dc = new DocumentComponent(1);
            dc.DeleteFile(model.GetSharepointFileName(), model.Attachment.GetFolderPath());

            attachmentDetailRepository.Delete(model);

            uow.Commit();
        }

        public string DownloadAuditProcedureAttachmentFile(string parameters)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameters);
            return DownloadAuditProcedureAttachmentFile(model.Value);
        }

        private string DownloadAuditProcedureAttachmentFile(int attachmentDetailID)
        {
            IRepository<AuditProcedureAttachmentDetail> attachmentDetailRepository = uow.Repository<AuditProcedureAttachmentDetail>();
            AuditProcedureAttachmentDetail model = attachmentDetailRepository.GetById(attachmentDetailID);
            string folder = model.Attachment.GetFolderPath();

            var dc = new DocumentComponent();
            var response = dc.DownloadFileInternal(folder, model.GetSharepointFileName());
            if (response.Status == ResponseStatus.Failure)
            {
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, response.ResponseMessage);
            }

            var dict = new DictionaryModel<string, string> { Key = model.FileName, Value = response.ModelXml };
            return ModelSerializer.GenerateReponseXml(dict, ResponseStatus.Success, "Success");
        }

        #endregion Audit Procedure Attachments

        #region Helper Audit Procedure

        public AuditProcedureTabViewModel ToTabViewModel(AuditProcedureTab tab)
        {
            return tab == null ? null :
            new AuditProcedureTabViewModel
            {
                Id = tab.ID,
                Caption = tab.Caption,
                EntityId = tab.EntityId,
                TabOrder = tab.TabOrder,
                IsActive = tab.IsActive,
                IsViewable = tab.IsViewable,
                AuditProcedureTabContent = tab.AuditProcedureTabContent.Select(ToTabContentsViewModel).ToList()
            };
        }

        public AuditProcedureTabContentViewModel ToTabContentsViewModel(AuditProcedureTabContent tabContent)
        {
            return tabContent == null ? null :
            new AuditProcedureTabContentViewModel
            {
                Id = tabContent.ID,
                AuditProcedureTabId = tabContent.AuditProcedureTabId,
                Caption = tabContent.Caption,
                ContentType = tabContent.ContentType,
                ControlType = tabContent.ControlType,
                IsActive = tabContent.IsActive,
                Order = tabContent.Order,
                IsViewable = tabContent.IsViewable,
                AuditProcedure = tabContent.AuditProcedure.Where(ap => ap.ParentId == null && ap.StatusID == Status.Active).Select(ToAuditProceduresViewModel).ToList()
            };
        }
        
        public AuditProcedureViewModel ToAuditProceduresViewModel(AuditProcedure procedure)
        {
            return procedure == null ? null :
            new AuditProcedureViewModel
            {
                Id = procedure.ID,
                AuditProcedureTabContentId = procedure.AuditProcedureTabContentId,
                Title = procedure.Title,
                Attachments = procedure.Attachments,
                Oml = procedure.Oml,
                Comments = procedure.Comments,
                ControlType = procedure.ControlType,
                AnswerControlType = procedure.AnswerControlType,
                Checkbox = procedure.Checkbox,
                Childs = procedure.Childs.Where(ap => ap.StatusID == Status.Active).Select(ToAuditProceduresViewModel).ToList(),
                AuditProcedureOptionDetail = GetOptionDetailForAuditProcdure(procedure.ID, _auditProcedureOption, _auditProcedureOptionDetail),
                AuditProcedureColumn = GetAuditProcedureColumn(procedure.ID, _auditProcedureColumn)
            };

        }

        public List<AuditProcedureOptionDetailViewModel> GetOptionDetailForAuditProcdure(int auditProcId, List<AuditProcedureOption> auditProcedureOption, List<AuditProcedureOptionDetail> auditProcedureOptionDetail)
        {
            var lstDet = new List<AuditProcedureOptionDetailViewModel>();
            var searIn = auditProcedureOption.Where(x => x.AuditProcedureId == auditProcId);
            foreach (var procedureOption in searIn)
            {
                var model = auditProcedureOptionDetail.Find(x => x.ID == procedureOption.AuditProcedureOptionDetailId);
                lstDet.Add(new AuditProcedureOptionDetailViewModel
                {
                    Id = model.ID,
                    Field = model.Field,
                    Value = model.Value
                });
            }

            return lstDet;
        }

        public string GetAuditProcedureOptionDetail(string xml)
        {
            var models = uow.Repository<AuditProcedureOptionDetail>().GetAll().Select(p => new AuditProcedureOptionDetailViewModel { Field = p.Field, Id = p.ID, Value = p.Value }).ToList();

            return ModelSerializer.GenerateReponseXml<AuditProcedureOptionDetailViewModel>(models, ResponseStatus.Success, "Successful");
        }

        public List<AuditProcedureColumnViewModel> GetAuditProcedureColumn(int auditProcId, List<AuditProcedureColumn> auditProcedureColumn)
        {
            var searIn = auditProcedureColumn.Where(x => x.AuditProcedureId == auditProcId);

            var lstColumns = searIn.Select(x => new AuditProcedureColumnViewModel
            {
                Id = x.ID,
                ColumnName = x.ColumnName,
                ColumnValue = x.ColumnValue
            }).ToList();

            return lstColumns;

        }

        public void SetProcedureAnswer(int jobID, List<AuditProcedureViewModel> lst, List<AuditProcedureAnswer> auditProcedureAnswer)
        {
            if (lst == null)
                return;

            foreach (var obj in lst)
            {
                AuditProcedureAnswer model = auditProcedureAnswer.FirstOrDefault(a => a.JobId == jobID && a.AuditProcedureId == obj.Id);
                obj.Answer = ToAuditProcedureAnswerViewModel(model);
                SetProcedureAnswer(jobID, obj.Childs, auditProcedureAnswer);
            }
        }

        public AuditProcedureAnswer Get(int jobID, int procedureID)
        {
            AuditProcedureAnswer model = answerRepository.GetAll().FirstOrDefault(a => a.JobId == jobID && a.AuditProcedureId == procedureID);
            return model;
        }

        public TestSummaryAnswerViewModel ToTestSummaryAnswerViewModel(TestSummaryAnswer model)
        {
            return model == null ? null :
            new TestSummaryAnswerViewModel
            {
                Id = model.ID,
                JobId = model.JobId,
                TestSummaryId = model.TestSummaryId,
                IsCompleted = model.IsCompleted
            };
        }

        private AuditProcedureCommentViewModel ToModel(AuditProcedureComment entity)
        {
            if (entity == null) return null;

            return new AuditProcedureCommentViewModel
            {
                AuditProcedure = entity.AuditProcedure.Title,
                AuditProcedureId = entity.AuditProcedureId,
                Comment = entity.Text,
                CreatedOn = entity.CreatedOn,
                Id = entity.ID,
                JobId = entity.JobId,
                ParentID = entity.ParentID,
                AccountManagerName =
                    _accountmanageRepository.GetAll()
                        .Where(a => a.ID == entity.CreatedBy)
                        .Select(a => new {Name = a.FirstName + " " + a.LastName})
                        .First()
                        .Name,
                ChildComments =
                    entity.ChildComments.Any()
                        ? entity.ChildComments.OrderByDescending(a => a.CreatedOn).Select(ToModel).ToList()
                        : null
            };
        }

        #endregion
    }
}