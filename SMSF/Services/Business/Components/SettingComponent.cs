﻿using System.Collections.ObjectModel;
using System.Runtime.InteropServices;
using System.Text;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using SettingTypes = DBASystem.SMSF.Contracts.Common.SettingTypes;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class SettingComponent : Component
    {
        private UnitOfWork uow = new UnitOfWork();
        private IRepository<Settings> repository;

        public SettingComponent()
        {
            repository = uow.Repository<Settings>();
        }
        
        public string GetSetting(string xml)
        {
            var model = (GenericModel<SettingHeads>)ModelDeserializer.DeserializeFromXml<GenericModel<SettingHeads>>(xml);

            if (model != null)
            {
                switch (model.Value)
                {
                    case SettingHeads.System:

                        #region System Settings

                        var settingList =
                            GetSettings(new List<SettingTypes>
                            {
                                SettingTypes.EmailSettings,
                                SettingTypes.PasswordSettings,
                                SettingTypes.SessionSettings,
                                SettingTypes.VersionSettings
                            });
                        if (settingList.Any())
                        {
                            var systemSettings = new SystemSettingsModel
                            {
                                EmailSetting =
                                {
                                    SMTPFromAddress = settingList["SMTPFromAddress"],
                                    SMTPHost = settingList["SMTPHost"],
                                    SMTPPassword = settingList["SMTPPassword"],
                                    SMTPPort =
                                        String.IsNullOrEmpty(settingList["SMTPPort"])
                                            ? 0
                                            : Convert.ToInt32(settingList["SMTPPort"]),
                                    SMTPRequireSSL = settingList["SMTPRequireSSL"],
                                    SMTPUserName = settingList["SMTPUserName"]
                                },
                                PasswordSetting =
                                {
                                    MaxPasswordRetries = Convert.ToInt32(settingList.GetValue("MaxPasswordRetries")),
                                    MinNumericCharacters = Convert.ToInt32(settingList.GetValue("MinNumericCharacters")),
                                    MinPasswordLength = Convert.ToInt32(settingList.GetValue("MinPasswordLength")),
                                    MinUpperCaseCharacters = Convert.ToInt32(settingList.GetValue("MinUpperCaseCharacters")),
                                    MinLowerCaseCharacters = Convert.ToInt32(settingList.GetValue("MinLowerCaseCharacters"))
                                },
                                SessionSetting =
                                {
                                    MaxSessionTimeout =
                                        String.IsNullOrEmpty(settingList["MaxSessionTimeOut"])
                                            ? 20
                                            : Convert.ToInt32(settingList["MaxSessionTimeOut"])
                                },
                                VersionSetting =
                                {
                                    VersionNumber = settingList.GetValue("VersionNumber")
                                }
                            };

                            return ModelSerializer.GenerateReponseXml(systemSettings, ResponseStatus.Success,
                                "Successful");
                        }

                        return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure,
                            Resource_EN.Error_EmailSettingsNotFound);

                        #endregion

                    case SettingHeads.Sharepoint:

                        #region Sharepoint

                        var sharepointSettings = new SharePointSettingModel();
                        var sharepointSettingList =
                            GetSettings(new List<SettingTypes>
                            {
                                SettingTypes.SharepointSettings
                            });
                        if (sharepointSettingList.Any())
                        {
                            sharepointSettings.SharePointUrl = sharepointSettingList.GetValue("SharePointUrl");
                            sharepointSettings.UserName = sharepointSettingList.GetValue("SharePointUserName");
                            sharepointSettings.Password = sharepointSettingList.GetValue("SharePointPassword");
                            sharepointSettings.MaximumFileSize = sharepointSettingList.GetValue("SharePointMaxFileSize").ToType<int>();
                            sharepointSettings.AllowedFileTypes = sharepointSettingList.GetValue("SharePointAllowedFileType");
                            sharepointSettings.JobDefaultDocumentsPath = sharepointSettingList.GetValue("SharePointJobDefaultDocumentsPath");
                            sharepointSettings.ReportTemplatePath = sharepointSettingList.GetValue("ReportTemplatePath");
                        }
                        return ModelSerializer.GenerateReponseXml(sharepointSettings, ResponseStatus.Success,
                            "Successful");

                        #endregion

                    case SettingHeads.ReportServer:

                        #region Report Server

                        var reportServerSettings = new ReportServerSettingModel();
                        var reportServerSettingList =
                            GetSettings(new List<SettingTypes>
                            {
                                SettingTypes.ReportServerSettings
                            });

                        if (reportServerSettingList != null && reportServerSettingList.Any())
                        {
                            reportServerSettings.ReportServerUrl = reportServerSettingList.GetValue("ReportServerUrl");
                            reportServerSettings.ReportServerUserName = reportServerSettingList.GetValue("ReportServerUsername");
                            reportServerSettings.Password = reportServerSettingList.GetValue("ReportServerPassword");
                        }

                        return ModelSerializer.GenerateReponseXml(reportServerSettings, ResponseStatus.Success,
                            "Successful");

                        #endregion
                }
            }

            return NullModelReference();
        }

        private Dictionary<string, string> GetSettings(List<SettingTypes> settingtypes)
        {
            var settingList = repository.GetAll().Where(p => settingtypes.Contains((SettingTypes)p.SettingTypeID)).ToDictionary(t => t.Description, t => t.SettingValue);
            return settingList;
        }

        public string GetSettingByType(string xml)
        {
            var model = (GenericModel<SettingTypes>)ModelDeserializer.DeserializeFromXml<GenericModel<SettingTypes>>(xml);
            return GetSettingByType(model.Value);
        }

        internal string GetSettingByType(SettingTypes settingType)
        {
            var returnResponse = ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Failure");
            switch (settingType)
            {
                case SettingTypes.PasswordSettings:

                    #region Password Settings

                    var passwordPolicy = repository.GetAll().Where(p => (SettingTypes)p.SettingTypeID == settingType).ToList();
                    int minUpperCase;
                    int minLowerCase;
                    int minNumericCharacters;
                    int passwordLength;
                    int maxPasswordRetry;

                    Int32.TryParse(passwordPolicy.Single(p => p.Description == "MinPasswordLength").SettingValue, out passwordLength);
                    Int32.TryParse(passwordPolicy.Single(p => p.Description == "MinUpperCaseCharacters").SettingValue, out minUpperCase);
                    Int32.TryParse(passwordPolicy.Single(p => p.Description == "MinLowerCaseCharacters").SettingValue, out minLowerCase);
                    Int32.TryParse(passwordPolicy.Single(p => p.Description == "MinNumericCharacters").SettingValue, out minNumericCharacters);
                    Int32.TryParse(passwordPolicy.Single(p => p.Description == "MaxPasswordRetries").SettingValue, out maxPasswordRetry);
                    var validationExpresion = new StringBuilder();
                    //Sample Regular Expression  --> ^(?=(.*\d){2})(?=(.*[a-z]){2})(?=(.*[A-Z]){2}).{7,10}$
                    validationExpresion.Append("^");
                    validationExpresion.Append(minNumericCharacters != 0 ? @"(?=(.*\d){" + minNumericCharacters + "})" : "");
                    validationExpresion.Append(minUpperCase != 0 ? @"(?=(.*[A-Z]){" + minUpperCase + "})" : "");
                    validationExpresion.Append(minLowerCase != 0 ? @"(?=(.*[a-z]){" + minLowerCase + "})" : "");
                    validationExpresion.Append(@".{" + passwordLength + ",50}$");

                    var passwordPolicyModel = new PasswordPolicyModel
                    {
                        ValidationExpression = validationExpresion.ToString(),
                        MaxPasswordRetry = maxPasswordRetry
                    };
                    var validationMessage = new StringBuilder();
                    validationMessage.Append((minNumericCharacters != 0 || minUpperCase != 0 || minLowerCase != 0) ? "password should contain atleast " : " Password should be ");
                    validationMessage.Append(minNumericCharacters != 0 ? minNumericCharacters + " digit" + (minNumericCharacters > 1 ? "s" : "") + "," : "");
                    validationMessage.Append(minUpperCase != 0 ? minUpperCase + " capital letter" + (minUpperCase > 1 ? "s" : "") + "," : "");
                    validationMessage.Append(minLowerCase != 0 ? minLowerCase + " small letter" + (minLowerCase > 1 ? "s" : "") + "," : "");
                    validationMessage.Append(((minNumericCharacters != 0 || minUpperCase != 0 || minLowerCase != 0) ? "and should be " : "") + "minimum " + passwordLength + " characters long");
                    passwordPolicyModel.ValidationMessage = validationMessage.ToString();

                    returnResponse = ModelSerializer.GenerateReponseXml(new GenericModel<PasswordPolicyModel> { Value = passwordPolicyModel }, ResponseStatus.Success, "Successful");

                    #endregion Password Settings

                    break;

                case SettingTypes.EmailSettings:

                    #region Email Settings

                    var settings = repository.GetAll().Where(p => (SettingTypes)p.SettingTypeID == settingType).ToList();
                    var emailSettingsModel = new EmailSettingsModel
                    {
                        SMTPHost = settings.Single(p => p.Description == "SMTPHost").SettingValue,
                        SMTPUserName = settings.Single(p => p.Description == "SMTPUserName").SettingValue,
                        SMTPPassword = settings.Single(p => p.Description == "SMTPPassword").SettingValue,
                        SMTPFromAddress = settings.Single(p => p.Description == "SMTPFromAddress").SettingValue,
                        SMTPPort =
                            String.IsNullOrEmpty(settings.Single(p => p.Description == "SMTPPort").SettingValue)
                                ? 0
                                : Convert.ToInt32(settings.Single(p => p.Description == "SMTPPort").SettingValue),
                        SMTPRequireSSL = settings.Single(p => p.Description == "SMTPRequireSSL").SettingValue
                    };
                    returnResponse = ModelSerializer.GenerateReponseXml(new GenericModel<EmailSettingsModel> { Value = emailSettingsModel }, ResponseStatus.Success, "Successful");

                    #endregion Email Settings

                    break;

                case SettingTypes.SharepointSettings:

                    #region Sharepoint Settings

                    var sharepointSettings = repository.GetAll().Where(p => (SettingTypes)p.SettingTypeID == settingType).ToList();

                    var sharepointSettingModel = new SharePointSettingModel
                    {
                        SharePointUrl = sharepointSettings.Single(p => p.Description == "SharePointUrl").SettingValue,
                        Password = sharepointSettings.Single(p => p.Description == "SharePointPassword").SettingValue,
                        UserName = sharepointSettings.Single(p => p.Description == "SharePointUserName").SettingValue,

                        MaximumFileSize =
                            sharepointSettings.Single(p => p.Description == "SharePointMaxFileSize")
                                .SettingValue.ToType<int>(),

                        AllowedFileTypes =
                            sharepointSettings.Single(p => p.Description == "SharePointAllowedFileType").SettingValue,

                        JobDefaultDocumentsPath =
                            sharepointSettings.Any(p => p.Description == "SharePointJobDefaultDocumentsPath")
                                ? sharepointSettings.Single(p => p.Description == "SharePointJobDefaultDocumentsPath")
                                    .SettingValue
                                : "",
                        ReportTemplatePath =
                            sharepointSettings.Any(p => p.Description == "ReportTemplatePath")
                                ? sharepointSettings.Single(p => p.Description == "ReportTemplatePath")
                                    .SettingValue
                                : ""
                    };

                    returnResponse = ModelSerializer.GenerateReponseXml(new GenericModel<SharePointSettingModel> { Value = sharepointSettingModel }, ResponseStatus.Success, "Successful");

                    #endregion Sharepoint Settings

                    break;

                case SettingTypes.ReportServerSettings:

                    #region Report Server Settings

                    var reportServerSettings =
                        repository.GetAll().Where(a => (SettingTypes)a.SettingTypeID == settingType).ToList();

                    var reportServerSettingModel = new ReportServerSettingModel
                    {
                        Password = reportServerSettings.First(a => a.Description == "ReportServerPassword").SettingValue,
                        ReportServerUserName = reportServerSettings.First(a => a.Description == "ReportServerUsername").SettingValue,
                        ReportServerUrl = reportServerSettings.First(a => a.Description == "ReportServerUrl").SettingValue,
                    };

                    returnResponse =
                        ModelSerializer.GenerateReponseXml(
                            new GenericModel<ReportServerSettingModel> { Value = reportServerSettingModel },
                            ResponseStatus.Success, "Successful");

                    #endregion

                    break;
            }

            return returnResponse;
        }

        #region Update

        public string UpdateEmailSettings(string xml)
        {
            var model = (GenericModel<EmailSettingsModel>)ModelDeserializer.DeserializeFromXml<GenericModel<EmailSettingsModel>>(xml);
            var status = ResponseStatus.Failure;
            var message = Resource_EN.Error_UnableToUpdate;

            if (model != null)
            {
                UpdateEmailSettings(model.Value);
                status = ResponseStatus.Success;
                message = "Successful";
            }
            return ModelSerializer.GenerateReponseXml(null, status, message);
        }

        private void UpdateEmailSettings(EmailSettingsModel model)
        {
            var settings = repository.GetAll().Where(p => p.SettingTypeID == (int)SettingTypes.EmailSettings).ToList();
            UpdateSettingValue(settings.Single(p => p.Description == "SMTPHost"), model.SMTPHost, model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "SMTPUserName"), model.SMTPUserName, model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "SMTPPassword"), model.SMTPPassword, model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "SMTPFromAddress"), model.SMTPFromAddress, model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "SMTPPort"), model.SMTPPort.ToString(), model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "SMTPRequireSSL"), model.SMTPRequireSSL, model.ModifiedBy);
            uow.Commit();
        }

        public string UpdatePasswordSettings(string xml)
        {
            var model = (GenericModel<PasswordSettingsModel>)ModelDeserializer.DeserializeFromXml<GenericModel<PasswordSettingsModel>>(xml);
            var status = ResponseStatus.Failure;
            var message = Resource_EN.Error_UnableToUpdate;

            if (model != null)
            {
                UpdatePasswordSettings(model.Value);
                status = ResponseStatus.Success;
                message = "Successful";
            }
            return ModelSerializer.GenerateReponseXml(null, status, message);
        }

        private void UpdatePasswordSettings(PasswordSettingsModel model)
        {
            var settings = repository.GetAll().Where(p => p.SettingTypeID == (int)SettingTypes.PasswordSettings).ToList();
            UpdateSettingValue(settings.Single(p => p.Description == "MinPasswordLength"), model.MinPasswordLength.ToString(), model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "MinUpperCaseCharacters"), model.MinUpperCaseCharacters.ToString(), model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "MinLowerCaseCharacters"), model.MinLowerCaseCharacters.ToString(), model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "MinNumericCharacters"), model.MinNumericCharacters.ToString(), model.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "MaxPasswordRetries"), model.MaxPasswordRetries.ToString(), model.ModifiedBy);
            uow.Commit();
        }

        public string UpdateSessionSettings(string xml)
        {
            var model = (GenericModel<SessionSettingModel>)ModelDeserializer.DeserializeFromXml<GenericModel<SessionSettingModel>>(xml);
            var status = ResponseStatus.Failure;
            var message = Resource_EN.Error_UnableToUpdate;

            if (model != null)
            {
                UpdateSessionSettings(model.Value);
                status = ResponseStatus.Success;
                message = "Successful";
            }
            return ModelSerializer.GenerateReponseXml(null, status, message);
        }

        private void UpdateSessionSettings(SessionSettingModel model)
        {
            var settings = repository.GetAll().Where(p => p.SettingTypeID == (int)SettingTypes.SessionSettings).ToList();
            UpdateSettingValue(settings.Single(p => p.Description == "MaxSessionTimeOut"), model.MaxSessionTimeout.ToString(), model.ModifiedBy);
            uow.Commit();
        }

        public string UpdateSharepointSettings(string xml)
        {
            var model = (GenericModel<SharePointSettingModel>)ModelDeserializer.DeserializeFromXml<GenericModel<SharePointSettingModel>>(xml);
            var status = ResponseStatus.Failure;
            var message = Resource_EN.Error_UnableToUpdate;

            if (model != null)
            {
                UpdateSharepointSettings(model.Value);
                status = ResponseStatus.Success;
                message = "Successful";
            }
            return ModelSerializer.GenerateReponseXml(null, status, message);
        }

        private void UpdateSharepointSettings(SharePointSettingModel sharepointSettings)
        {
            var settings = repository.GetAll().Where(p => p.SettingTypeID == (int)SettingTypes.SharepointSettings).ToList();

            UpdateSettingValue(settings.Single(p => p.Description == "SharePointUrl"), sharepointSettings.SharePointUrl, sharepointSettings.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "SharePointUserName"), sharepointSettings.UserName, sharepointSettings.ModifiedBy);
            UpdateSettingValue(settings.Single(p => p.Description == "SharePointMaxFileSize"), sharepointSettings.MaximumFileSize.ToString(), sharepointSettings.ModifiedBy);

            if (!String.IsNullOrEmpty(sharepointSettings.Password))
                UpdateSettingValue(settings.Single(p => p.Description == "SharePointPassword"),
                    sharepointSettings.Password, sharepointSettings.ModifiedBy);

            if (!String.IsNullOrEmpty(sharepointSettings.AllowedFileTypes))
                UpdateSettingValue(settings.Single(p => p.Description == "SharePointAllowedFileType"),
                    sharepointSettings.AllowedFileTypes, sharepointSettings.ModifiedBy);

            #region Update Default Document Setting

            if (settings.Any(a => a.Description == "SharePointJobDefaultDocumentsPath"))
                UpdateSettingValue(settings.Single(p => p.Description == "SharePointJobDefaultDocumentsPath"), sharepointSettings.JobDefaultDocumentsPath, sharepointSettings.ModifiedBy);
            else
            {
                repository.Add(new Settings
                {
                    ID = 18,
                    CreatedBy = 1,
                    CreatedOn = DateTime.Now,
                    Description = "SharePointJobDefaultDocumentsPath",
                    SettingTypeID = (int)SettingTypes.SharepointSettings,
                    SettingValue = sharepointSettings.JobDefaultDocumentsPath,
                    StatusID = Status.Active
                });
            }

            #endregion

            #region Update Audit Reports Settings

            Func<string, ResponseModel> CreateReportFolder = (path) =>
            {
                var folders = path.Split('/');
                var documentComponent = new DocumentComponent();
                var subFolderList = folders.Where(p => p != folders[0] && p!=String.Empty).ToList();
                return documentComponent.AddFolder(folders[0], "", subFolderList);
                
            };
            if (settings.Any(a => a.Description == "ReportTemplatePath"))
            {
                if (settings.Single(p => p.Description == "ReportTemplatePath").SettingValue !=
                    sharepointSettings.ReportTemplatePath)
                {
                    if (CreateReportFolder(sharepointSettings.ReportTemplatePath).Status == ResponseStatus.Success)
                        UpdateSettingValue(settings.Single(p => p.Description == "ReportTemplatePath"),
                            sharepointSettings.ReportTemplatePath, sharepointSettings.ModifiedBy);
                }

            }
            else
            {
                if (CreateReportFolder(sharepointSettings.ReportTemplatePath).Status == ResponseStatus.Success)
                {
                    repository.Add(new Settings
                    {
                        ID = 19,
                        CreatedBy = 1,
                        CreatedOn = DateTime.Now,
                        Description = "ReportTemplatePath",
                        SettingTypeID = (int) SettingTypes.SharepointSettings,
                        SettingValue = sharepointSettings.ReportTemplatePath,
                        StatusID = Status.Active
                    });
                }
            }

            #endregion

            uow.Commit();
        }

        public string UpdateReportServerSettings(string xml)
        {
            var model = (ReportServerSettingModel)ModelDeserializer.DeserializeFromXml<ReportServerSettingModel>(xml);
            return UpdateReportServerSettings(model);
        }

        private string UpdateReportServerSettings(ReportServerSettingModel model)
        {
            var settings =
                repository.GetAll()
                    .Where(a => (SettingTypes)a.SettingTypeID == SettingTypes.ReportServerSettings)
                    .ToList();

            UpdateSettingValue(settings.First(a => a.Description == "ReportServerUrl"), model.ReportServerUrl, model.ModifiedBy);
            UpdateSettingValue(settings.First(a => a.Description == "ReportServerUsername"), model.ReportServerUserName, model.ModifiedBy);
            UpdateSettingValue(settings.First(a => a.Description == "ReportServerPassword"), model.Password, model.ModifiedBy);

            uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        private void UpdateSettingValue(Settings setting, string settingValue, int? modifiedBy)
        {
            setting.SettingValue = settingValue;
            setting.ModifiedBy = modifiedBy;
            setting.ModifiedOn = DateTime.Now;
        }

        public string UpdateUserPassword(string xml)
        {
            var model = (GenericModel<ChangePasswordModel>)ModelDeserializer.DeserializeFromXml<GenericModel<ChangePasswordModel>>(xml);
            var status = ResponseStatus.Failure;
            var message = Resource_EN.Error_UnableToUpdate;

            if (model != null)
            {
                message = UpdateUserPassword(model.Value);
                status = message == "Successful" ? ResponseStatus.Success : ResponseStatus.Failure;
            }
            return ModelSerializer.GenerateReponseXml(null, status, message);
        }

        private string UpdateUserPassword(ChangePasswordModel model)
        {
            var thisUser = uow.Repository<Users>().GetAll().FirstOrDefault(p => p.ID == model.UserId && p.Password == model.CurrentPassword);

            if (thisUser != null)
            {
                thisUser.Password = model.NewPassword;
                thisUser.ModifiedBy = model.UserId;
                thisUser.ModifiedOn = DateTime.Now;
                uow.Commit();
                return "Successful";
            }

            return Resource_EN.Error_InvalidCurrentPassword;
        }

        public string UpdateVersionSettings(string xml)
        {
            var model = (GenericModel<VersionSettingModel>)ModelDeserializer.DeserializeFromXml<GenericModel<VersionSettingModel>>(xml);
            var status = ResponseStatus.Failure;
            var message = Resource_EN.Error_UnableToUpdate;

            if (model != null)
            {
                UpdateVersionSettings(model.Value);
                status = ResponseStatus.Success;
                message = "Successful";
            }
            return ModelSerializer.GenerateReponseXml(null, status, message);
        }

        private void UpdateVersionSettings(VersionSettingModel model)
        {
            var settings = repository.GetAll().Where(p => p.SettingTypeID == (int)SettingTypes.VersionSettings).ToList();
            if (settings.Any(a => a.Description == "VersionNumber"))
            {
                UpdateSettingValue(settings.Single(p => p.Description == "VersionNumber"), model.VersionNumber, model.ModifiedBy);
                uow.Commit();
            }
            else
            {
                var settingTypesRepository = uow.Repository<Data.Models.SettingTypes>();
                if (!settingTypesRepository.Any(a=>a.ID == 6))
                {
                    settingTypesRepository.Add(new Data.Models.SettingTypes
                    {
                        Description = "Version Settings",
                        ID = (int)SettingTypes.VersionSettings,
                        StatusID = Status.Active
                    });
                }

                repository.Add(new Settings
                {
                    CreatedBy = (int) model.ModifiedBy,
                    CreatedOn = (DateTime)model.ModifiedOn,
                    Description = "VersionNumber",
                    ModifiedBy = model.ModifiedBy,
                    ModifiedOn = model.ModifiedOn,
                    SettingValue = model.VersionNumber,
                    StatusID = Status.Active,
                    SettingTypeID = (int)SettingTypes.VersionSettings
                });

                uow.Commit();
            }
        } 

        #endregion
    }
}