﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class ValidationComponent : Component
    {        
        public string GetAllowedOptionsByOptionType(string xml)
        {
            var model = (OptionModel)ModelDeserializer.DeserializeFromXml<OptionModel>(xml);
            return model != null ? GetAllowedOptionsByOptionType(model.UserID, model.OptionType) : NullModelReference();
        }

        public string GetAllowedOptionsByOptionType(int userID, OptionType optionType)
        {
            List<Option> list = GetAllowedOptions(userID, optionType);
            return ModelSerializer.GenerateReponseXml<Option>(list, ResponseStatus.Success, "Successful");
        }

        public List<Option> GetAllowedOptions(int userID, OptionType optionType)
        {
            var uow = new UnitOfWork();
            var users = uow.Repository<Users>().GetAll();
            var groupRoles = uow.Repository<GroupRoles>().GetAll();
            var roleOptions = uow.Repository<RoleOptions>().GetAll();
            var options = uow.Repository<Options>().GetAll();

            return (from user in users
                    join gr in groupRoles
                    on user.GroupID equals gr.GroupID
                    join ro in roleOptions
                    on gr.RoleID equals ro.RoleID
                    join o in options
                    on ro.OptionID equals o.ID
                    where
                      o.StatusID == Status.Active
                      &&
                      (user.ID == userID
                      &&
                      ro.IsAssigned
                      &&
                      o.TypeID == (int)optionType)
                      ||
                      o.AllowAnonymous
                    select (Option)ro.OptionID).ToList();
        }
    }
}
