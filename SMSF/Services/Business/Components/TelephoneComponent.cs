﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class TelephoneComponent : Component
    {
        private readonly UnitOfWork _uow = new UnitOfWork();
        private readonly IRepository<Telephones> _repository;

        public TelephoneComponent()
        {
            _repository = _uow.Repository<Telephones>();
        }
        
        public string GetTelephoneTypes(string xml)
        {
            return GetTelephoneTypes();
        }

        private string GetTelephoneTypes()
        {
            var listPhoneTypes = _uow.Repository<TelephoneTypes>().GetAll()
                                                                 .Where(a => a.StatusID == Status.Active)
                                                                 .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Name }).ToList();

            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(listPhoneTypes, ResponseStatus.Success, "Successful");
        }
                
        public string AddTelephone(string xml)
        {
            var model = (ContactCreateModel)ModelDeserializer.DeserializeFromXml<ContactCreateModel>(xml);
            return model != null ? AddTelephone(model) : NullModelReference ();
        }

        private string AddTelephone(ContactCreateModel model)
        {
            var entity = new Telephones
            {
                AccountManagerID = model.AccountManagerID,
                AreaCode = model.AreaCode,
                CountryCode = model.CountryCode,
                PhoneNumber = model.PhoneNumber,
                StatusID = Status.Active,
                TypeID = model.TypeID
            };

            _repository.Add(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(entity.ID > 0 ? new GenericModel<int> { Value = entity.ID } : null, ResponseStatus.Success, "Successful");
        }
        
        public string UpdateTelephone(string xml)
        {
            var model = (ContactCreateModel)ModelDeserializer.DeserializeFromXml<ContactCreateModel>(xml);
            return model != null ? UpdateTelephone(model) : NullModelReference();
        }

        private string UpdateTelephone(ContactCreateModel model)
        {
            var entity = _repository.GetById(model.ID);

            entity.AreaCode = model.AreaCode;
            entity.CountryCode = model.CountryCode;
            entity.PhoneNumber = model.PhoneNumber;
            entity.TypeID = model.TypeID;

            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }
        
        public string DeleteTelephone(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return model != null ? DeleteTelephone(model.Value) : NullModelReference();
        }

        private string DeleteTelephone(int id)
        {
            var entity = _repository.GetById(id);
            _repository.Delete(entity);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }
        
        public string GetCountryCode(string xml)
        {
            var model = (GenericModel<string>)ModelDeserializer.DeserializeFromXml<GenericModel<string>>(xml);

            return ListCountryCode(model.Value);
        }

        private string ListCountryCode(string text)
        {
            var list = !string.IsNullOrEmpty(text)
                            ? _uow.Repository<Countries>()
                                .GetAll()
                                .Where(a => a.Name.Contains(text) || a.Code.Contains(text))
                                .Select(a => new DictionaryModel<string, string> {Key = a.Code, Value = a.Code + " " + a.Name})
                                .ToList()
                            : _uow.Repository<Countries>()
                                .GetAll()
                                .Select(a => new DictionaryModel<string, string> {Key = a.Code, Value = a.Code + " " + a.Name})
                                .ToList();

            return ModelSerializer.GenerateReponseXml<DictionaryModel<string, string>>(list, ResponseStatus.Success, "Successful");
        }
    }
}
