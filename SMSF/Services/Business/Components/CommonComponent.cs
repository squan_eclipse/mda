﻿using System.Net.Configuration;
using System.Text;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using EntityTypes = DBASystem.SMSF.Contracts.Common.EntityTypes;
using SettingTypes = DBASystem.SMSF.Contracts.Common.SettingTypes;
namespace DBASystem.SMSF.Service.Business.Components
{
    public class CommonComponent : Component
    {
        public List<int> GetParentnChildEntities(UnitOfWork uow, int entityId, bool onlyParents = false)
        {
            var entities = uow.Repository<Entities>().GetAll()
                .Where(a => a.StatusID == Status.Active &&
                            (
                                (EntityTypes) a.TypeID == EntityTypes.Entity ||
                                (EntityTypes) a.TypeID == EntityTypes.Client ||
                                (EntityTypes) a.TypeID == EntityTypes.Group
                            )
                )
                .ToDictionary(a => a.ID, a => a.ParentID);

            List<int> ids = new List<int>();
            ids.Add(entityId);
            
            GetParentsNChilds(entityId, ids, entities,onlyParents);
            
            return ids;
        }

        /// <summary>
        ///  Returns Complete Hierarchy of Entities w.r.t.  given entityId
        /// </summary>
        /// <param name="entityId">Source entity id</param>
        /// <param name="ids">List of int as a container</param>
        /// <param name="entities">Dictionary of entities (ID, ParentID)</param>
        /// <param name="onlyParents">If Only Parents are Required</param>
        public void GetParentsNChilds(int entityId, List<int> ids, Dictionary<int, int?> entities,bool onlyParents = false)
        {
            if (entities.ContainsKey(entityId))
            {
                var parentId = entities[entityId];

                // Parent should not be null, Entities list must contains parent as entity, Parent as Entity must have parent, IDs list must have one value for parent.
                if (parentId != null && entities.ContainsKey(parentId.Value) && entities[parentId.Value] != null && !ids.Contains(parentId.Value))
                {
                    ids.Add(parentId.Value);
                    GetParentsNChilds(parentId.Value, ids, entities);
                }
                if (!onlyParents)
                {
                    var children = entities.Where(a => a.Value == entityId);
                    if (children.Any())
                    {
                        foreach (var id in children.Select(a => a.Key))
                        {
                            if (!ids.Contains(id))
                            {
                                ids.Add(id);
                                GetParentsNChilds(id, ids, entities);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        ///  Returns Complete Hierarchy of Entities w.r.t.  given entityId
        /// </summary>
        /// <param name="entityId">Source entity id</param>
        /// <param name="ids">List of int as a container</param>
        /// <param name="entities">Dictionary of entities (ID, ParentID)</param>
        public void GetChilds(Guid entityId, List<Guid> ids, Dictionary<Guid, Guid?> entities)
        {
            if (entities.ContainsKey(entityId))
            {
                var children = entities.Where(a => a.Value == entityId);
                if (children.Any())
                {
                    foreach (var id in children.Select(a => a.Key))
                    {
                        if (!ids.Contains(id))
                        {
                            ids.Add(id);
                            GetChilds(id, ids, entities);
                        }
                    }
                }
            }
        }
        /// <summary>
        ///  Returns Parents of an entity
        /// </summary>
        /// <param name="entityId">Source entity id</param>
        /// <param name="uow">Instance Of unit of work</param>
        /// <param name="filter">Optional Filter For Type</param>

        public List<int> GetParents(UnitOfWork uow, int entityId, List<EntityTypes> filter = null)
        {
            filter = filter ?? new List<EntityTypes>() { EntityTypes.Client, EntityTypes.Group, EntityTypes.Entity };
            var entities = uow.Repository<Entities>().GetAll()
                                                     .Where(a => a.StatusID == Status.Active &&
                                                                 filter.Any(m => m == (Contracts.Common.EntityTypes)a.TypeID))
                                                     .ToDictionary(a => a.ID, a => a.ParentID);

            List<int> ids = new List<int>();
            ids.Add(entityId);

            GetParentsNChilds(entityId, ids, entities,true);

            return ids;
        }
        /// <summary>
        ///  this Function is used by other components 
        /// </summary>
        /// <param name="settingType"></param>
        /// <returns></returns>
        internal T GetSettingByType<T>() where T : ISettingModel
        {
            ISettingModel settingModel = (T)Activator.CreateInstance(typeof(T));
            var responseModel = ModelDeserializer.ParseResponseXml(new SettingComponent().GetSettingByType(settingModel.SettingType));
            settingModel = ((GenericModel<T>)ModelDeserializer.DeserializeFromXml<GenericModel<T>>(responseModel.ModelXml)).Value;

            return (T)settingModel;
        }
        public string FormatAddress(Addresses address)
        {
            try
            {
                var addressString = new StringBuilder();
                bool hasAddress = false;

                if (!string.IsNullOrEmpty(address.UnitNumber))
                {
                    addressString.Append(address.UnitNumber);
                    hasAddress = true;
                }

                if (!string.IsNullOrEmpty(address.StreetNumber))
                {
                    addressString.Append((string.IsNullOrEmpty(address.UnitNumber) ? "" : " / ") + address.StreetNumber);
                    hasAddress = true;
                }

                if (!string.IsNullOrEmpty(address.StreetName))
                {
                    addressString.Append((hasAddress ? " " : "") + address.StreetName);
                    hasAddress = true;
                }
                if (!string.IsNullOrEmpty(address.StreetType))
                {
                    addressString.Append((hasAddress ? " " : "") + address.StreetType);
                    hasAddress = true;
                }
                if (!string.IsNullOrEmpty(address.City))
                {
                    addressString.Append((hasAddress ? ", " : "") + address.City);
                    hasAddress = true;
                }
                if (!string.IsNullOrEmpty(address.City))
                {
                    addressString.Append((hasAddress ? ", " : "") + address.City);
                    hasAddress = true;
                }
                if (!string.IsNullOrEmpty(address.PostCode))
                {
                    addressString.Append((hasAddress ? " " : "") + address.PostCode);
                    hasAddress = true;
                }

                if (!string.IsNullOrEmpty(address.State.Name))
                {
                    addressString.Append((hasAddress ? " " : "") + address.State.Name);
                    hasAddress = true;
                }
                if (!string.IsNullOrEmpty(address.Country.Name))
                {
                    addressString.Append((hasAddress ? " " : "") + address.Country.Name);
                    hasAddress = true;
                }
                return addressString.ToString();
            }
            catch
            {
                return "Not Available";
            }

        }
            

                       

                      

            
        }


    }

