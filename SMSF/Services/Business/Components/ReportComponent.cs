﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class ReportComponent : Component
    {
        private readonly UnitOfWork _uow = new UnitOfWork();
        private IRepository<Jobs> _repository;

        private void GetFields(List<PDFHelper.PdfFormField> fields, int jobId)
        {
            var job = _uow.Repository<Jobs>().GetById(jobId);
            var auditAdmin = job.AccountManagerJobs.Where(q => q.JobRoleID == 6).FirstOrDefault();
            foreach (var item in fields)
            {
                switch (item.GenericName)
                {
                    case "auditorName":
                        item.Value = auditAdmin == null
                               ? "Not Available"
                               : (auditAdmin.AccountManager.FirstName + " " + auditAdmin.AccountManager.LastName);
                        //if (job.Fund.ParentEntity.ParentEntity.CategoryID == 8)
                        //{
                        //    item.Value = auditAdmin == null
                        //        ? "Not Available"
                        //        : (auditAdmin.AccountManager.FirstName + " " + auditAdmin.AccountManager.LastName);
                        //}
                        //else
                        //{
                        //    item.Value = job.Fund.ParentEntity.ParentEntity.Name;
                        //}
                        break;

                    case "auditorAddress":
                        item.Value = auditAdmin == null
                            ? "Not Available"
                            : (auditAdmin.AccountManager.Email);
                        break;

                    case "firmName":
                        item.Value = job.Fund.ParentEntity.ParentEntity.Name;
                        break;

                    case "firmAddress":
                        var entityAddress =
                            job.Fund.ParentEntity.ParentEntity.Addresses.Where(p => p.Default).FirstOrDefault();
                        if (entityAddress == null)
                        {
                            entityAddress =
                                job.Fund.ParentEntity.ParentEntity.Addresses.FirstOrDefault();
                        }
                        item.Value = entityAddress == null
                            ? "Not Available"
                            : new CommonComponent().FormatAddress(entityAddress);
                        break;

                    case "fundName":
                        item.Value = job.Fund.Name;
                        break;

                    case "fundABN":
                        item.Value = job.Fund.ABN;
                        break;

                    case "fundTFN":
                        item.Value = job.Fund.TFN;
                        break;

                    case "abnOrTfn":
                        item.Value = (job.Fund.ABN ?? "") + (string.IsNullOrEmpty(job.Fund.TFN) ? "" : (string.IsNullOrEmpty(job.Fund.ABN) ? "" : "/") + job.Fund.TFN);
                        break;

                    case "clientName":
                        item.Value = job.Fund.ParentEntity.Name;
                        break;

                    case "clientAddress":
                        var clientAddress =
                         job.Fund.ParentEntity.Addresses.Where(p => p.Default).FirstOrDefault();
                        if (clientAddress == null)
                        {
                            clientAddress =
                                job.Fund.ParentEntity.Addresses.FirstOrDefault();
                        }

                        item.Value = clientAddress == null
                         ? "Not Available"
                         : new CommonComponent().FormatAddress(clientAddress);
                        break;

                    case "financialYear":
                        item.Value = DateTime.Now.Year.ToString();
                        break;

                    case "jobSigningDate":
                        item.Value = DateTime.Now.Year.ToString();
                        break;

                    case "AuditorOpinion":
                        item.Value = "Auditor Opinion";
                        break;

                    case "SMSFAuditorOpinion":
                        item.Value = "SMSF Auditor Opinion";
                        break;

                    case "BasisforSMSFauditoropinion":
                        item.Value = "Basis for SMSF auditor opinion";
                        break;

                    default:
                        item.Value = "----" + item.GenericName + "----";
                        break;
                }
            }
        }

        #region Generate Audit Reports

        public string GenerateAuditReports(string xml)
        {
            var model = (AuditReportGenerateModel)ModelDeserializer.DeserializeFromXml<AuditReportGenerateModel>(xml);
            return GenerateAuditReports(model.TemplateName, model.FolderPath, model.JobId);
        }

        public string GenerateAuditReports(string templateName, string folder, int jobId)
        {
            var document = new DocumentComponent().DownloadFileInternal(folder, templateName);
            var fileBuffer = Convert.FromBase64String(document.ModelXml);
            var pdfHelper = new PDFHelper.PdfFormEditor(fileBuffer);
            var fields = pdfHelper.GetRequiredFields();
            int counter = 0;
            GetFields(fields, jobId);

            var updatedFiles = pdfHelper.GetUpdatedForm(fields);
            var base64File = Convert.ToBase64String(updatedFiles.ToArray());

            return ModelSerializer.GenerateReponseXml(new GenericModel<string>() { Value = base64File }, ResponseStatus.Success, "Successful");
        }

        #endregion Generate Audit Reports
    }
}