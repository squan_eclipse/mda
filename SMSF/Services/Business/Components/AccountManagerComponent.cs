﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

using Common = DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class AccountManagerComponent : Component
    {
        private UnitOfWork _uow = new UnitOfWork();
        private IRepository<AccountManagers> _repository;

        public AccountManagerComponent()
        {
            _repository = _uow.Repository<AccountManagers>();
        }

        public string DeleteAccountManager(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            return model != null ? DeleteAccountManager(model.Key, model.Value) : NullModelReference();
        }

        public string DeleteAccountManager(int id, int userId)
        {
            var manager = _repository.GetById(id);

            if (manager == null)
                return ModelSerializer.GenerateReponseXml(new GenericModel<bool> {Value = false}, ResponseStatus.Failure,
                    Resource_EN.Error_UnableToDelete);

            manager.StatusID = Status.Deleted;
            manager.ModifiedBy = userId;
            manager.ModifiedOn = DateTime.Now;

            if (manager.User != null)
                manager.User.StatusID = Status.Deleted;

            _uow.Commit();
            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success, "Record Deleted Successfully");
        }

        public string AddAccountManager(string xml)
        {
            var model = (AccountManagerCreateModel)ModelDeserializer.DeserializeFromXml<AccountManagerCreateModel>(xml);
            return model != null ? AddAccountManager(model) : NullModelReference();
        }

        private string AddAccountManager(AccountManagerCreateModel model)
        {
            if (IsExists(null, model.EntityID, model.Email))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_LoginNameAlreadyExists);
            var userIdentifier = Guid.NewGuid();

            var entity = new AccountManagers
            {
                EntityID = model.EntityID,
                Identifier = userIdentifier,
                CreatedBy = model.CreatedBy,
                CreatedOn = model.CreatedOn,
                ModifiedBy = model.CreatedBy,
                ModifiedOn = model.CreatedOn,
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                MidName = model.MidName,
                StatusID = Status.Active,
                Title = model.Title,
                User = model.User == null ? null :
                        new Users
                        {
                            CreatedBy = model.CreatedBy,
                            CreatedOn = model.CreatedOn,
                            GroupID = model.User.GroupID,
                            Email = model.Email,
                            Password = model.User.Password,
                            StatusID = Status.Active,
                            Identifier = userIdentifier
                        }
            };

            if (model.ListContactInfo != null && model.ListContactInfo.Any())
            {
                foreach (var contact in model.ListContactInfo)
                {
                    var telephone = new Telephones
                    {
                        AccountManagerID = entity.ID,
                        AreaCode = contact.AreaCode,
                        CountryCode = contact.CountryCode,
                        PhoneNumber = contact.PhoneNumber,
                        StatusID = Status.Active,
                        TypeID = contact.TypeID
                    };
                    entity.Telephones.Add(telephone);
                }
            }

            _repository.Add(entity);
            _uow.Commit();

            if (entity.ID > 0)
            {
                new NotificationComponent().InsertNotification(NotificationTypes.Info, "New user " + model.FirstName + " " + model.LastName + " registered", null, "User", entity.ID, false, model.CreatedBy);

                return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = entity.ID }, ResponseStatus.Success, "Successful");
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_UnableToAdd);
        }

        public string UpdateAccountManager(string xml)
        {
            var model = (AccountManagerCreateModel)ModelDeserializer.DeserializeFromXml<AccountManagerCreateModel>(xml);
            return model != null ? UpdateAccountManager(model) : NullModelReference();
        }

        private string UpdateAccountManager(AccountManagerCreateModel model)
        {
            if (IsExists(model.ID, model.EntityID, model.Email))
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_LoginNameAlreadyExists);

            var user = _repository.GetById(model.ID);

            if (user != null)
            {
                user.ModifiedBy = model.ModifiedBy;
                user.ModifiedOn = model.ModifiedOn;
                user.EntityID = model.EntityID;
                user.Email = model.Email;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.MidName = model.MidName;
                user.Title = model.Title;

                #region Contact information

                if (model.ListContactInfo != null)
                {
                    foreach (var contact in model.ListContactInfo.Where(a => a.StatusID == Status.Deleted).ToList())
                    {
                        var contactEntity = _uow.Repository<Telephones>().GetById(contact.ID);

                        contactEntity.StatusID = Status.Deleted;
                    }

                    foreach (var contact in model.ListContactInfo.Where(a => a.ID > 0 && a.StatusID == Status.Active).ToList())
                    {
                        var contactEntity = _uow.Repository<Telephones>().GetById(contact.ID);

                        contactEntity.AreaCode = contact.AreaCode;
                        contactEntity.CountryCode = contact.CountryCode;
                        contactEntity.PhoneNumber = contact.PhoneNumber;
                        contactEntity.TypeID = contact.TypeID;
                    }

                    foreach (var contact in model.ListContactInfo.Where(a => a.ID < 0 && a.StatusID == Status.Active).ToList())
                    {
                        _uow.Repository<Telephones>().Add(new Telephones
                        {
                            AccountManagerID = user.ID,
                            AreaCode = contact.AreaCode,
                            CountryCode = contact.CountryCode,
                            PhoneNumber = contact.PhoneNumber,
                            StatusID = Status.Active,
                            TypeID = contact.TypeID
                        });
                    }
                }

                #endregion Contact information

                #region User

                if (model.User != null)
                {
                    if (user.User == null)
                    {
                        user.User = new Users
                        {
                            CreatedOn = model.ModifiedOn.Value,
                            CreatedBy = model.ModifiedBy.Value,
                            StatusID = Status.Active,
                            Password = model.User.Password,
                            Identifier =  user.Identifier
                        };
                    }
                    else
                    {
                        user.User.ModifiedBy = model.ModifiedBy;
                        user.User.ModifiedOn = model.ModifiedOn;
                    }

                    user.User.Email = model.Email;
                    user.User.GroupID = model.User.GroupID;
                }
                else
                {
                    if (user.User != null)
                        _uow.Repository<Users>().Delete(user.User);
                }

                #endregion User

                _uow.Commit();
                _uow = new UnitOfWork();
                _repository = _uow.Repository<AccountManagers>();
               
                return GetAccountManagerDetail(user.ID);
            }
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "User Not Found");
        }

        public string GetAccountManager(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return model != null ? GetAccountManager(model.Value) : NullModelReference();
        }

        private string GetAccountManager(int id)
        {
            var entity = _repository.GetById(id);

            if (entity != null)
            {
                var model = new AccountManagerCreateModel
                {
                    EntityID = entity.EntityID,
                    Email = entity.Email,
                    FirstName = entity.FirstName,
                    ID = entity.ID,
                    LastName = entity.LastName,
                    MidName = entity.MidName,
                    HasLogin = entity.User != null,
                    Status = entity.StatusID,
                    Title = entity.Title,
                    ListContactInfo = entity.Telephones.Any(b => b.StatusID == Status.Active) ?
                                        entity.Telephones.Select(b => new ContactCreateModel
                                        {
                                            TypeID = b.TypeID,
                                            AreaCode = b.AreaCode,
                                            CountryCode = b.CountryCode,
                                            ID = b.ID,
                                            PhoneNumber = b.PhoneNumber,
                                            Type = b.TelephoneType.Name,
                                            StatusID = b.StatusID
                                        }).ToList() : null,
                    User = entity.User == null ? null :
                            new UserCreateModel
                            {
                                Group = entity.User.Group.Title,
                                ID = entity.User.ID,
                                Login = entity.User.Email,
                                Password = entity.User.Password,
                                ConfirmPassword = entity.User.Password,
                                GroupID = entity.User.GroupID
                            }
                };

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_UserNotFound);
        }

        public string GetFirmAccountManager(string xml)
        {
            var model = (GenericModel<int?>)ModelDeserializer.DeserializeFromXml<GenericModel<int?>>(xml);
            return model != null ? GetFirmAccountManager(model.Value) : NullModelReference();
        }

        private string GetFirmAccountManager(int? entityID)
        {
            var managers = _repository.GetAll()
                                     .Where(a => a.StatusID == Status.Active && a.EntityID == entityID).AsEnumerable()
                                    .Select(a => new AccountManagerCreateModel
                                    {
                                        FirstName = a.FirstName,
                                        MidName = a.MidName,
                                        LastName = a.LastName,
                                        Email = a.Email,
                                        ID = a.ID,
                                        EntityID = a.EntityID,
                                        Status = a.StatusID,
                                        ListContactInfo = a.Telephones.Any(x => x.StatusID == Status.Active) ? a.Telephones.Select(x => new ContactCreateModel
                                        {
                                            AccountManagerID = x.AccountManagerID,
                                            AreaCode = x.AreaCode,
                                            CountryCode = x.CountryCode,
                                            ID = x.ID,
                                            PhoneNumber = x.PhoneNumber,
                                            TypeID = x.TypeID,
                                            Type = x.TelephoneType.Name,
                                            StatusID = x.StatusID
                                        }).ToList() : null,
                                        HasLogin = a.User != null,
                                        User = a.User == null ? null :
                                                new UserCreateModel
                                                {
                                                    Group = a.User.Group.Title,
                                                    GroupID = a.User.GroupID,
                                                    ID = a.User.ID,
                                                    Login = a.User.Email,
                                                    Password = a.User.Password
                                                }
                                    })
                                    .ToList();

            return ModelSerializer.GenerateReponseXml<AccountManagerCreateModel>(managers, ResponseStatus.Success, "Successful");
        }

        public string GetAccountManagerDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return model != null ? GetAccountManagerDetail(model.Value) : NullModelReference();
        }

        private string GetAccountManagerDetail(int id)
        {
            var userDetial = _repository.GetById(id);
            var passwordPolicy = new CommonComponent().GetSettingByType<PasswordPolicyModel>();

            if (userDetial != null)
            {
                var model = new AccountManagerDetailModel
                {
                    ID = userDetial.ID,
                    Email = userDetial.Email,
                    FirstName = userDetial.FirstName,
                    LastName = userDetial.LastName,
                    MidName = userDetial.MidName,
                    Title = userDetial.Title,
                    ParentEntity =
                        userDetial.Entity.TypeID == (int) Common.EntityTypes.Client
                            ? userDetial.Entity.ParentEntity.Name
                            : userDetial.Entity.Name,
                    ParentClient =
                        userDetial.Entity.TypeID == (int) Common.EntityTypes.Client ? userDetial.Entity.Name : "",

                    ListContactInfo = userDetial.Telephones.Any(b => b.StatusID == Status.Active)
                        ? userDetial.Telephones.Where(b => b.StatusID == Status.Active).Select(b => new ContactModel
                        {
                            AreaCode = b.AreaCode,
                            CountryCode = b.CountryCode,
                            ID = b.ID,
                            PhoneNumber = b.PhoneNumber,
                            Type = b.TelephoneType.Name
                        }).ToList()
                        : null,
                    UserDetail = userDetial.User == null
                        ? null
                        : new UserDetailModel
                        {
                            Email = userDetial.User.Email,
                            Group = userDetial.User.Group.Title,
                            ID = userDetial.User.ID,
                            LastLoginTime = userDetial.User.LastLoginTime,
                        },
                    Jobs =
                        userDetial.Jobs.Any()
                            ? (from a in _uow.WorkflowRepository<WorkflowProcessInstance>().GetAll()
                                join b in _uow.Repository<AccountManagerJobs>().GetAll() on a.Id equals
                                    (Guid) b.Job.Identifier
                                where b.AccountManagerID == id
                                group b by new
                               {
                                   ClientName = b.Job.Fund.ParentEntity.Name, FundName = b.Job.Fund.Name, JobFlowStatus = a.StateName,
                                   JobTitle = b.Job.Title,
                               
                               }
                                into g
                                select new AccountManagerJobViewModel
                                {
                                    ClientName = g.Key.ClientName,
                                    FundName = g.Key.FundName,
                                    JobFlowStatus =  g.Key.JobFlowStatus,
                                    JobTitle = g.Key.JobTitle,
                                    ManagerRole =g.Select(r => r.JobRole.Name).ToList()

                                }
                                ).ToList()
                            : null,
                    Locked = userDetial.User == null ? false :
                        userDetial.User.FailedPasswordAttemptCount == null
                            ? false
                            : userDetial.User.FailedPasswordAttemptCount >= passwordPolicy.MaxPasswordRetry

                };

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, Resource_EN.Error_UserNotFound);
        }

        public string ValidateEmail(string xml)
        {
            var model = (AccountManangerEmailValidation)ModelDeserializer.DeserializeFromXml<AccountManangerEmailValidation>(xml);

            var isExists = _repository.Any(m => m.Email == model.Email); //IsExists(model.ID, model.EntityID, model.Email);

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, isExists ? Resource_EN.Error_LoginNameAlreadyExists : Resource_EN.Info_LoginNameAvailable);
        }

        private bool IsExists(int? id, int entityId, string email)
        {
            var entity = _uow.Repository<Entities>().GetById(entityId);

            if (entity != null && entity.CategoryID == 1 && entity.ParentID == null) // The SYSTEM entity. cannot allow duplication on system level users.
            {
                return id == null
                    ? _repository.Any(a => a.Email == email && a.StatusID == Status.Active)
                    : _repository.Any(a => a.Email == email && a.StatusID == Status.Active && a.ID != id);
            }

            var entities = _uow.Repository<Entities>()
                                .GetAll()
                                .Where(
                                    a =>
                                        a.StatusID == Status.Active &&
                                        ((Common.EntityTypes) a.TypeID == Common.EntityTypes.Entity ||
                                         (Common.EntityTypes) a.TypeID == Common.EntityTypes.Client ||
                                         (Common.EntityTypes) a.TypeID == Common.EntityTypes.Group))
                                .ToDictionary(a => a.ID, a => a.ParentID);

            var ids = new List<int> {entityId};
            var common = new CommonComponent();
            common.GetParentsNChilds(entityId, ids, entities);

            return id == null
                ? _repository.Any(
                    a => a.Email == email && ids.Contains(a.EntityID) && a.StatusID == Status.Active)
                : _repository.Any(
                    a =>
                        a.Email == email && ids.Contains(a.EntityID) && a.StatusID == Status.Active && a.ID != id);
        }

        public string GetAccountManagers(string xml)
        {
            var model = (SearchFilterModel)ModelDeserializer.DeserializeFromXml<SearchFilterModel>(xml);
            return GetAccountManagers(model);
        }

        private string GetAccountManagers(SearchFilterModel model)
        {
            var retryAllowedAttempts =
                Convert.ToInt32(_uow.Repository<Settings>().GetAll()
                    .First(
                        a =>
                            a.SettingTypeID == (int) Common.SettingTypes.PasswordSettings &&
                            a.Description == "MaxPasswordRetries").SettingValue);

            var managers = new List<AccountManagers>();
            var query = ApplyFilter(_repository.GetAll(), model);
            var count = query.Count();

            if (count > 1000)
                query = query.Take(1000);

            var ids = query.Select(a => a.ID).ToList();
            var sessionManager = _repository.GetAll().First(a => a.ID == model.UserID);

            CheckChild(sessionManager.Entity, ids, managers);

            var models = managers.Select(a => new AccountManagerViewModel
            {
                Account = (Common.EntityTypes) a.Entity.TypeID == Common.EntityTypes.Client ? a.Entity.Name : "-",
                CreationDate = a.CreatedOn,
                Email = a.Email,
                Firm =
                    (Common.EntityTypes) a.Entity.TypeID == Common.EntityTypes.Client
                        ? a.Entity.ParentEntity.Name
                        : a.Entity.Name,
                Group = a.User != null ? a.User.Group.Title : "",
                ID = a.ID,
                Name = a.FirstName + " " + a.LastName,
                Jobs = a.Jobs.Count(x => x.StatusID == Status.Active),
                LastLogonTime = a.User != null ? a.User.LastLoginTime : null,
                Locked =
                    a.User != null
                        ? (a.User.FailedPasswordAttemptCount != null
                            ? (a.User.FailedPasswordAttemptCount >= retryAllowedAttempts ? "Yes" : "No")
                            : "")
                        : "-"
            }).ToList();

            return ModelSerializer.GenerateReponseXml<AccountManagerViewModel>(models,
                count > 1000 ? ResponseStatus.Warning : ResponseStatus.Success, count > 100 ? count.ToString() : "Successful");
        }

        private IQueryable<AccountManagers> ApplyFilter(IQueryable<AccountManagers> accountManagers, SearchFilterModel model)
        {
            accountManagers = accountManagers.Where(a => a.StatusID == Status.Active);

            if (!string.IsNullOrEmpty(model.Firm))
            {
                accountManagers = from a in accountManagers
                                  where
                                    (Common.EntityTypes)a.Entity.TypeID == Common.EntityTypes.Client ?
                                        a.Entity.ParentEntity.Name.ToLower().Contains(model.Firm.ToLower()) :
                                        a.Entity.Name.Contains(model.Firm)
                                  select a;
            }

            if (!string.IsNullOrEmpty(model.Account))
            {
                accountManagers = from a in accountManagers
                                  where
                                      (Common.EntityTypes)a.Entity.TypeID == Common.EntityTypes.Client ?
                                          a.Entity.ParentEntity.Name.ToLower().Contains(model.Firm.ToLower()) : true
                                  select a;
            }

            if (!string.IsNullOrEmpty(model.FirstName))
                accountManagers = accountManagers.Where(a => a.FirstName.ToLower() == model.FirstName.ToLower());

            if (!string.IsNullOrEmpty(model.LastName))
                accountManagers = accountManagers.Where(a => a.LastName.ToLower() == model.LastName.ToLower());

            if (!string.IsNullOrEmpty(model.Email))
                accountManagers = accountManagers.Where(a => a.Email.ToLower() == model.Email.ToLower());

            if (model.DateFrom != null)
                accountManagers = accountManagers.Where(a => a.CreatedOn >= model.DateFrom.Value);

            if (model.DateTo != null)
                accountManagers = accountManagers.Where(a => a.CreatedOn <= model.DateTo.Value);

            return accountManagers;
        }

        private void CheckChild(Entities entity, List<int> listAccountManagerID, List<AccountManagers> managers)
        {
            foreach (var item in entity.AccountManagers.Where(a => listAccountManagerID.Contains(a.ID)).ToList())
            {
                if (!managers.Any(a => a.ID == item.ID))
                    managers.Add(item);
            }

            if (entity.TypeID == (int)Common.EntityTypes.Entity)
            {
                foreach (var childsEntities in entity.ChildEntities)
                {
                    CheckChild(childsEntities, listAccountManagerID, managers);
                }
            }
        }

        public string HasMultipleUserAccess(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return HasMultipleUserAccess(model.Value);
        }

        private string HasMultipleUserAccess(int managerId)
        {
            var entity = _uow.Repository<AccountManagers>().GetById(managerId).Entity;
            var list = GetAccountManagers(entity);

            var id = list.Count == 1 ? list[0] : 0; // Check if only one id exists in the list then return the id else return 0.

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = id },
                ResponseStatus.Success, "Successful");
        }

        private List<int> GetAccountManagers(Entities entity)
        {
            var list = new List<int>();

            if ((Common.EntityTypes) entity.TypeID == Common.EntityTypes.Entity ||
                (Common.EntityTypes) entity.TypeID == Common.EntityTypes.Client)
            {
                list.AddRange(entity.AccountManagers.Where(a => a.StatusID == Status.Active).Select(a => a.ID));
            }

            if (list.Count > 1)
                return list;

            foreach (var item in entity.ChildEntities.Where(a=>a.StatusID == Status.Active))
            {
                if ((Common.EntityTypes) item.TypeID == Common.EntityTypes.Entity ||
                    (Common.EntityTypes) item.TypeID == Common.EntityTypes.Group ||
                    (Common.EntityTypes) item.TypeID == Common.EntityTypes.Client)
                {
                    list.AddRange(GetAccountManagers(item));
                }

                if (list.Count > 1)
                    return list;
            }

            return list;
        }
    }
}