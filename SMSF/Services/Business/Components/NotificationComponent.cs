﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class NotificationComponent : Component
    {
        private readonly UnitOfWork _uow = new UnitOfWork();
        private readonly IRepository<Notifications> _repository;

        public NotificationComponent()
        {
            _repository = _uow.Repository<Notifications>();
        }

        public string AddError(string xml)
        {
            var model = (ErrorModel)ModelDeserializer.DeserializeFromXml<ErrorModel>(xml);
            
            InsertNotification(NotificationTypes.Error, model.Message, model.MessageDetail, null, model.EntityID, false, model.UserID);

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        public void AddError(Exception ex, RequestModel requestModel)
        {
            var message = ex.Message;
            var messageDetail = ex.InnerException != null ? ex.InnerException.Message : null;
            var entityId = ModelDeserializer.GetAttributeValue(requestModel.ModelXml, "EntityID");
            entityId = entityId ?? ModelDeserializer.GetAttributeValue(requestModel.ModelXml, "Key");

            InsertNotification(NotificationTypes.Error, message, messageDetail, null, (entityId != null? Convert.ToInt32(entityId): (int?)null), false, requestModel.UserID);
        }

        public void InsertNotification(NotificationTypes type, string message, string messageDetail, string entityType, int? releventId, bool isSystem, int createBy)
        {
            _repository.Add(new Notifications
            {
                CreatedBy = createBy,
                EntityType = entityType,
                MessageDetail = messageDetail,
                IsSystem = isSystem,
                Message = message,
                ReleventID = releventId,
                Type = (int)type,
                CreatedOn = DateTime.Now
            });

            _uow.Commit();
        }

        public string DeleteNotifications(string xml)
        {
            var model = (NotificationDeleteModel)ModelDeserializer.DeserializeFromXml<NotificationDeleteModel>(xml);
            return DeleteNotifications(model.ID, model.EntityID, model.Days);
        }

        private string DeleteNotifications(int? id, int? entityId, int? days)
        {
            if (id == null)
            {
                var entities = _uow.Repository<Entities>().GetAll()
                                                         .Where(a => a.StatusID == Status.Active && ((Contracts.Common.EntityTypes)a.TypeID == Contracts.Common.EntityTypes.Entity || (Contracts.Common.EntityTypes)a.TypeID == Contracts.Common.EntityTypes.Client || (Contracts.Common.EntityTypes)a.TypeID == Contracts.Common.EntityTypes.Group))
                                                         .ToDictionary(a => a.ID, a => a.ParentID);
                var ids = new List<int>();
                if (entityId != null)
                {
                    ids.Add((int)entityId);
                    new CommonComponent().GetParentsNChilds((int)entityId, ids, entities);
                }

                var date = DateTime.Now;
                if (days != 0)
                    if (days != null) date = date.AddDays((int)days * -1);

                var notifications = (from a in _repository.GetAll()
                                     join b in _uow.Repository<AccountManagers>().GetAll() on a.CreatedBy equals b.ID
                                     where b.StatusID == Status.Active && ids.Contains(b.EntityID) && !a.IsSystem && (days != 0 ? a.CreatedOn >= date : true)
                                     select a).ToList();

                foreach (var n in notifications)
                    _repository.Delete(n);

                _uow.Commit();
            }
            else
            {
                _repository.Delete(_repository.GetById((int)id));
                _uow.Commit();
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        public string GetNotifications(string xml)
        {
            var model = (NotificationGetModel)ModelDeserializer.DeserializeFromXml<NotificationGetModel>(xml);
            return GetNotifications(model.Type, model.UserID, model.Days, model.IsSystem, model.Page, model.Count);
        }

        private string GetNotifications(NotificationTypes? type, int userID, int days, bool systemNotifications, int page, int count)
        {
            var entityId = _uow.Repository<AccountManagers>().GetAll().First(a => a.ID == userID).EntityID;
            var entities = _uow.Repository<Entities>().GetAll().Where(a => a.StatusID == Status.Active && ((Contracts.Common.EntityTypes)a.TypeID == Contracts.Common.EntityTypes.Entity || (Contracts.Common.EntityTypes)a.TypeID == Contracts.Common.EntityTypes.Client || (Contracts.Common.EntityTypes)a.TypeID == Contracts.Common.EntityTypes.Group))
                                               .ToDictionary(a => a.ID, a => a.ParentID);
            var ids = new List<int> {entityId};
            new CommonComponent().GetParentsNChilds(entityId, ids, entities);

            var query = from n in _repository.GetAll()
                        join a in _uow.Repository<AccountManagers>().GetAll() on n.CreatedBy equals a.ID
                        where a.StatusID == Status.Active && ids.Contains(a.EntityID) && n.IsSystem == systemNotifications
                        select new { n, a.EntityID };

            if (type != null)
                query = query.Where(a => a.n.Type == (int)type);
            if (days != 0)
            {
                var date = DateTime.Now.AddDays(days * -1);
                query = query.Where(a => a.n.CreatedOn >= date);
            }

            var notifications = query.OrderByDescending(a => a.n.CreatedOn)
                                    .Skip((page - 1) * count)
                                    .Take(count)
                                    .Select(a => new NotificationViewModel
                                    {
                                        ID = a.n.ID,
                                        Message = a.n.Message,
                                        MessageDetail = a.n.MessageDetail,
                                        ReleventID = a.n.ReleventID,
                                        EntityType = a.n.EntityType,
                                        Type = (NotificationTypes)a.n.Type,
                                        CreateDate = a.n.CreatedOn
                                    }).ToList();

            var totalCount = query.Count();

            return ModelSerializer.GenerateReponseXml(new NotificationModel { Count = totalCount, Data = notifications }, ResponseStatus.Success, "Successful");
        }
    }
}
