﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Extensions;
using DBASystem.SMSF.Data.Models;
using DBASystem.SMSF.Service.Business.WorkflowHelper.Common;
using DBASystem.SMSF.Service.Business.WorkflowHelper.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;

using Common = DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class JobComponent : Component
    {
        private UnitOfWork _uow = new UnitOfWork();
        private IRepository<Jobs> _repository;

        public string ImportFilesFolderName
        {
            get
            {
                return "JobImportFiles";
            }
        }

        public string AuditProcedureFolderName
        {
            get
            {
                return "Audit Procedures";
            }
        }

        public JobComponent()
        {
            _repository = _uow.Repository<Jobs>();
        }

        #region AccountManagers

        public string GetJobManagers(string xml)
        {
            var model = (GenericModel<int?>)ModelDeserializer.DeserializeFromXml<GenericModel<int?>>(xml);
            if (model != null)
                return GetJobManagers(model.Value);

            return NullModelReference();
        }

        private string GetJobManagers(int? fundId)
        {

            // Entity1 is the foriegn key of Client ID and ParentID is the ID Client's Entity
            var entityId = _uow.Repository<Entities>().GetAll().First(b => b.ID == fundId).ParentEntity.ParentID;
            var entities = new CommonComponent().GetParents(_uow, (int)entityId,new List<Common.EntityTypes>(){Common.EntityTypes.Entity});
            
            var managers = _uow.Repository<AccountManagers>().GetAll()
                                                            .Where(a => a.StatusID == Status.Active && entities.Contains(a.EntityID) && a.Entity.EntityType.ID == 1 )
                                                            .Select(a => new AccountManagerSelect
                                                            {
                                                                AccountManagerName = a.FirstName + " " + a.LastName + "(" + a.Email + ")",
                                                                ID = a.ID
                                                            })
                                                            .ToList();

            return ModelSerializer.GenerateReponseXml<AccountManagerSelect>(managers, ResponseStatus.Success,
                "Successful");
        }

        public string GetJobRoles(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            var workflowSelectModel = new WorkflowSelector().GetFlowforJobProcessByFund(model.Value);

            return ModelSerializer.GenerateReponseXml<JobRolesModel>(new WorkflowComponent().GetWorkFlowActors(workflowSelectModel), ResponseStatus.Success, "Successful");
        }

        #endregion AccountManagers

        #region Jobs [CRUD]

        #region Create

        public string AddJob(string xml)
        {
            var model = (JobCreateModel)ModelDeserializer.DeserializeFromXml<JobCreateModel>(xml);
            return AddJob(model);
        }

        private string AddJob(JobCreateModel model)
        {
            var workflowSelectModel = new WorkflowSelector().GetFlowforJobProcessByFund(model.FundId);
            if (String.IsNullOrEmpty(workflowSelectModel.Name))
                throw new Exception("Workflow for Job process not found");

            var newJob = new Jobs
                {
                    Description = model.Description,
                    CreatedBy = model.CreatedBy,
                    CreatedOn = model.CreatedOn,
                    ModifiedBy = model.ModifiedBy,
                    ModifiedOn = model.ModifiedOn,
                    StartDate = model.StartDate,
                    CompleteDate = model.CompleteDate,
                    DataLink = model.DataLink,
                    Title = model.Title,
                    StatusID = Status.Active,
                    WorkflowStatusID = model.SelectedJobManager.Any() ? 1 : 4,
                    FundID = model.FundId,
                    Identifier = Guid.NewGuid(),
                    WorkingHours = model.WorkingHours,
                    EstimatedHours = model.EstimatedHours,
                    AccountManagerJobs = model.SelectedJobManager.Select(a =>
                        new AccountManagerJobs
                        {
                            AccountManagerID = a.ID,
                            StatusID = Status.Active,
                            CreatedBy = model.CreatedBy,
                            CreatedOn = model.CreatedOn,
                            JobRoleID = a.SelectedRoleID
                        }).ToList()
                };

            _repository.Add(newJob);
            _uow.Commit();
            _uow = new UnitOfWork();
            _repository = _uow.Repository<Jobs>();
            var job = _repository.GetById(newJob.ID);

            var message =
                WorkflowInit.GetWorkFlowInstance(Workflows.JobProcess, (Guid)newJob.Identifier, workflowSelectModel.Name).Status;

            var response = CreateJobFolder(job, model.CreatedBy);
            if (response.Status == ResponseStatus.Failure)
            {
                return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = newJob.ID },
                    ResponseStatus.Warning, response.StatusMessage);
            }
            else
            {
                response = new DocumentComponent(model.CreatedBy).CopyFiles(response.ResponseMessage);
            }
            _uow.Dispose();
            return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = newJob.ID }, ResponseStatus.Success,
                "Successful");
        }

        private ResponseModel CreateJobFolder(Jobs job, int userID)
        {
            var entityList = job.Fund.GetFolderHierarchy();
            var folderName = job.Title;
            entityList.Reverse();
            var rootFolder = String.Join("/", entityList);
            //Create new Job Folder with Sub Folder for import Files
            return new DocumentComponent(userID).AddFolder(folderName, rootFolder, new List<string>() { ImportFilesFolderName, AuditProcedureFolderName });
        }

        #endregion Create

        #region Read

        public string GetJobDetail(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetJobDetail(model.Value);
        }

        private string GetJobDetail(int id)
        {
            var job = _repository.GetById(id);
            if (job != null)
            {
                var model = new JobCreateModel
                {
                    ID = job.ID,
                    Title = job.Title,
                    SelectedJobManager = job.AccountManagerJobs.Select(p =>
                                                             new AccountManagerSelect
                                                             {
                                                                 Email = p.AccountManager.Email,
                                                                 AccountManagerName =
                                                                     p.AccountManager.FirstName + " " + p.AccountManager.LastName +
                                                                     (p.AccountManager.StatusID != Status.Active
                                                                         ? " ( " + (p.AccountManager.StatusID).ToString() + " )"
                                                                         : ""),
                                                                 Selected = p.StatusID == Status.Active,
                                                                 SelectedRoleName = (p.JobRole != null ? p.JobRole.Name : ""),
                                                                 SelectedRoleID = (p.JobRole != null ? p.JobRole.ID : 0),
                                                                 ID = p.AccountManager.ID
                                                             }).ToList(),
                    CompleteDate = job.CompleteDate,
                    StartDate = job.StartDate,
                    CreatedOn = job.CreatedOn,
                    CreatedBy = job.CreatedBy,
                    EntityID = Convert.ToInt32(job.Fund.ParentID),
                    DataLink = job.DataLink,
                    FundId = job.FundID,
                    Description = job.Description,
                    EstimatedHours = job.EstimatedHours,
                    ActualWorkingHours =
                        job.WorkLogs.Any()
                            ? job.WorkLogs.Select(x => Math.Round(x.End.Subtract(x.Start).TotalHours, 2)).Sum(x => x)
                            : (double?)null,
                    ClientName = job.Fund.ParentEntity.Name,
                    FundName = job.Fund.Name,
                    Path = job.GetFolderPath(),
                    Firm = job.Fund.ParentEntity.ParentEntity.Name,
                    Notes = job.JobNotes.Select(n =>
                                                             new NotesViewModel
                                                             {
                                                                 Note = n.Note,
                                                                 Date = n.ModifiedOn ?? n.CreatedOn,
                                                                 Id = n.ID,
                                                                 CreatedBy =
                                                                     _uow.Repository<AccountManagers>()
                                                                         .GetAll()
                                                                         .Where(p => p.ID == (n.ModifiedBy ?? n.CreatedBy))
                                                                         .Select(q => q.FirstName + " " + q.LastName)
                                                                         .FirstOrDefault()
                                                             }).ToList(),
                    AuditComments = job.AuditComments.Select(q => new AuditProcedureCommentViewModel
                        {
                            Id = q.ID,
                            Comment = q.Text,
                            AuditProcedureId = q.AuditProcedureId,
                            AccountManagerName = _uow.Repository<AccountManagers>()
                                                                           .GetAll()
                                                                           .Where(p => p.ID == (q.ModifiedBy ?? q.CreatedBy))
                                                                           .Select(r => r.FirstName + " " + r.LastName)
                                                                           .FirstOrDefault(),
                            CreatedOn = q.CreatedOn
                        }).ToList()
                };

                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
            }

            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Successful");
        }

        public string GetJob(string xml)
        {
            var requestParams = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetJob(requestParams.Value);
        }

        private string GetJob(int id)
        {
            var job = _repository.GetById(id);

            if (job == null) return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Successful");

            var jobFlows = (
                from x in _uow.WorkflowRepository<WorkflowTransitionUsers>().GetAll()
                join y in _uow.WorkflowRepository<WorkflowTransactionHistory>().GetAll() on x.TransactionId equals y.Id
                join z in _uow.Repository<AccountManagerJobs>().GetAll() on y.ProcessIdentity equals z.Job.Identifier
                where
                    z.JobID == id
                    && z.JobRole.Identifier == x.UserID
                    && y.TransitionTime == null
                    && y.Command != "Auto"
                    && y.WorkflowId == (int)Workflows.JobProcess
                group y by new { z.AccountManager.FirstName, z.AccountManager.LastName, z.Job.Identifier, y.InitialState, jobRole = z.JobRole.Identifier }
                    into g
                    select new WorkflowJobDetail
                    {
                        Assignee = new JobAssignee { Name = g.Key.FirstName + " " + g.Key.LastName, Role = g.Key.jobRole },
                        JobIdentifier = (Guid)g.Key.Identifier,
                        WorkflowState = g.Key.InitialState
                    }

                ).ToList();

            var model = new JobModel
            {
                ID = job.ID,
                FundID = job.FundID,
                Title = job.Title,
                Description = job.Description,
                StartDate = job.StartDate,
                CompleteDate = job.CompleteDate,
                CreatedOn = job.CreatedOn,
                WorkingHours =
                    job.WorkLogs.Any()
                        ? job.WorkLogs.Select(x => Math.Round(x.End.Subtract(x.Start).TotalHours, 2)).Sum(x => x)
                        : (double?)null,
                StatusID = job.StatusID,
                DataLink = job.DataLink,
                CreatedBy = job.CreatedBy,
                Path = job.GetFolderPath(),
                ClientName = job.Fund.ParentEntity.Name,
                Fund = new FirmModels
                {
                    ID = job.Fund.ID,
                    Name = job.Fund.Name,
                    Type = (Common.EntityTypes)job.Fund.TypeID,
                },
                AccountManager = job.AccountManagerJobs.Any(
                    q => q.JobRole.Identifier == new Guid("BA0A02DB-9AD8-406F-96B6-2AB87C8023FF"))
                    ? job.AccountManagerJobs.Where(
                        q => q.JobRole.Identifier == new Guid("BA0A02DB-9AD8-406F-96B6-2AB87C8023FF"))
                        .Select(p => p.AccountManager.FirstName + " " + p.AccountManager.LastName)
                        .FirstOrDefault()
                    : "",
                Auditor = string.Join(", ", jobFlows.Where(p => p.JobIdentifier == (Guid)job.Identifier)
                    .Select(p => p.Assignee.Name)
                    .ToList())
            };

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        public string GetJobs(string xml)
        {
            var model = (SearchFilterModel)ModelDeserializer.DeserializeFromXml<SearchFilterModel>(xml);
            return GetJobs(model);
        }

        private string GetJobs(SearchFilterModel model)
        {
            var query = ApplyFilter(_repository.GetAll(), model);
            var count = query.Count();
            var jobs = count > 1000
                        ? query.Take(1000).OrderByDescending(a => a.CreatedOn).ToList()
                        : query.OrderByDescending(a => a.CreatedOn).ToList();

            var list = new List<Jobs>();
            foreach (var item in jobs)
            {
                var client = item.Fund.ParentEntity;
                if (client.AccountManagers.Any(a => a.ID == model.UserID))
                    list.Add(item);
                else
                {
                    if (CheckParent(client.ParentEntity, model.UserID))
                        list.Add(item);
                }
            }

            var jobIds = list.Select(x => x.ID).ToArray();

            var jobFlows = (
                from x in _uow.WorkflowRepository<WorkflowTransitionUsers>().GetAll()
                join y in _uow.WorkflowRepository<WorkflowTransactionHistory>().GetAll() on x.TransactionId equals y.Id
                join z in _uow.Repository<AccountManagerJobs>().GetAll() on y.ProcessIdentity equals z.Job.Identifier
                where
                    jobIds.Contains(z.JobID)
                    && z.JobRole.Identifier == x.UserID
                    && y.TransitionTime == null
                    && y.Command != "Auto"
                    && y.WorkflowId == (int)Workflows.JobProcess
                group y by new { z.AccountManager.FirstName, z.AccountManager.LastName, z.Job.Identifier, y.InitialState, jobRole = z.JobRole.Identifier }
                    into g
                    select new WorkflowJobDetail
                {
                    Assignee = new JobAssignee { Name = g.Key.FirstName + " " + g.Key.LastName, Role = g.Key.jobRole },
                    JobIdentifier = (Guid)g.Key.Identifier,
                    WorkflowState = g.Key.InitialState
                }

                ).ToList();

            var models = new List<JobsModel>();
            if (list.Count > 0)
            {
                models = list.Select(a => new JobsModel
                {
                    Account = a.Fund.ParentEntity.Name,
                    CompletedOn = a.CompleteDate,
                    CreatedOn = a.CreatedOn,
                    FundID = a.FundID,
                    Fund = a.Fund.Name,
                    ID = a.ID,
                    Name = a.Title,
                    Status = JobStatus((JobWorkflowStatus)a.WorkflowStatusID, a.StartDate, a.CompleteDate).GetDescription(),
                    WorkflowState =
                        WorkflowState(JobStatus((JobWorkflowStatus)a.WorkflowStatusID, a.StartDate, a.CompleteDate),
                            jobFlows.FirstOrDefault(p => p.JobIdentifier == (Guid)a.Identifier)),
                    Path = a.GetFolderPath(),
                    EstimatedHours = a.EstimatedHours,
                    Actual =
                        a.WorkLogs.Any()
                            ? a.WorkLogs.Select(x => Math.Round(x.End.Subtract(x.Start).TotalHours, 2))
                                .Sum(x => x)
                                .ToString()
                            : "",
                    AccountManagers = a.AccountManagerJobs.Where(
                                q => q.JobRole.Identifier == new Guid("BA0A02DB-9AD8-406F-96B6-2AB87C8023FF"))
                                .Select(p => p.AccountManager.FirstName + " " + p.AccountManager.LastName)
                        .ToList(),
                    Assignee =
                        jobFlows.Where(p => p.JobIdentifier == (Guid)a.Identifier)
                            .Select(p => p.Assignee.Name)
                            .ToList(),
                }).ToList();
            }

            return ModelSerializer.GenerateReponseXml<JobsModel>(models,
                count > 1000 ? ResponseStatus.Warning : ResponseStatus.Success,
                count > 100 ? count.ToString() : "Successful");
        }

        private JobWorkflowStatus JobStatus(JobWorkflowStatus status, DateTime startDate, DateTime? completionDate)
        {
            if (status == JobWorkflowStatus.Completed)
                return status;
            if (startDate > DateTime.Now)
                return JobWorkflowStatus.NotStarted;
            if (completionDate < DateTime.Now && (status == JobWorkflowStatus.InProgress || status == JobWorkflowStatus.NotStarted))
                return JobWorkflowStatus.Overdue;

            return status;
        }

        private string WorkflowState(JobWorkflowStatus jobStatus, WorkflowJobDetail workflowJobDetail)
        {
            if (jobStatus == JobWorkflowStatus.InProgress && workflowJobDetail != null)
            {
                return workflowJobDetail.WorkflowState;
            }

            return "";
        }

        private bool CheckParent(Entities entity, int userId)
        {
            if (entity.AccountManagers.Any(a => a.ID == userId))
                return true;

            if (entity.ParentEntity != null)
                return CheckParent(entity.ParentEntity, userId);

            return false;
        }

        public string GetFundJobs(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return GetFundJobs(model.Value);
        }

        private string GetFundJobs(int id)
        {
            var fundRepository = _uow.Repository<Entities>();
            var fund = fundRepository.GetAll().FirstOrDefault(
                a => a.StatusID == Status.Active &&
                     a.TypeID == (int)Common.EntityTypes.Fund &&
                     a.ID == id
                );
            if (fund != null)
            {
                if (fund.Jobs.Any())
                {
                    var jobs = fund.Jobs.Where(q => q.StatusID == Status.Active || q.StatusID == Status.Pending).Select(
                        p => new FundsJobView { JobFlowStatus = p.WorkflowStatus.Name, Title = p.Title }).ToList();
                    return ModelSerializer.GenerateReponseXml(jobs, ResponseStatus.Success, "Successful");
                }
            }

            return ModelSerializer.GenerateReponseXml(new List<FundsJobView>(), ResponseStatus.Success, "Successful");
        }

        private IQueryable<Jobs> ApplyFilter(IQueryable<Jobs> jobs, SearchFilterModel model)
        {
            jobs = jobs.Where(a => a.StatusID == Status.Active);

            if (model.Status != 0)
            {
                if (model.Status == 1)
                    jobs = jobs.Where(a => a.WorkflowStatusID == model.Status && a.StartDate <= DateTime.Now && a.AccountManagerJobs.Any());
                else if (model.Status == 3)//If overdue
                    jobs = jobs.Where(a => (a.WorkflowStatusID == 1 || a.WorkflowStatusID == 4) && a.CompleteDate < DateTime.Now);
                else if (model.Status == 4)
                    jobs = jobs.Where(a => a.WorkflowStatusID == model.Status || a.StartDate > DateTime.Now);
                else
                    jobs = jobs.Where(a => a.WorkflowStatusID == model.Status);
            }

            if (!string.IsNullOrEmpty(model.Firm))
                jobs = jobs.Where(a => a.Fund.ParentEntity.ParentEntity.Name.ToLower().Contains(model.Firm.ToLower()));

            if (!string.IsNullOrEmpty(model.Account))
                jobs = jobs.Where(a => a.Fund.ParentEntity.Name.ToLower().Contains(model.Account.ToLower()));

            if (!string.IsNullOrEmpty(model.Fund))
                jobs = jobs.Where(a => a.Fund.Name.ToLower().Contains(model.Fund.ToLower()));

            if (!string.IsNullOrEmpty(model.ABN))
                jobs = jobs.Where(a => a.Fund.ABN.ToLower().Contains(model.ABN.ToLower()));

            if (model.DateFrom != null)
                jobs = jobs.Where(a => a.CreatedOn >= model.DateFrom.Value);

            if (model.DateTo != null)
                jobs = jobs.Where(a => a.CreatedOn <= model.DateTo.Value);

            if (model.IsMyJob)
                jobs = jobs.Where(a => a.AccountManagerJobs.Any(x => x.AccountManagerID == model.UserID));

            if (!string.IsNullOrEmpty(model.AccountManager))
                jobs =
                    jobs.Where(
                        a =>
                            a.AccountManagerJobs.Any(
                                x =>
                                    x.AccountManager.FirstName.ToLower().Contains(model.AccountManager.ToLower()) ||
                                    x.AccountManager.LastName.ToLower().Contains(model.AccountManager.ToLower())));

            return jobs;
        }

        public string HasMultipleJobAccess(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return HasMultipleJobAccess(model.Value);
        }

        private string HasMultipleJobAccess(int userId)
        {
            var entity = _uow.Repository<AccountManagers>().GetById(userId).Entity;
            var list = GetJobs(entity);

            var id = list.Count == 1 ? list[0] : 0;
            // Check if only one id exists in the list then return the id else return 0.

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = id },
                ResponseStatus.Success, "Successful");
        }

        private List<int> GetJobs(Entities entity)
        {
            var list = new List<int>();

            foreach (var item in entity.ChildEntities.Where(a => a.StatusID == Status.Active))
            {
                if ((Common.EntityTypes)item.TypeID == Common.EntityTypes.Fund)
                    list.AddRange(
                        item.Jobs.Where(
                            a =>
                                a.StatusID == Status.Active &&
                                //(JobWorkflowStatus)a.WorkflowStatusID == JobWorkflowStatus.InProgress &&
                                a.StartDate <= DateTime.Now && a.AccountManagerJobs.Any()).Select(a => a.ID)
                        );

                if ((Common.EntityTypes)item.TypeID == Common.EntityTypes.Entity ||
                    (Common.EntityTypes)item.TypeID == Common.EntityTypes.Group ||
                    (Common.EntityTypes)item.TypeID == Common.EntityTypes.Client)
                {
                    list.AddRange(GetJobs(item));
                }

                if (list.Count > 1)
                    return list;
            }

            return list;
        }

        #endregion Read

        #region Update

        public string UpdateJob(string xml)
        {
            var model = (JobCreateModel)ModelDeserializer.DeserializeFromXml<JobCreateModel>(xml);
            return UpdateJob(model);
        }

        private string UpdateJob(JobCreateModel model)
        {
            var updatedJob = _repository.GetById(model.ID);

            bool renameFolder = updatedJob.Title != model.Title;
            string oldJobName = renameFolder ? updatedJob.Title : "";
            updatedJob.Description = model.Description;
            updatedJob.ModifiedBy = model.ModifiedBy;
            updatedJob.ModifiedOn = model.ModifiedOn;
            updatedJob.StartDate = model.StartDate;
            updatedJob.DataLink = model.DataLink;
            updatedJob.Title = model.Title;
            updatedJob.StatusID = Status.Active;
            updatedJob.FundID = model.FundId;
            updatedJob.WorkingHours = model.WorkingHours;
            updatedJob.CompleteDate = model.CompleteDate;
            updatedJob.EstimatedHours = model.EstimatedHours;
            //UpdateAccountManagersOnJob(model, UpdatedJob);

            while (updatedJob.AccountManagerJobs.Count > 0)
            {
                _uow.Repository<AccountManagerJobs>().Delete(updatedJob.AccountManagerJobs.ElementAt(0));
            }

            updatedJob.AccountManagerJobs = model.SelectedJobManager.Select(a =>
                        new AccountManagerJobs
                        {
                            AccountManagerID = a.ID,
                            StatusID = Status.Active,
                            CreatedBy = model.CreatedBy,
                            CreatedOn = DateTime.Now,
                            JobRoleID = a.SelectedRoleID,
                            JobID = updatedJob.ID,
                            Job = updatedJob
                        }).ToList();
            _uow.Commit();
            if (renameFolder)
            {
                var response = RenameJobFolder(updatedJob, oldJobName, (int)model.ModifiedBy);
                if (response.Status == ResponseStatus.Failure)
                {
                    return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Warning, response.StatusMessage);
                }
            }

            _uow.Dispose();
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Successful");
        }

        private ResponseModel RenameJobFolder(Jobs job, string oldName, int userId)
        {
            var entityList = job.Fund.GetFolderHierarchy();
            entityList.Reverse();
            var rootFolder = String.Join("/", entityList);
            return new DocumentComponent(userId).RenameFolder(oldName, job.Title, String.Join("/", entityList));
        }

        #endregion Update

        #region Delete

        public string Deletejob(string xml)
        {
            var jobToDelete =
                (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            var entityID = DeleteJob(jobToDelete.Key, jobToDelete.Value);

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = entityID }, ResponseStatus.Success,
                "Successful");
        }

        private int DeleteJob(int jobID, int userId)
        {
            var jobToDelete = _repository.GetById(jobID);
            if (jobToDelete != null)
            {
                jobToDelete.StatusID = Status.Deleted;
                jobToDelete.ModifiedBy = userId;
                jobToDelete.ModifiedOn = DateTime.Now;
            }

            DeleteJobFolder(jobToDelete, userId);
            _uow.Commit();

            return jobToDelete.FundID;
        }

        private ResponseModel DeleteJobFolder(Jobs job, int userID)
        {
            var entityList = job.Fund.GetFolderHierarchy();
            entityList.Reverse();
            var rootFolder = String.Join("/", entityList);
            return new DocumentComponent(userID).DeleteFolder(String.Join("/", entityList), job.Title);
        }

        #endregion Delete

        #region Chart

        public string GetJobToBeCompleted(string xml)
        {
            var model = (JobGraphModel)ModelDeserializer.DeserializeFromXml<JobGraphModel>(xml);
            return GetJobToBeCompleted(model.UserID, model.Group.Value, model.StartDate, model.EndDate);
        }

        private string GetJobToBeCompleted(int userId, UserGroup group, DateTime startDate, DateTime endDate)
        {
            var jobs = new List<Jobs>();
            var query =
                _repository.GetAll()
                    .Where(
                        a =>
                            a.StatusID == Status.Active && a.CompleteDate != null && a.CompleteDate >= startDate &&
                            a.CompleteDate <= endDate);

            if (group == UserGroup.Administrators)
                jobs.AddRange(query.AsEnumerable().Where(item => CheckParent(item.Fund, userId)));

            if (group == UserGroup.Operators)
                jobs.AddRange(query.Where(item => item.AccountManagerJobs.Any(a => a.AccountManagerID == userId)));

            var model = (
                            from a in jobs
                            let completeDate = a.CompleteDate
                            where completeDate != null
                            orderby completeDate
                            group a by Convert.ToDateTime(completeDate.Value.ToShortDateString())
                                into g
                                select new JobGraphModel
                                {
                                    Count = g.Count(),
                                    Status = g.Key.ToString("MM/dd/yyyy")
                                }
                        ).ToList();

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        public string GetCompletedJobs(string xml)
        {
            var model = (JobGraphModel)ModelDeserializer.DeserializeFromXml<JobGraphModel>(xml);
            return GetCompletedJobs(model.UserID, model.Group.Value, model.StartDate, model.EndDate);
        }

        private string GetCompletedJobs(int userId, UserGroup group, DateTime startDate, DateTime endDate)
        {
            var jobs = new List<Jobs>();
            var query =
                _repository.GetAll()
                    .Where(
                        a =>
                            a.StatusID == Status.Active && a.StartDate >= startDate && a.StartDate <= endDate &&
                            (JobWorkflowStatus) a.WorkflowStatusID == JobWorkflowStatus.Completed);

            if (group == UserGroup.Administrators)
                jobs.AddRange(query.AsEnumerable().Where(item => CheckParent(item.Fund, userId)));

            if (group == UserGroup.Operators)
                jobs.AddRange(query.Where(item => item.AccountManagerJobs.Any(a => a.AccountManagerID == userId)));

            var model = (
                            from a in jobs
                            let completeDate = a.CompleteDate
                            where completeDate != null
                            orderby completeDate
                            group a by Convert.ToDateTime(completeDate.Value.ToShortDateString())
                                into g
                                select new JobGraphModel
                                {
                                    Count = g.Count(),
                                    Date = g.Key.ToString("d-MMM")
                                }
                        ).ToList();

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        public string GetScheduledJobs(string xml)
        {
            var model = (JobGraphModel)ModelDeserializer.DeserializeFromXml<JobGraphModel>(xml);
            return GetScheduledJobs(model.UserID, model.Group.Value, model.StartDate, model.EndDate);
        }

        private string GetScheduledJobs(int userId, UserGroup group, DateTime startDate, DateTime endDate)
        {
            var jobs = new List<Jobs>();
            var query =
                _repository.GetAll()
                    .Where(
                        a =>
                            a.StatusID == Status.Active &&
                            ((a.StartDate >= startDate && a.StartDate <= endDate)
                            || (a.CompleteDate != null ? (a.CompleteDate >= startDate && a.CompleteDate <= endDate) : (a.StartDate >= startDate && a.StartDate <= endDate))));

            if (group == UserGroup.Administrators)
                jobs.AddRange(query.AsEnumerable().Where(item => CheckParent(item.Fund, userId)));

            if (group == UserGroup.Operators)
                jobs.AddRange(query.Where(item => item.AccountManagerJobs.Any(a => a.AccountManagerID == userId)));

            var list =
                jobs.Select(a => new { a.ID, Status = JobStatus((JobWorkflowStatus)a.WorkflowStatusID, a.StartDate, a.CompleteDate) })
                    .ToList();

            var model = (
                            from a in list
                            orderby a.Status
                            group a by a.Status
                                into g
                                select new JobGraphModel
                                {
                                    Count = g.Count(),
                                    Status = g.Key.GetDescription()
                                }
                        ).ToList();

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        #endregion 

        #endregion

        #region JobImportedData [CRUD]

        public string AddUpdateJobsImportedData(string parameters)
        {
            var model = (JobsImportedDataModel)ModelDeserializer.DeserializeFromXml<JobsImportedDataModel>(parameters);
            if (model != null)
                return AddUpdateJobsImportedData(model);
            return NullModelReference();
        }

        private string AddUpdateJobsImportedData(JobsImportedDataModel model)
        {
            AddJobsImportedData(model);

            var job = _repository.GetById(model.JobId);
            var importFilesFolderPath = job.GetFolderPath() + ImportFilesFolderName + "/";
            var fileUploadResponse = new DocumentComponent().UploadFiles(new FileModel() { FolderPath = importFilesFolderPath, Files = new List<Contracts.ViewModels.FileInfo>() { model.UploadFileInfo } });
            if (fileUploadResponse.Status == ResponseStatus.Success)
            {
                _uow.Commit();
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Success, "Success");
            }
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, fileUploadResponse.ResponseMessage);
        }

        private void AddJobsImportedData(JobsImportedDataModel model)
        {
            var jobsImportedData = new JobImportedData
            {
                Filename = model.UploadFileInfo.FileName,
                CreatedBy = model.CreatedBy,
                CreatedOn = model.CreatedOn,
                JobId = model.JobId,
                TemplateId = model.TemplateId,
                XmlData = model.XMLData,
                ControlType = WorkpaperContentType.Import
            };
            var repositoryTd = _uow.Repository<JobImportedData>();
            repositoryTd.Add(jobsImportedData);
        }

        public string GetJobFiles(string parameters)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(parameters);
            var models = GetJobFiles(model.Value);
            return ModelSerializer.GenerateReponseXml<JobImportedDataViewModel>(models, ResponseStatus.Success,
                "Successful");
        }

        private List<JobImportedDataViewModel> GetJobFiles(int jobID)
        {
            var job = _repository.GetById(jobID);
            var models = job.JobsImportedData.Select(j => new JobImportedDataViewModel
            {
                FileName = j.Template.Name,
                Id = j.ID,
                ControlType = j.ControlType
            }).ToList();

            return models;
        }

        public string GetJobImportData(string parameters)
        {
            var model =
                (DictionaryModel<int, List<int>>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, List<int>>>(parameters);
            return GetJobImportData(model.Key, model.Value);
        }

        private string GetJobImportData(int jobId, List<int> templateIds)
        {
            var template =
                _uow.Repository<JobImportedData>()
                    .GetAll()
                    .FirstOrDefault(p => p.JobId == jobId && templateIds.Contains(p.TemplateId));

            if (template != null)
            {
                var model = new JobImportedDataViewModel
                {
                    TemplateId = template.TemplateId,
                    XmlData = template.XmlData,
                    ControlType = WorkpaperContentType.Import
                };
                return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
            }
            return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure, "Failure");
        }

        #endregion JobImportedData [CRUD]

        #region Work Log

        public string GetWorkLogAccountManagers(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            return GetWorkLogAccountManagers(model.Key, model.Value);
        }

        private string GetWorkLogAccountManagers(int jobId, int userId)
        {
            var user = _uow.Repository<Users>().GetById(userId);

            var job = _repository.GetById(jobId);
            var model = new JobWorkLogAccountManagerModel
            {
                JobID = job.ID,
                Title = job.Title,
                Description = job.Description,
                AccountManagers = (UserGroup) user.GroupID == UserGroup.Administrators
                    ? (job.AccountManagerJobs.Any(a => a.StatusID == Status.Active)
                        ? job.AccountManagerJobs.Select(
                            a =>
                                new
                                {
                                    a.AccountManagerID,
                                    Name = a.AccountManager.FirstName + " " + a.AccountManager.LastName
                                })
                            .Distinct()
                            .Select(
                                a =>
                                new DictionaryModel<int, string>
                                {
                                    Key = a.AccountManagerID,
                                        Value = a.Name
                                }).ToList()
                        : null)
                    : (
                        (job.AccountManagerJobs.Any(a => a.StatusID == Status.Active && a.AccountManagerID == user.ID)
                            ? job.AccountManagerJobs.Where(a => a.AccountManagerID == user.ID).Select(
                                a =>
                                    new DictionaryModel<int, string>
                                    {
                                        Key = a.AccountManagerID,
                                        Value = a.AccountManager.FirstName + " " + a.AccountManager.LastName
                                    }).ToList()
                            : null)
                        )
            };

            return ModelSerializer.GenerateReponseXml(model, ResponseStatus.Success, "Successful");
        }

        public string GetWorkLogs(string xml)
        {
            var model = (DictionaryModel<int, int>)ModelDeserializer.DeserializeFromXml<DictionaryModel<int, int>>(xml);
            return GetWorkLogs(model.Key, model.Value);
        }

        private string GetWorkLogs(int jobId, int userId)
        {
            var query = _uow.Repository<JobWorkLogs>().GetAll();

            if (userId != 0)
                query = query.Where(a => a.AccountManagerID == userId);
            if (jobId != 0)
                query = query.Where(a => a.JobID == jobId);

            var logs = query.Select(a => new JobWorkLogModel
            {
                ID = a.ID,
                AccountManagerID = a.AccountManagerID,
                AccountManager = a.AccountManager.FirstName + " " + a.AccountManager.LastName,
                Job = a.Job.Title,
                JobID = a.JobID,
                Description = a.Description,
                End = a.End,
                Start = a.Start
            }).ToList();

            return ModelSerializer.GenerateReponseXml(logs, ResponseStatus.Success, "Successfull");
        }

        public string AddWorkLog(string xml)
        {
            var model = (JobWorkLogModel)ModelDeserializer.DeserializeFromXml<JobWorkLogModel>(xml);
            return AddWorkLog(model);
        }

        private string AddWorkLog(JobWorkLogModel model)
        {
            var workLog = new JobWorkLogs
            {
                AccountManagerID = model.AccountManagerID,
                CreatedBy = model.CreatedBy,
                CreatedOn = model.CreatedOn,
                Description = model.Description,
                End = model.End,
                JobID = model.JobID,
                ModifiedBy = model.CreatedBy,
                ModifiedOn = model.CreatedOn,
                Start = model.Start
            };

            _uow.Repository<JobWorkLogs>().Add(workLog);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = workLog.ID }, ResponseStatus.Success,
                "Successful");
        }

        public string UpdateWorkLog(string xml)
        {
            var model = (JobWorkLogModel)ModelDeserializer.DeserializeFromXml<JobWorkLogModel>(xml);
            return UpdateWorkLog(model);
        }

        private string UpdateWorkLog(JobWorkLogModel model)
        {
            var workLog = _uow.Repository<JobWorkLogs>().GetById(model.ID);

            workLog.AccountManagerID = model.AccountManagerID;
            workLog.Description = model.Description;
            workLog.End = model.End;
            workLog.ModifiedBy = model.ModifiedBy;
            workLog.ModifiedOn = model.ModifiedOn;
            workLog.Start = model.Start;

            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = workLog.ID }, ResponseStatus.Success,
                "Successful");
        }

        public string DeleteWorkLog(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return DeleteWorkLog(model.Value);
        }

        private string DeleteWorkLog(int id)
        {
            var workLog = _uow.Repository<JobWorkLogs>().GetById(id);
            _uow.Repository<JobWorkLogs>().Delete(workLog);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(new GenericModel<bool> { Value = true }, ResponseStatus.Success,
                "Successful");
        }

        #endregion Work Log

        #region Lists

        public string GetJobFlowStatusList(string xml)
        {
            return GetJobFlowStatusList();
        }

        public string GetJobFlowStatusList()
        {
            var statusList = _uow.Repository<WorkflowStatus>().GetAll().Select(p => new JobFlowStaus { value = p.ID, text = p.Name }).ToList();
            return ModelSerializer.GenerateReponseXml<JobFlowStaus>(statusList, ResponseStatus.Success, "Successful");
        }

        #endregion Lists
    }
}