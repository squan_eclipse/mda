﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.Components
{
    public class AddressComponent : Component
    {
        private readonly UnitOfWork _uow = new UnitOfWork();
        
        public string GetCountries(string xml)
        {
            return GetCountries();
        }
        
        public string GetCountries()
        {
            var countries =
                _uow.Repository<Countries>()
                    .GetAll()
                    .OrderBy(a => a.Name)
                    .Select(a => new CountryModel {ID = a.ID, Name = a.Name, Default = a.Default})
                    .ToList();
            return ModelSerializer.GenerateReponseXml<CountryModel>(countries, ResponseStatus.Success, "Successful");
        }
        
        public string GetStates(string xml)
        {
            var model = (GenericModel<int>)ModelDeserializer.DeserializeFromXml<GenericModel<int>>(xml);
            return model != null ? GetStates(model.Value) : NullModelReference();
        }

        public string GetStates(int countryId)
        {
            var states =
                _uow.Repository<States>()
                    .GetAll()
                    .Where(a => a.CountryID == countryId)
                    .OrderBy(a => a.Name)
                    .Select(a => new DictionaryModel<int, string> {Key = a.ID, Value = a.Name})
                    .ToList();
            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(states, ResponseStatus.Success, "Successful");
        }
        
        public string GetAddressTypes(string xml)
        {
            return GetAddressTypes();
        }
        
        public string GetAddressTypes()
        {
            var types = _uow.Repository<AddressTypes>().GetAll().Where(a => a.StatusID == Status.Active)
                                                .Select(a => new DictionaryModel<int, string> { Key = a.ID, Value = a.Description }).ToList();

            return ModelSerializer.GenerateReponseXml<DictionaryModel<int, string>>(types, ResponseStatus.Success, "Successful");
        }

        public string AddCountry(string xml)
        {
            var model = (DictionaryModel<string, string>)ModelDeserializer.DeserializeFromXml<DictionaryModel<string, string>>(xml);
            return AddCountry(model.Key, model.Value);
        }

        private string AddCountry(string name, string code)
        {
            var repository = _uow.Repository<Countries>();
            if (repository.Any(a => a.Name == name))
            {
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure,
                    "Country with the same name already exists");
            }

            var country = new Countries {Code = !string.IsNullOrEmpty(code) ? code : null, Name = name, Default = false};
            repository.Add(country);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> {Value = country.ID}, ResponseStatus.Success,
                "Successful");
        }

        public string AddState(string xml)
        {
            var model =
                (DictionaryModel<int, string>) ModelDeserializer.DeserializeFromXml<DictionaryModel<int, string>>(xml);
            return AddState(model.Key, model.Value);
        }

        private string AddState(int countryId, string name)
        {
            var repository = _uow.Repository<States>();
            if (repository.Any(a => a.CountryID == countryId && a.Name == name))
            {
                return ModelSerializer.GenerateReponseXml(null, ResponseStatus.Failure,
                    "State with the same name already exists in this country");
            }

            var state = new States {CountryID = countryId, Name = name};
            repository.Add(state);
            _uow.Commit();

            return ModelSerializer.GenerateReponseXml(new GenericModel<int> { Value = state.ID }, ResponseStatus.Success,
                "Successful");
        }
    }
}
