﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.Common.Encryption;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Service.Business.Components;
using System;
using System.ServiceModel;

namespace DBASystem.SMSF.Service.Business
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.PerCall)]
    public class SMSFService : ISMSFService
    {
        public string Execute(string value)
        {
            var startTime = DateTime.Now;
            var requestModel = new RequestModel();

            try
            {
                var decryptedValue = AesEncryption.Decrypt(value,true);
                requestModel = ModelDeserializer.ParseRequestXML(decryptedValue);
                
                if (requestModel == null) return null;

                startTime = Tracer.Start(requestModel.ModelXml);

                var result = new ComponentFactory().InvokeAction(requestModel.Option, requestModel.ModelXml);

                Tracer.End(startTime);

                result = AesEncryption.Encrypt(result.ToString(),true);

                Tracer.End(startTime);

                return result.ToString();
            }

            catch (Exception ex)
            {
                Utility.Logger.Error(Utility.GetExceptionDetails(ex));

                if (ex.Source != "EntityFramework")
                    new NotificationComponent().AddError(ex, requestModel);

                Tracer.End(startTime);

                return AesEncryption.Encrypt(ModelSerializer.GenerateErrorResponseXml(ex),true);
            }
        }
    }
}

