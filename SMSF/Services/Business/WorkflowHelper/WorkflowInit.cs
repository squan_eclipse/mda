﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using DBASystem.SMSF.Service.Business.WorkflowHelper.Common;
using DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess;
using OptimaJet.Workflow.Core.Builder;
using OptimaJet.Workflow.Core.Bus;
using OptimaJet.Workflow.Core.Model;
using OptimaJet.Workflow.Core.Parser;
using OptimaJet.Workflow.Core.Persistence;
using OptimaJet.Workflow.Core.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using PersistenceProvider = OptimaJet.Workflow.DbPersistence;
using TransitionClassifier = OptimaJet.Workflow.Core.Model.TransitionClassifier;
using WorkflowRuntime = OptimaJet.Workflow.Core.Runtime.WorkflowRuntime;

namespace DBASystem.SMSF.Service.Business.WorkflowHelper.Workflow
{
    public class WorkflowInit
    {
        #region Private Declaration

        private string _connectionString;
        private Guid processIdentifier;
        private Workflows workflowType;
        private WorkflowRuntime runtime;
        private string workflowName;
        private string processName;

        #endregion Private Declaration

        #region Private Properties

        private string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectionString))
                {
                    _connectionString = Utility.GetConnectionString;
                }
                return _connectionString;
            }
        }

        #endregion Private Properties

        #region Private Methods

        private WorkflowInit(Workflows type, Guid processId, string workflow)
        {
            workflowType = type;
            workflowName = workflow;
            processIdentifier = processId;
            processName = workflow;
            CreateRuntime();
            CreateInstance();
        }

        private WorkflowInit(Workflows type, Guid processId)
        {
            workflowType = type;
            processIdentifier = processId;
            CreateRuntime();
        }

        private IWorkflowBuilder GetDefaultBuilder()
        {
            var generator = new PersistenceProvider.DbXmlWorkflowGenerator(ConnectionString);

            var builder = new WorkflowBuilder<XElement>(generator,
                new XmlWorkflowParser(),
                new PersistenceProvider.DbSchemePersistenceProvider(ConnectionString)
                ).WithDefaultCache();
            return builder;
        }

        private WorkflowBuilder<XElement> GetWorkflowBuilderXElement()
        {
            return GetDefaultBuilder() as WorkflowBuilder<XElement>;
        }

        private void CreateRuntime()
        {
            IWorkflowRoleProvider roleProvider = null;
            IWorkflowRuleProvider ruleProvider = null;
            switch (workflowType)
            {
                case Workflows.JobProcess:
                    roleProvider = new JobProcessRoles();
                    ruleProvider = new JobProcessRules();
                    break;
            }

            var builder = GetDefaultBuilder();
            var nullBus = new NullBus();
            runtime = new WorkflowRuntime(Guid.NewGuid())
                .WithBuilder(builder)
                .WithBus(new NullBus())
                .WithRoleProvider(roleProvider)
                .WithRuleProvider(ruleProvider)
                .WithRuntimePersistance(new PersistenceProvider.RuntimePersistence(ConnectionString))
                .WithPersistenceProvider(new PersistenceProvider.DbPersistenceProvider(ConnectionString))
                .SwitchAutoUpdateSchemeBeforeGetAvailableCommandsOn()
                .Start();

            runtime.ProcessStatusChanged += new EventHandler<ProcessStatusChangedEventArgs>(_runtime_ProcessStatusChanged);
            runtime.OnSchemaWasChanged += runtime_OnSchemaWasChanged;
        }

        void runtime_OnSchemaWasChanged(object sender, SchemaWasChangedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        private string CreateInstance()
        {
            if (runtime.IsProcessExists(processIdentifier))
            {
                return "Process already exist.";
            }
            var uow = new UnitOfWork();
            using (var sync = new WorkflowSync(runtime, processIdentifier))
            {
                runtime.CreateInstance(processName, processIdentifier);

                sync.StatrtWaitingFor(new List<ProcessStatus> { ProcessStatus.Idled, ProcessStatus.Initialized });

                sync.Wait(new TimeSpan(0, 0, 10));
            }
            runtime.PreExecuteFromInitialActivity(processIdentifier);

            var transactionRepository = uow.WorkflowRepository<WorkflowTransactionHistory>();
            WriteTransactionHistory(processIdentifier, transactionRepository);
            uow.Commit();
            return runtime.GetCurrentStateName(processIdentifier);
        }

        private void _runtime_ProcessStatusChanged(object sender, ProcessStatusChangedEventArgs e)
        {
            if (e.NewStatus != ProcessStatus.Idled && e.NewStatus != ProcessStatus.Finalized)
                return;

            if (string.IsNullOrEmpty(e.ProcessName))
                return;
            //WriteTransactionHistory(e.ProcessId);
            runtime.PreExecuteFromCurrentActivity(e.ProcessId);
        }

        private void UpdateCurrentActivityHistory(Guid processId, Guid userId, IWorkflowRepository<WorkflowTransactionHistory> repsitory, string commandName)
        {
            var activityName = runtime.GetCurrentActivityName(processId);
            var processInstance = runtime.Builder.GetProcessInstance(processId);
            var currentActivity = processInstance.ProcessScheme.FindActivity(activityName);
            var previousTransitions = processInstance.ProcessScheme.GetPossibleTransitionsForActivity(currentActivity).Where(t => t.Trigger.Type == TriggerType.Command);

            if (previousTransitions != null)
            {
                var currentTransaction = previousTransitions.Where(t => (t.Classifier == TransitionClassifier.Direct || t.Classifier == TransitionClassifier.Reverse) && t.Trigger.Command.Name == commandName).FirstOrDefault();
                if (currentTransaction != null)
                {
                    var historyItem = repsitory.GetAll().FirstOrDefault(p => p.ProcessIdentity == processId && p.InitialState == currentTransaction.From.Name && p.DestinationState == currentTransaction.To.Name);
                    historyItem.TransitionTime = DateTime.Now;
                    historyItem.UserID = userId;
                }
            }
        }

        private void WriteTransactionHistory(Guid processId, IWorkflowRepository<WorkflowTransactionHistory> repsitory)
        {
            var activityName = runtime.GetCurrentActivityName(processId);

            var processInstance = runtime.Builder.GetProcessInstance(processId);
            var currentActivity = processInstance.ProcessScheme.FindActivity(activityName);
            var response = currentActivity.State;
            var nextTransitions = processInstance.ProcessScheme.GetPossibleTransitionsForActivity(currentActivity).Where(t => t.Classifier == TransitionClassifier.Direct || t.Classifier == TransitionClassifier.Reverse);
            var emptyTransactions = repsitory.GetAll(p => !p.TransitionTime.HasValue && p.ProcessIdentity == processId && p.Command != "Auto");
            foreach (var transaction in emptyTransactions)
            {
                repsitory.Delete(transaction);
            }
            Dictionary<TransitionDefinition, IEnumerable<Guid>> ActorsList = new Dictionary<TransitionDefinition, IEnumerable<Guid>>();

            // Incase the last transaction of the flow is auto it will be written in history
            // Otherwise only command transactions will be written in history
            if (nextTransitions.Any(p => p.Trigger.Type == TriggerType.Command))
            {
                nextTransitions = nextTransitions.Where(p => p.Trigger.Type == TriggerType.Command);
            }
            foreach (TransitionDefinition transition in nextTransitions)
            {
                //GetActors method is private in original source of engine we have made it public to utilize as per our need
                //Definition in Workflowruntime.cs
                WorkflowTransactionHistory historyItem = new WorkflowTransactionHistory();
                historyItem.Id = Guid.NewGuid();
                historyItem.InitialState = transition.From.Name;
                historyItem.DestinationState = transition.To.Name;
                historyItem.Command = transition.Trigger.Type == TriggerType.Auto ? "Auto" : transition.Trigger.Command.Name;
                historyItem.ProcessIdentity = processId;
                historyItem.CreatedOn = DateTime.Now;
                historyItem.WorkflowId = (int)workflowType;
                IEnumerable<Guid> actors = runtime.GetActors(processId, transition);
                foreach (var userId in actors)
                {
                    WorkflowTransitionUsers user = new WorkflowTransitionUsers { Id = Guid.NewGuid(), TransactionId = historyItem.Id, UserID = userId };
                    historyItem.TransactionUsers.Add(user);
                }

                repsitory.Add(historyItem);
            }
        }

        private void WriteTransactionHistory(Guid processId)
        {
            UnitOfWork uow = new UnitOfWork();
            var workflowRepository = uow.WorkflowRepository<WorkflowTransactionHistory>();
            WriteTransactionHistory(processId, workflowRepository);
            uow.Commit();
        }

        #region Helper Methods

        private Guid GetUserIdentifier(int userId)
        {
            UnitOfWork uow = new UnitOfWork();
            var userRepository = uow.Repository<Users>();
            var userIdentifier = userRepository.GetAll().First(p => p.ID == userId).Identifier;
            return (Guid)userIdentifier;
        }

        #endregion Helper Methods

        #endregion Private Methods

        #region Public Properties

        public string Status
        {
            get { return runtime.GetCurrentStateName(processIdentifier); }
        }

        public string CurrentActivity
        {
            get { return runtime.GetCurrentActivityName(processIdentifier); }
        }

        #endregion Public Properties

        #region Public Methods

        public static WorkflowInit GetWorkFlowInstance(Workflows workflowType, Guid processId, string workFlow)
        {
            return new WorkflowInit(workflowType, processId, workFlow);
        }

        public static WorkflowInit GetWorkFlowInstance(Workflows workflowType, Guid processId)
        {
            return new WorkflowInit(workflowType, processId);
        }

        public string ExecuteCommand(string commandName, int userId, Guid processId)
        {
            return ExecuteCommand(commandName, GetUserIdentifier(userId), processId);
        }

        public string ExecuteCommand(string commandName, Guid userId, Guid processId)
        {
            var uow = new UnitOfWork();

            var repository = uow.WorkflowRepository<WorkflowTransactionHistory>();

            var commands = runtime.GetAvailableCommands(processId, userId);
            var command = commands.FirstOrDefault(c => c.CommandName.Equals(commandName, StringComparison.CurrentCultureIgnoreCase));
            if (command == null)
                return "No commands found to execute";

            UpdateCurrentActivityHistory(processId, userId, repository, commandName);

            runtime.ExecuteCommand(processId, userId, userId, command);

            WriteTransactionHistory(processId, repository);

            uow.Commit();
            return runtime.GetCurrentStateName(processId);
        }

        public Dictionary<string, string> GetAvailableCommands(int userId)
        {
            var result = GetAvailableCommands(GetUserIdentifier(userId));
            return result;
        }

        public Dictionary<string, string> GetAvailableCommands(Guid userId)
        {
            var result = new Dictionary<string, string>();
            var commands = runtime.GetAvailableCommands(processIdentifier, userId);
            foreach (var workflowCommand in commands)
            {
                if (!result.ContainsKey(workflowCommand.CommandName))
                    result.Add(workflowCommand.CommandName, workflowCommand.CommandName);
            }
            return result;
        }

        public bool ProcessFlowExists
        {
            get
            {
                return runtime.IsProcessExists(processIdentifier);
            }
        }

        private Dictionary<string, string> GetAvailableStateToSet(Guid processId)
        {
            var result = new Dictionary<string, string>();
            var states = runtime.GetAvailableStateToSet(processId);
            foreach (var state in states)
            {
                if (!result.ContainsKey(state.Name))
                    result.Add(state.Name, state.VisibleName);
            }
            return result;
        }

        #endregion Public Methods
    }
}