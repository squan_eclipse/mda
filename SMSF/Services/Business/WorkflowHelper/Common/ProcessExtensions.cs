﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Service.Business.WorkflowHelper.Common
{
    public static class WorkFlowProcessExtensions
    {
        private static bool RemoveWhere<T>(this IList<T> query, Func<T, bool> predicate)
        {
            var x = query.Where(predicate).FirstOrDefault();
            return query.Remove(x);
        }

        public static bool RemoveActivity(this Process process, string activityName)
        {
            if (!process.Transitions.Any(p => p.From == activityName || p.To == activityName))
                return process.Activities.RemoveWhere(r => r.Name == activityName);
            else
                throw new Exception("Activity is associated with a transaction");
        }

        public static bool RemoveTransaction(this Process process, string transactionName)
        {
            return process.Transitions.RemoveWhere(r => r.Name == transactionName);
        }

        public static string ToXml(this Process process)
        {
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(process.GetType());
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            using (var stream = new StringWriter())
            using (var writer = XmlWriter.Create(stream, settings))
            {
                serializer.Serialize(writer, process, emptyNamepsaces);
                return stream.ToString();
            }
        }

        public static byte[] ToByteArray(this Process process)
        {
            return Encoding.ASCII.GetBytes(process.ToXml());
        }

        public static void Load(this Process process, System.Data.Linq.Binary binaryData)
        {
            var workflowXml = Encoding.ASCII.GetString(binaryData.ToArray());
            process.Load(workflowXml);
        }

        public static void Load(this Process process, string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Process));
            StringReader xmlReader = new StringReader(xml);
            var p = (Process)serializer.Deserialize(xmlReader);
            process.Actions = p.Actions;
            process.Activities = p.Activities;
            process.Commands = p.Commands;
            process.Name = p.Name;
            process.Timers = p.Timers;
            process.Transitions = p.Transitions;
            process.Parameters = p.Parameters;
            process.Actors = p.Actors;
        }

        public static ProcessActivityDesigner GetActivityDesign(this Process process, int activityId)
        {
            var selectedActivity = process.Activities.FirstOrDefault(p => p.ID == activityId);
            if (selectedActivity != null)
            {
                return selectedActivity.Designer;
            }
            return new ProcessActivityDesigner() {X = -1, Y = -1};
        }
    }
}