﻿namespace DBASystem.SMSF.Service.Business.WorkflowHelper.Common
{
    internal class ActivityDetails
    {
        public string ActivityName { get; set; }

        public string RedirectionUrl { get; set; }
    }
}