﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.ViewModels;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.WorkflowHelper.Common
{
    public class WorkflowSelector
    {
        #region Entity Flow Selection

        //depricated
        //public string GetFlowForEntity(int id)
        //{
        //    return GetFlow(Workflows.CreateEntities.ToString(), id, EntityGenericType.Entity);
        //}

        //public string GetFlowForFund(int id)
        //{
        //    return GetFlow(Workflows.CreateFund.ToString(), id, EntityGenericType.Entity);
        //}

        //public string GetFlowForClient(int id)
        //{
        //    return GetFlow(Workflows.CreateClient.ToString(), id, EntityGenericType.Entity);
        //}

        //public string GetFlowForGroup(int id)
        //{
        //    return GetFlow(Workflows.CreateGroup.ToString(), id, EntityGenericType.Entity);
        //}

        private string GetFlow(string flowName, int id, EntityGenericType type)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();
                var entityRepository = uow.Repository<Entities>();
                var entity = entityRepository.GetAll().First(p => p.ID == id && p.TypeID == (int)type);
                if (entity != null)
                {
                    var workflowMap = entity.WorkflowMap.Where(m => m.WorkFlowType.Name == flowName).FirstOrDefault();
                    if (workflowMap == null)
                    {
                        var workflowMapRepository = uow.Repository<WorkflowMap>();
                        workflowMap = workflowMapRepository.GetAll().FirstOrDefault(p => p.WorkFlowType.Name == flowName && !p.EntityId.HasValue);
                    }
                    return workflowMap == null ? "" : workflowMap.WorkFlowId.ToString();
                }
                return "";
            }
            catch
            {
                return "";
            }
        }

        #endregion Entity Flow Selection

        #region Job Flow Selection

        public WorkflowSelectModel GetFlowforJobProcessByFund(int fundId)
        {
            var uow = new UnitOfWork();
            var entityRepository = uow.Repository<Entities>();
            var fund = entityRepository.GetAll().First(p => p.ID == fundId);

            if (fund != null)
            {
                return GetFlow((int)Workflows.JobProcess, fund.ParentEntity.ParentEntity.ID);
            }
            return new WorkflowSelectModel();
        }

        public WorkflowSelectModel GetFlowforJobProcessByEntity(int entityId)
        {
            return GetFlow((int)Workflows.JobProcess, entityId);
        }

        #endregion Job Flow Selection

        private WorkflowSelectModel GetFlow(int flowId, int entityId)
        {
            var selectModel = new WorkflowSelectModel();
            selectModel.IsDefault = true;
            var uow = new UnitOfWork();
            var workflowMapRepository = uow.Repository<WorkflowMap>();
            var workflowList =
                workflowMapRepository.GetAll().Where(p => p.WorkFlowType.ID == flowId && p.IsActive && (p.EntityId == entityId || !p.EntityId.HasValue)).ToList();
            var workflowMap = workflowList.Where(p => p.EntityId == entityId).FirstOrDefault();
            if (workflowMap == null)
            {
                workflowMap = workflowList.Where(p => !p.EntityId.HasValue).FirstOrDefault();
                selectModel.IsDefault = true;
                selectModel.Name = workflowMap.WorkFlowId.ToString();
                selectModel.Id = workflowMap.ID;
            }
            else
            {
                selectModel.IsDefault = false;
                selectModel.Name = workflowMap.WorkFlowId.ToString();
                selectModel.Id = workflowMap.ID;
            }

            return selectModel;
        }
    }
}