﻿using DBASystem.SMSF.Contracts.Common;
using OptimaJet.Workflow.Designer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.WorkflowHelper.Common
{
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot("", Namespace = "")]
    public class Process
    {
        public string SchemaName { get; set; }

        public object Timers { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Actor", IsNullable = false)]
        public List<ProcessActor> Actors { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Parameter", IsNullable = false)]
        public List<ProcessParameters> Parameters { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Command", IsNullable = false)]
        public List<ProcessCommand> Commands { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Action", IsNullable = false)]
        public List<ProcessAction> Actions { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Activity", IsNullable = false)]
        public List<ProcessActivity> Activities { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("Transition", IsNullable = false)]
        public List<ProcessTransition> Transitions { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name { get; set; }

        public string ID { get; set; }

        public List<string> Validatate()
        {
            List<string> ErrorList = new List<string>();
            var invalidTransitions = Transitions.Where(p => !Activities.Any(m => m.Name == p.From || m.Name == p.To));
            foreach (var transition in invalidTransitions)
            {
                ErrorList.Add("<b>" + transition.Name + @" Contains invalid actvities </b>\r\n");
            }
            invalidTransitions = Transitions.Where(p => !p.Restrictions.Any(m => Actors.Any(actors => actors.Name == m.NameRef)) && (TransitionCommandType)Enum.Parse(typeof(TransitionCommandType), p.Triggers.Trigger.Type, true) == TransitionCommandType.Command);
            foreach (var transition in invalidTransitions)
            {
                ErrorList.Add("<b>" + transition.Name + @" Contains invalid actvities </b>\r\n");
            }

            var defaultSettings = new GeneratorSettings()
            {
                ReadOnly = false,
            };

            var designGenerator = Generator.Create(defaultSettings);
            designGenerator.LoadScheme(this.ToXml());
            if (!designGenerator.Validate())
                ErrorList.Add(designGenerator.ValidationErrors);

            return ErrorList;
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessActor
    {
        public ProcessActorIsInRole IsInRole { get; set; }

        public ProcessActorRule Rule { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessActorIsInRole
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Id { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessActorRule
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string RuleName { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessParameters
    {
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Purpose { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessCommand
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ParameterRef", IsNullable = false)]
        public List<CommandParameterRef> InputParameters { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class CommandParameterRef
    {
        private string nameField;

        private string nameRefField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NameRef
        {
            get
            {
                return this.nameRefField;
            }
            set
            {
                this.nameRefField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessAction
    {
        public ProcessActionExecuteMethod ExecuteMethod { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessActionExecuteMethod
    {
        [System.Xml.Serialization.XmlArrayItemAttribute("ParameterRef", IsNullable = false)]
        public List<ParameterRef> InputParameters { get; set; }

        [System.Xml.Serialization.XmlArrayItemAttribute("ParameterRef", IsNullable = false)]
        public List<ParameterRef> OutputParameters { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string MethodName { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ParameterRef
    {
        private int orderField;

        private string nameRefField;

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Order
        {
            get
            {
                return this.orderField;
            }
            set
            {
                this.orderField = value;
            }
        }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NameRef
        {
            get
            {
                return this.nameRefField;
            }
            set
            {
                this.nameRefField = value;
            }
        }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessActivity
    {
        private List<ActivityActionRef> implementation;
        private List<ActivityActionRef> preExecutionImplementation;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ActionRef", IsNullable = false)]
        public List<ActivityActionRef> Implementation
        {
            get { return implementation; }
            set { implementation = value; }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ActionRef", IsNullable = false)]
        public List<ActivityActionRef> PreExecutionImplementation
        {
            get { return preExecutionImplementation; }
            set { preExecutionImplementation = value; }
        }

        /// <remarks/>
        public ProcessActivityDesigner Designer { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string State { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string IsInitial { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string IsFinal { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string IsForSetState { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string IsAutoSchemeUpdate { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int ID { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ActivityActionRef
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Order { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NameRef { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public class ProcessActivityDesigner
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int X { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Y { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ProcessTransition
    {
        /// <remarks/>
        public List<Restriction> Restrictions { get; set; }

        /// <remarks/>
        public Triggers Triggers { get; set; }

        /// <remarks/>
        public Conditions Conditions { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string From { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string To { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Classifier { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string FromActivityID { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ToActivityID { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class Restriction
    { /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NameRef { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ActorID { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string TypeID { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }
    }

    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class Triggers
    {
        /// <remarks/>
        public TriggersDef Trigger { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class TriggersDef
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NameRef { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class Conditions
    {
        /// <remarks/>
        public ConditionDef Condition { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class ConditionDef
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string NameRef { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ID { get; set; }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string ResultOnPreExecution { get; set; }
    }
}