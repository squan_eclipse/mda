﻿using System;
using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess
{
    public class Actions
    {
        public void InitiateJob(Guid processId)
        {
        }

        public void DataImport(Guid processId)
        {
        }

        public void IncomeStatementReview(Guid processId)
        {
        }

        public void BalanceSheetReview(Guid processId)
        {
        }

        public void SisComplianceAudit(Guid processId)
        {
        }
        public void ProcessReview(Guid processId)
        {
        }
        public void ProcessSignOff(Guid processId)
        {
        }

        public void ClientResponse(Guid processId)
        {

        }

        public void JobComplete(Guid processId)
        {
            var uow = new UnitOfWork();
            var job = uow.Repository<Jobs>().GetAll().Single(p => p.Identifier == processId);
            job.WorkflowStatusID = 2;// Complete
            job.CompleteDate = DateTime.Now;
            uow.Commit();


        }
    }
}
