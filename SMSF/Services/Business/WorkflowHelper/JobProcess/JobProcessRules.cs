﻿using DBASystem.SMSF.Data;
using DBASystem.SMSF.Data.Models;
using OptimaJet.Workflow.Core.Runtime;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess
{
    public class JobProcessRules : IWorkflowRuleProvider
    {
        private Dictionary<string, Func<Guid, Guid, bool>> _funcs = new Dictionary<string, Func<Guid, Guid, bool>>();

        private Dictionary<string, Func<Guid, IEnumerable<Guid>>> _getIdentitiesFuncs = new Dictionary<string, Func<Guid, IEnumerable<Guid>>>();
      
        public JobProcessRules()
        {
            _funcs.Add("IsAuditAdmin", IsAuditAdmin);
            _funcs.Add("IsBalanceSheetAuditor", IsBalanceSheetAuditor);
            _funcs.Add("IsIncomeStatementAuditor", IsIncomeStatementAuditor);
            _funcs.Add("IsSISAuditor", IsSISAuditor);
            _funcs.Add("IsReviewManager", IsReviewManager);
            _funcs.Add("IsSignOffManager", IsSignOffManager);
            _funcs.Add("IsClientUser", IsClientUser);

            _getIdentitiesFuncs.Add("IsAuditAdmin", GetAuditAdmin);
            _getIdentitiesFuncs.Add("IsBalanceSheetAuditor", GetBalanceSheetAuditor);
            _getIdentitiesFuncs.Add("IsIncomeStatementAuditor", GetIncomeStatementAuditor);
            _getIdentitiesFuncs.Add("IsSISAuditor", GetSISAuditor);
            _getIdentitiesFuncs.Add("IsReviewManager", GeReviewManager);
            _getIdentitiesFuncs.Add("IsSignOffManager", SignOffManager);
            _getIdentitiesFuncs.Add("IsClientUser", GetJobClient);
            
        }

        #region Check Rule

        private bool IsAuditAdmin(Guid processId, Guid identityId)
        {
            return CheckUserRight(processId, identityId, new Guid("C81231AB-1FFE-433A-A2B1-066476CCE6EE"));
        }

        private bool IsBalanceSheetAuditor(Guid processId, Guid identityId)
        {
            return CheckUserRight(processId, identityId, new Guid("577406D2-46F9-47ED-B6FF-0C3C6BF99BBF"));
        }
        private bool IsIncomeStatementAuditor(Guid processId, Guid identityId)
        {
            return CheckUserRight(processId, identityId, new Guid("73106F7D-BA51-4C20-B608-4ADF0DF2DD34"));
        }
        private bool IsSISAuditor(Guid processId, Guid identityId)
        {
            return CheckUserRight(processId, identityId, new Guid("F53F1E30-62CA-4A43-8E75-BAE26D616BB6"));
        }
        private bool IsReviewManager(Guid processId, Guid identityId)
        {
            return CheckUserRight(processId, identityId, new Guid("BA0A02DB-9AD8-406F-96B6-2AB87C8023FF"));
        }
        private bool IsSignOffManager(Guid processId, Guid identityId)
        {
            return CheckUserRight(processId, identityId, new Guid("69BE8D68-6727-408B-B187-3B6B89945F4F"));
        }
        private bool IsClientUser(Guid processId, Guid identityId)
        {
            var uow = new UnitOfWork();

            var clientUser = (from a in uow.Repository<Jobs>().GetAll()
                      join c in uow.Repository<Users>().GetAll() on a.Fund.ParentID equals c.AccountManager.EntityID
                      where c.Identifier == identityId
                      select a).Any();
            return clientUser;
           
        }
        
        public bool CheckRule(Guid processId, Guid identityId, string ruleName)
        {
            return _funcs.ContainsKey(ruleName) && _funcs[ruleName].Invoke(processId, identityId);
        }

        public bool CheckRule(Guid processId, Guid identityId, string ruleName, IDictionary<string, string> parameters)
        {
            throw new NotImplementedException();
        }

        #endregion Check Rule

        #region GetIdentites
                
              
                


                //new AccountManagerJobRoles { ID = 6, Name = "Sign Off Manager",  IsActive = true, Identifier = new Guid("69BE8D68-6727-408B-B187-3B6B89945F4F")},
                //new AccountManagerJobRoles { ID = 7, Name = "Client User",  IsActive = false, Identifier = new Guid("D2BE6DFA-27E1-4DAE-A79A-496FE899EE95")},
        private IEnumerable<Guid> GetAuditAdmin(Guid processId)
        {
            return new List<Guid>() { new Guid("C81231AB-1FFE-433A-A2B1-066476CCE6EE") };
        }

        private IEnumerable<Guid> GetBalanceSheetAuditor(Guid processId)
        {
            return new List<Guid>() { new Guid("577406D2-46F9-47ED-B6FF-0C3C6BF99BBF") };
        }

        private IEnumerable<Guid> GetSISAuditor(Guid processId)
        {
            return new List<Guid>() { new Guid("F53F1E30-62CA-4A43-8E75-BAE26D616BB6") };
        }
        private IEnumerable<Guid> GeReviewManager(Guid processId)
        {
            return new List<Guid>() { new Guid("BA0A02DB-9AD8-406F-96B6-2AB87C8023FF") };
        }
        private IEnumerable<Guid> SignOffManager(Guid processId)
        {
            return new List<Guid>() { new Guid("69BE8D68-6727-408B-B187-3B6B89945F4F") };
        }
        private IEnumerable<Guid> GetIncomeStatementAuditor(Guid processId)
        {
            return new List<Guid>() { new Guid("73106F7D-BA51-4C20-B608-4ADF0DF2DD34") };
        }

        private IEnumerable<Guid> GetJobClient(Guid processId)
        {

            return new List<Guid>() { new Guid("D2BE6DFA-27E1-4DAE-A79A-496FE899EE95") };
        }

        public IEnumerable<Guid> GetIdentitiesForRule(Guid processId, string ruleName)
        {
            return !_getIdentitiesFuncs.ContainsKey(ruleName) ? new List<Guid> { } : _getIdentitiesFuncs[ruleName].Invoke(processId);
        }

        public IEnumerable<Guid> GetIdentitiesForRule(Guid processId, string ruleName, IDictionary<string, string> parameters)
        {
            throw new NotImplementedException();
        }

        #endregion GetIdentites

        #region Helper Methods

        private bool CheckUserRight(Guid processId, Guid userIdentifier, Guid roleIdentifier)
        {
            UnitOfWork uow = new UnitOfWork();
            var repository = uow.Repository<Users>();

            var user = repository.GetAll().FirstOrDefault(p => p.Identifier == userIdentifier && p.AccountManager.Jobs.Any(q => q.JobRole.Identifier == roleIdentifier && q.Job.Identifier == processId));

            if (user != null)
            {
                uow.Dispose();
                return true;
            }
            else
            {
                uow.Dispose();
                return false;
            }
        }

        #endregion Helper Methods
    }
}
