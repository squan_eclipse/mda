﻿using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections.Generic;

namespace DBASystem.SMSF.Service.Business.PDFHelper
{
    public class PdfFormEditor
    {
        private PdfReader _pdfReader;
        public PdfFormEditor(byte[] pdfFormTemplate)
        {
            _pdfReader = new PdfReader(pdfFormTemplate);
        }

        public List<PdfFormField> GetRequiredFields()
        {
            return _pdfReader.AcroFields.Fields.Select(de => new PdfFormField(de.Key.ToString(), _pdfReader.AcroFields.GetField(de.Key.ToString()), "")).ToList();
        }

        public byte[] GetUpdatedForm(List<PdfFormField> fieldList)
        {
            var streamTemplate = new MemoryStream();
            using (var stamper = new PdfStamper(_pdfReader, streamTemplate))
            {
                AcroFields pdfFormFields = stamper.AcroFields;
                foreach (var field in fieldList)
                {
                    pdfFormFields.SetField(field.Name, field.Value);
                }
                stamper.FormFlattening = true;
                stamper.Writer.CloseStream = false;
            }
            return streamTemplate.ToArray();
        }

    }
}
