﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBASystem.SMSF.Service.Business.PDFHelper
{
    public class PdfFormField
    {
        public PdfFormField(string name, string genericName, string value = "")
        {
            Name = name;
            GenericName = genericName;
            Value = value;
        }

        public string Name { get; private set; }

        public string GenericName { get; private set; }

        public string Value { get; set; }
    }
}
