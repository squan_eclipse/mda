﻿using DBASystem.SMSF.Contracts.ViewModels;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace DBASystem.SMSF.Service.Sharepoint
{
    public class DocumentManagement
    {
        #region Declaration

        private ClientContext context;
        private const string SharedDocuments = "/Shared Documents/";

        #endregion Declaration

        #region Constructor

        public DocumentManagement(string sharepointSiteUrl, string userName, string password)
        {
            context = new ClientContext(sharepointSiteUrl);
            context.Credentials = new NetworkCredential(userName, password);
        }

        #endregion Constructor

        #region Folder

        /// <summary>
        /// Add new folder in sharepoint document library
        /// </summary>
        /// <param name="name">New folder name</param>
        /// <param name="parentFolder">Parent folder path</param>
        public void AddFolder(string name, string parentFolder)
        {
           AddFolder(name,parentFolder,new List<string>());
        }

        /// <summary>
        /// Add new folder in sharepoint document library with Sub Folders
        /// </summary>
        /// <param name="name">New folder name</param>
        /// <param name="parentFolder">Parent folder path</param>
        /// <param name="subFolderList">List of Sub Folder To be added</param>
        public void AddFolder(string name, string parentFolder,List<string> subFolderList)
        {
            var web = context.Web;
            var folder = web.GetFolderByServerRelativeUrl(SharedDocuments + parentFolder);
            var newfolder = folder.Folders.Add(name);
            foreach (var subFoldder in subFolderList)
            {
                newfolder.Folders.Add(subFoldder);
            }
            //context.ExecuteQuery();
        }



        /// <summary>
        /// Rename sharepoint document library folder
        /// </summary>
        /// <param name="name">Folder old name</param>
        /// <param name="newName">Folder new name</param>
        /// <param name="path">Folder path without name</param>
        public void RenameFolder(string name, string newName, string path)
        {
            //var web = context.Web;
            //var folders = web.GetFolderByServerRelativeUrl(SharedDocuments + path + (path != "" ? @"/" : "") + name).ListItemAllFields;
            //context.Load(folders);

            //context.ExecuteQuery();

            //folders["FileLeafRef"] = newName;
            //folders["Title"] = newName;
            //folders.Update();

            //context.Load(folders);
            //context.ExecuteQuery();
        }

        /// <summary>
        /// Rename folder with 'Deleted' keyword
        /// </summary>
        /// <param name="name">Folder name</param>
        /// <param name="path">Folder path</param>
        public void DeleteFolder(string name, string path)
        {
            RenameFolder(name, name + " - Deleted", path);
        }

        /// <summary>
        /// Get all folders and files from the sharepoint document library folder
        /// </summary>
        /// <param name="path">Folder path withtout name</param>
        /// <returns></returns>
        public List<DocumentModel> GetFolderAllItems(string path)
        {
            var web = context.Web;

            var f = web.GetFolderByServerRelativeUrl(SharedDocuments + path);
            //context.Load(f, a => a.Files, a => a.Folders);
            //context.ExecuteQuery();

            var models = new List<DocumentModel>();

            if (f.Folders != null && f.Folders.Count > 0)
            {
                models.AddRange(
                                f.Folders.AsEnumerable().Where(a => a.Name != "Forms")
                                                        .Select(a => new DocumentModel
                                                        {
                                                            Name = a.Name,
                                                            Type = DocumentType.Folder,
                                                            ServerRelativeUrl = a.ServerRelativeUrl.Replace(SharedDocuments, ""),
                                                            //ModifiedOn = (DateTime)a.ListItemAllFields["Modified"]
                                                        })
                                                        .OrderBy(a => a.Name)
                                );
            }

            if (f.Files != null && f.Files.Count > 0)
            {
                models.AddRange(
                                f.Files.AsEnumerable().Select(a => new DocumentModel
                                                        {
                                                            Name = a.Name,
                                                            Type = DocumentType.File,
                                                            ModifiedOn = a.TimeLastModified,
                                                            ServerRelativeUrl = a.ServerRelativeUrl.Replace(SharedDocuments, ""),
                                                        })
                                                        .OrderBy(a => a.Name)
                                );
            }

            return models;
        }

        /// <summary>
        ///  Moves All folder from One destination to another with files.
        /// </summary>
        /// <param name="sourceFolder"></param>
        /// <param name="destinatioFolder"></param>
        public void MoveFolder(string sourceFolder, string destinatioFolder)
        {
            string sourceFolderName = "";
            sourceFolder = SharedDocuments.Replace("/", "") + "/" + sourceFolder;
            destinatioFolder = SharedDocuments.Replace("/", "") + "/" + destinatioFolder;

            CamlQuery query = new CamlQuery();

            query.ViewXml = @"<View Scope='RecursiveAll'>
                                <Query>
                                       <Where>
                                               <Or>
                                                    <Contains>
                                                            <FieldRef Name='FileRef'/>
                                                            <Value Type='Text'>" + sourceFolder + "/" + "</Value>" +
                                                @" </Contains>
                                                   <Eq>
                                                            <FieldRef Name='FileRef'/>
                                                            <Value Type='Text'>" + sourceFolder + "</Value>" +
                                                   @"</Eq>
                                              </Or>
                                        </Where>
                                        <QueryOptions></QueryOptions>
                                        <RowLimit>100</RowLimit>
                                </Query>
                            </View>";

            ListItemCollection docs = context.Web.Lists.GetByTitle("Documents").GetItems(query);
            context.Load(docs, f => f.Include(
                                                     p => p.File.Title,
                                                     p => p.File.Name,
                                                     p => p.File.ServerRelativeUrl,
                                                     p => p.Folder.Files,
                                                     p => p.Folder,
                                                     p => p.Folder.ListItemAllFields,
                                                     p => p.Folder.Name,
                                                     p => p.Folder.Folders,
                                                     p => p.FileSystemObjectType));
            context.ExecuteQuery();

            Folder destinationFolder = context.Web.GetFolderByServerRelativeUrl(destinatioFolder);
            bool deleteFolder = false;

            foreach (ListItem item in docs)
            {
                if (item.FileSystemObjectType.ToString() == "Folder")
                {
                    if ("/" + sourceFolder == item.Folder.ServerRelativeUrl)
                    {
                        sourceFolderName = item.Folder.Name;
                        destinationFolder.Folders.Add(sourceFolderName);
                        deleteFolder = true;
                    }
                    else
                    {
                        string newFolderPath = sourceFolderName + item.Folder.ServerRelativeUrl.Replace("/" + sourceFolder, "");
                        Console.WriteLine("Create Folder : " + newFolderPath);
                        destinationFolder.Folders.Add(destinatioFolder + "/" + newFolderPath);
                    }
                }
                else if (item.FileSystemObjectType.ToString() == "File")
                {
                    string copyFilePath = item.File.ServerRelativeUrl.Replace("/" + sourceFolder, destinatioFolder + "/" + sourceFolderName);
                    item.File.MoveTo(copyFilePath, MoveOperations.Overwrite);
                }
            }
            if (deleteFolder)
                context.Web.GetFolderByServerRelativeUrl(sourceFolder).DeleteObject();
            context.ExecuteQuery();
        }

        #endregion Folder

        #region File

        /// <summary>
        ///  Upload File(s) to share piont
        /// </summary>
        /// <param name="uploadFiles"></param>
        public void UploadFiles(FileModel uploadFiles)
        {
            // Load Exiting files in target folder
            FileCollection files = context.Web.GetFolderByServerRelativeUrl(SharedDocuments + uploadFiles.FolderPath).Files;
            context.Load(files);
            context.ExecuteQuery();

            //List of files which has to be checked out and updated
            var updateFiles = (from existingFile in files.AsEnumerable()
                               join newFileUpload in uploadFiles.Files on existingFile.Name equals newFileUpload.FileName

                               select new { existingFile, newFileUpload }
                              ).ToList();
            
            // Checking Out Existing Filess
            foreach (var oldFile in updateFiles)
            {
                if (oldFile.existingFile.CheckOutType == CheckOutType.None)
                    oldFile.existingFile.CheckOut();
                
            }
            context.ExecuteQuery();

            //Uploading new and Existing Files
            foreach (var file in uploadFiles.Files)
            {
                Microsoft.SharePoint.Client.File.SaveBinaryDirect(context, SharedDocuments + uploadFiles.FolderPath + file.FileName, new MemoryStream(Convert.FromBase64String(file.FileData)), true);
            }
            // Checking in Files
            foreach (var oldFile in updateFiles)
            {
                oldFile.existingFile.CheckIn("Updated", CheckinType.OverwriteCheckIn);
            }

            context.ExecuteQuery();
        }

        /// <summary>
        /// Download file from sharepoint document library
        /// </summary>
        /// <param name="rootPath"></param>
        /// <param name="fileName"></param>
        /// <returns>File memory stream</returns>
        public byte[] DownloadFile(string rootPath, string fileName)
        {
            var web = context.Web;
            var file = web.GetFileByServerRelativeUrl(SharedDocuments + rootPath + fileName).OpenBinaryStream();
            context.ExecuteQuery();
            byte[] fileByteArray;
            using (BinaryReader br = new BinaryReader(file.Value))
            {
                fileByteArray = br.ReadBytes((int)file.Value.Length);
            }
            return fileByteArray;
        }

        /// <summary>
        /// Get list of files from the sharepoint document library folder
        /// </summary>
        /// <param name="folderUrl">Folder url</param>
        /// <returns>List of DocumentModel</returns>
        public List<DocumentModel> GetFiles(string folderUrl)
        {
            var web = context.Web;
            var files = web.GetFolderByServerRelativeUrl(SharedDocuments + folderUrl).Files;
            context.Load(files);

            context.ExecuteQuery();

            var models = files.AsEnumerable()
                              .Where(a => !a.Name.Contains("- Deleted"))
                              .Select(a => new DocumentModel
                              {
                                  Name = a.Name,
                                  Type = DocumentType.File,
                                  ModifiedOn = a.TimeLastModified,
                                  ServerRelativeUrl = a.ServerRelativeUrl.Replace(SharedDocuments, ""),
                              })
                              .OrderBy(a => a.Name)
                              .ToList();

            return models;
        }

        /// <summary>
        /// Delete file from sharepoint document library
        /// </summary>
        /// <param name="name">File name</param>
        /// <param name="filePath">File's folder url</param>
        public void DeleteFile(string name, string filePath)
        {
            RenameFile(name, " - Deleted", filePath);
        }

        /// <summary>
        /// Rename file in sharepoint document library
        /// </summary>
        /// <param name="name">Old file name</param>
        /// <param name="newName">New file name</param>
        /// <param name="filePath">File's folder url</param>
        public void RenameFile(string name, string newName, string filePath)
        {
            var web = context.Web;
            var file = web.GetFileByServerRelativeUrl(SharedDocuments + filePath + name).ListItemAllFields;
            context.Load(file);

            newName = SetFileName(name, newName);
            file["FileLeafRef"] = newName;
            file["Title"] = newName;
            file.Update();

            context.Load(file);
            context.ExecuteQuery();
        }

        public void CopyFiles(string sourceUrl, string destinationUrl)
        {
            var web = context.Web;
            var files = web.GetFolderByServerRelativeUrl(SharedDocuments + sourceUrl).Files;
            
            context.Load(files);
            context.ExecuteQuery();

            foreach (var file in files)
            {
                var fileInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, file.ServerRelativeUrl);

                Microsoft.SharePoint.Client.File.SaveBinaryDirect(context, SharedDocuments + destinationUrl + file.Name,
                    fileInfo.Stream, true);
            }

            context.ExecuteQuery();
        }

        #endregion File

        #region Private Methods/Functions

        /// <summary>
        /// Set file name for delete method by remove the extension and then set the name
        /// </summary>
        /// <param name="oldName">Complete file name with extension</param>
        /// <param name="newName">Value to add in name</param>
        /// <returns>Complete file name with addition of new name and extension</returns>
        public string SetFileName(string oldName, string newName)
        {
            var name = oldName.Split('.');
            var extension = name[name.Length - 1];
            var returnValue = "";

            for (int i = 0; i < name.Length - 1; i++)
            {
                returnValue = name[i];
            }

            return returnValue + newName + "." + extension;
        }

        #endregion Private Methods/Functions
    }
}