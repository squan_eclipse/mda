﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Service.Business;

namespace DBASystem.SMSF.Services
{
    public partial class ServiceSync : ServiceBase
    {
        ServiceHost m_host = null;
 
        public ServiceSync()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                StartService();

                Utility.Logger.Info("The service is started");
            }
            catch (Exception ex)
            {
                Utility.Logger.Error(Utility.GetExceptionDetails(ex));
            }


        }

        protected override void OnStop()
        {
            try
            {
                StopService();

                Utility.Logger.Info("The service is stopped");
            }
            catch (Exception ex)
            {
                Utility.Logger.Error(Utility.GetExceptionDetails(ex));
            }

            
        }

        protected override void OnPause()
        {
            try
            {
                base.OnPause();

                StopService();

                Utility.Logger.Info("The service is paused");
            }
            catch (Exception ex)
            {
                Utility.Logger.Error(Utility.GetExceptionDetails(ex));
            }
        }

        protected override void OnContinue()
        {
            try
            {
                
                base.OnContinue();

                StartService();

                Utility.Logger.Info("The service is continued");
            }
            catch (Exception ex)
            {
                Utility.Logger.Error(Utility.GetExceptionDetails(ex));
            }
        }

        private void StartService()
        {

            m_host = new ServiceHost(typeof(SMSFService));
            m_host.Open();

        }  

        private void StopService()
        {

            if (m_host != null)
            {
                m_host.Close();
            }
        }
    }
}
