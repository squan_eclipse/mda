﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace HostConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri httpUrl = new Uri("net.tcp://localhost:9000/DBASystem/Business");

            var host = new ServiceHost(typeof(DBASystem.SMSF.Service.Business.SMSFService), httpUrl);
            NetTcpBinding tcpBinding =  new NetTcpBinding();
            tcpBinding.MaxReceivedMessageSize = 2000000000;
            tcpBinding.MaxBufferSize = 2000000000;
            
            host.AddServiceEndpoint(typeof(DBASystem.SMSF.Service.Business.ISMSFService),tcpBinding , "");

            var smb = new ServiceMetadataBehavior();

            try
            {
                host.Description.Behaviors.Add(smb);
                host.Open();
                Console.WriteLine("Service is host at " + DateTime.Now.ToString());
                Console.WriteLine("Host is running... Press <Enter> key to stop");
                Console.ReadLine();
            }
            catch
            {
               
            }

          
        }
    }
}
