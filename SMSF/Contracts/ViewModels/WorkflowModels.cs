﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    #region Activity Models

    [XmlRoot("Value")]
    public class ActivityCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public List<ActivityDispositionCreateModel> Dispositions { get; set; }

        public int EntityID { get; set; }
    }

    public class ActivityDispositionCreateModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public Status Status { get; set; }
    }

    #endregion Activity Models

    #region Workflow Template

    [XmlRoot("Value")]
    public class WorkflowTemplateModel : IModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int WorkflowTypeId { get; set; }

        public int UserId { get; set; }

        public int EntityId { get; set; }

        public bool IsSystemDefault { get; set; }

        public bool IsDefault { get; set; }

        public List<WorkflowTransitionsModel> Transitions { get; set; }

        public List<WorkflowTransitionsModel> AvailableTransitions { get; set; }

        public List<DictionaryModel<int, string>> Actors { get; set; }

        public List<DictionaryModel<int, string>> Activities { get; set; }

        public List<DictionaryModel<int, string>> RestrictionTypes { get; set; }

        public List<TransitionCommandModel> AvailableCommands { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkflowTransitionsModel : IModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int FromActivityID { get; set; }

        public string FromActivity { get; set; }

        public int ToActivityID { get; set; }

        public string ToActivity { get; set; }

        public TransitionCommandModel Command { get; set; }

        public TransitionCommandType CommandType { get; set; }

        public List<WorkflowRestrictionsModel> Restrictions { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkflowRestrictionsModel : IModel
    {
        public int ID { get; set; }

        public int TypeID { get; set; }

        public int ActorID { get; set; }

        public string Actor { get; set; }

        public string Type { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkflowSelectModel : IModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsDefault { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkflowCreateModel : IModel
    {
        public int WorkflowTypeId { get; set; }

        public int FirmId { get; set; }

        public string TemplateName { get; set; }

        public int TemplateId { get; set; }
    }

    #endregion Workflow Template

    #region Workflow Pending Process

    [XmlRoot("Value")]
    public class WorkflowPendingProcessModel : IModel
    {
        public Guid ProcessId { get; set; }

        public string Name { get; set; }

        public string WorkflowName { get; set; }

        public int WorkflowTypeId { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkflowPendingProcessDetailModel : IModel
    {
        public List<DictionaryModel<string, string>> Commands { get; set; }

        public string ViewUrl { get; set; }

        public Guid Identifier { get; set; }

        public string CurrentActivity { get; set; }

        public int WorkflowTypeID { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkflowPendingProcessViewModel : IModel
    {
        public int TypeId { get; set; }

        public Guid ProcessId { get; set; }

        public Guid userId { get; set; }

        public string Command { get; set; }

        public int ProcessIdentity { get; set; }
    }

    #endregion Workflow Pending Process

    #region Workflow Graph

    [XmlRoot("Value")]
    public class WorkflowGraphModel : IModel
    {
        public string HtmlString { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkflowJobDetail : IModel
    {
        public JobAssignee Assignee { get; set; }

        public Guid JobIdentifier { get; set; }

        public string WorkflowState { get; set; }
    }

    [XmlRoot("Value")]
    public class JobAssignee : IModel
    {
        public string Name { get; set; }

        public Guid Role { get; set; }
    }

    [XmlRoot("Value")]
    public class TransitionCommandModel : IModel
    {
        public string Name { get; set; }

        public int ID { get; set; }
    }

    [XmlRoot("Value")]
    public class GraphDeserializeModel :IModel
    {
        private string _value;

        public string Key { get; set; }

        public Object Value
        {
            get { return _value; }
            set
            {
                try
                {
                    _value = value.ToString();
                }
                catch (Exception)
                {
                    _value = "";
                }
            }
        }
    }

    [XmlRoot("Value")]
    public class GraphDesignerModel : IModel
    {

        public GraphDesignerModel()
        {
            ActivityPostions = new List<ActivityGraphPostion>();
        }

        public int WorkflowTemplateId { get; set; }
        public int DesignerX { get; set; }

        public int DesignerY { get; set; }

        public int DesignerScale { get; set; }

        public List<ActivityGraphPostion> ActivityPostions { get; set; }
    }
    [XmlRoot("Value")]
    public class ActivityGraphPostion : IModel
    {
        public string Name { get; set; }

        public int LocationX { get; set; }

        public int LocationY { get; set; }
    }

    #endregion Workflow Graph
}