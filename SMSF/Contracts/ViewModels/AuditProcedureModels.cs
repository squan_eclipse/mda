﻿using System;
using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class AuditProcedureTabViewModel : IModel
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public int TabOrder { get; set; }
        public int? EntityId { get; set; }
        public WorkpaperContentType? ContentType { get; set; }
        public bool IsActive { get; set; }
        public List<AuditProcedureTabContentViewModel> AuditProcedureTabContent { get; set; }
        public bool IsViewable { get; set; }
    }

    [XmlRoot("Value")]
    public class AuditProcedureTabContentViewModel : IModel
    {
        public int Id { get; set; }
        public int AuditProcedureTabId { get; set; }
        public string Caption { get; set; }
        public WorkpaperContentType ContentType { get; set; }
        public ControlType? ControlType { get; set; }
        public int? TemplateId { get; set; }
        public int Order { get; set; }
        public int EntityId { get; set; }
        public bool IsActive { get; set; }
        public virtual AuditProcedureTabViewModel AuditProcedureTab { get; set; }
        public virtual List<AuditProcedureViewModel> AuditProcedure { get; set; }
        public bool IsViewable { get; set; }

    }

    [XmlRoot("Value")]
    public class AuditProcedureViewModel : IModel
    {
        public int Id { get; set; }
        public int AuditProcedureTabContentId { get; set; }
        public int? ParentId { get; set; }
        public string Title { get; set; }
        public bool Attachments { get; set; }
        public bool Oml { get; set; }
        public bool Comments { get; set; }
        public ControlType? ControlType { get; set; }
        public ControlType? AnswerControlType { get; set; }
        public bool Checkbox { get; set; }
        public AuditProcedureViewModel Parent { get; set; }
        public AuditProcedureTabContentViewModel AuditProcedureTabContent { get; set; }
        public List<AuditProcedureOptionViewModel> AuditProcedureOption { get; set; }
        public List<AuditProcedureOptionDetailViewModel> AuditProcedureOptionDetail { get; set; }

        public List<AuditProcedureOptionDetailViewModel> AuditProcedureOptionDetailSelected { get; set; }

        public string[] OptionDetailIds { get; set; }

        public bool IsOptionUpdated { get; set; }

        public List<AuditProcedureColumnViewModel> AuditProcedureColumn { get; set; }
        public AuditProcedureAnswerViewModel Answer { get; set; }
        public List<AuditProcedureViewModel> Childs { get; set; }
    }

    [XmlRoot("Value")]
    public class AuditProcedureOptionViewModel : IModel
    {
        public int Id { get; set; }
        public int AuditProcedureId { get; set; }
        public int AuditProcedureOptionDetailId { get; set; }

    }


    [XmlRoot("Value")]
    public class AuditProcedureOptionDetailViewModel : IModel
    {
        public int Id { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }

    [XmlRoot("Value")]
    public class AuditProcedureColumnViewModel : IModel
    {
        public int Id { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }

    }

    [XmlRoot("Value")]
    public class AuditProcedureAnswerViewModel : IModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool? IsChecked { get; set; }
        public int AuditProcedureId { get; set; }
        public int JobId { get; set; }
    }

    [XmlRoot("Value")]
    public class AuditProcedureOmlViewModel : IModel
    {
        public int Id { get; set; }
        public string CustomOption { get; set; }
        public bool NonFundamental { get; set; }
        public int JobId { get; set; }
        public string Item { get; set; }
        public string StaffComments { get; set; }
        public string Initials { get; set; }
        public bool IsCleared { get; set; }
        public bool IsManagerApproved { get; set; }
        public int AuditProcedureId { get; set; }
        public ControlType? ControlType { get; set; }

    }

    [XmlRoot("Value")]
    public class AuditProcedureCommentViewModel : BaseModel, IModel
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public int JobId { get; set; }
        public int AuditProcedureId { get; set; }
        public string AccountManagerName { get; set; }
        public int? ParentID { get; set; }
        public string AuditProcedure { get; set; }
        public List<AuditProcedureCommentViewModel> ChildComments { get; set; }
    }

    [XmlRoot("Value")]
    public class AuditProcedureAttachmentViewModel : IModel
    {
        public AuditProcedureAttachmentViewModel()
        {
            Files = new List<AttachmentInfo>();
        }

        public int Id { get; set; }
        public List<AttachmentInfo> Files { get; set; }
        public int JobId { get; set; }
        public int AuditProcedureId { get; set; }

    }

    [XmlRoot("Value")]
    public class AttachmentInfo
    {
        public AttachmentInfo()
        {
            Document = new DocumentModel();
        }

        public int Id { get; set; }
        public DocumentModel Document { get; set; }
        public string FileData { get; set; }
    }

    [XmlRoot("Value")]
    public class TestSummaryViewModel : IModel
    {
        public int Id { get; set; }
        //public int JobId { get; set; }
        public string TestName { get; set; }
        public string Description { get; set; }
        //public bool? IsCompleted { get; set; }
        public List<TestSummaryCommentViewModel> TestSummaryComment { get; set; }
        public TestSummaryAnswerViewModel Answer { get; set; }

    }

    [XmlRoot("Value")]
    public class TestSummaryCommentViewModel : IModel
    {
        public int Id { get; set; }
        public string TestComments { get; set; }
        public int TestSummaryId { get; set; }
        public TestSummaryViewModel TestSummary { get; set; }
    }

    [XmlRoot("Value")]
    public class TestSummaryAnswerViewModel : IModel
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int TestSummaryId { get; set; }
        public bool? IsCompleted { get; set; }

    }

    [XmlRoot("Value")]
    public class ManageWorkPaperModel : IModel
    {
        public int AccountId { get; set; }

        public int JobId { get; set; }

        public int AuditProcTabId { get; set; }

        public int AuditProcTabContentId { get; set; }

        public WorkpaperContentType? ContentType { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkpaperTemplateModel : BaseModel, IModel
    {
        public int AccountId { get; set; }

        public int ID { get; set; }

        public string Name { get; set; }

        public string FileName { get; set; }
    }

    
    [XmlType(TypeName = "AuditProcedureTab")]
    [XmlRoot(ElementName = "AuditProcedureTab", IsNullable = false)]    
    public class AuditProcedureTabTemplates
    {
        public int ID { get; set; }
        public string Caption { get; set; }
        public int TabOrder { get; set; }
        public int? EntityId { get; set; }
        public int ContentType { get; set; }
        public bool IsActive { get; set; }
        public bool IsViewable { get; set; }

        [XmlArrayItemAttribute("AuditProcedureTabContent", IsNullable = false)]
        public List<AuditProcedureTabContentTemplates> AuditProcedureTabContents { get; set; }
    }

    /// <remarks/>
    [XmlTypeAttribute(AnonymousType = true)]
    public class AuditProcedureTabContentTemplates
    {
        public int ID { get; set; }
        public int AuditProcedureTabId { get; set; }
        public string Caption { get; set; }
        public int ContentType { get; set; }
        public int? ControlType { get; set; }
        public int? TemplateId { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public int? EntityId { get; set; }
        public bool IsViewable { get; set; }

        [XmlArrayItemAttribute("AuditProcedure", IsNullable = false)]
        public List<AuditProcedureTemplates> AuditProcedures { get; set; }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public class AuditProcedureTemplates
    {
        public int ID { get; set; }
        public int AuditProcedureTabContentId { get; set; }
        public int? ParentId { get; set; }
        public string Title { get; set; }
        public bool Attachments { get; set; }
        public bool Oml { get; set; }
        public bool Comments { get; set; }
        public int? ControlType { get; set; }
        public int? AnswerControlType { get; set; }
        public bool Checkbox { get; set; }
        public int StatusID { get; set; }

        [XmlArrayItemAttribute("AuditProcedureOption", IsNullable = false)]
        public List<AuditProcedureOptionTemplates> AuditProcedureOption { get; set; }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public class AuditProcedureOptionTemplates
    {
        public int ID { get; set; }
        public int AuditProcedureId { get; set; }
        public int AuditProcedureOptionDetailId { get; set; }
        public virtual AuditProcedureOptionDetailTemplates AuditProcedureOptionDetail { get; set; }
    }

    [XmlTypeAttribute(AnonymousType = true)]
    public class AuditProcedureOptionDetailTemplates
    {
        public int ID { get; set; }
        public string Field { get; set; }
        public string Value { get; set; }
    }
}
