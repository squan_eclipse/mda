﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    // Dont change the case of the properties, they are case sensetive for kendo tree view.
    [XmlRoot("Value")]
    public class ClientTreeViewItemModel : IModel
    {
        public int id { get; set; }

        public string text { get; set; }

        public string imageUrl { get; set; }

        public bool expanded { get; set; }

        public bool hasChildren { get; set; }

        public string css { get; set; }

        public string link { get; set; }

        public List<ClientTreeViewItemModel> items { get; set; }

        public bool selected { get; set; }

        public bool enable { get; set; }
    }
    
}
 