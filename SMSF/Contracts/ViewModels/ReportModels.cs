﻿using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class AuditReportGenerateModel : IModel
    {
        public string TemplateName { get; set; }

        public string FolderPath { get; set; }

        public int JobId { get; set; }
    }
}