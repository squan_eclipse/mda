﻿using System.ComponentModel;
using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    public static class JobUtitlies
    {
        public static string FormattedID(int fundId, int jobId)
        {
            return fundId.ToString().PadLeft(5, '0') + jobId.ToString().PadLeft(3, '0');
        }
    }

    [XmlRoot("Value")]
    public class JobCreateModel : BaseModel, IModel
    {
        public JobCreateModel()
        {
            SelectedJobManager = new List<AccountManagerSelect>();
        }

        public string FormattedID
        {
            get { return JobUtitlies.FormattedID(FundId, ID); }
        }
        public string EncryptedID
        {
            get { return Utility.EncryptQueryString(ID.ToString()); }
        }
        public int ID { get; set; }
        public int EntityType { get; set; }

        [Required(ErrorMessage = "The Fund field is required")]
        public int FundId { get; set; }

        [Required(ErrorMessage = "The Entity field is required")]
        public int EntityID { get; set; }

        [Required(ErrorMessage = "The Job Title field is required")]
        [StringLength(50, ErrorMessage = "Job Title should be max 50 characters")]
        [RegularExpression(@"^[0-9a-zA-Z''-'\s]*", ErrorMessage = "Special characters are not allowed")]
        public string Title { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "The Start Date field is required")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public DateTime? CompleteDate { get; set; }

        [Range(1, Int32.MaxValue)]
        [RegularExpression(@"^\d+$", ErrorMessage = "Invalid Working Hours")]
        public int? WorkingHours { get; set; }

        [StringLength(100, ErrorMessage = "Datalink should be max 100 characters")]
        public string DataLink { get; set; }

        public List<AccountManagerSelect> SelectedJobManager { get; set; }
        public List<SelectListItem> ListClients { get; set; }
        public List<SelectListItem> ListFunds { get; set; }
        public string ClientName { get; set; }
        public string FundName { get; set; }
        public string Path { get; set; }
        public string Firm { get; set; }
        public List<NotesViewModel> Notes { get; set; }

        [Range(1, Int32.MaxValue, ErrorMessage = "The Estimated Hours must be between 1 and 2147483647.")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Invalid Estimated Hours")]
        public int? EstimatedHours { get; set; }

        public List<AuditProcedureCommentViewModel> AuditComments { get; set; }
        public double? ActualWorkingHours { get; set; }
    }

    [XmlRoot("Value")]
    public class JobDetailModel : BaseModel, IModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string ClientName { get; set; }

        public string FundName { get; set; }

        public int EntityId { get; set; }

        public List<JobManager> jobManagers { get; set; }

        public string ActiveAccountManagers
        {
            get
            {
                var activeManagers = jobManagers.Where(p => p.status == Status.Active).Select(p => p.name).ToArray();
                return string.Join(",", activeManagers);
            }
        }

        public string AllAccountManagers
        {
            get
            {
                var allManagers = jobManagers.Select(p => p.name + (p.status == Status.Active ? "" : "(" + p.status + ")")).ToArray();
                return string.Join(",", allManagers);
            }
        }

        public string description { get; set; }

        public DateTime startDate { get; set; }

        public DateTime? completeDate { get; set; }

        public int? workingHours { get; set; }

        public string dataLink { get; set; }

        public int? estimatedHours { get; set; }
    }

    [XmlRoot("Value")]
    public class AccountManagerSelect : IModel
    {
        public int ID { get; set; }

        public string AccountManagerName { get; set; }

        public bool Selected { get; set; }

        public int SelectedRoleID { get; set; }

        public string SelectedRoleName { get; set; }

        public string Email { get; set; }
    }

    [XmlRoot("Value")]
    public class AccountManagerSelectView : IModel
    {
        public List<AccountManagerSelect> SelectedManagers { get; set; }

        public List<AccountManager> Managers { get; set; }

        public List<JobRolesModel> Roles { get; set; }
    }

    [XmlRoot("Value")]
    public class AccountManager : IModel
    {
        public int Id { get; set; }

        public string AccountManagerName { get; set; }

        public string Email { get; set; }
    }

    [XmlRoot("Value")]
    public class JobRolesModel : IModel
    {
        public int ID { get; set; }

        public string RoleName { get; set; }
    }

    [XmlRoot("Value")]
    public class JobManager
    {
        public string name { get; set; }

        public Status status { get; set; }
    }

    [XmlRoot("Value")]
    public class JobsImportedDataModel : BaseModel, IModel
    {
        public int ID { set; get; }

        public int JobId { get; set; }

        public int TemplateId { get; set; }

        [Required]
        public string XMLData { get; set; }

        public virtual TemplateModel Template { get; set; }

        public List<SelectListItem> ListTemplates { get; set; }

        public FileInfo UploadFileInfo { get; set; }
    }

    [XmlRoot("Value")]
    public class JobsModel : IModel
    {
        public int ID { get; set; }
        public string EncryptedID
        {
            get { return Utility.EncryptQueryString(ID.ToString()); }
        }
        public int FundID { get; set; }
        public string FormattedID
        {
            get { return JobUtitlies.FormattedID(FundID, ID); }
        }
        public string Name { get; set; }
        public string Account { get; set; }
        public string Fund { get; set; }
        public List<string> AccountManagers { get; set; }
        public List<string> Assignee { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? CompletedOn { get; set; }
        public string Status { get; set; }
        public string Estimation { get; set; }
        public string Actual { get; set; }
        public string Path { get; set; }
        public int? EstimatedHours { get; set; }
        public string WorkflowState { get; set; }
    }

    [XmlRoot("Value")]
    public class WorkpaperMenuViewModel : IModel
    {
        public List<AuditProcedureTabViewModel> WorkPapers { get; set; }
    }

    [XmlRoot("Value")]
    public class FundsJobView : IModel
    {
        public string Title { get; set; }

        public string JobFlowStatus { get; set; }
    }

    [XmlRoot("Value")]
    public class JobModel : BaseModel, IModel
    {
        public int ID { get; set; }
        public int FundID { get; set; }
        public string FormattedID
        {
            get { return JobUtitlies.FormattedID(FundID, ID); }
        }
        [Required]
        [StringLength(100)]
        public string Title { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public double? WorkingHours { get; set; }
        [StringLength(100)]
        public string DataLink { get; set; }
        public Status StatusID { get; set; }
        public string Path { get; set; }
        public virtual FirmModels Fund { get; set; }
        public virtual List<JobsImportedDataModel> JobsImportedData { get; set; }
        public virtual List<NotesModel> Notes { get; set; }
        public NotesGenericModel NoteGenericModel { get; set; }
        public string ClientName { get; set; }
        public string AccountManager { get; set; }
        public string Auditor { get; set; }
    }

    [XmlRoot("Value")]
    public class JobFlowStaus :  IModel
    {
        public int value { get; set; }
        public string text { get; set; }

        public bool selected { get; set; }

    }

    [XmlRoot("Value")]
    public class JobScheduledModel : IModel
    {
        public int Count { get; set; }
        public string Status { get; set; }
    }

    [XmlRoot("Value")]
    public class JobGraphModel : IModel
    {
        public int UserID { get; set; }
        public UserGroup? Group { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Count { get; set; }
        public string Status { get; set; }
        public string Date { get; set; }
    }
}