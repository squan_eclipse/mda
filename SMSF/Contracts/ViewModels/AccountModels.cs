﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class OptionModel : IModel
    {
        [Required]
        [Display(Name = "Option Type")]
        public OptionType OptionType { get; set; }

        [Required]
        [Display(Name = "User ID")]
        public int UserID { get; set; }
    }

    [XmlRoot("Value")]
    public class LoginModel : IModel
    {
        [Required]
        [Display(Name = "Login Name")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    [XmlRoot("Value")]
    public class UserModel : IModel
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public Guid AuthCode { get; set; }

        public string Name { get; set; }

        public AssociatedEntites Client { get; set; }

        public AssociatedEntites Entity { get; set; }

        public UserGroup Group { get; set; }

        public EntityTypes EntityType { get; set; }

        public string UserImage { get; set; }
        
        public Guid? UserIdentifier { get; set; }

        public Guid GroupIdentifier { get; set; }

        public SystemSettingsModel SystemSettings { get; set; }
    }

    [XmlRoot("Value")]
    public class UserDetailModel : IModel
    {
        public int ID { get; set; }

        public string Group { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public DateTime? LastLoginTime { get; set; }
    }

    [XmlRoot("Value")]
    public class UserCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Group field is required")]
        public int GroupID { get; set; }

        public string Group { get; set; }

        public List<SelectListItem> ListGroups { get; set; }

        [Required(ErrorMessage = "The Login name field is required")]
        [StringLength(50, ErrorMessage = "Login name must be at least 6 characters long", MinimumLength = 6)]
        public string Login { get; set; }

        [Required(ErrorMessage = "The Password field is required")]
        [DataType(DataType.Password)]
        [StringLength(20, ErrorMessage = "Password must be at least 6 characters long", MinimumLength = 6)]
        public string Password { get; set; }

        [Required(ErrorMessage = "The Confirm Password field is required")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "Password doest not match")]
        public string ConfirmPassword { get; set; }
    }

    [XmlRoot("Value")]
    public class UserGroupModel : IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Group Name field is requred")]
        public string Title { get; set; }

        public string Description { get; set; }

        public Status Status { get; set; }

        public bool IsSystem { get; set; }
    }

    [XmlRoot("Value")]
    public class UserGroupRolesModel : IModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<RoleModel> AvailableRoles { get; set; }

        public List<RoleModel> Roles { get; set; }
    }

    [XmlRoot("Value")]
    public class RoleModel : IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Role Name field is requred")]
        public string Title { get; set; }

        public string Description { get; set; }

        public Status Status { get; set; }

        public bool IsSystem { get; set; }
    }

    [XmlRoot("Value")]
    public class RoleOptionsModel : IModel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int CreateBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public List<ClientTreeViewItemModel> OptionsTree { get; set; }

        public List<int> SelectedOptions { get; set; }
    }

    [XmlRoot("Value")]
    public class ForgotPasswordModel : IModel
    {
        [Required(ErrorMessage = "Email Address is Required")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email address")]
        public string EmailAddress { get; set; }

        public int EntityID { get; set; }

        public string BaseUrl { get; set; }

        [Required(ErrorMessage = "Invalid New Password")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "Password does not match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }

    [XmlRoot("Value")]
    public class AssociatedEntites : IModel
    {
        public int EntityID { get; set; }

        public string EntityName { get; set; }
    }
}