﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class TemplateModel : BaseModel, IModel
    {
        public int ID { set; get; }
        [MaxLength(100)]
        public string Name { get; set; }
        public int CategoryID { get; set; }
        [Required]
        public string TemplateText { get; set; }

        public virtual TemplateCategoriesModel Category { get; set; }
        public virtual List<JobsImportedDataModel> JobsImportedData { get; set; }
    }
    
    [XmlRoot("Value")]
    public class TemplateCategoriesModel : IModel
    {
        public int ID { set; get; }
        [Required]
        [MaxLength(50)]
        public string Name { set; get; }
        public int TemplateProviderID { set; get; }

        public Status Status { set; get; }
    }
    [XmlRoot("Value")]
    public class TemplateProviderModel : IModel
    {
        [Required]
        [MaxLength(50)]
        public string Name { set; get; }
        public Status StatusID { set; get; }
    }
}
