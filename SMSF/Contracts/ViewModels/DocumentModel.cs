﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Contracts.Common.Encryption;
using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    public class DocumentModel
    {
        public string Name { get; set; }

        public DocumentType Type { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string ModifiedBy { get; set; }

        public string ServerRelativeUrl { get; set; }

        public string PublicUrl
        {
            get
            {
                var url = "";
                if (DocumentType == "office")
                {
                    url = System.Web.HttpContext.Current.Request.Url.Host +
                    //"http://www.dbasystem.com.au/SMSF.Web" +
                    "/Sharepoint/ViewFile?file=" +
                    AesEncryption.Encrypt(ServerRelativeUrl + "|" + DateTime.Now.AddMinutes(PublicUrlExipry).ToString());
                    url = url.Replace("+", "[Plus]");
                    return HttpUtility.UrlEncode(url);
                }
                url = AesEncryption.Encrypt(ServerRelativeUrl).ToString();
                url = url.Replace("+", "[Plus]");
                return HttpUtility.UrlEncode(url);
                //return AESEncryption.Encrypt(ServerRelativeUrl).ToString();

            }
        }

        public string DocumentType
        {
            get
            {
                return GetDocumentType(ServerRelativeUrl);
            }
        }

        internal static string GetDocumentType(string filepath)
        {
            string fileType = "other";

            if (!String.IsNullOrEmpty(filepath))
            {
                fileType = Enum.IsDefined(typeof(OfficeFileTypes), System.IO.Path.GetExtension(filepath).Replace(".", "")) ? "office" : "other";
            }
            return fileType;
        }

        public int PublicUrlExipry {get;set;}
        
    }

    public enum DocumentType
    {
        Folder,

        File
    }

    [XmlRoot("Value")]
    public class FileModel : IModel
    {
        public FileModel()
        {
            Files = new List<FileInfo>();
        }

        public string FolderPath { get; set; }

        public List<FileInfo> Files { get; set; }
    }

    [XmlRoot("Value")]
    public class FileInfo
    {
        public string FileName { get; set; }

        public string FileData { get; set; }
    }
}