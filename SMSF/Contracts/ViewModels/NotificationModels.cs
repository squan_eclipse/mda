﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class NotificationGetModel : IModel
    {
        public NotificationTypes? Type { get; set; }

        public int UserID { get; set; }

        public int Days { get; set; }

        public bool IsSystem { get; set; }

        public int Page { get; set; }

        public int Count { get; set; }
    }

    [XmlRoot("Value")]
    public class NotificationModel : IModel
    {
        public List<NotificationViewModel> Data { get; set; }

        public int Count { get; set; }
    }

    [XmlRoot("Value")]
    public class NotificationViewModel : IModel
    {
        public string Message { get; set; }

        public string MessageDetail { get; set; }

        public NotificationTypes Type { get; set; }

        public string EntityType { get; set; }

        public int? ReleventID { get; set; }

        public DateTime CreateDate { get; set; }

        public int ID { get; set; }

        public string EncryptedID
        {
            get { return Utility.EncryptQueryString(ReleventID.ToString()); }
        }
    }

    [XmlRoot("Value")]
    public class ErrorModel :IModel
    {
        public string Message { get; set; }

        public string MessageDetail { get; set; }

        public int UserID { get; set; }

        public int? EntityID { get; set; }
    }

    [XmlRoot("Value")]
    public class NotificationDeleteModel : IModel
    {
        public int? ID { get; set; }
        public int? EntityID { get; set; }
        public int? Days { get; set; }
    }
}
