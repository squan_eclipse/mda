﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot ("Value")]
    public class SearchFilterModel : IModel
    {
        public int EntityID { get; set; }
        public int UserID { get; set; }
        public EntityTypes EntityType { get; set; }
        public string Firm { get; set; }
        public string Name { get; set; }
        public string ABN { get; set; }
        public string AccountManager { get; set; }
        public string PrimaryContact { get; set; }
        public string Account { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }
        public string Fund { get; set; }
        public string Assignee { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsMyJob { get; set; }

        public bool ShowOverdueOnly { get; set; }

    }    
}
