﻿using System.Web;
using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;
using DBASystem.SMSF.Contracts.Common.Encryption;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class FirmModels : IModel
    {
        public int ID { get; set; }

        public string EncryptedID
        {
            get { return Utility.EncryptQueryString(ID.ToString()); }
        }
        public string Name { get; set; }

        public string Firm { get; set; }

        public string Account { get; set; }

        public EntityTypes Type { get; set; }

        public string ABN { get; set; }

        public string PrimaryContact { get; set; }

        public string AccountManager { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string Path { get; set; }

        public int? CategoryID { get; set; }

        public bool HasChild { get; set; }
    }

    [XmlRoot("Value")]
    public class FirmDetailModels : IModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string ParentName { get; set; }

        public EntityTypes Type { get; set; }

        public string ABN { get; set; }

        public string ACN { get; set; }

        public string GST { get; set; }

        public string TFN { get; set; }

        public string ManagedAccount { get; set; }

        public List<AddressModel> Addresses { get; set; }

        public List<NotesViewModel> Notes { get; set; }

        public List<AccountManagerDetailModel> AccountManagers { get; set; }

        public string FolderPath { get; set; }

        public bool HasChild { get; set; }

        public string Category { get; set; }
    }

    [XmlRoot("Value")]
    public class FirmCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The Entity type field is required")]
        public int CategoryID { get; set; }

        public List<SelectListItem> ListCategories { get; set; }

        public string ABN { get; set; }

        public string ACN { get; set; }

        public string GST { get; set; }

        public string TFN { get; set; }

        [Required(ErrorMessage = "The Name field is required")]
        [StringLength(200, ErrorMessage = "Name should be max 200 characters")]
        [RegularExpression(@"^[0-9a-zA-Z''-'\s]*", ErrorMessage = "Special characters are not allowed")]
        public string Name { get; set; }

        public string Note { get; set; }

        public int? ParentID { get; set; }

        public List<AddressCreateModel> ListAddress { get; set; }

        public List<AccountManagerCreateModel> ListUser { get; set; }

        public EntityTypes EntityType { get; set; }

        public int? PrimaryAccountMangerID { get; set; }
    }

    [XmlRoot("Value")]
    public class ParentFirmModel : IModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Parent { get; set; }

        public EntityTypes Type { get; set; }

        public int? CategoryID { get; set; }
    }
}