﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class NotesModel : BaseModel, IModel
    {
        public int ID { get; set; }
        [StringLength(500)]
        [Required]
        public string Note { get; set; }
        public int EntityID { get; set; }
        public int JobID { get;set; }
        public int TypeID { get; set; }
    }

    [XmlRoot("Value")]
    public class NoteModel : BaseModel, IModel
    {
        public int ID { get; set; }
        [StringLength(500)]
        [Required]
        public string Note { get; set; }
        public int ParentId { get; set; }
        public EntityGenericType Type { get; set; }
    }

    [XmlRoot("Value")]
    public class NotesViewModel: IModel
    {
        public string Note { get; set; }
        public DateTime Date { get; set; }
        public int Id { get; set; }
        public string CreatedBy { get; set; }
    }

    [XmlRoot("Value")]
    public class NotesGenericModel :  IModel
    {
        public int ParentID { get; set; }
        public int TypeId { get; set; }
    }
}
