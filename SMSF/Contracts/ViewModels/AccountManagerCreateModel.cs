﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class AccountManagerCreateModel : BaseModel, IModel
    {
        public int ID { get; set; }

        public int EntityID { get; set; }

        public bool Selected { get; set; }

        [Required(ErrorMessage = "The First Name field is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }

        public string MidName { get; set; }

        [Required(ErrorMessage = "The Last Name field is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "The Email field is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        public List<ContactCreateModel> ListContactInfo { get; set; }

        public bool HasLogin { get; set; }

        public UserCreateModel User { get; set; }

        public Status Status { get; set; }

        private List<SelectListItem> listTitle;

        public List<SelectListItem> ListTitle
        {
            get
            {
                if (listTitle == null)
                {
                    listTitle = new List<SelectListItem>();
                    var type = typeof(UserTitles);
                    var names = Enum.GetNames(type);
                    foreach (var name in names)
                    {
                        var field = type.GetField(name);
                        var fds = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
                        foreach (DescriptionAttribute fd in fds)
                        {
                            listTitle.Add(new SelectListItem { Value = fd.Description, Text = fd.Description, Selected = Title == fd.Description });
                        }
                    }
                }
                return listTitle;
            }
        }
    }

    [XmlRoot("Value")]
    public class AccountManagerDetailModel : IModel
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "The First Name field is required")]
        public string FirstName { get; set; }

        public string Title { get; set; }

        public string MidName { get; set; }

        [Required(ErrorMessage = "The Last Name field is required")]
        public string LastName { get; set; }

        public string Email { get; set; }

        public string ParentEntity { get; set; }

        public string ParentClient { get; set; }
        public bool Locked { get; set; }

        public UserDetailModel UserDetail { get; set; }

        public List<ContactModel> ListContactInfo { get; set; }

        public List<AccountManagerJobViewModel> Jobs { get; set; }
    }

    [XmlRoot("Value")]
    public class AccountManangerEmailValidation : IModel
    {
        public int? ID { get; set; }

        public int EntityID { get; set; }

        public string Email { get; set; }
    }

    [XmlRoot("Value")]
    public class AccountManagerJobViewModel : IModel
    {
        public string FundName { get; set; }

        public string ClientName { get; set; }

        public string JobTitle { get; set; }

        public List<string> ManagerRole { get; set; }

        public string JobFlowStatus { get; set; }
    }

    [XmlRoot("Value")]
    public class AccountManagerViewModel : IModel
    {
        public int ID { get; set; }

        public string EncryptedID
        {
            get { return Utility.EncryptQueryString(ID.ToString()); }
        }

        public string Name { get; set; }

        public string Firm { get; set; }

        public string Account { get; set; }

        public string Email { get; set; }

        public string Group { get; set; }

        public int Jobs { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? LastLogonTime { get; set; }

        public string Locked { get; set; }
    }
}