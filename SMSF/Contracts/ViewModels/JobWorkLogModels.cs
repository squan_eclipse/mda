﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class JobWorkLogAccountManagerModel : IModel
    {
        public int JobID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<DictionaryModel<int, string>> AccountManagers { get; set; }
    }

    [XmlRoot("Value")]
    public class JobWorkLogModel : BaseModel, IModel
    {
        public int ID { get; set; }
        public int JobID { get; set; }
        public string Job { get; set; }
        [Required]
        public int AccountManagerID { get; set; }
        public string AccountManager { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Start { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime End { get; set; }
        public string Description { get; set; }
        public double Hours
        {
            get { return Math.Round(End.Subtract(Start).TotalHours, 2); }
        }
    }
}
