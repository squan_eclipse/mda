﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    [XmlRoot("Value")]
    public class JobImportedDataViewModel : IModel
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public int TemplateId { get; set; }
        public WorkpaperContentType ControlType { get; set; }
        public string XmlData { get; set; }
        public string JsonData { get; set; }
        public string FileName { get; set; }

        public string Caption
        {
            get { return FileName; }
        }

        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DocumentModel SourceFile { get; set; }

    }

    [XmlRoot("Value")]
    public class JobImportedFileViewModel : IModel
    {
        public int Id { get; set; }
        public int JobId { get; set; }
        public string Category { get; set; }
        public string Template { get; set; }
        public string FileName { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
