﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace DBASystem.SMSF.Contracts.ViewModels
{
    #region System Setting Models

    [XmlRoot("Value")]
    public class SystemSettingsModel : BaseModel, IModel
    {
        public SystemSettingsModel()
        {
            PasswordSetting = new PasswordSettingsModel();
            EmailSetting = new EmailSettingsModel();
            VersionSetting = new VersionSettingModel();
            SessionSetting = new SessionSettingModel();
        }

        public PasswordSettingsModel PasswordSetting { get; set; }

        public EmailSettingsModel EmailSetting { get; set; }

        [Required(ErrorMessage = "The field Max Session Timeout is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Max Session Timeout")]
        [Range(20, Int32.MaxValue, ErrorMessage = "Minimum Session Timeout should be {1} minutes")]
        public SessionSettingModel SessionSetting { get; set; }

        public VersionSettingModel VersionSetting { get; set; }
    }

    [XmlRoot("Value")]
    public class PasswordSettingsModel : BaseModel, IModel, ISettingModel
    {
        private SettingTypes _settingType;

        public SettingTypes SettingType
        {
            get
            {
                return _settingType;
            }
        }

        public PasswordSettingsModel()
        {
            _settingType = SettingTypes.PasswordSettings;
        }

        [Required(ErrorMessage = "The field password length is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid password length")]
        [Range(6, 50, ErrorMessage = "Password length should be greater then equal to {1} and less then equal to {2}")]
        public int MinPasswordLength { get; set; }

        [Required(ErrorMessage = "The field  minimum upper case characters is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid minimum upper case characters")]
        public int MinUpperCaseCharacters { get; set; }

        [Required(ErrorMessage = "The field  minimum lower case characters is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid minimum lower case characters")]
        public int MinLowerCaseCharacters { get; set; }

        [Required(ErrorMessage = "The field  minimum numeric characters is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid minimum numeric characters")]
        public int MinNumericCharacters { get; set; }

        [Required(ErrorMessage = "The field  max password retries  is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid max password retries")]
        [Range(3, 15, ErrorMessage = "Max password retries should be greater then equal to {1} and less then {2}")]
        public int MaxPasswordRetries { get; set; }
    }

    [XmlRoot("Value")]
    public class EmailSettingsModel : BaseModel, IModel, ISettingModel
    {
        private SettingTypes _settingType;

        public SettingTypes SettingType
        {
            get
            {
                return _settingType;
            }
        }

        public EmailSettingsModel()
        {
            _settingType = SettingTypes.EmailSettings;
        }

        [Required(ErrorMessage = "SMTP host is required")]
        public string SMTPHost { get; set; }

        [Required(ErrorMessage = "SMTP user name is required")]
        public string SMTPUserName { get; set; }

        public string SMTPPassword { get; set; }

        [System.ComponentModel.DataAnnotations.Compare("SMTPPassword", ErrorMessage = "Password does not match")]
        public string ConfirmSMTPPassword { set; get; }

        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "Invalid email address")]
        [Required(ErrorMessage = "SMTP from address is required ")]
        public string SMTPFromAddress { get; set; }

        [Required(ErrorMessage = "SMTP port is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid port")]
        public int SMTPPort { get; set; }

        [RegularExpression(@"^(true|false)$", ErrorMessage = "Invalid SSL [valid values : true/false]")]
        public string SMTPRequireSSL { get; set; }
    }

    [XmlRoot("Value")]
    public class PasswordPolicyModel : IModel, ISettingModel
    {
        private SettingTypes _settingType;

        public SettingTypes SettingType
        {
            get
            {
                return _settingType;
            }
        }

        public PasswordPolicyModel()
        {
            _settingType = SettingTypes.PasswordSettings;
        }

        public string ValidationExpression { get; set; }

        public string ValidationMessage { get; set; }

        public int MaxPasswordRetry { get; set; }
    }

    [XmlRoot("Value")]
    public class SessionSettingModel : BaseModel, IModel, ISettingModel
    {
        private SettingTypes _settingType;

        public SettingTypes SettingType
        {
            get
            {
                return _settingType;
            }
        }

        public SessionSettingModel()
        {
            _settingType = SettingTypes.SessionSettings;
        }

        [Required(ErrorMessage = "The field Max Session Timeout is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid Max Session Timeout")]
        [Range(20, Int32.MaxValue, ErrorMessage = "Minimum Session Timeout should be {1} minutes")]
        public int MaxSessionTimeout { get; set; }
    }

    [XmlRoot("Value")]
    public class VersionSettingModel : BaseModel, IModel, ISettingModel
    {
        private SettingTypes _settingType;

        public SettingTypes SettingType
        {
            get
            {
                return _settingType;
            }
        }

        public VersionSettingModel()
        {
            _settingType = SettingTypes.VersionSettings;
        }

        public string VersionNumber { get; set; }
    }

    #endregion System Setting Models

    #region User Profile Models

    [XmlRoot("Value")]
    public class UserProfileModel : BaseModel, IModel
    {
        public AccountManagerDetailModel UserDetail { get; set; }

        public string EntityAddress { get; set; }

        public bool IsPrimaryAccountManager { get; set; }

        public List<AddressCreateModel> ListAddress { get; set; }

        public ChangePasswordModel ChangePassword { get; set; }

        public string ProfilePicBase64 { get; set; }

        public bool UpdatePersonalInfo { get; set; }

        public bool UpdateProfilePic { get; set; }

        public bool UpdatePassword { get; set; }

        public PasswordPolicyModel PasswordPolicy { get; set; }

        public List<ContactCreateModel> ListContact { get; set; }
    }

    [XmlRoot("Value")]
    public class ChangePasswordModel : IModel
    {
        [Required(ErrorMessage = "The field current password is required")]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "The field new password is required")]
        public string NewPassword { get; set; }

        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "Password does not match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public int UserId { get; set; }
    }

    #endregion User Profile Models

    #region Entity Setting Models

    public class EntitySettingModel : IModel
    {
        public int EntityId { get; set; }

        public int EntityAddress { get; set; }

        public int UserId { get; set; }
    }

    #endregion Entity Setting Models

    #region Share point Setting Models

    [XmlRoot("Value")]
    public class SharePointSettingModel : BaseModel, IModel, ISettingModel
    {
        private SettingTypes _settingTypes;

        public SettingTypes SettingType
        {
            get { return _settingTypes; }
        }

        public SharePointSettingModel()
        {
            _settingTypes = SettingTypes.SharepointSettings;
        }

        [Required(ErrorMessage = "The field User Name is required")]
        public string UserName { get; set; }

        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Password does not match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Sharepoint URL is required")]
        public string SharePointUrl { get; set; }

        [Required(ErrorMessage = "Max file size is required")]
        public int MaximumFileSize { get; set; }

        [Required(ErrorMessage = "Allowed file type is required")]
        public string AllowedFileTypes { get; set; }

        public List<string> AllowedFileTypeList
        {
            get
            {
                return AllowedFileTypes == null ? new List<string>() : new List<string>(AllowedFileTypes.Split(','));
            }
        }

        [Required(ErrorMessage = "Job default document path is required")]
        public string JobDefaultDocumentsPath { get; set; }

        [Required(ErrorMessage = "Report Template path is required")]
        public string ReportTemplatePath { get; set; }
    }

    #endregion Share point Setting Models

    #region Report Server Model

    [XmlRoot("Value")]
    public class ReportServerSettingModel : BaseModel, IModel, ISettingModel
    {
        private SettingTypes _settingType;

        public SettingTypes SettingType
        {
            get
            {
                return _settingType;
            }
        }

        public ReportServerSettingModel()
        {
            _settingType = SettingTypes.ReportServerSettings;
        }

        [Required(ErrorMessage = "The field Report Server Url is required")]
        public string ReportServerUrl { get; set; }

        public string ReportServerUserName { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessage = "Password does not match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public bool SetPassword { get; set; }
    }

    #endregion Report Server Model
}