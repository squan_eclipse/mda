﻿using System;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;

namespace DBASystem.SMSF.Contracts.Common.Encryption
{
    public static class AesEncryption
    {
        // 128bit(16byte)IV and Key
        private const string AesIv = @"OBJECTSYNERGYFFF";
        private const string AesKey = @"FFFOBJECTSYNERGY";

        /// <summary>
        /// AES Encryption , Returns Base64 string 
        /// </summary>
        public static string Encrypt(string text,bool compressText=false)
        {
            // AesCryptoServiceProvider
            if (compressText)
                text = Compress(text);
            
            var aes = new AesCryptoServiceProvider
            {
                BlockSize = 128,
                KeySize = 128,
                IV = Encoding.UTF8.GetBytes(AesIv),
                Key = Encoding.UTF8.GetBytes(AesKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            // Convert string to byte array
            var src = Encoding.Unicode.GetBytes(text);

            // encryption
            using (var encrypt = aes.CreateEncryptor())
            {
                byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

                // Convert byte array to Base64 strings
                return Convert.ToBase64String(dest);
            }
        }

        /// <summary>
        /// AES decryption
        /// </summary>
        public static string Decrypt(string text, bool deCompressText=false)
        {
            // AesCryptoServiceProvider
            var aes = new AesCryptoServiceProvider
            {
                BlockSize = 128,
                KeySize = 128,
                IV = Encoding.UTF8.GetBytes(AesIv),
                Key = Encoding.UTF8.GetBytes(AesKey),
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            };

            // Convert Base64 strings to byte array
            var src = Convert.FromBase64String(text);

            // decryption
            string decryptedText;
            using (var decrypt = aes.CreateDecryptor())
            {
                var dest = decrypt.TransformFinalBlock(src, 0, src.Length);
                decryptedText = Encoding.Unicode.GetString(dest);
            }
            return deCompressText ? Decompress(decryptedText) : decryptedText;
        }

        public static string Compress(string s)
        {
            var bytes = Encoding.Unicode.GetBytes(s);
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    msi.CopyTo(gs);
                }
                return Convert.ToBase64String(mso.ToArray());
            }
        }

        public static string Decompress(string s)
        {
            var bytes = Convert.FromBase64String(s);
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    gs.CopyTo(mso);
                }
                return Encoding.Unicode.GetString(mso.ToArray());
            }
        }
    }
}
