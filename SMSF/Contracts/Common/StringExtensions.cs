﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;

namespace DBASystem.SMSF.Contracts.Common
{
    public static class StringExtensions
    {
        public static T ToType<T>(this string value)
        {
            return (T)Convert.ChangeType(value, typeof(T));
        }
    }

    public static class DictionaryExtension
    {
        public static string GetValue(this Dictionary<string, string> dictionary, string key)
        {
            return dictionary.ContainsKey(key) ? !string.IsNullOrEmpty(dictionary[key]) ? dictionary[key] : "" : "";
        }

        public static T GetValueOrDefault<T, TKey, TValue>(this Dictionary<TKey, TValue> value, TKey key)
        {
            if (value.ContainsKey(key))
            {
                try
                {
                    return (T) Convert.ChangeType(value[key], typeof (T));
                }
                catch
                {
                    return default(T);
                }
            }
            return default(T);
        }
    }

    public static class EnumExtension
    {
        public static string GetDescription(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());
            DescriptionAttribute attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }
    }

    public static class StreamExtensions
    {
        public static Stream CopyStream(this Stream inputStream)
        {
            const int readSize = 256;
            byte[] buffer = new byte[readSize];
            MemoryStream ms = new MemoryStream();

            int count = inputStream.Read(buffer, 0, readSize);
            while (count > 0)
            {
                ms.Write(buffer, 0, count);
                count = inputStream.Read(buffer, 0, readSize);
            }
            ms.Position = 0;
            inputStream.Close();
            return ms;
        }

        public static string ToBase64String(this Stream inputSream)
        {
            byte[] data;
            using (var stream = inputSream)
            {
                var ms = stream as MemoryStream;
                if (ms == null)
                {
                    ms = new MemoryStream();
                    stream.CopyTo(ms);
                }
                data = ms.ToArray();
            }
            return Convert.ToBase64String(data);
        }
    }
}