﻿using DBASystem.SMSF.Contracts.Common.Encryption;
using NLog;
using System;
using System.Globalization;

namespace DBASystem.SMSF.Contracts.Common 
{
    public class Utility
    {
        private static Logger m_logger = LogManager.GetCurrentClassLogger();

        static public Logger Logger 
        {
            get
            {
                return m_logger;
            }
        }

        static public string GetExceptionDetails(Exception ex)
        {
            return string.Format
                (
                    "Message: {0}\r\nInner Message: {1}\r\n", 
                    (ex != null) ? ex.Message : "", 
                    (ex != null) ? ((ex.InnerException != null)? ex.InnerException.Message : "") : ""
                );
        }

        static public string GetComputerName()
        {
            if (System.Configuration.ConfigurationManager.ConnectionStrings[Environment.MachineName] != null)
                return Environment.MachineName;

            return "SMSFEntities";
        }

        public static string EnumValue(Enum e)
        {
            return ((char)e.GetHashCode()).ToString(CultureInfo.InvariantCulture);
        }

        public static T ToEnum<T>(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                throw new ArgumentException("Argument null or empty");
            }
            if (s.Length > 1)
            {
                throw new ArgumentException("Argument length greater than one");
            }
            return (T)Enum.ToObject(typeof(T), s[0]);
        }

        public static string GetConnectionString
        {
            get
            {
                if (System.Configuration.ConfigurationManager.ConnectionStrings[Environment.MachineName] != null)
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[Environment.MachineName].ConnectionString;

                    if (!string.IsNullOrEmpty(connectionString))
                    {
                        Logger.Info("using the local connection string: {0}", connectionString);
                        return connectionString;
                    }
                }

                Logger.Info("using the default connection string");
                return System.Configuration.ConfigurationManager.ConnectionStrings["SMSFEntities"].ConnectionString;
            }
        }

        public static string EncryptQueryString(int queryString)
        {
            return EncryptQueryString(queryString.ToString());
        }

        public static string EncryptQueryString(string queryString)
        {
            return AesEncryption.Encrypt(queryString).Replace('+', '±').Replace('/', '|');
        }

        public static string DecryptQueryString(string queryString)
        {
            return AesEncryption.Decrypt(queryString.Replace('±', '+').Replace('|', '/'));
        }
    }

    public static class Tracer
    {
        public static DateTime Start(string modelXml)
        {
            Utility.Logger.Trace
            (
                string.Format
                (
                    ">> Execute >> Server: {0} >> Thread ID: {1} >> Data: {2}",
                    Utility.GetComputerName(),
                    System.Threading.Thread.CurrentThread.ManagedThreadId,
                    modelXml
                )
            );

            return DateTime.Now;
        }

        public static DateTime End(DateTime time)
        {
            TimeSpan interval = DateTime.Now - time;
            Utility.Logger.Trace
            (
                string.Format
                (
                    ">> Execute >> Server: {0} >> Thread ID: {1} >> Completed: {2} ms>>",
                    Utility.GetComputerName(),
                    System.Threading.Thread.CurrentThread.ManagedThreadId,
                    interval.TotalMilliseconds
                )
            );

            return DateTime.Now;
        }
    }
}