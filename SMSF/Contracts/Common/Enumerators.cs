﻿using System.ComponentModel;

namespace DBASystem.SMSF.Contracts.Common
{
    public enum Status
    {
        Active = 1,
        Inactive = 2,
        Deleted = 3,
        Pending = 4,
        Denied = 5
    }

    public enum ResponseStatus
    {
        Success,
        Failure,
        Error,
        Warning
    }

    public enum Option
    {
        // Login
        Login = 101,

        LogOut = 102,
        UpdateLastLogin = 103,

        // Anonymous actions
        AllowedPages = 201,

        AllowedOptions = 202,
        GetUserProfile = 203,
        UpdatePersonalInfo = 204,
        UpdateEntityAddress = 205,

        // Address
        ListAddressTypes = 301,
        ListCountries = 302,
        ListStates = 303,
        AddCountry = 304,
        AddState = 305,

        // Telephone
        GetTelephoneTypes = 401,

        AddTelephone = 402,
        UpdateTelephone = 403,
        DeleteTelephone = 404,
        GetCountryCode = 405,

        // User
        CreateAccountManager = 501,

        DeleteUser = 502,
        UpdateAccountManager = 503,
        GetAccountManager = 504,
        ValidateEmail = 505,
        GetAccountManagers = 506,
        HasMultipleUserAccess = 507,
        GetFirmAccountManager = 508,
        GetAccountManagerDetail = 509,
        UnlockUser = 510,

        // Rights Management
        CreateUserGroup = 601,

        UpdateUserGroup = 602,
        ChangeUserGroupStatus = 603,
        GetUserGroups = 604,
        GetUserGroup = 605,
        GetRoles = 606,
        GetRole = 607,
        CreateRole = 608,
        UpdateRole = 609,
        ChangeRoleStatus = 610,
        GetUserGroupRoles = 611,
        AddUserGroupRoles = 612,
        GetUserRoleOptions = 613,
        AddUserRoleOptions = 614,
        GetUerAssociatedEntites = 615,
        SendForgotPassEmail = 616,
        ResetPassword = 617,

        //For Job
        GetJobManagers = 701,
        GetScheduledJobs = 702,
        GetCompletedJobs = 703,
        AddJob = 704,
        GetJobInfo = 705,
        UpdateJob = 706,
        Deletejob = 707,
        GetJobToBeCompleted = 708,
        GetJobFiles = 722,
        GetJobs = 723,
        GetJobRoles = 728,
        GetFundJobs = 729,
        HasMultipleJobAccess = 730,

        //For Job (AuditProcedures/WorkPapers)
        GetJob = 731, // reuse this 
        WorkPaper = 732,
        GetWorkpaperMenu = 733,
        GetAuditProcedures = 734,
        GetAuditProcedureTabContents = 735,
        UpdateAuditProcedureAnswer = 736,
        AddProcedureComments = 737,
        GetProcedureComments = 738,
        AddEditProcedureOml = 739,
        GetProcedureOml = 740,
        GetAuditProcedureAttachments = 741,
        AddEditAuditProcedureAttachments = 742,
        DownloadAttachmentFile = 743,
        DeleteAuditProcedureAttachmentFile = 744,
        GetProcedureTestSummary = 745,
        GetProcedureOmlForManagerReview = 746,

        // Work Logs
        GetWorkLogAccountManagers = 747,
        GetWorkLogs = 748,
        AddWorkLog = 749,
        UpdateWorkLog = 750,
        DeleteWorkLog = 751,
        SaveTestSummary = 752,
        GetJobImportData = 753,

        //Manage Audit
        GetAuditProcedureTabs = 769,
        GetAuditProcedureAddContents=770,
        GetAuditProcedureEditContents = 771,
        AddAuditProcedure = 772,
        UpdateAuditProcedureEditContents = 773,
        DeleteAuditProcedure = 774,
        AddAuditProcedureTab = 775,
        DeleteAuditProcedureTab = 776,
        UpdateAuditProcedureTab = 777,
        GetAuditProcedureTabContent = 778,
        UpdateAuditProcedureTabContent=779,
        DeleteAuditProcedureTabContent=780,
        AddAuditProcedureTabContent=781,
        GetAuditProcedureOptionDetail = 782,
        GetAuditProcedureCommentsByJob = 783,
        SaveWorkpaperTemplate = 784,
        GetWorkpaperTemplates = 785,
        LoadWorkpaperTemplate = 786,
        SaveWorkpaperTemplateAsXML = 787,
        LoadWorkpaperTemplateFromXml = 788,

        // Notifications
        GetNotifications = 801,

        AddError = 802,
        DeleteNotifications = 803,

        // Activity
        GetActivities = 901,

        GetActivity = 902,
        AddActivity = 903,
        UpdateActivity = 904,
        DeleteActivity = 905,

        // Setting
        GetSetting = 1001,

        UpdateEmailSettings = 1002,
        UpdateUserPassword = 1003,
        GetSettingByType = 1004,
        UpdateSharepointSetting = 1005,
        UpdatePasswordSettings = 1006,
        UpdateSessionSettings = 1007,
        UpdateReportServerSettings = 1008,
        UpdateVersionSettings = 1009,
        // Sharepoint
        GetFiles = 1101,

        DeleteFile = 1102,
        DownloadFile = 1103,
        UploadFiles = 1104,

        // Workflow
        GetWorkflow = 1201,

        GetWorkflowTemplate = 1202,
        GetWorkflowPendingProcesses = 1203,
        GetWorkflowPendingProcessDetail = 1204,
        WorkflowProcessExecuteCommand = 1205,
        UpdateWorkflowProcess = 1206,
        GetWorkFlowGraph = 1207,
        GetWorkflowTemplateList = 1208,
        SetDefaultWorkFlow = 1209,
        SaveWorkFlowGraph = 1210,

        //Imports
        GetImportTemplates = 1301,

        GetTemplate = 1302,
        GetTemplateProviders = 1303,
        AddUpdateJobsImportedData = 1304,
        GetTemplateCategories = 1305,
        GetTemplatesBy = 1306,
        GetJobsImportedData = 1307,
        GetJobFlowStatusList = 1308,
        GetJobsImportedFiles = 1309,

        //Notes
        GetNotes = 1401,

        AddUpdateNotes = 1402,
        AddFirmNotes = 1403,
        UpdateFirmNotes = 1404,
        GetFirmNotes = 1405,
        AddJobNotes = 1406,
        UpdateJobNotes = 1407,
        GetJobNotes = 1408,
        DeleteFirmNotes = 1409,
        DeleteJobNotes = 1410,

        //Firms
        GetFirms = 1501,

        GetFirmDetail = 1502,
        HasMultipleAccess = 1503,
        GetParents = 1504,
        AddFirm = 1505,
        GetFirmCategories = 1506,
        DeleteFirm = 1507,
        GetFirm = 1508,
        UpdateFirm = 1509,

        //Accounts
        GetAccounts = 1601,

        GetAccountDetail = 1602,
        AddAccount = 1603,
        UpdateFund = 1604,

        //Funds
        GetFunds = 1701,

        GetFundDetail = 1702,
        AddFund = 1703,
        UpdateAccount = 1704,

        // Group
        AddGroup = 1801,

        UpdateGroup = 1802,
        GetGroupDetail = 1803,

        //Reports 
        AuditReports = 1901,


        
    }

    public enum OptionType
    {
        Page = 1,
        Menu = 2,
        NextPage = 3,
        Report = 4,
        View = 5,
        Modify = 6,
        New = 7,
        Remove = 8,
        Cancel = 9,
        Print = 10,
        Search = 11,
        Maker = 12,
        Checker = 13,
        CustomAction = 14
    }

    public enum EntityTypes
    {
        [Description("Firms")]
        Entity = 1,

        Group = 2,

        [Description("Accounts")]
        Client = 3,

        [Description("Funds")]
        Fund = 4,

        // -- Following have been added to generalize CRUD operarions on Entity pages
        User = 5,

        Job = 6,

        IFM = 7,
    }

    public enum StreetType
    {
        ALLEY,
        APPROACH,
        ARCADE,
        AVENUE,
        BOULEVARD,
        BROW,
        BYPASS,
        CAUSEWAY,
        CIRCUIT,
        CIRCUS,
        CLOSE,
        COPSE,
        CORNER,
        COVE,
        COURT,
        CRESCENT,
        DRIVE,
        END,
        ESPLANANDE,
        FLAT,
        FREEWAY,
        FRONTAGE,
        GARDENS,
        GLADE,
        GLEN,
        GREEN,
        GROVE,
        HEIGHTS,
        HIGHWAY,
        LANE,
        LINK,
        LOOP,
        MALL,
        MEWS,
        PACKET,
        PARADE,
        PARK,
        PARKWAY,
        PLACE,
        PROMENADE,
        RESERVE,
        RIDGE,
        RISE,
        ROAD,
        ROW,
        SQUARE,
        STREET,
        STRIP,
        TARN,
        TERRACE,
        THOROUGHFARE,
        TRACK,
        TRUNKWAY,
        VIEW,
        VISTA,
        WALK,
        WAY,
        WALKWAY,
        YARD
    }

    public enum StatusMessage
    {
        [Description("")]
        None,

        [Description("Successful")]
        Successful,

        [Description("Failure")]
        Failure,

        [Description("Multiple users found")]
        MultipleUsersFound,

        [Description("Error")]
        Error,

        [Description("User Not Found")]
        UserNotFound,

        [Description("Login Name Already Exists")]
        LoginNameAlreadyExists,

        [Description("Unexpected Error Occured")]
        UnexpectedErrorOccured,

        [Description("Entity Not Found")]
        EntityNotFound,

        [Description("Invalid Username Or Password")]
        InvalidUsernameOrPassword,

        [Description("Login Name Available")]
        LoginNameAvailable,

        [Description("No Action Signature Found")]
        NoActionSignatureFound,

        [Description("Access Denied")]
        AccessDenied,

        [Description("ABN Already Exists")]
        ABNAlreadyExists,

        [Description("ACN Already Exists")]
        ACNAlreadyExists,

        [Description("GST Already Exists")]
        GSTAlreadyExists,

        [Description("TFN Already Exists")]
        TFNAlreadyExists,

        [Description("Name Already Exists")]
        NameAlreadyExists,

        [Description("User Login Already Exists")]
        UserLoginAlreadyExists,

        [Description("Entity Type Of \"Group\" Not Found")]
        EntityTypeOfGroupNotFound,

        [Description("Unable To Add")]
        UnableToAdd,

        [Description("Unable To Delete")]
        UnableToDelete,

        [Description("Record Added Successfully")]
        RecordAddedSuccessfully,

        [Description("Record Deleted Successfully")]
        RecordDeletedSuccessfully,

        [Description("Group with the same name already exists")]
        UserGroupAlreadyExists,

        [Description("The model proided is either Empty or Invalid")]
        EmptyOrInvalidModel,

        [Description("Unable to update the specified record(s)")]
        UnableToUpdate,

        [Description("Role with the same name already exists")]
        RoleAlreadyExists,

        [Description("Invalid request or reset request has already been used")]
        ResetKeyUsedOrNotFound,

        [Description("Reset request has expired")]
        ResetKeyExpired,

        [Description("Your account is locked, contact your adminstartor or use forgot password option to activate your account")]
        UserAccountLocked,

        [Description("Top Level Entity cannot be moved")]
        TopLevelEntity,

        [Description("Entity associations found")]
        EntityAssociationsFound,

        [Description("Email Settings Not Found")]
        EmailSettingsNotFound,

        [Description("System Settings Not Found")]
        SystemSettingsNotFound,

        [Description("Invalid current password")]
        InvalidCurrentPassword,

        [Description("Invalid image file")]
        InvalidImageFile,

        [Description("Destination not available")]
        DestinationNotFound
    }

    public enum UserGroup
    {
        [Description("System Administrators")]
        Administrators = 1,
        Operators = 2,
        Clients = 3
    }

    public enum EntityTreeType
    {
        All,
        Entities,
        Clients,
        Funds,
        Jobs,
        Users
    }

    public enum EntityGenericType
    {
        Entity = 1,
        Group = 2,
        Client = 3,
        Fund = 4,
        User = 5,
        Job = 6,
        IFM = 7,
    }

    public enum EntityCategories
    {
        System = 1,
        ClubOrSociety = 2,
        Company = 3,
        Individual = 4,
        NonprofitOrganisation = 5,
        Partnership = 6,
        SMSF = 7,
        SoleTrader = 8,
        SuperannuationFund = 9,
        Trust = 10
    }

    public enum SettingTypes
    {
        EmailSettings = 1,
        PasswordSettings = 2,
        SessionSettings = 3,
        SharepointSettings = 4,
        ReportServerSettings = 5,
        VersionSettings = 6,
    }

    public enum SettingHeads
    {
        System = 1,
        Enittes = 2,
        ChangePassword = 3,
        UserProfile = 4,
        Logs = 5,
        Sharepoint = 6,
        DefaultDocuments = 7,
        ReportServer = 8,
        Groups = 9,
        Roles = 10,
        ReportsTemplates= 11
    }

    public enum NotificationTypes : int
    {
        Info = 1,
        Error = 2
    }

    public enum OfficeFileTypes
    {
        docx,
        doc,
        xls,
        xlsx,
        ppt,
        pptx
    }

    #region Workflow

    public enum ParameterPurpose
    {
        Temporary = 1,
        System = 2,
        Persistence = 3
    }

    public enum TransitionClassifier
    {
        Direct = 1,
        Reverse = 2,
        NotSpecified = 3
    }

    public enum TransitionCommandType
    {
        Command = 1,
        Auto = 2,
        Timer = 3
    }

    public enum TransitionActionType
    {
        Always = 1,
        Action = 2,
        Otherwise = 3
    }

    public enum TransitionRestrictionType
    {
        Allow = 1,
        Restrict = 2
    }

    #endregion Workflow

    public enum WorkpaperContentType
    {
        AuditProcedure = 1,
        Import = 2,
        Report = 3,
        ManagerReview = 4,
        TestSummary = 5,
        InitialAuditQuries = 6
    }

    public enum ControlType
    {
        //For Generic
        Label = 1,
        TextBox = 2,
        Dropdown = 3,
        Form = 4,
        List = 5,
        Grid = 6,
        GridMultiCol = 7,

        //For Custom
        ManagerReview = 20,
        TestSummary = 21,
        InitialAuditQuries = 22,
        Custom = 99,
    }

    public enum AnswerControlType
    {
        //For Generic
        Label = 1,
        TextBox = 2,
        Dropdown = 3,
    }

    public enum ImportTemplate
    {
        //Prefixes stands for
        //DS -> Desktop Super
        //CS -> Class Super
        //BGL -> BGL Provider


        DSStatementOfCashflows = 1,
        DSStatementOfFinancialPosition = 2,
        DSGeneralLedger = 3,
        DSInvestmentIncomeSummary = 4,
        DSInvestmentMovement = 5,
        DSInvestmentSummary = 6,
        DSOperatingStatement = 7,
        DSRealisedCGT = 8,
        DSTaxReconciliation = 9,
        DSTrialBalance = 10,
        DSUnrealisedCGT = 11,

        BGLStatementCashflows = 12,
        BGLStatementFinancialPosition = 13,
        BGLInvestmentDisposals = 14,
        BGLInvestmentSummary = 15,
        BGLNotesFinancialStatement = 16,
        BGLOperatingStatement = 17,
        BGLIncomeStatement = 18,
        BGLInvestmentIncome = 19,
        BGLInvestmentMovement = 20,
        BGLMemberStatement = 21,
        BGLInvestmentSchedule = 22,
        BGLTaxReconciliation = 23,

        CSInvestmentIncomeComparison = 24,
        CSInvestmentIncomeSummary = 25,
        CSInvestmentMovementSummary = 26,
        CSInvestmentPerformance = 27,
        CSInvestmentPortfolio = 28,
        CSInvestmentSummary = 29,

        SF360BalanceSheet = 30,
        SF360Cashflow = 31,
        SF360InvestmentDisposal = 32,
        SF360InvestmentMovement = 33,
        SF360InvestmentSummary = 34,
        SF360MembersStatement = 35,
        SF360NotesToFinancialStatement = 36,
        SF360OperatingStatement = 37,
        SF360InvestmentIncome = 38,

        BGLTaxableIncome = 39,
        SF360TaxableIncome = 40,

        CSBankStatement = 41,
        CSGeneralLedger = 42,
        CSInvestmentStrategy = 43,
        CSMembersStatement = 44,
        CSNotesToFinancialStatements = 45,
        CSOperatingStatement = 46,
        CSRealisedCapitalGains = 47,
        CSFinancialPosition = 48,
        CSTaxableIncome = 49,
        CSTrusteeMinuteResolution = 50
    }

    public enum WorkflowActorType
    {
        ExecuteRule = 1,
        InRole = 2,
        Identity = 3
    }

    public enum AuditProcedureDialogType
    {
        Attachment = 1,
        Oml = 2,
        Comments = 3,
    }

    public enum UserTitles
    {
        [Description("Mr.")]
        Mr,

        [Description("Miss")]
        Miss,

        [Description("Mrs.")]
        Mrs,
    }
}

public enum Workflows
{
    JobProcess = 1,
}

public enum JobWorkflowStatus
{
    [Description("In Progress")]
    InProgress = 1,
    [Description("Completed")]
    Completed = 2,
    [Description("Overdue")]
    Overdue = 3,
    [Description("Not Started")]
    NotStarted = 4
}