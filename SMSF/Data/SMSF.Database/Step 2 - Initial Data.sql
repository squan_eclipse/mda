﻿/*****************************************************************
	User this script with a new database 
*****************************************************************/
USE [DBASystem]
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1000000, N'Test 1', N'Did the fund fail to meet the defination of an SMSF?')
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1001000, N'Test 2', N'As at the end of the financial year, is the SMSF less than 15 months old and did the value of any single contravention exceed $2,000?')
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1002000, N'Test 3', N'Hav the trustees previously received advice of a ccontravention that they breached again?')
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1003000, N'Test 4', N'Is there an identified contravention from a previous year that has not been rectified at the time the audit is being conducted?')
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1004000, N'Test 5', N'Dis the trustees fail to meet a statutory time period prescribed in the SISA and SISR by more than 14 days?')
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1005000, N'Test 6', N'Was the total value of all contraventions greater than 5% of the total value of the fund''s assets?')
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1006000, N'Test 7', N'Was the total of all contraventions greater than $30,000?')
GO
INSERT [dbo].[TestSummary] ([ID], [TestName], [Description]) VALUES (1007000, N'Test 8', N'Is there a reportable contravention?')
GO
SET IDENTITY_INSERT [dbo].[EntityCategories] ON 

GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (1, N'System', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (2, N'Club or Society', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (3, N'Company', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (4, N'Individual', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (5, N'Non-profit Organisation', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (6, N'Partnership', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (7, N'SMSF', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (8, N'Sole Trader', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (9, N'Superannuation Fund', 1)
GO
INSERT [dbo].[EntityCategories] ([ID], [Description], [StatusID]) VALUES (10, N'Trust', 1)
GO
SET IDENTITY_INSERT [dbo].[EntityCategories] OFF
GO
SET IDENTITY_INSERT [dbo].[EntityTypes] ON 

GO
INSERT [dbo].[EntityTypes] ([ID], [Description], [StatusID]) VALUES (1, N'Entity', 1)
GO
INSERT [dbo].[EntityTypes] ([ID], [Description], [StatusID]) VALUES (2, N'Group', 1)
GO
INSERT [dbo].[EntityTypes] ([ID], [Description], [StatusID]) VALUES (3, N'Client', 1)
GO
INSERT [dbo].[EntityTypes] ([ID], [Description], [StatusID]) VALUES (4, N'Fund', 1)
GO
SET IDENTITY_INSERT [dbo].[EntityTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Entities] ON 

GO
INSERT [dbo].[Entities] ([ID], [Identifier], [TypeID], [CategoryID], [ABN], [ACN], [GST], [TFN], [Name], [Note], [ParentID], [PrimaryAccountManagerID], [SecondaryAccountManagerID], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'144df887-99c6-4c42-953e-f2927a382532', 1, 1, NULL, NULL, NULL, NULL, N'SYSTEM', NULL, NULL, NULL, NULL, 1, 0, CAST(0x0000A35B00000000 AS DateTime), 0, CAST(0x0000A35B00000000 AS DateTime))
GO
INSERT [dbo].[Entities] ([ID], [Identifier], [TypeID], [CategoryID], [ABN], [ACN], [GST], [TFN], [Name], [Note], [ParentID], [PrimaryAccountManagerID], [SecondaryAccountManagerID], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'd0a790c7-db4f-4837-b8e1-0ee0ee7bcd6d', 1, 3, N'56-456-654-656', NULL, NULL, NULL, N'DBA Advisory Pty Ltd', NULL, 1, NULL, NULL, 1, 0, CAST(0x0000A35B00AB6A46 AS DateTime), 0, CAST(0x0000A35B00AB6A46 AS DateTime))
GO
INSERT [dbo].[Entities] ([ID], [Identifier], [TypeID], [CategoryID], [ABN], [ACN], [GST], [TFN], [Name], [Note], [ParentID], [PrimaryAccountManagerID], [SecondaryAccountManagerID], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, N'301f33c8-24c4-47fc-9377-f724745b5999', 1, 3, N'56-457-569-235', NULL, NULL, NULL, N'DBA System Pty Ltd', NULL, 1, NULL, NULL, 1, 0, CAST(0x0000A35B00AB6A46 AS DateTime), 0, CAST(0x0000A35B00AB6A46 AS DateTime))
GO
INSERT [dbo].[Entities] ([ID], [Identifier], [TypeID], [CategoryID], [ABN], [ACN], [GST], [TFN], [Name], [Note], [ParentID], [PrimaryAccountManagerID], [SecondaryAccountManagerID], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, N'10cba2b7-730e-471a-8421-5b3b644aeb2b', 1, 3, N'52-538-654-563', NULL, NULL, NULL, N'DBA Global Shared Services Inc', NULL, 2, NULL, NULL, 1, 0, CAST(0x0000A35B00AB6A46 AS DateTime), 0, CAST(0x0000A35B00AB6A46 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Entities] OFF
GO
SET IDENTITY_INSERT [dbo].[TestSummaryComments] ON 

GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (1, N'Go To Test 2', 1000000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (2, N'REPORT THIS EVENT - Section 17A - SMSF Defination in section E then proceed to Test 2', 1000000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (3, N'Go To Test 3', 1001000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (4, N'Report all identified contraventions of the sections and regulations listed in Table 1, regardless of the financial thresholds. Do not apply further test', 1001000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (5, N'Go To Test 4', 1002000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (6, N'Test not required', 1002000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (7, N'Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 4', 1002000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (8, N'Go To Test 5', 1003000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (9, N'Test not required', 1003000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (10, N'Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 5', 1003000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (11, N'Go To Test 6', 1004000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (12, N'Test not required', 1004000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (13, N'Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 6', 1004000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (14, N'Go To Test 7', 1005000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (15, N'Test not required', 1005000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (16, N'Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 7', 1005000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (17, N'Go To Test 8', 1006000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (18, N'Test not required', 1006000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (19, N'Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To test. Report Additional information in accordance with the Auditing and Assurance Standards and your professional judgement.', 1006000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (20, N'Report Additional information in accordance with the Auditing and Assurance Standards and your professional judgement.', 1006000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (21, N'Report any other additional information in accordance with the Auditing and Assurance Standards and your professional judgement.', 1007000)
GO
INSERT [dbo].[TestSummaryComments] ([ID], [TestComments], [TestSummaryId]) VALUES (22, N'Complete Contravention Report below for manager/partner review', 1007000)
GO
SET IDENTITY_INSERT [dbo].[TestSummaryComments] OFF
GO
SET IDENTITY_INSERT [dbo].[AccountManagers] ON 

GO
INSERT [dbo].[AccountManagers] ([ID], [Identifier], [EntityID], [Title], [FirstName], [MidName], [LastName], [Email], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'2e525fe5-a1e8-4b49-b36d-13decb375ca8', 1, N'Mr', N'System', NULL, N'Administrator', N'admin@dbasystem.com', 1, 0, CAST(0x0000A35B00000000 AS DateTime), 0, CAST(0x0000A35B00000000 AS DateTime))
GO
INSERT [dbo].[AccountManagers] ([ID], [Identifier], [EntityID], [Title], [FirstName], [MidName], [LastName], [Email], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'cf701278-264b-490e-8898-bdbc05af662d', 2, N'Mr', N'Darlow', NULL, N'Parazo', N'dparazo@dbaadvisory.com', 1, 0, CAST(0x0000A35B00AB6A54 AS DateTime), 0, CAST(0x0000A35B00AB6A54 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[AccountManagers] OFF
GO
SET IDENTITY_INSERT [dbo].[AccountManagerJobRoles] ON 

GO
INSERT [dbo].[AccountManagerJobRoles] ([ID], [Name], [IsActive], [Identifier]) VALUES (1, N'Audit Admin', 1, N'c81231ab-1ffe-433a-a2b1-066476cce6ee')
GO
INSERT [dbo].[AccountManagerJobRoles] ([ID], [Name], [IsActive], [Identifier]) VALUES (2, N'Balance Sheet Auditor', 1, N'577406d2-46f9-47ed-b6ff-0c3c6bf99bbf')
GO
INSERT [dbo].[AccountManagerJobRoles] ([ID], [Name], [IsActive], [Identifier]) VALUES (3, N'Income Statement Auditor', 1, N'73106f7d-ba51-4c20-b608-4adf0df2dd34')
GO
INSERT [dbo].[AccountManagerJobRoles] ([ID], [Name], [IsActive], [Identifier]) VALUES (4, N'SIS Compliance Auditor', 1, N'f53f1e30-62ca-4a43-8e75-bae26d616bb6')
GO
INSERT [dbo].[AccountManagerJobRoles] ([ID], [Name], [IsActive], [Identifier]) VALUES (5, N'Review Manager', 1, N'ba0a02db-9ad8-406f-96b6-2ab87c8023ff')
GO
INSERT [dbo].[AccountManagerJobRoles] ([ID], [Name], [IsActive], [Identifier]) VALUES (6, N'Sign Off Manager', 1, N'69be8d68-6727-408b-b187-3b6b89945f4f')
GO
INSERT [dbo].[AccountManagerJobRoles] ([ID], [Name], [IsActive], [Identifier]) VALUES (7, N'Client User', 0, N'd2be6dfa-27e1-4dae-a79a-496fe899ee95')
GO
SET IDENTITY_INSERT [dbo].[AccountManagerJobRoles] OFF
GO
SET IDENTITY_INSERT [dbo].[AccountRoles] ON 

GO
INSERT [dbo].[AccountRoles] ([ID], [Name], [StatusID], [WorkflowType]) VALUES (1, N'Auditor', 1, 1)
GO
INSERT [dbo].[AccountRoles] ([ID], [Name], [StatusID], [WorkflowType]) VALUES (2, N'Manager', 1, 1)
GO
INSERT [dbo].[AccountRoles] ([ID], [Name], [StatusID], [WorkflowType]) VALUES (3, N'Accountant', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[AccountRoles] OFF
GO
SET IDENTITY_INSERT [dbo].[TelephoneTypes] ON 

GO
INSERT [dbo].[TelephoneTypes] ([ID], [Name], [StatusID]) VALUES (1, N'Fax', 1)
GO
INSERT [dbo].[TelephoneTypes] ([ID], [Name], [StatusID]) VALUES (2, N'Mobile', 1)
GO
INSERT [dbo].[TelephoneTypes] ([ID], [Name], [StatusID]) VALUES (3, N'Fixed Line', 1)
GO
SET IDENTITY_INSERT [dbo].[TelephoneTypes] OFF
GO
INSERT [dbo].[Users] ([ID], [Identifier], [GroupID], [Email], [Password], [LastLoginTime], [StatusID], [FailedPasswordAttemptCount], [FailedPasswordAttemptStart], [UserImage], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, N'2e525fe5-a1e8-4b49-b36d-13decb375ca8', 1, N'admin@dbasystem.com', N'21232F297A57A5A743894A0E4A801FC3', NULL, 1, NULL, NULL, NULL, 0, CAST(0x0000A35B00000000 AS DateTime), 0, CAST(0x0000A35B00000000 AS DateTime))
GO
INSERT [dbo].[Users] ([ID], [Identifier], [GroupID], [Email], [Password], [LastLoginTime], [StatusID], [FailedPasswordAttemptCount], [FailedPasswordAttemptStart], [UserImage], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, N'13bc0b8b-9fd1-472e-b307-4e827214790f', 1, N'dparazo@dbaadvisory.com', N'B78D7CD4555821042A70D9EC034B0DEA', NULL, 1, NULL, NULL, NULL, 0, CAST(0x0000A35B00AB6A58 AS DateTime), 0, CAST(0x0000A35B00AB6A58 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[AddressTypes] ON 

GO
INSERT [dbo].[AddressTypes] ([ID], [Description], [StatusID]) VALUES (1, N'Business', 1)
GO
INSERT [dbo].[AddressTypes] ([ID], [Description], [StatusID]) VALUES (2, N'Mailing', 1)
GO
INSERT [dbo].[AddressTypes] ([ID], [Description], [StatusID]) VALUES (3, N'PO Box', 1)
GO
SET IDENTITY_INSERT [dbo].[AddressTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Countries] ON 

GO
INSERT [dbo].[Countries] ([ID], [Name], [Default], [Code]) VALUES (1, N'Australia', 1, N'+61')
GO
SET IDENTITY_INSERT [dbo].[Countries] OFF
GO
SET IDENTITY_INSERT [dbo].[States] ON 

GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (1, 1, N'Australian Capital Territory', NULL)
GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (2, 1, N'New South Wales', NULL)
GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (3, 1, N'Northern Territory', NULL)
GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (4, 1, N'Queensland', NULL)
GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (5, 1, N'South Australia', NULL)
GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (6, 1, N'Tasmania', NULL)
GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (7, 1, N'Victoria', NULL)
GO
INSERT [dbo].[States] ([ID], [CountryID], [Name], [Code]) VALUES (8, 1, N'Western Australia', NULL)
GO
SET IDENTITY_INSERT [dbo].[States] OFF
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (50, N'Client Reports', 0, NULL, 1, 2)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (100, N'Client Assessment', 1, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (200, N'Planning', 2, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (300, N'Regulatory Review', 3, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (400, N'Statement of FP', 4, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (500, N'Operating Statement', 5, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (600, N'Investments', 6, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (700, N'Income', 7, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (800, N'Members', 8, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (900, N'Manager Partner Review', 9, NULL, 1, 4)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (1000, N'Contravention Report', 10, NULL, 1, 3)
GO
INSERT [dbo].[AuditProcedureTab] ([ID], [Caption], [TabOrder], [EntityId], [IsActive], [ContentType]) VALUES (1100, N'Conclusion', 11, NULL, 1, 3)
GO
SET IDENTITY_INSERT [dbo].[Workflows] ON 

GO
INSERT [dbo].[Workflows] ([ID], [Name]) VALUES (1, N'Job Process')
GO
SET IDENTITY_INSERT [dbo].[Workflows] OFF
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (1000, 100, N'Audit Objectives', 1, 5, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (2000, 100, N'Compliance with Ethical Requirements including  Independence', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (3000, 100, N'Client Acceptance and Retention', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (4000, 100, N'Conclusion', 1, 5, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (5000, 200, N'Audit Objectives', 1, 5, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (8000, 200, N'Change Evolv to BCS Super', 1, 6, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (9000, 200, N'Planning, Correspondence', 1, 5, NULL, 5, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (10000, 200, N'Audit Plan', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (11000, 200, N'Conclusion', 1, 5, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (12000, 300, N'Audit Objectives', 1, 5, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (13000, 300, N'Regulatory Review', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (14000, 300, N'Trust Deed Review', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (15000, 300, N'Commercial Viability', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (16000, 300, N'Trustees Miniutes and Declaration', 1, 6, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (17000, 300, N'Investment Strategy', 1, 6, NULL, 5, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (18000, 300, N'Compliance Summary', 1, 7, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (19000, 300, N'Conclusion', 1, 5, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (20000, 400, N'Audit Objectives', 1, 6, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (21000, 400, N'Statement of FP', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (22000, 400, N'Prior Year Checks', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (23000, 400, N'General Ledger', 1, 6, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (24000, 400, N'Receivables', 1, 6, NULL, 5, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (25000, 400, N'Payables', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (26000, 400, N'Income Tax Reconcilliation', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (27000, 400, N'Accounting Policies', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (28000, 400, N'Conclusion', 1, 5, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (29000, 500, N'Audit Objectives', 1, 5, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (30000, 500, N'Opening Statement', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (31000, 500, N'Prior Year Checks', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (32000, 500, N'Changes in Market Value', 1, 6, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (33000, 500, N'Expenses', 1, 6, NULL, 5, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (34000, 500, N'Income Tax Expenses', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (35000, 500, N'Conclusion', 1, 5, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (36000, 600, N'Audit Objectives', 1, 5, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (37000, 600, N'Bank Accounts', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (38000, 600, N'Term Deposit / Fixed Interest Securities', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (39000, 600, N'Listed companies, managed investments &amp; listed &amp; unlisted (widely held) trusts investments', 1, 6, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (40000, 600, N'Unlisted companies and trusts (closely held or private related)', 1, 6, NULL, 5, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (41000, 600, N'Property', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (42000, 600, N'Collectables', 1, 6, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (43000, 600, N'Loans', 1, 6, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (44000, 600, N'Instalment Warrants', 1, 6, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (45000, 600, N'Conclusion', 1, 5, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (46000, 700, N'Audit Objectives', 1, 5, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (47000, 700, N'Interest', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (48000, 700, N'Dividends and Distributions', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (49000, 700, N'Property', 1, 6, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (50000, 700, N'Collectibles (if applicable)', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (51000, 700, N'Conclusion', 1, 6, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (54000, 800, N'Audit Objectives', 1, 6, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (55000, 800, N'Contributions', 1, 6, NULL, 2, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (56000, 800, N'In-specie Contributions refer to work done in investment movement report', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (57000, 800, N'Transfer in', 1, 6, NULL, 4, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (58000, 800, N'Transfer out', 1, 6, NULL, 5, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (59000, 800, N'Benefits paid', 1, 6, NULL, 6, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (60000, 800, N'Other benefits payments', 1, 6, NULL, 7, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (61000, 800, N'Contribution Splitting', 1, 6, NULL, 8, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (62000, 800, N'Conclusion', 1, 5, NULL, 9, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (65000, 900, N'Manager Review', 4, 20, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (75000, 1000, N'Test Summary', 5, 21, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (77000, 1000, N'General Question', 1, 6, NULL, 3, 1)
GO
INSERT [dbo].[AuditProcedureTabContent] ([ID], [AuditProcedureTabId], [Caption], [ContentType], [ControlType], [TemplateId], [Order], [IsActive]) VALUES (82000, 1100, N'Initial Audit Quries', 6, 22, NULL, 1, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1000, 1000, NULL, N'To ensure that in accepting or continuing the audit engagement the firm:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (2000, 1000, 1000, N'Will comply with the relevant ethical requirements including independence;', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (3000, 1000, 1000, N'Is notified of breaches of independence requirements;', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (4000, 1000, 1000, N'Is competent to perform the engagement and has the capabilities, time and resources to do so;', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (5000, 1000, 1000, N'Has considered the integrity of the client, and any information that would lead it to conclude that the client lacks integrity is identified: and', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (6000, 1000, 1000, N'Has determined whether the pre-conditions for an audit have been met.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (7000, 2000, NULL, N'Before accepting any new audit engagement (Newly Established Fund), consider whether the acceptance of such engagement would threaten compliance with the fundamental principles of APES 110: Code of Ethics for Professional Accountants.  List any threats below.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (8000, 2000, 7000, N'Self Review Threat to Independence (as determined by the following questions):', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (9000, 2000, 8000, N'Are the SMSF accounts prepared by our firm?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (10000, 2000, 8000, N'Is a different partner responsible for the conduct of the audit?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (11000, 2000, 8000, N'Is there pressure not to qualify the audit reports?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (12000, 2000, 7000, N'Self Interest Threat  (as determined by the following questions):', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (13000, 2000, 12000, N'Are the Trustees in a business or family relationship to the auditor?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (15000, 2000, 12000, N'Is the Trustee a significant client of the firm?  i.e. Does the sum of the client''s work make up a significant source of income to the firm?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (16000, 2000, 12000, N'Does this impact on objectivity?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (17000, 2000, 12000, N'Does the firm provide investment advice to the Trustees?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (18000, 2000, 12000, N'Is the firm''s income linked to investment performances?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (19000, 2000, 12000, N'Does the firm invest monies of clients in entities related to the firm?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (20000, 2000, 7000, N'Advocacy Threat  (as determined by the following questions):', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (21000, 2000, 20000, N'Have the Trustees adopted a strategy advocated by the firm?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (22000, 2000, 20000, N'Does this impact objectivity?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (23000, 2000, 7000, N'Intimidation Threat (as determined by the following question):', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (24000, 2000, 23000, N'Is there evidence of Trustees intimidating the auditor?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (25000, 2000, 7000, N'Familiarity Threat  (as determined by the following question):', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (26000, 2000, 25000, N'Has the same partner been the audit partner for this SMSF for over 5 consecutive years? Where audits have been performed for more than five years, staff are rotated to minimise this threat.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (27000, 2000, NULL, N'Before accepting any new audit engagement ,request the prospective client''''s permission to communicate with the existing auditor.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (28000, 2000, NULL, N'On receipt of the client''''s permission, send ethical letter to incumbent auditor to assist in identifying any information to suggest that accepting the client engagement would create a threat to compliance with the fundamental principles of APES 110, including independence.  In assessing the existence of such threats, consider also any correspondence from the ATO or other regulatory body.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (29000, 2000, NULL, N'For recurring engagements, review prior year''''s work papers, along with any correspondence from the ATO, to suggest continuing with the engagement would threaten compliance with the fundamental principles of APES 110.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (30000, 2000, NULL, N'Upon completion of the steps listed above and after evaluating any identified threat(s), determine whether appropriate safeguards are available and can be applied to eliminate the threat(s), or reduce them to an acceptable level.  List such safeguards in the comments column.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (31000, 3000, NULL, N'Competence to perform the engagement.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (31010, 3000, 31000, N'Are we competent to perform the engagement?', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (32000, 3000, 31000, N'On what basis was the above conclusion formed?', 1, 0, 1, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (33000, 3000, 32000, N'We have audited similar SMSFs to this previously', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (34000, 3000, 32000, N'We have good understanding of the SIS requirements', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (35000, 3000, 32000, N'We have good understanding of the AASBs', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (36000, 3000, 32000, N'We have good understanding of ASAs and APES''', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (37000, 3000, 32000, N'We have good list of experts to consult if we experiences issues', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (38000, 3000, 31000, N'Will the use of any outside expert(s) be required?', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (39000, 3000, NULL, N'Time and Resources to perform the engagement', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (40000, 3000, 39000, N'Do we have sufficient time to allow us to complete the audit?', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (41000, 3000, NULL, N'Compliance with ethical requirements', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (42000, 3000, 41000, N'Based on the steps taken above, based on our conclusions formed, can we comply with the ethical requirements in undertaking this engagement?', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (43000, 3000, NULL, N'Considering the integrity of the client', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (44000, 3000, 43000, N'For new engagements, review the ethical clearance letter requested', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (45000, 3000, 43000, N'Review prior year''s workpapers to identify any issues associated with the client to suggest the client may lack integrity.', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (46000, 3000, 43000, N'Review any correspondence for the ATO or other regulatory body to indicate the client may lack integrity', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (47000, 3000, NULL, N'Determining whether the pre-conditions for an audit have been met', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (48000, 3000, 47000, N'Are the financial reporting framework used to prepare the fund''s financials appropriate (eg. Special Purposes Financial Report and accounting standards & policies)', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (49000, 4000, NULL, N'After completing the above procedures, it appears:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (50000, 4000, 49000, N'We will be able to comply with the relevant ethical requirements including independence;', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (51000, 4000, 49000, N'Any breaches of independence requirements have been identified;', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (52000, 4000, 49000, N'The firm is competent to perform the engagement and has the capabilities, time and resources to do so;', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (53000, 4000, 49000, N'No information has been identified that would lead us to conclude that the client lacks integrity; and', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (54000, 4000, 49000, N'The pre-conditions for an audit have been met.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (62000, 5000, NULL, N'To plan the audit so that it will be performed in an effective manner.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (74000, 8000, NULL, N'If Evolv is auditing this fund for the first time, ensure that we have the following :', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (75000, 8000, 74000, N'Prior Year Audited Financial statements', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (76000, 8000, 74000, N'Income tax return', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (77000, 8000, 74000, N'Contravention report (if Any)', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (78000, 8000, 74000, N'Signed audit report,', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (79000, 8000, 74000, N'Correspondence.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (80000, 8000, NULL, N'Review and Check prior year OML points have been satisfactorily resolved.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (80001, 8000, NULL, N'Review and Check if prior contravention report items have been satisfactorily resolved (if applicable).', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (81000, 9000, NULL, N'To plan the audit so that it will be performed in an effective manner.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (83000, 10000, NULL, N'Audit Strategy', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (85000, 10000, 83000, N'SIS Legislation', 1, 1, 1, 6, 1, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (86000, 10000, 83000, N'SIS Regulations', 1, 1, 1, 6, 1, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (87000, 10000, 83000, N'Financial Reporting Framework', 1, 1, 1, 6, 1, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (88000, 10000, 83000, N'Applicable Accounting Standards', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (89000, 10000, 88000, N'AASB110 Events After Reporting Period', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (90000, 10000, 88000, N'AASB 112 Income Taxes', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (91000, 10000, 88000, N'AASB 1031 Materiality', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (92000, 10000, 83000, N'Applicable Accounting Policies', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (93000, 10000, 92000, N'Accrual accounting', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (94000, 10000, 92000, N'Assets valued at Net Market Values', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (95000, 10000, NULL, N'Timing of Audit', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (96000, 10000, 95000, N'The timing of the audit is dependent upon when all information is sent to us.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (97000, 10000, NULL, N'Materiality', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (98000, 10000, 97000, N'Financial', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (99000, 10000, 98000, N'After assessing the fraud and risk procedures above ,  a quantatitive materiality level of 5% of the funds total nett assets (as listed in the materiality calculator below) will be applied together with the auditors professional judgement  for further audit procedures and selection strategies.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (100000, 10000, 98000, N'Materiality CALCULATOR', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (101000, 10000, 98000, N'All material errors will be communicated to the trustees.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (102000, 10000, 97000, N'Compliance', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (103000, 10000, 102000, N'The trustees will be advised of all contraventions, the ATO will be advised all reportable contraventions in accordance with their reporting cr', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (104000, 10000, NULL, N'Electronic Information / Data Controls', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (105000, 10000, 104000, N'A mainstream superfund accounting package is used with no modification being made to the core functionalities.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (106000, 10000, 104000, N'All internet banking access, computers, servers and software is logged in using a username and password, therefore, sufficient security controls are in place to ensure authorisation, retention and protection of information.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (107000, 10000, 104000, N'Full work papers are prepared, and the file reviewed by manager and partner, therefore sufficient data processing controls are in place over data input ensuring completeness, accuracy and validity.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (108000, 10000, 104000, N'Policies and procedures are in place to manage daily IT operations and implementation of computer software / hardware including controls over staff who prepares the scanned audit file for Evolv to audit.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (109000, 10000, 104000, N'Policies and procedures are in place to manage daily IT operations and implementation of computer software / hardware.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (110000, 10000, 104000, N'Conclusion: Sufficient electronic information and data controls are in place and the risk is low.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (111000, 10000, NULL, N'Internal Controls', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (112000, 10000, 111000, N'We have assessed the fund''s control environment and compliance framework as ineffective as there is no proper segregation of duties.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (113000, 10000, 111000, N'As the administration of the fund is outsourced to an accountant / administrator, we have also  enquired of their information system controls and related business processes relevant to the production of the financial reports. We have assessed this control environment as not being of a high risk.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (114000, 10000, 111000, N'Conclusion: We are unable to rely on the effectiveness of the fund''s internal controls and adopt a substantive testing approach.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (115000, 10000, NULL, N'Risk Assessment Procedures', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (116000, 10000, 115000, N'We have based our risk assessment for the financial audit on identifying and assessing risks at the financial report level and at the assertion level for classes of transactions, account balances and disclosures.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (117000, 10000, 115000, N'The following are the main risk areas and audit assertions which will be considered:', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (118000, 10000, 117000, N'Investments and other assets', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (119000, 10000, 118000, N'The stated assets may not exist (existence risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (120000, 10000, 118000, N'The stated assets may not relate to the fund (rights & obligations risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (121000, 10000, 118000, N'The assets are materially over or understated (valuation risk).', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (122000, 10000, 118000, N'The assets are not properly disclosed (disclosure risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (123000, 10000, 117000, N'Liabilities and member benefits', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (124000, 10000, 123000, N'Liabilities may be materially understated (completeness risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (125000, 10000, 123000, N'There may exist unrecorded liabilities (completeness risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (126000, 10000, 123000, N'Member benefits are materially over or understated (measurem', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (127000, 10000, 123000, N'Member benefits may not be appropriately classified (rights & obligations risk, disclosure risk)', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (128000, 10000, 117000, N'Contributions and other income', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (129000, 10000, 128000, N'All contributions and other income may not be received by the SMSF (completeness risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (130000, 10000, 128000, N'Contributions and other income may be materially misstated (measurement risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (131000, 10000, 128000, N'Contributions received may not be allocated to appropriate member &/or benefit category (disclosure risk)', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (132000, 10000, 117000, N'Benefit payments and expenses', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (133000, 10000, 132000, N'Stated expenses do not relate to the SMSF (occurrence risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (134000, 10000, 132000, N'Expenses may be materially misstated (measurement risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (135000, 10000, 132000, N'Benefits paid may be materially miscalculated (measurement risk).', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (136000, 10000, 117000, N'Compliance with SIS Act', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (137000, 10000, 136000, N'The SMSF’s operations may have contravened the requirements of the SIS Act (compliance risk).', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (138000, 10000, 115000, N'The following procedures have also been undertaken in assessing risk:', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (139000, 10000, 138000, N'Make enquiries of trustees as to the following:', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (140000, 10000, 139000, N'1. Their assessment of the risk that the financial report may be materially misstated due to fraud, including the nature, extent and frequency of such assessments (ASA 240.17(a));', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (141000, 10000, 139000, N'2. Their process for identifying and responding to the risks of fraud, including any specific risks of fraud that trustee(s) has identified or that have been brought to their attention, or classes of transactions, account balances, or disclosures for which a risk of fraud is likely to exist (ASA 240.17(b));', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (142000, 10000, 139000, N'3. Whether they have knowledge of any actual, suspected or alleged fraud affecting the SMSF (ASA 240.18);', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (143000, 10000, 139000, N'4. Whether the entity is in compliance with relevant laws and regulations (ASA 250.14(a));', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (144000, 10000, 139000, N'5. The identity of the entity’s related parties, including changes from the prior period, the nature of the relationships between the entity and these related parties, and Whether the entity entered into any transactions with these related parties during the period and, if so, the type and purpose of the transactions (ASA 550.13);', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (145000, 10000, 139000, N'6. Obtain an understanding of the controls, if any, established Identify, account for, and disclose related party relationships and transactions, authorise and approve significant transactions and arrangements with related parties, and Authorise and approve significant transactions and arrangements outside the normal course of business (ASA 550.14);', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (146000, 10000, 139000, N'7. Whether any subsequent events have occurred which might affect the financial report (ASA 560.7(b));', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (147000, 10000, 139000, N'8.  Their knowledge of events or conditions beyond the period of trustee(s) assessment that may cast significant doubt on the SMSF’s ability to continue as a going concern (ASA 570.15);', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (148000, 10000, 115000, N'Conclusion: The above items have been considered partly in the trustees signed representation letter and no further work is deemed necessary.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (149000, 10000, 115000, N'Analytical Procedures', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (150000, 10000, 149000, N'Analytical procedure reviews are performed throughout the audit program. These procedures together with our risk assessment above provide corroborative evidence to justify a sample selection of approximately 30%.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (151000, 10000, 149000, N'In addition, the following specific items, represent areas of  risk. Determine if any of the following items are applicable:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (152000, 10000, 151000, N'In-house assets', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (153000, 10000, 151000, N'Pre-99 and/or Post 99 Related Units Trust', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (154000, 10000, 151000, N'Related Party transactions', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (155000, 10000, 151000, N'First year funds', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (156000, 10000, 151000, N'Breach in previous year', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (157000, 10000, 151000, N'Unusual investments (eg. overseas assets)', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (158000, 10000, 151000, N'Investment in related and closely held entities', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (159000, 10000, 151000, N'Borrowings / Instalment Warrants', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (160000, 10000, 151000, N'members non resident', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (161000, 10000, 151000, N'Carry on business of selling goods or services', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (162000, 10000, 151000, N'Segregated assets', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (163000, 10000, 149000, N'If you answered yes to any of the above,  perform additional testing on each item.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (164000, 10000, 115000, N'Observation & Inspection', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (165000, 10000, 164000, N'The auditor is to makes observations and inspect all relevant documentation necessary to assess risk.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (166000, 10000, NULL, N'Staff Allocation', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (167000, 10000, 166000, N'We have determined that for any high risk areas that we are competent to perform the audit. Appropriately qualified staff will be assigned to a fund which is identified as having a high risk and an audit manager will be responsible for the supervision of these high risk areas.', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (168000, 10000, 166000, N'Any fund falling outside the following parameters or that is classified a risking fund as per the risk calculator will automatically be delegated to a senior auditor over a junior auditor:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (169000, 10000, 168000, N'''Value of fund under $2M', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (170000, 10000, 168000, N'Type of investments: Cash, Shares in listed companies, managed funds, wrap accounts, real-estate property.', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (171000, 10000, 168000, N'Accumulation or pension (2nd year of operations)', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (172000, 10000, 168000, N'Not the first year of audit by Evolv', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (173000, 10000, NULL, N'Fraud', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (173010, 10000, 173000, N'Audit Objective:', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (174000, 10000, 173010, N'To identify and assess the risks of material misstatement of the financial report due to fraud;', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (175000, 10000, 173010, N'To obtain sufficient appropriate audit evidence regarding the assessed risks of material misstatement due to fraud, through designing and implementing appropriate responses; and', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (176000, 10000, 173010, N'To respond appropriately to fraud or suspected fraud identified during the audit.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (177000, 10000, 173000, N'When planning for the audit of the SMSF, if there is more than 1 staff member involved with the audit, discuss amongst each other how and where the SMSFs financial report, and compliance with the SIS requirements, may be susceptible to material misstatement/non-compliant due to fraud.', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (178000, 10000, 173000, N'Make enquiries of the SMSF trustees regarding:', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (179000, 10000, 178000, N'The trustee(s) assessment of the risk that the financial report may be materially misstated due to fraud, and in contravention of the SIS requirements, including the nature, extent and frequency of such assessments;', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (180000, 10000, 178000, N'Trustee(s) process for identifying and responding to the risks of fraud, including any specific risks of fraud that trustee(s) have identified or that have been brought to their attention, or classes of transactions, account balances, or disclosures for which a risk of fraud is likely to exist;', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (181000, 10000, 178000, N'Their knowledge of any actual, suspected or alleged fraud affecting the entity.', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (182000, 10000, 178000, N'If considered appropriate, send the trustees a fraud questionnaire to complete.', 1, 0, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (183000, 10000, 173000, N'Note: Where responses to enquiries of trustees are inconsistent, investigate all inconsistencies.', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (184000, 10000, 173000, N'When performing analytical procedures, assess both risk assessment procedures and  evaluate whether unusual or unexpected relationships that have been identified, including those related to revenue accounts, may indicate risks of material misstatement due to fraud.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (185000, 10000, 173000, N'Refer to risk analysis and consider whether other information obtained by the auditor indicates risks of material misstatement due to fraud.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (186000, 10000, 173000, N'Upon completing the above fraud risk procedures, should any fraud risk factors be identified, assess the risks of material misstatement/non-compliance with SIS requirements, due to fraud at the financial report level, and at the assertion level for classes of transactions, account balances and disclosures.  (Not applicable if no fraud risk factors identified above).', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (187000, 10000, 173000, N'Should any risks of fraud be identified, ensure the corresponding audit procedures, including their nature, timing and extent are responsive to the assessed risks of material misstatement due to fraud at the assertion and/or SIS level.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (188000, 10000, 173000, N'When performing analytical procedures near the end of the audit, when forming an overall conclusion as to whether the financial report is consistent with the our understanding of the fund, evaluate whether the procedures indicate a previously unrecognised risk of material misstatement due to fraud.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (189000, 10000, 173000, N'For any identified misstatements, or SIS contraventions, evaluate whether they are indicative of fraud. If there is such an indication, evaluate the implications in relation to other aspects of the audit, particularly the reliability of trustee representations, recognising that an instance of fraud is unlikely to be an isolated occurrence.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (190000, 10000, 173000, N'If, as a result of a misstatement resulting from fraud or suspected fraud, exceptional circumstances arise that bring into question our ability to continue performing the audit:', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (191000, 10000, 190000, N'Determine the professional and legal responsibilities applicable in the circumstances, including whether there is a requirement to report to the trustee appointment or to the ATO;', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (192000, 10000, 190000, N'Consider whether it is appropriate to withdraw from the engagement, where withdrawal is possible under applicable law or regulation; and if we withdraw from the engagement:', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (193000, 10000, 190000, N'Discuss with the trustee(s) our withdrawal from the engagement and the reasons for the withdrawal; and', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (194000, 10000, 190000, N'Determine whether there is a professional or legal requirement to report to the trustee(s) or the ATO, our withdrawal from the engagement and the reasons for the withdrawal.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (195000, 10000, 173000, N'Conclusion', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (196000, 10000, 195000, N'Where applicable, risks of material misstatement of the financial report, and SIS level, due to fraud have been identified, assessed and evaluated along with the appropriate further audit procedures in response to such risks;', 0, 1, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (197000, 10000, 195000, N'Sufficient appropriate audit evidence regarding the assessed risks of material misstatement due to fraud has been obtained; and', 0, 1, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (198000, 10000, 195000, N'We have responded appropriately to any identified fraud or suspected fraud identified during the audit.', 0, 1, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (200000, 11000, NULL, N'After completing the above procedures, we believe the audit has been appropriately planned so that it will be performed in an effective manner', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (208100, 12000, NULL, N'To ensure the fund complies with:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (208200, 12000, 208100, N'all relevant sections of SISA & SISR', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (208300, 12000, 208100, N'the trust deed', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (208400, 12000, 208100, N'the investment strategy', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (209000, 13000, NULL, N'Do we have a signed Engagement letter', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (210000, 13000, NULL, N'Do we have a signed Representation Letter?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (214000, 14000, NULL, N'Trustees, Members and Fund Compliance', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (215000, 14000, 214000, N'Name of Trustee:', 0, 0, 0, 5, 2, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (216000, 14000, 214000, N'Record date deed of establishment or last amendment', 0, 0, 0, 5, 2, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (217000, 14000, 214000, N'Trust deed provider', 0, 0, 0, 5, 2, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (218000, 14000, 214000, N'Name of Members 1:', 0, 0, 0, 5, 2, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (219000, 14000, 214000, N'Name of Members 2:', 0, 0, 0, 5, 2, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (220000, 14000, 214000, N'Name of Members 3:', 0, 0, 0, 5, 2, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (221000, 14000, 214000, N'Name of Members 4:', 0, 0, 0, 5, 2, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (222000, 14000, 214000, N'If corporate trustee, review ASIC company statement & confirm all directors are members of the fund. Annotate any discrepancies. Hyper Link:https://connectonline.asic.gov.au/  Hyper Link:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (223000, 14000, 214000, N'If individual trustees, confirm all individuals are members of the fund. Annotate any discrepancies.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (224000, 14000, 214000, N'Check  the trustees / Directors are not disqualified persons.Hyper Link: http://www.search.asic.gov.au/cgi-bin/grb040c?FORMID=a  Hyper Link:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (225000, 14000, 214000, N'Check that the fund is complying on the SuperFundLook website Hyper Link: http://superfundlookup.gov.au/  Hyper Link:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (226000, 14000, NULL, N'Ensure that we have the following documents for setting up a SMSF including ensuring we have copies for new members added to the fund during the year:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (227000, 14000, 226000, N'Copy of the consent to act as trustee of the fund and declaration that the trustee is not a disqualified person (Form no. NAT 71089).', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (228000, 14000, 226000, N'Copy of the application for membership of the fund.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (229000, 14000, 226000, N'Copy of the binding death nominations if any', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (230000, 14000, 226000, N'Documents relating to appointment of trustees and members.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (231000, 14000, NULL, N'If any members under a legal disability ie under the age of 18 yrs of review parent / guardian consent', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (232000, 14000, 231000, N'Establishment and amendment', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (233000, 14000, 231000, N'Ensure that it has been properly executed, signed by all trustees & witnessed', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (234000, 14000, 231000, N'Ensure the rules incorporate SISA, SISR and applicable taxation rules', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (235000, 14000, 231000, N'Ensure it notes the core and ancillary purposes of the fund', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (236000, 14000, 231000, N'Ensure it notes an irrevocable election to become a regulated fund or subject to SISA or SISR', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (237000, 14000, 231000, N'Ensure it contains a clause which deems the appropriate legislation into or out of the deed to allow the fund to remain complying', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (238000, 14000, NULL, N'Contributions', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (239000, 14000, 238000, N'Ensure it allows for all types of contributions received by the fund', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (240000, 14000, 238000, N'Are co contributions or contribution splitting allowed?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (241000, 14000, 238000, N'Are in specie contributions of assets made during the year allowed', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (242000, 14000, 238000, N'Can excess contributions be rejected?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (243000, 14000, 238000, N'Can excess contributions tax levied on a member be by paid by the fund?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (244000, 14000, NULL, N'Benefit payments', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (245000, 14000, 244000, N'Is compulsory cashing of members balances at a specific age required?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (246000, 14000, 244000, N'Is a lump sum benefit required to be paid in lieu of a pension?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (247000, 14000, 244000, N'Ensure it allows for all types of pensions paid by the fund, or segregation of assets', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (248000, 14000, 244000, N'Ensure it allows for commutation of pensions', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (249000, 14000, NULL, N'Borrowings', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (250000, 14000, 249000, N'Does it permit temporary borrowings in specific circumstances and instalment warrant arrangements?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (251000, 14000, NULL, N'Investments', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (252000, 14000, 251000, N'Does it specify what assets the fund may or may not invest in?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (253000, 14000, 251000, N'Ensure it requires an investment strategy to be formulated and given effect to?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (254000, 14000, NULL, N'Financial Records', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (255000, 14000, 254000, N'Does it require the preparation of an annual financial report and audit?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (256000, 14000, 254000, N'Ensure minutes and records are required to be kept for at least 10 years, and financial reports and tax returns for at least 5 years?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (257000, 15000, NULL, N'Are the value of the assets of the fund under $140,000, if yes, insert OML point?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (258000, 16000, NULL, N'Review the minutes of meetings held during the year and ensure that they are signed by all Directors / Trustees or solely by the chairperson. (SIS Reg. 4.03(3)).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (259000, 16000, NULL, N'Sight signed copy of the trustees declaration. Ensure that all individual trustees or directors of trustee company have signed accordingly.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (261000, 17000, NULL, N'Ensure that the investments align with the Investment Strategy by checking the analytical review in holdings section', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (262000, 17000, NULL, N'Have the Trustees documented and given effect to an investment strategy for the Fund that has regard to the following:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (263000, 17000, 262000, N'The risk in making, holding and realising assets;', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (264000, 17000, 262000, N'Likely return from investments;', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (265000, 17000, 262000, N'Liquidity of investments in comparison to expected Cash flow requirements of the fund; and', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (266000, 17000, 262000, N'Investment composition as a whole including diversification or risks of inadequate diversification', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (267000, 17000, 262000, N'Insurance requirements', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (268000, 18000, NULL, N'Establishment and operation of the SMSF', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (269000, 18000, 268000, N'Meets the definition of a SMSF.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (270000, 18000, 268000, N'Trustees are not disqualified persons.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (271000, 18000, 268000, N'Maintains minutes and records for specified time periods.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (272000, 18000, 268000, N'Maintains trustees '' declarations regarding duties for those who become trustees for the first time after 30 June 2007.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (273000, 18000, 268000, N'Proper accounting records kept and retained for 5 years.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (274000, 18000, 268000, N'Annual financial report prepared, signed and retained for 5 years.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (275000, 18000, 268000, N'Trustee provides auditor documents within 14 days of request (14 day letter).', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (276000, 18000, 268000, N'Prohibition on entering contracts restricting trustees '' functions and powers.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (277000, 18000, 268000, N'Trustees formulate and give effect to an investment strategy.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (278000, 18000, NULL, N'Sole purpose', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (279000, 18000, 278000, N'Established for the sole purpose of funding a member ''s benefits for retirement, attainment of a certain age, death, ill-health or termination.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (280000, 18000, NULL, N'Investment restrictions', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (281000, 18000, 280000, N'Restrictions on acquiring or holding ''in-house'' assets.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (282000, 18000, 280000, N'Restrictions on acquisitions of assets from related parties.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (283000, 18000, 280000, N'Maintains arm ''s length investments.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (284000, 18000, 280000, N'Maintains SMSF money and other assets separate from those of the trustees, employer-sponsors and other related parties.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (285000, 18000, 280000, N'Prohibition on lending or providing financial assistance to member or relative.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (286000, 18000, 280000, N'Restrictions on borrowings.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (287000, 18000, 280000, N'Prohibition on charges over SMSF assets.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (288000, 18000, NULL, N'Benefits Trustees', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (289000, 18000, 288000, N'Trustees maintain members '' minimum benefits.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (290000, 18000, 288000, N'Minimum pension amount to be paid annually.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (291000, 18000, 288000, N'Restrictions on payment of benefits.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (292000, 18000, 288000, N'Prohibition on assignment of members '' superannuation interest.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (293000, 18000, 288000, N'Prohibition on creating charges over members '' benefits.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (294000, 18000, NULL, N'Contributions restrictions', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (295000, 18000, 294000, N'Accepts contributions within specified restrictions.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (296000, 18000, NULL, N'Reserves / Investment Return Allocation', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (297000, 18000, 296000, N'Reserves to be used appropriately and investment returns must be allocated to members '' accounts in a manner that is fair and reasonable.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (298000, 18000, NULL, N'Solvency', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (299000, 18000, 298000, N'Unsatisfactory financial position.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (300000, 18000, NULL, N'Other regulatory information', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (301000, 18000, 300000, N'Information regarding the SMSF or trustees which may assist the ATO, including compliance with other relevant SISA sections and SISR regulations.', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (302000, 19000, NULL, N'The fund complies with all relevant sections of SISA & SISR to the extent required in the audit report, the trust deed and the investment strategy.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (320000, 20000, NULL, N'Ensure that the additions in the financial report are correct.', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (321000, 22000, NULL, N'Do the prior year closing balances agree with current year opening balances?', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (322000, 23000, NULL, N'Review the General Ledger for appropriateness of material or abnormal transactions recorded in the general ledger and other adjustments made in the preparation of the financial report.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (324000, 24000, NULL, N'Review Receivables balance ensuring:', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (325000, 24000, 324000, N'Receivables owing from prior year have been received.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (326000, 24000, 324000, N'Where material, consider recoverability of the amount and agree amounts subsequently received or reinvested to relevant documentation or bank statement.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (327000, 24000, 324000, N'Ensure that loans have not been advanced to a member of the fund or a relative of the member. If so, consider the following sections of SIS Act: s62, s65, s83 & s109.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (328000, 24000, 324000, N'Ensure that contributions are not recorded as a receivable', 1, 1, 1, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (330000, 25000, NULL, N'Review Payables ensuring:', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (331000, 25000, 330000, N'Creditors owing from prior year have been paid.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (332000, 25000, 330000, N'Where material, consider authenticity of creditor / accrual and agree amounts subsequently paid to relevant documentation or bank statement.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (333000, 25000, 330000, N'Member benefits have not been accrued (other than in accordance with the ATO criteria permitted regarding pension shortfall eg honest administration error, error amount was small - not greater than 1/12th of the annual payment, and paid as soon as practical after year end).', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (334000, 25000, 330000, N'The fund has no borrowings other than permitted by s67(4a).', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (335000, 25000, 330000, N'Review BAS returns lodged during the year for reasonableness.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (336000, 26000, NULL, N'Ensure all referenced and reconciled amounts agree to relevant reports.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (337000, 26000, NULL, N'Agree Income Tax instalments paid to ATO Running Balance Account / Tax Portal.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (338000, 26000, NULL, N'Where non arm''s income is identified as per the income procedures, ensure it is taxed at maximum rates.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (339000, 26000, NULL, N'Agree exempt pension income to actuarial certificate where applicable or segregated assets and income schedule.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (340000, 26000, NULL, N'Complete or Review exempt pension calculator and determine if exempt pension income is reasonable.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (340100, 26000, NULL, N'Tax Rec Exempt Pension Income (Gross Method) Calculator', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (340200, 26000, NULL, N'Tax Rec Exempt Pension Income (Net Method) Calculator', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (343000, 26000, NULL, N'Complete or Review Deferred Tax calculator and determine if un-realised capital gains / losses are recorded as reasonable.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (343100, 26000, NULL, N'Deffered Tax Calculator', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (345000, 27000, NULL, N'Review and gain an understanding of the accounting policies adopted by the trustees of the fund as stated in Note 1 of the financial statements ensuring the following is stated:', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (346000, 27000, 345000, N'Financial statements are either special purpose accounts or the fund is a non reporting entity.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (347000, 27000, 345000, N'How the assets are valued.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (348000, 27000, 345000, N'Whether cash or accruals base accounting has been adopted.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (349000, 27000, 345000, N'Whether deferred tax has been adopted.', 1, 1, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (350000, 27000, NULL, N'Where polices adopted are not in line with financial accounts prepared, notify the trustees accordingly.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (351000, 28000, NULL, N'The statement of financial position is fairly stated', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (359000, 29000, NULL, N'To ensure that investments and liabilities', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (360000, 29000, 359000, N'Have occurred and pertain to the SMSF', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (361000, 29000, 359000, N'Are completely recorded', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (362000, 29000, 359000, N'Have been accurately recorded', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (363000, 29000, 359000, N'Have been recorded in the correct period.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (364000, 29000, 359000, N'Have been recorded in the proper accounts', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (365000, 30000, NULL, N'Check Additions in the financial report are correct.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (365100, 31000, NULL, N'Do the prior year closing balances agree with current year opening balances?', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (366000, 32000, NULL, N'Unrealised Change in market value', 1, 0, 1, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (367000, 32000, 366000, N'Agree change in market value to the change in market value calculator (complete if necessary)', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (368000, 33000, NULL, N'Agree large or unusual expenses to supporting documentation', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (369000, 33000, NULL, N'Review a sample of expenses to ensure that they have been paid from the fund''s bank account?', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (370000, 33000, NULL, N'Agree the amount paid to the insurance policy document and/or invoice', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (371000, 33000, NULL, N'Review the insurance policy to check title of the policy owner - ensure that a tax deduction has not been claimed if the trustee is not the policy owner', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (372000, 34000, NULL, N'Agree Income Tax Expense to the Income Tax Expense calculator (complete if necessary)', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (373000, 35000, NULL, N'We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that expenses are fairly stated.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (385000, 36000, NULL, N'To ensure that additions and disposals are', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (386000, 36000, 385000, N'a. Complete', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (387000, 36000, 385000, N'b. Properly valued', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (388000, 36000, 385000, N'c. Ownership is correct', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (390000, 37000, NULL, N'Agree bank account statement balance and title as at 30 June to the financial statements.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (391000, 37000, NULL, N'Review bank statements for large & abnormal transactions, overdrawn balance.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (392000, 37000, NULL, N'Review subsequent period bank statements for large or unusual items', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (393000, 37000, NULL, N'Where the bank balance represents more than 50% of the value of the fund, obtain direct confirmation from financial institution either by completing bank confirmation audit request or obtain direct correspondence from the financial institution.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (394000, 38000, NULL, N'Agree term deposit statement balance  and title at 30 June', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (395000, 38000, NULL, N'Obtain and review post 30 June maturity notices', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (396000, 38000, NULL, N'Where term deposit balance represents more than 50% of the value of the fund, obtain direct confirmation from financial institution either by completing bank confirmation audit request or obtain direct correspondence from the financial institution.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (397000, 39000, NULL, N'Holdings', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (398000, 39000, NULL, N'Non-managed share holdings and/or Broker managed', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (399000, 39000, 398000, N'Where holdings have been checked against automatic data feeds ensure that no discrepancies are highlighted.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (400000, 39000, 398000, N'Where holdings have not been automatically verified, test ownership by agreeing the title and agree the number of securities held as at 30 June  to either year end holding statements or via the following (obtain HIN & postcode from dividend notices):(Refer to samples selected above in the investment holding report):', 1, 1, 1, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (404000, 39000, 398000, N'Confirm the closing market price of the securities at 30 June to CRS booklet or Yahoo finance and cross check calculations', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (405000, 39000, NULL, N'Managed Investments / Wrap Accounts', 1, 0, 1, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (406000, 39000, 405000, N'Review managed portfolio valuations report as at year end for completeness.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (407000, 39000, 405000, N'Obtain Independent External Auditor''s report of the fund manager.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (408000, 39000, NULL, N'Movement', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (409000, 39000, 408000, N'Select a sample of additions and disposals and agree to buy contract notes, brokers’ transaction statements, portfolio valuation statement, share transfer forms and bank statements.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (410000, 39000, 408000, N'Check the calculation of the gains / losses: Sale price - purchase price = profit / loss agreed to Investment disposals report (if applicable).', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (411000, 39000, NULL, N'Off-market Transfers', 1, 0, 1, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (412000, 39000, 411000, N'If there are any off Market Transfers, agree to off market transfer form and minutes of meeting.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (413000, 39000, 411000, N'Agree sale prices to Hyper Link: http://yahoo.com.au/finance  Hyper Link: or Hyper Link:http://tradingroom.com.au  Hyper Link: to ensure they recorded at market value.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (414000, 39000, 411000, N'If off-market transfer is done in-specie, ensure the trust deed allows this.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (415000, 39000, 411000, N'Check the calculation of the gains / losses: Sale price - purchase price = profit agreed to Investment disposals report (if applicable).', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (416000, 40000, NULL, N'Movement', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (417000, 40000, 416000, N'Select a sample of additions and disposals and agree to share / unit transfer forms, minutes of meetings, valuation, bank statements and other supporting documents', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (418000, 40000, 416000, N'Check the calculation of the gains / losses: Sale price - purchase price = profit / loss agreed to Investment disposals report (if applicable)', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (419000, 40000, 416000, N'Where the new or increase in a existing investment has been made in a closely held company or trust, ensure that the member/trustees and their Part 8 Associates do not collectively have control of the company / trust by majority shareholding/unit holding and/or voting rights).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (420000, 40000, 416000, N'If yes, ensure that the investment complies with the in-house asset provisions of the SIS Act (sections 71 - 85).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (421000, 40000, 416000, N'If acquired from related party, ensure section 66 of the SIS Act permits the acquisition', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (422000, 40000, NULL, N'Holdings', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (423000, 40000, 422000, N'Investment in unlisted companies / trusts / partnerships (un-related)', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (424000, 40000, 423000, N'Select a sample and sight original share / unit certificates and agree the number of shares / units held and the title correctly recorded as at 30 June.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (425000, 40000, 423000, N'Obtain a copy of the financial report and income tax return of the company / trust for closely held investments.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (426000, 40000, 423000, N'Ensure the investment is appropriately valued and in accordance with accounting polices adopted.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (427000, 40000, 422000, N'Unlisted Related Company/Trust - pre 11 August 1999', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (428000, 40000, 427000, N'Obtain a signed copy of the unit trust deed to confirm that the date of establishment is prior to 11 August 1999.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (429000, 40000, 427000, N'Sight original share / unit certificates and agree the number of shares /units and title is correctly held at 30 June.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (430000, 40000, 427000, N'Obtain a copy of the financial report and Income tax return of the company / trust. Ensure director''s / trustee''s declarations is signed.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (431000, 40000, 427000, N'Ensure that the value of the shares / units of ...........Pty ltd / unit trust recorded in the financial statements of fund agree with the net asset per unit recorded in the financial statements of the  ...........Pty ltd / unit trust.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (432000, 40000, 431000, N'If the fund made additional investments in the unlisted unit trust during the year either by way of reinvestment of earnings or an additional investment ensure that the additional investments comply with section 71D and 71E as follows:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (433000, 40000, 432000, N's.71D Distribution Re-investment - The total additional investment in the trust between 11/8/99 and 30/6/09 do not exceed the superfund distribution entitlements between the same dates.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (434000, 40000, 432000, N's.71E Debt Election - If the trustee has made a written election by 22/10/00 no other exceptions apply. If not, the total additional investments in the trust between 11/8/99 and 30/6/09 do not exceed the debt level in the trust as at 11/8/99 (excluding loans from the superfund).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (435000, 40000, 432000, N'c. Post 01/07/2009 - Any additional investment made by the fund into the pre 11 August 1999 trust, will be classed as an in-house asset under s.82.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (436000, 40000, 427000, N'If the pre 11 August 1999 is converted into a trust under Reg 13.22C, any additional investments made by the fund in the trust will be excluded as an in-house asset if the following are met:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (437000, 40000, 436000, N'That the trust has not borrowed any further, conducted any new business or acquired an interest in any other entity post 28/6/2000', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (438000, 40000, 436000, N'Does not have any borrowings or loans.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (439000, 40000, 436000, N'Does not have any investments in any other entity (including the employer sponsor of the fund or an associate of either of them).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (440000, 40000, 436000, N'Does not have a charge over any assets.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (441000, 40000, 436000, N'Has not acquired any assets from a related party of the fund (except business real property).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (442000, 40000, 436000, N'Has not entered into a lease agreement with a related party of the fund (except where the lease relates to a business real property).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (443000, 40000, 436000, N'Has not entered into a loan or other form of financial assistance to a related party of the fund.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (444000, 40000, 436000, N'Does not conduct a business.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (445000, 40000, 436000, N'Does not enter into any transaction other than on an arm''s length basis.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (446000, 40000, 422000, N'That the trust has any loans with related parties or members, ensure the interest rate is charged at arm''s length (refer to ATO benchmark interest rates).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (447000, 40000, NULL, N'Unlisted Related Company/Trust - post 11 August 1999', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (448000, 40000, 447000, N'If the investment is in an unlisted related company, ensure that the said investment and the investments in other related assets together do not constitute more than 5% of the total assets of the super fund.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (449000, 40000, 447000, N'Sight original share / unit certificates and agree the number of shares /units and title is correctly held at 30 June.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (450000, 40000, 447000, N'Obtain a copy of the financial report and Income tax return of the company / trust. Ensure director''s / trustee''s declarations is signed and the company / trust does not do the following:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (451000, 40000, 450000, N'Does not have any borrowings or loans.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (452000, 40000, 450000, N'Does not have any investments in any other entity (including the employer sponsor of the fund or an associate of either of them).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (453000, 40000, 450000, N'Does not have a charge over any assets.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (454000, 40000, 450000, N'Has not acquired any assets from a related party of the fund (except business real property).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (455000, 40000, 450000, N'Has not entered into a lease agreement with a related party of the fund (except where the lease relates to a business real property).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (456000, 40000, 450000, N'Has not entered into a loan or other form of financial assistance to a related party of the fund.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (457000, 40000, 450000, N'Does not conduct a business.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (458000, 40000, 450000, N'Does not enter into any transaction other than on an arm''s length basis.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (459000, 40000, 447000, N'If the related unit trust did not comply with any of the above the fund''s whole investment in the trust breaches section 82, thereby perpetually remaining as in-house asset of the fund. The trustees should be notified of this breach as the trust must be wound up.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (460000, 40000, 447000, N'Ensure that the value of the shares / units of ...........Pty ltd / unit trust recorded in the financial statements of fund agree withy the net asset per unit recorded in the financial statements of the  ...........Pty ltd / unit trust.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (461000, 40000, 447000, N'Verify material underlying items appearing in the financial statements of the company / trust per normal audit procedures in this program.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (462000, 41000, NULL, N'Holdings', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (463000, 41000, 462000, N'Perform a title search on the property if not provided. Ensure that the  title of the real estate property is in the name of the trustees and perform the following:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (464000, 41000, 463000, N'Agree property details on the title search (i.e. Lot number and Plan number) to the property details recorded in the financial statements.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (465000, 41000, 463000, N'Confirm fund ownership by sighting either a purchase contract, deed of trust, or caveat acknowledging the trustees.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (466000, 41000, 463000, N'Where fund owns less than 100%, ensure the title of the property is held as tenants in common, not as joint tenants..', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (467000, 41000, 462000, N'Ensure that there are no encumbrances over the title.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (468000, 41000, 462000, N'Ensure annual valuation of the property is based on objective and supportive data. Eg appraisal or a minute by the trustees (minute must state value of the property and how the value was determined).', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (469000, 41000, 462000, N'Agree valuation to the financial statements and is consistant with the accounting policies.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (470000, 41000, 462000, N'Where applicable, review depreciation rates to determine their appropriateness and consistency with prior years.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (471000, 41000, NULL, N'Movement', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (472000, 41000, 471000, N'Agree additions and disposals to supporting documentation including purchase and Sale contracts and solicitors documentation and settlement sheet.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (473000, 41000, 471000, N'If the real estate property acquired from a related party of the fund, ensure the acquisition is permitted in accordance with section 66 of the SIS Act ie it is business real property and acquired on an arm''s length basis.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (474000, 41000, 471000, N'Check the calculation of the gains / losses: Sale price - purchase price = profit / loss agreed to Investment disposals report (if applicable)', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (475000, 42000, NULL, N'Collectables and personal use assets (Reg13.18AA) includes artwork, jewellery, antiques, artefacts, coins, medallion, bank notes, postage stamps, folio, manuscripts, books, memorabilia, wine, spirits, motor vehicles, recreational boats, memberships of sporting or social clubs,', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (476000, 42000, 475000, N'Holdings', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (477000, 42000, 476000, N'Confirm the following:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (478000, 42000, 476000, N'Fund has legal ownership of the collectable', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (479000, 42000, 476000, N'Where material, ensure the collectable exists', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (480000, 42000, 476000, N'Confirm the location of the collectable and ensure the collectable is not made available for personal use or enjoyment', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (481000, 42000, 476000, N'Ensure value of the collectable is based on objective and supportive data', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (482000, 42000, 476000, N'Ensure collectable is adequately insured', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (483000, 42000, NULL, N'Movement', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (484000, 42000, 483000, N'For acquisitions, they must comply with all of the following:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (485000, 42000, 484000, N'Asset must not be leased to a related party', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (486000, 42000, 484000, N'Item must not be stored in private residence of a related party', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (487000, 42000, 484000, N'Decision on storage of item must be documented', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (488000, 42000, 484000, N'Item must be insured in fund''s name within 7 days of acquiition', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (489000, 42000, 484000, N'Item must not be used by related party', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (490000, 42000, NULL, N'For disposals:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (491000, 42000, 490000, N'Was the collectable disposed of to a related party?', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (492000, 42000, 490000, N'If yes and acquired before 1 July 2011, was it based on objective and supportive data?', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (493000, 42000, 490000, N'If yes and acquired after 1 July 2011, was an independent valuation obtained?', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (494000, 43000, NULL, N'Holdings', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (495000, 43000, 494000, N'Obtain loan agreement and review the following:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (496000, 43000, 495000, N'Review reasonableness of interest rate (refer to ATO benchmark interest rates) and  loan term.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (497000, 43000, 495000, N'Perform review of the loan to identify if loan has been advanced to related party or third party', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (498000, 43000, 494000, N'If the loan has been made to a related party ensure the following:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (499000, 43000, 498000, N'The loan does not exceed 5% of the value of the fund.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (500000, 43000, 498000, N'The loan is not advanced to member of the fund or a relative of the member.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (501000, 43000, 498000, N'The interest rate is charged at arm''s length (eg refer to ATO benchmark interest rates).', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (502000, 43000, 498000, N'Agree a sample of loan repayments to the loan agreement to confirm repayments are as per the loan agreement.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (503000, 43000, 494000, N'add loan confirmation template here', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (504000, 43000, NULL, N'Movement', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (505000, 43000, 504000, N'Agree a sample of the loan repayments per the loan agreement to the bank statement', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (506000, 44000, NULL, N'Does the trust deed permit the borrowing under an instalment warrant arrangement.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (507000, 44000, NULL, N'The borrowing was used to acquire a single asset.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (508000, 44000, NULL, N'Review the loan agreement, ensuring the borrower is the fund trustee, agreement is limited in recourse, security provided is limited to the assets acquired. Also check the following:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (509000, 44000, 508000, N'Where personal guarantees have been provided, ensure agreement has been prepared where the guarantors have no further recourse against the fund assets.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (510000, 44000, 508000, N'If final instalment has been repaid, has the title been transferred to the trustee of the fund.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (511000, 44000, 508000, N'If yes, has the mortgage been discharged?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (512000, 44000, 508000, N'Was the loan entered into before 6 July 2010?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (513000, 44000, 508000, N'If yes, has the loan been refinanced since that date?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (514000, 44000, NULL, N'Review the custodian/bare trust borrowing agreement ensuring the following:', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (515000, 44000, 514000, N'Confirm the custodian trustee is not the same as the trustee of the fund.', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (516000, 44000, 514000, N'Is the custodian / bare trust established on or before the date of the contract of sale, or the date of the nomination election?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (517000, 44000, 514000, N'If an “and / or nominee” is noted on the contract of sale, does the nomination form note the custody trustee in trust for the fund trustee?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (518000, 44000, NULL, N'Has the trustees considered the future funding arrangements and the risk associated in the event of a member of the fund deceasing or becoming permanently disabled eg taken out life, trauma and/or income protection insurance policies on each member?', 0, 0, 0, 5, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (519000, 45000, NULL, N'We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that movements are fairly stated.', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (520000, 46000, NULL, N'To ensure the revenue is:', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (521000, 46000, 520000, N'a. Have occurred and pertain to the SMSF', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (522000, 46000, 520000, N'b. Are completely recorded', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (523000, 46000, 520000, N'c. Have been accurately recorded', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (524000, 46000, 520000, N'd. Have been recorded in the correct period', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (525000, 46000, 520000, N'e. Have been recorded in the proper accounts', 0, 0, 0, 5, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (526000, 47000, NULL, N'Agree a sample of interest received to bank confirmation,  bank statement and/or loan agreements and complete the interest calculator.', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (528000, 48000, NULL, N'Peruse the list of dividends / distributions received to determine where possible if all dividends / distributions have been recorded as being received.', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (529000, 48000, NULL, N'Where income has not been automatically verified, agree to the share registry websites or dividend / distribution notices and bank statements. Refer to samples selected above in the income report.', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (530000, 48000, NULL, N'If income is from unlisted (non marketable) investment, Agree dividend / distribution to source documentation and where material consider whether further external confirmation is required. Consider tax implications of the income being classified as special income.', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (531000, 49000, NULL, N'Obtain lease agreement or annual rental statement and agree rent received to bank statements.', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (532000, 49000, NULL, N'If the property is leased to a related party of the fund, ensure that it is business real property.if residential get confirmation that tenant is not a related party. ', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (533000, 49000, NULL, N'Complete and review the analytical rental calculator and determine if the rental yield is reasonable and is leased on an arm''s length commercial basis ie (3 - 5 % residential, 5 - 10% commercial).', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (535000, 50000, NULL, N'Agree rent received to the lease agreement and bank statements to ensure an arms length commercial arrangement is in place.', 1, 1, 1, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (536000, 51000, NULL, N'We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that revenue is fairly stated.', 1, 1, 1, 5, NULL, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (801000, 54000, NULL, N'Exists', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (802000, 54000, NULL, N'Are Complete', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (803000, 54000, NULL, N'Properly valued', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (804000, 54000, NULL, N'Ownership is correct', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (805000, 54000, NULL, N'Have been classified correctly.', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (806000, 54000, NULL, N'Properly valued', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (807000, 54000, NULL, N'Agree members opening balance to last year members statements.', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (808000, 54000, NULL, N'Review member''s statements to ensure reasonableness of the following:', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (809000, 54000, 808000, N'Share of net result for the year', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (810000, 54000, 808000, N'Contributions tax (15%)', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (811000, 54000, 808000, N'Ensure above amounts are allocated to the correct category of members benefits eg. preserved, restricted and non restricted unpreserved', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (812000, 54000, NULL, N'Review address of member as indication of their residence eg. Overseas or residing in rental property owned by the fund', 1, 1, 1, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (813000, 55000, NULL, N'Are the employer and member contributions within the contribution limits  as per the following table:', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (814000, 55000, 813000, N'Concessional (employer, member deductable)', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (815000, 55000, 813000, N'Less than 50 years old', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (816000, 55000, 813000, N'Between 50 and 65 years old on 30 June', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (817000, 55000, 813000, N'Between 65 and 75 years old and the work test is met*', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (818000, 55000, 813000, N'Over 75 years old and the work test is met* (ONLY Mandated employer allowed)^', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (819000, 55000, 813000, N'Non-concessional (personal/member)', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (820000, 55000, 813000, N'Less than 65 years old', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (821000, 55000, 813000, N'Between 65 and 75 years old and the work test is met*', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (822000, 55000, 813000, N'Over 75', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (823000, 55000, NULL, N'* Work Test Definition r being gainfully employed for 40 hours in a continuous period 30 days', 1, 0, 1, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (824000, 55000, NULL, N'^ This must not include salary sacrifice', 1, 1, 1, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (825000, 55000, NULL, N'If contribution cap has been exceeded advice trustees accordingly.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (826000, 55000, NULL, N'Ensure all contributions (except in specie contributions) and Rollovers into the fund been deposited into the funds'' bank account?', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (827000, 55000, NULL, N'Employer contributions(Concessional)', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (828000, 55000, 827000, N'Where practical, obtain confirmation from the employer and agree.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (829000, 55000, 827000, N'Ensure that the SG contributions do not exceed 9% of the member''s annual salary or up to the annual limit.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (830000, 55000, NULL, N'Member Contributions(concessional and non-concessional)', 0, 0, 0, 99, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (831000, 55000, 830000, N'For concessional member contributions, agree amount claimed to the signed copy of the notice of intent to claim a deduction under s290-170 of ITAA.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (832000, 55000, 830000, N'For members under the age of 65, If the non-concessional contributions exceed $150,000 per person ensure that it is within the bring forward cap of $450,000 by checking if the cap was triggered in prior years.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (833000, 55000, 830000, N'Ensure that the all contributions not complying with reg7.04 are refunded within 30 days of the trustees becoming aware of the breach.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (834000, 57000, NULL, N'Agree transfers in to rollover forms and minutes of meetings and ensure the categories have been correctly classified in the members statements.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (835000, 57000, NULL, N'Where material agree amounts to bank statements.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (836000, 58000, NULL, N'Agree payment to member rollover form, minutes of meeting or other documentation.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (837000, 58000, NULL, N'Ensure rollover complies with the trust deed', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (838000, 58000, NULL, N'Where the rollover is a result of a family law court order, review such orders to ensure payment is permissible.', 1, 1, 1, 99, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (839000, 59000, NULL, N'Where a new pension (including rollbacks) has been commenced  or a lump sum payment made during the year:', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (840000, 59000, 839000, N'Confirm trust deed permits benefit payment to be mad', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (841000, 59000, 839000, N'Sight commencement / lump sum request documentation and associated minutes of trustees.', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (842000, 59000, 839000, N'Has a condition of release been satisfied for the payment to be made (refer table below)', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (843000, 59000, 839000, N'Confirm payments to member statements and bank statements', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (844000, 59000, 839000, N'Where member under the age of 60, has appropriate ATO documentation been prepared?', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (845000, 59000, NULL, N'For pension accounts, ensure', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (846000, 59000, 845000, N'Complete the pension calculator ensure miminum amounts have been paid', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (847000, 59000, 845000, N'If minimum not met and the shortfall less than 1/12th of the annual payment amount, has the payment been made by the trustees as soon as practical?', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (848000, 59000, 845000, N'Where the pension is a Transition to Retirement Pension, ensure  the payment has not exceeded the 10% upper threshold limit', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (849000, 59000, 845000, N'Pension Calculator', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (850000, 59000, 845000, N'Date Of birth', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (851000, 60000, NULL, N'Defined benefit payments', 0, 0, 0, 6, NULL, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (852000, 60000, 851000, N'Ensure an actuarial report is obtained and review the report for reasonableness of assumptions, member details and solvency of the fund.', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (853000, 60000, 851000, N'Ensure that the actuarial report does not disclose a deficiency in accrued and vested benefits', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (854000, 60000, 851000, N'Agree payment amount to actuarial report', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (855000, 60000, NULL, N'Death benefit payments', 0, 0, 0, 6, NULL, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (856000, 60000, 855000, N'Sight death certificate, review trust deed, reviewing binding death benefit nomination if any, minutes of meetings.', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (857000, 60000, 855000, N'Agree payment to payment to bank statement', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (858000, 60000, 855000, N'Ensure payment is made to authorised beneficiary', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (859000, 60000, 855000, N'Ensure the member has been removed as a trustee / director of the trustee company of the fund.', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (860000, 60000, NULL, N'Disability benefit payments', 0, 0, 0, 6, NULL, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (861000, 60000, 860000, N'Sight at least two appropriate medical certifications confirming the inability of the member to work again either temporarily or permanently.', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (862000, 60000, 860000, N'Where material, trace amounts paid to bank statements', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (863000, 60000, 860000, N'Ensure the PAYG obligations have been correctly calculated and remitted.', 0, 0, 0, 6, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (864000, 61000, NULL, N'Ensure that the amount split is a result of a contribution received in the prior year from the splitting spouse and does not exceed 85% of the gross amount and is made in accordance with SIS Reg 6.42 & 6.44(1)', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (865000, 61000, NULL, N'Ensure the contribution has been split been between eligible spouses aged between 55 and 65 and whilst still accumulation stage.', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (866000, 61000, NULL, N'Ensure that the trust deed allows contribution splitting.', 0, 0, 0, 5, 3, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (867000, 62000, NULL, N'We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that the investment balances are fairly stated.', 0, 0, 0, 5, NULL, 1)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1008000, 77000, NULL, N'Fund Structure:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1009000, 77000, 1008000, N'Where there any issue/s or contravention/s in relation to:', 0, 0, 0, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1010000, 77000, 1009000, N's17A - SMSF defination, or', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1011000, 77000, 1009000, N's126K - disqualified person/s?', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1012000, 77000, NULL, N'Administration:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1013000, 77000, 1012000, N'Where there any contravention/s in relation to:', 0, 0, 0, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1014000, 77000, 1013000, N's103 - minutes and records', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1015000, 77000, 1013000, N's104A - trustee declaration, or,', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1016000, 77000, 1013000, N's35C(2) - documents requested by the auditor', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1017000, 77000, NULL, N'Contributions:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1018000, 77000, 1017000, N'Where there any contravention/s in relation to:', 0, 0, 0, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1019000, 77000, 1018000, N'r7.04 - the contribution standards?', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1020000, 77000, NULL, N'Investments:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1021000, 77000, 1020000, N'Where there any contravention/s in relation to:', 0, 0, 0, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1022000, 77000, 1021000, N's65 - lending', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1023000, 77000, 1021000, N's82-s85 - in-house assets,', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1024000, 77000, 1021000, N's67 - borrowing', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1025000, 77000, 1021000, N's109 - Investments to be on an arm''s length basis', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1026000, 77000, 1021000, N'r13.14 - charges over assets,', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1027000, 77000, 1021000, N'r4.09 - investments strategy, or,', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1028000, 77000, 1021000, N's52(2)(d) - seperation of assets', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1029000, 77000, NULL, N'Benefit Payments:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1030000, 77000, 1029000, N'Where there any contravention/s in relation to:', 0, 0, 0, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1031000, 77000, 1030000, N'r6.17 - benefit payment standards, or', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1032000, 77000, 1030000, N'r5.08 - maintaining minimum benefits?', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1033000, 77000, NULL, N'Sole purpose:', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1034000, 77000, 1033000, N'has the fund contravened the sole purpose test under S62?', 0, 0, 0, 6, 3, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1035000, 77000, 1034000, N'r6.17 - benefit payment standards, or', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedure] ([ID], [AuditProcedureTabContentId], [ParentId], [Title], [Attachments], [Oml], [Comments], [ControlType], [AnswerControlType], [Checkbox]) VALUES (1036000, 77000, 1034000, N'r5.08 - maintaining minimum benefits?', 0, 0, 0, 6, NULL, 0)
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (1000, 269000, N'Auditor''s Report Part B SISSA/SISSR', N'S.17A(97)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (2000, 269000, N'Auditor''s Report Part B SISSA/SISSR', N'S.17A')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (3000, 270000, N'Auditor''s Report Part B SISSA/SISSR', N'S.126K(98)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (4000, 270000, N'Auditor''s Report Part B SISSA/SISSR', N'S.126K')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (5000, 271000, N'Auditor''s Report Part B SISSA/SISSR', N'S.103')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (6000, 271000, N'Auditor''s Report Part B SISSA/SISSR', N'S.103')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (7000, 272000, N'Auditor''s Report Part B SISSA/SISSR', N'S.104A(99)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (8000, 272000, N'Auditor''s Report Part B SISSA/SISSR', N'S.104A')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (9000, 273000, N'Auditor''s Report Part B SISSA/SISSR', N'S.35A')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (10000, 273000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (11000, 274000, N'Auditor''s Report Part B SISSA/SISSR', N'S.35B')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (12000, 274000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (13000, 275000, N'Auditor''s Report Part B SISSA/SISSR', N'S.35C(2)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (14000, 275000, N'Auditor''s Report Part B SISSA/SISSR', N'S.35C(2)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (15000, 276000, N'Auditor''s Report Part B SISSA/SISSR', N'S.52(2)(e)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (16000, 276000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (17000, 277000, N'Auditor''s Report Part B SISSA/SISSR', N'R.4.09')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (18000, 277000, N'Auditor''s Report Part B SISSA/SISSR', N'R.4.09')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (19000, 279000, N'Auditor''s Report Part B SISSA/SISSR', N'S.62')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (20000, 279000, N'Auditor''s Report Part B SISSA/SISSR', N'S.62')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (21000, 281000, N'Auditor''s Report Part B SISSA/SISSR', N'Ss.69-71E; Ss.73-75; Ss.80-85')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (22000, 281000, N'Auditor''s Report Part B SISSA/SISSR', N'S.82, S.83, S.84 & S.85')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (23000, 282000, N'Auditor''s Report Part B SISSA/SISSR', N'S.66')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (24000, 282000, N'Auditor''s Report Part B SISSA/SISSR', N'S.66')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (25000, 283000, N'Auditor''s Report Part B SISSA/SISSR', N'S.109')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (26000, 283000, N'Auditor''s Report Part B SISSA/SISSR', N'S.109')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (27000, 284000, N'Auditor''s Report Part B SISSA/SISSR', N'S.52(2)(d)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (28000, 284000, N'Auditor''s Report Part B SISSA/SISSR', N'S.52(2)(d)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (29000, 285000, N'Auditor''s Report Part B SISSA/SISSR', N'S.65')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (30000, 285000, N'Auditor''s Report Part B SISSA/SISSR', N'S.65')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (31000, 286000, N'Auditor''s Report Part B SISSA/SISSR', N'S.67')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (32000, 286000, N'Auditor''s Report Part B SISSA/SISSR', N'S.67')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (33000, 287000, N'Auditor''s Report Part B SISSA/SISSR', N'R.13.14')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (34000, 287000, N'Auditor''s Report Part B SISSA/SISSR', N'R.13.14')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (35000, 289000, N'Auditor''s Report Part B SISSA/SISSR', N'R.5.08')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (36000, 289000, N'Auditor''s Report Part B SISSA/SISSR', N'R.5.08')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (37000, 290000, N'Auditor''s Report Part B SISSA/SISSR', N'R.1.06(9A)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (38000, 290000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (39000, 291000, N'Auditor''s Report Part B SISSA/SISSR', N'R.1.06(9A)')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (40000, 291000, N'Auditor''s Report Part B SISSA/SISSR', N'R.6.17')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (41000, 292000, N'Auditor''s Report Part B SISSA/SISSR', N'R.13.12')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (42000, 292000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (43000, 293000, N'Auditor''s Report Part B SISSA/SISSR', N'R.13.13')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (44000, 293000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (45000, 295000, N'Auditor''s Report Part B SISSA/SISSR', N'R.7.04')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (46000, 295000, N'Auditor''s Report Part B SISSA/SISSR', N'R.7.04')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (47000, 297000, N'Auditor''s Report Part B SISSA/SISSR', N'R. 5.03')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (48000, 297000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (49000, 299000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (50000, 299000, N'Auditor''s Report Part B SISSA/SISSR', N'S.130')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (51000, 301000, N'Auditor''s Report Part B SISSA/SISSR', N'-')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (52000, 301000, N'Auditor''s Report Part B SISSA/SISSR', N'S.130A')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60010, 814000, N'2010', N'')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60020, 814000, N'2011', N'')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60030, 814000, N'2012', N'')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60040, 814000, N'2013', N'')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60100, 815000, N'2010', N'$25,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60110, 815000, N'2011', N'$25,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60120, 815000, N'2012', N'$25,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60130, 815000, N'2013', N'$25,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60140, 816000, N'2010', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60150, 816000, N'2011', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60160, 816000, N'2012', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60170, 816000, N'2013', N'$25,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60180, 817000, N'2010', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60190, 817000, N'2011', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60200, 817000, N'2012', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60210, 817000, N'2013', N'$25,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60220, 818000, N'2010', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60230, 818000, N'2011', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60240, 818000, N'2012', N'$50,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60250, 818000, N'2013', N'$25,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60260, 820000, N'2010', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60270, 820000, N'2011', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60280, 820000, N'2012', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60290, 820000, N'2013', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60300, 821000, N'2010', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60310, 821000, N'2011', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60320, 821000, N'2012', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60330, 821000, N'2013', N'$150,000.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60340, 822000, N'2010', N'$0.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60350, 822000, N'2011', N'$0.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60360, 822000, N'2012', N'$0.00')
GO
INSERT [dbo].[AuditProcedureColumn] ([ID], [AuditProcedureId], [ColumnName], [ColumnValue]) VALUES (60370, 822000, N'2013', N'$0.00')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (100, N'Dash', N'--')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (200, N'NA', N'NA')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (300, N'Yes', N'Yes')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (400, N'No', N'No')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (500, N'Complete', N'Complete')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (600, N'NotComplete', N'NotComplete')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (700, N'Comply', N'Comply')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (800, N'NotComply', N'NotComply')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (900, N'SafeGuardInPlace', N'SafeGuardInPlace')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1000, N'Accumulation', N'Accumulation')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1100, N'Pension', N'Pension')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1200, N'AccumulationAndPension', N'Accumulation & Pension')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1300, N'AcceptAsReasonable', N'Accept as reasonable')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1400, N'Unacceptable', N'Unacceptable')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1500, N'Significant', N'Significant')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1600, N'High', N'High')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1700, N'Moderate', N'Moderate')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1800, N'Low', N'Low')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (1900, N'Insignificant', N'Insignificant')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2000, N'Considered', N'Considered')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2100, N'NotConsidered', N'NotConsidered')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2200, N'Agree', N'Agree')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2300, N'NotAgree', N'Not Agree')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2301, N'NotAvailable', N'Not Available')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2400, N'Auto', N'Auto')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2500, N'Compiled', N'Compiled')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2600, N'NotCompiled', N'Not Compiled')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2700, N'17A, 35A, 35B, 35C(2), 52(2)(d), 52(e), 62, 65, 66, 67, 69-71E, 73-75, 80-85, 103, 104A, 109, 126K', N'17A, 35A, 35B, 35C(2), 52(2)(d), 52(e), 62, 65, 66, 67, 69-71E, 73-75, 80-85, 103, 104A, 109, 126K')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2800, N'1.06(9A), 4.09, 5.03, 5.08, 6.17, 7.04, 13.12, 13.13, 13.14, 13.18AA', N'1.06(9A), 4.09, 5.03, 5.08, 6.17, 7.04, 13.12, 13.13, 13.14, 13.18AA')
GO
INSERT [dbo].[AuditProcedureOptionDetail] ([ID], [Field], [Value]) VALUES (2900, N'Special Purpose Financial Reporting Framework', N'Special Purpose Financial Reporting Framework')
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10000, 8000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10010, 8000, 1500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10020, 8000, 1600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10030, 8000, 1700)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10040, 8000, 1800)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10050, 8000, 1900)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10060, 9000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10070, 9000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10080, 9000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10090, 10000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10100, 10000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10110, 10000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10120, 10000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10130, 11000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10140, 11000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10150, 11000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10160, 12000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10170, 12000, 1500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10180, 12000, 1600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10190, 12000, 1700)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10200, 12000, 1800)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10210, 12000, 1900)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10220, 13000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10230, 13000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10240, 13000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10250, 15000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10260, 15000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10270, 15000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10280, 16000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10290, 16000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10300, 16000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10310, 16000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10320, 17000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10330, 17000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10340, 17000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10350, 17000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10360, 18000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10370, 18000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10380, 18000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10400, 19000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10410, 19000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10420, 19000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10430, 20000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10440, 20000, 1500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10450, 20000, 1600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10460, 20000, 1700)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10470, 20000, 1800)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10480, 20000, 1900)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10490, 21000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10500, 21000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10510, 21000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10520, 22000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10530, 22000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10540, 22000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10550, 22000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10560, 23000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10570, 23000, 1500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10580, 23000, 1600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10590, 23000, 1700)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10600, 23000, 1800)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10610, 23000, 1900)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10620, 24000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10630, 24000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10640, 24000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10650, 25000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10660, 25000, 1500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10670, 25000, 1600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10680, 25000, 1700)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10690, 25000, 1800)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10700, 25000, 1900)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10710, 26000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10720, 26000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10730, 26000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10740, 27000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10750, 27000, 500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10760, 27000, 600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10770, 28000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10780, 28000, 500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10790, 28000, 600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10800, 29000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10810, 29000, 700)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10820, 29000, 800)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10830, 30000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10840, 30000, 900)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10850, 30000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10860, 31010, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10870, 31010, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10880, 31010, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10890, 33000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10900, 33000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10910, 33000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10920, 34000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10930, 34000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10940, 34000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10950, 35000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10960, 35000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10970, 35000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10980, 36000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (10990, 36000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11000, 36000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11010, 37000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11020, 37000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11030, 37000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11040, 38000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11050, 38000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11060, 38000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11070, 40000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11080, 40000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11090, 40000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11100, 42000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11110, 42000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11120, 42000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11130, 44000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11140, 44000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11150, 44000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11160, 45000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11170, 45000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11180, 45000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11190, 46000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11200, 46000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11210, 46000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11220, 48000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11230, 48000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11240, 48000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11250, 74000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11260, 74000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11270, 74000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11280, 75000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11290, 75000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11300, 75000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11310, 76000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11320, 76000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11330, 76000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11340, 77000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11350, 77000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11360, 77000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11370, 78000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11380, 78000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11390, 78000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11400, 79000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11410, 79000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11420, 79000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11430, 80000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11440, 80000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11450, 80000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11490, 85000, 2700)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11500, 86000, 2800)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11510, 87000, 2900)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11520, 89000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11530, 89000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11540, 89000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11550, 90000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11560, 90000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11570, 90000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11580, 91000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11590, 91000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11600, 91000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11610, 93000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11620, 93000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11630, 93000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11640, 94000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11650, 94000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11660, 94000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11670, 152000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11680, 152000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11690, 152000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11700, 153000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11710, 153000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11720, 153000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11730, 154000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11740, 154000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11750, 154000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11760, 155000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11770, 155000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11780, 155000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11790, 156000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11800, 156000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11810, 156000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11820, 157000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11830, 157000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11840, 157000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11850, 158000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11860, 158000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11870, 158000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11880, 159000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11890, 159000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11900, 159000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11910, 160000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11920, 160000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11930, 160000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11940, 161000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11950, 161000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11960, 161000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11970, 163000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11980, 163000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (11990, 163000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12000, 177000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12010, 177000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12020, 177000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12030, 179000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12040, 179000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12050, 179000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12060, 180000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12070, 180000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12080, 180000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12090, 181000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12100, 181000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12110, 181000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12120, 182000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12130, 182000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12140, 182000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12150, 184000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12160, 184000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12170, 184000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12180, 185000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12190, 185000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12200, 185000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12210, 186000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12220, 186000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12230, 186000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12240, 187000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12250, 187000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12260, 187000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12270, 188000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12280, 188000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12290, 188000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12300, 189000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12310, 189000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12320, 189000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12330, 190000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12340, 190000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12350, 190000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12360, 190000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12370, 191000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12380, 191000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12390, 191000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12400, 191000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12410, 192000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12420, 192000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12430, 192000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12440, 192000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12460, 193000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12470, 193000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12480, 193000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12490, 193000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12500, 194000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12510, 194000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12520, 194000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12530, 194000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12540, 195000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12550, 195000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12560, 195000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12570, 195000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12580, 196000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12590, 196000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12600, 196000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12610, 196000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12620, 197000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12630, 197000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12640, 197000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12650, 197000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12660, 198000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12670, 198000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12680, 198000, 2000)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12690, 198000, 2100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12700, 209000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12710, 209000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12720, 209000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12730, 210000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12735, 210000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12740, 210000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12750, 222000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12760, 222000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12770, 222000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12780, 223000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12790, 223000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12800, 223000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12810, 224000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12820, 224000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12830, 224000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12840, 225000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12850, 225000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12860, 225000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12870, 231000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12880, 231000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12890, 231000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12900, 233000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12910, 233000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12920, 233000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12930, 234000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12940, 234000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12950, 234000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12960, 235000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12970, 235000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12980, 235000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (12990, 236000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13000, 236000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13010, 236000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13020, 237000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13030, 237000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13040, 237000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13050, 239000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13060, 239000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13070, 239000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13080, 240000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13090, 240000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13100, 240000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13110, 241000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13120, 241000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13130, 241000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13140, 242000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13150, 242000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13160, 242000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13170, 243000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13180, 243000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13190, 243000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13200, 245000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13210, 245000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13220, 245000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13230, 246000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13240, 246000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13250, 246000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13260, 247000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13270, 247000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13280, 247000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13290, 248000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13300, 248000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13310, 248000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13320, 250000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13330, 250000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13340, 250000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13350, 252000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13360, 252000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13370, 252000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13380, 253000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13390, 253000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13400, 253000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13405, 255000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13410, 255000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13420, 255000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13430, 256000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13440, 256000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13450, 256000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13460, 257000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13470, 257000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13480, 257000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13490, 258000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13500, 258000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13510, 258000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13520, 259000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13530, 259000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13540, 259000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13550, 261000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13560, 261000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13570, 261000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13580, 263000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13590, 263000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13600, 263000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13610, 264000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13620, 264000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13630, 264000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13640, 265000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13650, 265000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13660, 265000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13670, 266000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13680, 266000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13690, 266000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13700, 267000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13710, 267000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13720, 267000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13790, 320000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13800, 320000, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13810, 320000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13820, 320000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13830, 320000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13835, 321000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13840, 321000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13850, 321000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13860, 322000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13870, 322000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13880, 322000, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13890, 324000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13900, 324000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13910, 324000, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13920, 330000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13930, 330000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13940, 330000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13950, 336000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13960, 336000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13970, 336000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13980, 337000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (13990, 337000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14000, 337000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14010, 338000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14020, 338000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14030, 338000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14040, 339000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14050, 339000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14060, 339000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14070, 340000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14080, 340000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14090, 340000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14100, 343000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14110, 343000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14120, 343000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14130, 345000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14140, 345000, 2500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14150, 345000, 2600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14160, 350000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14170, 350000, 2500)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14180, 350000, 2600)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14190, 365000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14200, 365000, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14210, 365000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14220, 365000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14230, 365100, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14240, 365100, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14250, 365100, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14260, 365100, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14270, 367000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14280, 367000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14290, 367000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14300, 368000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14310, 368000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14320, 368000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14330, 368000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14350, 368000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14360, 369000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14370, 369000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14380, 369000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14390, 369000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14400, 369000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14410, 370000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14420, 370000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14430, 370000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14440, 370000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14450, 370000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14460, 371000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14470, 371000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14480, 371000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14490, 371000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14500, 371000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14510, 372000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14520, 372000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14530, 372000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14540, 390000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14550, 390000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14560, 390000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14570, 391000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14580, 391000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14590, 391000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14600, 392000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14610, 392000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14620, 392000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14630, 393000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14640, 393000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14650, 393000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14660, 394000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14670, 394000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14680, 394000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14690, 395000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14700, 395000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14710, 395000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14720, 396000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14730, 396000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14740, 396000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14750, 399000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14760, 399000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14770, 399000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14780, 400000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14790, 400000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14800, 400000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14810, 404000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14820, 404000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14830, 404000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14840, 406000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14850, 406000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14860, 406000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14870, 407000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14880, 407000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14890, 407000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14900, 409000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14910, 409000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14920, 409000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14930, 410000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14940, 410000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14950, 410000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14960, 412000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14970, 412000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14980, 412000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (14990, 413000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15000, 413000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15010, 413000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15020, 414000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15030, 414000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15040, 414000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15050, 415000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15060, 415000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15070, 415000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15080, 417000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15090, 417000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15100, 417000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15110, 418000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15120, 418000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15130, 418000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15140, 419000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15150, 419000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15160, 419000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15170, 420000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15180, 420000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15190, 420000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15200, 421000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15210, 421000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15220, 421000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15230, 423000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15240, 423000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15250, 423000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15260, 424000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15270, 424000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15280, 424000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15290, 425000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15300, 425000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15310, 425000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15320, 426000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15330, 426000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15340, 426000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15350, 428000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15360, 428000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15370, 428000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15380, 429000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15390, 429000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15400, 429000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15410, 430000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15420, 430000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15430, 430000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15440, 431000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15450, 431000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15460, 431000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15470, 433000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15480, 433000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15490, 433000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15500, 434000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15510, 434000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15520, 434000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15530, 435000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15540, 435000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15550, 435000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15560, 436000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15570, 436000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15580, 436000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15590, 436000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15600, 437000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15610, 437000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15620, 437000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15630, 437000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15640, 438000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15650, 438000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15660, 438000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15670, 439000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15680, 439000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15690, 439000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15700, 440000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15710, 440000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15720, 440000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15730, 441000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15740, 441000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15750, 441000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15760, 442000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15770, 442000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15780, 442000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15790, 443000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15800, 443000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15810, 443000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15820, 444000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15830, 444000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15840, 444000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15850, 445000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15860, 445000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15870, 445000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15880, 446000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15890, 446000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15900, 446000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15910, 446000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15920, 448000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15930, 448000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15940, 448000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15950, 449000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15960, 449000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15970, 449000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15980, 450000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (15990, 450000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16000, 450000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16010, 451000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16020, 451000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16030, 451000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16040, 452000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16050, 452000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16060, 452000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16070, 453000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16080, 453000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16090, 453000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16100, 454000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16110, 454000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16120, 454000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16130, 455000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16140, 455000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16150, 456000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16160, 456000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16170, 456000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16180, 456000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16190, 457000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16200, 457000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16210, 457000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16220, 458000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16230, 458000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16240, 458000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16250, 459000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16260, 459000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16270, 459000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16280, 459000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16290, 460000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16300, 460000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16310, 460000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16320, 461000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16330, 461000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16340, 461000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16350, 463000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16360, 463000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16370, 463000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16380, 464000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16390, 464000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16400, 464000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16410, 465000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16420, 465000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16430, 465000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16440, 466000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16450, 466000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16460, 466000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16470, 467000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16480, 467000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16490, 467000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16500, 469000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16510, 469000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16520, 469000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16525, 470000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16530, 470000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16540, 470000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16550, 470000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16560, 472000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16570, 472000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16580, 472000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16590, 473000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16600, 473000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16610, 473000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16620, 474000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16630, 474000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16640, 474000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16650, 477000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16660, 477000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16670, 477000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16680, 478000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16690, 478000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16700, 478000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16710, 479000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16720, 479000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16730, 479000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16750, 479000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16760, 480000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16770, 480000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16780, 480000, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16790, 481000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16800, 481000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16810, 481000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16820, 482000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16830, 482000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16840, 482000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16850, 484000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16860, 484000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16870, 484000, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16880, 490000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16890, 490000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16900, 490000, 2400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16910, 495000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16920, 495000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16930, 495000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16940, 499000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16950, 499000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16960, 499000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16970, 499000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16980, 500000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (16990, 500000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17000, 500000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17010, 500000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17020, 501000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17030, 501000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17040, 501000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17050, 501000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17060, 502000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17070, 502000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17080, 502000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17090, 502000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17100, 503000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17110, 503000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17120, 503000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17130, 503000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17140, 505000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17150, 505000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17160, 505000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17170, 505000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17180, 506000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17190, 506000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17200, 506000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17210, 507000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17220, 507000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17230, 507000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17240, 508000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17250, 508000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17260, 508000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17270, 509000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17280, 509000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17290, 509000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17300, 510000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17310, 510000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17320, 510000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17330, 511000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17340, 511000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17350, 511000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17360, 512000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17370, 512000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17380, 512000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17390, 495000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17400, 495000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17410, 495000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17420, 514000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17430, 514000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17440, 514000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17450, 514000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17460, 515000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17470, 515000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17480, 515000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17490, 516000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17500, 516000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17510, 516000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17520, 517000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17530, 517000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17540, 517000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17550, 518000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17560, 518000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17570, 518000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17590, 526000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17600, 526000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17610, 526000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17620, 528000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17630, 528000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17640, 528000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17650, 529000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17660, 529000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17670, 529000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17680, 530000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17690, 530000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17700, 530000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17710, 531000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17720, 531000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17730, 531000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17740, 532000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17750, 532000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17760, 532000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17770, 533000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17780, 533000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17790, 533000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17800, 535000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17810, 535000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17820, 535000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17830, 536000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17840, 536000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17850, 536000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17860, 807000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17870, 807000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17880, 807000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17890, 809000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17900, 809000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17910, 809000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17920, 810000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17930, 810000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17940, 810000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17950, 811000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17960, 811000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17970, 811000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17975, 812000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17976, 812000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17977, 812000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17980, 813000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (17990, 813000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18000, 813000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18010, 813000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18020, 825000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18030, 825000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18040, 825000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18050, 826000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18060, 826000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18070, 826000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18080, 826000, 2301)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18090, 828000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18100, 828000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18110, 828000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18120, 828000, 200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18130, 829000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18140, 829000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18150, 829000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18160, 831000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18170, 831000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18180, 831000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18190, 831000, 2301)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18200, 832000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18210, 832000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18220, 832000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18230, 832000, 2301)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18232, 833000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18234, 833000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18236, 833000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18238, 833000, 2301)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18240, 834000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18250, 834000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18260, 834000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18270, 835000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18280, 835000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18290, 835000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18310, 836000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18320, 836000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18330, 836000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18340, 837000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18350, 837000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18360, 837000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18370, 838000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18380, 838000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18390, 838000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18460, 852000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18470, 852000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18480, 852000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18490, 853000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18500, 853000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18510, 853000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18520, 854000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18530, 854000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18540, 854000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18550, 856000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18560, 856000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18570, 856000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18580, 857000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18590, 857000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18600, 857000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18610, 858000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18620, 858000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18630, 858000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18640, 859000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18650, 859000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18660, 859000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18670, 861000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18680, 861000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18690, 861000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18700, 862000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18710, 862000, 2200)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18720, 862000, 2300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18730, 863000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18740, 863000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18750, 863000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18760, 864000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18770, 864000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18780, 864000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18790, 865000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18800, 865000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18810, 865000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18820, 866000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18830, 866000, 1300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (18840, 866000, 1400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22000, 1009000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22010, 1009000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22020, 1009000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22030, 1013000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22040, 1013000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22050, 1013000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22060, 1018000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22070, 1018000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22080, 1018000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22090, 1021000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22100, 1021000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22110, 1021000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22120, 1030000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22130, 1030000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22140, 1030000, 400)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22150, 1034000, 100)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22160, 1034000, 300)
GO
INSERT [dbo].[AuditProcedureOption] ([ID], [AuditProcedureId], [AuditProcedureOptionDetailId]) VALUES (22170, 1034000, 400)
GO
SET IDENTITY_INSERT [dbo].[ActivityStates] ON 

GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (1, N'Job Initiated')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (2, N'Document Check & Data Import')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (3, N'Balance Sheet Review')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (4, N'Income Statements Review')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (5, N'SIS Compliance Audit')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (6, N'Review')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (7, N'Sign Off')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (8, N'Client Inquiry ')
GO
INSERT [dbo].[ActivityStates] ([ID], [Name]) VALUES (9, N'Audit Complete ')
GO
SET IDENTITY_INSERT [dbo].[ActivityStates] OFF
GO
SET IDENTITY_INSERT [dbo].[Activities] ON 

GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (1, 1, N'Document Check & Data Import', 2, 1, 0, 1, 0, N'/Imports/?jobId=[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (2, 1, N'Balance Sheet Review', 3, 0, 0, 1, 0, N'/Imports/BalanceSheetReview/[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (3, 1, N'Income Statements Review', 4, 0, 0, 1, 0, N'/Imports/IncomeStatementReview/[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (4, 1, N'SIS Compliance Audit', 5, 0, 0, 1, 0, N'/Job/WorkPaper?jobId=[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (5, 1, N'Review', 6, 0, 0, 1, 0, N'/Job/WorkPaper?jobId=[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (6, 1, N'Sign Off', 7, 0, 0, 1, 0, N'/Job/WorkPaper?jobId=[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (7, 1, N'Data Import Client Response', 8, 0, 0, 1, 0, N'/job/Detail/[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (8, 1, N'Balance Sheet Client Response', 8, 0, 0, 1, 0, N'/job/Detail/[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (9, 1, N'Income Statement Client Response', 8, 0, 0, 1, 0, N'/job/Detail/[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (10, 1, N'SIS Client Response', 8, 0, 0, 1, 0, N'/job/Detail/[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (11, 1, N'Review Client Response', 8, 0, 0, 1, 0, N'/job/Detail/[ID]')
GO
INSERT [dbo].[Activities] ([ID], [WorkflowID], [Name], [StateID], [IsInitial], [IsFinal], [IsForSetState], [IsAutoScheme], [ActivityView]) VALUES (12, 1, N'Job Complete', 9, 0, 1, 1, 0, N'/job/Detail/[ID]')
GO
SET IDENTITY_INSERT [dbo].[Activities] OFF
GO
SET IDENTITY_INSERT [dbo].[Actions] ON 

GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (1, N'Data Import', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'DataImport', 0)
GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (2, N'Balance Sheet Review', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'BalanceSheetReview', 0)
GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (3, N'Income Statement Review', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'IncomeStatementReview', 0)
GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (4, N'SIS Compliance Audit', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'SisComplianceAudit', 0)
GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (5, N'Review', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'ProcessReview', 0)
GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (6, N'Sign Off', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'ProcessSignOff', 0)
GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (7, N'Client Response', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'ClientResponse', 0)
GO
INSERT [dbo].[Actions] ([ID], [Name], [Type], [Method], [IsConditional]) VALUES (8, N'Job Complete', N'DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business', N'JobComplete', 0)
GO
SET IDENTITY_INSERT [dbo].[Actions] OFF
GO
SET IDENTITY_INSERT [dbo].[Commands] ON 

GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (1, N'Send For Balance Sheet Review')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (2, N'Get Client Response')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (3, N'Forward To Audit')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (4, N'Send For Income Statement Review')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (5, N'Send For SIS Compliance')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (6, N'Send For Review')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (7, N'Send For Sign Off')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (8, N'Force Send To Audit')
GO
INSERT [dbo].[Commands] ([ID], [Name]) VALUES (9, N'Audit Complete')
GO
SET IDENTITY_INSERT [dbo].[Commands] OFF
GO
SET IDENTITY_INSERT [dbo].[Transitions] ON 

GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (1, 1, N'DataImport', 1, 2, 1, 1, 1, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (2, 1, N'DataImportClientRequest', 1, 7, 1, 1, 2, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (3, 1, N'DataImportClientResponse', 7, 1, 2, 1, 3, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (4, 1, N'BalanceSheetReview', 2, 3, 1, 1, 4, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (5, 1, N'BalanceSheetClientRequest', 2, 8, 1, 1, 2, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (6, 1, N'BalanceSheetClientResponse', 8, 2, 2, 1, 3, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (7, 1, N'IncomeStatement', 3, 4, 1, 1, 5, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (8, 1, N'IncomeStatementClientRequest', 3, 9, 1, 1, 2, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (9, 1, N'IncomeStatementClientResponse', 9, 3, 2, 1, 3, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (10, 1, N'SISCompliance', 4, 5, 1, 1, 6, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (11, 1, N'SISClientRequest', 4, 10, 1, 1, 2, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (12, 1, N'SISClientResponse', 10, 4, 2, 1, 3, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (13, 1, N'Review', 5, 6, 1, 1, 7, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (14, 1, N'ReviewClientRequest', 5, 11, 1, 1, 2, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (15, 1, N'ReviewClientResponse', 11, 5, 2, 1, 3, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (16, 1, N'Data Import Client Override', 7, 1, 2, 1, 8, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (17, 1, N'Balance Sheet Client Override', 8, 2, 2, 1, 8, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (18, 1, N'Income Statement Client Override', 9, 3, 2, 1, 8, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (19, 1, N'SIS  Client Override', 10, 4, 2, 1, 8, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (20, 1, N'Review Client Override', 11, 5, 1, 1, 8, 1, NULL, 0,1,GetDate())
GO
INSERT [dbo].[Transitions] ([ID], [WorkflowID], [Name], [FromActivityID], [ToActivityID], [Classifier], [CommandType], [CommandID], [ActionType], [ActionID], [PreExecutionResult] ,[CreatedBy],[CreatedOn]) VALUES (21, 1, N'Job Complete', 6, 12, 1, 1, 9, 1, NULL, 0,1,GetDate())
GO
SET IDENTITY_INSERT [dbo].[Transitions] OFF
GO
SET IDENTITY_INSERT [dbo].[ActivityActions] ON 

GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (1, 0, 1, 1, 1)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (2, 0, 1, 2, 2)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (3, 0, 1, 3, 3)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (4, 0, 1, 4, 4)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (5, 0, 1, 5, 5)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (6, 0, 1, 6, 6)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (7, 0, 1, 8, 7)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (8, 0, 1, 9, 7)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (9, 0, 1, 10, 7)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (10, 0, 1, 11, 7)
GO
INSERT [dbo].[ActivityActions] ([ID], [IsPreExecution], [Order], [ActivityID], [ActionID]) VALUES (11, 0, 1, 12, 8)
GO
SET IDENTITY_INSERT [dbo].[ActivityActions] OFF
GO
SET IDENTITY_INSERT [dbo].[Parameters] ON 

GO
INSERT [dbo].[Parameters] ([ID], [Name], [Type], [Purpose], [SerializedDefaultValue]) VALUES (1, N'Comment', N'System.String', 1, NULL)
GO
INSERT [dbo].[Parameters] ([ID], [Name], [Type], [Purpose], [SerializedDefaultValue]) VALUES (2, N'ProcessId', N'System.Guid', 2, NULL)
GO
INSERT [dbo].[Parameters] ([ID], [Name], [Type], [Purpose], [SerializedDefaultValue]) VALUES (3, N'PreviousState', N'System.String', 2, NULL)
GO
INSERT [dbo].[Parameters] ([ID], [Name], [Type], [Purpose], [SerializedDefaultValue]) VALUES (4, N'CurrentState', N'System.String', 3, NULL)
GO
INSERT [dbo].[Parameters] ([ID], [Name], [Type], [Purpose], [SerializedDefaultValue]) VALUES (5, N'PreviousStateForDirect', N'System.String', 3, NULL)
GO
INSERT [dbo].[Parameters] ([ID], [Name], [Type], [Purpose], [SerializedDefaultValue]) VALUES (6, N'IsPreExecution', N'System.Boolean', 2, NULL)
GO
INSERT [dbo].[Parameters] ([ID], [Name], [Type], [Purpose], [SerializedDefaultValue]) VALUES (7, N'ConditionResult', N'System.Boolean', 2, NULL)
GO
SET IDENTITY_INSERT [dbo].[Parameters] OFF
GO
SET IDENTITY_INSERT [dbo].[ActionParameters] ON 

GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (1, 0, 1, 1, 2)
GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (2, 0, 1, 2, 2)
GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (3, 0, 1, 3, 2)
GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (4, 0, 1, 4, 2)
GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (5, 0, 1, 5, 2)
GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (6, 0, 1, 6, 2)
GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (7, 0, 1, 7, 2)
GO
INSERT [dbo].[ActionParameters] ([ID], [IsOutput], [Order], [ActionID], [ParameterID]) VALUES (8, 0, 1, 8, 2)
GO
SET IDENTITY_INSERT [dbo].[ActionParameters] OFF
GO
SET IDENTITY_INSERT [dbo].[Actors] ON 

GO
INSERT [dbo].[Actors] ([ID], [Name], [MethodName]) VALUES (1, N'Audit Admin', N'IsAuditAdmin')
GO
INSERT [dbo].[Actors] ([ID], [Name], [MethodName]) VALUES (2, N'Balance Sheet Auditor', N'IsBalanceSheetAuditor')
GO
INSERT [dbo].[Actors] ([ID], [Name], [MethodName]) VALUES (3, N'Income Statement Auditor', N'IsIncomeStatementAuditor')
GO
INSERT [dbo].[Actors] ([ID], [Name], [MethodName]) VALUES (4, N'SIS Compliance Auditor', N'IsSISAuditor')
GO
INSERT [dbo].[Actors] ([ID], [Name], [MethodName]) VALUES (5, N'Review Manager', N'IsReviewManager')
GO
INSERT [dbo].[Actors] ([ID], [Name], [MethodName]) VALUES (6, N'Sign Off Manager', N'IsSignOffManager')
GO
INSERT [dbo].[Actors] ([ID], [Name], [MethodName]) VALUES (7, N'ClientUser', N'IsClientUser')
GO
SET IDENTITY_INSERT [dbo].[Actors] OFF
GO
SET IDENTITY_INSERT [dbo].[TransitionActors] ON 

GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (1, 1, 1, 1)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (2, 1, 2, 1)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (3, 1, 3, 7)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (4, 1, 4, 2)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (5, 1, 5, 2)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (6, 1, 6, 7)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (7, 1, 7, 3)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (8, 1, 8, 3)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (9, 1, 9, 7)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (10, 1, 10, 4)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (11, 1, 11, 4)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (12, 1, 12, 7)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (13, 1, 13, 5)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (14, 1, 14, 5)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (15, 1, 15, 7)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (16, 1, 16, 1)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (17, 1, 17, 1)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (18, 1, 18, 1)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (19, 1, 19, 1)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (20, 1, 20, 1)
GO
INSERT [dbo].[TransitionActors] ([ID], [RestrictionType], [TransitionID], [ActorID]) VALUES (21, 1, 21, 6)
GO
SET IDENTITY_INSERT [dbo].[TransitionActors] OFF
GO
INSERT [dbo].[GroupRoles] ([GroupID], [RoleID]) VALUES (1, 1)
GO
INSERT [dbo].[GroupRoles] ([GroupID], [RoleID]) VALUES (3, 4)
GO
SET IDENTITY_INSERT [dbo].[SettingTypes] ON 

GO
INSERT [dbo].[SettingTypes] ([ID], [Description], [StatusID]) VALUES (1, N'Email Settings', 1)
GO
INSERT [dbo].[SettingTypes] ([ID], [Description], [StatusID]) VALUES (2, N'Password Settings', 1)
GO
INSERT [dbo].[SettingTypes] ([ID], [Description], [StatusID]) VALUES (3, N'Session Settings', 1)
GO
INSERT [dbo].[SettingTypes] ([ID], [Description], [StatusID]) VALUES (4, N'Sharepoint Settings', 1)
GO
INSERT [dbo].[SettingTypes] ([ID], [Description], [StatusID]) VALUES (5, N'Report Server Settings', 1)
GO
SET IDENTITY_INSERT [dbo].[SettingTypes] OFF
GO
SET IDENTITY_INSERT [dbo].[Settings] ON 

GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (1, 1, N'SMTPHost', N'exchange.dbaadvisory.com', 1, 1, CAST(0x0000A35B00AB6A5F AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (2, 1, N'SMTPUserName', N'dbaadvisory\dev.test', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (3, 1, N'SMTPPassword', N'devtest1', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (4, 1, N'SMTPFromAddress', N'dtest@dbaadvisory.com', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (5, 1, N'SMTPPort', N'587', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (6, 1, N'SMTPRequireSSL', N'false', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (7, 2, N'MinPasswordLength', N'6', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (8, 2, N'MinUpperCaseCharacters', N'0', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (9, 2, N'MinLowerCaseCharacters', N'0', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (10, 2, N'MinNumericCharacters', N'3', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (11, 2, N'MaxPasswordRetries', N'3', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (12, 3, N'MaxSessionTimeOut', N'20', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (13, 4, N'SharePointUrl', N'http://www.dbasystem.com.au:8080/', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (14, 4, N'SharePointUserName', N'Dbasystem\administrator', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (15, 4, N'SharePointPassword', N'$dbasystem#1', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (16, 4, N'SharePointMaxFileSize', N'1', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (17, 4, N'SharePointAllowedFileType', N'doc,xlsx', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (18, 4, N'SharePointJobDefaultDocumentsPath', N'DefaultDocuments/Job/', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (19, 5, N'ReportServerUrl', N'http://localhost:80/ReportServer', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (20, 5, N'ReportServerUsername', N'', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Settings] ([ID], [SettingTypeID], [Description], [SettingValue], [StatusID], [CreatedBy], [CreatedOn], [ModifiedBy], [ModifiedOn]) VALUES (21, 5, N'ReportServerPassword', N'', 1, 1, CAST(0x0000A35B00AB6A60 AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Settings] OFF
GO

/********************************************************************************/
/**********************************WORKFLOW**************************************/

IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'spWorkflowProcessResetRunningStatus')
DROP PROCEDURE spWorkflowProcessResetRunningStatus
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spWorkflowProcessResetRunningStatus]
AS
BEGIN
	UPDATE [dbo].[WorkflowProcessInstanceStatus] SET [WorkflowProcessInstanceStatus].[Status] = 2 WHERE [WorkflowProcessInstanceStatus].[Status] = 1
END
GO
Delete From [dbo].[WorkflowMaps]
GO
Delete from [dbo].[WorkflowScheme]
GO
/****** Object:  Table [dbo].[WorkflowProcessTransitionHistory]    Script Date: 04/02/2014 14:53:17 ******/
Delete From [dbo].[WorkflowProcessScheme]
GO

/****** Object:  Table [dbo].[WorkflowProcessScheme]    Script Date: 04/02/2014 14:53:16 ******/

INSERT INTO dbo.WorkflowProcessScheme (Id, [Scheme], DefiningParameters, DefiningParametersHash, ProcessName, IsObsolete)
VALUES ('DACFC694-29F6-45FA-9244-6AEEE43DE988', '<Process Name="Job Process">
  <Actors>
    <Actor Name="Audit Admin" ID="1">
      <Rule RuleName="IsAuditAdmin" />
    </Actor>
    <Actor Name="Balance Sheet Auditor" ID="2">
      <Rule RuleName="IsBalanceSheetAuditor" />
    </Actor>
    <Actor Name="Income Statement Auditor" ID="3">
      <Rule RuleName="IsIncomeStatementAuditor" />
    </Actor>
    <Actor Name="SIS Compliance Auditor" ID="4">
      <Rule RuleName="IsSISAuditor" />
    </Actor>
    <Actor Name="Review Manager" ID="5">
      <Rule RuleName="IsReviewManager" />
    </Actor>
    <Actor Name="Sign Off Manager" ID="6">
      <Rule RuleName="IsSignOffManager" />
    </Actor>
    <Actor Name="ClientUser" ID="7">
      <Rule RuleName="IsClientUser" />
    </Actor>
  </Actors>
  <Parameters />
  <Commands>
    <Command Name="Send For Balance Sheet Review" ID="1">
      <InputParameters />
    </Command>
    <Command Name="Get Client Response" ID="2">
      <InputParameters />
    </Command>
    <Command Name="Forward To Audit" ID="3">
      <InputParameters />
    </Command>
    <Command Name="Send For Income Statement Review" ID="4">
      <InputParameters />
    </Command>
    <Command Name="Send For SIS Compliance" ID="5">
      <InputParameters />
    </Command>
    <Command Name="Send For Review" ID="6">
      <InputParameters />
    </Command>
    <Command Name="Send For Sign Off" ID="7">
      <InputParameters />
    </Command>
    <Command Name="Force Send To Audit" ID="8">
      <InputParameters />
    </Command>
    <Command Name="Audit Complete" ID="9">
      <InputParameters />
    </Command>
  </Commands>
  <Actions>
    <Action Name="Data Import" ID="1">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="DataImport">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
    <Action Name="Balance Sheet Review" ID="2">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="BalanceSheetReview">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
    <Action Name="Income Statement Review" ID="3">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="IncomeStatementReview">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
    <Action Name="SIS Compliance Audit" ID="4">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="SisComplianceAudit">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
    <Action Name="Review" ID="5">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="ProcessReview">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
    <Action Name="Sign Off" ID="6">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="ProcessSignOff">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
    <Action Name="Client Response" ID="7">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="ClientResponse">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
    <Action Name="Job Complete" ID="8">
      <ExecuteMethod Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business" MethodName="JobComplete">
        <InputParameters>
          <ParameterRef Order="1" NameRef="ProcessId" />
        </InputParameters>
        <OutputParameters />
      </ExecuteMethod>
    </Action>
  </Actions>
  <Activities>
    <Activity Name="Document Check &amp; Data Import" State="Document Check &amp; Data Import" IsInitial="True" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="1">
      <Implementation>
        <ActionRef Order="1" NameRef="Data Import" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="0" Y="0" />
    </Activity>
    <Activity Name="Balance Sheet Review" State="Balance Sheet Review" IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="2">
      <Implementation>
        <ActionRef Order="1" NameRef="Balance Sheet Review" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="200" Y="0" />
    </Activity>
    <Activity Name="Income Statements Review" State="Income Statements Review" IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="3">
      <Implementation>
        <ActionRef Order="1" NameRef="Income Statement Review" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="400" Y="0" />
    </Activity>
    <Activity Name="SIS Compliance Audit" State="SIS Compliance Audit" IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="4">
      <Implementation>
        <ActionRef Order="1" NameRef="SIS Compliance Audit" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="600" Y="0" />
    </Activity>
    <Activity Name="Review" State="Review" IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="5">
      <Implementation>
        <ActionRef Order="1" NameRef="Review" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="800" Y="0" />
    </Activity>
    <Activity Name="Sign Off" State="Sign Off" IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="6">
      <Implementation>
        <ActionRef Order="1" NameRef="Sign Off" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="0" Y="150" />
    </Activity>
    <Activity Name="Data Import Client Response" State="Client Inquiry " IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="7">
      <Implementation />
      <PreExecutionImplementation />
      <Designer X="200" Y="150" />
    </Activity>
    <Activity Name="Balance Sheet Client Response" State="Client Inquiry " IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="8">
      <Implementation>
        <ActionRef Order="1" NameRef="Client Response" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="400" Y="150" />
    </Activity>
    <Activity Name="Income Statement Client Response" State="Client Inquiry " IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="9">
      <Implementation>
        <ActionRef Order="1" NameRef="Client Response" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="600" Y="150" />
    </Activity>
    <Activity Name="SIS Client Response" State="Client Inquiry " IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="10">
      <Implementation>
        <ActionRef Order="1" NameRef="Client Response" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="800" Y="150" />
    </Activity>
    <Activity Name="Review Client Response" State="Client Inquiry " IsInitial="False" IsFinal="False" IsForSetState="True" IsAutoSchemeUpdate="false" ID="11">
      <Implementation>
        <ActionRef Order="1" NameRef="Client Response" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="0" Y="300" />
    </Activity>
    <Activity Name="Job Complete" State="Audit Complete " IsInitial="False" IsFinal="True" IsForSetState="True" IsAutoSchemeUpdate="false" ID="12">
      <Implementation>
        <ActionRef Order="1" NameRef="Job Complete" />
      </Implementation>
      <PreExecutionImplementation />
      <Designer X="200" Y="300" />
    </Activity>
  </Activities>
  <Transitions>
    <Transition Name="DataImport" From="Document Check &amp; Data Import" To="Balance Sheet Review" Classifier="Direct" ID="1" FromActivityID="1" ToActivityID="2">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Audit Admin" ActorID="1" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Send For Balance Sheet Review" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="DataImportClientRequest" From="Document Check &amp; Data Import" To="Data Import Client Response" Classifier="Direct" ID="2" FromActivityID="1" ToActivityID="7">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Audit Admin" ActorID="1" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Get Client Response" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="DataImportClientResponse" From="Data Import Client Response" To="Document Check &amp; Data Import" Classifier="Reverse" ID="3" FromActivityID="7" ToActivityID="1">
      <Restrictions>
        <Restriction Type="Allow" NameRef="ClientUser" ActorID="7" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Forward To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="BalanceSheetReview" From="Balance Sheet Review" To="Income Statements Review" Classifier="Direct" ID="4" FromActivityID="2" ToActivityID="3">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Balance Sheet Auditor" ActorID="2" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Send For Income Statement Review" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="BalanceSheetClientRequest" From="Balance Sheet Review" To="Balance Sheet Client Response" Classifier="Direct" ID="5" FromActivityID="2" ToActivityID="8">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Balance Sheet Auditor" ActorID="2" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Get Client Response" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="BalanceSheetClientResponse" From="Balance Sheet Client Response" To="Balance Sheet Review" Classifier="Reverse" ID="6" FromActivityID="8" ToActivityID="2">
      <Restrictions>
        <Restriction Type="Allow" NameRef="ClientUser" ActorID="7" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Forward To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="IncomeStatement" From="Income Statements Review" To="SIS Compliance Audit" Classifier="Direct" ID="7" FromActivityID="3" ToActivityID="4">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Income Statement Auditor" ActorID="3" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Send For SIS Compliance" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="IncomeStatementClientRequest" From="Income Statements Review" To="Income Statement Client Response" Classifier="Direct" ID="8" FromActivityID="3" ToActivityID="9">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Income Statement Auditor" ActorID="3" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Get Client Response" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="IncomeStatementClientResponse" From="Income Statement Client Response" To="Income Statements Review" Classifier="Reverse" ID="9" FromActivityID="9" ToActivityID="3">
      <Restrictions>
        <Restriction Type="Allow" NameRef="ClientUser" ActorID="7" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Forward To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="SISCompliance" From="SIS Compliance Audit" To="Review" Classifier="Direct" ID="10" FromActivityID="4" ToActivityID="5">
      <Restrictions>
        <Restriction Type="Allow" NameRef="SIS Compliance Auditor" ActorID="4" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Send For Review" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="SISClientRequest" From="SIS Compliance Audit" To="SIS Client Response" Classifier="Direct" ID="11" FromActivityID="4" ToActivityID="10">
      <Restrictions>
        <Restriction Type="Allow" NameRef="SIS Compliance Auditor" ActorID="4" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Get Client Response" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="SISClientResponse" From="SIS Client Response" To="SIS Compliance Audit" Classifier="Reverse" ID="12" FromActivityID="10" ToActivityID="4">
      <Restrictions>
        <Restriction Type="Allow" NameRef="ClientUser" ActorID="7" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Forward To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="Review" From="Review" To="Sign Off" Classifier="Direct" ID="13" FromActivityID="5" ToActivityID="6">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Review Manager" ActorID="5" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Send For Sign Off" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="ReviewClientRequest" From="Review" To="Review Client Response" Classifier="Direct" ID="14" FromActivityID="5" ToActivityID="11">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Review Manager" ActorID="5" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Get Client Response" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="ReviewClientResponse" From="Review Client Response" To="Review" Classifier="Reverse" ID="15" FromActivityID="11" ToActivityID="5">
      <Restrictions>
        <Restriction Type="Allow" NameRef="ClientUser" ActorID="7" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Forward To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="Data Import Client Override" From="Data Import Client Response" To="Document Check &amp; Data Import" Classifier="Reverse" ID="16" FromActivityID="7" ToActivityID="1">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Audit Admin" ActorID="1" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Force Send To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="Balance Sheet Client Override" From="Balance Sheet Client Response" To="Balance Sheet Review" Classifier="Reverse" ID="17" FromActivityID="8" ToActivityID="2">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Audit Admin" ActorID="1" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Force Send To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="Income Statement Client Override" From="Income Statement Client Response" To="Income Statements Review" Classifier="Reverse" ID="18" FromActivityID="9" ToActivityID="3">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Audit Admin" ActorID="1" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Force Send To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="SIS  Client Override" From="SIS Client Response" To="SIS Compliance Audit" Classifier="Reverse" ID="19" FromActivityID="10" ToActivityID="4">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Audit Admin" ActorID="1" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Force Send To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="Review Client Override" From="Review Client Response" To="Review" Classifier="Direct" ID="20" FromActivityID="11" ToActivityID="5">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Audit Admin" ActorID="1" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Force Send To Audit" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
    <Transition Name="Job Complete" From="Sign Off" To="Job Complete" Classifier="Direct" ID="21" FromActivityID="6" ToActivityID="12">
      <Restrictions>
        <Restriction Type="Allow" NameRef="Sign Off Manager" ActorID="6" TypeID="1" ID="0" />
      </Restrictions>
      <Triggers>
        <Trigger Type="Command" NameRef="Audit Complete" />
      </Triggers>
      <Conditions>
        <Condition Type="Always" ResultOnPreExecution="false" />
      </Conditions>
    </Transition>
  </Transitions>
  <ID>1</ID>
</Process>', '{}', 'r4ztHEDMTwYwDqoEyePFlg==', 'DACFC694-29F6-45FA-9244-6AEEE43DE988', 0)
GO

/****** Object:  Table [dbo].[WorkflowProcessTransitionHistory]    Script Date: 04/02/2014 14:53:17 ******/

/****** Object:  Table [dbo].[WorkflowMaps]    Script Date: 04/02/2014 14:53:16 ******/

/****** Object:  Trigger [tWorkflowProcessTransitionHistoryInsert]    Script Date: 04/02/2014 14:53:17 ******/
Go
IF EXISTS (SELECT name FROM sysobjects
      WHERE name = 'tWorkflowProcessTransitionHistoryInsert' AND type = 'TR')
   DROP TRIGGER tWorkflowProcessTransitionHistoryInsert 
GO
Create TRIGGER [dbo].[tWorkflowProcessTransitionHistoryInsert]
   ON  [dbo].[WorkflowProcessTransitionHistory]
   AFTER INSERT
AS 
BEGIN
	BEGIN TRAN 
				UPDATE [WorkflowProcessInstance] 
				SET 
					StateName = COALESCE(i.ToStateName,StateName), 
					ActivityName = i.ToActivityName,
					PreviousActivity = i.FromActivityName,
					PreviousState = COALESCE(i.FromStateName,StateName),
					PreviousActivityForDirect = CASE WHEN i.TransitionClassifier = 'Direct' THEN i.FromActivityName ELSE PreviousActivityForDirect END,
					PreviousActivityForReverse = CASE WHEN i.TransitionClassifier = 'Reverse' THEN i.FromActivityName ELSE PreviousActivityForReverse END,
					PreviousStateForDirect = CASE WHEN i.TransitionClassifier = 'Direct' THEN COALESCE(i.FromStateName,PreviousStateForDirect) ELSE PreviousStateForDirect END,
					PreviousStateForReverse = CASE WHEN i.TransitionClassifier = 'Reverse' THEN COALESCE(i.FromStateName,PreviousStateForReverse) ELSE PreviousStateForReverse END
					FROM inserted i INNER JOIN [WorkflowProcessInstance] wpi ON i.ProcessId = wpi.Id
				     
	COMMIT TRAN	
END
GO
IF TYPE_ID(N'[IdsTableType]') IS NULL
CREATE TYPE [dbo].[IdsTableType] AS TABLE(	[Id] [uniqueidentifier] NULL)
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'DropWorkflowProcesses')
DROP PROCEDURE DropWorkflowProcesses
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DropWorkflowProcesses] 
		@Ids  IdsTableType	READONLY
	AS	
	BEGIN
		BEGIN TRAN
	
		DELETE dbo.WorkflowProcessInstance FROM dbo.WorkflowProcessInstance wpi  INNER JOIN @Ids  ids ON wpi.Id = ids.Id 
		DELETE dbo.WorkflowProcessInstanceStatus FROM dbo.WorkflowProcessInstanceStatus wpi  INNER JOIN @Ids  ids ON wpi.Id = ids.Id 
		DELETE dbo.WorkflowProcessInstanceStatus FROM dbo.WorkflowProcessInstancePersistence wpi  INNER JOIN @Ids  ids ON wpi.ProcessId = ids.Id 
	

		COMMIT TRAN
	END
GO


IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'DropWorkflowProcess')
DROP PROCEDURE DropWorkflowProcess
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DropWorkflowProcess] 
		@id uniqueidentifier
	AS
	BEGIN
		BEGIN TRAN
	
		DELETE FROM dbo.WorkflowProcessInstance WHERE Id = @id
		DELETE FROM dbo.WorkflowProcessInstanceStatus WHERE Id = @id
		DELETE FROM dbo.WorkflowProcessInstancePersistence  WHERE ProcessId = @id
	
		COMMIT TRAN
	END
GO
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'DropWorkflowInbox')
DROP PROCEDURE DropWorkflowInbox
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DropWorkflowInbox] 
		@processId uniqueidentifier
	AS
	BEGIN
		BEGIN TRAN	
		DELETE FROM dbo.WorkflowInbox WHERE ProcessId = @processId	
		COMMIT TRAN
	END
GO
Delete From dbo.WorkflowMaps
Go

INSERT INTO dbo.WorkflowMaps ( WorkFlowTypeId, WorkFlowId,CreatedBy,CreatedOn,ModifiedOn,ModifiedBy,Name,IsActive)
VALUES (1, 'DACFC694-29F6-45FA-9244-6AEEE43DE988',1,GetDate(),GetDate(),1,'Default',1)
Go
Delete From WorkflowScheme
Go
INSERT INTO WorkflowScheme( Id,Code,[Scheme] )
SELECT NEWID(),ProcessName,Scheme from 
WorkflowProcessScheme
Go