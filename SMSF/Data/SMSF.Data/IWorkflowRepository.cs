﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Linq;

namespace DBASystem.SMSF.Data
{
    public interface IWorkflowRepository<T> where T : WorkflowBase
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAll(Func<T, Boolean> where);
        IQueryable<T> GetAll(Func<T, Boolean> where, string[] includes);
        T GetById(Guid Id);
        T Get(Func<T, Boolean> where);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        bool Any(Func<T, bool> where);
    }
}
