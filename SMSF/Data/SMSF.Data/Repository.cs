﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace DBASystem.SMSF.Data
{
    internal class Repository<T> : IRepository<T> where T : BaseEntity
    {
        internal SMSFContext Context { get; set; }
        protected DbSet<T> Set { get; set; }

        internal Repository(SMSFContext context)
        {
            Context = context;
            Set = Context.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return Set;
        }

        public T GetById(int id)
        {
            return Set.FirstOrDefault(a => a.ID == id);
        }

        public void Add(T entity)
        {
            var entry = Context.Entry<T>(entity);

            if (entry.State != EntityState.Detached)
                Set.Add(entity);
            else
                entry.State = EntityState.Added;
        }

        public void Update(T entity)
        {
            var entry = Context.Entry<T>(entity);

            if (entry.State == EntityState.Detached)
                Set.Attach(entity);
            else
                Context.Entry<T>(GetById(entity.ID)).CurrentValues.SetValues(entity);
        }

        public void Delete(T entity)
        {
            var entry = Context.Entry<T>(entity);

            if (entry.State != EntityState.Deleted)
                entry.State = EntityState.Deleted;
            else
            {
                Set.Attach(entity);
                Set.Remove(entity);
            }
        }

        public bool Any(Func<T, bool> where)
        {
            return Set.Any(where);
        }
    }
}
