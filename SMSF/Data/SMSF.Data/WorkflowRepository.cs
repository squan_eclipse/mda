﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace DBASystem.SMSF.Data
{
    internal class WorkflowRepository<T> : IWorkflowRepository<T> where T : WorkflowBase
    {
        internal SMSFContext Context { get; set; }
        protected DbSet<T> Set { get; set; }

        internal WorkflowRepository(SMSFContext context)
        {
            Context = context;
            Set = Context.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return Set;
        }

        public IQueryable<T> GetAll(Func<T, bool> where)
        {
            return Set.Where(where).AsQueryable<T>();
        }

        public IQueryable<T> GetAll(Func<T, Boolean> where, string[] includes)
        {
            DbQuery<T> query = Set;

            foreach(var include in includes)
            {
               query = query.Include(include);
            }

            return query.Where(where).AsQueryable<T>();
        }

        public T GetById(Guid Id)
        {
            return Set.FirstOrDefault(a => a.Id == Id);
        }

        public T Get(Func<T, bool> where)
        {
            return Set.FirstOrDefault(where);
        }

        public void Add(T entity)
        {
            var entry = Context.Entry<T>(entity);

            if (entry.State != EntityState.Detached)
                Set.Add(entity);
            else
                entry.State = EntityState.Added;
        }

        public void Update(T entity)
        {
            var entry = Context.Entry<T>(entity);

            if (entry.State != EntityState.Detached)
                Set.Attach(entity);
            else
                Context.Entry<T>(GetById(entity.Id)).CurrentValues.SetValues(entity);
        }

        public void Delete(T entity)
        {
            var entry = Context.Entry<T>(entity);

            if (entry.State != EntityState.Deleted)
                entry.State = EntityState.Deleted;
            else
            {
                Set.Attach(entity);
                Set.Remove(entity);
            }
        }

        public bool Any(Func<T, bool> where)
        {
            return Set.Any(where);
        }
    }
}
