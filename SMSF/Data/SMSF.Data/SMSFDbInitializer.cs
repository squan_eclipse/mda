﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DBASystem.SMSF.Data
{
    internal partial class SMSFDbInitializer : DropCreateDatabaseIfModelChanges<SMSFContext>
    {
        private Contracts.Common.Status active = Contracts.Common.Status.Active;

        protected override void Seed(SMSFContext context)
        {
            AddRoles(context);
            AddEntityCategories(context);
            AddEntityTypes(context);
            AddGroups(context);
            AddAddressTypes(context);
            AddTelephoneTypes(context);
            AddCountries(context);
            AddSettingTypes(context);

            AddGroupRoles(context);
            AddOptionTypes(context);
            AddEntities(context);
            AddAccountManager(context);
            AddUsers(context);
            AddStates(context);
            AddSettings(context);
            AddAccountRoles(context);
            AddJobManagerRoles(context);

            AddWorkflow(context);
            AddAuditProcedures(context);

            AddTemplateProviders(context);
            AddTemplateCategories(context);
            AddTemplates(context);
            AddWorkflowStatus(context);

            base.Seed(context);
        }

        #region Seed Data

        private void AddRoles(SMSFContext context)
        {
            var roles = new List<Roles>
            {
                new Roles{ ID = 1, Title = "Admin", Description = "Administrator", IsSystem = true, StatusID = active },
                new Roles{ ID = 2, Title = "Auditor", Description = "Auditor", IsSystem = true, StatusID = active },
                new Roles{ ID = 3, Title = "Accountant", Description = "Accountant", IsSystem = true, StatusID = active },
                new Roles{ ID = 4, Title = "Client", Description = "Client", IsSystem = true, StatusID = active },
        
            };

            context.Roles.AddRange(roles);
            context.SaveChanges();
        }

        private void AddEntityCategories(SMSFContext context)
        {
            var categories = new List<EntityCategories>
            {
                new EntityCategories{ ID = 1, Description = "System", StatusID = active },
                new EntityCategories{ ID = 2, Description = "Club or Society", StatusID = active },
                new EntityCategories{ ID = 3, Description = "Company", StatusID = active },
                new EntityCategories{ ID = 4, Description = "Individual", StatusID = active },
                new EntityCategories{ ID = 5, Description = "Non-profit Organisation", StatusID = active },
                new EntityCategories{ ID = 6, Description = "Partnership", StatusID = active },
                new EntityCategories{ ID = 7, Description = "SMSF", StatusID = active },
                new EntityCategories{ ID = 8, Description = "Sole Trader", StatusID = active },
                new EntityCategories{ ID = 9, Description = "Superannuation Fund", StatusID = active },
                new EntityCategories{ ID = 10, Description = "Trust", StatusID = active }
            };

            context.EntityCategories.AddRange(categories);
            context.SaveChanges();
        }

        private void AddEntityTypes(SMSFContext context)
        {
            var types = new List<EntityTypes>
            {
                new EntityTypes { ID = 1, Description = "Entity", StatusID = active },
                new EntityTypes { ID = 2, Description = "Group", StatusID = active },
                new EntityTypes { ID = 3, Description = "Client", StatusID = active },
                new EntityTypes { ID = 4, Description = "Fund", StatusID = active }
            };

            context.EntityTypes.AddRange(types);
            context.SaveChanges();
        }

        private void AddGroups(SMSFContext context)
        {
            var groups = new List<Groups>
            {
                new Groups{ ID = 1, Identifier = Guid.NewGuid() , Description = "Administrators group", IsSystem = true, StatusID = active, Title = "Administrators" },
                //new Groups{ ID = 2, Identifier = Guid.NewGuid() , Description = "Entity administrators group", IsSystem = true, StatusID = active, Title = "Administrators" },
                new Groups{ ID = 3, Identifier = Guid.NewGuid() , Description = "Users perform the business-operations e.g. Accountants, Auditors, Managers etc", IsSystem = true, StatusID = active, Title = "Operators" },
                new Groups{ ID = 4, Identifier = Guid.NewGuid() , Description = "Clients group", IsSystem = true, StatusID = active, Title = "Clients" }
            };

            context.Groups.AddRange(groups);
            context.SaveChanges();
        }

        private void AddGroupRoles(SMSFContext context)
        {
            var groupRoles = new List<GroupRoles>
            {
                new GroupRoles { GroupID = 1, RoleID = 1 },
                new GroupRoles { GroupID = 3, RoleID = 4 }
            };

            context.GroupRoles.AddRange(groupRoles);
            context.SaveChanges();
        }

        private void AddOptionTypes(SMSFContext context)
        {
            var types = new List<OptionTypes>
            {
                new OptionTypes { ID = 1, Description = "Page", StatusID = active, Title = "Page" },
                new OptionTypes { ID = 2, Description = "Menu", StatusID = active, Title = "Main" },
                new OptionTypes { ID = 3, Description = "NextPage", StatusID = active, Title = "Main" },
                new OptionTypes { ID = 4, Description = "Report", StatusID = active, Title = "Main" },
                new OptionTypes { ID = 5, Description = "View", StatusID = active, Title = "Read Only Access" },
                new OptionTypes { ID = 6, Description = "Modify", StatusID = active, Title = "Edit Update" },
                new OptionTypes { ID = 7, Description = "New/Add", StatusID = active, Title = "Add Update" },
                new OptionTypes { ID = 8, Description = "Remove", StatusID = active, Title = "Delete Update" },
                new OptionTypes { ID = 9, Description = "Cancel", StatusID = active, Title = "Cancel" },
                new OptionTypes { ID = 10, Description = "Print", StatusID = active, Title = "Print" },
                new OptionTypes { ID = 11, Description = "Search", StatusID = active, Title = "Search" },
                new OptionTypes { ID = 12, Description = "Maker", StatusID = active, Title = "Maker" },
                new OptionTypes { ID = 13, Description = "Checker", StatusID = active, Title = "Checker" },
                new OptionTypes { ID = 14, Description = "CustomAction", StatusID = active, Title = "CustomAction" }
            };

            context.OptionTypes.AddRange(types);
            context.SaveChanges();
        }

        private void AddEntities(SMSFContext context)
        {
            var entities = new List<Entities>
            {
                new Entities {
                     ID = 1, Identifier =  Guid.NewGuid(), CategoryID = 1, TypeID = 1, CreatedBy = 0, CreatedOn = DateTime.Now, Name = "SYSTEM", StatusID = active, ModifiedBy = 0, ModifiedOn = DateTime.Now },
                new Entities {
                     ID = 2, Identifier =  Guid.NewGuid(), CategoryID = 3, TypeID = 1, CreatedBy = 0, CreatedOn = DateTime.Now, Name = "DBA Advisory Pty Ltd", StatusID = active, ParentID = 1, ABN = "56-456-654-656", ModifiedBy = 0, ModifiedOn = DateTime.Now },
                new Entities {
                     ID = 3, Identifier =  Guid.NewGuid(), CategoryID = 3, TypeID = 1, CreatedBy = 0, CreatedOn = DateTime.Now, Name = "DBA System Pty Ltd", StatusID = active, ParentID = 1, ABN = "56-457-569-235", ModifiedBy = 0, ModifiedOn = DateTime.Now },
                new Entities {
                     ID = 4, Identifier =  Guid.NewGuid(), CategoryID = 3, TypeID = 1, CreatedBy = 0, CreatedOn = DateTime.Now, Name = "DBA Global Shared Services Inc", StatusID = active, ParentID = 2, ABN = "52-538-654-563", ModifiedBy = 0, ModifiedOn = DateTime.Now },
            };

            context.Entities.AddRange(entities);
            context.SaveChanges();
        }

        private void AddAccountManager(SMSFContext context)
        {
            var managers = new List<AccountManagers>
            {
                new AccountManagers {
                        Identifier =  Guid.NewGuid(),
                        EntityID = 1, ModifiedBy = 0, ModifiedOn = DateTime.Now,
                        CreatedBy = 0, CreatedOn = DateTime.Now, Email = "admin@dbasystem.com", FirstName = "System", LastName = "Administrator",
                        StatusID = active, Title = "Mr" },
                new AccountManagers {
                        Identifier =  Guid.NewGuid(),
                        EntityID = 2, ModifiedBy = 0, ModifiedOn = DateTime.Now,
                        CreatedBy = 0, CreatedOn = DateTime.Now, Email = "dparazo@dbaadvisory.com", FirstName = "Darlow", LastName = "Parazo",
                        StatusID = active , Title = "Mr" }
            };

            context.AccountManagers.AddRange(managers);
            context.SaveChanges();
        }

        private void AddUsers(SMSFContext context)
        {
            var users = new List<Users>
            {
                new Users { Identifier = Guid.NewGuid(),
                    CreatedBy = 0, CreatedOn = DateTime.Now, ModifiedBy = 0, ModifiedOn = DateTime.Now, 
                    Email = "admin@dbasystem.com", Password = "21232F297A57A5A743894A0E4A801FC3", StatusID = active,
                    GroupID = 1, ID = 1 },
                new Users {
                    Identifier =  Guid.NewGuid(),
                    CreatedBy = 0, CreatedOn = DateTime.Now, ModifiedBy = 0, ModifiedOn = DateTime.Now, 
                    Email = "dparazo@dbaadvisory.com", Password = "B78D7CD4555821042A70D9EC034B0DEA", StatusID = active,
                    GroupID = 1, ID = 2 }
            };

            context.Users.AddRange(users);
            context.SaveChanges();
        }

        private void AddAddressTypes(SMSFContext context)
        {
            var types = new List<AddressTypes>
            {
                new AddressTypes { ID = 1, Description = "Business", StatusID = active },
                new AddressTypes { ID = 2, Description = "Mailing", StatusID = active },
                new AddressTypes { ID = 3, Description = "PO Box", StatusID = active }
            };

            context.AddressTypes.AddRange(types);
            context.SaveChanges();
        }

        private void AddTelephoneTypes(SMSFContext context)
        {
            var types = new List<TelephoneTypes>
            {
                new TelephoneTypes { ID = 1, Name = "Fax", StatusID = active },
                new TelephoneTypes { ID = 2, Name = "Mobile", StatusID = active },
                new TelephoneTypes { ID = 3, Name = "Fixed Line", StatusID = active },
            };

            context.TelephoneTypes.AddRange(types);
            context.SaveChanges();
        }

        private void AddCountries(SMSFContext context)
        {
            var countries = new List<Countries>
            {
                new Countries { ID = 1, Code = "+61", Default = true, Name = "Australia" }
            };

            context.Countries.AddRange(countries);
            context.SaveChanges();
        }

        private void AddStates(SMSFContext context)
        {
            var states = new List<States>
            {
                new States { ID = 1, Name = "Australian Capital Territory", CountryID = 1 },
                new States { ID = 2, Name = "New South Wales", CountryID = 1 },
                new States { ID = 3, Name = "Northern Territory", CountryID = 1 },
                new States { ID = 4, Name = "Queensland", CountryID = 1 },
                new States { ID = 5, Name = "South Australia", CountryID = 1 },
                new States { ID = 6, Name = "Tasmania", CountryID = 1 },
                new States { ID = 7, Name = "Victoria", CountryID = 1 },
                new States { ID = 8, Name = "Western Australia", CountryID = 1 }
            };

            context.States.AddRange(states);
            context.SaveChanges();
        }

        private void AddSettingTypes(SMSFContext context)
        {
            var types = new List<SettingTypes>
            {
                new SettingTypes { ID = 1, Description = "Email Settings", StatusID = active },
                new SettingTypes { ID = 2, Description = "Password Settings", StatusID = active },
                new SettingTypes { ID = 3, Description = "Session Settings", StatusID = active },
                new SettingTypes { ID = 4, Description = "Sharepoint Settings", StatusID = active },
                new SettingTypes { ID = 5, Description = "Report Server Settings", StatusID = active },
            };

            context.SettingTypes.AddRange(types);
            context.SaveChanges();
        }

        private void AddSettings(SMSFContext context)
        {
            var emailSettingId = 1;
            var passwordSettingId = 2;
            var sessionSettingId = 3;
            var sharepointSettingId = 4;
            var reportServerId = 5;

            var settings = new List<Settings>
            {
                new Settings { ID = 1, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SMTPHost", SettingTypeID = emailSettingId, SettingValue = "exchange.dbaadvisory.com", StatusID = active },
                new Settings { ID = 2, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SMTPUserName", SettingTypeID = emailSettingId, SettingValue = "dbaadvisory\\dev.test", StatusID = active },
                new Settings { ID = 3, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SMTPPassword", SettingTypeID = emailSettingId, SettingValue = "devtest1", StatusID = active },
                new Settings { ID = 4, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SMTPFromAddress", SettingTypeID = emailSettingId, SettingValue = "dtest@dbaadvisory.com", StatusID = active },
                new Settings { ID = 5, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SMTPPort", SettingTypeID = emailSettingId, SettingValue = "587", StatusID = active },
                new Settings { ID = 6, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SMTPRequireSSL", SettingTypeID = emailSettingId, SettingValue = "false", StatusID = active },

                new Settings { ID = 7, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "MinPasswordLength", SettingTypeID = passwordSettingId, SettingValue = "6", StatusID = active },
                new Settings { ID = 8, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "MinUpperCaseCharacters", SettingTypeID = passwordSettingId, SettingValue = "0", StatusID = active },
                new Settings { ID = 9, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "MinLowerCaseCharacters", SettingTypeID = passwordSettingId, SettingValue = "0", StatusID = active },
                new Settings { ID = 10, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "MinNumericCharacters", SettingTypeID = passwordSettingId, SettingValue = "3", StatusID = active },
                new Settings { ID = 11, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "MaxPasswordRetries", SettingTypeID = passwordSettingId, SettingValue = "3", StatusID = active },

                new Settings { ID = 12, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "MaxSessionTimeOut", SettingTypeID = sessionSettingId, SettingValue = "20", StatusID = active },

                new Settings { ID = 13, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SharePointUrl", SettingTypeID = sharepointSettingId, SettingValue = "http://www.dbasystem.com.au:8080/", StatusID = active },
                new Settings { ID = 14, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SharePointUserName", SettingTypeID = sharepointSettingId, SettingValue = @"Dbasystem\administrator", StatusID = active },
                new Settings { ID = 15, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SharePointPassword", SettingTypeID = sharepointSettingId, SettingValue = "$dbasystem#1", StatusID = active },
                new Settings { ID = 16, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SharePointMaxFileSize", SettingTypeID = sharepointSettingId, SettingValue = "1", StatusID = active },
                new Settings { ID = 17, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SharePointAllowedFileType", SettingTypeID = sharepointSettingId, SettingValue = "doc,xlsx", StatusID = active },
                new Settings { ID = 18, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "SharePointJobDefaultDocumentsPath", SettingTypeID = sharepointSettingId, SettingValue = "DefaultDocuments/Job/", StatusID = active },
                
                new Settings { ID = 19, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "ReportServerUrl", SettingTypeID = reportServerId, SettingValue = "http://localhost:80/ReportServer", StatusID = active },
                new Settings { ID = 20, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "ReportServerUsername", SettingTypeID = reportServerId, SettingValue = "", StatusID = active },
                new Settings { ID = 21, CreatedBy = 1, CreatedOn = DateTime.Now, Description = "ReportServerPassword", SettingTypeID = reportServerId, SettingValue = "", StatusID = active },
            };

            context.Settings.AddRange(settings);
            context.SaveChanges();
        }

        private void AddAccountRoles(SMSFContext context)
        {
            var roles = new List<AccountRoles> {
                new AccountRoles { ID = 1, Name = "Auditor",    StatusID = Contracts.Common.Status.Active, WorkflowType = Contracts.Common.WorkflowActorType.ExecuteRule },
                new AccountRoles { ID = 2, Name = "Manager",    StatusID = Contracts.Common.Status.Active, WorkflowType = Contracts.Common.WorkflowActorType.ExecuteRule },
                new AccountRoles { ID = 3, Name = "Accountant", StatusID = Contracts.Common.Status.Active, WorkflowType = Contracts.Common.WorkflowActorType.ExecuteRule },
            };

            context.AccountRoles.AddRange(roles);
            context.SaveChanges();
        }

        private void AddWorkflow(SMSFContext context)
        {
            var workflow = new List<Workflow>
            {
                new Workflow { ID = 1, Name = "Job Process" },
            };

            var parameters = new List<Parameters>
            {
                #region Parameters
                new Parameters
                {
                    ID = 1,
                    Name = "Comment",
                    Type = "System.String",
                    Purpose = Contracts.Common.ParameterPurpose.Temporary
                },
                new Parameters
                {
                    ID = 2,
                    Name = "ProcessId",
                    Type = "System.Guid",
                    Purpose = Contracts.Common.ParameterPurpose.System
                },
                new Parameters
                {
                    ID = 3,
                    Name = "PreviousState",
                    Type = "System.String",
                    Purpose = Contracts.Common.ParameterPurpose.System
                },
                new Parameters
                {
                    ID = 4,
                    Name = "CurrentState",
                    Type = "System.String",
                    Purpose = Contracts.Common.ParameterPurpose.Persistence
                },
                new Parameters
                {
                    ID = 5,
                    Name = "PreviousStateForDirect",
                    Type = "System.String",
                    Purpose = Contracts.Common.ParameterPurpose.Persistence
                },
                new Parameters
                {
                    ID = 6,
                    Name = "IsPreExecution",
                    Type = "System.Boolean",
                    Purpose = Contracts.Common.ParameterPurpose.System
                },
                new Parameters
                {
                    ID = 7,
                    Name = "ConditionResult",
                    Type = "System.Boolean",
                    Purpose = Contracts.Common.ParameterPurpose.System
                }
                #endregion
            };

            var activityStates = new List<ActivityState>
            {

                #region JobProcess States

                new ActivityState {ID = 1, Name = "Job Initiated"},
                new ActivityState {ID = 2, Name = "Document Check & Data Import"},
                new ActivityState {ID = 3, Name = "Balance Sheet Review"},
                new ActivityState {ID = 4, Name = "Income Statements Review"},
                new ActivityState {ID = 5, Name = "SIS Compliance Audit"},
                new ActivityState {ID = 6, Name = "Review"},
                new ActivityState {ID = 7, Name = "Sign Off"},
                new ActivityState {ID = 8, Name = "Client Inquiry "},
                new ActivityState {ID = 9, Name = "Audit Complete "},

                #endregion JobProcess States
            };

            var actions = new List<Actions>
            {
                #region Job Process Actions

                // Type="DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business", Method = "StartJobImport"

                new Actions
                {
                    ID = 1,
                    Name = "Data Import",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "DataImport",
                    IsConditional = false
                },

                new Actions
                {
                    ID = 2,
                    Name = "Balance Sheet Review",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "BalanceSheetReview",
                    IsConditional = false
                },
                 new Actions
                {
                    ID = 3,
                    Name = "Income Statement Review",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "IncomeStatementReview",
                    IsConditional = false
                },
                new Actions
                {
                    ID = 4,
                    Name = "SIS Compliance Audit",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "SisComplianceAudit",
                    IsConditional = false
                },
                new Actions
                {
                    ID = 5,
                    Name = "Review",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "ProcessReview",
                    IsConditional = false
                },
                new Actions
                {
                    ID = 6,
                    Name = "Sign Off",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "ProcessSignOff",
                    IsConditional = false
                },
                new Actions
                {
                    ID = 7,
                    Name = "Client Response",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "ClientResponse",
                    IsConditional = false
                },
                 new Actions
                {
                    ID = 8,
                    Name = "Job Complete",
                    Type = "DBASystem.SMSF.Service.Business.WorkflowHelper.JobProcess.Actions,DBASystem.SMSF.Service.Business",
                    Method = "JobComplete",
                    IsConditional = false
                }
                #endregion Job Process Actions
            };

            var actionParameters = new List<ActionParameters>
            {

                #region Job Process Action Parameters

                new ActionParameters {ActionID = 1, ParameterID = 2, Order = 1, IsOutput = false},
                new ActionParameters {ActionID = 2, ParameterID = 2, Order = 1, IsOutput = false},
                new ActionParameters {ActionID = 3, ParameterID = 2, Order = 1, IsOutput = false},
                new ActionParameters {ActionID = 4, ParameterID = 2, Order = 1, IsOutput = false},
                new ActionParameters {ActionID = 5, ParameterID = 2, Order = 1, IsOutput = false},
                new ActionParameters {ActionID = 6, ParameterID = 2, Order = 1, IsOutput = false},
                new ActionParameters {ActionID = 7, ParameterID = 2, Order = 1, IsOutput = false},
                new ActionParameters {ActionID = 8, ParameterID = 2, Order = 1, IsOutput = false}

                #endregion Job Process Action Parameters
            };

            var commands = new List<Commands>
            {

                #region Job Process Commands

                new Commands {ID = 1, Name = "Send For Balance Sheet Review"},
                new Commands {ID = 2, Name = "Get Client Response"},
                new Commands {ID = 3, Name = "Forward To Audit"},
                new Commands {ID = 4, Name = "Send For Income Statement Review"},
                new Commands {ID = 5, Name = "Send For SIS Compliance"},
                new Commands {ID = 6, Name = "Send For Review"},
                new Commands {ID = 7, Name = "Send For Sign Off"},
                new Commands {ID = 8, Name = "Force Send To Audit"},
                new Commands {ID = 9, Name = "Audit Complete"},

                #endregion Job Process Commands
            };

            var activities = new List<Activities>
            {

                #region Job Process Activities
 
                new Activities
                {
                    ID = 1,
                    Name = "Document Check & Data Import",
                    StateID = 2,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = true,
                    WorkflowID = 1,
                    ActivityView = "/Imports/?jobId=[ID]"
                },
                new Activities
                {
                    ID = 2,
                    Name = "Balance Sheet Review",
                    StateID = 3,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/Imports/BalanceSheetReview/[ID]"
                },
                new Activities
                {
                    ID = 3,
                    Name = "Income Statements Review",
                    StateID = 4,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/Imports/IncomeStatementReview/[ID]"
                },
                new Activities
                {
                    ID = 4,
                    Name = "SIS Compliance Audit",
                    StateID = 5,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/Job/WorkPaper?jobId=[ID]"
                },
                new Activities
                {
                    ID = 5,
                    Name = "Review",
                    StateID = 6,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/Job/WorkPaper?jobId=[ID]"
                },
                new Activities
                {
                    ID = 6,
                    Name = "Sign Off",
                    StateID = 7,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/Job/WorkPaper?jobId=[ID]"
                },
                new Activities
                {
                    ID = 7,
                    Name = "Data Import Client Response",
                    StateID = 8,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/job/Detail/[ID]"
                },
                new Activities
                {
                    ID = 8,
                    Name = "Balance Sheet Client Response",
                    StateID = 8,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/job/Detail/[ID]"
                },
                new Activities
                {
                    ID = 9,
                    Name = "Income Statement Client Response",
                    StateID = 8,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/job/Detail/[ID]"
                },
                new Activities
                {
                    ID = 10,
                    Name = "SIS Client Response",
                    StateID = 8,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/job/Detail/[ID]"
                },
                new Activities
                {
                    ID = 11,
                    Name = "Review Client Response",
                    StateID = 8,
                    IsFinal = false,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                    ActivityView = "/job/Detail/[ID]"
                },
                new Activities
                {
                    ID = 12,
                    Name = "Job Complete",
                    StateID = 9,
                    IsFinal = true,
                    IsForSetState = true,
                    IsInitial = false,
                    WorkflowID = 1,
                     ActivityView = "/job/Detail/[ID]"
                },

                #endregion Job Process Activities
            };

            var activityActions = new List<ActivityActions>
            {
                #region Job process Activities Actions

                new ActivityActions {ActionID = 1, ActivityID = 1, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 2, ActivityID = 2, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 3, ActivityID = 3, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 4, ActivityID = 4, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 5, ActivityID = 5, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 6, ActivityID = 6, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 7, ActivityID = 8, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 7, ActivityID = 9, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 7, ActivityID = 10, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 7, ActivityID = 11, IsPreExecution = false, Order = 1},
                new ActivityActions {ActionID = 8, ActivityID = 12, IsPreExecution = false, Order = 1},

                #endregion Job process Activities Actions
            };

            var transitions = new List<Transitions>
            {
                #region Job Process Transition


                new Transitions
                {
                    ID = 1,
                    WorkflowID = 1,
                    Name = "DataImport",
                    FromActivityID = 1,
                    ToActivityID = 2,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 1,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 2,
                    WorkflowID = 1,
                    Name = "DataImportClientRequest",
                    FromActivityID = 1,
                    ToActivityID = 7,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 2,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 3,
                    WorkflowID = 1,
                    Name = "DataImportClientResponse",
                    FromActivityID = 7,
                    ToActivityID = 1,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 3,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 4,
                    WorkflowID = 1,
                    Name = "BalanceSheetReview",
                    FromActivityID = 2,
                    ToActivityID = 3,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 4,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 5,
                    WorkflowID = 1,
                    Name = "BalanceSheetClientRequest",
                    FromActivityID = 2,
                    ToActivityID = 8,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 2,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 6,
                    WorkflowID = 1,
                    Name = "BalanceSheetClientResponse",
                    FromActivityID = 8,
                    ToActivityID = 2,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 3,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 7,
                    WorkflowID = 1,
                    Name = "IncomeStatement",
                    FromActivityID = 3,
                    ToActivityID = 4,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 5,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 8,
                    WorkflowID = 1,
                    Name = "IncomeStatementClientRequest",
                    FromActivityID = 3,
                    ToActivityID = 9,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 2,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 9,
                    WorkflowID = 1,
                    Name = "IncomeStatementClientResponse",
                    FromActivityID = 9,
                    ToActivityID = 3,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 3,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 10,
                    WorkflowID = 1,
                    Name = "SISCompliance",
                    FromActivityID = 4,
                    ToActivityID = 5,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 6,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 11,
                    WorkflowID = 1,
                    Name = "SISClientRequest",
                    FromActivityID = 4,
                    ToActivityID = 10,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 2,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 12,
                    WorkflowID = 1,
                    Name = "SISClientResponse",
                    FromActivityID = 10,
                    ToActivityID = 4,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 3,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 13,
                    WorkflowID = 1,
                    Name = "Review",
                    FromActivityID = 5,
                    ToActivityID = 6,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 7,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 14,
                    WorkflowID = 1,
                    Name = "ReviewClientRequest",
                    FromActivityID = 5,
                    ToActivityID = 11,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 2,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 15,
                    WorkflowID = 1,
                    Name = "ReviewClientResponse",
                    FromActivityID = 11,
                    ToActivityID = 5,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 3,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 16,
                    WorkflowID = 1,
                    Name = "Data Import Client Override",
                    FromActivityID = 7,
                    ToActivityID = 1,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 8,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 17,
                    WorkflowID = 1,
                    Name = "Balance Sheet Client Override",
                    FromActivityID = 8,
                    ToActivityID = 2,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 8,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 18,
                    WorkflowID = 1,
                    Name = "Income Statement Client Override",
                    FromActivityID = 9,
                    ToActivityID = 3,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 8,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 19,
                    WorkflowID = 1,
                    Name = "SIS  Client Override",
                    FromActivityID = 10,
                    ToActivityID = 4,
                    Classifier = Contracts.Common.TransitionClassifier.Reverse,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 8,
                    PreExecutionResult = 0
                },
                new Transitions
                {
                    ID = 20,
                    WorkflowID = 1,
                    Name = "Review Client Override",
                    FromActivityID = 11,
                    ToActivityID = 5,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID = 8,
                    PreExecutionResult = 0
                },
                 new Transitions
                {
                    ID = 21,
                    WorkflowID = 1,
                    Name = "Job Complete",
                    FromActivityID = 6,
                    ToActivityID = 12,
                    Classifier = Contracts.Common.TransitionClassifier.Direct,
                    CommandType = Contracts.Common.TransitionCommandType.Command,
                    ActionType = Contracts.Common.TransitionActionType.Always,
                    CommandID =9,
                    PreExecutionResult = 0
                },

                #endregion Job Process Transition
            };

            var actors = new List<Actors>
            {
                #region Actors

                new Actors {ID = 1, Name = "Audit Admin", MethodName = "IsAuditAdmin"},
                new Actors {ID = 2, Name = "Balance Sheet Auditor", MethodName = "IsBalanceSheetAuditor"},
                new Actors {ID = 3, Name = "Income Statement Auditor", MethodName = "IsIncomeStatementAuditor"},
                new Actors {ID = 4, Name = "SIS Compliance Auditor", MethodName = "IsSISAuditor"},
                new Actors {ID = 5, Name = "Review Manager", MethodName = "IsReviewManager"},
                new Actors {ID = 6, Name = "Sign Off Manager", MethodName = "IsSignOffManager"},
                new Actors {ID = 7, Name = "ClientUser", MethodName = "IsClientUser"},

            #endregion Actors

               

            };

            var transitionActors = new List<TransitionActors> {

                #region Job Import Transaction Actors

		        new TransitionActors { ActorID = 1, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 1 },
                new TransitionActors { ActorID = 1, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 2 },
                new TransitionActors { ActorID = 7, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 3 },

                new TransitionActors { ActorID = 2, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 4 },
                new TransitionActors { ActorID = 2, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 5 },
                new TransitionActors { ActorID = 7, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 6 },

                new TransitionActors { ActorID = 3, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 7 },
                new TransitionActors { ActorID = 3, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 8 },
                new TransitionActors { ActorID = 7, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 9 },

                new TransitionActors { ActorID = 4, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 10 },
                new TransitionActors { ActorID = 4, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 11 },
                new TransitionActors { ActorID = 7, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 12 },

                new TransitionActors { ActorID = 5, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 13 },
                new TransitionActors { ActorID = 5, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 14 },
                new TransitionActors { ActorID = 7, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 15 },
                
                new TransitionActors { ActorID = 1, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 16 },
                new TransitionActors { ActorID = 1, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 17 },
                new TransitionActors { ActorID = 1, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 18 },
                new TransitionActors { ActorID = 1, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 19 },
                new TransitionActors { ActorID = 1, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 20 },
                new TransitionActors { ActorID = 6, RestrictionType = Contracts.Common.TransitionRestrictionType.Allow, TransitionID = 21 },
           

	            #endregion Job Import Transaction Actors
            };

            context.Workflow.AddRange(workflow);
            context.Parameters.AddRange(parameters);
            context.Actions.AddRange(actions);
            context.ActionParameters.AddRange(actionParameters);
            context.Commands.AddRange(commands);
            context.ActivityStates.AddRange(activityStates);
            context.Activities.AddRange(activities);
            context.ActivityActions.AddRange(activityActions);
            context.Transitions.AddRange(transitions);
            context.Actors.AddRange(actors);
            context.TransitionActors.AddRange(transitionActors);

            context.SaveChanges();
        }

        private void AddJobManagerRoles(SMSFContext context)
        {
            var jobManagerRoles = new List<AccountManagerJobRoles>
            {
                new AccountManagerJobRoles { ID = 1, Name = "Audit Admin", IsActive = true , Identifier = new Guid("C81231AB-1FFE-433A-A2B1-066476CCE6EE")},
                new AccountManagerJobRoles { ID = 2, Name = "Balance Sheet Auditor", IsActive = true ,Identifier = new Guid("577406D2-46F9-47ED-B6FF-0C3C6BF99BBF")},
                new AccountManagerJobRoles { ID = 3, Name = "Income Statement Auditor",  IsActive = true, Identifier = new Guid("73106F7D-BA51-4C20-B608-4ADF0DF2DD34")},
                new AccountManagerJobRoles { ID = 4, Name = "SIS Compliance Auditor",  IsActive = true, Identifier = new Guid("F53F1E30-62CA-4A43-8E75-BAE26D616BB6")},
                new AccountManagerJobRoles { ID = 5, Name = "Review Manager",  IsActive = true, Identifier = new Guid("BA0A02DB-9AD8-406F-96B6-2AB87C8023FF")},
                new AccountManagerJobRoles { ID = 6, Name = "Sign Off Manager",  IsActive = true, Identifier = new Guid("69BE8D68-6727-408B-B187-3B6B89945F4F")},
                new AccountManagerJobRoles { ID = 7, Name = "Client User",  IsActive = false, Identifier = new Guid("D2BE6DFA-27E1-4DAE-A79A-496FE899EE95")},

          


            };
            context.JobManagerRoles.AddRange(jobManagerRoles);
            context.SaveChanges();
        }

        private void AddTemplateProviders(SMSFContext context)
        {
            var templateProvider = new List<TemplateProvider> {
                                    new TemplateProvider {ID = 1, Name = "BGL", StatusID = Contracts.Common.Status.Active  },
                                    new TemplateProvider {ID = 2, Name = "Class Super", StatusID = Contracts.Common.Status.Active },
                                    new TemplateProvider {ID = 3, Name = "Desktop Super", StatusID = Contracts.Common.Status.Active },
                                    new TemplateProvider {ID = 4, Name = "Supermate", StatusID = Contracts.Common.Status.Active }
            };
            context.TemplateProvider.AddRange(templateProvider);
            context.SaveChanges();
        }

        private void AddTemplateCategories(SMSFContext context)
        {
            var templateCategories = new List<TemplateCategories> {
                                     new TemplateCategories {ID = 1, Name = "Cashflow", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 2, Name = "Financial Position", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 3, Name = "General Ledger", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 4, Name = "Investment Income Summary", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 5, Name = "Investment Movement", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 6, Name = "Investment Summary", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 7, Name = "Operating Statement", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 8, Name = "Realised CGT", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 9, Name = "Tax Reconciliation", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 10, Name = "Trial Balance", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 11, Name = "Unrealised CGT", TemplateProviderID = 3, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 12, Name = "Cashflow", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 13, Name = "Financial Position", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 14, Name = "Investment Disposals", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 15, Name = "Investment Summary", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 16, Name = "Notes To Financial Statement", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 17, Name = "Operating Statement", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 18, Name = "Income Statement", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 19, Name = "Investment Income", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
                                     new TemplateCategories {ID = 20, Name = "Investment Movement", TemplateProviderID = 1, StatusID = Contracts.Common.Status.Active  },
            };
            context.TemplateCategories.AddRange(templateCategories);
            context.SaveChanges();
        }

        private void AddTemplates(SMSFContext context)
        {
            var templates = new List<Templates> {
            new Templates {ID = 1, Name = "Statement of Cashflows", CategoryID = 1,

                #region TemplateText

	            TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

	#endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 2, Name = "Statement of Financial Position", CategoryID = 2,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 3, Name = "General Ledger", CategoryID = 3,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Date</column>
        <column>Type</column>
        <column>Sub-Type</column>
        <column>Member</column>
        <column>Bank</column>
        <column>Ref.</column>
        <column>Details</column>
        <column>Units</column>
        <column>Debit</column>
        <column>Credit</column>
        <column>Balance</column>
    </columns>]]>
  </print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""7"" distance-threshold=""10"" padding-text="""" />
  <values-resolve-overlapping target-columns=""11"" distance-threshold=""10""/>
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
        switch(values.current.value)
        {
          case 'Equities':
          case 'Trust':
            elementTAG='heading';
          break;
        }
      }
      else if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 4, Name = "Investment Income Summary", CategoryID = 4,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Accounting Income</column>
        <column>Frank. Credits</column>
        <column>Tax FY</column>
        <column>Tax Deferred</column>
        <column>Tax Free</column>
        <column>Tax Exempt</column>
        <column>Gross Disc.</column>
        <column>Other Gain</column>
        <column>Losses Rec.</column>
        <column>Fgn Income</column>
        <column>Taxable Inc. (ex CGT/Fgn)</column>
        <column>Fgn Income</column>
        <column>Fgn Credits</column>
        <column>Other Credits</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""15"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
      switch(values.current.value)
      {
      case 'Cash at Bank':
      case 'Equities':
      case 'Trust':
      elementTAG='heading';
      break;
      }
      }
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 5, Name = "Investment Movement", CategoryID = 5,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Opening Units</column>
        <column>Opening Cost</column>
        <column>Additions Units</column>
        <column>Additions Cost</column>
        <column>Disposals Units</column>
        <column>Disposals Cost</column>
        <column>Disposals Profit/(Loss)</column>
        <column>Closing Units</column>
        <column>Closing Cost</column>
        <column>Closing Market Value</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""6"" distance-threshold=""10"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
        switch(values.current.value)
        {
          case 'Equities':
          case 'Trust':
            elementTAG='heading';
          break;
        }
      }
      else if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 6, Name = "Investment Summary", CategoryID = 6,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Units</column>
        <column>Average Cost</column>
        <column>Cost</column>
        <column>Average Market</column>
        <column>Market Value</column>
        <column>Accounting Income</column>
        <column>Market Yield</column>
        <column>Total Mkt Value %</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""5"" distance-threshold=""10"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
        switch(values.current.value)
        {
          case 'Equities':
          case 'Trust':
            elementTAG='heading';
          break;
        }
      }
      else if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 7, Name = "Operating Statement", CategoryID = 7,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 8, Name = "Realised CGT", CategoryID = 8,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Member Account</column>
        <column>Sale Date</column>
        <column>Units</column>
        <column>Proceeds</column>
        <column>Cost Base</column>
        <column>Gross Discounted</column>
        <column>Frozen Indexed</column>
        <column>Other Gain</column>
        <column>Loss</column>
        <column>Recouped Losses</column>
        <column>Taxable Gain</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <!--<if condition=""text.getValue()=='Trust'"">
      <break-loop/>
    </if>-->
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""12"" min-columns=""8"" distance-threshold=""18"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
      switch(values.current.value)
      {
      case 'Equities':
      case 'Trust':
      elementTAG='heading';
      break;
      }
      }
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 9, Name = "Tax Reconciliation", CategoryID = 9,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Chart</column>
        <column>Description</column>
        <column>Inv. Code</column>
        <column>Accounting</column>
        <column>Gross Taxable</column>
        <column>Pension Deductions</column>
        <column>Net Taxable</column>
        <column>Difference</column>
        <column>Fgn Income</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""3"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <!--<exec>
      var isNotify = false;
      if (values.line.valuesCount &lt; 9 &amp;&amp; elementTAG == 'item' )
      isNotify = true;
    </exec>-->
    <!--<if condition=""isNotify==true"">
      <then>
        <print><![CDATA[<Notification>PDF Format is not appropriate.</Notification>]]></print>
        <print><![CDATA[</data>]]></print>
        <print><![CDATA[</report>]]></print>
        <break-loop/>
      </then>
      <else>-->
        <print><![CDATA[<row>]]></print>
        <values-each-value>
          <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
        </values-each-value>
        <print><![CDATA[</row>]]></print>
      <!--</else>
    </if>-->
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 10, Name = "Trial Balance", CategoryID = 10,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Last Year</column>
        <column>Chart</column>
        <column>Investment</column>
        <column>Description</column>
        <column>Debit</column>
        <column>Credit</column>
    </columns>]]>
  </print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""5"" min-columns=""4"" distance-threshold=""15"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <!--<exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<totalCols>#!values.line.valuesCount#</totalCols>]]></print>
    -->
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 11, Name = "Unrealised CGT", CategoryID = 11,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Member Account</column>
        <column>Last Valuation</column>
        <column>Units</column>
        <column>Unit Value</column>
        <column>Market Value</column>
        <column>Cost Base</column>
        <column>Gross Discounted</column>
        <column>Frozen Indexed</column>
        <column>Other Gain</column>
        <column>Loss</column>
        <column>Recouped Losses</column>
        <column>Unrealised Taxable Gain</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <!--<if condition=""text.getValue()=='Trust'"">
      <break-loop/>
    </if>-->
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""12"" min-columns=""8"" distance-threshold=""18"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
      switch(values.current.value)
      {
      case 'Equities':
      case 'Trust':
      elementTAG='heading';
      break;
      }
      }
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 12, Name = "Statement of Cashflows", CategoryID = 12,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>

          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
          <print><![CDATA[<columns>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</columns></header>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 13, Name = "Balance Sheet", CategoryID = 13,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>

          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<columns>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</columns></header>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 14, Name = "Investment Disposals", CategoryID = 14,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 15, Name = "Investment Summary", CategoryID = 15,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
   <print><![CDATA[</data></report>]]></print>
  </while>
</template>"

	            #endregion TemplateText

                 , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 16, Name = "Notes To Financial Statement", CategoryID = 16,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>
          
          <print><![CDATA[</header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 17, Name = "Operating Statement", CategoryID = 17,

                #region TemplateText

		             TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>

          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
          <print><![CDATA[<columns>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</columns></header>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>"

	            #endregion TemplateText

                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 18, Name = "Income Statement", CategoryID = 18,
                
                #region TemplateText
                TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>

          <print><![CDATA[</header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>"
                #endregion TemplateText
                
                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 19, Name = "Investment Income", CategoryID = 19,
                
                #region TemplateText
                TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
   <print><![CDATA[</data></report>]]></print>
  </while>
</template>"
                #endregion TemplateText
                
                , CreatedBy = 0, CreatedOn = DateTime.Now
            },
            new Templates {ID = 20, Name = "Investment Movement", CategoryID = 20,
              
                #region TemplateText
                TemplateText=@"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
   <print><![CDATA[</data></report>]]></print>
  </while>
</template>"
                 #endregion TemplateText
                
                , CreatedBy = 0, CreatedOn = DateTime.Now
            }
            };
            context.Templates.AddRange(templates);
            context.SaveChanges();
        }

        private void AddWorkflowStatus(SMSFContext context)
        {
            var status = new List<WorkflowStatus> {
                new WorkflowStatus { ID = 1, Name = "In Progress" },
                new WorkflowStatus { ID = 2, Name = "Completed" },
            };

            context.WorkflowStatus.AddRange(status);
            context.SaveChanges();
        }

        #endregion Seed Data
    }
}