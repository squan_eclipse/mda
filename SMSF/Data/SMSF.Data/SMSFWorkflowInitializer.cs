﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DBASystem.SMSF.Data
{
    internal partial class SMSFDbInitializer : DropCreateDatabaseIfModelChanges<SMSFContext>
    {
        private void CreateDefaultWorkflows(SMSFContext context)
        {
            //  Must add mapping in CreateWorkflowMapings for every workflow
            var workflows = new List<WorkflowProcessSchemes>();
            // Entiy Default Flow
            workflows.Add(new WorkflowProcessSchemes()
            {
                Id = new Guid("266ECAF9-1F67-4004-9D0E-C11D90E62717"),
                IsObsolete = false,
                ProcessName = "266ECAF9-1F67-4004-9D0E-C11D90E62717",
                DefiningParameters = "{}",
                DefiningParametersHash = "r4ztHEDMTwYwDqoEyePFlg==",
                #region shcema xml

                Scheme = @"<Process Name=""EntityCreation"">
                              <Designer X=""0"" Y=""0"" Scale=""1"" />
                              <Actors>
                                <Actor Name=""SystemAdmin"">
                                  <Rule RuleName=""IsSystemAdmin"" />
                                </Actor>
                                <Actor Name=""Administrator"">
                                  <Rule RuleName=""IsAdmin"" />
                                </Actor>
                                <Actor Name=""Operator"">
                                  <Rule RuleName=""IsOperator"" />
                                </Actor>
                                <Actor Name=""Client"">
                                  <Rule RuleName=""IsClient"" />
                                </Actor>
                              </Actors>
                              <Parameters>
                                <Parameter Name=""Comment"" Type=""System.String"" Purpose=""Temporary"" />
                              </Parameters>
                              <Commands>
                                <Command Name=""Approved"" />
                                <Command Name=""Deny"" />
                                <Command Name=""Dummy"" />
                              </Commands>
                              <Actions>
                                <Action Name=""CreateEntity"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""CreateEntity"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                    </InputParameters>
                                  </ExecuteMethod>
                                </Action>
                                <Action Name=""DenyEntityCreate"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""DenyEntityCreate"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                    </InputParameters>
                                  </ExecuteMethod>
                                </Action>
                                <Action Name=""IsEntityCreatedBySysAdmin"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""IsEntityCreatedBySysAdmin"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                      <ParameterRef Order=""1"" NameRef=""IsPreExecution"" />
                                    </InputParameters>
                                    <OutputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ConditionResult"" />
                                    </OutputParameters>
                                  </ExecuteMethod>
                                </Action>
                                <Action Name=""InitiateEntity"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""InitiateEntity"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                    </InputParameters>
                                  </ExecuteMethod>
                                </Action>
                              </Actions>
                              <Activities>
                                <Activity Name=""EntityRequest"" State=""Your request has  been sent for approval."" IsInitial=""True"" IsFinal=""False"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                                  <Implementation>
                                    <ActionRef Order=""1"" NameRef=""InitiateEntity"" />
                                  </Implementation>
                                  <Designer X=""450"" Y=""60"" />
                                </Activity>
                                <Activity Name=""Approved"" State=""Entity created successfully"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                                  <Implementation>
                                    <ActionRef Order=""1"" NameRef=""CreateEntity"" />
                                  </Implementation>
                                  <Designer X=""550"" Y=""260"" />
                                </Activity>
                                <Activity Name=""RequsetDenied"" State=""You Request has been denied By Admin"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                                  <Implementation>
                                    <ActionRef Order=""1"" NameRef=""DenyEntityCreate"" />
                                  </Implementation>
                                  <Designer X=""370"" Y=""260"" />
                                </Activity>
                              </Activities>
                              <Transitions>
                                <Transition Name=""Request"" To=""Approved"" From=""EntityRequest"" Classifier=""Direct"">
                                  <Triggers>
                                    <Trigger Type=""Auto"" />
                                  </Triggers>
                                  <Conditions>
                                    <Condition Type=""Action"" NameRef=""IsEntityCreatedBySysAdmin"" />
                                  </Conditions>
                                  <Designer Bending="""" />
                                </Transition>
                                <Transition Name=""RequestForApproval"" To=""Approved"" From=""EntityRequest"" Classifier=""Direct"">
                                  <Restrictions>
                                    <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                                  </Restrictions>
                                  <Triggers>
                                    <Trigger Type=""Command"" NameRef=""Approved"" />
                                  </Triggers>
                                  <Conditions>
                                    <Condition Type=""Always"" />
                                  </Conditions>
                                  <Designer Bending="""" />
                                </Transition>
                                <Transition Name=""RequestDenied"" To=""RequsetDenied"" From=""EntityRequest"" Classifier=""Direct"">
                                  <Restrictions>
                                    <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                                  </Restrictions>
                                  <Triggers>
                                    <Trigger Type=""Command"" NameRef=""Deny"" />
                                  </Triggers>
                                  <Conditions>
                                    <Condition Type=""Always"" />
                                  </Conditions>
                                  <Designer Bending="""" />
                                </Transition>
                              </Transitions>
                            </Process>"

                #endregion shcema xml
            });
            //Client Default Flow
            workflows.Add(new WorkflowProcessSchemes()
            {
                Id = new Guid("F1493640-1AA7-4552-9C89-8DBA0BDBBE72"),
                IsObsolete = false,
                ProcessName = "F1493640-1AA7-4552-9C89-8DBA0BDBBE72",
                DefiningParameters = "{}",
                DefiningParametersHash = "r4ztHEDMTwYwDqoEyePFlg==",
                #region shcema xml

                Scheme = @"<Process Name=""ClientCreation"">
                          <Designer X=""0"" Y=""0"" Scale=""1"" />
                          <Actors>
                            <Actor Name=""SystemAdmin"">
                              <Rule RuleName=""IsSystemAdmin"" />
                            </Actor>
                            <Actor Name=""Administrator"">
                              <Rule RuleName=""IsAdmin"" />
                            </Actor>
                            <Actor Name=""Operator"">
                              <Rule RuleName=""IsOperator"" />
                            </Actor>
                            <Actor Name=""Client"">
                              <Rule RuleName=""IsClient"" />
                            </Actor>
                          </Actors>
                          <Parameters>
                            <Parameter Name=""Comment"" Type=""System.String"" Purpose=""Temporary"" />
                          </Parameters>
                          <Commands>
                            <Command Name=""Approved"" />
                            <Command Name=""Deny"" />
                            <Command Name=""Dummy"" />
                          </Commands>
                          <Actions>
                            <Action Name=""CreateClient"">
                              <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Client.Actions,DBASystem.SMSF.Service.Business"" MethodName=""CreateClient"">
                                <InputParameters>
                                  <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                </InputParameters>
                              </ExecuteMethod>
                            </Action>
                            <Action Name=""DenyClientCreate"">
                              <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Client.Actions,DBASystem.SMSF.Service.Business"" MethodName=""DenyClientCreate"">
                                <InputParameters>
                                  <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                </InputParameters>
                              </ExecuteMethod>
                            </Action>
                            <Action Name=""IsClientCreatedBySysAdmin"">
                              <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Client.Actions,DBASystem.SMSF.Service.Business"" MethodName=""IsClientCreatedBySysAdmin"">
                                <InputParameters>
                                  <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                  <ParameterRef Order=""1"" NameRef=""IsPreExecution"" />
                                </InputParameters>
                                <OutputParameters>
                                  <ParameterRef Order=""1"" NameRef=""ConditionResult"" />
                                </OutputParameters>
                              </ExecuteMethod>
                            </Action>
                            <Action Name=""InitiateClient"">
                              <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Client.Actions,DBASystem.SMSF.Service.Business"" MethodName=""InitiateClient"">
                                <InputParameters>
                                  <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                </InputParameters>
                              </ExecuteMethod>
                            </Action>
                          </Actions>
                          <Activities>
                            <Activity Name=""ClientRequest"" State=""Your request has  been sent for approval."" IsInitial=""True"" IsFinal=""False"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                              <Implementation>
                                <ActionRef Order=""1"" NameRef=""InitiateClient"" />
                              </Implementation>
                              <Designer X=""450"" Y=""60"" />
                            </Activity>
                            <Activity Name=""Approved"" State=""Client created successfully"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                              <Implementation>
                                <ActionRef Order=""1"" NameRef=""CreateClient"" />
                              </Implementation>
                              <Designer X=""550"" Y=""260"" />
                            </Activity>
                            <Activity Name=""RequsetDenied"" State=""Your Request has been denied By Admin"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                              <Implementation>
                                <ActionRef Order=""1"" NameRef=""DenyClientCreate"" />
                              </Implementation>
                              <Designer X=""370"" Y=""260"" />
                            </Activity>
                          </Activities>
                          <Transitions>
                            <Transition Name=""Request"" To=""Approved"" From=""ClientRequest"" Classifier=""Direct"">
                              <Triggers>
                                <Trigger Type=""Auto"" />
                              </Triggers>
                              <Conditions>
                                <Condition Type=""Action"" NameRef=""IsClientCreatedBySysAdmin"" ResultOnPreExecution=""false"" />
                              </Conditions>
                              <Designer Bending="""" />
                            </Transition>
                            <Transition Name=""RequestForApproval"" To=""Approved"" From=""ClientRequest"" Classifier=""Direct"">
                              <Restrictions>
                                <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                              </Restrictions>
                              <Triggers>
                                <Trigger Type=""Command"" NameRef=""Approved"" />
                              </Triggers>
                              <Conditions>
                                <Condition Type=""Always"" />
                              </Conditions>
                              <Designer Bending="""" />
                            </Transition>
                            <Transition Name=""RequestDenied"" To=""RequsetDenied"" From=""ClientRequest"" Classifier=""Direct"">
                              <Restrictions>
                                <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                              </Restrictions>
                              <Triggers>
                                <Trigger Type=""Command"" NameRef=""Deny"" />
                              </Triggers>
                              <Conditions>
                                <Condition Type=""Always"" />
                              </Conditions>
                              <Designer Bending="""" />
                            </Transition>
                          </Transitions>
                        </Process>"
                #endregion shcema xml
            });

            workflows.Add(new WorkflowProcessSchemes()
            {
                Id = new Guid("266ECAF9-1F67-4004-9D0E-C11D90E62717"),
                IsObsolete = false,
                ProcessName = "266ECAF9-1F67-4004-9D0E-C11D90E62717",
                DefiningParameters = "{}",
                DefiningParametersHash = "r4ztHEDMTwYwDqoEyePFlg==",
                #region shcema xml

                Scheme = @"<Process Name=""FundCreation"">
                  <Designer X=""0"" Y=""0"" Scale=""1"" />
                  <Actors>
                    <Actor Name=""SystemAdmin"">
                      <Rule RuleName=""IsSystemAdmin"" />
                    </Actor>
                    <Actor Name=""Administrator"">
                      <Rule RuleName=""IsAdmin"" />
                    </Actor>
                    <Actor Name=""Operator"">
                      <Rule RuleName=""IsOperator"" />
                    </Actor>
                    <Actor Name=""Client"">
                      <Rule RuleName=""IsClient"" />
                    </Actor>
                  </Actors>
                  <Parameters>
                    <Parameter Name=""Comment"" Type=""System.String"" Purpose=""Temporary"" />
                  </Parameters>
                  <Commands>
                    <Command Name=""Approved"" />
                    <Command Name=""Deny"" />
                    <Command Name=""Dummy"" />
                  </Commands>
                  <Actions>
                    <Action Name=""CreateFund"">
                      <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Fund.Actions,DBASystem.SMSF.Service.Business"" MethodName=""CreateFund"">
                        <InputParameters>
                          <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                        </InputParameters>
                      </ExecuteMethod>
                    </Action>
                    <Action Name=""DenyFundCreate"">
                      <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Fund.Actions,DBASystem.SMSF.Service.Business"" MethodName=""DenyFundCreate"">
                        <InputParameters>
                          <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                        </InputParameters>
                      </ExecuteMethod>
                    </Action>
                    <Action Name=""IsFundCreatedBySysAdmin"">
                      <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Fund.Actions,DBASystem.SMSF.Service.Business"" MethodName=""IsFundCreatedBySysAdmin"">
                        <InputParameters>
                          <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                          <ParameterRef Order=""1"" NameRef=""IsPreExecution"" />
                        </InputParameters>
                        <OutputParameters>
                          <ParameterRef Order=""1"" NameRef=""ConditionResult"" />
                        </OutputParameters>
                      </ExecuteMethod>
                    </Action>
                    <Action Name=""InitiateFund"">
                      <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Fund.Actions,DBASystem.SMSF.Service.Business"" MethodName=""InitiateFund"">
                        <InputParameters>
                          <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                        </InputParameters>
                      </ExecuteMethod>
                    </Action>
                  </Actions>
                  <Activities>
                    <Activity Name=""FundRequest"" State=""Your request has  been sent for approval."" IsInitial=""True"" IsFinal=""False"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                      <Implementation>
                        <ActionRef Order=""1"" NameRef=""InitiateFund"" />
                      </Implementation>
                      <Designer X=""450"" Y=""60"" />
                    </Activity>
                    <Activity Name=""Approved"" State=""Fund created successfully"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                      <Implementation>
                        <ActionRef Order=""1"" NameRef=""CreateFund"" />
                      </Implementation>
                      <Designer X=""550"" Y=""260"" />
                    </Activity>
                    <Activity Name=""RequsetDenied"" State=""Your Request has been denied By Admin"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                      <Implementation>
                        <ActionRef Order=""1"" NameRef=""DenyFundCreate"" />
                      </Implementation>
                      <Designer X=""370"" Y=""260"" />
                    </Activity>
                  </Activities>
                  <Transitions>
                    <Transition Name=""Request"" To=""Approved"" From=""FundRequest"" Classifier=""Direct"">
                      <Triggers>
                        <Trigger Type=""Auto"" />
                      </Triggers>
                      <Conditions>
                        <Condition Type=""Action"" NameRef=""IsFundCreatedBySysAdmin"" ResultOnPreExecution=""false"" />
                      </Conditions>
                      <Designer Bending="""" />
                    </Transition>
                    <Transition Name=""RequestForApproval"" To=""Approved"" From=""FundRequest"" Classifier=""Direct"">
                      <Restrictions>
                        <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                      </Restrictions>
                      <Triggers>
                        <Trigger Type=""Command"" NameRef=""Approved"" />
                      </Triggers>
                      <Conditions>
                        <Condition Type=""Always"" />
                      </Conditions>
                      <Designer Bending="""" />
                    </Transition>
                    <Transition Name=""RequestDenied"" To=""RequsetDenied"" From=""FundRequest"" Classifier=""Direct"">
                      <Restrictions>
                        <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                      </Restrictions>
                      <Triggers>
                        <Trigger Type=""Command"" NameRef=""Deny"" />
                      </Triggers>
                      <Conditions>
                        <Condition Type=""Always"" />
                      </Conditions>
                      <Designer Bending="""" />
                    </Transition>
                  </Transitions>
                </Process>"

                #endregion shcema xml
            });

            workflows.Add(new WorkflowProcessSchemes()
            {
                Id = new Guid("266ECAF9-1F67-4004-9D0E-C11D90E62717"),
                IsObsolete = false,
                ProcessName = "266ECAF9-1F67-4004-9D0E-C11D90E62717",
                DefiningParameters = "{}",
                DefiningParametersHash = "r4ztHEDMTwYwDqoEyePFlg==",
                #region shcema xml

                Scheme = @"<Process Name=""EntityCreation"">
                              <Designer X=""0"" Y=""0"" Scale=""1"" />
                              <Actors>
                                <Actor Name=""SystemAdmin"">
                                  <Rule RuleName=""IsSystemAdmin"" />
                                </Actor>
                                <Actor Name=""Administrator"">
                                  <Rule RuleName=""IsAdmin"" />
                                </Actor>
                                <Actor Name=""Operator"">
                                  <Rule RuleName=""IsOperator"" />
                                </Actor>
                                <Actor Name=""Client"">
                                  <Rule RuleName=""IsClient"" />
                                </Actor>
                              </Actors>
                              <Parameters>
                                <Parameter Name=""Comment"" Type=""System.String"" Purpose=""Temporary"" />
                              </Parameters>
                              <Commands>
                                <Command Name=""Approved"" />
                                <Command Name=""Deny"" />
                                <Command Name=""Dummy"" />
                              </Commands>
                              <Actions>
                                <Action Name=""CreateEntity"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""CreateEntity"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                    </InputParameters>
                                  </ExecuteMethod>
                                </Action>
                                <Action Name=""DenyEntityCreate"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""DenyEntityCreate"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                    </InputParameters>
                                  </ExecuteMethod>
                                </Action>
                                <Action Name=""IsEntityCreatedBySysAdmin"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""IsEntityCreatedBySysAdmin"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                      <ParameterRef Order=""1"" NameRef=""IsPreExecution"" />
                                    </InputParameters>
                                    <OutputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ConditionResult"" />
                                    </OutputParameters>
                                  </ExecuteMethod>
                                </Action>
                                <Action Name=""InitiateEntity"">
                                  <ExecuteMethod Type=""DBASystem.SMSF.Service.Business.WorkflowHelper.Entity.Actions,DBASystem.SMSF.Service.Business"" MethodName=""InitiateEntity"">
                                    <InputParameters>
                                      <ParameterRef Order=""1"" NameRef=""ProcessId"" />
                                    </InputParameters>
                                  </ExecuteMethod>
                                </Action>
                              </Actions>
                              <Activities>
                                <Activity Name=""EntityRequest"" State=""Your request has  been sent for approval."" IsInitial=""True"" IsFinal=""False"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                                  <Implementation>
                                    <ActionRef Order=""1"" NameRef=""InitiateEntity"" />
                                  </Implementation>
                                  <Designer X=""450"" Y=""60"" />
                                </Activity>
                                <Activity Name=""Approved"" State=""Entity created successfully"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                                  <Implementation>
                                    <ActionRef Order=""1"" NameRef=""CreateEntity"" />
                                  </Implementation>
                                  <Designer X=""550"" Y=""260"" />
                                </Activity>
                                <Activity Name=""RequsetDenied"" State=""You Request has been denied By Admin"" IsInitial=""False"" IsFinal=""True"" IsForSetState=""True"" IsAutoSchemeUpdate=""True"">
                                  <Implementation>
                                    <ActionRef Order=""1"" NameRef=""DenyEntityCreate"" />
                                  </Implementation>
                                  <Designer X=""370"" Y=""260"" />
                                </Activity>
                              </Activities>
                              <Transitions>
                                <Transition Name=""Request"" To=""Approved"" From=""EntityRequest"" Classifier=""Direct"">
                                  <Triggers>
                                    <Trigger Type=""Auto"" />
                                  </Triggers>
                                  <Conditions>
                                    <Condition Type=""Action"" NameRef=""IsEntityCreatedBySysAdmin"" />
                                  </Conditions>
                                  <Designer Bending="""" />
                                </Transition>
                                <Transition Name=""RequestForApproval"" To=""Approved"" From=""EntityRequest"" Classifier=""Direct"">
                                  <Restrictions>
                                    <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                                  </Restrictions>
                                  <Triggers>
                                    <Trigger Type=""Command"" NameRef=""Approved"" />
                                  </Triggers>
                                  <Conditions>
                                    <Condition Type=""Always"" />
                                  </Conditions>
                                  <Designer Bending="""" />
                                </Transition>
                                <Transition Name=""RequestDenied"" To=""RequsetDenied"" From=""EntityRequest"" Classifier=""Direct"">
                                  <Restrictions>
                                    <Restriction Type=""Allow"" NameRef=""SystemAdmin"" />
                                  </Restrictions>
                                  <Triggers>
                                    <Trigger Type=""Command"" NameRef=""Deny"" />
                                  </Triggers>
                                  <Conditions>
                                    <Condition Type=""Always"" />
                                  </Conditions>
                                  <Designer Bending="""" />
                                </Transition>
                              </Transitions>
                            </Process>"

                #endregion shcema xml
            });

            context.WorkflowProcessScheme.AddRange(workflows);

            context.SaveChanges();
        }

        private void CreateWorkflowMapings(SMSFContext context)
        {
            var workflowMapping = new List<WorkflowMap>()
            {
               new WorkflowMap{
                   WorkFlowId =   new Guid("266ECAF9-1F67-4004-9D0E-C11D90E62717"),
                   WorkFlowTypeId = 4
               }
            };

            context.WorkflowMapping.AddRange(workflowMapping);
            context.SaveChanges();
        }
    }
}