﻿using DBASystem.SMSF.Data.Models;

namespace DBASystem.SMSF.Data.Extensions
{
    public static class JobExtensions
    {
        public static string GetFolderPath(this Jobs job)
        {
            if (job.Fund == null) return "";

            var folderpath = job.Fund.GetFolderPath();
            return folderpath + job.Title + "/";
        }
    }
}
