﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;

namespace DBASystem.SMSF.Data.Extensions
{
    public static class EntityExtensions
    {
        public static List<string> GetFolderHierarchy(this Entities entity)
        {
            List<string> entityList = new List<string>();
            var e = entity;

            while (e.ParentEntity != null)
            {
                if (e.ParentEntity != null)
                    entityList.Add(e.Name);

                e = e.ParentEntity;
            }
            return entityList;
        }

        public static string GetFolderPath(this Entities entity)
        {
            var folderList = entity.GetFolderHierarchy();
            folderList.Reverse();

            return String.Join("/", folderList) + "/";
        }

        public static IEnumerable<Guid> GetParentEntites(this Entities entity)
        {
            List<Guid> entityList = new List<Guid>();
            var e = entity;

            while (e.ParentEntity != null)
            {
                if (e.ParentEntity != null)
                    entityList.Add((Guid)e.Identifier);

                e = e.ParentEntity;
            }
            return entityList;
        }
    }
}