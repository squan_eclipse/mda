﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBASystem.SMSF.Data.Extensions
{
    public static class UserExtensions
    {
        public static List<Guid> GetAllowedEntites(this Users user, bool includePending = false)
        {
            var allowedEntites = GetChildEntites(user.AccountManager.Entity, includePending);
            allowedEntites.Add((Guid)user.AccountManager.Entity.Identifier);
            return allowedEntites;
        }

        private static List<Guid> GetChildEntites(Entities entity, bool includePending)
        {
            var e = entity.ChildEntities.Where(m => m.StatusID == Status.Active || (includePending && m.StatusID == Status.Pending)).Select(p => p);

            var IdentifierList = e.Select(p => (Guid)p.Identifier).ToList();

            foreach (var childEntity in e)
            {
                var nestedChilds = GetChildEntites(childEntity,includePending);
                IdentifierList.AddRange(nestedChilds);
            }
            return IdentifierList;
        }
    }
}