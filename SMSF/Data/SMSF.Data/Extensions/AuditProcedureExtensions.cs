﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBASystem.SMSF.Data.Extensions
{
    public static class AuditProceduresExtensions
    {
        public static string GetFolderPath(this AuditProcedureAttachment auditProcedure)
        {
            if (auditProcedure.Job == null) return "";

            var folderpath = auditProcedure.Job.GetFolderPath();
            return folderpath + "Audit Procedures" + "/";
       }
        public static string GetSharepointFileName(this AuditProcedureAttachmentDetail attachmentDetail)
        {
            return string.Concat(attachmentDetail.ID, "_", attachmentDetail.FileName);
        }
    }
}
