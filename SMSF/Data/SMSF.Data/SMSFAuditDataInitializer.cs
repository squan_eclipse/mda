﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;

namespace DBASystem.SMSF.Data
{
    internal partial class SMSFDbInitializer : DropCreateDatabaseIfModelChanges<SMSFContext>
    {
        private void AddAuditProcedures(SMSFContext context)
        {
            AuditProcedureOptionDetails(context);
            AuditProcedureTabs(context);
            AuditProcedureTabContents(context);
        }
        private static void AuditProcedureOptionDetails(SMSFContext context)
        {
            var auditProcedureOptionDetails = new List<AuditProcedureOptionDetail>
            {
                new AuditProcedureOptionDetail {ID = 100, Field="Dash", Value="--" },
                new AuditProcedureOptionDetail {ID = 200, Field="NA", Value="NA" },
                new AuditProcedureOptionDetail {ID = 300, Field="Yes", Value="Yes" },
                new AuditProcedureOptionDetail {ID = 400, Field="No", Value="No" },
                new AuditProcedureOptionDetail {ID = 500, Field="Complete", Value="Complete" },
                new AuditProcedureOptionDetail {ID = 600, Field="NotComplete", Value="NotComplete" },
                new AuditProcedureOptionDetail {ID = 700, Field="Comply", Value="Comply" },
                new AuditProcedureOptionDetail {ID = 800, Field="NotComply", Value="NotComply" },
                new AuditProcedureOptionDetail {ID = 900, Field="SafeGuardInPlace", Value="SafeGuardInPlace" },
                new AuditProcedureOptionDetail {ID = 1000, Field="Accumulation", Value="Accumulation" },
                new AuditProcedureOptionDetail {ID = 1100, Field="Pension", Value="Pension" },
                new AuditProcedureOptionDetail {ID = 1200, Field="AccumulationAndPension", Value="Accumulation & Pension" },
                new AuditProcedureOptionDetail {ID = 1300, Field="AcceptAsReasonable", Value="Accept as reasonable" },
                new AuditProcedureOptionDetail {ID = 1400, Field="Unacceptable", Value="Unacceptable" },
                new AuditProcedureOptionDetail {ID = 1500, Field="Significant", Value="Significant" },
                new AuditProcedureOptionDetail {ID = 1600, Field="High", Value="High" },
                new AuditProcedureOptionDetail {ID = 1700, Field="Moderate", Value="Moderate" },
                new AuditProcedureOptionDetail {ID = 1800, Field="Low", Value="Low" },
                new AuditProcedureOptionDetail {ID = 1900, Field="Insignificant", Value="Insignificant" },
                new AuditProcedureOptionDetail {ID = 2000, Field="Considered", Value="Considered" },
                new AuditProcedureOptionDetail {ID = 2100, Field="NotConsidered", Value="NotConsidered" },
                new AuditProcedureOptionDetail {ID = 2200, Field="Agree", Value="Agree" },
                new AuditProcedureOptionDetail {ID = 2300, Field="NotAgree", Value="Not Agree" },
                new AuditProcedureOptionDetail {ID = 2301, Field="NotAvailable", Value="Not Available" },
                new AuditProcedureOptionDetail {ID = 2400, Field="Auto", Value="Auto" },
                new AuditProcedureOptionDetail {ID = 2500, Field="Compiled", Value="Compiled" },
                new AuditProcedureOptionDetail {ID = 2600, Field="NotCompiled", Value="Not Compiled" },
                new AuditProcedureOptionDetail {ID = 2700, Field="17A, 35A, 35B, 35C(2), 52(2)(d), 52(e), 62, 65, 66, 67, 69-71E, 73-75, 80-85, 103, 104A, 109, 126K", Value="17A, 35A, 35B, 35C(2), 52(2)(d), 52(e), 62, 65, 66, 67, 69-71E, 73-75, 80-85, 103, 104A, 109, 126K" },
                new AuditProcedureOptionDetail {ID = 2800, Field="1.06(9A), 4.09, 5.03, 5.08, 6.17, 7.04, 13.12, 13.13, 13.14, 13.18AA", Value="1.06(9A), 4.09, 5.03, 5.08, 6.17, 7.04, 13.12, 13.13, 13.14, 13.18AA" },
                new AuditProcedureOptionDetail {ID = 2900, Field="Special Purpose Financial Reporting Framework", Value="Special Purpose Financial Reporting Framework" },

            };
            context.AuditProcedureOptionDetail.AddRange(auditProcedureOptionDetails);
            context.SaveChanges();

        }
        private static void AuditProcedureTabs(SMSFContext context)
        {
            var auditProcedureTabs = new List<AuditProcedureTab>
            {
                //For import Data
                new AuditProcedureTab {ID = 50, Caption =  "Client Reports", TabOrder = 0, EntityId = null, ContentType= WorkpaperContentType.Import,  IsActive=true},

                //For Audit
                new AuditProcedureTab {ID = 100, Caption =  "Client Assessment", TabOrder = 1, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true},
                new AuditProcedureTab {ID = 200, Caption =  "Planing", TabOrder = 2, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true},
                new AuditProcedureTab {ID = 300, Caption =  "Regulatory Review", TabOrder = 3, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true},
                new AuditProcedureTab {ID = 400, Caption =  "Statement of FP", TabOrder = 4, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true},
                new AuditProcedureTab {ID = 500, Caption =  "Operating Statement", TabOrder = 5, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true},
                new AuditProcedureTab {ID = 600, Caption =  "Investments", TabOrder = 6, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true},
                
                //For Audit
                new AuditProcedureTab {ID = 700, Caption =  "Income", TabOrder = 7, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true},
                new AuditProcedureTab {ID = 800, Caption =  "Members", TabOrder = 8, EntityId = null, ContentType= WorkpaperContentType.AuditProcedure, IsActive=true}, //
                new AuditProcedureTab {ID = 900, Caption =  "Manager Partner Review", TabOrder = 9, EntityId = null,ContentType= WorkpaperContentType.ManagerReview, IsActive=true},
                new AuditProcedureTab {ID = 1000, Caption =  "Contravention Report", TabOrder = 10, EntityId = null,ContentType= WorkpaperContentType.Report, IsActive=true},
                new AuditProcedureTab {ID = 1100, Caption =  "Conclusion", TabOrder = 11, EntityId = null,ContentType= WorkpaperContentType.Report, IsActive=true},
                //new AuditProcedureTabs {ID = 1200, Caption =  "Validation", TabOrder = 12, EntityID = null, IsActive=true},
                
            };

            context.AuditProcedureTab.AddRange(auditProcedureTabs);
            context.SaveChanges();
        }
        private static void AuditProcedureTabContents(SMSFContext context)
        {
            AuditProcedureIntoTAB_1(context);
            AuditProcedureIntoTAB_2(context);
            AuditProcedureIntoTAB_3(context);
            AuditProcedureIntoTAB_4(context);
            AuditProcedureIntoTAB_5(context);
            AuditProcedureIntoTAB_6(context);
            AuditProcedureIntoTAB_7(context);
            AuditProcedureIntoTAB_8(context);
            AuditProcedureIntoTAB_9(context);
            AuditProcedureIntoTAB_10(context);
            AuditProcedureIntoTAB_11(context);

        }

        #region "Tab_1"
        private static void AuditProcedureIntoTAB_1(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 1000, AuditProcedureTabId=100, Caption =  "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=1,IsActive=true},
                    new AuditProcedureTabContent {ID = 2000, AuditProcedureTabId=100, Caption =  "Complience with Ethical Requirements including  Independence", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 3000, AuditProcedureTabId=100, Caption =  "Client Acceptance and Rentation", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                    new AuditProcedureTabContent {ID = 4000, AuditProcedureTabId=100, Caption =  "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=4,IsActive=true}
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();

            AuditProcedureIntoTAB_1_Item_1(context);
            AuditProcedureIntoTAB_1_Item_2(context);
            AuditProcedureIntoTAB_1_Item_3(context);
            AuditProcedureIntoTAB_1_Item_4(context);
            AuditProcedureIntoTAB_1_Item_2_Options(context);
            AuditProcedureIntoTAB_1_Item_3_Options(context);


        }
        private static void AuditProcedureIntoTAB_1_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 1000, AuditProcedureTabContentId =  1000, ParentId=null, Title="To ensure that in accepting or continuing the audit engagement the firm:", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 2000, AuditProcedureTabContentId =  1000, ParentId=1000, Title="Will comply with the relevant ethical requirements including independence;", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 3000, AuditProcedureTabContentId =  1000, ParentId=1000, Title="Is notified of breaches of independence requirements;", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 4000, AuditProcedureTabContentId =  1000, ParentId=1000, Title="Is competent to perform the engagement and has the capabilities, time and resources to do so;", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 5000, AuditProcedureTabContentId =  1000, ParentId=1000, Title="Has considered the integrity of the client, and any information that would lead it to conclude that the client lacks integrity is identified: and", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 6000, AuditProcedureTabContentId =  1000, ParentId=1000, Title="Has determined whether the pre-conditions for an audit have been met.", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false}
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_1_Item_2(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 7000, AuditProcedureTabContentId =  2000, ParentId=null, Title="Before accepting any new audit engagement (Newly Established Fund), consider whether the acceptance of such engagement would threaten compliance with the fundamental principles of APES 110: Code of Ethics for Professional Accountants.  List any threats below.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID = 8000, AuditProcedureTabContentId =  2000, ParentId=7000, Title="Self Review Threat to Independence (as determined by the following questions):", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 9000, AuditProcedureTabContentId =  2000, ParentId=8000, Title="Are the SMSF accounts prepared by our firm?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 10000, AuditProcedureTabContentId =  2000, ParentId=8000, Title="Is a different partner responsible for the conduct of the audit?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 11000, AuditProcedureTabContentId =  2000, ParentId=8000, Title="Is there pressure not to qualify the audit reports?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 12000, AuditProcedureTabContentId =  2000, ParentId=7000, Title="Self Interest Threat  (as determined by the following questions):", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 13000, AuditProcedureTabContentId =  2000, ParentId=12000, Title="Are the Trustees in a business or family relationship to the auditor?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 15000, AuditProcedureTabContentId =  2000, ParentId=12000, Title="Is the Trustee a significant client of the firm?  i.e. Does the sum of the client's work make up a significant source of income to the firm?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 16000, AuditProcedureTabContentId =  2000, ParentId=12000, Title="Does this impact on objectivity?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 17000, AuditProcedureTabContentId =  2000, ParentId=12000, Title="Does the firm provide investment advice to the Trustees?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 18000, AuditProcedureTabContentId =  2000, ParentId=12000, Title="Is the firm's income linked to investment performances?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 19000, AuditProcedureTabContentId =  2000, ParentId=12000, Title="Does the firm invest monies of clients in entities related to the firm?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 20000, AuditProcedureTabContentId =  2000, ParentId=7000, Title="Advocacy Threat  (as determined by the following questions):", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 21000, AuditProcedureTabContentId =  2000, ParentId=20000, Title="Have the Trustees adopted a strategy advocated by the firm?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 22000, AuditProcedureTabContentId =  2000, ParentId=20000, Title="Does this impact objectivity?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                        new AuditProcedure {ID = 23000, AuditProcedureTabContentId =  2000, ParentId=7000, Title="Intimidation Threat (as determined by the following question):", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 24000, AuditProcedureTabContentId =  2000, ParentId=23000, Title="Is there evidence of Trustees intimidating the auditor?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},    
                        new AuditProcedure {ID = 25000, AuditProcedureTabContentId =  2000, ParentId=7000, Title="Familiarity Threat  (as determined by the following question):", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 26000, AuditProcedureTabContentId =  2000, ParentId=25000, Title="Has the same partner been the audit partner for this SMSF for over 5 consecutive years? Where audits have been performed for more than five years, staff are rotated to minimise this threat.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 27000, AuditProcedureTabContentId =  2000, ParentId=null, Title="Before accepting any new audit engagement ,request the prospective client''s permission to communicate with the existing auditor.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType= Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                    new AuditProcedure {ID = 28000, AuditProcedureTabContentId =  2000, ParentId=null, Title="On receipt of the client''s permission, send ethical letter to incumbent auditor to assist in identifying any information to suggest that accepting the client engagement would create a threat to compliance with the fundamental principles of APES 110, including independence.  In assessing the existence of such threats, consider also any correspondence from the ATO or other regulatory body.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 29000, AuditProcedureTabContentId =  2000, ParentId=null, Title="For recurring engagements, review prior year''s work papers, along with any correspondence from the ATO, to suggest continuing with the engagement would threaten compliance with the fundamental principles of APES 110.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 30000, AuditProcedureTabContentId =  2000, ParentId=null, Title="Upon completion of the steps listed above and after evaluating any identified threat(s), determine whether appropriate safeguards are available and can be applied to eliminate the threat(s), or reduce them to an acceptable level.  List such safeguards in the comments column.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false}
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_1_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 31000, AuditProcedureTabContentId =  3000, ParentId=null, Title="Competence to perform the engagement.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID = 31010, AuditProcedureTabContentId =  3000, ParentId=31000, Title="Are we competent to perform the engagement?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 32000, AuditProcedureTabContentId =  3000, ParentId=31000, Title="On what basis was the above conclusion formed?", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID = 33000, AuditProcedureTabContentId =  3000, ParentId=32000, Title="We have audited similar SMSFs to this previously", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 34000, AuditProcedureTabContentId =  3000, ParentId=32000, Title="We have good understanding of the SIS requirements", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 35000, AuditProcedureTabContentId =  3000, ParentId=32000, Title="We have good understanding of the AASBs", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 36000, AuditProcedureTabContentId =  3000, ParentId=32000, Title="We have good understanding of ASAs and APES'", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID = 37000, AuditProcedureTabContentId =  3000, ParentId=32000, Title="We have good list of experts to consult if we experiences issues", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 38000, AuditProcedureTabContentId =  3000, ParentId=31000, Title="Will the use of any outside expert(s) be required?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 39000, AuditProcedureTabContentId =  3000, ParentId=null, Title="Time and Resources to perform the engagement", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID = 40000, AuditProcedureTabContentId =  3000, ParentId=39000, Title="Do we have sufficient time to allow us to complete the audit?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 41000, AuditProcedureTabContentId =  3000, ParentId=null, Title="Compliance with ethical requirements", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID = 42000, AuditProcedureTabContentId =  3000, ParentId=41000, Title="Based on the steps taken above, based on our conclusions formed, can we comply with the ethical requirements in undertaking this engagement?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 43000, AuditProcedureTabContentId =  3000, ParentId=null, Title="Considering the integrity of the client", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID = 44000, AuditProcedureTabContentId =  3000, ParentId=43000, Title="For new engagements, review the ethical clearance letter requested", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 45000, AuditProcedureTabContentId =  3000, ParentId=43000, Title="Review prior year's workpapers to identify any issues associated with the client to suggest the client may lack integrity.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 46000, AuditProcedureTabContentId =  3000, ParentId=43000, Title="Review any correspondence for the ATO or other regulatory body to indicate the client may lack integrity", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                     new AuditProcedure {ID = 47000, AuditProcedureTabContentId =  3000, ParentId=null, Title="Determining whether the pre-conditions for an audit have been met", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID = 48000, AuditProcedureTabContentId =  3000, ParentId=47000, Title="Are the financial reporting framework used to prepare the fund's financials appropriate (eg. Special Purposes Financial Report and accounting standards & policies)", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_1_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                new AuditProcedure {ID = 49000, AuditProcedureTabContentId =  4000, ParentId=null, Title="After completing the above procedures, it appears:", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 50000, AuditProcedureTabContentId =  4000, ParentId=49000, Title="We will be able to comply with the relevant ethical requirements including independence;", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 51000, AuditProcedureTabContentId =  4000, ParentId=49000, Title="Any breaches of independence requirements have been identified;", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 52000, AuditProcedureTabContentId =  4000, ParentId=49000, Title="The firm is competent to perform the engagement and has the capabilities, time and resources to do so;", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 53000, AuditProcedureTabContentId =  4000, ParentId=49000, Title="No information has been identified that would lead us to conclude that the client lacks integrity; and", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID = 54000, AuditProcedureTabContentId =  4000, ParentId=49000, Title="The pre-conditions for an audit have been met.", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_1_Item_2_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
            {
                new AuditProcedureOption {ID = 10000, AuditProcedureId =8000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10010, AuditProcedureId=8000, AuditProcedureOptionDetailId=1500 },
                new AuditProcedureOption {ID = 10020, AuditProcedureId=8000, AuditProcedureOptionDetailId=1600 },
                new AuditProcedureOption {ID = 10030, AuditProcedureId =8000, AuditProcedureOptionDetailId=1700 },
                new AuditProcedureOption {ID = 10040, AuditProcedureId=8000, AuditProcedureOptionDetailId=1800 },
                new AuditProcedureOption {ID = 10050, AuditProcedureId=8000, AuditProcedureOptionDetailId=1900 },


                new AuditProcedureOption {ID = 10060, AuditProcedureId =9000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10070, AuditProcedureId=9000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10080, AuditProcedureId=9000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10090, AuditProcedureId =10000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10100, AuditProcedureId=10000, AuditProcedureOptionDetailId=200 },
                new AuditProcedureOption {ID = 10110, AuditProcedureId=10000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10120, AuditProcedureId=10000, AuditProcedureOptionDetailId=400 },

                
                new AuditProcedureOption {ID = 10130, AuditProcedureId =11000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10140, AuditProcedureId=11000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10150, AuditProcedureId=11000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10160, AuditProcedureId =12000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10170, AuditProcedureId=12000, AuditProcedureOptionDetailId=1500 },
                new AuditProcedureOption {ID = 10180, AuditProcedureId=12000, AuditProcedureOptionDetailId=1600 },
                new AuditProcedureOption {ID = 10190, AuditProcedureId =12000, AuditProcedureOptionDetailId=1700 },
                new AuditProcedureOption {ID = 10200, AuditProcedureId=12000, AuditProcedureOptionDetailId=1800 },
                new AuditProcedureOption {ID = 10210, AuditProcedureId=12000, AuditProcedureOptionDetailId=1900 },


                new AuditProcedureOption {ID = 10220, AuditProcedureId =13000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10230, AuditProcedureId=13000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10240, AuditProcedureId=13000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10250, AuditProcedureId =15000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10260, AuditProcedureId=15000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10270, AuditProcedureId=15000, AuditProcedureOptionDetailId=400 },
                

                new AuditProcedureOption {ID = 10280, AuditProcedureId =16000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10290, AuditProcedureId=16000, AuditProcedureOptionDetailId=200 },
                new AuditProcedureOption {ID = 10300, AuditProcedureId=16000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10310, AuditProcedureId=16000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10320, AuditProcedureId =17000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10330, AuditProcedureId=17000, AuditProcedureOptionDetailId=200 },
                new AuditProcedureOption {ID = 10340, AuditProcedureId=17000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10350, AuditProcedureId=17000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10360, AuditProcedureId =18000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10370, AuditProcedureId=18000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10380, AuditProcedureId=18000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10400, AuditProcedureId =19000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10410, AuditProcedureId=19000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10420, AuditProcedureId=19000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10430, AuditProcedureId =20000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10440, AuditProcedureId=20000, AuditProcedureOptionDetailId=1500 },
                new AuditProcedureOption {ID = 10450, AuditProcedureId=20000, AuditProcedureOptionDetailId=1600 },
                new AuditProcedureOption {ID = 10460, AuditProcedureId =20000, AuditProcedureOptionDetailId=1700 },
                new AuditProcedureOption {ID = 10470, AuditProcedureId=20000, AuditProcedureOptionDetailId=1800 },
                new AuditProcedureOption {ID = 10480, AuditProcedureId=20000, AuditProcedureOptionDetailId=1900 },

                new AuditProcedureOption {ID = 10490, AuditProcedureId =21000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10500, AuditProcedureId=21000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10510, AuditProcedureId=21000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID = 10520, AuditProcedureId =22000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10530, AuditProcedureId=22000, AuditProcedureOptionDetailId=200 },
                new AuditProcedureOption {ID = 10540, AuditProcedureId=22000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10550, AuditProcedureId=22000, AuditProcedureOptionDetailId=400 },


                new AuditProcedureOption {ID = 10560, AuditProcedureId =23000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10570, AuditProcedureId=23000, AuditProcedureOptionDetailId=1500 },
                new AuditProcedureOption {ID = 10580, AuditProcedureId=23000, AuditProcedureOptionDetailId=1600 },
                new AuditProcedureOption {ID = 10590, AuditProcedureId =23000, AuditProcedureOptionDetailId=1700 },
                new AuditProcedureOption {ID = 10600, AuditProcedureId=23000, AuditProcedureOptionDetailId=1800 },
                new AuditProcedureOption {ID = 10610, AuditProcedureId=23000, AuditProcedureOptionDetailId=1900 },

                new AuditProcedureOption {ID = 10620, AuditProcedureId =24000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10630, AuditProcedureId =24000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID = 10640, AuditProcedureId=24000, AuditProcedureOptionDetailId=400 },


                new AuditProcedureOption {ID = 10650, AuditProcedureId =25000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10660, AuditProcedureId=25000, AuditProcedureOptionDetailId=1500 },
                new AuditProcedureOption {ID = 10670, AuditProcedureId=25000, AuditProcedureOptionDetailId=1600 },
                new AuditProcedureOption {ID = 10680, AuditProcedureId =25000, AuditProcedureOptionDetailId=1700 },
                new AuditProcedureOption {ID = 10690, AuditProcedureId=25000, AuditProcedureOptionDetailId=1800 },
                new AuditProcedureOption {ID = 10700, AuditProcedureId=25000, AuditProcedureOptionDetailId=1900 },



                new AuditProcedureOption {ID = 10710, AuditProcedureId =26000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10720, AuditProcedureId =26000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID = 10730, AuditProcedureId=26000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID = 10740, AuditProcedureId =27000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10750, AuditProcedureId =27000, AuditProcedureOptionDetailId=500 },
                new AuditProcedureOption {ID = 10760, AuditProcedureId=27000, AuditProcedureOptionDetailId=600 },

                new AuditProcedureOption {ID = 10770, AuditProcedureId =28000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10780, AuditProcedureId =28000, AuditProcedureOptionDetailId=500 },
                new AuditProcedureOption {ID = 10790, AuditProcedureId=28000, AuditProcedureOptionDetailId=600 },


                new AuditProcedureOption {ID = 10800, AuditProcedureId =29000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10810, AuditProcedureId =29000, AuditProcedureOptionDetailId=700 },
                new AuditProcedureOption {ID = 10820, AuditProcedureId=29000, AuditProcedureOptionDetailId=800 },

                new AuditProcedureOption {ID = 10830, AuditProcedureId =30000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID = 10840, AuditProcedureId =30000, AuditProcedureOptionDetailId=900 },
                new AuditProcedureOption {ID = 10850, AuditProcedureId=30000, AuditProcedureOptionDetailId=200 },

            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_1_Item_3_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {

                    new AuditProcedureOption {ID = 10860, AuditProcedureId =31010, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 10870, AuditProcedureId=31010, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 10880, AuditProcedureId=31010, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 10890, AuditProcedureId =33000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 10900, AuditProcedureId=33000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 10910, AuditProcedureId=33000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 10920, AuditProcedureId =34000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 10930, AuditProcedureId=34000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 10940, AuditProcedureId=34000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 10950, AuditProcedureId =35000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 10960, AuditProcedureId=35000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 10970, AuditProcedureId=35000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 10980, AuditProcedureId =36000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 10990, AuditProcedureId=36000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11000, AuditProcedureId=36000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11010, AuditProcedureId =37000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11020, AuditProcedureId=37000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11030, AuditProcedureId=37000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11040, AuditProcedureId =38000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11050, AuditProcedureId=38000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11060, AuditProcedureId=38000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11070, AuditProcedureId =40000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11080, AuditProcedureId=40000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11090, AuditProcedureId=40000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11100, AuditProcedureId =42000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11110, AuditProcedureId=42000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11120, AuditProcedureId=42000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11130, AuditProcedureId =44000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11140, AuditProcedureId=44000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID = 11150, AuditProcedureId=44000, AuditProcedureOptionDetailId=1400 },

                    new AuditProcedureOption {ID = 11160, AuditProcedureId =45000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11170, AuditProcedureId=45000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID = 11180, AuditProcedureId=45000, AuditProcedureOptionDetailId=1400 },

                    new AuditProcedureOption {ID = 11190, AuditProcedureId =46000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11200, AuditProcedureId=46000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID = 11210, AuditProcedureId=46000, AuditProcedureOptionDetailId=1400 },

                    new AuditProcedureOption {ID = 11220, AuditProcedureId =48000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11230, AuditProcedureId=48000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11240, AuditProcedureId=48000, AuditProcedureOptionDetailId=400 }

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        #endregion

        #region "Tab_2"
        private static void AuditProcedureIntoTAB_2(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 5000, AuditProcedureTabId=200, Caption =  "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=1, IsActive=true},
                    //new AuditProcedureTabContents {ID = 6000, AuditProcedureTabsID=200, Caption =  "Fund Details", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Form, Order=2,IsActive=false},
                    //new AuditProcedureTabContents {ID = 7000, AuditProcedureTabsID=200, Caption =  "Audit Tree", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=3,IsActive=false},
                    new AuditProcedureTabContent {ID = 8000, AuditProcedureTabId=200, Caption =  "Prior Year", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=4,IsActive=true},
                    new AuditProcedureTabContent {ID = 9000, AuditProcedureTabId=200, Caption =  "Client Correspondance", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=5,IsActive=true},
                    new AuditProcedureTabContent {ID = 10000, AuditProcedureTabId=200, Caption =  "Audit Plan", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 11000, AuditProcedureTabId=200, Caption =  "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=7,IsActive=true}
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
            AuditProcedureIntoTAB_2_Item_1(context);
            // AuditProcedureIntoTAB_2_Item_2(context);
            //AuditProcedureIntoTAB_2_Item_3(context);
            AuditProcedureIntoTAB_2_Item_4(context);
            AuditProcedureIntoTAB_2_Item_5(context);
            AuditProcedureIntoTAB_2_Item_6(context);
            AuditProcedureIntoTAB_2_Item_7(context);

            AuditProcedureIntoTAB_2_Item_4_Options(context);
            AuditProcedureIntoTAB_2_Item_6_Options(context);
        }
        private static void AuditProcedureIntoTAB_2_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 62000, AuditProcedureTabContentId =  5000, ParentId=null, Title="To plan the audit so that it will be performed in an effective manner.", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false}

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_2_Item_2(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 63000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Name", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 64000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Trustees", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType= Contracts.Common.ControlType.TextBox ,Checkbox=false},
                    new AuditProcedure {ID = 65000, AuditProcedureTabContentId =  6000, ParentId=null, Title="ABN", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 66000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Administrator / Accountant", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 67000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Address", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 68000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Year", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 69000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Date Audited Completed", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 70000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Auditing Entity", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 71000, AuditProcedureTabContentId =  6000, ParentId=null, Title="Type Of Fund", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false}
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_2_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 72000, AuditProcedureTabContentId =  7000, ParentId=null, Title="Prior Year", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                    new AuditProcedure {ID = 73005, AuditProcedureTabContentId =  7000, ParentId=null, Title="Current Year", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Form, AnswerControlType= Contracts.Common.ControlType.TextBox ,Checkbox=false}
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_2_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 74000, AuditProcedureTabContentId =  8000, ParentId=null, Title="If Evolv is auditing this fund for the first time, ensure that we have the following :", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 75000, AuditProcedureTabContentId =  8000, ParentId=74000, Title="Prior Year Audited Financial statements", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 76000, AuditProcedureTabContentId =  8000, ParentId=74000, Title="Income tax return", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 77000, AuditProcedureTabContentId =  8000, ParentId=74000, Title="Contravention report (if Any)", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 78000, AuditProcedureTabContentId =  8000, ParentId=74000, Title="Signed audit report,", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID = 79000, AuditProcedureTabContentId =  8000, ParentId=74000, Title="Correspondence.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 80000, AuditProcedureTabContentId =  8000, ParentId=null, Title="Review and Check prior year OML points have been satisfactorily resolved.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID = 80001, AuditProcedureTabContentId =  8000, ParentId=null, Title="Review and Check if prior contravention report items have been satisfactorily resolved (if applicable).", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_2_Item_5(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    //new AuditProcedures {ID = 81000, AuditProcedureTabContentsId =  9000, ParentID=null, Title="Audit Procedure", Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                     new AuditProcedure {ID = 81000, AuditProcedureTabContentId =  9000, ParentId=null, Title="To plan the audit so that it will be performed in an effective manner.", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false}
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_2_Item_6(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 83000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Audit Strategy", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =85000, AuditProcedureTabContentId =  10000, ParentId=83000, Title="SIS Legislation", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Label,Checkbox=false},
                        new AuditProcedure {ID =86000, AuditProcedureTabContentId =  10000, ParentId=83000, Title="SIS Regulations", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Label,Checkbox=false}, 
                        new AuditProcedure {ID =87000, AuditProcedureTabContentId =  10000, ParentId=83000, Title="Financial Reporting Framework", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Label,Checkbox=false},

                        new AuditProcedure {ID =88000, AuditProcedureTabContentId =  10000, ParentId=83000, Title="Applicable Accounting Standards", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =89000, AuditProcedureTabContentId =  10000, ParentId=88000, Title="AASB110 Events After Reporting Period", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =90000, AuditProcedureTabContentId =  10000, ParentId=88000, Title="AASB 112 Income Taxes", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =91000, AuditProcedureTabContentId =  10000, ParentId=88000, Title="AASB 1031 Materiality", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                        new AuditProcedure {ID =92000, AuditProcedureTabContentId =  10000, ParentId=83000, Title="Applicable Accounting Policies", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =93000, AuditProcedureTabContentId =  10000, ParentId=92000, Title="Accrual accounting", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =94000, AuditProcedureTabContentId =  10000, ParentId=92000, Title="Assets valued at Net Market Values", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =95000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Timing of Audit", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =96000, AuditProcedureTabContentId =  10000, ParentId=95000, Title="The timing of the audit is dependent upon when all information is sent to us.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                    new AuditProcedure {ID =97000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Materiality", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =98000, AuditProcedureTabContentId =  10000, ParentId=97000, Title="Financial", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =99000, AuditProcedureTabContentId =  10000, ParentId=98000, Title="After assessing the fraud and risk procedures above ,  a quantatitive materiality level of 5% of the funds total nett assets (as listed in the materiality calculator below) will be applied together with the auditors professional judgement  for further audit procedures and selection strategies.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =100000, AuditProcedureTabContentId =  10000, ParentId=98000, Title="Materiality CALCULATOR", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =101000, AuditProcedureTabContentId =  10000, ParentId=98000, Title="All material errors will be communicated to the trustees.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                        new AuditProcedure {ID =102000, AuditProcedureTabContentId =  10000, ParentId=97000, Title="Compliance", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =103000, AuditProcedureTabContentId =  10000, ParentId=102000, Title="The trustees will be advised of all contraventions, the ATO will be advised all reportable contraventions in accordance with their reporting cr", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                        new AuditProcedure {ID =104000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Electronic Information / Data Controls", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =105000, AuditProcedureTabContentId =  10000, ParentId=104000, Title="A mainstream superfund accounting package is used with no modification being made to the core functionalities.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =106000, AuditProcedureTabContentId =  10000, ParentId=104000, Title="All internet banking access, computers, servers and software is logged in using a username and password, therefore, sufficient security controls are in place to ensure authorisation, retention and protection of information.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =107000, AuditProcedureTabContentId =  10000, ParentId=104000, Title="Full work papers are prepared, and the file reviewed by manager and partner, therefore sufficient data processing controls are in place over data input ensuring completeness, accuracy and validity.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =108000, AuditProcedureTabContentId =  10000, ParentId=104000, Title="Policies and procedures are in place to manage daily IT operations and implementation of computer software / hardware including controls over staff who prepares the scanned audit file for Evolv to audit.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =109000, AuditProcedureTabContentId =  10000, ParentId=104000, Title="Policies and procedures are in place to manage daily IT operations and implementation of computer software / hardware.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =110000, AuditProcedureTabContentId =  10000, ParentId=104000, Title="Conclusion: Sufficient electronic information and data controls are in place and the risk is low.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                        new AuditProcedure {ID =111000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Internal Controls", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =112000, AuditProcedureTabContentId =  10000, ParentId=111000, Title="We have assessed the fund's control environment and compliance framework as ineffective as there is no proper segregation of duties.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =113000, AuditProcedureTabContentId =  10000, ParentId=111000, Title="As the administration of the fund is outsourced to an accountant / administrator, we have also  enquired of their information system controls and related business processes relevant to the production of the financial reports. We have assessed this control environment as not being of a high risk.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =114000, AuditProcedureTabContentId =  10000, ParentId=111000, Title="Conclusion: We are unable to rely on the effectiveness of the fund's internal controls and adopt a substantive testing approach.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                        new AuditProcedure {ID =115000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Risk Assessment Procedures", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =116000, AuditProcedureTabContentId =  10000, ParentId=115000, Title="We have based our risk assessment for the financial audit on identifying and assessing risks at the financial report level and at the assertion level for classes of transactions, account balances and disclosures.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =117000, AuditProcedureTabContentId =  10000, ParentId=115000, Title="The following are the main risk areas and audit assertions which will be considered:", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =118000, AuditProcedureTabContentId =  10000, ParentId=117000, Title="Investments and other assets", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =119000, AuditProcedureTabContentId =  10000, ParentId=118000, Title="The stated assets may not exist (existence risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =120000, AuditProcedureTabContentId =  10000, ParentId=118000, Title="The stated assets may not relate to the fund (rights & obligations risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =121000, AuditProcedureTabContentId =  10000, ParentId=118000, Title="The assets are materially over or understated (valuation risk).", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =122000, AuditProcedureTabContentId =  10000, ParentId=118000, Title="The assets are not properly disclosed (disclosure risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                            new AuditProcedure {ID =123000, AuditProcedureTabContentId =  10000, ParentId=117000, Title="Liabilities and member benefits", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =124000, AuditProcedureTabContentId =  10000, ParentId=123000, Title="Liabilities may be materially understated (completeness risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =125000, AuditProcedureTabContentId =  10000, ParentId=123000, Title="There may exist unrecorded liabilities (completeness risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =126000, AuditProcedureTabContentId =  10000, ParentId=123000, Title="Member benefits are materially over or understated (measurem", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =127000, AuditProcedureTabContentId =  10000, ParentId=123000, Title="Member benefits may not be appropriately classified (rights & obligations risk, disclosure risk)", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                            new AuditProcedure {ID =128000, AuditProcedureTabContentId =  10000, ParentId=117000, Title="Contributions and other income", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =129000, AuditProcedureTabContentId =  10000, ParentId=128000, Title="All contributions and other income may not be received by the SMSF (completeness risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =130000, AuditProcedureTabContentId =  10000, ParentId=128000, Title="Contributions and other income may be materially misstated (measurement risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =131000, AuditProcedureTabContentId =  10000, ParentId=128000, Title="Contributions received may not be allocated to appropriate member &/or benefit category (disclosure risk)", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                            new AuditProcedure {ID =132000, AuditProcedureTabContentId =  10000, ParentId=117000, Title="Benefit payments and expenses", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =133000, AuditProcedureTabContentId =  10000, ParentId=132000, Title="Stated expenses do not relate to the SMSF (occurrence risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =134000, AuditProcedureTabContentId =  10000, ParentId=132000, Title="Expenses may be materially misstated (measurement risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =135000, AuditProcedureTabContentId =  10000, ParentId=132000, Title="Benefits paid may be materially miscalculated (measurement risk).", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                            new AuditProcedure {ID =136000, AuditProcedureTabContentId =  10000, ParentId=117000, Title="Compliance with SIS Act", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =137000, AuditProcedureTabContentId =  10000, ParentId=136000, Title="The SMSF’s operations may have contravened the requirements of the SIS Act (compliance risk).", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                            new AuditProcedure {ID =138000, AuditProcedureTabContentId =  10000, ParentId=115000, Title="The following procedures have also been undertaken in assessing risk:", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =139000, AuditProcedureTabContentId =  10000, ParentId=138000, Title="Make enquiries of trustees as to the following:", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =140000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="1. Their assessment of the risk that the financial report may be materially misstated due to fraud, including the nature, extent and frequency of such assessments (ASA 240.17(a));", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =141000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="2. Their process for identifying and responding to the risks of fraud, including any specific risks of fraud that trustee(s) has identified or that have been brought to their attention, or classes of transactions, account balances, or disclosures for which a risk of fraud is likely to exist (ASA 240.17(b));", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =142000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="3. Whether they have knowledge of any actual, suspected or alleged fraud affecting the SMSF (ASA 240.18);", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =143000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="4. Whether the entity is in compliance with relevant laws and regulations (ASA 250.14(a));", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =144000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="5. The identity of the entity’s related parties, including changes from the prior period, the nature of the relationships between the entity and these related parties, and Whether the entity entered into any transactions with these related parties during the period and, if so, the type and purpose of the transactions (ASA 550.13);", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =145000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="6. Obtain an understanding of the controls, if any, established Identify, account for, and disclose related party relationships and transactions, authorise and approve significant transactions and arrangements with related parties, and Authorise and approve significant transactions and arrangements outside the normal course of business (ASA 550.14);", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =146000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="7. Whether any subsequent events have occurred which might affect the financial report (ASA 560.7(b));", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =147000, AuditProcedureTabContentId =  10000, ParentId=139000, Title="8.  Their knowledge of events or conditions beyond the period of trustee(s) assessment that may cast significant doubt on the SMSF’s ability to continue as a going concern (ASA 570.15);", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                            new AuditProcedure {ID =148000, AuditProcedureTabContentId =  10000, ParentId=115000, Title="Conclusion: The above items have been considered partly in the trustees signed representation letter and no further work is deemed necessary.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =149000, AuditProcedureTabContentId =  10000, ParentId=115000, Title="Analytical Procedures", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =150000, AuditProcedureTabContentId =  10000, ParentId=149000, Title="Analytical procedure reviews are performed throughout the audit program. These procedures together with our risk assessment above provide corroborative evidence to justify a sample selection of approximately 30%.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =151000, AuditProcedureTabContentId =  10000, ParentId=149000, Title="In addition, the following specific items, represent areas of  risk. Determine if any of the following items are applicable:", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                        new AuditProcedure {ID =152000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="In-house assets", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =153000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Pre-99 and/or Post 99 Related Units Trust", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =154000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Related Party transactions", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =155000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="First year funds", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =156000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Breach in previous year", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =157000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Unusual investments (eg. overseas assets)", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =158000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Investment in related and closely held entities", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =159000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Borrowings / Instalment Warrants", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =160000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="members non resident", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =161000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Carry on business of selling goods or services", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                        new AuditProcedure {ID =162000, AuditProcedureTabContentId =  10000, ParentId=151000, Title="Segregated assets", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown ,Checkbox=false},
                                    new AuditProcedure {ID =163000, AuditProcedureTabContentId =  10000, ParentId=149000, Title="If you answered yes to any of the above,  perform additional testing on each item.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =164000, AuditProcedureTabContentId =  10000, ParentId=115000, Title="Observation & Inspection", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =165000, AuditProcedureTabContentId =  10000, ParentId=164000, Title="The auditor is to makes observations and inspect all relevant documentation necessary to assess risk.", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                        new AuditProcedure {ID =166000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Staff Allocation", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =167000, AuditProcedureTabContentId =  10000, ParentId=166000, Title="We have determined that for any high risk areas that we are competent to perform the audit. Appropriately qualified staff will be assigned to a fund which is identified as having a high risk and an audit manager will be responsible for the supervision of these high risk areas.", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =168000, AuditProcedureTabContentId =  10000, ParentId=166000, Title="Any fund falling outside the following parameters or that is classified a risking fund as per the risk calculator will automatically be delegated to a senior auditor over a junior auditor:", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =169000, AuditProcedureTabContentId =  10000, ParentId=168000, Title="'Value of fund under $2M", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =170000, AuditProcedureTabContentId =  10000, ParentId=168000, Title="Type of investments: Cash, Shares in listed companies, managed funds, wrap accounts, real-estate property.", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =171000, AuditProcedureTabContentId =  10000, ParentId=168000, Title="Accumulation or pension (2nd year of operations)", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =172000, AuditProcedureTabContentId =  10000, ParentId=168000, Title="Not the first year of audit by Evolv", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                        new AuditProcedure {ID =173000, AuditProcedureTabContentId =  10000, ParentId=null, Title="Fraud", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =173010, AuditProcedureTabContentId =  10000, ParentId=173000, Title="Audit Objective:", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =174000, AuditProcedureTabContentId =  10000, ParentId=173010, Title="To identify and assess the risks of material misstatement of the financial report due to fraud;" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =175000, AuditProcedureTabContentId =  10000, ParentId=173010, Title="To obtain sufficient appropriate audit evidence regarding the assessed risks of material misstatement due to fraud, through designing and implementing appropriate responses; and" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =176000, AuditProcedureTabContentId =  10000, ParentId=173010, Title="To respond appropriately to fraud or suspected fraud identified during the audit." , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                
                            new AuditProcedure {ID =177000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="When planning for the audit of the SMSF, if there is more than 1 staff member involved with the audit, discuss amongst each other how and where the SMSFs financial report, and compliance with the SIS requirements, may be susceptible to material misstatement/non-compliant due to fraud." , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =178000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="Make enquiries of the SMSF trustees regarding:" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =179000, AuditProcedureTabContentId =  10000, ParentId=178000, Title="The trustee(s) assessment of the risk that the financial report may be materially misstated due to fraud, and in contravention of the SIS requirements, including the nature, extent and frequency of such assessments;" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =180000, AuditProcedureTabContentId =  10000, ParentId=178000, Title="Trustee(s) process for identifying and responding to the risks of fraud, including any specific risks of fraud that trustee(s) have identified or that have been brought to their attention, or classes of transactions, account balances, or disclosures for which a risk of fraud is likely to exist;" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =181000, AuditProcedureTabContentId =  10000, ParentId=178000, Title="Their knowledge of any actual, suspected or alleged fraud affecting the entity." , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =182000, AuditProcedureTabContentId =  10000, ParentId=178000, Title="If considered appropriate, send the trustees a fraud questionnaire to complete." , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                            new AuditProcedure {ID =183000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="Note: Where responses to enquiries of trustees are inconsistent, investigate all inconsistencies." , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =184000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="When performing analytical procedures, assess both risk assessment procedures and  evaluate whether unusual or unexpected relationships that have been identified, including those related to revenue accounts, may indicate risks of material misstatement due to fraud." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =185000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="Refer to risk analysis and consider whether other information obtained by the auditor indicates risks of material misstatement due to fraud." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =186000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="Upon completing the above fraud risk procedures, should any fraud risk factors be identified, assess the risks of material misstatement/non-compliance with SIS requirements, due to fraud at the financial report level, and at the assertion level for classes of transactions, account balances and disclosures.  (Not applicable if no fraud risk factors identified above)." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =187000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="Should any risks of fraud be identified, ensure the corresponding audit procedures, including their nature, timing and extent are responsive to the assessed risks of material misstatement due to fraud at the assertion and/or SIS level." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =188000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="When performing analytical procedures near the end of the audit, when forming an overall conclusion as to whether the financial report is consistent with the our understanding of the fund, evaluate whether the procedures indicate a previously unrecognised risk of material misstatement due to fraud." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =189000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="For any identified misstatements, or SIS contraventions, evaluate whether they are indicative of fraud. If there is such an indication, evaluate the implications in relation to other aspects of the audit, particularly the reliability of trustee representations, recognising that an instance of fraud is unlikely to be an isolated occurrence." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =190000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="If, as a result of a misstatement resulting from fraud or suspected fraud, exceptional circumstances arise that bring into question our ability to continue performing the audit:" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =191000, AuditProcedureTabContentId =  10000, ParentId=190000, Title="Determine the professional and legal responsibilities applicable in the circumstances, including whether there is a requirement to report to the trustee appointment or to the ATO;" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =192000, AuditProcedureTabContentId =  10000, ParentId=190000, Title="Consider whether it is appropriate to withdraw from the engagement, where withdrawal is possible under applicable law or regulation; and if we withdraw from the engagement:" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =193000, AuditProcedureTabContentId =  10000, ParentId=190000, Title="Discuss with the trustee(s) our withdrawal from the engagement and the reasons for the withdrawal; and" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =194000, AuditProcedureTabContentId =  10000, ParentId=190000, Title="Determine whether there is a professional or legal requirement to report to the trustee(s) or the ATO, our withdrawal from the engagement and the reasons for the withdrawal." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =195000, AuditProcedureTabContentId =  10000, ParentId=173000, Title="Conclusion" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =196000, AuditProcedureTabContentId =  10000, ParentId=195000, Title="Where applicable, risks of material misstatement of the financial report, and SIS level, due to fraud have been identified, assessed and evaluated along with the appropriate further audit procedures in response to such risks;" , Attachments=false,Oml=true,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =197000, AuditProcedureTabContentId =  10000, ParentId=195000, Title="Sufficient appropriate audit evidence regarding the assessed risks of material misstatement due to fraud has been obtained; and" , Attachments=false,Oml=true,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                                new AuditProcedure {ID =198000, AuditProcedureTabContentId =  10000, ParentId=195000, Title="We have responded appropriately to any identified fraud or suspected fraud identified during the audit." , Attachments=false,Oml=true,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false}
                };

            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_2_Item_7(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =200000, AuditProcedureTabContentId =  11000, ParentId=null, Title="After completing the above procedures, we believe the audit has been appropriately planned so that it will be performed in an effective manner", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_2_Item_4_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID = 11250, AuditProcedureId =74000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11260, AuditProcedureId=74000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11270, AuditProcedureId=74000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11280, AuditProcedureId =75000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11290, AuditProcedureId=75000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11300, AuditProcedureId=75000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11310, AuditProcedureId =76000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11320, AuditProcedureId=76000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11330, AuditProcedureId=76000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11340, AuditProcedureId =77000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11350, AuditProcedureId=77000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11360, AuditProcedureId=77000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11370, AuditProcedureId =78000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11380, AuditProcedureId=78000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11390, AuditProcedureId=78000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11400, AuditProcedureId =79000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11410, AuditProcedureId=79000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11420, AuditProcedureId=79000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID = 11430, AuditProcedureId =80000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID = 11440, AuditProcedureId=80000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID = 11450, AuditProcedureId=80000, AuditProcedureOptionDetailId=400 },

                    //new AuditProcedureOption {ID = 11460, AuditProcedureId =46000, AuditProcedureOptionDetailId=100 },
                    //new AuditProcedureOption {ID = 11470, AuditProcedureId=46000, AuditProcedureOptionDetailId=300 },
                    //new AuditProcedureOption {ID = 11480, AuditProcedureId=46000, AuditProcedureOptionDetailId=400 }

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_2_Item_6_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =11490, AuditProcedureId =85000, AuditProcedureOptionDetailId=2700 },
                    new AuditProcedureOption {ID =11500, AuditProcedureId =86000, AuditProcedureOptionDetailId=2800 },
                    new AuditProcedureOption {ID =11510, AuditProcedureId =87000, AuditProcedureOptionDetailId=2900 },

                    new AuditProcedureOption {ID =11520, AuditProcedureId =89000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11530, AuditProcedureId=89000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11540, AuditProcedureId=89000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =11550, AuditProcedureId =90000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11560, AuditProcedureId=90000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11570, AuditProcedureId=90000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =11580, AuditProcedureId =91000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11590, AuditProcedureId=91000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11600, AuditProcedureId=91000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11610, AuditProcedureId =93000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11620, AuditProcedureId=93000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11630, AuditProcedureId=93000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =11640, AuditProcedureId =94000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11650, AuditProcedureId=94000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11660, AuditProcedureId=94000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =11670, AuditProcedureId =152000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11680, AuditProcedureId=152000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11690, AuditProcedureId=152000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =11700, AuditProcedureId =153000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11710, AuditProcedureId=153000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11720, AuditProcedureId=153000, AuditProcedureOptionDetailId=400 },

                    
                    new AuditProcedureOption {ID =11730, AuditProcedureId =154000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11740, AuditProcedureId=154000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11750, AuditProcedureId=154000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11760, AuditProcedureId =155000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11770, AuditProcedureId=155000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11780, AuditProcedureId=155000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11790, AuditProcedureId =156000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11800, AuditProcedureId=156000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11810, AuditProcedureId=156000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11820, AuditProcedureId =157000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11830, AuditProcedureId=157000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11840, AuditProcedureId=157000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11850, AuditProcedureId =158000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11860, AuditProcedureId=158000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11870, AuditProcedureId=158000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11880, AuditProcedureId =159000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11890, AuditProcedureId=159000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11900, AuditProcedureId=159000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11910, AuditProcedureId =160000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11920, AuditProcedureId=160000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11930, AuditProcedureId=160000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11940, AuditProcedureId =161000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11950, AuditProcedureId=161000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11960, AuditProcedureId=161000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =11970, AuditProcedureId =163000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =11980, AuditProcedureId=163000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =11990, AuditProcedureId=163000, AuditProcedureOptionDetailId=400 },
                    
                    
                    new AuditProcedureOption {ID =12000, AuditProcedureId =177000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12010, AuditProcedureId=177000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12020, AuditProcedureId=177000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12030, AuditProcedureId =179000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12040, AuditProcedureId=179000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12050, AuditProcedureId=179000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12060, AuditProcedureId =180000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12070, AuditProcedureId=180000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12080, AuditProcedureId=180000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12090, AuditProcedureId =181000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12100, AuditProcedureId=181000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12110, AuditProcedureId=181000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12120, AuditProcedureId =182000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12130, AuditProcedureId=182000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12140, AuditProcedureId=182000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12150, AuditProcedureId =184000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12160, AuditProcedureId=184000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12170, AuditProcedureId=184000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12180, AuditProcedureId =185000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12190, AuditProcedureId=185000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12200, AuditProcedureId=185000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12210, AuditProcedureId =186000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12220, AuditProcedureId=186000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12230, AuditProcedureId=186000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12240, AuditProcedureId =187000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12250, AuditProcedureId=187000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12260, AuditProcedureId=187000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12270, AuditProcedureId =188000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12280, AuditProcedureId=188000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12290, AuditProcedureId=188000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12300, AuditProcedureId =189000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12310, AuditProcedureId=189000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12320, AuditProcedureId=189000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12330, AuditProcedureId =190000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12340, AuditProcedureId =190000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12350, AuditProcedureId=190000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12360, AuditProcedureId=190000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12370, AuditProcedureId =191000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12380, AuditProcedureId =191000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12390, AuditProcedureId=191000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12400, AuditProcedureId=191000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12410, AuditProcedureId =192000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12420, AuditProcedureId =192000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12430, AuditProcedureId=192000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12440, AuditProcedureId=192000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12460, AuditProcedureId =193000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12470, AuditProcedureId =193000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12480, AuditProcedureId=193000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12490, AuditProcedureId=193000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12500, AuditProcedureId =194000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12510, AuditProcedureId =194000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12520, AuditProcedureId=194000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12530, AuditProcedureId=194000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12540, AuditProcedureId =195000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12550, AuditProcedureId =195000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12560, AuditProcedureId=195000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12570, AuditProcedureId=195000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12580, AuditProcedureId =196000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12590, AuditProcedureId =196000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12600, AuditProcedureId=196000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12610, AuditProcedureId=196000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12620, AuditProcedureId =197000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12630, AuditProcedureId =197000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12640, AuditProcedureId=197000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12650, AuditProcedureId=197000, AuditProcedureOptionDetailId=2100 },
                    
                    new AuditProcedureOption {ID =12660, AuditProcedureId =198000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12670, AuditProcedureId =198000, AuditProcedureOptionDetailId=200 },
                    new AuditProcedureOption {ID =12680, AuditProcedureId=198000, AuditProcedureOptionDetailId=2000 },
                    new AuditProcedureOption {ID =12690, AuditProcedureId=198000, AuditProcedureOptionDetailId=2100 }
                
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        #endregion

        #region "Tab_3"
        private static void AuditProcedureIntoTAB_3(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 12000, AuditProcedureTabId=300, Caption =  "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=1,IsActive=true},
                    new AuditProcedureTabContent {ID = 13000, AuditProcedureTabId=300, Caption =  "Regulatory Review", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 14000, AuditProcedureTabId=300, Caption =  "Trust Deed Review", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 15000, AuditProcedureTabId=300, Caption =  "Commercial Viability", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                    new AuditProcedureTabContent {ID = 16000, AuditProcedureTabId=300, Caption =  "Trustee''s Miniutes and Declaration", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=4,IsActive=true},
                    new AuditProcedureTabContent {ID = 17000, AuditProcedureTabId=300, Caption =  "Investment Strategy", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=5,IsActive=true},
                    new AuditProcedureTabContent {ID = 18000, AuditProcedureTabId=300, Caption =  "Compliance Summary", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.GridMultiCol, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 19000, AuditProcedureTabId=300, Caption =  "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=7,IsActive=true}
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
            AuditProcedureIntoTAB_3_Item_1(context);
            AuditProcedureIntoTAB_3_Item_2(context);
            AuditProcedureIntoTAB_3_Item_3(context);
            AuditProcedureIntoTAB_3_Item_4(context);
            AuditProcedureIntoTAB_3_Item_5(context);
            AuditProcedureIntoTAB_3_Item_6(context);
            AuditProcedureIntoTAB_3_Item_7(context);
            AuditProcedureIntoTAB_3_Item_8(context);

            AuditProcedureIntoTAB_3_Item_2_Options(context);
            AuditProcedureIntoTAB_3_Item_3_Options(context);
            AuditProcedureIntoTAB_3_Item_4_Options(context);
            AuditProcedureIntoTAB_3_Item_5_Options(context);
            AuditProcedureIntoTAB_3_Item_6_Options(context);
            AuditProcedureIntoTAB_3_Item_7_Columns(context);
        }
        private static void AuditProcedureIntoTAB_3_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 208100, AuditProcedureTabContentId =  12000, ParentId=null, Title="To ensure the fund complies with:", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =208200, AuditProcedureTabContentId =  12000, ParentId=208100, Title="all relevant sections of SISA & SISR", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =208300, AuditProcedureTabContentId =  12000, ParentId=208100, Title="the trust deed", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =208400, AuditProcedureTabContentId =  12000, ParentId=208100, Title="the investment strategy", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_3_Item_2(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 209000, AuditProcedureTabContentId =  13000, ParentId=null, Title="Do we have a signed Engagement letter", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =210000, AuditProcedureTabContentId =  13000, ParentId=null, Title="Do we have a signed Representation Letter?", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_3_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =214000, AuditProcedureTabContentId =  14000, ParentId=null, Title="Trustees, Members and Fund Compliance" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =215000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Name of Trustee:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                        new AuditProcedure {ID =216000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Record date deed of establishment or last amendment" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                        new AuditProcedure {ID =217000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Trust deed provider" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                        new AuditProcedure {ID =218000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Name of Members 1:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                        new AuditProcedure {ID =219000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Name of Members 2:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                        new AuditProcedure {ID =220000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Name of Members 3:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                        new AuditProcedure {ID =221000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Name of Members 4:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.TextBox,Checkbox=false},
                        new AuditProcedure {ID =222000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="If corporate trustee, review ASIC company statement & confirm all directors are members of the fund. Annotate any discrepancies. Hyper Link:https://connectonline.asic.gov.au/  Hyper Link:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =223000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="If individual trustees, confirm all individuals are members of the fund. Annotate any discrepancies." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =224000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Check  the trustees / Directors are not disqualified persons.Hyper Link: http://www.search.asic.gov.au/cgi-bin/grb040c?FORMID=a  Hyper Link:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =225000, AuditProcedureTabContentId =  14000, ParentId=214000, Title="Check that the fund is complying on the SuperFundLook website Hyper Link: http://superfundlookup.gov.au/  Hyper Link:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                    new AuditProcedure {ID =226000, AuditProcedureTabContentId =  14000, ParentId=null, Title="Ensure that we have the following documents for setting up a SMSF including ensuring we have copies for new members added to the fund during the year:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =227000, AuditProcedureTabContentId =  14000, ParentId=226000, Title="Copy of the consent to act as trustee of the fund and declaration that the trustee is not a disqualified person (Form no. NAT 71089)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =228000, AuditProcedureTabContentId =  14000, ParentId=226000, Title="Copy of the application for membership of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =229000, AuditProcedureTabContentId =  14000, ParentId=226000, Title="Copy of the binding death nominations if any" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =230000, AuditProcedureTabContentId =  14000, ParentId=226000, Title="Documents relating to appointment of trustees and members." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},

                    new AuditProcedure {ID =231000, AuditProcedureTabContentId =  14000, ParentId=null, Title="If any members under a legal disability ie under the age of 18 yrs of review parent / guardian consent" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =232000, AuditProcedureTabContentId =  14000, ParentId=231000, Title="Establishment and amendment" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =233000, AuditProcedureTabContentId =  14000, ParentId=231000, Title="Ensure that it has been properly executed, signed by all trustees & witnessed" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =234000, AuditProcedureTabContentId =  14000, ParentId=231000, Title="Ensure the rules incorporate SISA, SISR and applicable taxation rules" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =235000, AuditProcedureTabContentId =  14000, ParentId=231000, Title="Ensure it notes the core and ancillary purposes of the fund" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =236000, AuditProcedureTabContentId =  14000, ParentId=231000, Title="Ensure it notes an irrevocable election to become a regulated fund or subject to SISA or SISR" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =237000, AuditProcedureTabContentId =  14000, ParentId=231000, Title="Ensure it contains a clause which deems the appropriate legislation into or out of the deed to allow the fund to remain complying" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                    new AuditProcedure {ID =238000, AuditProcedureTabContentId =  14000, ParentId=null, Title="Contributions" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =239000, AuditProcedureTabContentId =  14000, ParentId=238000, Title="Ensure it allows for all types of contributions received by the fund" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =240000, AuditProcedureTabContentId =  14000, ParentId=238000, Title="Are co contributions or contribution splitting allowed?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =241000, AuditProcedureTabContentId =  14000, ParentId=238000, Title="Are in specie contributions of assets made during the year allowed" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =242000, AuditProcedureTabContentId =  14000, ParentId=238000, Title="Can excess contributions be rejected?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =243000, AuditProcedureTabContentId =  14000, ParentId=238000, Title="Can excess contributions tax levied on a member be by paid by the fund?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                    new AuditProcedure {ID =244000, AuditProcedureTabContentId =  14000, ParentId=null, Title="Benefit payments" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =245000, AuditProcedureTabContentId =  14000, ParentId=244000, Title="Is compulsory cashing of members balances at a specific age required?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =246000, AuditProcedureTabContentId =  14000, ParentId=244000, Title="Is a lump sum benefit required to be paid in lieu of a pension?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =247000, AuditProcedureTabContentId =  14000, ParentId=244000, Title="Ensure it allows for all types of pensions paid by the fund, or segregation of assets" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =248000, AuditProcedureTabContentId =  14000, ParentId=244000, Title="Ensure it allows for commutation of pensions" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                    new AuditProcedure {ID =249000, AuditProcedureTabContentId =  14000, ParentId=null, Title="Borrowings" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =250000, AuditProcedureTabContentId =  14000, ParentId=249000, Title="Does it permit temporary borrowings in specific circumstances and instalment warrant arrangements?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                    new AuditProcedure {ID =251000, AuditProcedureTabContentId =  14000, ParentId=null, Title="Investments" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =252000, AuditProcedureTabContentId =  14000, ParentId=251000, Title="Does it specify what assets the fund may or may not invest in?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =253000, AuditProcedureTabContentId =  14000, ParentId=251000, Title="Ensure it requires an investment strategy to be formulated and given effect to?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                    new AuditProcedure {ID =254000, AuditProcedureTabContentId =  14000, ParentId=null, Title="Financial Records" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =255000, AuditProcedureTabContentId =  14000, ParentId=254000, Title="Does it require the preparation of an annual financial report and audit?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =256000, AuditProcedureTabContentId =  14000, ParentId=254000, Title="Ensure minutes and records are required to be kept for at least 10 years, and financial reports and tax returns for at least 5 years?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_3_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =257000, AuditProcedureTabContentId =  15000, ParentId=null, Title="Are the value of the assets of the fund under $140,000, if yes, insert OML point?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_5(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =258000, AuditProcedureTabContentId =  16000, ParentId=null, Title="Review the minutes of meetings held during the year and ensure that they are signed by all Directors / Trustees or solely by the chairperson. (SIS Reg. 4.03(3))." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =259000, AuditProcedureTabContentId =  16000, ParentId=null, Title="Sight signed copy of the trustees declaration. Ensure that all individual trustees or directors of trustee company have signed accordingly." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_6(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =261000, AuditProcedureTabContentId =  17000, ParentId=null, Title="Ensure that the investments align with the Investment Strategy by checking the analytical review in holdings section" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =262000, AuditProcedureTabContentId =  17000, ParentId=null, Title="Have the Trustees documented and given effect to an investment strategy for the Fund that has regard to the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =263000, AuditProcedureTabContentId =  17000, ParentId=262000, Title="The risk in making, holding and realising assets;" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =264000, AuditProcedureTabContentId =  17000, ParentId=262000, Title="Likely return from investments;" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =265000, AuditProcedureTabContentId =  17000, ParentId=262000, Title="Liquidity of investments in comparison to expected Cash flow requirements of the fund; and" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =266000, AuditProcedureTabContentId =  17000, ParentId=262000, Title="Investment composition as a whole including diversification or risks of inadequate diversification" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =267000, AuditProcedureTabContentId =  17000, ParentId=262000, Title="Insurance requirements" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_7(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =268000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Establishment and operation of the SMSF" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =269000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Meets the definition of a SMSF." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =270000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Trustees are not disqualified persons." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =271000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Maintains minutes and records for specified time periods." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =272000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Maintains trustees ' declarations regarding duties for those who become trustees for the first time after 30 June 2007." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =273000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Proper accounting records kept and retained for 5 years." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =274000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Annual financial report prepared, signed and retained for 5 years." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =275000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Trustee provides auditor documents within 14 days of request (14 day letter)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =276000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Prohibition on entering contracts restricting trustees ' functions and powers." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =277000, AuditProcedureTabContentId =  18000, ParentId=268000, Title="Trustees formulate and give effect to an investment strategy." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =278000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Sole purpose" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =279000, AuditProcedureTabContentId =  18000, ParentId=278000, Title="Established for the sole purpose of funding a member 's benefits for retirement, attainment of a certain age, death, ill-health or termination." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =280000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Investment restrictions" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =281000, AuditProcedureTabContentId =  18000, ParentId=280000, Title="Restrictions on acquiring or holding 'in-house' assets." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =282000, AuditProcedureTabContentId =  18000, ParentId=280000, Title="Restrictions on acquisitions of assets from related parties." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =283000, AuditProcedureTabContentId =  18000, ParentId=280000, Title="Maintains arm 's length investments." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =284000, AuditProcedureTabContentId =  18000, ParentId=280000, Title="Maintains SMSF money and other assets separate from those of the trustees, employer-sponsors and other related parties." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =285000, AuditProcedureTabContentId =  18000, ParentId=280000, Title="Prohibition on lending or providing financial assistance to member or relative." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =286000, AuditProcedureTabContentId =  18000, ParentId=280000, Title="Restrictions on borrowings." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =287000, AuditProcedureTabContentId =  18000, ParentId=280000, Title="Prohibition on charges over SMSF assets." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =288000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Benefits Trustees" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =289000, AuditProcedureTabContentId =  18000, ParentId=288000, Title="Trustees maintain members ' minimum benefits." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =290000, AuditProcedureTabContentId =  18000, ParentId=288000, Title="Minimum pension amount to be paid annually." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =291000, AuditProcedureTabContentId =  18000, ParentId=288000, Title="Restrictions on payment of benefits." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =292000, AuditProcedureTabContentId =  18000, ParentId=288000, Title="Prohibition on assignment of members ' superannuation interest." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =293000, AuditProcedureTabContentId =  18000, ParentId=288000, Title="Prohibition on creating charges over members ' benefits." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =294000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Contributions restrictions" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =295000, AuditProcedureTabContentId =  18000, ParentId=294000, Title="Accepts contributions within specified restrictions." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =296000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Reserves / Investment Return Allocation" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =297000, AuditProcedureTabContentId =  18000, ParentId=296000, Title="Reserves to be used appropriately and investment returns must be allocated to members ' accounts in a manner that is fair and reasonable." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =298000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Solvency" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =299000, AuditProcedureTabContentId =  18000, ParentId=298000, Title="Unsatisfactory financial position." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =300000, AuditProcedureTabContentId =  18000, ParentId=null, Title="Other regulatory information" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =301000, AuditProcedureTabContentId =  18000, ParentId=300000, Title="Information regarding the SMSF or trustees which may assist the ATO, including compliance with other relevant SISA sections and SISR regulations." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},

                    
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_8(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =302000, AuditProcedureTabContentId =  19000, ParentId=null, Title="The fund complies with all relevant sections of SISA & SISR to the extent required in the audit report, the trust deed and the investment strategy." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_2_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =12700, AuditProcedureId =209000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12710, AuditProcedureId =209000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =12720, AuditProcedureId=209000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =12730, AuditProcedureId =210000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12735, AuditProcedureId =210000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =12740, AuditProcedureId=210000, AuditProcedureOptionDetailId=400 },

                
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_3_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =12750, AuditProcedureId =222000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12760, AuditProcedureId =222000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =12770, AuditProcedureId=222000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =12780, AuditProcedureId =223000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12790, AuditProcedureId =223000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =12800, AuditProcedureId=223000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =12810, AuditProcedureId =224000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12820, AuditProcedureId =224000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =12830, AuditProcedureId=224000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =12840, AuditProcedureId =225000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12850, AuditProcedureId =225000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =12860, AuditProcedureId=225000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =12870, AuditProcedureId =231000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12880, AuditProcedureId =231000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =12890, AuditProcedureId=231000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =12900, AuditProcedureId =233000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12910, AuditProcedureId =233000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =12920, AuditProcedureId=233000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =12930, AuditProcedureId =234000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12940, AuditProcedureId =234000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =12950, AuditProcedureId=234000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =12960, AuditProcedureId =235000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =12970, AuditProcedureId =235000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =12980, AuditProcedureId=235000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =12990, AuditProcedureId =236000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13000, AuditProcedureId =236000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13010, AuditProcedureId=236000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =13020, AuditProcedureId =237000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13030, AuditProcedureId =237000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13040, AuditProcedureId=237000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =13050, AuditProcedureId =239000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13060, AuditProcedureId =239000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13070, AuditProcedureId=239000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =13080, AuditProcedureId =240000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13090, AuditProcedureId =240000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13100, AuditProcedureId=240000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13110, AuditProcedureId =241000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13120, AuditProcedureId =241000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13130, AuditProcedureId=241000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13140, AuditProcedureId =242000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13150, AuditProcedureId =242000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13160, AuditProcedureId=242000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13170, AuditProcedureId =243000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13180, AuditProcedureId =243000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13190, AuditProcedureId=243000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13200, AuditProcedureId =245000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13210, AuditProcedureId =245000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13220, AuditProcedureId=245000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13230, AuditProcedureId =246000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13240, AuditProcedureId =246000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13250, AuditProcedureId=246000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13260, AuditProcedureId =247000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13270, AuditProcedureId =247000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13280, AuditProcedureId=247000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =13290, AuditProcedureId =248000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13300, AuditProcedureId =248000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13310, AuditProcedureId=248000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =13320, AuditProcedureId =250000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13330, AuditProcedureId =250000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13340, AuditProcedureId=250000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13350, AuditProcedureId =252000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13360, AuditProcedureId =252000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13370, AuditProcedureId=252000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13380, AuditProcedureId =253000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13390, AuditProcedureId =253000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13400, AuditProcedureId=253000, AuditProcedureOptionDetailId=1400 },
                    
                    new AuditProcedureOption {ID =13405, AuditProcedureId =255000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13410, AuditProcedureId =255000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13420, AuditProcedureId=255000, AuditProcedureOptionDetailId=400 },
                    
                    new AuditProcedureOption {ID =13430, AuditProcedureId =256000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13440, AuditProcedureId =256000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13450, AuditProcedureId=256000, AuditProcedureOptionDetailId=1400 }

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_4_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =13460, AuditProcedureId =257000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13470, AuditProcedureId =257000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13480, AuditProcedureId=257000, AuditProcedureOptionDetailId=400 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_5_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =13490, AuditProcedureId =258000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13500, AuditProcedureId =258000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13510, AuditProcedureId=258000, AuditProcedureOptionDetailId=400 },

                    new AuditProcedureOption {ID =13520, AuditProcedureId =259000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13530, AuditProcedureId =259000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13540, AuditProcedureId=259000, AuditProcedureOptionDetailId=400 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_6_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {

                    new AuditProcedureOption {ID =	13550, AuditProcedureId =261000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	13560, AuditProcedureId =261000, AuditProcedureOptionDetailId=2200 },						
                    new AuditProcedureOption {ID =	13570, AuditProcedureId=261000, AuditProcedureOptionDetailId=2300 },						
								
                    new AuditProcedureOption {ID =	13580, AuditProcedureId =263000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	13590, AuditProcedureId =263000, AuditProcedureOptionDetailId=300 },						
                    new AuditProcedureOption {ID =	13600, AuditProcedureId=263000, AuditProcedureOptionDetailId=400 },						
								
                    new AuditProcedureOption {ID =	13610, AuditProcedureId =264000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	13620, AuditProcedureId =264000, AuditProcedureOptionDetailId=300 },						
                    new AuditProcedureOption {ID =	13630, AuditProcedureId=264000, AuditProcedureOptionDetailId=400 },						
								
                    new AuditProcedureOption {ID =	13640, AuditProcedureId =265000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	13650, AuditProcedureId =265000, AuditProcedureOptionDetailId=300 },						
                    new AuditProcedureOption {ID =	13660, AuditProcedureId=265000, AuditProcedureOptionDetailId=400 },						
								
                    new AuditProcedureOption {ID =	13670, AuditProcedureId =266000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	13680, AuditProcedureId =266000, AuditProcedureOptionDetailId=300 },						
                    new AuditProcedureOption {ID =	13690, AuditProcedureId=266000, AuditProcedureOptionDetailId=400 },						
								
                    new AuditProcedureOption {ID =	13700, AuditProcedureId =267000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	13710, AuditProcedureId =267000, AuditProcedureOptionDetailId=300 },						
                    new AuditProcedureOption {ID =	13720, AuditProcedureId=267000, AuditProcedureOptionDetailId=400 },						
								
                    //new AuditProcedureOption {ID =	13730, AuditProcedureId =258000, AuditProcedureOptionDetailId=100 },						
                    //new AuditProcedureOption {ID =	13740, AuditProcedureId =258000, AuditProcedureOptionDetailId=300 },						
                    //new AuditProcedureOption {ID =	13750, AuditProcedureId=258000, AuditProcedureOptionDetailId=400 },						
								
                    //new AuditProcedureOption {ID =	13760, AuditProcedureId =259000, AuditProcedureOptionDetailId=100 },						
                    //new AuditProcedureOption {ID =	13770, AuditProcedureId =259000, AuditProcedureOptionDetailId=300 },						
                    //new AuditProcedureOption {ID =	13780, AuditProcedureId=259000, AuditProcedureOptionDetailId=400 },						
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_3_Item_7_Columns(SMSFContext context)
        {
            var AuditProcedureColumn = new List<AuditProcedureColumn>
                {

                    new AuditProcedureColumn {ID =1000,AuditProcedureId =269000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.17A(97)"},
                    new AuditProcedureColumn {ID =2000,AuditProcedureId =269000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.17A"},

                    new AuditProcedureColumn {ID =3000,AuditProcedureId =270000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.126K(98)"},
                    new AuditProcedureColumn {ID =4000,AuditProcedureId =270000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.126K"},

                    new AuditProcedureColumn {ID =5000,AuditProcedureId =271000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.103"},
                    new AuditProcedureColumn {ID =6000,AuditProcedureId =271000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.103"},

                    new AuditProcedureColumn {ID =7000,AuditProcedureId =272000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.104A(99)"},
                    new AuditProcedureColumn {ID =8000,AuditProcedureId =272000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.104A"},
 
                    new AuditProcedureColumn {ID =9000,AuditProcedureId =273000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.35A"},
                    new AuditProcedureColumn {ID =10000,AuditProcedureId =273000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},
                
                    new AuditProcedureColumn {ID =11000,AuditProcedureId =274000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.35B"},
                    new AuditProcedureColumn {ID =12000,AuditProcedureId =274000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},

                    new AuditProcedureColumn {ID =13000,AuditProcedureId =275000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.35C(2)"},
                    new AuditProcedureColumn {ID =14000,AuditProcedureId =275000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.35C(2)"},

                    new AuditProcedureColumn {ID =15000,AuditProcedureId =276000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.52(2)(e)"},
                    new AuditProcedureColumn {ID =16000,AuditProcedureId =276000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},

                    new AuditProcedureColumn {ID =17000,AuditProcedureId =277000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.4.09"},
                    new AuditProcedureColumn {ID =18000,AuditProcedureId =277000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.4.09"},

                    new AuditProcedureColumn {ID =19000,AuditProcedureId =279000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.62"},
                    new AuditProcedureColumn {ID =20000,AuditProcedureId =279000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.62"},
                    
                    new AuditProcedureColumn {ID =21000,AuditProcedureId =281000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="Ss.69-71E; Ss.73-75; Ss.80-85"},
                    new AuditProcedureColumn {ID =22000,AuditProcedureId =281000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.82, S.83, S.84 & S.85"},

                    new AuditProcedureColumn {ID =23000,AuditProcedureId =282000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.66"},
                    new AuditProcedureColumn {ID =24000,AuditProcedureId =282000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.66"},
                        
                    new AuditProcedureColumn {ID =25000,AuditProcedureId =283000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.109"},
                    new AuditProcedureColumn {ID =26000,AuditProcedureId =283000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.109"},

                    new AuditProcedureColumn {ID =27000,AuditProcedureId =284000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.52(2)(d)"},
                    new AuditProcedureColumn {ID =28000,AuditProcedureId =284000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.52(2)(d)"},

                    new AuditProcedureColumn {ID =29000,AuditProcedureId =285000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.65"},
                    new AuditProcedureColumn {ID =30000,AuditProcedureId =285000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.65"},

                    new AuditProcedureColumn {ID =31000,AuditProcedureId =286000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.67"},
                    new AuditProcedureColumn {ID =32000,AuditProcedureId =286000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.67"},

                    new AuditProcedureColumn {ID =33000,AuditProcedureId =287000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.13.14"},
                    new AuditProcedureColumn {ID =34000,AuditProcedureId =287000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.13.14"},

                    new AuditProcedureColumn {ID =35000,AuditProcedureId =289000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.5.08"},
                    new AuditProcedureColumn {ID =36000,AuditProcedureId =289000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.5.08"},

                    new AuditProcedureColumn {ID =37000,AuditProcedureId =290000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.1.06(9A)"},
                    new AuditProcedureColumn {ID =38000,AuditProcedureId =290000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},

                    new AuditProcedureColumn {ID =39000,AuditProcedureId =291000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.1.06(9A)"},
                    new AuditProcedureColumn {ID =40000,AuditProcedureId =291000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.6.17"},

                    new AuditProcedureColumn {ID =41000,AuditProcedureId =292000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.13.12"},
                    new AuditProcedureColumn {ID =42000,AuditProcedureId =292000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},

                    new AuditProcedureColumn {ID =43000,AuditProcedureId =293000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.13.13"},
                    new AuditProcedureColumn {ID =44000,AuditProcedureId =293000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},

                    new AuditProcedureColumn {ID =45000,AuditProcedureId =295000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.7.04"},
                    new AuditProcedureColumn {ID =46000,AuditProcedureId =295000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R.7.04"},

                    new AuditProcedureColumn {ID =47000,AuditProcedureId =297000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="R. 5.03"},
                    new AuditProcedureColumn {ID =48000,AuditProcedureId =297000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},

                    new AuditProcedureColumn {ID =49000,AuditProcedureId =299000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},
                    new AuditProcedureColumn {ID =50000,AuditProcedureId =299000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.130"},

                    new AuditProcedureColumn {ID =51000,AuditProcedureId =301000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="-"},
                    new AuditProcedureColumn {ID =52000,AuditProcedureId =301000, ColumnName="Auditor's Report Part B SISSA/SISSR", ColumnValue="S.130A"}
                
                };
            context.AuditProcedureColumn.AddRange(AuditProcedureColumn);
            context.SaveChanges();

        }

        #endregion

        #region "Tab_4"
        private static void AuditProcedureIntoTAB_4(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 20000, AuditProcedureTabId=400, Caption =  "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=1,IsActive=true},
                    new AuditProcedureTabContent {ID = 21000, AuditProcedureTabId=400, Caption =  "Statement of FP", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 22000, AuditProcedureTabId=400, Caption =  "Prior Year Checks", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                    new AuditProcedureTabContent {ID = 23000, AuditProcedureTabId=400, Caption =  "General Ledger", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=4,IsActive=true},
                    new AuditProcedureTabContent {ID = 24000, AuditProcedureTabId=400, Caption =  "Receivables", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=5,IsActive=true},
                    new AuditProcedureTabContent {ID = 25000, AuditProcedureTabId=400, Caption =  "Payables", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 26000, AuditProcedureTabId=400, Caption =  "Income Tax Reconcilliation", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 27000, AuditProcedureTabId=400, Caption =  "Accounting Policies", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 28000, AuditProcedureTabId=400, Caption =  "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=7,IsActive=true}
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
            AuditProcedureIntoTAB_4_Item_1(context);
            //AuditProcedureIntoTAB_4_Item_2(context);
            AuditProcedureIntoTAB_4_Item_3(context);
            AuditProcedureIntoTAB_4_Item_4(context);
            AuditProcedureIntoTAB_4_Item_5(context);
            AuditProcedureIntoTAB_4_Item_6(context);
            AuditProcedureIntoTAB_4_Item_7(context);
            AuditProcedureIntoTAB_4_Item_8(context);
            AuditProcedureIntoTAB_4_Item_9(context);

            AuditProcedureIntoTAB_4_Item_2_Options(context);
            AuditProcedureIntoTAB_4_Item_3_Options(context);
            AuditProcedureIntoTAB_4_Item_4_Options(context);
            AuditProcedureIntoTAB_4_Item_5_Options(context);
            AuditProcedureIntoTAB_4_Item_6_Options(context);
            AuditProcedureIntoTAB_4_Item_7_Options(context);
            AuditProcedureIntoTAB_4_Item_8_Options(context);
        }

        private static void AuditProcedureIntoTAB_4_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 320000, AuditProcedureTabContentId =  20000, ParentId=null, Title="Ensure that the additions in the financial report are correct.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    //new AuditProcedures {ID =312000, AuditProcedureTabContentsId =  20000, ParentID=null, Title="To ensure that investments and liabilities" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    //    new AuditProcedures {ID =313000, AuditProcedureTabContentsId =  20000, ParentID=312000, Title="a. Exists;" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    //    new AuditProcedures {ID =314000, AuditProcedureTabContentsId =  20000, ParentID=312000, Title="b. Are properly valued;" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    //    new AuditProcedures {ID =315000, AuditProcedureTabContentsId =  20000, ParentID=312000, Title="c. Ownership is correct;" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    //    new AuditProcedures {ID =316000, AuditProcedureTabContentsId =  20000, ParentID=312000, Title="d. Agree with the investment strategy." , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    //new AuditProcedures {ID =317000, AuditProcedureTabContentsId =  20000, ParentID=null, Title="To ensure that liabilities are:" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    //    new AuditProcedures {ID =318000, AuditProcedureTabContentsId =  20000, ParentID=317000, Title="a. Complete;" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                    //    new AuditProcedures {ID =319000, AuditProcedureTabContentsId =  20000, ParentID=317000, Title="b. Measured reasonably." , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        //private static void AuditProcedureIntoTAB_4_Item_2(SMSFContext context)
        //{
        //    var auditProcedures = new List<AuditProcedures>
        //        {
        //            new AuditProcedures {ID = 320000, AuditProcedureTabContentsId =  21000, ParentID=null, Title="Ensure that the additions in the financial report are correct.", Attachments=true,OML=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
        //        };
        //    context.AuditProcedure.AddRange(auditProcedures);
        //    context.SaveChanges();

        //}

        private static void AuditProcedureIntoTAB_4_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 321000, AuditProcedureTabContentId =  22000, ParentId=null, Title="Do the prior year closing balances agree with current year opening balances?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_4_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 322000, AuditProcedureTabContentId =  23000, ParentId=null, Title="Review the General Ledger for appropriateness of material or abnormal transactions recorded in the general ledger and other adjustments made in the preparation of the financial report.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_4_Item_5(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =324000, AuditProcedureTabContentId =  24000, ParentId=null, Title="Review Receivables balance ensuring:" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =325000, AuditProcedureTabContentId =  24000, ParentId=324000, Title="Receivables owing from prior year have been received." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =326000, AuditProcedureTabContentId =  24000, ParentId=324000, Title="Where material, consider recoverability of the amount and agree amounts subsequently received or reinvested to relevant documentation or bank statement." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =327000, AuditProcedureTabContentId =  24000, ParentId=324000, Title="Ensure that loans have not been advanced to a member of the fund or a relative of the member. If so, consider the following sections of SIS Act: s62, s65, s83 & s109." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =328000, AuditProcedureTabContentId =  24000, ParentId=324000, Title="Ensure that contributions are not recorded as a receivable" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_4_Item_6(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =330000, AuditProcedureTabContentId =  25000, ParentId=null, Title="Review Payables ensuring:" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =331000, AuditProcedureTabContentId =  25000, ParentId=330000, Title="Creditors owing from prior year have been paid." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =332000, AuditProcedureTabContentId =  25000, ParentId=330000, Title="Where material, consider authenticity of creditor / accrual and agree amounts subsequently paid to relevant documentation or bank statement." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =333000, AuditProcedureTabContentId =  25000, ParentId=330000, Title="Member benefits have not been accrued (other than in accordance with the ATO criteria permitted regarding pension shortfall eg honest administration error, error amount was small - not greater than 1/12th of the annual payment, and paid as soon as practical after year end)." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =334000, AuditProcedureTabContentId =  25000, ParentId=330000, Title="The fund has no borrowings other than permitted by s67(4a)." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =335000, AuditProcedureTabContentId =  25000, ParentId=330000, Title="Review BAS returns lodged during the year for reasonableness." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_4_Item_7(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =336000, AuditProcedureTabContentId =  26000, ParentId=null, Title="Ensure all referenced and reconciled amounts agree to relevant reports." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =337000, AuditProcedureTabContentId =  26000, ParentId=null, Title="Agree Income Tax instalments paid to ATO Running Balance Account / Tax Portal." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =338000, AuditProcedureTabContentId =  26000, ParentId=null, Title="Where non arm's income is identified as per the income procedures, ensure it is taxed at maximum rates." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =339000, AuditProcedureTabContentId =  26000, ParentId=null, Title="Agree exempt pension income to actuarial certificate where applicable or segregated assets and income schedule." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =340000, AuditProcedureTabContentId =  26000, ParentId=null, Title="Complete or Review exempt pension calculator and determine if exempt pension income is reasonable." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =340100, AuditProcedureTabContentId =  26000, ParentId=null, Title="Tax Rec Exempt Pension Income (Gross Method) Calculator" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =340200, AuditProcedureTabContentId =  26000, ParentId=null, Title="Tax Rec Exempt Pension Income (Net Method) Calculator" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =343000, AuditProcedureTabContentId =  26000, ParentId=null, Title="Complete or Review Deferred Tax calculator and determine if un-realised capital gains / losses are recorded as reasonable." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =343100, AuditProcedureTabContentId =  26000, ParentId=null, Title="Deffered Tax Calculator" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_4_Item_8(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =345000, AuditProcedureTabContentId =  27000, ParentId=null, Title="Review and gain an understanding of the accounting policies adopted by the trustees of the fund as stated in Note 1 of the financial statements ensuring the following is stated:" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =346000, AuditProcedureTabContentId =  27000, ParentId=345000, Title="Financial statements are either special purpose accounts or the fund is a non reporting entity." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =347000, AuditProcedureTabContentId =  27000, ParentId=345000, Title="How the assets are valued." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =348000, AuditProcedureTabContentId =  27000, ParentId=345000, Title="Whether cash or accruals base accounting has been adopted." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =349000, AuditProcedureTabContentId =  27000, ParentId=345000, Title="Whether deferred tax has been adopted." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =350000, AuditProcedureTabContentId =  27000, ParentId=null, Title="Where polices adopted are not in line with financial accounts prepared, notify the trustees accordingly." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_4_Item_9(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 351000, AuditProcedureTabContentId =  28000, ParentId=null, Title="The statement of financial position is fairly stated", Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_4_Item_2_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =13790, AuditProcedureId =320000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13800, AuditProcedureId =320000, AuditProcedureOptionDetailId=2400 },
                    new AuditProcedureOption {ID =13810, AuditProcedureId =320000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13820, AuditProcedureId =320000, AuditProcedureOptionDetailId=400 },
                    new AuditProcedureOption {ID =13830, AuditProcedureId =320000, AuditProcedureOptionDetailId=200 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        private static void AuditProcedureIntoTAB_4_Item_3_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =13835, AuditProcedureId =321000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13840, AuditProcedureId =321000, AuditProcedureOptionDetailId=300 },
                    new AuditProcedureOption {ID =13850, AuditProcedureId =321000, AuditProcedureOptionDetailId=400 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        private static void AuditProcedureIntoTAB_4_Item_4_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =13860, AuditProcedureId =322000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13870, AuditProcedureId =322000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13880, AuditProcedureId =322000, AuditProcedureOptionDetailId=2400 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        private static void AuditProcedureIntoTAB_4_Item_5_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =13890, AuditProcedureId =324000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13900, AuditProcedureId =324000, AuditProcedureOptionDetailId=1300 },
                    new AuditProcedureOption {ID =13910, AuditProcedureId =324000, AuditProcedureOptionDetailId=2400 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_4_Item_6_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =13920, AuditProcedureId =330000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =13930, AuditProcedureId =330000, AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =13940, AuditProcedureId =330000, AuditProcedureOptionDetailId=2300 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        private static void AuditProcedureIntoTAB_4_Item_7_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =	13950, AuditProcedureId=336000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	13960, AuditProcedureId=336000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	13970, AuditProcedureId=336000,AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	13980, AuditProcedureId=337000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	13990, AuditProcedureId=337000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14000, AuditProcedureId=337000,AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14010, AuditProcedureId=338000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14020, AuditProcedureId=338000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14030, AuditProcedureId=338000,AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14040, AuditProcedureId=339000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14050, AuditProcedureId=339000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14060, AuditProcedureId=339000,AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14070, AuditProcedureId=340000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14080, AuditProcedureId=340000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14090, AuditProcedureId=340000,AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14100, AuditProcedureId=343000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14110, AuditProcedureId=343000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14120, AuditProcedureId=343000,AuditProcedureOptionDetailId=2300 },			
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }


        private static void AuditProcedureIntoTAB_4_Item_8_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =14130, AuditProcedureId =345000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14140, AuditProcedureId =345000, AuditProcedureOptionDetailId=2500 },
                    new AuditProcedureOption {ID =14150, AuditProcedureId =345000, AuditProcedureOptionDetailId=2600 },
                    
                    new AuditProcedureOption {ID =14160, AuditProcedureId =350000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14170, AuditProcedureId =350000, AuditProcedureOptionDetailId=2500 },
                    new AuditProcedureOption {ID =14180, AuditProcedureId =350000, AuditProcedureOptionDetailId=2600 },

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        #endregion

        #region "Tab_5"
        private static void AuditProcedureIntoTAB_5(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 29000, AuditProcedureTabId=500, Caption =  "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=1,IsActive=true},
                    new AuditProcedureTabContent {ID = 30000, AuditProcedureTabId=500, Caption =  "Opening Statement", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 31000, AuditProcedureTabId=500, Caption =  "Prior Year Checks", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                    new AuditProcedureTabContent {ID = 32000, AuditProcedureTabId=500, Caption =  "Changes in Market Value", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=4,IsActive=true},
                    new AuditProcedureTabContent {ID = 33000, AuditProcedureTabId=500, Caption =  "Expanses", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=5,IsActive=true},
                    new AuditProcedureTabContent {ID = 34000, AuditProcedureTabId=500, Caption =  "Income Tax Expanses", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 35000, AuditProcedureTabId=500, Caption =  "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=7,IsActive=true}
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
            AuditProcedureIntoTAB_5_Item_1(context);
            AuditProcedureIntoTAB_5_Item_2(context);
            AuditProcedureIntoTAB_5_Item_3(context);
            AuditProcedureIntoTAB_5_Item_4(context);
            AuditProcedureIntoTAB_5_Item_5(context);
            AuditProcedureIntoTAB_5_Item_6(context);
            AuditProcedureIntoTAB_5_Item_7(context);

            AuditProcedureIntoTAB_5_Item_2_Options(context);
            AuditProcedureIntoTAB_5_Item_3_Options(context);
            AuditProcedureIntoTAB_5_Item_4_Options(context);
            AuditProcedureIntoTAB_5_Item_5_Options(context);
        }

        private static void AuditProcedureIntoTAB_5_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =359000, AuditProcedureTabContentId =  29000, ParentId=null, Title="To ensure that investments and liabilities" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =360000, AuditProcedureTabContentId =  29000, ParentId=359000, Title="Have occurred and pertain to the SMSF" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =361000, AuditProcedureTabContentId =  29000, ParentId=359000, Title="Are completely recorded" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =362000, AuditProcedureTabContentId =  29000, ParentId=359000, Title="Have been accurately recorded" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =363000, AuditProcedureTabContentId =  29000, ParentId=359000, Title="Have been recorded in the correct period." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =364000, AuditProcedureTabContentId =  29000, ParentId=359000, Title="Have been recorded in the proper accounts" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_5_Item_2(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 365000, AuditProcedureTabContentId =  30000, ParentId=null, Title="Check Additions in the financial report are correct.", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_5_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 365100, AuditProcedureTabContentId =  31000, ParentId=null, Title="Do the prior year closing balances agree with current year opening balances?", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_5_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID = 366000, AuditProcedureTabContentId =  32000, ParentId=null, Title="Unrealised Change in market value", Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID = 367000, AuditProcedureTabContentId =  32000, ParentId=366000, Title="Agree change in market value to the change in market value calculator (complete if necessary)", Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_5_Item_5(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =368000, AuditProcedureTabContentId =  33000, ParentId=null, Title="Agree large or unusual expenses to supporting documentation" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =369000, AuditProcedureTabContentId =  33000, ParentId=null, Title="Review a sample of expenses to ensure that they have been paid from the fund's bank account?" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =370000, AuditProcedureTabContentId =  33000, ParentId=null, Title="Agree the amount paid to the insurance policy document and/or invoice" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =371000, AuditProcedureTabContentId =  33000, ParentId=null, Title="Review the insurance policy to check title of the policy owner - ensure that a tax deduction has not been claimed if the trustee is not the policy owner" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_5_Item_6(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =372000, AuditProcedureTabContentId =  34000, ParentId=null, Title="Agree Income Tax Expense to the Income Tax Expense calculator (complete if necessary)" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_5_Item_7(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =373000, AuditProcedureTabContentId =  35000, ParentId=null, Title="We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that expenses are fairly stated." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }

        private static void AuditProcedureIntoTAB_5_Item_2_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =14190, AuditProcedureId =365000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14200, AuditProcedureId =365000, AuditProcedureOptionDetailId=2400 },
                    new AuditProcedureOption {ID =14210, AuditProcedureId =365000, AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =14220, AuditProcedureId =365000, AuditProcedureOptionDetailId=2300 },

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        private static void AuditProcedureIntoTAB_5_Item_3_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =14230, AuditProcedureId =365100, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14240, AuditProcedureId =365100, AuditProcedureOptionDetailId=2400 },
                    new AuditProcedureOption {ID =14250, AuditProcedureId =365100, AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =14260, AuditProcedureId =365100, AuditProcedureOptionDetailId=2300 },

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        private static void AuditProcedureIntoTAB_5_Item_4_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =14270, AuditProcedureId =367000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14280, AuditProcedureId =367000, AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =14290, AuditProcedureId =367000, AuditProcedureOptionDetailId=2300 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        private static void AuditProcedureIntoTAB_5_Item_5_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    
                    new AuditProcedureOption {ID =	14300, AuditProcedureId=368000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14310, AuditProcedureId=368000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14320, AuditProcedureId=368000,AuditProcedureOptionDetailId=2300 },			
                    new AuditProcedureOption {ID =	14330, AuditProcedureId=368000,AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	14350, AuditProcedureId=368000,AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	14360, AuditProcedureId=369000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14370, AuditProcedureId=369000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14380, AuditProcedureId=369000,AuditProcedureOptionDetailId=2300 },			
                    new AuditProcedureOption {ID =	14390, AuditProcedureId=369000,AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	14400, AuditProcedureId=369000,AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	14410, AuditProcedureId=370000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14420, AuditProcedureId=370000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14430, AuditProcedureId=370000,AuditProcedureOptionDetailId=2300 },			
                    new AuditProcedureOption {ID =	14440, AuditProcedureId=370000,AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	14450, AuditProcedureId=370000,AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	14460, AuditProcedureId=371000,AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14470, AuditProcedureId=371000,AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14480, AuditProcedureId=371000,AuditProcedureOptionDetailId=2300 },			
                    new AuditProcedureOption {ID =	14490, AuditProcedureId=371000,AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	14500, AuditProcedureId=371000,AuditProcedureOptionDetailId=1400 },	
		
                    new AuditProcedureOption {ID =	14510, AuditProcedureId=372000,AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =	14520, AuditProcedureId=372000,AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =	14530, AuditProcedureId=372000,AuditProcedureOptionDetailId=2300 },
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        #endregion

        #region "Tab_6"
        private static void AuditProcedureIntoTAB_6(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 36000, AuditProcedureTabId=600, Caption =  "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=1,IsActive=true},
                    new AuditProcedureTabContent {ID = 37000, AuditProcedureTabId=600, Caption =  "Bank Accounts", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 38000, AuditProcedureTabId=600, Caption =  "Term Deposit / Fixed Interest Securities", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                    new AuditProcedureTabContent {ID = 39000, AuditProcedureTabId=600, Caption =  "Listed companies, managed investments &amp; listed &amp; unlisted (widely held) trusts investments", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=4,IsActive=true},
                    new AuditProcedureTabContent {ID = 40000, AuditProcedureTabId=600, Caption =  "Unlisted companies and trusts (closely held or private related)", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=5,IsActive=true},
                    new AuditProcedureTabContent {ID = 41000, AuditProcedureTabId=600, Caption =  "Property", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 42000, AuditProcedureTabId=600, Caption =  "Collectables", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=7,IsActive=true},
                    new AuditProcedureTabContent {ID = 43000, AuditProcedureTabId=600, Caption =  "Loans", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=7,IsActive=true},
                    new AuditProcedureTabContent {ID = 44000, AuditProcedureTabId=600, Caption =  "Instalment Warrants", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=7,IsActive=true},
                    new AuditProcedureTabContent {ID = 45000, AuditProcedureTabId=600, Caption =  "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=7,IsActive=true}
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
            AuditProcedureIntoTAB_6_Item_1(context);
            AuditProcedureIntoTAB_6_Item_2(context);
            AuditProcedureIntoTAB_6_Item_3(context);
            AuditProcedureIntoTAB_6_Item_4(context);
            AuditProcedureIntoTAB_6_Item_5(context);
            AuditProcedureIntoTAB_6_Item_6(context);
            AuditProcedureIntoTAB_6_Item_7(context);
            AuditProcedureIntoTAB_6_Item_8(context);
            AuditProcedureIntoTAB_6_Item_9(context);
            AuditProcedureIntoTAB_6_Item_10(context);

            AuditProcedureIntoTAB_6_Item_2_Options(context);
            AuditProcedureIntoTAB_6_Item_3_Options(context);
            AuditProcedureIntoTAB_6_Item_4_Options(context);
            AuditProcedureIntoTAB_6_Item_5_Options(context);
            AuditProcedureIntoTAB_6_Item_6_Options(context);
            AuditProcedureIntoTAB_6_Item_7_Options(context);
            AuditProcedureIntoTAB_6_Item_8_Options(context);
            AuditProcedureIntoTAB_6_Item_9_Options(context);

        }
        private static void AuditProcedureIntoTAB_6_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =385000, AuditProcedureTabContentId =  36000, ParentId=null, Title="To ensure that additions and disposals are" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =386000, AuditProcedureTabContentId =  36000, ParentId=385000, Title="a. Complete" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =387000, AuditProcedureTabContentId =  36000, ParentId=385000, Title="b. Properly valued" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =388000, AuditProcedureTabContentId =  36000, ParentId=385000, Title="c. Ownership is correct" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_2(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =390000, AuditProcedureTabContentId =  37000, ParentId=null, Title="Agree bank account statement balance and title as at 30 June to the financial statements." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =391000, AuditProcedureTabContentId =  37000, ParentId=null, Title="Review bank statements for large & abnormal transactions, overdrawn balance." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =392000, AuditProcedureTabContentId =  37000, ParentId=null, Title="Review subsequent period bank statements for large or unusual items" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =393000, AuditProcedureTabContentId =  37000, ParentId=null, Title="Where the bank balance represents more than 50% of the value of the fund, obtain direct confirmation from financial institution either by completing bank confirmation audit request or obtain direct correspondence from the financial institution." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =394000, AuditProcedureTabContentId =  38000, ParentId=null, Title="Agree term deposit statement balance  and title at 30 June" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =395000, AuditProcedureTabContentId =  38000, ParentId=null, Title="Obtain and review post 30 June maturity notices" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =396000, AuditProcedureTabContentId =  38000, ParentId=null, Title="Where term deposit balance represents more than 50% of the value of the fund, obtain direct confirmation from financial institution either by completing bank confirmation audit request or obtain direct correspondence from the financial institution." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =397000, AuditProcedureTabContentId =  39000, ParentId=null, Title="Holdings" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =398000, AuditProcedureTabContentId =  39000, ParentId=null, Title="Non-managed share holdings and/or Broker managed" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =399000, AuditProcedureTabContentId =  39000, ParentId=398000, Title="Where holdings have been checked against automatic data feeds ensure that no discrepancies are highlighted." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =400000, AuditProcedureTabContentId =  39000, ParentId=398000, Title="Where holdings have not been automatically verified, test ownership by agreeing the title and agree the number of securities held as at 30 June  to either year end holding statements or via the following (obtain HIN & postcode from dividend notices):(Refer to samples selected above in the investment holding report):" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =404000, AuditProcedureTabContentId =  39000, ParentId=398000, Title="Confirm the closing market price of the securities at 30 June to CRS booklet or Yahoo finance and cross check calculations" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =405000, AuditProcedureTabContentId =  39000, ParentId=null, Title="Managed Investments / Wrap Accounts" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =406000, AuditProcedureTabContentId =  39000, ParentId=405000, Title="Review managed portfolio valuations report as at year end for completeness." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =407000, AuditProcedureTabContentId =  39000, ParentId=405000, Title="Obtain Independent External Auditor's report of the fund manager." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =408000, AuditProcedureTabContentId =  39000, ParentId=null, Title="Movement" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =409000, AuditProcedureTabContentId =  39000, ParentId=408000, Title="Select a sample of additions and disposals and agree to buy contract notes, brokers’ transaction statements, portfolio valuation statement, share transfer forms and bank statements." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =410000, AuditProcedureTabContentId =  39000, ParentId=408000, Title="Check the calculation of the gains / losses: Sale price - purchase price = profit / loss agreed to Investment disposals report (if applicable)." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =411000, AuditProcedureTabContentId =  39000, ParentId=null, Title="Off-market Transfers" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =412000, AuditProcedureTabContentId =  39000, ParentId=411000, Title="If there are any off Market Transfers, agree to off market transfer form and minutes of meeting." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =413000, AuditProcedureTabContentId =  39000, ParentId=411000, Title="Agree sale prices to Hyper Link: http://yahoo.com.au/finance  Hyper Link: or Hyper Link:http://tradingroom.com.au  Hyper Link: to ensure they recorded at market value." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =414000, AuditProcedureTabContentId =  39000, ParentId=411000, Title="If off-market transfer is done in-specie, ensure the trust deed allows this." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =415000, AuditProcedureTabContentId =  39000, ParentId=411000, Title="Check the calculation of the gains / losses: Sale price - purchase price = profit agreed to Investment disposals report (if applicable)." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_5(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =416000, AuditProcedureTabContentId =  40000, ParentId=null, Title="Movement" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =417000, AuditProcedureTabContentId =  40000, ParentId=416000, Title="Select a sample of additions and disposals and agree to share / unit transfer forms, minutes of meetings, valuation, bank statements and other supporting documents" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =418000, AuditProcedureTabContentId =  40000, ParentId=416000, Title="Check the calculation of the gains / losses: Sale price - purchase price = profit / loss agreed to Investment disposals report (if applicable)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =419000, AuditProcedureTabContentId =  40000, ParentId=416000, Title="Where the new or increase in a existing investment has been made in a closely held company or trust, ensure that the member/trustees and their Part 8 Associates do not collectively have control of the company / trust by majority shareholding/unit holding and/or voting rights)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =420000, AuditProcedureTabContentId =  40000, ParentId=416000, Title="If yes, ensure that the investment complies with the in-house asset provisions of the SIS Act (sections 71 - 85)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =421000, AuditProcedureTabContentId =  40000, ParentId=416000, Title="If acquired from related party, ensure section 66 of the SIS Act permits the acquisition" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =422000, AuditProcedureTabContentId =  40000, ParentId=null, Title="Holdings" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =423000, AuditProcedureTabContentId =  40000, ParentId=422000, Title="Investment in unlisted companies / trusts / partnerships (un-related)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =424000, AuditProcedureTabContentId =  40000, ParentId=423000, Title="Select a sample and sight original share / unit certificates and agree the number of shares / units held and the title correctly recorded as at 30 June." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =425000, AuditProcedureTabContentId =  40000, ParentId=423000, Title="Obtain a copy of the financial report and income tax return of the company / trust for closely held investments." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =426000, AuditProcedureTabContentId =  40000, ParentId=423000, Title="Ensure the investment is appropriately valued and in accordance with accounting polices adopted." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                        new AuditProcedure {ID =427000, AuditProcedureTabContentId =  40000, ParentId=422000, Title="Unlisted Related Company/Trust - pre 11 August 1999" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =428000, AuditProcedureTabContentId =  40000, ParentId=427000, Title="Obtain a signed copy of the unit trust deed to confirm that the date of establishment is prior to 11 August 1999." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =429000, AuditProcedureTabContentId =  40000, ParentId=427000, Title="Sight original share / unit certificates and agree the number of shares /units and title is correctly held at 30 June." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =430000, AuditProcedureTabContentId =  40000, ParentId=427000, Title="Obtain a copy of the financial report and Income tax return of the company / trust. Ensure director's / trustee's declarations is signed." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =431000, AuditProcedureTabContentId =  40000, ParentId=427000, Title="Ensure that the value of the shares / units of ...........Pty ltd / unit trust recorded in the financial statements of fund agree with the net asset per unit recorded in the financial statements of the  ...........Pty ltd / unit trust." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =432000, AuditProcedureTabContentId =  40000, ParentId=431000, Title="If the fund made additional investments in the unlisted unit trust during the year either by way of reinvestment of earnings or an additional investment ensure that the additional investments comply with section 71D and 71E as follows:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                                    new AuditProcedure {ID =433000, AuditProcedureTabContentId =  40000, ParentId=432000, Title="s.71D Distribution Re-investment - The total additional investment in the trust between 11/8/99 and 30/6/09 do not exceed the superfund distribution entitlements between the same dates." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                    new AuditProcedure {ID =434000, AuditProcedureTabContentId =  40000, ParentId=432000, Title="s.71E Debt Election - If the trustee has made a written election by 22/10/00 no other exceptions apply. If not, the total additional investments in the trust between 11/8/99 and 30/6/09 do not exceed the debt level in the trust as at 11/8/99 (excluding loans from the superfund)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                    new AuditProcedure {ID =435000, AuditProcedureTabContentId =  40000, ParentId=432000, Title="c. Post 01/07/2009 - Any additional investment made by the fund into the pre 11 August 1999 trust, will be classed as an in-house asset under s.82." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                            new AuditProcedure {ID =436000, AuditProcedureTabContentId =  40000, ParentId=427000, Title="If the pre 11 August 1999 is converted into a trust under Reg 13.22C, any additional investments made by the fund in the trust will be excluded as an in-house asset if the following are met:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =437000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="That the trust has not borrowed any further, conducted any new business or acquired an interest in any other entity post 28/6/2000" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =438000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Does not have any borrowings or loans." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =439000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Does not have any investments in any other entity (including the employer sponsor of the fund or an associate of either of them)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =440000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Does not have a charge over any assets." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =441000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Has not acquired any assets from a related party of the fund (except business real property)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =442000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Has not entered into a lease agreement with a related party of the fund (except where the lease relates to a business real property)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =443000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Has not entered into a loan or other form of financial assistance to a related party of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =444000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Does not conduct a business." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                                new AuditProcedure {ID =445000, AuditProcedureTabContentId =  40000, ParentId=436000, Title="Does not enter into any transaction other than on an arm's length basis." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                        new AuditProcedure {ID =446000, AuditProcedureTabContentId =  40000, ParentId=422000, Title="That the trust has any loans with related parties or members, ensure the interest rate is charged at arm's length (refer to ATO benchmark interest rates)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =447000, AuditProcedureTabContentId =  40000, ParentId=null, Title="Unlisted Related Company/Trust - post 11 August 1999" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =448000, AuditProcedureTabContentId =  40000, ParentId=447000, Title="If the investment is in an unlisted related company, ensure that the said investment and the investments in other related assets together do not constitute more than 5% of the total assets of the super fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =449000, AuditProcedureTabContentId =  40000, ParentId=447000, Title="Sight original share / unit certificates and agree the number of shares /units and title is correctly held at 30 June." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =450000, AuditProcedureTabContentId =  40000, ParentId=447000, Title="Obtain a copy of the financial report and Income tax return of the company / trust. Ensure director's / trustee's declarations is signed and the company / trust does not do the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =451000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Does not have any borrowings or loans." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =452000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Does not have any investments in any other entity (including the employer sponsor of the fund or an associate of either of them)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =453000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Does not have a charge over any assets." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =454000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Has not acquired any assets from a related party of the fund (except business real property)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =455000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Has not entered into a lease agreement with a related party of the fund (except where the lease relates to a business real property)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =456000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Has not entered into a loan or other form of financial assistance to a related party of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =457000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Does not conduct a business." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =458000, AuditProcedureTabContentId =  40000, ParentId=450000, Title="Does not enter into any transaction other than on an arm's length basis." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                        new AuditProcedure {ID =459000, AuditProcedureTabContentId =  40000, ParentId=447000, Title="If the related unit trust did not comply with any of the above the fund's whole investment in the trust breaches section 82, thereby perpetually remaining as in-house asset of the fund. The trustees should be notified of this breach as the trust must be wound up." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =460000, AuditProcedureTabContentId =  40000, ParentId=447000, Title="Ensure that the value of the shares / units of ...........Pty ltd / unit trust recorded in the financial statements of fund agree withy the net asset per unit recorded in the financial statements of the  ...........Pty ltd / unit trust." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =461000, AuditProcedureTabContentId =  40000, ParentId=447000, Title="Verify material underlying items appearing in the financial statements of the company / trust per normal audit procedures in this program." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                        

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_6(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =462000, AuditProcedureTabContentId =  41000, ParentId=null, Title="Holdings" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =463000, AuditProcedureTabContentId =  41000, ParentId=462000, Title="Perform a title search on the property if not provided. Ensure that the  title of the real estate property is in the name of the trustees and perform the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =464000, AuditProcedureTabContentId =  41000, ParentId=463000, Title="Agree property details on the title search (i.e. Lot number and Plan number) to the property details recorded in the financial statements." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =465000, AuditProcedureTabContentId =  41000, ParentId=463000, Title="Confirm fund ownership by sighting either a purchase contract, deed of trust, or caveat acknowledging the trustees." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =466000, AuditProcedureTabContentId =  41000, ParentId=463000, Title="Where fund owns less than 100%, ensure the title of the property is held as tenants in common, not as joint tenants.." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =467000, AuditProcedureTabContentId =  41000, ParentId=462000, Title="Ensure that there are no encumbrances over the title." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =468000, AuditProcedureTabContentId =  41000, ParentId=462000, Title="Ensure annual valuation of the property is based on objective and supportive data. Eg appraisal or a minute by the trustees (minute must state value of the property and how the value was determined)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =469000, AuditProcedureTabContentId =  41000, ParentId=462000, Title="Agree valuation to the financial statements and is consistant with the accounting policies." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =470000, AuditProcedureTabContentId =  41000, ParentId=462000, Title="Where applicable, review depreciation rates to determine their appropriateness and consistency with prior years." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =471000, AuditProcedureTabContentId =  41000, ParentId=null, Title="Movement" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =472000, AuditProcedureTabContentId =  41000, ParentId=471000, Title="Agree additions and disposals to supporting documentation including purchase and Sale contracts and solicitors documentation and settlement sheet." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =473000, AuditProcedureTabContentId =  41000, ParentId=471000, Title="If the real estate property acquired from a related party of the fund, ensure the acquisition is permitted in accordance with section 66 of the SIS Act ie it is business real property and acquired on an arm's length basis." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =474000, AuditProcedureTabContentId =  41000, ParentId=471000, Title="Check the calculation of the gains / losses: Sale price - purchase price = profit / loss agreed to Investment disposals report (if applicable)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_7(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =475000, AuditProcedureTabContentId =  42000, ParentId=null, Title="Collectables and personal use assets (Reg13.18AA) includes artwork, jewellery, antiques, artefacts, coins, medallion, bank notes, postage stamps, folio, manuscripts, books, memorabilia, wine, spirits, motor vehicles, recreational boats, memberships of sporting or social clubs," , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =476000, AuditProcedureTabContentId =  42000, ParentId=475000, Title="Holdings" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =477000, AuditProcedureTabContentId =  42000, ParentId=476000, Title="Confirm the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =478000, AuditProcedureTabContentId =  42000, ParentId=476000, Title="Fund has legal ownership of the collectable" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =479000, AuditProcedureTabContentId =  42000, ParentId=476000, Title="Where material, ensure the collectable exists" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =480000, AuditProcedureTabContentId =  42000, ParentId=476000, Title="Confirm the location of the collectable and ensure the collectable is not made available for personal use or enjoyment" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =481000, AuditProcedureTabContentId =  42000, ParentId=476000, Title="Ensure value of the collectable is based on objective and supportive data" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =482000, AuditProcedureTabContentId =  42000, ParentId=476000, Title="Ensure collectable is adequately insured" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =483000, AuditProcedureTabContentId =  42000, ParentId=null, Title="Movement" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =484000, AuditProcedureTabContentId =  42000, ParentId=483000, Title="For acquisitions, they must comply with all of the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =485000, AuditProcedureTabContentId =  42000, ParentId=484000, Title="Asset must not be leased to a related party" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =486000, AuditProcedureTabContentId =  42000, ParentId=484000, Title="Item must not be stored in private residence of a related party" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =487000, AuditProcedureTabContentId =  42000, ParentId=484000, Title="Decision on storage of item must be documented" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =488000, AuditProcedureTabContentId =  42000, ParentId=484000, Title="Item must be insured in fund's name within 7 days of acquiition" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =489000, AuditProcedureTabContentId =  42000, ParentId=484000, Title="Item must not be used by related party" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},

                    new AuditProcedure {ID =490000, AuditProcedureTabContentId =  42000, ParentId=null, Title="For disposals:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =491000, AuditProcedureTabContentId =  42000, ParentId=490000, Title="Was the collectable disposed of to a related party?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =492000, AuditProcedureTabContentId =  42000, ParentId=490000, Title="If yes and acquired before 1 July 2011, was it based on objective and supportive data?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =493000, AuditProcedureTabContentId =  42000, ParentId=490000, Title="If yes and acquired after 1 July 2011, was an independent valuation obtained?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_8(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =494000, AuditProcedureTabContentId =  43000, ParentId=null, Title="Holdings" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =495000, AuditProcedureTabContentId =  43000, ParentId=494000, Title="Obtain loan agreement and review the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =496000, AuditProcedureTabContentId =  43000, ParentId=495000, Title="Review reasonableness of interest rate (refer to ATO benchmark interest rates) and  loan term." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =497000, AuditProcedureTabContentId =  43000, ParentId=495000, Title="Perform review of the loan to identify if loan has been advanced to related party or third party" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =498000, AuditProcedureTabContentId =  43000, ParentId=494000, Title="If the loan has been made to a related party ensure the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =499000, AuditProcedureTabContentId =  43000, ParentId=498000, Title="The loan does not exceed 5% of the value of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =500000, AuditProcedureTabContentId =  43000, ParentId=498000, Title="The loan is not advanced to member of the fund or a relative of the member." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =501000, AuditProcedureTabContentId =  43000, ParentId=498000, Title="The interest rate is charged at arm's length (eg refer to ATO benchmark interest rates)." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =502000, AuditProcedureTabContentId =  43000, ParentId=498000, Title="Agree a sample of loan repayments to the loan agreement to confirm repayments are as per the loan agreement." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =503000, AuditProcedureTabContentId =  43000, ParentId=494000, Title="add loan confirmation template here" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =504000, AuditProcedureTabContentId =  43000, ParentId=null, Title="Movement" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =505000, AuditProcedureTabContentId =  43000, ParentId=504000, Title="Agree a sample of the loan repayments per the loan agreement to the bank statement" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_9(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =506000, AuditProcedureTabContentId =  44000, ParentId=null, Title="Does the trust deed permit the borrowing under an instalment warrant arrangement." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =507000, AuditProcedureTabContentId =  44000, ParentId=null, Title="The borrowing was used to acquire a single asset." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =508000, AuditProcedureTabContentId =  44000, ParentId=null, Title="Review the loan agreement, ensuring the borrower is the fund trustee, agreement is limited in recourse, security provided is limited to the assets acquired. Also check the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =509000, AuditProcedureTabContentId =  44000, ParentId=508000, Title="Where personal guarantees have been provided, ensure agreement has been prepared where the guarantors have no further recourse against the fund assets." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =510000, AuditProcedureTabContentId =  44000, ParentId=508000, Title="If final instalment has been repaid, has the title been transferred to the trustee of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =511000, AuditProcedureTabContentId =  44000, ParentId=508000, Title="If yes, has the mortgage been discharged?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =512000, AuditProcedureTabContentId =  44000, ParentId=508000, Title="Was the loan entered into before 6 July 2010?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =513000, AuditProcedureTabContentId =  44000, ParentId=508000, Title="If yes, has the loan been refinanced since that date?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =514000, AuditProcedureTabContentId =  44000, ParentId=null, Title="Review the custodian/bare trust borrowing agreement ensuring the following:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =515000, AuditProcedureTabContentId =  44000, ParentId=514000, Title="Confirm the custodian trustee is not the same as the trustee of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =516000, AuditProcedureTabContentId =  44000, ParentId=514000, Title="Is the custodian / bare trust established on or before the date of the contract of sale, or the date of the nomination election?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =517000, AuditProcedureTabContentId =  44000, ParentId=514000, Title="If an “and / or nominee” is noted on the contract of sale, does the nomination form note the custody trustee in trust for the fund trustee?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =518000, AuditProcedureTabContentId =  44000, ParentId=null, Title="Has the trustees considered the future funding arrangements and the risk associated in the event of a member of the fund deceasing or becoming permanently disabled eg taken out life, trauma and/or income protection insurance policies on each member?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_10(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =519000, AuditProcedureTabContentId =  45000, ParentId=null, Title="We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that movements are fairly stated." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_6_Item_2_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =	14540	, AuditProcedureId =390000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	14550	, AuditProcedureId =390000, AuditProcedureOptionDetailId=2200 },						
                    new AuditProcedureOption {ID =	14560	, AuditProcedureId =390000, AuditProcedureOptionDetailId=2300 },						
								
                    new AuditProcedureOption {ID =	14570	, AuditProcedureId =391000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	14580	, AuditProcedureId =391000, AuditProcedureOptionDetailId=2200 },						
                    new AuditProcedureOption {ID =	14590	, AuditProcedureId =391000, AuditProcedureOptionDetailId=2300 },						
								
                    new AuditProcedureOption {ID =	14600, AuditProcedureId =392000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	14610	, AuditProcedureId =392000, AuditProcedureOptionDetailId=2200 },						
                    new AuditProcedureOption {ID =	14620, AuditProcedureId =392000, AuditProcedureOptionDetailId=2300 },						
								
                    new AuditProcedureOption {ID =	14630, AuditProcedureId =393000, AuditProcedureOptionDetailId=100 },						
                    new AuditProcedureOption {ID =	14640, AuditProcedureId =393000, AuditProcedureOptionDetailId=2200 },						
                    new AuditProcedureOption {ID =	14650, AuditProcedureId =393000, AuditProcedureOptionDetailId=2300 },						
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_6_Item_3_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =14660, AuditProcedureId =394000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14670, AuditProcedureId =394000, AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =14680, AuditProcedureId =394000, AuditProcedureOptionDetailId=2300 },

                    new AuditProcedureOption {ID =14690, AuditProcedureId =395000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14700, AuditProcedureId =395000, AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =14710, AuditProcedureId =395000, AuditProcedureOptionDetailId=2300 },

                    new AuditProcedureOption {ID =14720, AuditProcedureId =396000, AuditProcedureOptionDetailId=100 },
                    new AuditProcedureOption {ID =14730, AuditProcedureId =396000, AuditProcedureOptionDetailId=2200 },
                    new AuditProcedureOption {ID =14740, AuditProcedureId =396000, AuditProcedureOptionDetailId=2300 },

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_6_Item_4_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =	14750	, AuditProcedureId	=399000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14760	, AuditProcedureId	=399000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14770	, AuditProcedureId	=399000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14780	, AuditProcedureId	=400000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14790	, AuditProcedureId	=400000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14800, AuditProcedureId	=400000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14810	, AuditProcedureId	=404000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14820	, AuditProcedureId	=404000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14830	, AuditProcedureId	=404000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14840	, AuditProcedureId	=406000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14850	, AuditProcedureId	=406000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14860	, AuditProcedureId	=406000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14870	, AuditProcedureId	=407000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14880	, AuditProcedureId	=407000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14890	, AuditProcedureId	=407000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14900	, AuditProcedureId	=409000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14910	, AuditProcedureId	=409000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14920	, AuditProcedureId	=409000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14930	, AuditProcedureId	=410000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14940	, AuditProcedureId	=410000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14950	, AuditProcedureId	=410000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14960	, AuditProcedureId	=412000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	14970, AuditProcedureId	=412000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	14980	, AuditProcedureId	=412000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	14990	, AuditProcedureId	=413000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15000	, AuditProcedureId	=413000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15010	, AuditProcedureId	=413000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15020	, AuditProcedureId	=414000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15030	, AuditProcedureId	=414000,	AuditProcedureOptionDetailId=300 },			
                    new AuditProcedureOption {ID =	15040	, AuditProcedureId	=414000,	AuditProcedureOptionDetailId=400 },			
							
                    new AuditProcedureOption {ID =	15050	, AuditProcedureId	=415000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15060	, AuditProcedureId	=415000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15070	, AuditProcedureId	=415000,	AuditProcedureOptionDetailId=2300 }			

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_6_Item_5_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =	15080	, AuditProcedureId	=417000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15090	, AuditProcedureId	=417000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15100	, AuditProcedureId	=417000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15110	, AuditProcedureId	=418000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15120	, AuditProcedureId	=418000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15130	, AuditProcedureId	=418000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15140	, AuditProcedureId	=419000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15150	, AuditProcedureId	=419000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15160	, AuditProcedureId	=419000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15170	, AuditProcedureId	=420000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15180	, AuditProcedureId	=420000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15190	, AuditProcedureId	=420000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15200	, AuditProcedureId	=421000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15210	, AuditProcedureId	=421000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15220	, AuditProcedureId	=421000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15230	, AuditProcedureId	=423000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15240	, AuditProcedureId	=423000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15250	, AuditProcedureId	=423000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15260	, AuditProcedureId	=424000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15270	, AuditProcedureId	=424000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15280	, AuditProcedureId	=424000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15290	, AuditProcedureId	=425000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15300	, AuditProcedureId	=425000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15310	, AuditProcedureId	=425000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15320	, AuditProcedureId	=426000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15330	, AuditProcedureId	=426000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15340	, AuditProcedureId	=426000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15350	, AuditProcedureId	=428000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15360	, AuditProcedureId	=428000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15370	, AuditProcedureId	=428000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15380	, AuditProcedureId	=429000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15390	, AuditProcedureId	=429000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15400	, AuditProcedureId	=429000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15410	, AuditProcedureId	=430000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15420	, AuditProcedureId	=430000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15430	, AuditProcedureId	=430000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15440	, AuditProcedureId	=431000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15450	, AuditProcedureId	=431000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15460	, AuditProcedureId	=431000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15470	, AuditProcedureId	=433000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15480	, AuditProcedureId	=433000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15490	, AuditProcedureId	=433000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15500	, AuditProcedureId	=434000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15510	, AuditProcedureId	=434000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15520	, AuditProcedureId	=434000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15530	, AuditProcedureId	=435000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15540	, AuditProcedureId	=435000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15550	, AuditProcedureId	=435000,	AuditProcedureOptionDetailId=1400 },			
						
                    new AuditProcedureOption {ID =	15560	, AuditProcedureId	=436000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15570	, AuditProcedureId	=436000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15580	, AuditProcedureId	=436000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	15590	, AuditProcedureId	=436000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	15600	, AuditProcedureId	=437000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15610	, AuditProcedureId	=437000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15620	, AuditProcedureId	=437000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	15630	, AuditProcedureId	=437000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	15640	, AuditProcedureId	=438000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15650	, AuditProcedureId	=438000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15660	, AuditProcedureId	=438000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15670	, AuditProcedureId	=439000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15680	, AuditProcedureId	=439000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15690   , AuditProcedureId	=439000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15700	, AuditProcedureId	=440000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15710	, AuditProcedureId	=440000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15720	, AuditProcedureId	=440000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15730	, AuditProcedureId	=441000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15740	, AuditProcedureId	=441000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15750	, AuditProcedureId	=441000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15760	, AuditProcedureId	=442000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15770	, AuditProcedureId	=442000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15780	, AuditProcedureId	=442000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15790	, AuditProcedureId	=443000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15800	, AuditProcedureId	=443000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15810	, AuditProcedureId	=443000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15820	, AuditProcedureId	=444000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15830	, AuditProcedureId	=444000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15840	, AuditProcedureId	=444000,	AuditProcedureOptionDetailId=2300 },			
						
                    new AuditProcedureOption {ID =	15850	, AuditProcedureId	=445000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15860	, AuditProcedureId	=445000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15870	, AuditProcedureId	=445000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15880	, AuditProcedureId	=446000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15890	, AuditProcedureId	=446000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15900	, AuditProcedureId	=446000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	15910	, AuditProcedureId	=446000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	15920	, AuditProcedureId	=448000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15930	, AuditProcedureId	=448000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	15940	, AuditProcedureId	=448000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	15950	, AuditProcedureId	=449000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15960	, AuditProcedureId	=449000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	15970	, AuditProcedureId	=449000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	15980	, AuditProcedureId	=450000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	15990	, AuditProcedureId	=450000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16000	, AuditProcedureId	=450000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16010	, AuditProcedureId	=451000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16020	, AuditProcedureId	=451000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16030	, AuditProcedureId	=451000,	AuditProcedureOptionDetailId=1400 },			
						
                    new AuditProcedureOption {ID =	16040	, AuditProcedureId	=452000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16050	, AuditProcedureId	=452000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16060	, AuditProcedureId	=452000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16070	, AuditProcedureId	=453000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16080	, AuditProcedureId	=453000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16090	, AuditProcedureId	=453000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16100	, AuditProcedureId	=454000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16110	, AuditProcedureId	=454000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16120	, AuditProcedureId	=454000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16130	, AuditProcedureId	=455000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16140	, AuditProcedureId	=455000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16150	, AuditProcedureId	=456000,	AuditProcedureOptionDetailId=1400 },			

                    new AuditProcedureOption {ID =	16160	, AuditProcedureId	=456000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16170	, AuditProcedureId	=456000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16180	, AuditProcedureId	=456000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16190	, AuditProcedureId	=457000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16200	, AuditProcedureId	=457000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16210	, AuditProcedureId	=457000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16220	, AuditProcedureId	=458000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16230	, AuditProcedureId	=458000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16240	, AuditProcedureId	=458000,	AuditProcedureOptionDetailId=1400 },			
					
                    new AuditProcedureOption {ID =	16250	, AuditProcedureId	=459000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16260	, AuditProcedureId	=459000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16270	, AuditProcedureId	=459000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	16280	, AuditProcedureId	=459000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	16290	, AuditProcedureId	=460000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16300	, AuditProcedureId	=460000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16310	, AuditProcedureId	=460000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	16320	, AuditProcedureId	=461000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16330	, AuditProcedureId	=461000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16340	, AuditProcedureId	=461000,	AuditProcedureOptionDetailId=2300 }			


                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_6_Item_6_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {

                    new AuditProcedureOption {ID =	16350	, AuditProcedureId	=463000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16360	, AuditProcedureId	=463000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16370	, AuditProcedureId	=463000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	16380	, AuditProcedureId	=464000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16390	, AuditProcedureId	=464000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16400	, AuditProcedureId	=464000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	16410	, AuditProcedureId	=465000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16420	, AuditProcedureId	=465000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16430	, AuditProcedureId	=465000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	16440	, AuditProcedureId	=466000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16450	, AuditProcedureId	=466000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16460	, AuditProcedureId	=466000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16470	, AuditProcedureId	=467000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16480	, AuditProcedureId	=467000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16490	, AuditProcedureId	=467000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	16500	, AuditProcedureId	=469000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16510	, AuditProcedureId	=469000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16520	, AuditProcedureId	=469000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	16525, AuditProcedureId	=470000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16530	, AuditProcedureId	=470000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16540	, AuditProcedureId	=470000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	16550	, AuditProcedureId	=470000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	16560	, AuditProcedureId	=472000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16570	, AuditProcedureId	=472000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16580	, AuditProcedureId	=472000,	AuditProcedureOptionDetailId=2300 },			
							
                    new AuditProcedureOption {ID =	16590, AuditProcedureId	=473000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16600	, AuditProcedureId	=473000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16610	, AuditProcedureId	=473000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16620	, AuditProcedureId	=474000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16630	, AuditProcedureId	=474000,	AuditProcedureOptionDetailId=2200 },			
                    new AuditProcedureOption {ID =	16640	, AuditProcedureId	=474000,	AuditProcedureOptionDetailId=2300 }			


                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_6_Item_7_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {

                    new AuditProcedureOption {ID =	16650	, AuditProcedureId	=477000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16660	, AuditProcedureId	=477000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16670	, AuditProcedureId	=477000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16680	, AuditProcedureId	=478000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16690	, AuditProcedureId	=478000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16700	, AuditProcedureId	=478000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16710	, AuditProcedureId	=479000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16720	, AuditProcedureId	=479000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16730	, AuditProcedureId	=479000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	16750	, AuditProcedureId	=479000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	16760	, AuditProcedureId	=480000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16770	, AuditProcedureId	=480000,	AuditProcedureOptionDetailId=2300 },			
                    new AuditProcedureOption {ID =	16780	, AuditProcedureId	=480000,	AuditProcedureOptionDetailId=2400 },			
							
                    new AuditProcedureOption {ID =	16790	, AuditProcedureId	=481000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16800	, AuditProcedureId	=481000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16810	, AuditProcedureId	=481000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16820	, AuditProcedureId	=482000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16830	, AuditProcedureId	=482000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16840	, AuditProcedureId	=482000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16850	, AuditProcedureId	=484000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16860	, AuditProcedureId	=484000,	AuditProcedureOptionDetailId=2300 },			
                    new AuditProcedureOption {ID =	16870	, AuditProcedureId	=484000,	AuditProcedureOptionDetailId=2400 },			
							
                    new AuditProcedureOption {ID =	16880	, AuditProcedureId	=490000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16890	, AuditProcedureId	=490000,	AuditProcedureOptionDetailId=2300 },			
                    new AuditProcedureOption {ID =	16900	, AuditProcedureId	=490000,	AuditProcedureOptionDetailId=2400 },			
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_6_Item_8_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {

                    new AuditProcedureOption {ID =	16910	, AuditProcedureId	=495000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16920	, AuditProcedureId	=495000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16930	, AuditProcedureId	=495000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	16940	, AuditProcedureId	=499000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16950	, AuditProcedureId	=499000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	16960	, AuditProcedureId	=499000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	16970	, AuditProcedureId	=499000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	16980	, AuditProcedureId	=500000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	16990	, AuditProcedureId	=500000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17000	, AuditProcedureId	=500000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	17010	, AuditProcedureId	=500000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	17020	, AuditProcedureId	=501000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17030	, AuditProcedureId	=501000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17040	, AuditProcedureId	=501000,	AuditProcedureOptionDetailId=200 },			
                    new AuditProcedureOption {ID =	17050	, AuditProcedureId	=501000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17060	, AuditProcedureId	=502000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17070	, AuditProcedureId	=502000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17080	, AuditProcedureId	=502000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	17090	, AuditProcedureId	=502000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	17100	, AuditProcedureId	=503000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17110	, AuditProcedureId	=503000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17120	, AuditProcedureId	=503000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	17130	, AuditProcedureId	=503000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	17140	, AuditProcedureId	=505000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17150	, AuditProcedureId	=505000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17160	, AuditProcedureId	=505000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	17170	, AuditProcedureId	=505000,	AuditProcedureOptionDetailId=200 },			
							
							

                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_6_Item_9_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                {
                    new AuditProcedureOption {ID =	17180	, AuditProcedureId	=506000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17190	, AuditProcedureId	=506000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17200	, AuditProcedureId	=506000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17210	, AuditProcedureId	=507000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17220	, AuditProcedureId	=507000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17230	, AuditProcedureId	=507000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17240	, AuditProcedureId	=508000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17250	, AuditProcedureId	=508000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17260	, AuditProcedureId	=508000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17270	, AuditProcedureId	=509000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17280	, AuditProcedureId	=509000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17290	, AuditProcedureId	=509000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17300	, AuditProcedureId	=510000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17310	, AuditProcedureId	=510000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17320	, AuditProcedureId	=510000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17330	, AuditProcedureId	=511000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17340	, AuditProcedureId	=511000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17350	, AuditProcedureId	=511000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17360	, AuditProcedureId	=512000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17370	, AuditProcedureId	=512000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17380	, AuditProcedureId	=512000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17390	, AuditProcedureId	=495000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17400	, AuditProcedureId	=495000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17410, AuditProcedureId	=495000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17420	, AuditProcedureId	=514000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17430	, AuditProcedureId	=514000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17440	, AuditProcedureId	=514000,	AuditProcedureOptionDetailId=1400 },			
                    new AuditProcedureOption {ID =	17450	, AuditProcedureId	=514000,	AuditProcedureOptionDetailId=200 },			
							
                    new AuditProcedureOption {ID =	17460	, AuditProcedureId	=515000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17470	, AuditProcedureId	=515000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17480	, AuditProcedureId	=515000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17490	, AuditProcedureId	=516000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17500	, AuditProcedureId	=516000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17510	, AuditProcedureId	=516000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17520	, AuditProcedureId	=517000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17530	, AuditProcedureId	=517000,	AuditProcedureOptionDetailId=1300 },			
                    new AuditProcedureOption {ID =	17540	, AuditProcedureId	=517000,	AuditProcedureOptionDetailId=1400 },			
							
                    new AuditProcedureOption {ID =	17550	, AuditProcedureId	=518000,	AuditProcedureOptionDetailId=100 },			
                    new AuditProcedureOption {ID =	17560, AuditProcedureId	=518000,	AuditProcedureOptionDetailId=300 },			
                    new AuditProcedureOption {ID =	17570, AuditProcedureId	=518000,	AuditProcedureOptionDetailId=400 },			
							
                };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        #endregion

        #region "Tab_7"
        private static void AuditProcedureIntoTAB_7(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 46000, AuditProcedureTabId=700, Caption =  "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=1,IsActive=true},
                    new AuditProcedureTabContent {ID = 47000, AuditProcedureTabId=700, Caption =  "Interest", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 48000, AuditProcedureTabId=700, Caption =  "Dividends and Distributions", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                    new AuditProcedureTabContent {ID = 49000, AuditProcedureTabId=700, Caption =  "Property", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=4,IsActive=true},
                    new AuditProcedureTabContent {ID = 50000, AuditProcedureTabId=700, Caption =  "Collectibles (if applicable)", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 51000, AuditProcedureTabId=700, Caption =  "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=7,IsActive=true},
                   
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
            AuditProcedureIntoTAB_7_Item_1(context);
            AuditProcedureIntoTAB_7_Item_2(context);
            AuditProcedureIntoTAB_7_Item_3(context);
            AuditProcedureIntoTAB_7_Item_4(context);
            AuditProcedureIntoTAB_7_Item_5(context);
            AuditProcedureIntoTAB_7_Item_6(context);


            AuditProcedureIntoTAB_7_Item_2_Options(context);
            AuditProcedureIntoTAB_7_Item_3_Options(context);
            AuditProcedureIntoTAB_7_Item_4_Options(context);
            AuditProcedureIntoTAB_7_Item_5_Options(context);
            AuditProcedureIntoTAB_7_Item_6_Options(context);

        }
        private static void AuditProcedureIntoTAB_7_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =520000, AuditProcedureTabContentId =  46000, ParentId=null, Title="To ensure the revenue is:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
	 	                 new AuditProcedure {ID =521000, AuditProcedureTabContentId =  46000, ParentId=520000, Title="a. Have occurred and pertain to the SMSF" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
	 	                 new AuditProcedure {ID =522000, AuditProcedureTabContentId =  46000, ParentId=520000, Title="b. Are completely recorded" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
	 	                 new AuditProcedure {ID =523000, AuditProcedureTabContentId =  46000, ParentId=520000, Title="c. Have been accurately recorded" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
	 	                 new AuditProcedure {ID =524000, AuditProcedureTabContentId =  46000, ParentId=520000, Title="d. Have been recorded in the correct period" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
	 	                 new AuditProcedure {ID =525000, AuditProcedureTabContentId =  46000, ParentId=520000, Title="e. Have been recorded in the proper accounts" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=false},
                };

            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_2(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
            {
                new AuditProcedure { ID = 526000, AuditProcedureTabContentId = 47000, ParentId = null, Title = "Agree a sample of interest received to bank confirmation,  bank statement and/or loan agreements and complete the interest calculator.", Attachments = true, Oml = true, Comments = true, ControlType = Contracts.Common.ControlType.List, AnswerControlType = Contracts.Common.ControlType.Dropdown, Checkbox = true },
                //new AuditProcedures {ID =527000, AuditProcedureTabContentsId =  47000, ParentID=null, Title="Interest Calculator for Bank Account" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=true},
            };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
            {
	             new AuditProcedure {ID =528000, AuditProcedureTabContentId =  48000, ParentId=null, Title="Peruse the list of dividends / distributions received to determine where possible if all dividends / distributions have been recorded as being received." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	             new AuditProcedure {ID =529000, AuditProcedureTabContentId =  48000, ParentId=null, Title="Where income has not been automatically verified, agree to the share registry websites or dividend / distribution notices and bank statements. Refer to samples selected above in the income report." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	             new AuditProcedure {ID =530000, AuditProcedureTabContentId =  48000, ParentId=null, Title="If income is from unlisted (non marketable) investment, Agree dividend / distribution to source documentation and where material consider whether further external confirmation is required. Consider tax implications of the income being classified as special income." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
            };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
            {
	             new AuditProcedure {ID =531000, AuditProcedureTabContentId =  49000, ParentId=null, Title="Obtain lease agreement or annual rental statement and agree rent received to bank statements." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	             new AuditProcedure {ID =532000, AuditProcedureTabContentId =  49000, ParentId=null, Title="If the property is leased to a related party of the fund, ensure that it is business real property.if residential get confirmation that tenant is not a related party. " , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
            	 new AuditProcedure {ID =533000, AuditProcedureTabContentId =  49000, ParentId=null, Title="Complete and review the analytical rental calculator and determine if the rental yield is reasonable and is leased on an arm's length commercial basis ie (3 - 5 % residential, 5 - 10% commercial)." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	            // new AuditProcedures {ID =534000, AuditProcedureTabContentsId =  49000, ParentID=null, Title="Rental Income Yield Calculator" , Attachments=false,OML=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=true},

            };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_5(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
            {
	              new AuditProcedure {ID =535000, AuditProcedureTabContentId =  50000, ParentId=null, Title="Agree rent received to the lease agreement and bank statements to ensure an arms length commercial arrangement is in place." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true}
            };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_6(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
            {
	              new AuditProcedure {ID =536000, AuditProcedureTabContentId =  51000, ParentId=null, Title="We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that revenue is fairly stated." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=true},
            };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }

        #region Option Details
        private static void AuditProcedureIntoTAB_7_Item_2_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                        {
                            new AuditProcedureOption {ID =	17590	, AuditProcedureId =526000, AuditProcedureOptionDetailId=100 },
                            new AuditProcedureOption {ID =	17600	, AuditProcedureId =526000, AuditProcedureOptionDetailId=1300 },
                            new AuditProcedureOption {ID =	17610	, AuditProcedureId =526000, AuditProcedureOptionDetailId=1400},
                        };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_3_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                        {
                            new AuditProcedureOption {ID =	17620, AuditProcedureId =528000, AuditProcedureOptionDetailId=100 },
                            new AuditProcedureOption {ID =	17630, AuditProcedureId =528000, AuditProcedureOptionDetailId=1300 },
                            new AuditProcedureOption {ID =	17640, AuditProcedureId =528000, AuditProcedureOptionDetailId=1400},

                            new AuditProcedureOption {ID =	17650, AuditProcedureId =529000, AuditProcedureOptionDetailId=100 },
                            new AuditProcedureOption {ID =	17660, AuditProcedureId =529000, AuditProcedureOptionDetailId=1300 },
                            new AuditProcedureOption {ID =	17670, AuditProcedureId =529000, AuditProcedureOptionDetailId=1400},

                            new AuditProcedureOption {ID =	17680, AuditProcedureId =530000, AuditProcedureOptionDetailId=100 },
                            new AuditProcedureOption {ID =	17690, AuditProcedureId =530000, AuditProcedureOptionDetailId=1300 },
                            new AuditProcedureOption {ID =	17700, AuditProcedureId =530000, AuditProcedureOptionDetailId=1400}

							
                        };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_4_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                        {
                            new AuditProcedureOption {ID =	17710	, AuditProcedureId =531000, AuditProcedureOptionDetailId=100 },
                            new AuditProcedureOption {ID =	17720	, AuditProcedureId =531000, AuditProcedureOptionDetailId=2200},
                            new AuditProcedureOption {ID =	17730	, AuditProcedureId =531000, AuditProcedureOptionDetailId=2300},

                            new AuditProcedureOption {ID =	17740	, AuditProcedureId =532000, AuditProcedureOptionDetailId=100 },
                            new AuditProcedureOption {ID =	17750	, AuditProcedureId =532000, AuditProcedureOptionDetailId=2200},
                            new AuditProcedureOption {ID =	17760	, AuditProcedureId =532000, AuditProcedureOptionDetailId=2300},

                            new AuditProcedureOption {ID =	17770	, AuditProcedureId =533000, AuditProcedureOptionDetailId=100 },
                            new AuditProcedureOption {ID =	17780	, AuditProcedureId =533000, AuditProcedureOptionDetailId=2200},
                            new AuditProcedureOption {ID =	17790	, AuditProcedureId =533000, AuditProcedureOptionDetailId=2300},
							
                        };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_5_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                    {
                       new AuditProcedureOption {ID =	17800	, AuditProcedureId =535000, AuditProcedureOptionDetailId=100 },
                       new AuditProcedureOption {ID =	17810	, AuditProcedureId =535000, AuditProcedureOptionDetailId=2200},
                       new AuditProcedureOption {ID =	17820	, AuditProcedureId =535000, AuditProcedureOptionDetailId=2300},
                    };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_7_Item_6_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
                    {
                       new AuditProcedureOption {ID =	17830	, AuditProcedureId =536000, AuditProcedureOptionDetailId=100 },
                       new AuditProcedureOption {ID =	17840	, AuditProcedureId =536000, AuditProcedureOptionDetailId=2200},
                       new AuditProcedureOption {ID =	17850	, AuditProcedureId =536000, AuditProcedureOptionDetailId=2300},
                    };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        #endregion

        #endregion

        #region "Tab_8"
        private static void AuditProcedureIntoTAB_8(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
                {
                    new AuditProcedureTabContent {ID = 54000, AuditProcedureTabId=800, Caption = "Audit Objectives", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=1,IsActive=true},
                    new AuditProcedureTabContent {ID = 55000, AuditProcedureTabId=800, Caption = "Contributions", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=2,IsActive=true},
                    new AuditProcedureTabContent {ID = 56000, AuditProcedureTabId=800, Caption = "In species Contributions refer to work done in investment movement report", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                    new AuditProcedureTabContent {ID = 57000, AuditProcedureTabId=800, Caption = "Transfer in", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=4,IsActive=true},
                    new AuditProcedureTabContent {ID = 58000, AuditProcedureTabId=800, Caption = "Transfer out", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=5,IsActive=true},
                    new AuditProcedureTabContent {ID = 59000, AuditProcedureTabId=800, Caption = "Benefits paid", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=6,IsActive=true},
                    new AuditProcedureTabContent {ID = 60000, AuditProcedureTabId=800, Caption = "Other benefits payments", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=7,IsActive=true},
                    new AuditProcedureTabContent {ID = 61000, AuditProcedureTabId=800, Caption = "Contribution Splitting", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=8,IsActive=true},
                    new AuditProcedureTabContent {ID = 62000, AuditProcedureTabId=800, Caption = "Conclusion", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.List, Order=9,IsActive=true},
                };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();

            AuditProcedureIntoTAB_8_Item_1(context);
            AuditProcedureIntoTAB_8_Item_2(context);
            AuditProcedureIntoTAB_8_Item_4(context);
            AuditProcedureIntoTAB_8_Item_5(context);
            AuditProcedureIntoTAB_8_Item_6(context);
            AuditProcedureIntoTAB_8_Item_7(context);
            AuditProcedureIntoTAB_8_Item_8(context);
            AuditProcedureIntoTAB_8_Item_9(context);

            AuditProcedureIntoTAB_8_Item_1_Options(context);
            AuditProcedureIntoTAB_8_Item_2_Options(context);
            AuditProcedureIntoTAB_8_Item_4_Options(context);
            AuditProcedureIntoTAB_8_Item_5_Options(context);
            //AuditProcedureIntoTAB_8_Item_6_Options(context);
            AuditProcedureIntoTAB_8_Item_7_Options(context);
            AuditProcedureIntoTAB_8_Item_8_Options(context);

            AuditProcedureIntoTAB_8_Item_2_Columns(context);


        }
        private static void AuditProcedureIntoTAB_8_Item_1(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =801000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Exists" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =802000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Are Complete" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =803000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Properly valued" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =804000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Ownership is correct" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =805000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Have been classified correctly." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =806000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Properly valued" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},

                    new AuditProcedure {ID =807000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Agree members opening balance to last year members statements." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =808000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Review member's statements to ensure reasonableness of the following:" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =809000, AuditProcedureTabContentId =  54000, ParentId=808000, Title="Share of net result for the year" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =810000, AuditProcedureTabContentId =  54000, ParentId=808000, Title="Contributions tax (15%)" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =811000, AuditProcedureTabContentId =  54000, ParentId=808000, Title="Ensure above amounts are allocated to the correct category of members benefits eg. preserved, restricted and non restricted unpreserved" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =812000, AuditProcedureTabContentId =  54000, ParentId=null, Title="Review address of member as indication of their residence eg. Overseas or residing in rental property owned by the fund" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_8_Item_2(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =813000, AuditProcedureTabContentId =  55000, ParentId=null, Title="Are the employer and member contributions within the contribution limits  as per the following table:" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =814000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Concessional (employer, member deductable)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =815000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Less than 50 years old" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =816000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Between 50 and 65 years old on 30 June" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =817000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Between 65 and 75 years old and the work test is met*" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =818000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Over 75 years old and the work test is met* (ONLY Mandated employer allowed)^" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =819000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Non-concessional (personal/member)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =820000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Less than 65 years old" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =821000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Between 65 and 75 years old and the work test is met*" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =822000, AuditProcedureTabContentId = 55000, ParentId=813000, Title="Over 75" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =823000, AuditProcedureTabContentId =  55000, ParentId=null, Title="* Work Test Definition r being gainfully employed for 40 hours in a continuous period 30 days" , Attachments=true,Oml=false,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =824000, AuditProcedureTabContentId =  55000, ParentId=null, Title="^ This must not include salary sacrifice" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =825000, AuditProcedureTabContentId =  55000, ParentId=null, Title="If contribution cap has been exceeded advice trustees accordingly." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =826000, AuditProcedureTabContentId =  55000, ParentId=null, Title="Ensure all contributions (except in specie contributions) and Rollovers into the fund been deposited into the funds' bank account?" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                    new AuditProcedure {ID =827000, AuditProcedureTabContentId = 55000, ParentId=null, Title="Employer contributions(Concessional)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =828000, AuditProcedureTabContentId = 55000, ParentId=827000, Title="Where practical, obtain confirmation from the employer and agree." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =829000, AuditProcedureTabContentId = 55000, ParentId=827000, Title="Ensure that the SG contributions do not exceed 9% of the member's annual salary or up to the annual limit." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =830000, AuditProcedureTabContentId = 55000, ParentId=null, Title="Member Contributions(concessional and non-concessional)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =831000, AuditProcedureTabContentId = 55000, ParentId=830000, Title="For concessional member contributions, agree amount claimed to the signed copy of the notice of intent to claim a deduction under s290-170 of ITAA." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =832000, AuditProcedureTabContentId = 55000, ParentId=830000, Title="For members under the age of 65, If the non-concessional contributions exceed $150,000 per person ensure that it is within the bring forward cap of $450,000 by checking if the cap was triggered in prior years." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                        new AuditProcedure {ID =833000, AuditProcedureTabContentId = 55000, ParentId=830000, Title="Ensure that the all contributions not complying with reg7.04 are refunded within 30 days of the trustees becoming aware of the breach." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},

                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_8_Item_4(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =834000, AuditProcedureTabContentId =  57000, ParentId=null, Title="Agree transfers in to rollover forms and minutes of meetings and ensure the categories have been correctly classified in the members statements." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =835000, AuditProcedureTabContentId =  57000, ParentId=null, Title="Where material agree amounts to bank statements." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_8_Item_5(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =836000, AuditProcedureTabContentId =  58000, ParentId=null, Title="Agree payment to member rollover form, minutes of meeting or other documentation." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =837000, AuditProcedureTabContentId =  58000, ParentId=null, Title="Ensure rollover complies with the trust deed" , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                    new AuditProcedure {ID =838000, AuditProcedureTabContentId =  58000, ParentId=null, Title="Where the rollover is a result of a family law court order, review such orders to ensure payment is permissible." , Attachments=true,Oml=true,Comments=true,ControlType= Contracts.Common.ControlType.Custom, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_8_Item_6(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>{
             new AuditProcedure {ID =839000, AuditProcedureTabContentId =  59000, ParentId=null, Title="Where a new pension (including rollbacks) has been commenced  or a lump sum payment made during the year:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =840000, AuditProcedureTabContentId =  59000, ParentId=839000, Title="Confirm trust deed permits benefit payment to be mad" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =841000, AuditProcedureTabContentId =  59000, ParentId=839000, Title="Sight commencement / lump sum request documentation and associated minutes of trustees." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =842000, AuditProcedureTabContentId =  59000, ParentId=839000, Title="Has a condition of release been satisfied for the payment to be made (refer table below)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =843000, AuditProcedureTabContentId =  59000, ParentId=839000, Title="Confirm payments to member statements and bank statements" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =844000, AuditProcedureTabContentId =  59000, ParentId=839000, Title="Where member under the age of 60, has appropriate ATO documentation been prepared?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	         new AuditProcedure {ID =845000, AuditProcedureTabContentId =  59000, ParentId=null, Title="For pension accounts, ensure" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =846000, AuditProcedureTabContentId =  59000, ParentId=845000, Title="Complete the pension calculator ensure miminum amounts have been paid" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =847000, AuditProcedureTabContentId =  59000, ParentId=845000, Title="If minimum not met and the shortfall less than 1/12th of the annual payment amount, has the payment been made by the trustees as soon as practical?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =848000, AuditProcedureTabContentId =  59000, ParentId=845000, Title="Where the pension is a Transition to Retirement Pension, ensure  the payment has not exceeded the 10% upper threshold limit" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =849000, AuditProcedureTabContentId =  59000, ParentId=845000, Title="Pension Calculator" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	     new AuditProcedure {ID =850000, AuditProcedureTabContentId =  59000, ParentId=845000, Title="Date Of birth" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
        
            };

            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_7(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>{
                new AuditProcedure {ID =851000, AuditProcedureTabContentId =  60000, ParentId=null, Title="Defined benefit payments" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=true},
	 	        new AuditProcedure {ID =852000, AuditProcedureTabContentId =  60000, ParentId=851000, Title="Ensure an actuarial report is obtained and review the report for reasonableness of assumptions, member details and solvency of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	        new AuditProcedure {ID =853000, AuditProcedureTabContentId =  60000, ParentId=851000, Title="Ensure that the actuarial report does not disclose a deficiency in accrued and vested benefits" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	        new AuditProcedure {ID =854000, AuditProcedureTabContentId =  60000, ParentId=851000, Title="Agree payment amount to actuarial report" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	            new AuditProcedure {ID =855000, AuditProcedureTabContentId =  60000, ParentId=null, Title="Death benefit payments" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=true},
	 	        new AuditProcedure {ID =856000, AuditProcedureTabContentId =  60000, ParentId=855000, Title="Sight death certificate, review trust deed, reviewing binding death benefit nomination if any, minutes of meetings." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	        new AuditProcedure {ID =857000, AuditProcedureTabContentId =  60000, ParentId=855000, Title="Agree payment to payment to bank statement" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	        new AuditProcedure {ID =858000, AuditProcedureTabContentId =  60000, ParentId=855000, Title="Ensure payment is made to authorised beneficiary" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	        new AuditProcedure {ID =859000, AuditProcedureTabContentId =  60000, ParentId=855000, Title="Ensure the member has been removed as a trustee / director of the trustee company of the fund." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	            new AuditProcedure {ID =860000, AuditProcedureTabContentId =  60000, ParentId=null, Title="Disability benefit payments" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=true},
	 	        new AuditProcedure {ID =861000, AuditProcedureTabContentId =  60000, ParentId=860000, Title="Sight at least two appropriate medical certifications confirming the inability of the member to work again either temporarily or permanently." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	        new AuditProcedure {ID =862000, AuditProcedureTabContentId =  60000, ParentId=860000, Title="Where material, trace amounts paid to bank statements" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	 	        new AuditProcedure {ID =863000, AuditProcedureTabContentId =  60000, ParentId=860000, Title="Ensure the PAYG obligations have been correctly calculated and remitted." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
            };

            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_8(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>{
                new AuditProcedure {ID =864000, AuditProcedureTabContentId =  61000, ParentId=null, Title="Ensure that the amount split is a result of a contribution received in the prior year from the splitting spouse and does not exceed 85% of the gross amount and is made in accordance with SIS Reg 6.42 & 6.44(1)" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	            new AuditProcedure {ID =865000, AuditProcedureTabContentId =  61000, ParentId=null, Title="Ensure the contribution has been split been between eligible spouses aged between 55 and 65 and whilst still accumulation stage." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
	            new AuditProcedure {ID =866000, AuditProcedureTabContentId =  61000, ParentId=null, Title="Ensure that the trust deed allows contribution splitting." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=true},
            };

            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_9(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>{
                 new AuditProcedure {ID =867000, AuditProcedureTabContentId =  62000, ParentId=null, Title="We have obtained sufficient appropriate evidence to form an opinion on the above objectives and it appears that the investment balances are fairly stated." , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.List, AnswerControlType=null,Checkbox=true},
            };

            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_1_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
            {
                new AuditProcedureOption {ID =	17860	, AuditProcedureId =807000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	17870	, AuditProcedureId =807000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	17880	, AuditProcedureId =807000, AuditProcedureOptionDetailId=1400 },						

                new AuditProcedureOption {ID =	17890	, AuditProcedureId =809000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	17900	, AuditProcedureId =809000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	17910	, AuditProcedureId =809000, AuditProcedureOptionDetailId=1400 },						

                new AuditProcedureOption {ID =	17920	, AuditProcedureId =810000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	17930	, AuditProcedureId =810000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	17940	, AuditProcedureId =810000, AuditProcedureOptionDetailId=1400 },						

                new AuditProcedureOption {ID =	17950	, AuditProcedureId =811000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	17960	, AuditProcedureId =811000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	17970, AuditProcedureId =811000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	17975	, AuditProcedureId =812000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	17976	, AuditProcedureId =812000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	17977, AuditProcedureId =812000, AuditProcedureOptionDetailId=1400 },

                
            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_2_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
            {
                new AuditProcedureOption {ID =	17980	, AuditProcedureId =813000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	17990	, AuditProcedureId =813000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	18000	, AuditProcedureId =813000, AuditProcedureOptionDetailId=1400 },						
                new AuditProcedureOption {ID =	18010	, AuditProcedureId =813000, AuditProcedureOptionDetailId=200 },						

                new AuditProcedureOption {ID =	18020	, AuditProcedureId =825000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18030	, AuditProcedureId =825000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18040	, AuditProcedureId =825000, AuditProcedureOptionDetailId=2300 },						

                new AuditProcedureOption {ID =	18050	, AuditProcedureId =826000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18060	, AuditProcedureId =826000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18070	, AuditProcedureId =826000, AuditProcedureOptionDetailId=2300 },		
                new AuditProcedureOption {ID =	18080, AuditProcedureId =826000, AuditProcedureOptionDetailId=2301 },	
	
                new AuditProcedureOption {ID =	18090	, AuditProcedureId =828000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18100	, AuditProcedureId =828000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	18110	, AuditProcedureId =828000, AuditProcedureOptionDetailId=1400 },		
                new AuditProcedureOption {ID =	18120	, AuditProcedureId =828000, AuditProcedureOptionDetailId=200 },	
                
                new AuditProcedureOption {ID =	18130	, AuditProcedureId =829000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18140	, AuditProcedureId =829000, AuditProcedureOptionDetailId=1300 },						
                new AuditProcedureOption {ID =	18150	, AuditProcedureId =829000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18160	, AuditProcedureId =831000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18170	, AuditProcedureId =831000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18180	, AuditProcedureId =831000, AuditProcedureOptionDetailId=2300 },		
                new AuditProcedureOption {ID =	18190	, AuditProcedureId =831000, AuditProcedureOptionDetailId=2301 },

                new AuditProcedureOption {ID =	18200	, AuditProcedureId =832000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18210	, AuditProcedureId =832000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18220	, AuditProcedureId =832000, AuditProcedureOptionDetailId=2300 },		
                new AuditProcedureOption {ID =	18230	, AuditProcedureId =832000, AuditProcedureOptionDetailId=2301 },

                
                new AuditProcedureOption {ID =	18232	, AuditProcedureId =833000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18234	, AuditProcedureId =833000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18236	, AuditProcedureId =833000, AuditProcedureOptionDetailId=2300 },		
                new AuditProcedureOption {ID =	18238	, AuditProcedureId =833000, AuditProcedureOptionDetailId=2301 },

            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_4_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
            {
                new AuditProcedureOption {ID =	18240	, AuditProcedureId =834000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18250	, AuditProcedureId =834000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18260	, AuditProcedureId =834000, AuditProcedureOptionDetailId=2300},

                new AuditProcedureOption {ID =	18270	, AuditProcedureId =835000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18280	, AuditProcedureId =835000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18290, AuditProcedureId =835000, AuditProcedureOptionDetailId=2300 },
            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_5_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
            {
                new AuditProcedureOption {ID =	18310, AuditProcedureId =836000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18320, AuditProcedureId =836000, AuditProcedureOptionDetailId=2200 },						
                new AuditProcedureOption {ID =	18330, AuditProcedureId =836000, AuditProcedureOptionDetailId=2300 },

                new AuditProcedureOption {ID =	18340, AuditProcedureId =837000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18350, AuditProcedureId =837000, AuditProcedureOptionDetailId=2200},						
                new AuditProcedureOption {ID =	18360, AuditProcedureId =837000, AuditProcedureOptionDetailId=2300 },

                new AuditProcedureOption {ID =	18370, AuditProcedureId =838000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	18380, AuditProcedureId =838000, AuditProcedureOptionDetailId=2200},						
                new AuditProcedureOption {ID =	18390, AuditProcedureId =838000, AuditProcedureOptionDetailId=2300 },
            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_6_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>{
                new AuditProcedureOption {ID =	18400	, AuditProcedureId =73040, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18410	, AuditProcedureId =73040, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18420	, AuditProcedureId =73040, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18430	, AuditProcedureId =79040, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18440	, AuditProcedureId =79040, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18450	, AuditProcedureId =79040, AuditProcedureOptionDetailId=1400 },
            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_7_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>{

                new AuditProcedureOption {ID =	18460	, AuditProcedureId =852000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18470	, AuditProcedureId =852000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18480	, AuditProcedureId =852000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18490	, AuditProcedureId =853000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18500	, AuditProcedureId =853000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18510	, AuditProcedureId =853000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18520	, AuditProcedureId =854000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18530	, AuditProcedureId =854000, AuditProcedureOptionDetailId=2200 },
                new AuditProcedureOption {ID =	18540	, AuditProcedureId =854000, AuditProcedureOptionDetailId=2300 },

                new AuditProcedureOption {ID =	18550	, AuditProcedureId =856000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18560	, AuditProcedureId =856000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18570	, AuditProcedureId =856000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18580	, AuditProcedureId =857000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18590	, AuditProcedureId =857000, AuditProcedureOptionDetailId=2200 },
                new AuditProcedureOption {ID =	18600	, AuditProcedureId =857000, AuditProcedureOptionDetailId=2300 },

                new AuditProcedureOption {ID =	18610	, AuditProcedureId =858000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18620	, AuditProcedureId =858000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18630	, AuditProcedureId =858000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18640	, AuditProcedureId =859000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18650	, AuditProcedureId =859000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18660	, AuditProcedureId =859000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18670	, AuditProcedureId =861000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18680	, AuditProcedureId =861000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18690	, AuditProcedureId =861000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18700	, AuditProcedureId =862000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18710	, AuditProcedureId =862000, AuditProcedureOptionDetailId=2200 },
                new AuditProcedureOption {ID =	18720	, AuditProcedureId =862000, AuditProcedureOptionDetailId=2300 },

                new AuditProcedureOption {ID =	18730	, AuditProcedureId =863000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18740	, AuditProcedureId =863000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18750	, AuditProcedureId =863000, AuditProcedureOptionDetailId=1400 },

            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_8_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>{
                new AuditProcedureOption {ID =	18760	, AuditProcedureId =864000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18770	, AuditProcedureId =864000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18780	, AuditProcedureId =864000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18790	, AuditProcedureId =865000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18800	, AuditProcedureId =865000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18810	, AuditProcedureId =865000, AuditProcedureOptionDetailId=1400 },

                new AuditProcedureOption {ID =	18820	, AuditProcedureId =866000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	18830	, AuditProcedureId =866000, AuditProcedureOptionDetailId=1300 },
                new AuditProcedureOption {ID =	18840	, AuditProcedureId =866000, AuditProcedureOptionDetailId=1400 },
            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_8_Item_2_Columns(SMSFContext context)
        {
            var AuditProcedureColumn = new List<AuditProcedureColumn>
                {
                    new AuditProcedureColumn {ID =60010,AuditProcedureId =814000, ColumnName="2010", ColumnValue=""},
                    new AuditProcedureColumn {ID =60020,AuditProcedureId =814000, ColumnName="2011", ColumnValue=""},
                    new AuditProcedureColumn {ID =60030,AuditProcedureId =814000, ColumnName="2012", ColumnValue=""},
                    new AuditProcedureColumn {ID =60040,AuditProcedureId =814000, ColumnName="2013", ColumnValue=""},

                    new AuditProcedureColumn {ID =60100,AuditProcedureId =815000, ColumnName="2010", ColumnValue="$25,000.00"},
                    new AuditProcedureColumn {ID =60110,AuditProcedureId =815000, ColumnName="2011", ColumnValue="$25,000.00"},
                    new AuditProcedureColumn {ID =60120,AuditProcedureId =815000, ColumnName="2012", ColumnValue="$25,000.00"},
                    new AuditProcedureColumn {ID =60130,AuditProcedureId =815000, ColumnName="2013", ColumnValue="$25,000.00"},

                    new AuditProcedureColumn {ID =60140,AuditProcedureId =816000, ColumnName="2010", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60150,AuditProcedureId =816000, ColumnName="2011", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60160,AuditProcedureId =816000, ColumnName="2012", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60170,AuditProcedureId =816000, ColumnName="2013", ColumnValue="$25,000.00"},
                
                    new AuditProcedureColumn {ID =60180,AuditProcedureId =817000, ColumnName="2010", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60190,AuditProcedureId =817000, ColumnName="2011", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60200,AuditProcedureId =817000, ColumnName="2012", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60210,AuditProcedureId =817000, ColumnName="2013", ColumnValue="$25,000.00"},

                    new AuditProcedureColumn {ID =60220,AuditProcedureId =818000, ColumnName="2010", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60230,AuditProcedureId =818000, ColumnName="2011", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60240,AuditProcedureId =818000, ColumnName="2012", ColumnValue="$50,000.00"},
                    new AuditProcedureColumn {ID =60250,AuditProcedureId =818000, ColumnName="2013", ColumnValue="$25,000.00"},

                    new AuditProcedureColumn {ID =60260,AuditProcedureId =820000, ColumnName="2010", ColumnValue="$150,000.00"},
                    new AuditProcedureColumn {ID =60270,AuditProcedureId =820000, ColumnName="2011", ColumnValue="$150,000.00"},
                    new AuditProcedureColumn {ID =60280,AuditProcedureId =820000, ColumnName="2012", ColumnValue="$150,000.00"},
                    new AuditProcedureColumn {ID =60290,AuditProcedureId =820000, ColumnName="2013", ColumnValue="$150,000.00"},

                    new AuditProcedureColumn {ID =60300,AuditProcedureId =821000, ColumnName="2010", ColumnValue="$150,000.00"},
                    new AuditProcedureColumn {ID =60310,AuditProcedureId =821000, ColumnName="2011", ColumnValue="$150,000.00"},
                    new AuditProcedureColumn {ID =60320,AuditProcedureId =821000, ColumnName="2012", ColumnValue="$150,000.00"},
                    new AuditProcedureColumn {ID =60330,AuditProcedureId =821000, ColumnName="2013", ColumnValue="$150,000.00"},

                    new AuditProcedureColumn {ID =60340,AuditProcedureId =822000, ColumnName="2010", ColumnValue="$0.00"},
                    new AuditProcedureColumn {ID =60350,AuditProcedureId =822000, ColumnName="2011", ColumnValue="$0.00"},
                    new AuditProcedureColumn {ID =60360,AuditProcedureId =822000, ColumnName="2012", ColumnValue="$0.00"},
                    new AuditProcedureColumn {ID =60370,AuditProcedureId =822000, ColumnName="2013", ColumnValue="$0.00"},




                
                };
            context.AuditProcedureColumn.AddRange(AuditProcedureColumn);
            context.SaveChanges();

        }

        #endregion

        #region "Tab_9"
        private static void AuditProcedureIntoTAB_9(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
            {
                new AuditProcedureTabContent {ID = 65000, AuditProcedureTabId=900, Caption =  "Manager Review", ContentType= Contracts.Common.WorkpaperContentType.ManagerReview, ControlType= Contracts.Common.ControlType.ManagerReview, Order=1,IsActive=true},
            };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
        }

        #endregion

        #region "Tab_10"
        private static void AuditProcedureIntoTAB_10(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
            {
                new AuditProcedureTabContent {ID = 75000, AuditProcedureTabId=1000, Caption =  "Test Summary", ContentType= Contracts.Common.WorkpaperContentType.TestSummary, ControlType= Contracts.Common.ControlType.TestSummary, Order=1,IsActive=true},
                //new AuditProcedureTabContents {ID = 76000, AuditProcedureTabsID=1000, Caption =  "Review", ContentType= Contracts.Common.WorkpaperContentType.Report, ControlType= Contracts.Common.ControlType.Custom, Order=1,IsActive=true},
                new AuditProcedureTabContent {ID = 77000, AuditProcedureTabId=1000, Caption =  "General Question", ContentType= Contracts.Common.WorkpaperContentType.AuditProcedure, ControlType= Contracts.Common.ControlType.Grid, Order=3,IsActive=true},
                //new AuditProcedureTabContents {ID = 78000, AuditProcedureTabsID=1000, Caption =  "Event Summary", ContentType= Contracts.Common.WorkpaperContentType.ManagerReview, ControlType= Contracts.Common.ControlType.Custom, Order=1,IsActive=true},
                //new AuditProcedureTabContents {ID = 79000, AuditProcedureTabsID=1000, Caption =  "Mandatory Question", ContentType= Contracts.Common.WorkpaperContentType.ManagerReview, ControlType= Contracts.Common.ControlType.Custom, Order=1,IsActive=true},
            };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();

            AuditProcedureIntoTAB_10_Item_1(context);
            AuditProcedureIntoTAB_10_Item_1_TestSummaryComments(context);
            AuditProcedureIntoTAB_10_Item_3(context);
            AuditProcedureIntoTAB_10_Item_3_Options(context);

        }
        private static void AuditProcedureIntoTAB_10_Item_1(SMSFContext context)
        {
            var testSummary = new List<TestSummary>
            {
                //Infact are not AP
                 new TestSummary { ID=1000000, TestName="Test 1",Description= "Did the fund fail to meet the defination of an SMSF?" }, 
                 new TestSummary { ID=1001000, TestName="Test 2",Description= "As at the end of the financial year, is the SMSF less than 15 months old and did the value of any single contravention exceed $2,000?"},
                 new TestSummary { ID=1002000, TestName="Test 3",Description= "Hav the trustees previously received advice of a ccontravention that they breached again?"} ,
                 new TestSummary { ID=1003000, TestName="Test 4",Description= "Is there an identified contravention from a previous year that has not been rectified at the time the audit is being conducted?"} ,
                 new TestSummary { ID=1004000, TestName="Test 5",Description= "Dis the trustees fail to meet a statutory time period prescribed in the SISA and SISR by more than 14 days?"} ,
                 new TestSummary { ID=1005000, TestName="Test 6",Description= "Was the total value of all contraventions greater than 5% of the total value of the fund's assets?"} ,
                 new TestSummary { ID=1006000, TestName="Test 7",Description= "Was the total of all contraventions greater than $30,000?"} ,
                 new TestSummary { ID=1007000, TestName="Test 8",Description= "Is there a reportable contravention?"} ,
            };
            context.TestSummary.AddRange(testSummary);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_10_Item_1_TestSummaryComments(SMSFContext context)
        {
            var auditProcedureTestSummaryComments = new List<TestSummaryComment>
            {
                //For Test 1
                new TestSummaryComment  {ID =1000, TestSummaryId=1000000, TestComments ="Go To Test 2" },
                new TestSummaryComment  {ID =2000, TestSummaryId=1000000, TestComments ="REPORT THIS EVENT - Section 17A - SMSF Defination in section E then proceed to Test 2"},
                //For Test 2
                new TestSummaryComment  {ID =3000, TestSummaryId=1001000, TestComments ="Go To Test 3" },
                new TestSummaryComment  {ID =4000, TestSummaryId=1001000, TestComments ="Report all identified contraventions of the sections and regulations listed in Table 1, regardless of the financial thresholds. Do not apply further test"},
                //For Test 3
                new TestSummaryComment  {ID =5000, TestSummaryId=1002000, TestComments ="Go To Test 4"},
                new TestSummaryComment  {ID =6000, TestSummaryId=1002000, TestComments ="Test not required"},
                new TestSummaryComment  {ID =7000, TestSummaryId=1002000, TestComments ="Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 4"},
                //For Test 4
                new TestSummaryComment  {ID =8000, TestSummaryId=1003000, TestComments ="Go To Test 5"},
                new TestSummaryComment  {ID =9000, TestSummaryId=1003000, TestComments ="Test not required" },
                new TestSummaryComment  {ID =10000, TestSummaryId=1003000, TestComments ="Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 5" },
                //For Test 5
                new TestSummaryComment  {ID =11000, TestSummaryId=1004000, TestComments ="Go To Test 6" },
                new TestSummaryComment  {ID =12000, TestSummaryId=1004000, TestComments ="Test not required" },
                new TestSummaryComment  {ID =13000, TestSummaryId=1004000, TestComments ="Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 6" },
                //For Test 6
                new TestSummaryComment  {ID =14000, TestSummaryId=1005000, TestComments ="Go To Test 7" },
                new TestSummaryComment  {ID =15000, TestSummaryId=1005000, TestComments ="Test not required" },
                new TestSummaryComment  {ID =16000, TestSummaryId=1005000, TestComments ="Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To Test 7" },
                //For Test 7
                new TestSummaryComment  {ID =17000, TestSummaryId=1006000, TestComments ="Go To Test 8" },
                new TestSummaryComment  {ID =18000, TestSummaryId=1006000, TestComments ="Test not required" },
                new TestSummaryComment  {ID =19000, TestSummaryId=1006000, TestComments ="Report identified contraventions of the sections and regulations listed in Table 1, that meet this test. Go To test. Report Additional information in accordance with the Auditing and Assurance Standards and your professional judgement." },
                new TestSummaryComment  {ID =20000, TestSummaryId=1006000, TestComments ="Report Additional information in accordance with the Auditing and Assurance Standards and your professional judgement." },
                //For Test 8
                new TestSummaryComment  {ID =21000, TestSummaryId=1007000, TestComments ="Report any other additional information in accordance with the Auditing and Assurance Standards and your professional judgement." },
                new TestSummaryComment  {ID =22000, TestSummaryId=1007000, TestComments ="Complete Contravention Report below for manager/partner review" },
                
            };

            context.TestSummaryComment.AddRange(auditProcedureTestSummaryComments);
            context.SaveChanges();
        }
        private static void AuditProcedureIntoTAB_10_Item_3(SMSFContext context)
        {
            var auditProcedures = new List<AuditProcedure>
                {
                    new AuditProcedure {ID =1008000, AuditProcedureTabContentId =  77000, ParentId=null, Title="Fund Structure:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =1009000, AuditProcedureTabContentId =  77000, ParentId=1008000, Title="Where there any issue/s or contravention/s in relation to:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =1010000, AuditProcedureTabContentId =  77000, ParentId=1009000, Title="s17A - SMSF defination, or" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1011000, AuditProcedureTabContentId =  77000, ParentId=1009000, Title="s126K - disqualified person/s?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =1012000, AuditProcedureTabContentId =  77000, ParentId=null, Title="Administration:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =1013000, AuditProcedureTabContentId =  77000, ParentId=1012000, Title="Where there any contravention/s in relation to:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =1014000, AuditProcedureTabContentId =  77000, ParentId=1013000, Title="s103 - minutes and records" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1015000, AuditProcedureTabContentId =  77000, ParentId=1013000, Title="s104A - trustee declaration, or," , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1016000, AuditProcedureTabContentId =  77000, ParentId=1013000, Title="s35C(2) - documents requested by the auditor" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =1017000, AuditProcedureTabContentId =  77000, ParentId=null, Title="Contributions:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =1018000, AuditProcedureTabContentId =  77000, ParentId=1017000, Title="Where there any contravention/s in relation to:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =1019000, AuditProcedureTabContentId =  77000, ParentId=1018000, Title="r7.04 - the contribution standards?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =1020000, AuditProcedureTabContentId =  77000, ParentId=null, Title="Investments:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =1021000, AuditProcedureTabContentId =  77000, ParentId=1020000, Title="Where there any contravention/s in relation to:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =1022000, AuditProcedureTabContentId =  77000, ParentId=1021000, Title="s65 - lending" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1023000, AuditProcedureTabContentId =  77000, ParentId=1021000, Title="s82-s85 - in-house assets," , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1024000, AuditProcedureTabContentId =  77000, ParentId=1021000, Title="s67 - borrowing" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1025000, AuditProcedureTabContentId =  77000, ParentId=1021000, Title="s109 - Investments to be on an arm's length basis" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1026000, AuditProcedureTabContentId =  77000, ParentId=1021000, Title="r13.14 - charges over assets," , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1027000, AuditProcedureTabContentId =  77000, ParentId=1021000, Title="r4.09 - investments strategy, or," , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1028000, AuditProcedureTabContentId =  77000, ParentId=1021000, Title="s52(2)(d) - seperation of assets" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =1029000, AuditProcedureTabContentId =  77000, ParentId=null, Title="Benefit Payments:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =1030000, AuditProcedureTabContentId =  77000, ParentId=1029000, Title="Where there any contravention/s in relation to:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =1031000, AuditProcedureTabContentId =  77000, ParentId=1030000, Title="r6.17 - benefit payment standards, or" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1032000, AuditProcedureTabContentId =  77000, ParentId=1030000, Title="r5.08 - maintaining minimum benefits?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    new AuditProcedure {ID =1033000, AuditProcedureTabContentId =  77000, ParentId=null, Title="Sole purpose:" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                        new AuditProcedure {ID =1034000, AuditProcedureTabContentId =  77000, ParentId=1033000, Title="has the fund contravened the sole purpose test under S62?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=Contracts.Common.ControlType.Dropdown,Checkbox=false},
                            new AuditProcedure {ID =1035000, AuditProcedureTabContentId =  77000, ParentId=1034000, Title="r6.17 - benefit payment standards, or" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                            new AuditProcedure {ID =1036000, AuditProcedureTabContentId =  77000, ParentId=1034000, Title="r5.08 - maintaining minimum benefits?" , Attachments=false,Oml=false,Comments=false,ControlType= Contracts.Common.ControlType.Grid, AnswerControlType=null,Checkbox=false},
                    
                };
            context.AuditProcedure.AddRange(auditProcedures);
            context.SaveChanges();

        }
        private static void AuditProcedureIntoTAB_10_Item_3_Options(SMSFContext context)
        {
            var AuditProcedureOption = new List<AuditProcedureOption>
            {
                new AuditProcedureOption {ID =	22000, AuditProcedureId =1009000, AuditProcedureOptionDetailId=100 },						
                new AuditProcedureOption {ID =	22010, AuditProcedureId =1009000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID =	22020, AuditProcedureId =1009000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID =	22030, AuditProcedureId =1013000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	22040, AuditProcedureId =1013000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID =	22050, AuditProcedureId =1013000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID =	22060, AuditProcedureId =1018000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	22070, AuditProcedureId =1018000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID =	22080, AuditProcedureId =1018000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID =	22090, AuditProcedureId =1021000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	22100, AuditProcedureId =1021000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID =	22110, AuditProcedureId =1021000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID =	22120, AuditProcedureId =1030000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	22130, AuditProcedureId =1030000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID =	22140, AuditProcedureId =1030000, AuditProcedureOptionDetailId=400 },

                new AuditProcedureOption {ID =	22150, AuditProcedureId =1034000, AuditProcedureOptionDetailId=100 },
                new AuditProcedureOption {ID =	22160, AuditProcedureId =1034000, AuditProcedureOptionDetailId=300 },
                new AuditProcedureOption {ID =	22170, AuditProcedureId =1034000, AuditProcedureOptionDetailId=400 },

            };

            context.AuditProcedureOption.AddRange(AuditProcedureOption);
            context.SaveChanges();
        }

        #endregion

        #region "Tab_11"
        private static void AuditProcedureIntoTAB_11(SMSFContext context)
        {
            var auditProcedureTabContents = new List<AuditProcedureTabContent>
            {
                new AuditProcedureTabContent {ID = 82000, AuditProcedureTabId=1100, Caption =  "Initial Audit Quries", ContentType= Contracts.Common.WorkpaperContentType.InitialAuditQuries, ControlType= Contracts.Common.ControlType.InitialAuditQuries, Order=1,IsActive=true},
            };

            context.AuditProcedureTabContent.AddRange(auditProcedureTabContents);
            context.SaveChanges();
        }

        #endregion
    }
}