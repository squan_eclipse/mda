﻿using DBASystem.SMSF.Data.Models;
using System;
using System.Linq;

namespace DBASystem.SMSF.Data
{
    public interface IRepository<T> where T : BaseEntity
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        bool Any(Func<T, bool> where);
    }
}
