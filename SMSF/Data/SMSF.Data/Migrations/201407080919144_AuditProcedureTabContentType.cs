namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuditProcedureTabContentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AuditProcedureTab", "ContentType", c => c.Int(nullable: true));
            Sql("Update dbo.AuditProcedureTab Set ContentType = 2 Where ID = 50");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 100");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 200");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 300");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 400");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 500");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 600");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 700");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 1 Where ID = 800");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 4 Where ID = 900");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 3 Where ID = 1000");
            Sql("Update dbo.AuditProcedureTab Set ContentType = 3 Where ID = 1100");
            AlterColumn("dbo.AuditProcedureTab", "ContentType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AuditProcedureTab", "ContentType");
        }
    }
}
