using System.Linq;
using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data.Models;

namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<SMSFContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
            ContextKey = "DBASystem.SMSF.Data.SMSFContext";
        }

        protected override void Seed(SMSFContext context)
        {
            AddOptionTypes(context);
            AddOptions(context);
            AddGroups(context);
            AddRoles(context);
            AddRoleOptions(context);
            AddWorkFlowStatus(context);
            AddTemplateProvider(context);
            AddTemplateCategories(context);
            AddTemplates(context);
        }

        private void AddOptionTypes(SMSFContext context)
        {
            var types = new[]
            {
                new OptionTypes { ID = 1, Description = "Page", StatusID = Status.Active, Title = "Page" },
                new OptionTypes { ID = 2, Description = "Menu", StatusID = Status.Active, Title = "Main" },
                new OptionTypes { ID = 3, Description = "NextPage", StatusID = Status.Active, Title = "Main" },
                new OptionTypes { ID = 4, Description = "Report", StatusID = Status.Active, Title = "Main" },
                new OptionTypes { ID = 5, Description = "View", StatusID = Status.Active, Title = "Read Only Access" },
                new OptionTypes { ID = 6, Description = "Modify", StatusID = Status.Active, Title = "Edit Update" },
                new OptionTypes { ID = 7, Description = "New/Add", StatusID = Status.Active, Title = "Add Update" },
                new OptionTypes { ID = 8, Description = "Remove", StatusID = Status.Active, Title = "Delete Update" },
                new OptionTypes { ID = 9, Description = "Cancel", StatusID = Status.Active, Title = "Cancel" },
                new OptionTypes { ID = 10, Description = "Print", StatusID = Status.Active, Title = "Print" },
                new OptionTypes { ID = 11, Description = "Search", StatusID = Status.Active, Title = "Search" },
                new OptionTypes { ID = 12, Description = "Maker", StatusID = Status.Active, Title = "Maker" },
                new OptionTypes { ID = 13, Description = "Checker", StatusID = Status.Active, Title = "Checker" },
                new OptionTypes { ID = 14, Description = "CustomAction", StatusID = Status.Active, Title = "CustomAction" }
            };

            context.OptionTypes.AddOrUpdate(types);
        }

        private void AddOptions(SMSFContext context)
        {
            /* 
                Important:  when adding new option here, please use static values i.e. new DateTime(2014, 07, 01)
                            In CreateOn property, add current date with static values.
            */
            var options = new[]
            {
                new Options {ID = 100, Title = "Login", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 101, Title = "Login", PageURL = "LoginPage", NextPageURL = "Home", TypeID = 1, ParentID = 100, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 102, Title = "LogOff", PageURL = "LogOffPage", NextPageURL = "LoginPage", TypeID = 1, ParentID = 100, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 103, Title = "Update Last Logi", PageURL = "LoginPage", NextPageURL = "Home", TypeID = 1, ParentID = 100, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 200, Title = "Anonymous Actions", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 201, Title = "Allowed Pages", PageURL = "AllowedPages", NextPageURL = "AllowedPages", TypeID = 14, ParentID = 200, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 202, Title = "Allowed Actions", PageURL = "AllowedActions",NextPageURL =  "AllowedActions", TypeID = 14, ParentID = 200, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 203, Title = "Get User Profile", PageURL = "GetUserProfile", NextPageURL = "GetUserProfile", TypeID = 1, ParentID = 200, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 204, Title = "Update User Profile", PageURL = "UpdateUserProfile", NextPageURL = "UpdateUserProfile", TypeID = 1, ParentID = 200, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 205, Title = "Update Entity Address", PageURL = "UpdateEntityAddress", NextPageURL = "UpdateEntityAddress", TypeID = 1, ParentID = 200, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 300, Title = "Address", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 301, Title = "Get Address Types", PageURL = "ListAddressTypes", NextPageURL = "ListAddressTypes", TypeID = 1, ParentID = 300, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 302, Title = "Get Countries", PageURL = "ListCountries", NextPageURL = "ListCountries", TypeID = 1, ParentID = 300, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 303, Title = "Get States", PageURL = "ListStates", NextPageURL = "ListStates", TypeID = 1, ParentID = 300, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 400, Title = "Telephone", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 401, Title = "Get Telephone Types", PageURL = "GetTelephoneTypes", NextPageURL = "GetTelephoneTypes", TypeID = 1, ParentID = 400, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 402, Title = "Add Telephone", PageURL = "AddTelephone", NextPageURL = "AddTelephone", TypeID = 1, ParentID = 400, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 403, Title = "Update Telephone", PageURL = "UpdateTelephone", NextPageURL = "UpdateTelephone", TypeID = 1, ParentID = 400, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 404, Title = "Delete Telephone", PageURL = "DeleteTelephone", NextPageURL = "DeleteTelephone", TypeID = 1, ParentID = 400, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 405, Title = "Get Country Code", PageURL = "GetCountryCode", NextPageURL = "GetCountryCode", TypeID = 1, ParentID = 400, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 500, Title = "User", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 501, Title = "Add Account Manager", PageURL = "CreateAccountManager", NextPageURL = "CreateAccountManager", TypeID = 1,ParentID = 500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 502, Title = "Delete Account Manager", PageURL = "DeleteAccountManager", NextPageURL = "DeleteAccountManager", TypeID = 1,ParentID = 500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 503, Title = "Update Account Manager", PageURL = "UpdateAccountManager", NextPageURL = "UpdateAccountManager", TypeID = 1,ParentID = 500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 504, Title = "Get Account Manager", PageURL = "GetAccountManager", NextPageURL = "GetAccountManager", TypeID = 1,ParentID = 500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 505, Title = "Validate Email", PageURL = "ValidateEmail", NextPageURL = "ValidateEmail", TypeID = 1,ParentID = 500, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 506, Title = "Get Account Managers", PageURL = "GetAccountManagers", NextPageURL = "GetAccountManagers", TypeID = 1,ParentID = 500, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 507, Title = "Has Multiple User Access", PageURL = "HasMultipleUserAccess", NextPageURL = "HasMultipleUserAccess", TypeID = 1,ParentID = 500, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 508, Title = "Get Firm Account Manager", PageURL = "GetFirmAccountManager", NextPageURL = "GetFirmAccountManager", TypeID = 1,ParentID = 500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 509, Title = "Get Account Manager Detail", PageURL = "GetAccountManagerDetail", NextPageURL = "GetAccountManagerDetail", TypeID = 1,ParentID = 500, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 510, Title = "Unlock User", PageURL = "UnlockUser", NextPageURL = "UnlockUser", TypeID = 1,ParentID = 500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 600, Title = "Rights Management", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 601, Title = "Add User Group", PageURL = "CreateUserGroup", NextPageURL = "CreateUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 602, Title = "Update User Group", PageURL = "UpdateUserGroup", NextPageURL = "UpdateUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 603, Title = "Change User Group Status", PageURL = "ChangeUserGroupStatus", NextPageURL = "ChangeUserGroupStatus", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 604, Title = "Get User Groups", PageURL = "GetUserGroups", NextPageURL = "GetUserGroups", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 605, Title = "Get User Group", PageURL = "GetUserGroup", NextPageURL = "GetUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 606, Title = "Get Roles", PageURL = "GetUserGroup", NextPageURL = "GetUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 607, Title = "Get Role", PageURL = "GetUserGroup", NextPageURL = "GetUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 608, Title = "Create Role", PageURL = "GetUserGroup", NextPageURL = "GetUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 609, Title = "Update Role", PageURL = "GetUserGroup", NextPageURL = "GetUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 610, Title = "Change Role Status", PageURL = "GetUserGroup", NextPageURL = "GetUserGroup", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 611, Title = "Get User Group Roles", PageURL = "GetUserGroupRoles", NextPageURL = "GetUserGroupRoles", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 612, Title = "Add User Group Roles", PageURL = "AddUserGroupRoles", NextPageURL = "AddUserGroupRoles", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 613, Title = "Get User Role Options", PageURL = "GetUserRoleOptions", NextPageURL = "GetUserRoleOptions", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 614, Title = "Add User Role Options", PageURL = "AddUserRoleOptions", NextPageURL = "AddUserRoleOptions", TypeID = 1,ParentID = 600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 615, Title = "Get Uer Associated Entites", PageURL = "GetUerAssociatedEntites", NextPageURL = "GetUerAssociatedEntites", TypeID = 1,ParentID = 600, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 616, Title = "Send Forgot Password Email", PageURL = "SendForgotPassEmail", NextPageURL = "SendForgotPassEmail", TypeID = 1,ParentID = 600, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 617, Title = "Reset Password", PageURL = "ResetPassword", NextPageURL = "ResetPassword", TypeID = 1,ParentID = 600, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 700, Title = "Job", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 701, Title = "Get Job Manager", PageURL = "GetJobManagers", NextPageURL = "GetJobManagers", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 702, Title = "Get Scheduled Jobs", PageURL = "GetScheduledJobs", NextPageURL = "GetScheduledJobs", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 10)},
                new Options {ID = 703, Title = "Get Completed Jobs", PageURL = "GetCompletedJobs", NextPageURL = "GetCompletedJobs", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 10)},
                new Options {ID = 704, Title = "Add Job", PageURL = "AddJob", NextPageURL = "AddJob", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 705, Title = "Get Job Detail", PageURL = "GetJobDetail",NextPageURL =  "GetJobDetail", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 706, Title = "Update Job", PageURL = "Update Job", NextPageURL = "Update Job", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 707, Title = "Delete Job", PageURL = "Delete Job", NextPageURL = "Delete Job", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 729, Title = "Get Fund Jobs", PageURL = "GetFundJobs", NextPageURL = "GetFundJobs", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 723, Title = "Get Jobs", PageURL = "GetJobs", NextPageURL = "GetJobs", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 730, Title = "Has Multiple Job Access", PageURL = "HasMultipleJobAccess", NextPageURL = "HasMultipleJobAccess", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 731, Title = "Get Job", PageURL = "GetJob", NextPageURL = "GetJob", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 732, Title = "Work Paper", PageURL = "WorkPaper", NextPageURL = "WorkPaper", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 733, Title = "Get Workpaper Menu", PageURL = "GetWorkpaperMenu", NextPageURL = "GetWorkpaperMenu", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 734, Title = "Get Audit Procedures", PageURL = "GetAuditProcedures", NextPageURL = "GetAuditProcedures", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 736, Title = "Update AuditProcedure Answer", PageURL = "UpdateAuditProcedureAnswer", NextPageURL = "UpdateAuditProcedureAnswer", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 737, Title = "Add Procedure Comments", PageURL = "AddProcedureComments", NextPageURL = "AddProcedureComments", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 739, Title = "Add EditProcedure Oml", PageURL = "AddEditProcedureOml", NextPageURL = "AddEditProcedureOml", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 745, Title = "Get ProcedureTest Summary", PageURL = "GetProcedureTestSummary", NextPageURL = "GetProcedureTestSummary", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 746, Title = "Get Procedure Oml For Manager Review", PageURL = "GetProcedureOmlForManagerReview", NextPageURL = "GetProcedureOmlForManagerReview", TypeID = 1,ParentID = 700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 748, Title = "Get Work Logs", PageURL = "GetWorkLogs", NextPageURL = "GetWorkLogs", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 08, 29)},
                new Options {ID = 753, Title = "GetJobImportData", PageURL = "GetJobImportData", NextPageURL = "GetJobImportData", TypeID = 1,ParentID = 700, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 800, Title = "Notifications", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 801, Title = "Get Notifications", PageURL = "GetNotifications", NextPageURL = "GetNotifications", TypeID = 1,ParentID = 800, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 803, Title = "Delete Notifications", PageURL = "DeleteNotifications", NextPageURL = "DeleteNotifications", TypeID = 1,ParentID = 800, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 900, Title = "Workflow Activities" ,TypeID = 2, AllowAnonymous = true, StatusID = Status.Deleted, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 901, Title = "Get Activities", PageURL = "GetActivities", NextPageURL = "GetActivities", TypeID = 1,ParentID = 900, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 902, Title = "Get Activity",PageURL =  "GetActivity", NextPageURL = "GetActivity", TypeID = 1,ParentID = 900, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 903, Title = "Add Activity", PageURL = "AddActivity", NextPageURL = "AddActivity", TypeID = 1,ParentID = 900, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 904, Title = "Update Activity", PageURL = "UpdateActivity", NextPageURL = "UpdateActivity", TypeID = 1,ParentID = 900, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 905, Title = "Delete Activity", PageURL = "DeleteActivity", NextPageURL = "DeleteActivity", TypeID = 1,ParentID = 900, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 1000, Title = "Setting", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1001, Title = "Get Setting", PageURL = "GetSetting", NextPageURL = "GetSetting", TypeID = 1,ParentID = 1000, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1002, Title = "Update Email Settings", PageURL = "UpdateEmailSettings", NextPageURL = "UpdateEmailSettings", TypeID = 1,ParentID = 1000, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1003, Title = "Update User Profile", PageURL = "UpdateUserProfile", NextPageURL = "UpdateUserProfile", TypeID = 1,ParentID = 1000, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1004, Title = "Get Setting By Type", PageURL = "GetSettingByType", NextPageURL = "GetSettingByType", TypeID = 1,ParentID = 1000, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1005, Title = "Update Sharepoint Setting", PageURL = "UpdateSharepointSetting", NextPageURL = "UpdateSharepointSetting", TypeID = 1,ParentID = 1000, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1006, Title = "Update Password Settings", PageURL = "UpdatePasswordSettings", NextPageURL = "UpdatePasswordSettings", TypeID = 1,ParentID = 1000, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1007, Title = "Update Session Settings", PageURL = "UpdateSessioSettings", NextPageURL = "UpdateSessionSettings", TypeID = 1,ParentID = 1000, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1008, Title = "Update Report Server Settings", PageURL = "UpdateReportServerSettings", NextPageURL = "UpdateReportServerSettings", TypeID = 1,ParentID = 1000, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1009, Title = "Update Version Settings", PageURL = "UpdateVersionSettings", NextPageURL = "UpdateVersionSettings", TypeID = 1,ParentID = 1000, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 09, 18)},

                new Options {ID = 1100, Title = "Sharepoint Document Management", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1101, Title = "Get Files", PageURL = "GetFiles", NextPageURL = "GetFiles", TypeID = 1,ParentID = 1100, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1102, Title = "Delete File", PageURL = "DeleteFile", NextPageURL = "DeleteFile", TypeID = 1,ParentID = 1100, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1103, Title = "Download File", PageURL = "DownloadFile", NextPageURL = "DownloadFile", TypeID = 1,ParentID = 1100, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1104, Title = "Upload Files", PageURL = "UploadFiles", NextPageURL = "UploadFiles", TypeID = 1,ParentID = 1100, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 1200, Title = "Workflow", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1201, Title = "Get Workflow", PageURL = "GetWorkflow", NextPageURL = "GetWorkflow", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1202, Title = "Get Workflow Template", PageURL = "GetWorkflowTemplate", NextPageURL = "GetWorkflowTemplate", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1203, Title = "Get Workflow Pending Processes", PageURL = "GetWorkflowPendingProcesses", NextPageURL = "GetWorkflowPendingProcesses", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1204, Title = "Get Workflow Pending Process Detail", PageURL = "GetWorkflowPendingProcessDetail", NextPageURL = "GetWorkflowPendingProcessDetail", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1205, Title = "Workflow Process Execute Command", PageURL = "WorkflowProcessExecuteCommand", NextPageURL = "WorkflowProcessExecuteCommand", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1206, Title = "Update Workflow Process", PageURL = "UpdateWorkflowProcess", NextPageURL = "UpdateWorkflowProcess", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1207, Title = "Get Workflow Graph", PageURL = "GetWorkFlowGraph", NextPageURL = "GetWorkFlowGraph", TypeID = 1,ParentID = 1200, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 08)},
                new Options {ID = 1208, Title = "Get Workflow Template List", PageURL = "GetWorkflowTemplateList", NextPageURL = "GetWorkflowTemplateList", TypeID = 1,ParentID = 1200, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 08, 18)},
                new Options {ID = 1209, Title = "Set Default WorkFlow", PageURL = "SetDefaultWorkFlow", NextPageURL = "SetDefaultWorkFlow", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 08, 19)},
                new Options {ID = 1210, Title = "Save WorkFlow Graph", PageURL = "SaveWorkFlowGraph", NextPageURL = "SaveWorkFlowGraph", TypeID = 1,ParentID = 1200, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 08, 28)},
                
                new Options {ID = 1300, Title = "Jobs Import", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1301, Title = "Get Import Templates", PageURL = "GetImportTemplates", NextPageURL = "GetImportTemplates", TypeID = 1,ParentID = 1300, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1302, Title = "Get Template", PageURL = "GetTemplate", NextPageURL = "GetTemplate", TypeID = 1,ParentID = 1300, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1303, Title = "Get Template Provider",PageURL =  "GetTemplateProvider", NextPageURL = "GetTemplateProvider", TypeID = 1,ParentID = 1300, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1304, Title = "Add Update Jobs Imported Data", PageURL = "AddUpdateJobsImportedData",NextPageURL =  "AddUpdateJobsImportedData", TypeID = 1,ParentID = 1300, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1305, Title = "Get Template Categories", PageURL = "GetTemplateCategories", NextPageURL = "GetTemplateCategories", TypeID = 1,ParentID = 1300, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1306, Title = "Get Template Categories", PageURL = "GetTemplateCategories", NextPageURL = "GetTemplateCategories", TypeID = 1,ParentID = 1300, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1307, Title = "Get Jobs Imported Data", PageURL = "GetJobsImportedData", NextPageURL = "GetJobsImportedData", TypeID = 1,ParentID = 1300, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1308, Title = "Get Job Flow Status List", PageURL = "GetJobFowStatusList", NextPageURL = "GetJobFlowStatusList", TypeID = 1,ParentID = 1300, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 03)},
                new Options {ID = 1309, Title = "Get Jobs Imported Files List", PageURL = "GetJobsImportedFiles", NextPageURL = "GetJobsImportedFiles", TypeID = 1,ParentID = 1300, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 08, 21)},

                new Options {ID = 1400, Title = "Notes", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1401, Title = "Get Notes", PageURL = "Get Notes",NextPageURL =  "Get Notes", TypeID = 1,ParentID = 1400, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1402, Title = "Add Update Notes", PageURL = "AddUpdateNotes", NextPageURL = "Add Update Notes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1403, Title = "Add Firm Notes", PageURL = "AddFirmNotes", NextPageURL = "AddFirmNotes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1404, Title = "Update Firm Notes", PageURL = "UpdateFirmNotes", NextPageURL = "UpadteFirmNotes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1405, Title = "Get Firm Notes",PageURL =  "Get Firm Notes", NextPageURL = "Get Firm Notes", TypeID = 1,ParentID = 1400, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1406, Title = "Add Job Notes", PageURL = "AddJobNotes", NextPageURL = "AddJobNotes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1407, Title = "Update Job Notes", PageURL = "UpdateJobNotes", NextPageURL = "UpadteJobNotes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1408, Title = "Get Job Notes", PageURL = "GetJobNotes", NextPageURL = "GetJobNotes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1409, Title = "Delete Firm Notes", PageURL = "DeleteFirmNotes", NextPageURL = "DeleteFirmNotes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1410, Title = "Delete Job Notes", PageURL = "DeleteJobNotes", NextPageURL = "DeleteJobNotes", TypeID = 1,ParentID = 1400, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
  
                new Options {ID = 1500, Title = "Firm", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1501, Title = "Get Firms", PageURL = "GetFirms", NextPageURL = "GetFirms", TypeID = 1,ParentID = 1500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1502, Title = "Get Firm Detail", PageURL = "GetFirmDetail", NextPageURL = "GetFirmDetail", TypeID = 1,ParentID = 1500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1503, Title = "Has Multiple Access", PageURL = "HasMultipleAccess", NextPageURL = "HasMultipleAccess", TypeID = 1,ParentID = 1500, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1504, Title = "Get Parents", PageURL = "GetParents", NextPageURL = "GetParents", TypeID = 1,ParentID = 1500, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1505, Title = "Add Firm", PageURL = "AddFirm", NextPageURL = "AddFirm", TypeID = 1,ParentID = 1500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1506, Title = "Get Firm Categories", PageURL = "GetFirmCategories", NextPageURL = "GetFirmCategories", TypeID = 1,ParentID = 1500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1507, Title = "Delete Firm", PageURL = "DeleteFirm", NextPageURL = "DeleteFirm", TypeID = 1,ParentID = 1500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1508, Title = "Get Firm", PageURL = "GetFirm", NextPageURL = "GetFirm", TypeID = 1,ParentID = 1500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1509, Title = "Update Firm", PageURL = "UpdateFirm", NextPageURL = "UpdateFirm", TypeID = 1,ParentID = 1500, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 1600, Title = "Accounts", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1601, Title = "Get Accounts", PageURL = "GetAccounts", NextPageURL = "GetAccounts", TypeID = 1,ParentID = 1600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1602, Title = "Get Account Detail", PageURL = "GetAccountDetail", NextPageURL = "GetAccountDetail", TypeID = 1,ParentID = 1600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1603, Title = "Add Account", PageURL = "AddAccount", NextPageURL = "AddAccount", TypeID = 1,ParentID = 1600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1604, Title = "Update Fund", PageURL = "UpdateFund", NextPageURL = "UpdateFund", TypeID = 1,ParentID = 1600, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 1700, Title = "Funds", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1701, Title = "Get Funds", PageURL = "GetFunds", NextPageURL = "GetFunds", TypeID = 1,ParentID = 1700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1702, Title = "Get Fund Detail", PageURL = "GetFundDetail", NextPageURL = "GetFundDetail", TypeID = 1,ParentID = 1700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1703, Title = "Add Fund", PageURL = "AddFund", NextPageURL = "AddFund", TypeID = 1,ParentID = 1700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1704, Title = "Update Account", PageURL = "UpdateAccount", NextPageURL = "UpdateAccount", TypeID = 1,ParentID = 1700, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 1800, Title = "Groups", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1801, Title = "Add Group", PageURL = "AddGroup", NextPageURL = "AddGroup", TypeID = 1,ParentID = 1800, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1802, Title = "Update Group", PageURL = "UpdateGroup", NextPageURL = "UpdateGroup", TypeID = 1,ParentID = 1800, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},
                new Options {ID = 1803, Title = "Get Group Detail", PageURL = "GetGroupDetail", NextPageURL = "GetGroupDetail", TypeID = 1,ParentID = 1800, AllowAnonymous = false, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01)},

                new Options {ID = 1900, Title = "Reports", TypeID = 2, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 09, 18)},
                new Options {ID = 1901, Title = "Audit Reports", PageURL = "AuditReports", NextPageURL = "AuditReports", TypeID = 1,ParentID = 1900, AllowAnonymous = true, StatusID = Status.Active, CreatedBy = 1, CreatedOn = new DateTime(2014, 09, 18)}

                
            };

            context.Options.AddOrUpdate(options);
        }

        private void AddGroups(SMSFContext content)
        {
            var groups = new []
            {
                new Groups{ID = 1, Title = "Administrators", Description = "Administrators group", StatusID = Status.Active, IsSystem = true, Identifier = Guid.Parse("DBBF60CD-6EA2-46DD-8C76-C2AE77D4E1FB")},
                new Groups{ID = 2, Title = "Operators", Description = "Users perform the business-operations e.g. Accountants, Auditors, Managers etc", StatusID = Status.Active, IsSystem = true, Identifier = Guid.Parse("84549481-A9D3-4B3D-B9F5-3C485B9EBC6B")},
                new Groups{ID = 3, Title = "Clients", Description = "Clients group", StatusID = Status.Active, IsSystem = true, Identifier = Guid.Parse("B4FEA2B1-3E55-4454-83FE-EE847AE017A7")},
            };

            content.Groups.AddOrUpdate(groups);
        }

        private void AddRoles(SMSFContext context)
        {
            var roles = new[]
            {
                new Roles{ ID = 1, Title = "Admin", Description = "Administrator", IsSystem = true, StatusID = Status.Active },
                new Roles{ ID = 2, Title = "Auditor", Description = "Auditor", IsSystem = true, StatusID = Status.Active },
                new Roles{ ID = 3, Title = "Accountant", Description = "Accountant", IsSystem = true, StatusID = Status.Active },
                new Roles{ ID = 4, Title = "Client", Description = "Client", IsSystem = true, StatusID = Status.Active },
                new Roles{ ID = 5, Title = "Operator", Description = "Operator role", StatusID = Status.Active, IsSystem = true,
                    GroupRoles = new[]
                    {
                        new GroupRoles{ GroupID = (int)UserGroup.Operators, RoleID = 5}  
                    }
                }
            };

            context.Roles.AddOrUpdate(roles);
        }

        private void AddRoleOptions(SMSFContext context)
        {
            /* 
                Important:  when adding new option here, please use static values i.e. new DateTime(2014, 07, 01)
                            In CreateOn property, add current date with static values.
            */
            var roleOptions = new[]
            {
                new RoleOptions { OptionID = 101, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 1, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 102, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 2, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 103, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 3, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 201, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 4, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 202, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 5, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 203, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 6, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 204, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 7, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 205, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 8, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 301, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 9, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 302, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 10, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 303, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 11, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 401, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 12, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 402, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 13, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 403, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 14, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 404, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 15, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 405, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 16, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 501, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 17, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 502, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 18, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 503, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 19, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 504, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 20, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 505, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 21, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 506, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 22, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 507, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 23, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 508, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 24, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 509, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 25, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 510, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 26, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 601, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 27, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 602, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 28, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 603, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 29, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 604, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 30, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 605, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 31, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 606, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 32, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 607, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 33, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 608, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 34, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 609, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 35, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 610, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 36, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 611, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 37, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 612, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 38, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 613, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 39, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 614, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 40, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 615, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 41, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 616, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 42, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 617, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 43, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 701, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 44, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 704, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 45, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 705, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 46, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 706, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 47, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 707, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 48, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 723, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 49, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 729, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 50, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 730, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 51, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 731, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 52, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 732, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 53, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 733, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 54, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 734, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 55, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 736, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 56, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 737, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 57, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 739, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 58, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 745, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 59, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 746, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 60, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 753, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 61, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 801, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 62, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 803, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 63, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 901, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 64, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 902, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 65, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 903, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 66, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 904, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 67, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 905, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 68, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1001, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 69, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1002, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 70, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1003, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 71, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1004, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 72, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1005, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 73, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1006, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 74, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1007, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 75, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1008, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 76, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1101, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 77, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1102, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 78, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1103, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 79, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1104, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 80, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1201, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 81, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1202, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 82, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1203, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 83, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1204, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 84, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1205, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 85, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1206, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 86, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1301, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 87, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1302, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 88, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1303, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 89, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1304, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 90, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1305, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 91, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1306, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 92, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1307, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 93, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1401, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 94, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1402, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 95, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1403, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 96, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1404, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 97, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1405, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 98, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1406, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 99, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1407, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 100, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1408, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 101, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1409, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 102, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1410, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 103, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1501, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 104, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1502, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 105, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1503, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 106, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1504, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 107, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1505, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 108, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1506, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 109, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1507, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 110, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1508, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 111, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1509, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 112, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1601, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 113, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1602, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 114, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1603, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 115, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1604, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 116, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1701, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 117, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1702, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 118, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1703, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 119, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1704, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 120, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1801, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 121, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1802, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 122, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1803, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 123, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1406, RoleID = 4, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 124, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1407, RoleID = 4, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 125, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1408, RoleID = 4, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 126, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1410, RoleID = 4, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 127, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 732,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 128, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 733,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 129, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 734,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 130, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 736,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 131, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 737,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 132, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 739,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 133, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 745,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 134, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 746,  RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 135, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1401, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 136, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1402, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 137, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1403, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 138, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1404, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 139, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1405, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 140, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1406, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 141, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1407, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 142, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1408, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 143, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1409, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 144, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1410, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 01), ID = 145, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 702, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 10), ID = 146, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 703, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 10), ID = 147, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 702, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 10), ID = 148, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 703, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 10), ID = 149, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 731, RoleID = 5, CreatedBy = 1, CreatedOn = new DateTime(2014, 07, 10), ID = 150, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 1009, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 09, 18), ID = 151, IsAssigned = true, IsSystem = true},
                new RoleOptions { OptionID = 748, RoleID = 1, CreatedBy = 1, CreatedOn = new DateTime(2014, 08, 29), ID = 172, IsAssigned = true, IsSystem = true},
            };

            context.RoleOptions.AddOrUpdate(roleOptions);
        }

        private void AddWorkFlowStatus(SMSFContext context)
        {
            var wfStatus = new[]
            {
                new WorkflowStatus {ID = 1,Name = "In Progress"},
                new WorkflowStatus {ID = 2,Name = "Completed"},
                new WorkflowStatus {ID = 3,Name = "Overdue"},
                new WorkflowStatus {ID = 4,Name = "Not Started"}
            };
            context.WorkflowStatus.AddOrUpdate(wfStatus);
        }

        private void AddTemplateProvider(SMSFContext context)
        {
            var templateProvider = new[]
            {
                new TemplateProvider
                {
                    ID = 1,
                    Name = "BGL",
                    StatusID = Status.Active
                },
                new TemplateProvider
                {
                    ID = 2,
                    Name = "Class Super",
                    StatusID = Status.Active
                },
                new TemplateProvider
                {
                    ID = 3,
                    Name = "Desktop Super",
                    StatusID = Status.Active
                },
                new TemplateProvider
                {
                    ID = 4,
                    Name = "Supermate",
                    StatusID = Status.Active
                },
                new TemplateProvider
                {
                    ID = 5,
                    Name = "Simplefund 360",
                    StatusID = Status.Active
                }
            };

            context.TemplateProvider.AddOrUpdate(templateProvider);
        }

        private void AddTemplateCategories(SMSFContext context)
        {
            var templateCategories = new[]
            {
                new TemplateCategories
                {
                    ID = 1,
                    Name = "Cashflow",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 2,
                    Name = "Financial Position",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 3,
                    Name = "General Ledger",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 4,
                    Name = "Investment Income Summary",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 5,
                    Name = "Investment Movement",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 6,
                    Name = "Investment Summary",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 7,
                    Name = "Operating Statement",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 8,
                    Name = "Realised CGT",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 9,
                    Name = "Tax Reconciliation",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 10,
                    Name = "Trial Balance",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 11,
                    Name = "Unrealised CGT",
                    TemplateProviderID = 3,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 12,
                    Name = "Cashflow",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 13,
                    Name = "Financial Position",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 14,
                    Name = "Investment Disposals",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 15,
                    Name = "Investment Summary",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 16,
                    Name = "Notes To Financial Statement",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 17,
                    Name = "Operating Statement",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 18,
                    Name = "Income Statement",
                    TemplateProviderID = 1,
                    StatusID = Status.Inactive
                },
                new TemplateCategories
                {
                    ID = 19,
                    Name = "Investment Income",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 20,
                    Name = "Investment Movement",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 21,
                    Name = "Member Statement",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 22,
                    Name = "Investment Schedule",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 23,
                    Name = "Tax Reconciliation",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 24,
                    Name = "Investment Income Comparison",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 25,
                    Name = "Investment Income Summary",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 26,
                    Name = "Investment Movement Summary",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 27,
                    Name = "Investment Performance",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 28,
                    Name = "Investment Portfolio",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 29,
                    Name = "Investment Summary",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 30,
                    Name = "Balance Sheet",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 31,
                    Name = "Cashflow",
                    TemplateProviderID = 5,
                    StatusID = Status.Deleted
                },
                new TemplateCategories
                {
                    ID = 32,
                    Name = "Investment Disposal",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 33,
                    Name = "Investment Movement",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 34,
                    Name = "Investment Summary",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 35,
                    Name = "Members Statement",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 36,
                    Name = "Notes To Financial Statement",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 37,
                    Name = "Operating Statement",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 38,
                    Name = "Investment Income",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 39,
                    Name = "Taxable Income",
                    TemplateProviderID = 1,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 40,
                    Name = "Taxable Income",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 41,
                    Name = "Bank Statement",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 42,
                    Name = "General Ledger",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 43,
                    Name = "Investment Strategy",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 44,
                    Name = "Members Statement",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 45,
                    Name = "Notes To Financial Statements",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 46,
                    Name = "Operating Statement",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 47,
                    Name = "Realised Capital Gains",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 48,
                    Name = "Financial Position",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 49,
                    Name = "Taxable Income",
                    TemplateProviderID = 2,
                    StatusID = Status.Active
                },
                new TemplateCategories
                {
                    ID = 50,
                    Name = "Trustee Minute Resolution",
                    TemplateProviderID = 5,
                    StatusID = Status.Active
                }
            };
            context.TemplateCategories.AddOrUpdate(templateCategories);
        }

        private void AddTemplates(SMSFContext context)
        {
            var templates = new[]
            {
                new Templates
                {
                    ID = 1,
                    Name = "Statement of Cashflows",
                    CategoryID = 1,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 2,
                    Name = "Statement of Financial Position",
                    CategoryID = 2,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 3,
                    Name = "General Ledger",
                    CategoryID = 3,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Date</column>
        <column>Type</column>
        <column>Sub-Type</column>
        <column>Member</column>
        <column>Bank</column>
        <column>Ref.</column>
        <column>Details</column>
        <column>Units</column>
        <column>Debit</column>
        <column>Credit</column>
        <column>Balance</column>
    </columns>]]>
  </print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""7"" distance-threshold=""10"" padding-text="""" />
  <values-resolve-overlapping target-columns=""11"" distance-threshold=""10""/>
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
        switch(values.current.value)
        {
          case 'Equities':
          case 'Trust':
            elementTAG='heading';
          break;
        }
      }
      else if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 4,
                    Name = "Investment Income Summary",
                    CategoryID = 4,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Accounting Income</column>
        <column>Frank. Credits</column>
        <column>Tax FY</column>
        <column>Tax Deferred</column>
        <column>Tax Free</column>
        <column>Tax Exempt</column>
        <column>Gross Disc.</column>
        <column>Other Gain</column>
        <column>Losses Rec.</column>
        <column>Fgn Income</column>
        <column>Taxable Inc. (ex CGT/Fgn)</column>
        <column>Fgn Income</column>
        <column>Fgn Credits</column>
        <column>Other Credits</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""15"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
      switch(values.current.value)
      {
      case 'Cash at Bank':
      case 'Equities':
      case 'Trust':
      elementTAG='heading';
      break;
      }
      }
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 5,
                    Name = "Investment Movement",
                    CategoryID = 5,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Opening Units</column>
        <column>Opening Cost</column>
        <column>Additions Units</column>
        <column>Additions Cost</column>
        <column>Disposals Units</column>
        <column>Disposals Cost</column>
        <column>Disposals Profit/(Loss)</column>
        <column>Closing Units</column>
        <column>Closing Cost</column>
        <column>Closing Market Value</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""6"" distance-threshold=""10"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
        switch(values.current.value)
        {
          case 'Equities':
          case 'Trust':
            elementTAG='heading';
          break;
        }
      }
      else if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 6,
                    Name = "Investment Summary",
                    CategoryID = 6,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Units</column>
        <column>Average Cost</column>
        <column>Cost</column>
        <column>Average Market</column>
        <column>Market Value</column>
        <column>Accounting Income</column>
        <column>Market Yield</column>
        <column>Total Mkt Value %</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""5"" distance-threshold=""10"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
        switch(values.current.value)
        {
          case 'Equities':
          case 'Trust':
            elementTAG='heading';
          break;
        }
      }
      else if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 7,
                    Name = "Operating Statement",
                    CategoryID = 7,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 8,
                    Name = "Realised CGT",
                    CategoryID = 8,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Member Account</column>
        <column>Sale Date</column>
        <column>Units</column>
        <column>Proceeds</column>
        <column>Cost Base</column>
        <column>Gross Discounted</column>
        <column>Frozen Indexed</column>
        <column>Other Gain</column>
        <column>Loss</column>
        <column>Recouped Losses</column>
        <column>Taxable Gain</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <!--<if condition=""text.getValue()=='Trust'"">
      <break-loop/>
    </if>-->
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""12"" min-columns=""8"" distance-threshold=""18"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
      switch(values.current.value)
      {
      case 'Equities':
      case 'Trust':
      elementTAG='heading';
      break;
      }
      }
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 9,
                    Name = "Tax Reconciliation",
                    CategoryID = 9,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Chart</column>
        <column>Description</column>
        <column>Inv. Code</column>
        <column>Accounting</column>
        <column>Gross Taxable</column>
        <column>Pension Deductions</column>
        <column>Net Taxable</column>
        <column>Difference</column>
        <column>Fgn Income</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""3"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <!--<exec>
      var isNotify = false;
      if (values.line.valuesCount &lt; 9 &amp;&amp; elementTAG == 'item' )
      isNotify = true;
    </exec>-->
    <!--<if condition=""isNotify==true"">
      <then>
        <print><![CDATA[<Notification>PDF Format is not appropriate.</Notification>]]></print>
        <print><![CDATA[</data>]]></print>
        <print><![CDATA[</report>]]></print>
        <break-loop/>
      </then>
      <else>-->
        <print><![CDATA[<row>]]></print>
        <values-each-value>
          <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
        </values-each-value>
        <print><![CDATA[</row>]]></print>
      <!--</else>
    </if>-->
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>
"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 10,
                    Name = "Trial Balance",
                    CategoryID = 10,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Last Year</column>
        <column>Chart</column>
        <column>Investment</column>
        <column>Description</column>
        <column>Debit</column>
        <column>Credit</column>
    </columns>]]>
  </print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
    <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""5"" min-columns=""4"" distance-threshold=""15"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.current.value=='')
        elementTAG='total';
      else
        elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <!--<exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<totalCols>#!values.line.valuesCount#</totalCols>]]></print>
    -->
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 11,
                    Name = "Unrealised CGT",
                    CategoryID = 11,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print>
    <![CDATA[<columns>
        <column>Investment</column>
        <column>Member Account</column>
        <column>Last Valuation</column>
        <column>Units</column>
        <column>Unit Value</column>
        <column>Market Value</column>
        <column>Cost Base</column>
        <column>Gross Discounted</column>
        <column>Frozen Indexed</column>
        <column>Other Gain</column>
        <column>Loss</column>
        <column>Recouped Losses</column>
        <column>Unrealised Taxable Gain</column>
    </columns>]]>
  </print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <!--<if condition=""text.getValue()=='Trust'"">
      <break-loop/>
    </if>-->
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""12"" min-columns=""8"" distance-threshold=""18"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      {
      switch(values.current.value)
      {
      case 'Equities':
      case 'Trust':
      elementTAG='heading';
      break;
      }
      }
      else if (values.current.value=='')
      elementTAG='total';
      else
      elementTAG='item';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!values.current.value#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 12,
                    Name = "Statement of Cashflows",
                    CategoryID = 12,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>

          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
          <print><![CDATA[<columns>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</columns></header>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 13,
                    Name = "Balance Sheet",
                    CategoryID = 13,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>

          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<columns>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</columns></header>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<third-heading>#!text.getValue()#</third-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 14,
                    Name = "Investment Disposals",
                    CategoryID = 14,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns><row>]]></print>
  <print><![CDATA[<column>Investment</column>]]></print>
  <print><![CDATA[<column>Disposal Method</column>]]></print>
  <print><![CDATA[<column>Units Sold</column>]]></print>
  <print><![CDATA[<column>Purchase Cost</column>]]></print>
  <print><![CDATA[<column>Cost Base Adjustments</column>]]></print>
  <print><![CDATA[<column>Adjusted Cost Base</column>]]></print>
  <print><![CDATA[<column>Consideration</column>]]></print>
  <print><![CDATA[<column>Total Prof/(Loss)</column>]]></print>
  <print><![CDATA[<column>Taxable Prof/(Loss)</column>]]></print>
  <print><![CDATA[<column>Non Taxable Prof/(Loss)</column>]]></print>
  <print><![CDATA[<column>Accounting Prof/(Loss)</column>]]></print>
  <print><![CDATA[</row></columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""5"" distance-threshold=""10"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 15,
                    Name = "Investment Summary",
                    CategoryID = 15,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
   <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""3"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 16,
                    Name = "Notes To Financial Statement",
                    CategoryID = 16,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>
          
          <print><![CDATA[</header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 17,
                    Name = "Operating Statement",
                    CategoryID = 17,

                    #region TemplateText

                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>

          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>

          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
          <print><![CDATA[<columns>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</columns></header>]]></print>
          <exec>
            excel.jumpRows(1);
          </exec>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<third-heading>#!text.getValue()#</third-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"

                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 18,
                    Name = "Income Statement",
                    CategoryID = 18,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>

          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_sub_heading>#!xmlize(excel.rowStartText)#</second_sub_heading>]]></print>

          <print><![CDATA[</header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
    <print><![CDATA[</data></report>]]></print>
  </while>
</template>"
                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 19,
                    Name = "Investment Income",
                    CategoryID = 19,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
   <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns><row>]]></print>
  <print><![CDATA[<column>Investment</column>]]></print>
  <print><![CDATA[<column>Total Income</column>]]></print>
  <print><![CDATA[<column>Franked Amount</column>]]></print>
  <print><![CDATA[<column>Unfranked Amount</column>]]></print>
  <print><![CDATA[<column>Franking Credits</column>]]></print>
  <print><![CDATA[<column>Foreign Credits</column>]]></print>
  <print><![CDATA[<column>TFN Credits</column>]]></print>
  <print><![CDATA[<column>Tax Free</column>]]></print>
  <print><![CDATA[<column>Tax Exempt</column>]]></print>
  <print><![CDATA[<column>Tax Deferred</column>]]></print>
  <print><![CDATA[<column>Expenses</column>]]></print>
  <print><![CDATA[<column>Capital Gains Disc.*</column>]]></print>
  <print><![CDATA[<column>GST</column>]]></print>
  <print><![CDATA[<column>Taxable Income (incl Cap Gains)</column>]]></print>
  <print><![CDATA[<column>Gains Capital Indexed</column>]]></print>
  <print><![CDATA[<column>Discounted Capital Gains*</column>]]></print>
  <print><![CDATA[<column>Other Capital Gains*</column>]]></print>
  <print><![CDATA[<column>Taxable Capital Gains</column>]]></print>
  <print><![CDATA[</row></columns>]]></print>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""18"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 20,
                    Name = "Investment Movement",
                    CategoryID = 20,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""excel"">
  <while condition=""excel.readSheet()"">
    <print><![CDATA[<report>]]></print>
    <exec value=""var dataStarted=false""/>
    <while condition=""excel.readRow()"">
      <if condition=""excel.rowStartText.indexOf('SUPERANNUATION FUND')>-1 || excel.rowStartText.indexOf('SUPER FUND')>-1"">
        <then>
          <print><![CDATA[<header>]]></print>
          <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>
          <exec value=""excel.readRow()""/>
          <print><![CDATA[<columns>]]></print>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[<row>]]></print>
          <exec value=""excel.readRow()""/>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <print><![CDATA[<column>#!value#</column>]]></print>
          </while>
          <print><![CDATA[</row>]]></print>
          <print><![CDATA[</columns></header>]]></print>
        </then>
        <else>
          <if condition=""!dataStarted"">
            <then>
              <exec>dataStarted=true;</exec>
              <print><![CDATA[<data>]]></print>
            </then>
          </if>
          <print><![CDATA[<row>]]></print>
          <while condition=""excel.readColumn()"">
            <exec value=""var value=xmlize(excel.value)""/>
            <if condition=""excel.rowStartColumn>0"">
              <then>
                <print><![CDATA[<item_total>#!value#</item_total>]]></print>
              </then>
              <else>
                <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                  <then>
                    <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                  </then>
                  <else>
                    <print><![CDATA[<item>#!value#</item>]]></print>
                  </else>
                </if>
              </else>
            </if>
          </while>
          <print><![CDATA[</row>]]></print>
        </else>
      </if>
    </while>
   <print><![CDATA[</data></report>]]></print>
  </while>
</template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns><row>]]></print>
  <print><![CDATA[<column>Investment</column>]]></print>
  <print><![CDATA[<column>Opening Balance Units</column>]]></print>
  <print><![CDATA[<column>Opening Balance Cost</column>]]></print>
  <print><![CDATA[<column>Additions Units</column>]]></print>
  <print><![CDATA[<column>Additions Cost</column>]]></print>
  <print><![CDATA[<column>Disposals Units</column>]]></print>
  <print><![CDATA[<column>Disposals Cost</column>]]></print>
  <print><![CDATA[<column>Disposals Prof/(Loss)</column>]]></print>
  <print><![CDATA[<column>Closing Balance Units</column>]]></print>
  <print><![CDATA[<column>Closing Balance Cost</column>]]></print>
  <print><![CDATA[<column>Closing Balance Market</column>]]></print>
  <print><![CDATA[</row></columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""5"" distance-threshold=""10"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>"
                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                }
                ,
                new Templates
                {
                    ID = 21,
                    Name = "Member Statement",
                    CategoryID = 21,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""excel"">
                              <while condition=""excel.readSheet()"">
                                <print><![CDATA[<report>]]></print>
                                <exec value=""var dataStarted=false""/>
                                <while condition=""excel.readRow()"">
                                  <if condition=""excel.rowStartText.indexOf('Member')>-1 &amp;&amp; excel.rowStartText.indexOf('Statement')>-1"">
                                    <then>
                                      <print><![CDATA[<header>]]></print>
                                      <print><![CDATA[<main_heading>#!xmlize(excel.rowStartText)#</main_heading>]]></print>

                                      <exec value=""excel.readRow()""/>
                                      <print><![CDATA[<second_heading>#!xmlize(excel.rowStartText)#</second_heading>]]></print>

                                      <exec value=""excel.readRow()""/>
                                      <print><![CDATA[<second_sub_heading1>#!xmlize(excel.rowStartText)#</second_sub_heading1>]]></print>
          
                                      <exec value=""excel.readRow()""/>
                                      <print><![CDATA[<second_sub_heading2>#!xmlize(excel.rowStartText)#</second_sub_heading2>]]></print>
          
                                      <exec value=""excel.readRow()""/>
                                      <print><![CDATA[<second_sub_heading3>#!xmlize(excel.rowStartText)#</second_sub_heading3>]]></print>

                                      <exec value=""excel.readRow()""/>
                                      <print><![CDATA[<second_sub_heading4>#!xmlize(excel.rowStartText)#</second_sub_heading4>]]></print>

                                      <exec value=""excel.readRow()""/>
                                      <print><![CDATA[<second_sub_heading5>#!xmlize(excel.rowStartText)#</second_sub_heading5>]]></print>
  
                                      <print><![CDATA[</header>]]></print>
                                    </then>
                                    <else>
                                      <if condition=""!dataStarted"">
                                        <then>
                                          <exec>dataStarted=true;</exec>
                                          <print><![CDATA[<data>]]></print>
                                        </then>
                                      </if>
                                      <print><![CDATA[<row>]]></print>
                                      <while condition=""excel.readColumn()"">
                                        <exec value=""var value=xmlize(excel.value)""/>
                                        <if condition=""excel.rowStartColumn>0"">
                                          <then>
                                            <print><![CDATA[<item_total>#!value#</item_total>]]></print>
                                          </then>
                                          <else>
                                            <if condition=""excel.rowValueCount==1 &amp;&amp; excel.rowStartColumn==0"">
                                              <then>
                                                <print><![CDATA[<item_heading>#!value#</item_heading>]]></print>
                                              </then>
                                              <else>
                                                <print><![CDATA[<item>#!value#</item>]]></print>
                                              </else>
                                            </if>
                                          </else>
                                        </if>
                                      </while>
                                      <print><![CDATA[</row>]]></print>
                                    </else>
                                  </if>
                                </while>
                                <print><![CDATA[</data></report>]]></print>
                              </while>
                            </template>
<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
                            </template>"
                    #endregion TemplateText

                    ,
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 22,
                    Name = "Investment Schedule",
                    CategoryID = 22,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 23,
                    Name = "Tax Reconciliation",
                    CategoryID = 23,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column>Account Code</column>]]></print>
  <print><![CDATA[<column>Account</column>]]></print>
  <print><![CDATA[<column>Revaluation/Tax Deferred Amount</column>]]></print>
  <print><![CDATA[<column>Permanent Difference</column>]]></print>
  <print><![CDATA[<column>Amount</column>]]></print>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""5"" min-columns=""3"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 24,
                    Name = "Investment Income Comparison",
                    CategoryID = 24,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 25,
                    Name = "Investment Income Summary",
                    CategoryID = 25,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 26,
                    Name = "Investment Movement Summary",
                    CategoryID = 26,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""10"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 27,
                    Name = "Investment Performance",
                    CategoryID = 27,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""11"" min-columns=""10"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 28,
                    Name = "Investment Portfolio",
                    CategoryID = 28,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""6"" min-columns=""3"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 29,
                    Name = "Investment Summary",
                    CategoryID = 29,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""3"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 30,
                    Name = "Balance Sheet",
                    CategoryID = 30,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",

                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 31,
                    Name = "Cashflow",
                    CategoryID = 31,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 32,
                    Name = "Investment Disposal",
                    CategoryID = 32,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""6"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 33,
                    Name = "Investment Movement",
                    CategoryID = 33,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""6"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 34,
                    Name = "Investment Summary",
                    CategoryID = 34,
                  
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""7"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 35,
                    Name = "Members Statement",
                    CategoryID = 35,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<mainheading>#!header#</mainheading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<mainheading>#!header#</mainheading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<secondheading>#!text.getValue()#</secondheading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""7"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 36,
                    Name = "Notes To Financial Statement",
                    CategoryID = 36,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<mainheading>#!header#</mainheading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<mainheading>#!header#</mainheading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<secondheading>#!text.getValue()#</secondheading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 37,
                    Name = "Operating Statement",
                    CategoryID = 37,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 38,
                    Name = "Investment Income",
                    CategoryID = 38,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""6"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 39,
                    Name = "Taxable Income",
                    CategoryID = 39,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 40,
                    Name = "Taxable Income",
                    CategoryID = 40,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 41,
                    Name = "Bank Statement",
                    CategoryID = 41,

                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 42,
                    Name = "General Ledger",
                    CategoryID = 42,
                    
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""7"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 43,
                    Name = "Investment Strategy",
                    CategoryID = 43,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""7"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 44,
                    Name = "Members Statement",
                    CategoryID = 44,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<mainheading>#!header#</mainheading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<mainheading>#!header#</mainheading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<secondheading>#!text.getValue()#</secondheading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""9"" min-columns=""7"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 45,
                    Name = "Notes To Financial Statements",
                    CategoryID = 45,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""7"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 46,
                    Name = "Operating Statement",
                    CategoryID = 46,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""7"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 47,
                    Name = "Realised Capital Gains",
                    CategoryID = 47,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column>Investment</column>]]></print>
  <print><![CDATA[<column>Accounting Treatment Units</column>]]></print>
  <print><![CDATA[<column>Accounting Treatment Cost</column>]]></print>
  <print><![CDATA[<column>Accounting Treatment Proceeds</column>]]></print>
  <print><![CDATA[<column>Accounting Treatment Accounting Profit/(Loss)</column>]]></print>
  <print><![CDATA[<column>Tax Treatment Adjusted Cost Base</column>]]></print>
  <print><![CDATA[<column>Tax Treatment Reduced Cost Base</column>]]></print>
  <print><![CDATA[<column>Tax Treatment Indexed Cost Base</column>]]></print>
  <print><![CDATA[<column>Tax Treatment Indexed Gains</column>]]></print>
  <print><![CDATA[<column>Tax Treatment Discounted Gains (Gross)</column>]]></print>
  <print><![CDATA[<column>Tax Treatment Other Gains</column>]]></print>
  <print><![CDATA[<column>Tax Treatment Capital Loss</column>]]></print>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""7"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 48,
                    Name = "Financial Position",
                    CategoryID = 48,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""7"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 49,
                    Name = "Taxable Income",
                    CategoryID = 49,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[<params>]]></print>
  <collect-words>
    <print><![CDATA[<date>#!text.getValue()#</date>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</params>]]></print>
  <print><![CDATA[<columns>]]></print>
  <print><![CDATA[<column></column>]]></print>
  <while condition=""!text.endOfLine()"">
    <collect-words>
      <print><![CDATA[<column>#!text.getValue()#</column>]]></print>
    </collect-words>
  </while>
  <print><![CDATA[</columns>]]></print>
  <skip-line/>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <break-loop/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <break-loop/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""4"" min-columns=""2"" distance-threshold=""4"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                    #endregion TemplateText

                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                },
                new Templates
                {
                    ID = 50,
                    Name = "Trustee Minute Resolution",
                    CategoryID = 50,
                 
                    #region TemplateText
                    TemplateText = @"<template output=""xml"" input=""pdf"">
  <print><![CDATA[<report>]]></print>
  <print><![CDATA[<header>]]></print>
  <collect-words>
    <if condition=""text.isPageNumber()==true"">
      <then>
        <skip-line/>
        <collect-words>
          <exec value=""var header=text.getValue();""/>
          <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
        </collect-words>
      </then>
      <else>
        <exec value=""var header=text.getValue();""/>
        <print><![CDATA[<main-heading>#!header#</main-heading>]]></print>
      </else>
    </if>
  </collect-words>
  <skip-line/>
  <collect-words>
    <print><![CDATA[<second-heading>#!text.getValue()#</second-heading>]]></print>
  </collect-words>
  <skip-line/>
  <print><![CDATA[</header>]]></print>
  <print><![CDATA[<data>]]></print>
  <values-clear/>
  <while condition=""!text.endOfFile()"">
    <values-newline/>
    <while condition=""!text.endOfLine()"">
      <collect-words>
        <if condition=""text.isPageNumber()==true"">
          <then>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
            <skip-line/>
          </then>
          <else>
            <if condition=""text.getValue()==header"">
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
              <skip-line/>
            </if>
          </else>
        </if>
        <values-push/>
      </collect-words>
    </while>
    <skip-line/>
  </while>
  <values-resolve-padding target-columns=""7"" min-columns=""5"" distance-threshold=""5"" padding-text="""" />
  <values-each-line>
    <exec value=""var elementTAG='item';""/>
    <exec>
      if (values.line.valuesCount==1)
      elementTAG='heading';
      else if (values.current.value.indexOf('TOTAL')>-1)
      elementTAG='total';
    </exec>
    <print><![CDATA[<row>]]></print>
    <values-each-value>
      <print><![CDATA[<#!elementTAG#>#!xmlize(values.current.value)#</#!elementTAG#>]]></print>
    </values-each-value>
    <print><![CDATA[</row>]]></print>
  </values-each-line>
  <print><![CDATA[</data>]]></print>
  <print><![CDATA[</report>]]></print>
</template>",
                     #endregion TemplateText
                    
                    CreatedBy = 0,
                    CreatedOn = new DateTime(2014, 08, 05)
                }
            };
            context.Templates.AddOrUpdate(templates);
        }
    }
}
