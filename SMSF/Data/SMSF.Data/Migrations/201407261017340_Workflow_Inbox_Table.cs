namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Workflow_Inbox_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkflowInbox",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProcessId = c.Guid(nullable: false),
                        IdentityId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.WorkflowInbox");
        }
    }
}
