namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuditProcedureTemplateChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.EntityAuditProcedureTabTemplates", "AuditProcedureTabID", "dbo.AuditProcedureTab");
            DropForeignKey("dbo.EntityAuditProcedureTabContentTemplates", "AuditProcedureTabContentID", "dbo.AuditProcedureTabContent");
            DropForeignKey("dbo.EntityAuditProcedureTabContentTemplates", "EntityID", "dbo.Entities");
            DropIndex("dbo.EntityAuditProcedureTabTemplates", new[] { "AuditProcedureTabID" });
            DropIndex("dbo.EntityAuditProcedureTabContentTemplates", new[] { "AuditProcedureTabContentID" });
            DropIndex("dbo.EntityAuditProcedureTabContentTemplates", new[] { "EntityID" });
            CreateTable(
                "dbo.AuditProcedureTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EntityID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AuditProcedureTabContentTemplateDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTemplateID = c.Int(nullable: false),
                        AuditProcedureTabContentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTabContent", t => t.AuditProcedureTabContentID, cascadeDelete: true)
                .ForeignKey("dbo.AuditProcedureTemplates", t => t.AuditProcedureTemplateID, cascadeDelete: true)
                .Index(t => t.AuditProcedureTabContentID)
                .Index(t => t.AuditProcedureTemplateID);
            
            CreateTable(
                "dbo.AuditProcedureTabTemplateDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTemplateID = c.Int(nullable: false),
                        AuditProcedureTabID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTab", t => t.AuditProcedureTabID, cascadeDelete: true)
                .ForeignKey("dbo.AuditProcedureTemplates", t => t.AuditProcedureTemplateID, cascadeDelete: true)
                .Index(t => t.AuditProcedureTabID)
                .Index(t => t.AuditProcedureTemplateID);
            
            DropTable("dbo.EntityAuditProcedureTabTemplates");
            DropTable("dbo.EntityAuditProcedureTabContentTemplates");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.EntityAuditProcedureTabContentTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTabContentID = c.Int(nullable: false),
                        EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EntityAuditProcedureTabTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTabID = c.Int(nullable: false),
                        EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.AuditProcedureTabTemplateDetails", "AuditProcedureTemplateID", "dbo.AuditProcedureTemplates");
            DropForeignKey("dbo.AuditProcedureTabTemplateDetails", "AuditProcedureTabID", "dbo.AuditProcedureTab");
            DropForeignKey("dbo.AuditProcedureTabContentTemplateDetails", "AuditProcedureTemplateID", "dbo.AuditProcedureTemplates");
            DropForeignKey("dbo.AuditProcedureTabContentTemplateDetails", "AuditProcedureTabContentID", "dbo.AuditProcedureTabContent");
            DropIndex("dbo.AuditProcedureTabTemplateDetails", new[] { "AuditProcedureTemplateID" });
            DropIndex("dbo.AuditProcedureTabTemplateDetails", new[] { "AuditProcedureTabID" });
            DropIndex("dbo.AuditProcedureTabContentTemplateDetails", new[] { "AuditProcedureTemplateID" });
            DropIndex("dbo.AuditProcedureTabContentTemplateDetails", new[] { "AuditProcedureTabContentID" });
            DropTable("dbo.AuditProcedureTabTemplateDetails");
            DropTable("dbo.AuditProcedureTabContentTemplateDetails");
            DropTable("dbo.AuditProcedureTemplates");
            CreateIndex("dbo.EntityAuditProcedureTabContentTemplates", "EntityID");
            CreateIndex("dbo.EntityAuditProcedureTabContentTemplates", "AuditProcedureTabContentID");
            CreateIndex("dbo.EntityAuditProcedureTabTemplates", "AuditProcedureTabID");
            AddForeignKey("dbo.EntityAuditProcedureTabContentTemplates", "EntityID", "dbo.Entities", "ID", cascadeDelete: true);
            AddForeignKey("dbo.EntityAuditProcedureTabContentTemplates", "AuditProcedureTabContentID", "dbo.AuditProcedureTabContent", "ID", cascadeDelete: true);
            AddForeignKey("dbo.EntityAuditProcedureTabTemplates", "AuditProcedureTabID", "dbo.AuditProcedureTab", "ID", cascadeDelete: true);
        }
    }
}
