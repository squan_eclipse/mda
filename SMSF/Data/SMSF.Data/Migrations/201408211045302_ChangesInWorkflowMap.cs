namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangesInWorkflowMap : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkflowMaps", "Name", c => c.String(maxLength: 25));
            AddColumn("dbo.WorkflowMaps", "IsActive", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.WorkflowMaps", "CreatedBy", c => c.Int(nullable: false, defaultValue: 1));
            AddColumn("dbo.WorkflowMaps", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.WorkflowMaps", "ModifiedBy", c => c.Int(defaultValue: 1));
            AddColumn("dbo.WorkflowMaps", "ModifiedOn", c => c.DateTime());

            Sql("Update dbo.WorkflowMaps Set CreatedOn = GetDate(), ModifiedOn = GetDate()");
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkflowMaps", "ModifiedOn");
            DropColumn("dbo.WorkflowMaps", "ModifiedBy");
            DropColumn("dbo.WorkflowMaps", "CreatedOn");
            DropColumn("dbo.WorkflowMaps", "CreatedBy");
            DropColumn("dbo.WorkflowMaps", "IsActive");
            DropColumn("dbo.WorkflowMaps", "Name");
        }
    }
}
