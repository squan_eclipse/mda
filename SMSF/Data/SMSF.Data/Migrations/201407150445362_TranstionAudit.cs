using DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TranstionAudit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transitions", "StatusID", c => c.Int(nullable: false,defaultValue:(int)Status.Active));
            AddColumn("dbo.Transitions", "CreatedBy", c => c.Int(nullable: false));
            AddColumn("dbo.Transitions", "CreatedOn", c => c.DateTime(nullable: false));
            AddColumn("dbo.Transitions", "ModifiedBy", c => c.Int());
            AddColumn("dbo.Transitions", "ModifiedOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transitions", "ModifiedOn");
            DropColumn("dbo.Transitions", "ModifiedBy");
            DropColumn("dbo.Transitions", "CreatedOn");
            DropColumn("dbo.Transitions", "CreatedBy");
            DropColumn("dbo.Transitions", "StatusID");
        }
    }
}
