namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountWorkPaper : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EntityAuditProcedureTabs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTabID = c.Int(nullable: false),
                        EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTab", t => t.AuditProcedureTabID, cascadeDelete: true)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.AuditProcedureTabID)
                .Index(t => t.EntityID);
            
            CreateTable(
                "dbo.EntityAuditProcedureTabTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTabID = c.Int(nullable: false),
                        EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTab", t => t.AuditProcedureTabID, cascadeDelete: true)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.AuditProcedureTabID)
                .Index(t => t.EntityID);
            
            CreateTable(
                "dbo.EntityAuditProcedureTabContents",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTabContentID = c.Int(nullable: false),
                        EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTabContent", t => t.AuditProcedureTabContentID, cascadeDelete: true)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.AuditProcedureTabContentID)
                .Index(t => t.EntityID);
            
            CreateTable(
                "dbo.EntityAuditProcedureTabContentTemplates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureTabContentID = c.Int(nullable: false),
                        EntityID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTabContent", t => t.AuditProcedureTabContentID, cascadeDelete: true)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.AuditProcedureTabContentID)
                .Index(t => t.EntityID);
            
            AddColumn("dbo.AuditProcedureTabContent", "EntityId", c => c.Int());
            CreateIndex("dbo.AuditProcedureTabContent", "EntityId");
            AddForeignKey("dbo.AuditProcedureTabContent", "EntityId", "dbo.Entities", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EntityAuditProcedureTabContentTemplates", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.EntityAuditProcedureTabContentTemplates", "AuditProcedureTabContentID", "dbo.AuditProcedureTabContent");
            DropForeignKey("dbo.EntityAuditProcedureTabContents", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.EntityAuditProcedureTabContents", "AuditProcedureTabContentID", "dbo.AuditProcedureTabContent");
            DropForeignKey("dbo.AuditProcedureTabContent", "EntityId", "dbo.Entities");
            DropForeignKey("dbo.EntityAuditProcedureTabTemplates", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.EntityAuditProcedureTabTemplates", "AuditProcedureTabID", "dbo.AuditProcedureTab");
            DropForeignKey("dbo.EntityAuditProcedureTabs", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.EntityAuditProcedureTabs", "AuditProcedureTabID", "dbo.AuditProcedureTab");
            DropIndex("dbo.EntityAuditProcedureTabContentTemplates", new[] { "EntityID" });
            DropIndex("dbo.EntityAuditProcedureTabContentTemplates", new[] { "AuditProcedureTabContentID" });
            DropIndex("dbo.EntityAuditProcedureTabContents", new[] { "EntityID" });
            DropIndex("dbo.EntityAuditProcedureTabContents", new[] { "AuditProcedureTabContentID" });
            DropIndex("dbo.AuditProcedureTabContent", new[] { "EntityId" });
            DropIndex("dbo.EntityAuditProcedureTabTemplates", new[] { "EntityID" });
            DropIndex("dbo.EntityAuditProcedureTabTemplates", new[] { "AuditProcedureTabID" });
            DropIndex("dbo.EntityAuditProcedureTabs", new[] { "EntityID" });
            DropIndex("dbo.EntityAuditProcedureTabs", new[] { "AuditProcedureTabID" });
            DropColumn("dbo.AuditProcedureTabContent", "EntityId");
            DropTable("dbo.EntityAuditProcedureTabContentTemplates");
            DropTable("dbo.EntityAuditProcedureTabContents");
            DropTable("dbo.EntityAuditProcedureTabTemplates");
            DropTable("dbo.EntityAuditProcedureTabs");
        }
    }
}
