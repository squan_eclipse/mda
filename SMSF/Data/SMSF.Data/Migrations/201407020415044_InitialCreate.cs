namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountManagerContacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccountManagerID = c.Int(nullable: false),
                        ContactID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccountManagers", t => t.AccountManagerID, cascadeDelete: true)
                .ForeignKey("dbo.Contacts", t => t.ContactID, cascadeDelete: true)
                .Index(t => t.AccountManagerID)
                .Index(t => t.ContactID);
            
            CreateTable(
                "dbo.AccountManagers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Identifier = c.Guid(),
                        EntityID = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 4),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        MidName = c.String(maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 100),
                        StatusID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.EntityID);
            
            CreateTable(
                "dbo.Entities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Identifier = c.Guid(),
                        TypeID = c.Int(nullable: false),
                        CategoryID = c.Int(),
                        ABN = c.String(maxLength: 50),
                        ACN = c.String(maxLength: 50),
                        GST = c.String(maxLength: 50),
                        TFN = c.String(maxLength: 50),
                        Name = c.String(nullable: false, maxLength: 200),
                        Note = c.String(),
                        ParentID = c.Int(),
                        PrimaryAccountManagerID = c.Int(),
                        SecondaryAccountManagerID = c.Int(),
                        StatusID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Entities", t => t.ParentID)
                .ForeignKey("dbo.EntityCategories", t => t.CategoryID)
                .ForeignKey("dbo.EntityTypes", t => t.TypeID, cascadeDelete: true)
                .Index(t => t.ParentID)
                .Index(t => t.CategoryID)
                .Index(t => t.TypeID);
            
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EntityID = c.Int(nullable: false),
                        TypeID = c.Int(nullable: false),
                        UnitNumber = c.String(maxLength: 50),
                        StreetNumber = c.String(maxLength: 50),
                        StreetName = c.String(maxLength: 50),
                        StreetType = c.String(maxLength: 50),
                        City = c.String(nullable: false, maxLength: 50),
                        StateID = c.Int(),
                        CountryID = c.Int(),
                        PostCode = c.String(maxLength: 50),
                        Default = c.Boolean(nullable: false),
                        StatusID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AddressTypes", t => t.TypeID, cascadeDelete: true)
                .ForeignKey("dbo.Countries", t => t.CountryID)
                .ForeignKey("dbo.States", t => t.StateID)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.TypeID)
                .Index(t => t.CountryID)
                .Index(t => t.StateID)
                .Index(t => t.EntityID);
            
            CreateTable(
                "dbo.AddressTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        Default = c.Boolean(nullable: false),
                        Code = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.States",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CountryID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        Code = c.String(maxLength: 10),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Countries", t => t.CountryID, cascadeDelete: true)
                .Index(t => t.CountryID);
            
            CreateTable(
                "dbo.AuditProcedureTab",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Caption = c.String(nullable: false, maxLength: 50),
                        TabOrder = c.Int(nullable: false),
                        EntityId = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Entities", t => t.EntityId)
                .Index(t => t.EntityId);
            
            CreateTable(
                "dbo.AuditProcedureTabContent",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        AuditProcedureTabId = c.Int(nullable: false),
                        Caption = c.String(nullable: false, maxLength: 256),
                        ContentType = c.Int(nullable: false),
                        ControlType = c.Int(),
                        TemplateId = c.Int(),
                        Order = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTab", t => t.AuditProcedureTabId, cascadeDelete: true)
                .Index(t => t.AuditProcedureTabId);
            
            CreateTable(
                "dbo.AuditProcedure",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        AuditProcedureTabContentId = c.Int(nullable: false),
                        ParentId = c.Int(),
                        Title = c.String(),
                        Attachments = c.Boolean(nullable: false),
                        Oml = c.Boolean(nullable: false),
                        Comments = c.Boolean(nullable: false),
                        ControlType = c.Int(),
                        AnswerControlType = c.Int(),
                        Checkbox = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureTabContent", t => t.AuditProcedureTabContentId, cascadeDelete: true)
                .ForeignKey("dbo.AuditProcedure", t => t.ParentId)
                .Index(t => t.AuditProcedureTabContentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.AuditProcedureAnswer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        IsChecked = c.Boolean(),
                        AuditProcedureId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedure", t => t.AuditProcedureId, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .Index(t => t.AuditProcedureId)
                .Index(t => t.JobId);
            
            CreateTable(
                "dbo.Jobs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FundID = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 100),
                        Description = c.String(maxLength: 500),
                        StartDate = c.DateTime(nullable: false),
                        CompleteDate = c.DateTime(),
                        WorkingHours = c.Int(),
                        DataLink = c.String(maxLength: 100),
                        StatusID = c.Int(nullable: false),
                        Identifier = c.Guid(),
                        WorkflowStatusID = c.Int(nullable: false),
                        EstimatedHours = c.Int(),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Entities", t => t.FundID)
                .ForeignKey("dbo.WorkflowStatus", t => t.WorkflowStatusID, cascadeDelete: true)
                .Index(t => t.FundID)
                .Index(t => t.WorkflowStatusID);
            
            CreateTable(
                "dbo.AccountManagerJobs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccountManagerID = c.Int(nullable: false),
                        JobID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                        JobRoleID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccountManagers", t => t.AccountManagerID, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobID, cascadeDelete: true)
                .ForeignKey("dbo.AccountManagerJobRoles", t => t.JobRoleID, cascadeDelete: true)
                .Index(t => t.AccountManagerID)
                .Index(t => t.JobID)
                .Index(t => t.JobRoleID);
            
            CreateTable(
                "dbo.AccountManagerJobRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 35),
                        IsActive = c.Boolean(nullable: false),
                        Identifier = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AuditProcedureComment",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        AuditProcedureId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedure", t => t.AuditProcedureId, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .Index(t => t.AuditProcedureId)
                .Index(t => t.JobId);
            
            CreateTable(
                "dbo.JobNotes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Note = c.String(nullable: false, maxLength: 500),
                        JobID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobID, cascadeDelete: true)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.JobImportedData",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        JobId = c.Int(nullable: false),
                        TemplateId = c.Int(nullable: false),
                        ControlType = c.Int(nullable: false),
                        XmlData = c.String(storeType: "xml"),
                        Filename = c.String(nullable: false, maxLength: 200),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .ForeignKey("dbo.Templates", t => t.TemplateId, cascadeDelete: true)
                .Index(t => t.JobId)
                .Index(t => t.TemplateId);
            
            CreateTable(
                "dbo.Templates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        CategoryID = c.Int(nullable: false),
                        TemplateText = c.String(),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                        TemplateFormats_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TemplateCategories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.TemplateFormats", t => t.TemplateFormats_ID)
                .Index(t => t.CategoryID)
                .Index(t => t.TemplateFormats_ID);
            
            CreateTable(
                "dbo.TemplateCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        TemplateProviderID = c.Int(nullable: false),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkflowStatus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.JobWorkLogs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        JobID = c.Int(nullable: false),
                        AccountManagerID = c.Int(nullable: false),
                        Start = c.DateTime(nullable: false),
                        End = c.DateTime(nullable: false),
                        Description = c.String(),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccountManagers", t => t.AccountManagerID, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobID, cascadeDelete: true)
                .Index(t => t.AccountManagerID)
                .Index(t => t.JobID);
            
            CreateTable(
                "dbo.AuditProcedureAttachment",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AuditProcedureId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedure", t => t.AuditProcedureId, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .Index(t => t.AuditProcedureId)
                .Index(t => t.JobId);
            
            CreateTable(
                "dbo.AuditProcedureAttachmentDetail",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FileName = c.String(),
                        AuditProcedureAttachmentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedureAttachment", t => t.AuditProcedureAttachmentId, cascadeDelete: true)
                .Index(t => t.AuditProcedureAttachmentId);
            
            CreateTable(
                "dbo.AuditProcedureColumn",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        AuditProcedureId = c.Int(nullable: false),
                        ColumnName = c.String(),
                        ColumnValue = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedure", t => t.AuditProcedureId, cascadeDelete: true)
                .Index(t => t.AuditProcedureId);
            
            CreateTable(
                "dbo.AuditProcedureOml",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CustomOption = c.String(),
                        NonFundamental = c.Boolean(nullable: false),
                        AuditProcedureId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                        Item = c.String(),
                        StaffComments = c.String(),
                        Initials = c.String(),
                        IsCleared = c.Boolean(nullable: false),
                        IsManagerApproved = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedure", t => t.AuditProcedureId, cascadeDelete: true)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .Index(t => t.AuditProcedureId)
                .Index(t => t.JobId);
            
            CreateTable(
                "dbo.AuditProcedureOption",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        AuditProcedureId = c.Int(nullable: false),
                        AuditProcedureOptionDetailId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AuditProcedure", t => t.AuditProcedureId, cascadeDelete: true)
                .ForeignKey("dbo.AuditProcedureOptionDetail", t => t.AuditProcedureOptionDetailId, cascadeDelete: true)
                .Index(t => t.AuditProcedureId)
                .Index(t => t.AuditProcedureOptionDetailId);
            
            CreateTable(
                "dbo.AuditProcedureOptionDetail",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Field = c.String(),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ClientDetails",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        ContractID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contracts", t => t.ContractID, cascadeDelete: true)
                .ForeignKey("dbo.Entities", t => t.ID)
                .Index(t => t.ContractID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EntityCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EntityContacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EntityID = c.Int(nullable: false),
                        ContractID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Contacts", t => t.ContractID, cascadeDelete: true)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.ContractID)
                .Index(t => t.EntityID);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TypeID = c.Int(nullable: false),
                        CountryCode = c.String(maxLength: 50),
                        AreaCode = c.String(maxLength: 50),
                        Contact = c.String(nullable: false, maxLength: 250),
                        StatusID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ContactTypes", t => t.TypeID, cascadeDelete: true)
                .Index(t => t.TypeID);
            
            CreateTable(
                "dbo.ContactTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.EntityTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.FundDetails",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        TrusteeID = c.Int(nullable: false),
                        OwnerID = c.Int(nullable: false),
                        MainContactID = c.Int(nullable: false),
                        IsPreviousClient = c.Boolean(),
                        IsDirectDebit = c.Boolean(),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Entities", t => t.ID)
                .ForeignKey("dbo.AccountManagers", t => t.MainContactID)
                .ForeignKey("dbo.AccountManagers", t => t.OwnerID)
                .ForeignKey("dbo.AccountManagers", t => t.TrusteeID)
                .Index(t => t.ID)
                .Index(t => t.MainContactID)
                .Index(t => t.OwnerID)
                .Index(t => t.TrusteeID);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Note = c.String(nullable: false, maxLength: 500),
                        EntityID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Entities", t => t.EntityID, cascadeDelete: true)
                .Index(t => t.EntityID);
            
            CreateTable(
                "dbo.WorkflowMaps",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EntityId = c.Int(),
                        WorkFlowTypeId = c.Int(nullable: false),
                        WorkFlowId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Entities", t => t.EntityId)
                .ForeignKey("dbo.WorkflowProcessScheme", t => t.WorkFlowId, cascadeDelete: true)
                .ForeignKey("dbo.Workflows", t => t.WorkFlowTypeId, cascadeDelete: true)
                .Index(t => t.EntityId)
                .Index(t => t.WorkFlowId)
                .Index(t => t.WorkFlowTypeId);
            
            CreateTable(
                "dbo.WorkflowProcessScheme",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Scheme = c.String(nullable: false, storeType: "ntext"),
                        DefiningParameters = c.String(nullable: false, storeType: "ntext"),
                        DefiningParametersHash = c.String(nullable: false, maxLength: 1024),
                        ProcessName = c.String(nullable: false),
                        IsObsolete = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Workflows",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Activities",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        WorkflowID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        StateID = c.Int(nullable: false),
                        IsInitial = c.Boolean(nullable: false),
                        IsFinal = c.Boolean(nullable: false),
                        IsForSetState = c.Boolean(nullable: false),
                        IsAutoScheme = c.Boolean(nullable: false),
                        ActivityView = c.String(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ActivityStates", t => t.StateID, cascadeDelete: true)
                .ForeignKey("dbo.Workflows", t => t.WorkflowID, cascadeDelete: true)
                .Index(t => t.StateID)
                .Index(t => t.WorkflowID);
            
            CreateTable(
                "dbo.ActivityActions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IsPreExecution = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        ActivityID = c.Int(nullable: false),
                        ActionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Actions", t => t.ActionID, cascadeDelete: true)
                .ForeignKey("dbo.Activities", t => t.ActivityID, cascadeDelete: true)
                .Index(t => t.ActionID)
                .Index(t => t.ActivityID);
            
            CreateTable(
                "dbo.Actions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        Type = c.String(nullable: false, maxLength: 1024),
                        Method = c.String(nullable: false, maxLength: 250),
                        IsConditional = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ActionParameters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        IsOutput = c.Boolean(nullable: false),
                        Order = c.Int(nullable: false),
                        ActionID = c.Int(nullable: false),
                        ParameterID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Actions", t => t.ActionID, cascadeDelete: true)
                .ForeignKey("dbo.Parameters", t => t.ParameterID, cascadeDelete: true)
                .Index(t => t.ActionID)
                .Index(t => t.ParameterID);
            
            CreateTable(
                "dbo.Parameters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        Type = c.String(nullable: false, maxLength: 1024),
                        Purpose = c.Int(nullable: false),
                        SerializedDefaultValue = c.String(maxLength: 1024),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CommandParameters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CommandID = c.Int(nullable: false),
                        ParameterID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Commands", t => t.CommandID, cascadeDelete: true)
                .ForeignKey("dbo.Parameters", t => t.ParameterID, cascadeDelete: true)
                .Index(t => t.CommandID)
                .Index(t => t.ParameterID);
            
            CreateTable(
                "dbo.Commands",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Transitions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        WorkflowID = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 250),
                        FromActivityID = c.Int(nullable: false),
                        ToActivityID = c.Int(nullable: false),
                        Classifier = c.Int(nullable: false),
                        CommandType = c.Int(nullable: false),
                        CommandID = c.Int(),
                        ActionType = c.Int(nullable: false),
                        ActionID = c.Int(),
                        PreExecutionResult = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Actions", t => t.ActionID)
                .ForeignKey("dbo.Commands", t => t.CommandID)
                .ForeignKey("dbo.Activities", t => t.FromActivityID)
                .ForeignKey("dbo.Activities", t => t.ToActivityID)
                .ForeignKey("dbo.Workflows", t => t.WorkflowID, cascadeDelete: true)
                .Index(t => t.ActionID)
                .Index(t => t.CommandID)
                .Index(t => t.FromActivityID)
                .Index(t => t.ToActivityID)
                .Index(t => t.WorkflowID);
            
            CreateTable(
                "dbo.TransitionActors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RestrictionType = c.Int(nullable: false),
                        TransitionID = c.Int(nullable: false),
                        ActorID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Actors", t => t.ActorID, cascadeDelete: true)
                .ForeignKey("dbo.Transitions", t => t.TransitionID, cascadeDelete: true)
                .Index(t => t.ActorID)
                .Index(t => t.TransitionID);
            
            CreateTable(
                "dbo.Actors",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 250),
                        MethodName = c.String(nullable: false, maxLength: 250),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ActivityStates",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 1000),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.AccountManagerRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccountManagerID = c.Int(nullable: false),
                        AccountRoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccountManagers", t => t.AccountManagerID, cascadeDelete: true)
                .ForeignKey("dbo.AccountRoles", t => t.AccountRoleID, cascadeDelete: true)
                .Index(t => t.AccountManagerID)
                .Index(t => t.AccountRoleID);
            
            CreateTable(
                "dbo.AccountRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        StatusID = c.Int(nullable: false),
                        WorkflowType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Telephones",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AccountManagerID = c.Int(nullable: false),
                        TypeID = c.Int(nullable: false),
                        CountryCode = c.String(maxLength: 50),
                        AreaCode = c.String(maxLength: 50),
                        PhoneNumber = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccountManagers", t => t.AccountManagerID, cascadeDelete: true)
                .ForeignKey("dbo.TelephoneTypes", t => t.TypeID, cascadeDelete: true)
                .Index(t => t.AccountManagerID)
                .Index(t => t.TypeID);
            
            CreateTable(
                "dbo.TelephoneTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Identifier = c.Guid(),
                        GroupID = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 100),
                        Password = c.String(nullable: false, maxLength: 100),
                        LastLoginTime = c.DateTime(),
                        StatusID = c.Int(nullable: false),
                        FailedPasswordAttemptCount = c.Int(),
                        FailedPasswordAttemptStart = c.DateTime(),
                        UserImage = c.Binary(),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AccountManagers", t => t.ID)
                .ForeignKey("dbo.Groups", t => t.GroupID, cascadeDelete: true)
                .Index(t => t.ID)
                .Index(t => t.GroupID);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 500),
                        StatusID = c.Int(nullable: false),
                        IsSystem = c.Boolean(nullable: false),
                        Identifier = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.GroupRoles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        GroupID = c.Int(nullable: false),
                        RoleID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Groups", t => t.GroupID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.GroupID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 500),
                        StatusID = c.Int(nullable: false),
                        IsSystem = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RoleOptions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        OptionID = c.Int(nullable: false),
                        IsAssigned = c.Boolean(nullable: false),
                        IsSystem = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Options", t => t.OptionID, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleID, cascadeDelete: true)
                .Index(t => t.OptionID)
                .Index(t => t.RoleID);
            
            CreateTable(
                "dbo.Options",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        Title = c.String(nullable: false, maxLength: 50),
                        PageURL = c.String(maxLength: 200),
                        NextPageURL = c.String(maxLength: 200),
                        TypeID = c.Int(nullable: false),
                        ParentID = c.Int(),
                        AllowAnonymous = c.Boolean(nullable: false),
                        StatusID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Options", t => t.ParentID)
                .ForeignKey("dbo.OptionTypes", t => t.TypeID, cascadeDelete: true)
                .Index(t => t.ParentID)
                .Index(t => t.TypeID);
            
            CreateTable(
                "dbo.OptionTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 50),
                        Description = c.String(maxLength: 500),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Message = c.String(nullable: false, maxLength: 500),
                        MessageDetail = c.String(),
                        EntityType = c.String(maxLength: 50),
                        ReleventID = c.Int(),
                        IsSystem = c.Boolean(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SettingTypeID = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 50),
                        SettingValue = c.String(maxLength: 250),
                        StatusID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SettingTypes", t => t.SettingTypeID, cascadeDelete: true)
                .Index(t => t.SettingTypeID);
            
            CreateTable(
                "dbo.SettingTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TemplateFormats",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TemplateProviders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        StatusID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TestSummary",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        TestName = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.TestSummaryAnswer",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        JobId = c.Int(nullable: false),
                        TestSummaryId = c.Int(nullable: false),
                        IsCompleted = c.Boolean(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .ForeignKey("dbo.TestSummary", t => t.TestSummaryId, cascadeDelete: true)
                .Index(t => t.JobId)
                .Index(t => t.TestSummaryId);
            
            CreateTable(
                "dbo.TestSummaryComments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TestComments = c.String(),
                        TestSummaryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TestSummary", t => t.TestSummaryId, cascadeDelete: true)
                .Index(t => t.TestSummaryId);
            
            CreateTable(
                "dbo.UserForgotPasswords",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserID = c.Int(nullable: false),
                        ForgotPasswordToken = c.Guid(nullable: false),
                        NewPassword = c.String(nullable: false, maxLength: 100),
                        StatusID = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedBy = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.WorkflowProcessInstance",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StateName = c.String(nullable: false),
                        ActivityName = c.String(nullable: false),
                        SchemeId = c.Guid(),
                        PreviousState = c.String(),
                        PreviousStateForDirect = c.String(),
                        PreviousStateForReverse = c.String(),
                        PreviousActivity = c.String(),
                        PreviousActivityForDirect = c.String(),
                        PreviousActivityForReverse = c.String(),
                        IsDeterminingParametersChanged = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkflowProcessInstancePersistence",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProcessId = c.Guid(nullable: false),
                        ParameterName = c.String(nullable: false),
                        Value = c.String(nullable: false, storeType: "ntext"),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkflowProcessInstanceStatus",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Status = c.Byte(nullable: false),
                        Lock = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkflowProcessTransitionHistory",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProcessId = c.Guid(nullable: false),
                        ExecutorIdentityId = c.Guid(nullable: false),
                        ActorIdentityId = c.Guid(nullable: false),
                        FromActivityName = c.String(nullable: false),
                        ToActivityName = c.String(nullable: false),
                        ToStateName = c.String(),
                        TransitionTime = c.DateTime(nullable: false),
                        TransitionClassifier = c.String(nullable: false),
                        IsFinalised = c.Boolean(nullable: false),
                        FromStateName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkflowRuntime",
                c => new
                    {
                        RuntimeId = c.Guid(nullable: false),
                        Timer = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.RuntimeId);
            
            CreateTable(
                "dbo.WorkflowScheme",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Code = c.String(nullable: false, maxLength: 256),
                        Scheme = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WorkflowTransactionHistory",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ProcessIdentity = c.Guid(nullable: false),
                        UserID = c.Guid(),
                        AllowedToEmployeeNames = c.String(),
                        TransitionTime = c.DateTime(),
                        Order = c.Int(nullable: false),
                        TransitionTimeForSort = c.DateTime(),
                        InitialState = c.String(),
                        DestinationState = c.String(),
                        Command = c.String(),
                        WorkflowId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Workflows", t => t.WorkflowId, cascadeDelete: true)
                .Index(t => t.WorkflowId);
            
            CreateTable(
                "dbo.WorkflowTransitionUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TransactionId = c.Guid(nullable: false),
                        UserID = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WorkflowTransactionHistory", t => t.TransactionId, cascadeDelete: true)
                .Index(t => t.TransactionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WorkflowTransactionHistory", "WorkflowId", "dbo.Workflows");
            DropForeignKey("dbo.WorkflowTransitionUsers", "TransactionId", "dbo.WorkflowTransactionHistory");
            DropForeignKey("dbo.TestSummaryComments", "TestSummaryId", "dbo.TestSummary");
            DropForeignKey("dbo.TestSummaryAnswer", "TestSummaryId", "dbo.TestSummary");
            DropForeignKey("dbo.TestSummaryAnswer", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.Templates", "TemplateFormats_ID", "dbo.TemplateFormats");
            DropForeignKey("dbo.Settings", "SettingTypeID", "dbo.SettingTypes");
            DropForeignKey("dbo.Users", "GroupID", "dbo.Groups");
            DropForeignKey("dbo.RoleOptions", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.RoleOptions", "OptionID", "dbo.Options");
            DropForeignKey("dbo.Options", "TypeID", "dbo.OptionTypes");
            DropForeignKey("dbo.Options", "ParentID", "dbo.Options");
            DropForeignKey("dbo.GroupRoles", "RoleID", "dbo.Roles");
            DropForeignKey("dbo.GroupRoles", "GroupID", "dbo.Groups");
            DropForeignKey("dbo.Users", "ID", "dbo.AccountManagers");
            DropForeignKey("dbo.Telephones", "TypeID", "dbo.TelephoneTypes");
            DropForeignKey("dbo.Telephones", "AccountManagerID", "dbo.AccountManagers");
            DropForeignKey("dbo.AccountManagerRoles", "AccountRoleID", "dbo.AccountRoles");
            DropForeignKey("dbo.AccountManagerRoles", "AccountManagerID", "dbo.AccountManagers");
            DropForeignKey("dbo.WorkflowMaps", "WorkFlowTypeId", "dbo.Workflows");
            DropForeignKey("dbo.Activities", "WorkflowID", "dbo.Workflows");
            DropForeignKey("dbo.Activities", "StateID", "dbo.ActivityStates");
            DropForeignKey("dbo.Transitions", "WorkflowID", "dbo.Workflows");
            DropForeignKey("dbo.TransitionActors", "TransitionID", "dbo.Transitions");
            DropForeignKey("dbo.TransitionActors", "ActorID", "dbo.Actors");
            DropForeignKey("dbo.Transitions", "ToActivityID", "dbo.Activities");
            DropForeignKey("dbo.Transitions", "FromActivityID", "dbo.Activities");
            DropForeignKey("dbo.Transitions", "CommandID", "dbo.Commands");
            DropForeignKey("dbo.Transitions", "ActionID", "dbo.Actions");
            DropForeignKey("dbo.ActivityActions", "ActivityID", "dbo.Activities");
            DropForeignKey("dbo.ActivityActions", "ActionID", "dbo.Actions");
            DropForeignKey("dbo.CommandParameters", "ParameterID", "dbo.Parameters");
            DropForeignKey("dbo.CommandParameters", "CommandID", "dbo.Commands");
            DropForeignKey("dbo.ActionParameters", "ParameterID", "dbo.Parameters");
            DropForeignKey("dbo.ActionParameters", "ActionID", "dbo.Actions");
            DropForeignKey("dbo.WorkflowMaps", "WorkFlowId", "dbo.WorkflowProcessScheme");
            DropForeignKey("dbo.WorkflowMaps", "EntityId", "dbo.Entities");
            DropForeignKey("dbo.Notes", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.FundDetails", "TrusteeID", "dbo.AccountManagers");
            DropForeignKey("dbo.FundDetails", "OwnerID", "dbo.AccountManagers");
            DropForeignKey("dbo.FundDetails", "MainContactID", "dbo.AccountManagers");
            DropForeignKey("dbo.FundDetails", "ID", "dbo.Entities");
            DropForeignKey("dbo.Entities", "TypeID", "dbo.EntityTypes");
            DropForeignKey("dbo.EntityContacts", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.EntityContacts", "ContractID", "dbo.Contacts");
            DropForeignKey("dbo.Contacts", "TypeID", "dbo.ContactTypes");
            DropForeignKey("dbo.AccountManagerContacts", "ContactID", "dbo.Contacts");
            DropForeignKey("dbo.Entities", "CategoryID", "dbo.EntityCategories");
            DropForeignKey("dbo.ClientDetails", "ID", "dbo.Entities");
            DropForeignKey("dbo.ClientDetails", "ContractID", "dbo.Contracts");
            DropForeignKey("dbo.Entities", "ParentID", "dbo.Entities");
            DropForeignKey("dbo.AuditProcedureTab", "EntityId", "dbo.Entities");
            DropForeignKey("dbo.AuditProcedureTabContent", "AuditProcedureTabId", "dbo.AuditProcedureTab");
            DropForeignKey("dbo.AuditProcedure", "ParentId", "dbo.AuditProcedure");
            DropForeignKey("dbo.AuditProcedure", "AuditProcedureTabContentId", "dbo.AuditProcedureTabContent");
            DropForeignKey("dbo.AuditProcedureOption", "AuditProcedureOptionDetailId", "dbo.AuditProcedureOptionDetail");
            DropForeignKey("dbo.AuditProcedureOption", "AuditProcedureId", "dbo.AuditProcedure");
            DropForeignKey("dbo.AuditProcedureOml", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.AuditProcedureOml", "AuditProcedureId", "dbo.AuditProcedure");
            DropForeignKey("dbo.AuditProcedureColumn", "AuditProcedureId", "dbo.AuditProcedure");
            DropForeignKey("dbo.AuditProcedureAttachment", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.AuditProcedureAttachmentDetail", "AuditProcedureAttachmentId", "dbo.AuditProcedureAttachment");
            DropForeignKey("dbo.AuditProcedureAttachment", "AuditProcedureId", "dbo.AuditProcedure");
            DropForeignKey("dbo.AuditProcedureAnswer", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.JobWorkLogs", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.JobWorkLogs", "AccountManagerID", "dbo.AccountManagers");
            DropForeignKey("dbo.Jobs", "WorkflowStatusID", "dbo.WorkflowStatus");
            DropForeignKey("dbo.JobImportedData", "TemplateId", "dbo.Templates");
            DropForeignKey("dbo.Templates", "CategoryID", "dbo.TemplateCategories");
            DropForeignKey("dbo.JobImportedData", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.JobNotes", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.Jobs", "FundID", "dbo.Entities");
            DropForeignKey("dbo.AuditProcedureComment", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.AuditProcedureComment", "AuditProcedureId", "dbo.AuditProcedure");
            DropForeignKey("dbo.AccountManagerJobs", "JobRoleID", "dbo.AccountManagerJobRoles");
            DropForeignKey("dbo.AccountManagerJobs", "JobID", "dbo.Jobs");
            DropForeignKey("dbo.AccountManagerJobs", "AccountManagerID", "dbo.AccountManagers");
            DropForeignKey("dbo.AuditProcedureAnswer", "AuditProcedureId", "dbo.AuditProcedure");
            DropForeignKey("dbo.Addresses", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.States", "CountryID", "dbo.Countries");
            DropForeignKey("dbo.Addresses", "StateID", "dbo.States");
            DropForeignKey("dbo.Addresses", "CountryID", "dbo.Countries");
            DropForeignKey("dbo.Addresses", "TypeID", "dbo.AddressTypes");
            DropForeignKey("dbo.AccountManagers", "EntityID", "dbo.Entities");
            DropForeignKey("dbo.AccountManagerContacts", "AccountManagerID", "dbo.AccountManagers");
            DropIndex("dbo.WorkflowTransactionHistory", new[] { "WorkflowId" });
            DropIndex("dbo.WorkflowTransitionUsers", new[] { "TransactionId" });
            DropIndex("dbo.TestSummaryComments", new[] { "TestSummaryId" });
            DropIndex("dbo.TestSummaryAnswer", new[] { "TestSummaryId" });
            DropIndex("dbo.TestSummaryAnswer", new[] { "JobId" });
            DropIndex("dbo.Templates", new[] { "TemplateFormats_ID" });
            DropIndex("dbo.Settings", new[] { "SettingTypeID" });
            DropIndex("dbo.Users", new[] { "GroupID" });
            DropIndex("dbo.RoleOptions", new[] { "RoleID" });
            DropIndex("dbo.RoleOptions", new[] { "OptionID" });
            DropIndex("dbo.Options", new[] { "TypeID" });
            DropIndex("dbo.Options", new[] { "ParentID" });
            DropIndex("dbo.GroupRoles", new[] { "RoleID" });
            DropIndex("dbo.GroupRoles", new[] { "GroupID" });
            DropIndex("dbo.Users", new[] { "ID" });
            DropIndex("dbo.Telephones", new[] { "TypeID" });
            DropIndex("dbo.Telephones", new[] { "AccountManagerID" });
            DropIndex("dbo.AccountManagerRoles", new[] { "AccountRoleID" });
            DropIndex("dbo.AccountManagerRoles", new[] { "AccountManagerID" });
            DropIndex("dbo.WorkflowMaps", new[] { "WorkFlowTypeId" });
            DropIndex("dbo.Activities", new[] { "WorkflowID" });
            DropIndex("dbo.Activities", new[] { "StateID" });
            DropIndex("dbo.Transitions", new[] { "WorkflowID" });
            DropIndex("dbo.TransitionActors", new[] { "TransitionID" });
            DropIndex("dbo.TransitionActors", new[] { "ActorID" });
            DropIndex("dbo.Transitions", new[] { "ToActivityID" });
            DropIndex("dbo.Transitions", new[] { "FromActivityID" });
            DropIndex("dbo.Transitions", new[] { "CommandID" });
            DropIndex("dbo.Transitions", new[] { "ActionID" });
            DropIndex("dbo.ActivityActions", new[] { "ActivityID" });
            DropIndex("dbo.ActivityActions", new[] { "ActionID" });
            DropIndex("dbo.CommandParameters", new[] { "ParameterID" });
            DropIndex("dbo.CommandParameters", new[] { "CommandID" });
            DropIndex("dbo.ActionParameters", new[] { "ParameterID" });
            DropIndex("dbo.ActionParameters", new[] { "ActionID" });
            DropIndex("dbo.WorkflowMaps", new[] { "WorkFlowId" });
            DropIndex("dbo.WorkflowMaps", new[] { "EntityId" });
            DropIndex("dbo.Notes", new[] { "EntityID" });
            DropIndex("dbo.FundDetails", new[] { "TrusteeID" });
            DropIndex("dbo.FundDetails", new[] { "OwnerID" });
            DropIndex("dbo.FundDetails", new[] { "MainContactID" });
            DropIndex("dbo.FundDetails", new[] { "ID" });
            DropIndex("dbo.Entities", new[] { "TypeID" });
            DropIndex("dbo.EntityContacts", new[] { "EntityID" });
            DropIndex("dbo.EntityContacts", new[] { "ContractID" });
            DropIndex("dbo.Contacts", new[] { "TypeID" });
            DropIndex("dbo.AccountManagerContacts", new[] { "ContactID" });
            DropIndex("dbo.Entities", new[] { "CategoryID" });
            DropIndex("dbo.ClientDetails", new[] { "ID" });
            DropIndex("dbo.ClientDetails", new[] { "ContractID" });
            DropIndex("dbo.Entities", new[] { "ParentID" });
            DropIndex("dbo.AuditProcedureTab", new[] { "EntityId" });
            DropIndex("dbo.AuditProcedureTabContent", new[] { "AuditProcedureTabId" });
            DropIndex("dbo.AuditProcedure", new[] { "ParentId" });
            DropIndex("dbo.AuditProcedure", new[] { "AuditProcedureTabContentId" });
            DropIndex("dbo.AuditProcedureOption", new[] { "AuditProcedureOptionDetailId" });
            DropIndex("dbo.AuditProcedureOption", new[] { "AuditProcedureId" });
            DropIndex("dbo.AuditProcedureOml", new[] { "JobId" });
            DropIndex("dbo.AuditProcedureOml", new[] { "AuditProcedureId" });
            DropIndex("dbo.AuditProcedureColumn", new[] { "AuditProcedureId" });
            DropIndex("dbo.AuditProcedureAttachment", new[] { "JobId" });
            DropIndex("dbo.AuditProcedureAttachmentDetail", new[] { "AuditProcedureAttachmentId" });
            DropIndex("dbo.AuditProcedureAttachment", new[] { "AuditProcedureId" });
            DropIndex("dbo.AuditProcedureAnswer", new[] { "JobId" });
            DropIndex("dbo.JobWorkLogs", new[] { "JobID" });
            DropIndex("dbo.JobWorkLogs", new[] { "AccountManagerID" });
            DropIndex("dbo.Jobs", new[] { "WorkflowStatusID" });
            DropIndex("dbo.JobImportedData", new[] { "TemplateId" });
            DropIndex("dbo.Templates", new[] { "CategoryID" });
            DropIndex("dbo.JobImportedData", new[] { "JobId" });
            DropIndex("dbo.JobNotes", new[] { "JobID" });
            DropIndex("dbo.Jobs", new[] { "FundID" });
            DropIndex("dbo.AuditProcedureComment", new[] { "JobId" });
            DropIndex("dbo.AuditProcedureComment", new[] { "AuditProcedureId" });
            DropIndex("dbo.AccountManagerJobs", new[] { "JobRoleID" });
            DropIndex("dbo.AccountManagerJobs", new[] { "JobID" });
            DropIndex("dbo.AccountManagerJobs", new[] { "AccountManagerID" });
            DropIndex("dbo.AuditProcedureAnswer", new[] { "AuditProcedureId" });
            DropIndex("dbo.Addresses", new[] { "EntityID" });
            DropIndex("dbo.States", new[] { "CountryID" });
            DropIndex("dbo.Addresses", new[] { "StateID" });
            DropIndex("dbo.Addresses", new[] { "CountryID" });
            DropIndex("dbo.Addresses", new[] { "TypeID" });
            DropIndex("dbo.AccountManagers", new[] { "EntityID" });
            DropIndex("dbo.AccountManagerContacts", new[] { "AccountManagerID" });
            DropTable("dbo.WorkflowTransitionUsers");
            DropTable("dbo.WorkflowTransactionHistory");
            DropTable("dbo.WorkflowScheme");
            DropTable("dbo.WorkflowRuntime");
            DropTable("dbo.WorkflowProcessTransitionHistory");
            DropTable("dbo.WorkflowProcessInstanceStatus");
            DropTable("dbo.WorkflowProcessInstancePersistence");
            DropTable("dbo.WorkflowProcessInstance");
            DropTable("dbo.UserForgotPasswords");
            DropTable("dbo.TestSummaryComments");
            DropTable("dbo.TestSummaryAnswer");
            DropTable("dbo.TestSummary");
            DropTable("dbo.TemplateProviders");
            DropTable("dbo.TemplateFormats");
            DropTable("dbo.SettingTypes");
            DropTable("dbo.Settings");
            DropTable("dbo.Notifications");
            DropTable("dbo.OptionTypes");
            DropTable("dbo.Options");
            DropTable("dbo.RoleOptions");
            DropTable("dbo.Roles");
            DropTable("dbo.GroupRoles");
            DropTable("dbo.Groups");
            DropTable("dbo.Users");
            DropTable("dbo.TelephoneTypes");
            DropTable("dbo.Telephones");
            DropTable("dbo.AccountRoles");
            DropTable("dbo.AccountManagerRoles");
            DropTable("dbo.ActivityStates");
            DropTable("dbo.Actors");
            DropTable("dbo.TransitionActors");
            DropTable("dbo.Transitions");
            DropTable("dbo.Commands");
            DropTable("dbo.CommandParameters");
            DropTable("dbo.Parameters");
            DropTable("dbo.ActionParameters");
            DropTable("dbo.Actions");
            DropTable("dbo.ActivityActions");
            DropTable("dbo.Activities");
            DropTable("dbo.Workflows");
            DropTable("dbo.WorkflowProcessScheme");
            DropTable("dbo.WorkflowMaps");
            DropTable("dbo.Notes");
            DropTable("dbo.FundDetails");
            DropTable("dbo.EntityTypes");
            DropTable("dbo.ContactTypes");
            DropTable("dbo.Contacts");
            DropTable("dbo.EntityContacts");
            DropTable("dbo.EntityCategories");
            DropTable("dbo.Contracts");
            DropTable("dbo.ClientDetails");
            DropTable("dbo.AuditProcedureOptionDetail");
            DropTable("dbo.AuditProcedureOption");
            DropTable("dbo.AuditProcedureOml");
            DropTable("dbo.AuditProcedureColumn");
            DropTable("dbo.AuditProcedureAttachmentDetail");
            DropTable("dbo.AuditProcedureAttachment");
            DropTable("dbo.JobWorkLogs");
            DropTable("dbo.WorkflowStatus");
            DropTable("dbo.TemplateCategories");
            DropTable("dbo.Templates");
            DropTable("dbo.JobImportedData");
            DropTable("dbo.JobNotes");
            DropTable("dbo.AuditProcedureComment");
            DropTable("dbo.AccountManagerJobRoles");
            DropTable("dbo.AccountManagerJobs");
            DropTable("dbo.Jobs");
            DropTable("dbo.AuditProcedureAnswer");
            DropTable("dbo.AuditProcedure");
            DropTable("dbo.AuditProcedureTabContent");
            DropTable("dbo.AuditProcedureTab");
            DropTable("dbo.States");
            DropTable("dbo.Countries");
            DropTable("dbo.AddressTypes");
            DropTable("dbo.Addresses");
            DropTable("dbo.Entities");
            DropTable("dbo.AccountManagers");
            DropTable("dbo.AccountManagerContacts");
        }
    }
}
