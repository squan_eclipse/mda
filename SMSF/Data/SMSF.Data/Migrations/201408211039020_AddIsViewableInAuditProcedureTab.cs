namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsViewableInAuditProcedureTab : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AuditProcedureTabContent", "IsViewable", c => c.Boolean(nullable: false, defaultValue: true));
            AddColumn("dbo.AuditProcedureTab", "IsViewable", c => c.Boolean(nullable: false, defaultValue: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AuditProcedureTab", "IsViewable");
            DropColumn("dbo.AuditProcedureTabContent", "IsViewable");
        }
    }
}
