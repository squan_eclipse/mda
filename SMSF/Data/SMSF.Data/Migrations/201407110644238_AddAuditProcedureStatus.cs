using DBASystem.SMSF.Contracts.Common;

namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAuditProcedureStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AuditProcedure", "StatusID", c => c.Int(nullable: false, defaultValue: (int) Status.Active));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AuditProcedure", "StatusID");
        }
    }
}
