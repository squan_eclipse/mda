namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WorkflowCustomTransitions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Commands", "EntityId", c => c.Int());
            AddColumn("dbo.Transitions", "EntityId", c => c.Int());
            CreateIndex("dbo.Transitions", "EntityId");
            CreateIndex("dbo.Commands", "EntityId");
            AddForeignKey("dbo.Transitions", "EntityId", "dbo.Entities", "ID");
            AddForeignKey("dbo.Commands", "EntityId", "dbo.Entities", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Commands", "EntityId", "dbo.Entities");
            DropForeignKey("dbo.Transitions", "EntityId", "dbo.Entities");
            DropIndex("dbo.Commands", new[] { "EntityId" });
            DropIndex("dbo.Transitions", new[] { "EntityId" });
            DropColumn("dbo.Transitions", "EntityId");
            DropColumn("dbo.Commands", "EntityId");
        }
    }
}
