namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveRoleOptionID : DbMigration
    {
        public override void Up()
        {
            Sql("Delete from dbo.RoleOptions");
            DropPrimaryKey("dbo.RoleOptions", "PK_dbo.RoleOptions");
            DropColumn("dbo.RoleOptions", "ID");
            AddColumn("dbo.RoleOptions", "ID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.RoleOptions", "ID", "PK_dbo.RoleOptions");
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RoleOptions", "ID", c => c.Int(nullable: false, identity: true));
        }
    }
}
