namespace DBASystem.SMSF.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AuditProcedureCommentChanges : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AuditProcedureComment", "Text", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AuditProcedureComment", "Text", c => c.String());
        }
    }
}
