﻿using DBASystem.SMSF.Contracts.Common;
using System.Data.Entity.Infrastructure;

namespace DBASystem.SMSF.Data
{
    internal class MigrationsContextFactory : IDbContextFactory<SMSFContext>
    {
        public SMSFContext Create()
        {
            return new SMSFContext("SMSFEntities");
        }
    }
}
