﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data.Models;
using System;

namespace DBASystem.SMSF.Data
{
    public class UnitOfWork : IDisposable
    {
        private SMSFContext Context { get; set; }        
        private bool _disposed;

        public UnitOfWork()
        {
            Context = Context ?? new SMSFContext(Utility.GetConnectionString);
            //Context.Database.Log = Console.Write;
        }

        public void Commit()
        {
            try
            {
                Context.SaveChanges();
            }
            catch (Exception e)
            {
                Utility.Logger.Error(e);
                throw;
            }
        }

        public IRepository<T> Repository<T>() where T : BaseEntity
        {            
            return new Repository<T>(Context);
        }

        public IWorkflowRepository<T> WorkflowRepository<T>() where T : WorkflowBase
        {
            return new WorkflowRepository<T>(Context);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            _disposed = true;
        }
    }
}
