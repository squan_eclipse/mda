﻿using DBASystem.SMSF.Contracts.Common;
using DBASystem.SMSF.Data.Migrations;
using DBASystem.SMSF.Data.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using DBASystem.SMSF.Data.Models.WorkflowEngine;
using EntityCategories = DBASystem.SMSF.Data.Models.EntityCategories;
using EntityTypes = DBASystem.SMSF.Data.Models.EntityTypes;
using SettingTypes = DBASystem.SMSF.Data.Models.SettingTypes;

namespace DBASystem.SMSF.Data
{
    internal class SMSFContext : DbContext
    {
        public SMSFContext(string connectionString): base(connectionString)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SMSFContext, Configuration>(Utility.GetComputerName()));
            //Database.SetInitializer(new SMSFDbInitializer());
            //Database.Log = Console.Write;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            #region Setting up One to One Relationships

            #region Account Manager & User

            modelBuilder.Entity<Users>().HasKey(a => a.ID);
            modelBuilder.Entity<Users>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<Users>().HasRequired(a => a.AccountManager).WithRequiredDependent(a => a.User); 


            #endregion

            #region Entity & Client Detail

            modelBuilder.Entity<ClientDetail>().HasKey(a => a.ID);
            modelBuilder.Entity<ClientDetail>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<ClientDetail>().HasRequired(a => a.Entity).WithRequiredDependent(a => a.ClientDetail);

            #endregion

            #region Entity & Fund Detail

            modelBuilder.Entity<FundDetail>().HasKey(a => a.ID);
            modelBuilder.Entity<FundDetail>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<FundDetail>().HasRequired(a => a.Entity).WithRequiredDependent(a => a.FundDetail);

            #endregion

            #region Removing cascade

            modelBuilder.Entity<Jobs>().HasRequired(a => a.Fund).WithMany(a => a.Jobs).HasForeignKey(a => a.FundID).WillCascadeOnDelete(false);

            modelBuilder.Entity<FundDetail>().HasRequired(m => m.Owner).WithMany(t => t.Owners).HasForeignKey(m => m.OwnerID).WillCascadeOnDelete(false);
            modelBuilder.Entity<FundDetail>().HasRequired(m => m.Trustee).WithMany(t => t.Trustees).HasForeignKey(m => m.TrusteeID).WillCascadeOnDelete(false);
            modelBuilder.Entity<FundDetail>().HasRequired(m => m.MainContact).WithMany(t => t.MainContacts).HasForeignKey(m => m.MainContactID).WillCascadeOnDelete(false);

            modelBuilder.Entity<Transitions>().HasRequired(a => a.FromActivity).WithMany(a => a.FromTransitions).HasForeignKey(a => a.FromActivityID).WillCascadeOnDelete(false);
            modelBuilder.Entity<Transitions>().HasRequired(a => a.ToActivity).WithMany(a => a.ToTransitions).HasForeignKey(a => a.ToActivityID).WillCascadeOnDelete(false);

            #endregion

            #region Remove Database Generated Option

            modelBuilder.Entity<AuditProcedureTab>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<AuditProcedureTabContent>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<AuditProcedure>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<AuditProcedureOptionDetail>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<AuditProcedureOption>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<AuditProcedureColumn>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<TestSummary>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<Options>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            modelBuilder.Entity<RoleOptions>().Property(a => a.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            #endregion

            #endregion

            base.OnModelCreating(modelBuilder);
        }

        #region DbSets

        public DbSet<AccountManagerJobs> AccountManagerJobs { get; set; }
        public DbSet<AccountManagerRoles> AccountManagerRoles { get; set; }
        public DbSet<AccountManagers> AccountManagers { get; set; }
        public DbSet<AccountRoles> AccountRoles { get; set; }
        public DbSet<Addresses> Addresses { get; set; }
        public DbSet<AddressTypes> AddressTypes { get; set; }
        public DbSet<Countries> Countries { get; set; }
        public DbSet<Entities> Entities { get; set; }
        public DbSet<EntityCategories> EntityCategories { get; set; }
        public DbSet<EntityTypes> EntityTypes { get; set; }
        public DbSet<GroupRoles> GroupRoles { get; set; }
        public DbSet<Groups> Groups { get; set; }
        public DbSet<Jobs> Job { get; set; }
        public DbSet<AuditProcedureTab> AuditProcedureTab { get; set; }
        public DbSet<AuditProcedureTabContent> AuditProcedureTabContent { get; set; }
        public DbSet<AuditProcedure> AuditProcedure { get; set; }
        public DbSet<AuditProcedureOption> AuditProcedureOption { get; set; }
        public DbSet<AuditProcedureColumn> AuditProcedureColumn { get; set; }
        public DbSet<AuditProcedureAnswer> AuditProcedureAnswer { get; set; }
        public DbSet<AuditProcedureComment> AuditProcedureComment { get; set; }
        public DbSet<AuditProcedureOml> AuditProcedureOml { get; set; }
        public DbSet<AuditProcedureOptionDetail> AuditProcedureOptionDetail { get; set; }
        public DbSet<JobImportedData> JobImportedData { get; set; }
        public DbSet<TestSummary> TestSummary { get; set; }
        public DbSet<TestSummaryComment> TestSummaryComment { get; set; }
        public DbSet<Notifications> Notifications { get; set; }
        public DbSet<Options> Options { get; set; }
        public DbSet<OptionTypes> OptionTypes { get; set; }
        public DbSet<RoleOptions> RoleOptions { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<SettingTypes> SettingTypes { get; set; }
        public DbSet<States> States { get; set; }
        public DbSet<Telephones> Telephones { get; set; }
        public DbSet<TelephoneTypes> TelephoneTypes { get; set; }
        public DbSet<UserForgotPassword> UserForgotPassword { get; set; }
        public DbSet<Users> Users { get; set; }
        public DbSet<Models.Contracts> Contracts { get; set; }
        public DbSet<AccountManagerContacts> AccountManagerContacts { get; set; }
        public DbSet<ClientDetail> ClientDetail { get; set; }
        public DbSet<Contacts> Contacts { get; set; }
        public DbSet<ContactTypes> ContactTypes { get; set; }
        public DbSet<FundDetail> FundDetail { get; set; }
        public DbSet<Notes> Notes { get; set; }
        public DbSet<TemplateProvider> TemplateProvider { get; set; }
        public DbSet<TemplateCategories> TemplateCategories { get; set; }
        public DbSet<TemplateFormats> TemplateFormats { get; set; }
        public DbSet<Templates> Templates { get; set; }
        public DbSet<AccountManagerJobRoles> JobManagerRoles { get; set; }
        public DbSet<WorkflowStatus> WorkflowStatus { get; set; }
        public DbSet<JobWorkLogs> JobWorkLogs { get; set; }
        public DbSet<EntityAuditProcedureTab> EntityAuditProcedureTabs { get; set; }
        public DbSet<EntityAuditProcedureTabContent> EntityAuditProcedureTabContents { get; set; }
        public DbSet<AuditProcedureTemplate> AuditProcedureTemplates { get; set; }
        public DbSet<AuditProcedureTabTemplateDetail> AuditProcedureTabTemplateDetails { get; set; }
        public DbSet<AuditProcedureTabContentTemplateDetail> AuditProcedureTabContentTemplateDetails { get; set; }

        #endregion

        #region Workflow

        public DbSet<ActionParameters> ActionParameters { get; set; }
        public DbSet<Actions> Actions { get; set; }
        public DbSet<Activities> Activities { get; set; }
        public DbSet<ActivityActions> ActivityActions { get; set; }
        public DbSet<CommandParameters> CommandParameters { get; set; }
        public DbSet<Commands> Commands { get; set; }
        public DbSet<Parameters> Parameters { get; set; }
        public DbSet<TransitionActors> TransitionActors { get; set; }
        public DbSet<ActivityState> ActivityStates { get; set; }
        public DbSet<Transitions> Transitions { get; set; }
        public DbSet<Workflow> Workflow { get; set; }
        public DbSet<WorkflowTransactionHistory> WorkflowTransitionHistory { get; set; }
        public DbSet<WorkflowTransitionUsers> WorkflowTransitionUsers { get; set; }
        public DbSet<WorkflowMap> WorkflowMapping { get; set; }
        public DbSet<Actors> Actors { get; set; }
        
        #endregion

        #region Workflow Engine
        public DbSet<WorkflowProcessInstance> WorkflowProcessInstance { get; set; }
        public DbSet<WorkflowProcessInstancePersistence> WorkflowProcessInstancePersistence { get; set; }
        public DbSet<WorkflowProcessInstanceStatus> WorkflowProcessInstanceStatus { get; set; }
        public DbSet<WorkflowProcessSchemes> WorkflowProcessScheme { get; set; }
        public DbSet<WorkflowProcessTransitionHistories> WorkflowProcessTransitionHistory { get; set; }
        public DbSet<WorkflowRuntimes> WorkflowRuntime { get; set; }
        public DbSet<WorkflowSchemes> WorkflowScheme { get; set; }

        public DbSet<WorkflowInbox> WorkflowInbox { get; set; }

        #endregion
    }
}