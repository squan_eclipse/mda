﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class JobNotes : AuditEntity
    {
        [StringLength(500)]
        [Required]
        public string Note { get; set; }

        public int JobID { get; set; }


        [ForeignKey("JobID")]
        public virtual Jobs Job { get; set; }
    }

}
