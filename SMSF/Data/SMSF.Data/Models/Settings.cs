﻿using DBASystem.SMSF.Contracts.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Settings : AuditEntity
    {
        public int SettingTypeID { get; set; }
        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        [StringLength(250)]
        public string SettingValue { get; set; }
        public Status StatusID { get; set; }


        [ForeignKey("SettingTypeID")]
        public virtual SettingTypes SettingType { get; set; }
    }
}
