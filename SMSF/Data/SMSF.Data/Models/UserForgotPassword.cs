﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class UserForgotPassword : AuditEntity
    {
        public int UserID { get; set; }
        public Guid ForgotPasswordToken { get; set; }
        [Required]
        [StringLength(100)]
        public string NewPassword { get; set; }
        public Status StatusID { get; set; }
    }
}
