﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class GroupRoles : BaseEntity
    {
        public int GroupID { get; set; }
        public int RoleID { get; set; }


        [ForeignKey("GroupID")]
        public virtual Groups Group { get; set; }
        [ForeignKey("RoleID")]
        public virtual Roles Role { get; set; }
    }
}
