﻿
using System.ComponentModel.DataAnnotations;
namespace DBASystem.SMSF.Data.Models
{
    public class Notifications : AuditEntity
    {
        public int Type { get; set; }
        [Required]
        [StringLength(500)]
        public string Message { get; set; }
        public string MessageDetail { get; set; }
        [StringLength(50)]
        public string EntityType { get; set; }
        public int? ReleventID { get; set; }
        public bool IsSystem { get; set; }
    }
}
