﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DBASystem.SMSF.Data.Models
{
    public class EntityContacts : BaseEntity
    {
        public EntityContacts()
        {

        }
        public int EntityID { get; set; }
        public int ContractID { get; set; }

        [ForeignKey("EntityID")]
        public virtual Entities Entity { get; set; }
        [ForeignKey("ContractID")]
        public virtual Contacts Contact { get; set; }
    }
}
