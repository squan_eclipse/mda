﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Contracts : BaseEntity
    {
        public Contracts()
        {
            this.Clients = new HashSet<ClientDetail>();
        }

        [StringLength(50)]
        public string Description { get; set; }
        public Status StatusID { get; set; }

        public virtual ICollection<ClientDetail> Clients { get; set; }
    }
}
