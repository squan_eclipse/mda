﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Entities : AuditEntity
    {
        public Entities()
        {
            AccountManagers = new HashSet<AccountManagers>();
            Addresses = new HashSet<Addresses>();
            ChildEntities = new HashSet<Entities>();
            Jobs = new HashSet<Jobs>();
            AuditProcedureTabs = new HashSet<AuditProcedureTab>();
            Notes = new HashSet<Notes>();
            EntityContacts = new HashSet<EntityContacts>();
            WorkflowMap = new HashSet<WorkflowMap>();
            WorkFlowTransitions = new HashSet<Transitions>();
            TransitionCommands = new HashSet<Commands>();
            EntityAuditProcedureTabs = new HashSet<EntityAuditProcedureTab>();
            AuditProcedureTabContents = new HashSet<AuditProcedureTabContent>();
            EntityAuditProcedureTabContents = new HashSet<EntityAuditProcedureTabContent>();
            EntityAuditProcedureTabTemplate = new HashSet<AuditProcedureTemplate>();
        }

        public Guid? Identifier { get; set; }

        public int TypeID { get; set; }

        public int? CategoryID { get; set; }

        [StringLength(50)]
        public string ABN { get; set; }

        [StringLength(50)]
        public string ACN { get; set; }

        [StringLength(50)]
        public string GST { get; set; }

        [StringLength(50)]
        public string TFN { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        public string Note { get; set; }

        public int? ParentID { get; set; }

        public int? PrimaryAccountManagerID { get; set; }

        public int? SecondaryAccountManagerID { get; set; }

        public Status StatusID { get; set; }

        [ForeignKey("TypeID")]
        public virtual EntityTypes EntityType { get; set; }
        [ForeignKey("CategoryID")]
        public virtual EntityCategories EntityCategory { get; set; }
        [ForeignKey("ParentID")]
        public virtual Entities ParentEntity { get; set; }
        public virtual ICollection<AccountManagers> AccountManagers { get; set; }
        public virtual ICollection<Addresses> Addresses { get; set; }
        public virtual ICollection<Entities> ChildEntities { get; set; }
        public virtual ICollection<Jobs> Jobs { get; set; }
        public virtual ICollection<AuditProcedureTab> AuditProcedureTabs { get; set; }
        public virtual ICollection<Notes> Notes { get; set; }
        public virtual ClientDetail ClientDetail { get; set; }
        public virtual FundDetail FundDetail { get; set; }
        public virtual ICollection<EntityContacts> EntityContacts { get; set; }
        public virtual ICollection<WorkflowMap> WorkflowMap { get; set; }
        public virtual ICollection<Transitions> WorkFlowTransitions { get; set; }
        public virtual ICollection<Commands> TransitionCommands { get; set; }
        public virtual ICollection<EntityAuditProcedureTab> EntityAuditProcedureTabs { get; set; }
        public virtual ICollection<AuditProcedureTabContent> AuditProcedureTabContents { get; set; }
        public virtual ICollection<EntityAuditProcedureTabContent> EntityAuditProcedureTabContents { get; set; }
        public virtual ICollection<AuditProcedureTemplate> EntityAuditProcedureTabTemplate { get; set; }
    }
}
