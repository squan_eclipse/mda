﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class AccountManagerRoles : BaseEntity
    {
        public int AccountManagerID { get; set; }
        public int AccountRoleID { get; set; }


        [ForeignKey("AccountManagerID")]
        public virtual AccountManagers AccountManager { get; set; }

        [ForeignKey("AccountRoleID")]
        public virtual AccountRoles AccountRole { get; set; }
    }
}
