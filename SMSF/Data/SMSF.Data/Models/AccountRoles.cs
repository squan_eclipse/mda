﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class AccountRoles : BaseEntity
    {
        public AccountRoles()
        {
            this.AccountManagerRoles = new HashSet<AccountManagerRoles>();
        }

        [Required]
        public string Name { get; set; }
        public Status StatusID { get; set; }

        public WorkflowActorType WorkflowType { get; set; }
        
        public virtual ICollection<AccountManagerRoles> AccountManagerRoles { get; set; }
    }
}
