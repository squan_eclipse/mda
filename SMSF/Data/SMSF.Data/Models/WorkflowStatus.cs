﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class WorkflowStatus : BaseEntity
    {
        public WorkflowStatus()
        {
            this.Jobs = new HashSet<Jobs>();
        }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public virtual ICollection<Jobs> Jobs { get; set; }
    }
}
