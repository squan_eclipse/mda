﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class SettingTypes : BaseEntity
    {
        public SettingTypes()
        {
            this.Settings = new HashSet<Settings>();
        }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        public Status StatusID { get; set; }


        public virtual ICollection<Settings> Settings { get; set; }
    }
}
