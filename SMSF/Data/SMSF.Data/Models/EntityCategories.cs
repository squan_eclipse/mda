﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class EntityCategories : BaseEntity
    {
        public EntityCategories()
        {
            this.Entities = new HashSet<Entities>();
        }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        public Status StatusID { get; set; }


        public virtual ICollection<Entities> Entities { get; set; }
    }
}
