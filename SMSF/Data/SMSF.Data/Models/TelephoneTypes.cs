﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class TelephoneTypes : BaseEntity
    {
        public TelephoneTypes()
        {
            this.Telephones = new HashSet<Telephones>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public Status StatusID { get; set; }


        public virtual ICollection<Telephones> Telephones { get; set; }
    }
}
