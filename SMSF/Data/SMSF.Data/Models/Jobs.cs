﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Jobs : AuditEntity
    {
        public Jobs()
        {
            AccountManagerJobs = new HashSet<AccountManagerJobs>();
            JobsImportedData = new HashSet<JobImportedData>();
            JobNotes = new HashSet<JobNotes>();
            AuditComments = new HashSet<AuditProcedureComment>();
        }

        public int FundID { get; set; }
        [Required]
        [StringLength(100)]
        public string Title { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? CompleteDate { get; set; }
        public int? WorkingHours { get; set; }
        [StringLength(100)]
        public string DataLink { get; set; }
        public Status StatusID { get; set; }
        public Guid? Identifier { get; set; }
        public int WorkflowStatusID { get; set; }
        public int? EstimatedHours { get; set; }


        [ForeignKey("FundID")]
        public virtual Entities Fund { get; set; }
        public virtual ICollection<AccountManagerJobs> AccountManagerJobs { get; set; }
        public virtual ICollection<JobImportedData> JobsImportedData { get; set; }
        public virtual ICollection<JobNotes> JobNotes { get; set; }
        [ForeignKey("WorkflowStatusID")]
        public virtual WorkflowStatus WorkflowStatus { get; set; }
        public virtual ICollection<JobWorkLogs> WorkLogs { get; set; }
        public virtual ICollection<AuditProcedureComment> AuditComments { get; set; }
    }
}