﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Options : AuditEntity
    {
        public Options()
        {
            this.ChildOptions = new HashSet<Options>();
            this.RoleOptions = new HashSet<RoleOptions>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(200)]
        public string PageURL { get; set; }
        [StringLength(200)]
        public string NextPageURL { get; set; }
        public int TypeID { get; set; }
        public int? ParentID { get; set; }
        public bool AllowAnonymous { get; set; }
        public Status StatusID { get; set; }


        [ForeignKey("TypeID")]
        public virtual OptionTypes OptionType { get; set; }
        [ForeignKey("ParentID")]
        public virtual Options Parent { get; set; }
        public virtual ICollection<Options> ChildOptions { get; set; }
        public virtual ICollection<RoleOptions> RoleOptions { get; set; }
    }
}
