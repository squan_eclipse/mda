﻿using DBASystem.SMSF.Contracts.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("JobImportedData")]
    public class JobImportedData : AuditEntity
    {
        public int JobId { get; set; }
        public int TemplateId { get; set; }
        [Required]
        public WorkpaperContentType ControlType { get; set; }
        [Column(TypeName = "xml")]
        public string XmlData { get; set; }
        [MaxLength(200)]
        [Required]
        public string Filename { get; set; }

        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }
        [ForeignKey("TemplateId")]
        public virtual Templates Template { get; set; }

    }
}
