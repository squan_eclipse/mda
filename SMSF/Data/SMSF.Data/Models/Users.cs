﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Users : AuditEntity
    {
        public Guid? Identifier { get; set; }
        public int GroupID { get; set; }
        [Required]
        [StringLength(100)]
        public string Email { get; set; }
        [Required]
        [StringLength(100)]
        public string Password { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public Status StatusID { get; set; }
        public int? FailedPasswordAttemptCount { get; set; }
        public DateTime? FailedPasswordAttemptStart { get; set; }
        public byte[] UserImage { get; set; }


        [ForeignKey("ID")]
        public virtual AccountManagers AccountManager { get; set; }
        [ForeignKey("GroupID")]
        public virtual Groups Group { get; set; }
    }
}
