﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class RoleOptions : AuditEntity
    {
        public int RoleID { get; set; }
        public int OptionID { get; set; }
        public bool IsAssigned { get; set; }
        public bool IsSystem { get; set; }


        [ForeignKey("RoleID")]
        public virtual Roles Role { get; set; }
        [ForeignKey("OptionID")]
        public virtual Options Option { get; set; }
    }
}
