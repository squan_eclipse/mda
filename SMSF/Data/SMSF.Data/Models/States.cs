﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class States : BaseEntity
    {
        public States()
        {
            this.Addresses = new HashSet<Addresses>();
          
        }

        public int CountryID { get; set; }
        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        [StringLength(10)]
        public string Code { get; set; }


        [ForeignKey("CountryID")]
        public virtual Countries Country { get; set; }
        public virtual ICollection<Addresses> Addresses { get; set; }
       
    }
}
