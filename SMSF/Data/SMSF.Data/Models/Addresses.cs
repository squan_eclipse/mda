﻿using DBASystem.SMSF.Contracts.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Addresses : AuditEntity
    {
        public int EntityID { get; set; }
        public int TypeID { get; set; }
        [StringLength(50)]
        public string UnitNumber { get; set; }
        [StringLength(50)]
        public string StreetNumber { get; set; }
        [StringLength(50)]
        public string StreetName { get; set; }
        [StringLength(50)]
        public string StreetType { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        public int? StateID { get; set; }
        public int? CountryID { get; set; }
        [StringLength(50)]
        public string PostCode { get; set; }
        public bool Default { get; set; }
        public Status StatusID { get; set; }


        [ForeignKey("EntityID")]
        public virtual Entities Entity { get; set; }

        [ForeignKey("TypeID")]
        public virtual AddressTypes AddressType { get; set; }

        [ForeignKey("CountryID")]
        public virtual Countries Country { get; set; }

        [ForeignKey("StateID")]
        public virtual States State { get; set; }
    }
}
