﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureTabContent")]
    public class AuditProcedureTabContent : BaseEntity
    {
        public AuditProcedureTabContent()
        {
            AuditProcedure = new HashSet<AuditProcedure>();
            EntityAuditProcedureTabContents = new HashSet<EntityAuditProcedureTabContent>();
        }

        public int AuditProcedureTabId { get; set; }
        [Required]
        [StringLength(256)]
        public string Caption { get; set; }                
        public WorkpaperContentType ContentType { get; set; }
        public ControlType? ControlType { get; set; }
        public int? TemplateId { get; set; }
        public int Order { get; set; }
        public bool IsActive { get; set; }
        public int? EntityId { get; set; }
        public bool IsViewable { get; set; }

        [ForeignKey("EntityId")]
        public virtual Entities Entity { get; set; }
        [ForeignKey("AuditProcedureTabId")]
        public virtual AuditProcedureTab AuditProcedureTab { get; set; }
        public virtual ICollection<AuditProcedure> AuditProcedure { get; set; }
        public virtual ICollection<EntityAuditProcedureTabContent> EntityAuditProcedureTabContents { get; set; }
    }
}
