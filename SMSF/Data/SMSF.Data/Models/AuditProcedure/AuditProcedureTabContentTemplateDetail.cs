﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class AuditProcedureTabContentTemplateDetail : BaseEntity
    {
        public int AuditProcedureTemplateID { get; set; }
        public int AuditProcedureTabContentID { get; set; }

        [ForeignKey("AuditProcedureTabContentID")]
        public virtual AuditProcedureTabContent AuditProcedureTabContent { get; set; }
        [ForeignKey("AuditProcedureTemplateID")]
        public virtual AuditProcedureTemplate AuditProcedureTemplate { get; set; }
    }
}
