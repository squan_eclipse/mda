﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class EntityAuditProcedureTab : BaseEntity
    {
        public int AuditProcedureTabID { get; set; }
        public int EntityID { get; set; }

        [ForeignKey("AuditProcedureTabID")]
        public virtual AuditProcedureTab AuditProcedureTab { get; set; }
        [ForeignKey("EntityID")]
        public virtual Entities Entity { get; set; }
    }
}
