﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("TestSummaryAnswer")]
    public class TestSummaryAnswer : BaseEntity
    {
        public int JobId { get; set; }
        public int TestSummaryId { get; set; }
        public bool? IsCompleted { get; set; }

        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }

        [ForeignKey("TestSummaryId")]
        public virtual TestSummary TestSummary { get; set; }
    }
}
