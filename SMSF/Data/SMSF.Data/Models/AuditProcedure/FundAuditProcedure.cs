﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("FundAuditProcedure")]
    public class FundAuditProcedure : BaseEntity
    {
        public FundAuditProcedure()
        {
            FundAuditProcedureAttachment = new HashSet<FundAuditProcedureAttachment>();
            FundAuditProcedureOml = new HashSet<FundAuditProcedureOml>();
            FundAuditProcedureComment = new HashSet<FundAuditProcedureComment>();
        }

        public int FundId { get; set; }
        public int AuditProcedureId { get; set; }
        public ControlType AnswerControlType { get; set; }
        public string AnswerControlValue { get; set; }
        public bool Checkbox { get; set; }

        [ForeignKey("FundId")]
        public virtual Entities Fund { get; set; }
        public virtual ICollection<FundAuditProcedureAttachment> FundAuditProcedureAttachment { get; set; }
        public virtual ICollection<FundAuditProcedureOml> FundAuditProcedureOml { get; set; }
        public virtual ICollection<FundAuditProcedureComment> FundAuditProcedureComment { get; set; }
        
    }

    public class FundAuditProcedureAttachment : BaseEntity
    {
        public int FundAuditProcedureId { get; set; }

        [ForeignKey("FundAuditProcedureId")]
        public virtual FundAuditProcedure FundAuditProcedure { get; set; }
    }
    public class FundAuditProcedureOml : BaseEntity
    {
        public int FundAuditProcedureId { get; set; }

        [ForeignKey("FundAuditProcedureId")]
        public virtual FundAuditProcedure FundAuditProcedure { get; set; }
    }
    public class FundAuditProcedureComment : BaseEntity
    {
        public int FundAuditProcedureId { get; set; }

        [ForeignKey("FundAuditProcedureId")]
        public virtual FundAuditProcedure FundAuditProcedure { get; set; }
    }

}