﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureOption")]
    public class AuditProcedureOption: BaseEntity
    {
        public int AuditProcedureId { get; set; }
        public int AuditProcedureOptionDetailId { get; set; }
        
        [ForeignKey("AuditProcedureId")]
        public virtual AuditProcedure AuditProcedure { get; set; }
        [ForeignKey("AuditProcedureOptionDetailId")]
        public virtual AuditProcedureOptionDetail AuditProcedureOptionDetail { get; set; }
    }
}
