﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureColumn")]
    public class AuditProcedureColumn : BaseEntity
    {
        public int AuditProcedureId { get; set; }
        public string ColumnName { get; set; }
        public string ColumnValue { get; set; }
        [ForeignKey("AuditProcedureId")]
        public virtual AuditProcedure AuditProcedure { get; set; }
    }
}
