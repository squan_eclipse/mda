﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureAttachmentDetail")]
    public class AuditProcedureAttachmentDetail : BaseEntity
    {
        public string FileName { get; set; }
        public int AuditProcedureAttachmentId { get; set; }
        [ForeignKey("AuditProcedureAttachmentId")]
        public virtual AuditProcedureAttachment Attachment { get; set; }
    }
}
