﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class EntityAuditProcedureTabContent : BaseEntity
    {
        public int AuditProcedureTabContentID { get; set; }
        public int EntityID { get; set; }

        [ForeignKey("AuditProcedureTabContentID")]
        public virtual AuditProcedureTabContent AuditProcedureTabContent { get; set; }
        [ForeignKey("EntityID")]
        public virtual Entities Entity { get; set; }
    }
}
