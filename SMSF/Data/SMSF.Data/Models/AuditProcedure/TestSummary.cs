﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("TestSummary")]
    public class TestSummary : BaseEntity
    {
        public TestSummary() {
            TestSummaryComment = new HashSet<TestSummaryComment>();
        }
        //public int JobId { get; set; }
        public string TestName { get; set; }
        public string Description { get; set; }
        //public bool? IsCompleted { get; set; }
        public virtual ICollection<TestSummaryComment> TestSummaryComment { get; set; }
        public virtual ICollection<TestSummaryAnswer> TestSummaryAnswer { get; set; }
        
    }

    public class TestSummaryComment:BaseEntity
    {
        public string TestComments { get; set; }
        public int TestSummaryId { get; set; }
        [ForeignKey("TestSummaryId")]
        public virtual TestSummary TestSummary { get; set; }
    }

}
