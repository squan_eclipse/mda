﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureTab")]
    public class AuditProcedureTab : BaseEntity
    {
        public AuditProcedureTab()
        {
            AuditProcedureTabContent = new HashSet<AuditProcedureTabContent>();
            EntityAuditProcedureTabs = new HashSet<EntityAuditProcedureTab>();
        }

        [Required]
        [StringLength(50)]
        public string Caption { get; set; }
        public int TabOrder { get; set; }
        public int? EntityId { get; set; }
        public WorkpaperContentType ContentType { get; set; }
        public bool IsActive { get; set; }
        public bool IsViewable { get; set; }

        [ForeignKey("EntityId")]
        public virtual Entities Entity { get; set; }
        public virtual ICollection<AuditProcedureTabContent> AuditProcedureTabContent { get; set; }
        public virtual ICollection<EntityAuditProcedureTab> EntityAuditProcedureTabs { get; set; }
    }
}