﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureAttachment")]
    public class AuditProcedureAttachment : BaseEntity
    {
        public int AuditProcedureId { get; set; }

        [ForeignKey("AuditProcedureId")]
        public virtual AuditProcedure AuditProcedure { get; set; }
        public int JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }
        public virtual ICollection<AuditProcedureAttachmentDetail> AuditProcedureAttachmentDetail { get; set; }
    }
}
