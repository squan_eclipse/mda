﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureOml")]
    public class AuditProcedureOml : BaseEntity
    {
        public string CustomOption { get; set; }
        public bool NonFundamental { get; set; }
        public int AuditProcedureId { get; set; }
        [ForeignKey("AuditProcedureId")]
        public virtual AuditProcedure AuditProcedure { get; set; }
        public int JobId { get; set; }
        public string Item { get; set; }
        public string StaffComments { get; set; }
        public string Initials { get; set; }
        public bool IsCleared { get; set; }
        public bool IsManagerApproved { get; set; }
        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }
    }
}
