﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class AuditProcedureTabTemplateDetail : BaseEntity
    {
        public int AuditProcedureTemplateID { get; set; }
        public int AuditProcedureTabID { get; set; }

        [ForeignKey("AuditProcedureTabID")]
        public virtual AuditProcedureTab AuditProcedureTab { get; set; }
        [ForeignKey("AuditProcedureTemplateID")]
        public virtual AuditProcedureTemplate AuditProcedureTemplate { get; set; }
    }
}
