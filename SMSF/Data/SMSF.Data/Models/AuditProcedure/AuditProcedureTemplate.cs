﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class AuditProcedureTemplate : AuditEntity
    {
        public AuditProcedureTemplate()
        {
            AuditProcedureTabTemplateDetails = new HashSet<AuditProcedureTabTemplateDetail>();
            AuditProcedureTabContentTemplateDetails = new HashSet<AuditProcedureTabContentTemplateDetail>();
        }

        public int EntityID { get; set; }
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }
        
        [ForeignKey("EntityID")]
        public virtual Entities Entity { get; set; }
        public virtual ICollection<AuditProcedureTabTemplateDetail> AuditProcedureTabTemplateDetails { get; set; }
        public virtual ICollection<AuditProcedureTabContentTemplateDetail> AuditProcedureTabContentTemplateDetails { get; set; }
    }
}
