﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedure")]
    public class AuditProcedure : BaseEntity
    {
        public AuditProcedure()
        {
            AuditProcedureOption = new HashSet<AuditProcedureOption>();
            AuditProcedureColumn = new HashSet<AuditProcedureColumn>();
        }

        public int AuditProcedureTabContentId { get; set; }
        public int? ParentId { get; set; }
        public string Title { get; set; }
        public bool Attachments { get; set; }
        public bool Oml { get; set; }
        public bool Comments { get; set; }
        public ControlType? ControlType { get; set; }
        public ControlType? AnswerControlType { get; set; }
        public bool Checkbox { get; set; }
        public Status StatusID { get; set; }

        [ForeignKey("ParentId")]
        public virtual AuditProcedure Parent { get; set; }
        [ForeignKey("AuditProcedureTabContentId")]
        public virtual AuditProcedureTabContent AuditProcedureTabContent { get; set; }
        public virtual ICollection<AuditProcedureOption> AuditProcedureOption { get; set; }
        public virtual ICollection<AuditProcedureColumn> AuditProcedureColumn { get; set; }
        public virtual ICollection<AuditProcedure> Childs { get; set; }
        public virtual ICollection<AuditProcedureAnswer> AuditProcedureAnswer { get; set; }
        public virtual ICollection<AuditProcedureComment> AuditProcedureComment { get; set; }
        public virtual ICollection<AuditProcedureOml> AuditProcedureOml { get; set; }
        public virtual ICollection<AuditProcedureAttachment> AuditProcedureAttachment { get; set; }
    }
}
