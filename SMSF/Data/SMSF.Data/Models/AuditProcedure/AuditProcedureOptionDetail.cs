﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureOptionDetail")]
    public class AuditProcedureOptionDetail : BaseEntity
    {
        public AuditProcedureOptionDetail()
        {
            AuditProcedureOption = new HashSet<AuditProcedureOption>();
        }

        public string Field { get; set; }
        public string Value { get; set; }

        public virtual ICollection<AuditProcedureOption> AuditProcedureOption { get; set; }
    }
}
