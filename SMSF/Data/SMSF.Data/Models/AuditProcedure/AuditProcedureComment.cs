﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureComment")]
    public class AuditProcedureComment : AuditEntity
    {
        public AuditProcedureComment()
        {
            ChildComments = new HashSet<AuditProcedureComment>();
        }
        [Required]
        public string Text { get; set; }
        public int AuditProcedureId { get; set; }
        public int JobId { get; set; }
        public int? ParentID { get; set; }

        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }
        [ForeignKey("AuditProcedureId")]
        public virtual AuditProcedure AuditProcedure { get; set; }
        [ForeignKey("ParentID")]
        public virtual AuditProcedureComment ParentComment { get; set; }
        public virtual ICollection<AuditProcedureComment> ChildComments { get; set; }
    }
}
