﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("AuditProcedureAnswer")]
    public class AuditProcedureAnswer : BaseEntity
    {
        public string Text { get; set; }
        public bool? IsChecked { get; set; }
        public int AuditProcedureId { get; set; }
        [ForeignKey("AuditProcedureId")]
        public virtual AuditProcedure AuditProcedure { get; set; }
        public int JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }
    }
}
