﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class OptionTypes : BaseEntity
    {
        public OptionTypes()
        {
            this.Options = new HashSet<Options>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public Status StatusID { get; set; }


        public virtual ICollection<Options> Options { get; set; }
    }
}
