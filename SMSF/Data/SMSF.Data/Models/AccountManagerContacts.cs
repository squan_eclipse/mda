﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DBASystem.SMSF.Data.Models
{
    public class AccountManagerContacts:BaseEntity
    {
        public int AccountManagerID { get; set; }
        public int ContactID { get; set; }

        [ForeignKey("AccountManagerID")]
        public virtual AccountManagers AccountManager { get; set; }
        [ForeignKey("ContactID")]
        public virtual Contacts Contacts { get; set; }
    }
}
