﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class AddressTypes : BaseEntity
    {
        public AddressTypes()
        {
            this.Addresses = new HashSet<Addresses>();
        }

        [Required]
        [StringLength(50)]
        public string Description { get; set; }
        public Status StatusID { get; set; }


        public virtual ICollection<Addresses> Addresses { get; set; }
    }
}
