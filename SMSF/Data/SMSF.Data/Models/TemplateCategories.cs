﻿using DBASystem.SMSF.Contracts.Common;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class TemplateCategories : BaseEntity
    {
        public TemplateCategories()
        { }
        [Required]
        [MaxLength(50)]
        public string Name { set; get; }
        public int TemplateProviderID { set; get; }
        public Status StatusID { set; get; }
    }
}
