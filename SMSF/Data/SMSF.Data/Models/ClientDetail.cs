﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class ClientDetail : BaseEntity
    {
        public int ContractID { get; set; }

        [ForeignKey("ContractID")]
        public virtual Contracts Contract { get; set; }
        [ForeignKey("ID")]
        public virtual Entities Entity { get; set; }
    }
}
