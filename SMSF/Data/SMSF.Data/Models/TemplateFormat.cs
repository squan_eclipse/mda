﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DBASystem.SMSF.Data.Models
{
    public class TemplateFormats : BaseEntity
    {
        public TemplateFormats()
        {
            this.Templates = new HashSet<Templates>();
        }
        [Required]
        [MaxLength(50)]
        public string Name { set; get; }
        public Status StatusID { set; get; }
        

        public ICollection<Templates> Templates { get; set; }



    }
}
