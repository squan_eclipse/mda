﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class AccountManagers : AuditEntity
    {
        public AccountManagers()
        {
            Jobs = new HashSet<AccountManagerJobs>();
            Roles = new HashSet<AccountManagerRoles>();
            Telephones = new HashSet<Telephones>();
            Trustees = new HashSet<FundDetail>();
            Owners = new HashSet<FundDetail>();
            MainContacts = new HashSet<FundDetail>();
            AccountManagerContacts = new HashSet<AccountManagerContacts>();
            JobWorkLogs = new HashSet<JobWorkLogs>();
        }

        public Guid? Identifier { get; set; }
        public int EntityID { get; set; }
        [Required]
        [StringLength(4)]
        public string Title { get; set; }
        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string MidName { get; set; }
        [Required]
        [StringLength(100)]
        public string LastName { get; set; }
        [Required]
        [StringLength(100)]
        public string Email { get; set; }
        public Status StatusID { get; set; }

        [ForeignKey("EntityID")]
        public virtual Entities Entity { get; set; }
        public virtual ICollection<AccountManagerJobs> Jobs { get; set; }
        public virtual ICollection<AccountManagerRoles> Roles { get; set; }
        public virtual ICollection<Telephones> Telephones { get; set; }
        public virtual Users User { get; set; }
        public virtual ICollection<FundDetail> Trustees { get; set; }
        public virtual ICollection<FundDetail> Owners { get; set; }
        public virtual ICollection<FundDetail> MainContacts { get; set; }
        public virtual ICollection<AccountManagerContacts> AccountManagerContacts { get; set; }
        public virtual ICollection<JobWorkLogs> JobWorkLogs { get; set; } 
    }
}