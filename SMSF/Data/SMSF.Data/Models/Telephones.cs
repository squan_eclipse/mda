﻿using DBASystem.SMSF.Contracts.Common;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Telephones : BaseEntity
    {
        public int AccountManagerID { get; set; }
        public int TypeID { get; set; }
        [StringLength(50)]
        public string CountryCode { get; set; }
        [StringLength(50)]
        public string AreaCode { get; set; }
        [Required]
        [StringLength(50)]
        public string PhoneNumber { get; set; }
        public Status StatusID { get; set; }


        [ForeignKey("AccountManagerID")]
        public virtual AccountManagers AccountManager { get; set; }
        [ForeignKey("TypeID")]
        public virtual TelephoneTypes TelephoneType { get; set; }
    }
}
