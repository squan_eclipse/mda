﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class TemplateProvider : BaseEntity
    {
        public TemplateProvider()
        {
            //this.TemplatesCategories = new HashSet<TemplateCategories>();
        }
        [Required]
        [MaxLength(50)]
        public string Name { set; get; }
        public Status StatusID { set; get; }
    }
}
