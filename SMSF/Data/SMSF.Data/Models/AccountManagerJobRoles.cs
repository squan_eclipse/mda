﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class AccountManagerJobRoles : BaseEntity
    {
        [Required]
        [StringLength(35)]
        public string Name { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool IsActive { get; set; }

        [Required]
        [DefaultValue(true)]
        public Guid Identifier { get; set; }
    }
}