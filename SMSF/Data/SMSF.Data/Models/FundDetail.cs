﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class FundDetail : AuditEntity
    {
        public int TrusteeID { get; set; }
        public int OwnerID { get; set; }
        public int MainContactID { get; set; }
        public Nullable<bool> IsPreviousClient { get; set; }
        public Nullable<bool> IsDirectDebit { get; set; }

        [ForeignKey("MainContactID")]
        public virtual AccountManagers MainContact { get; set; }
        [ForeignKey("OwnerID")]
        public virtual AccountManagers Owner { get; set; }
        [ForeignKey("TrusteeID")]
        public virtual AccountManagers Trustee { get; set; }
        [ForeignKey("ID")]
        public virtual Entities Entity { get; set; }
    }
}
