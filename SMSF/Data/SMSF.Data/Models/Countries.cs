﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace DBASystem.SMSF.Data.Models
{
    public class Countries : BaseEntity
    {
        public Countries()
        {
            this.Addresses = new HashSet<Addresses>();
            this.States = new HashSet<States>();
            

        }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        public bool Default { get; set; }
        [StringLength(10)]
        public string Code { get; set; }


        public virtual ICollection<Addresses> Addresses { get; set; }
        public virtual ICollection<States> States { get; set; }

     
    }
}
