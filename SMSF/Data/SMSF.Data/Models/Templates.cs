﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBASystem.SMSF.Data.Models
{
    public class Templates : AuditEntity
    {
        public Templates()
        {
            this.JobsImportedData = new HashSet<JobImportedData>();
        }
        [MaxLength(100)]
        public string Name { get; set; }
        public int CategoryID { get; set; }
        public string TemplateText { get; set; }
       
        [ForeignKey("CategoryID")]
        public virtual TemplateCategories Category { get; set; }
        [ForeignKey("FormatID")]
        public virtual ICollection<JobImportedData> JobsImportedData { get; set; }

    }
}
