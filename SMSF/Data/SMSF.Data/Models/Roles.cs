﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Roles : BaseEntity
    {
        public Roles()
        {
            this.RoleOptions = new HashSet<RoleOptions>();
            this.GroupRoles = new HashSet<GroupRoles>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public Status StatusID { get; set; }
        public bool IsSystem { get; set; }


        public virtual ICollection<RoleOptions> RoleOptions { get; set; }
        public virtual ICollection<GroupRoles> GroupRoles { get; set; }
    }
}
