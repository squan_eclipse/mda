﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Notes : AuditEntity
    {
        [StringLength(500)]
        [Required]
        public string Note { get; set; }
        
        public int EntityID { get; set; }


        [ForeignKey("EntityID")]
        public virtual Entities Entity { get; set; }
    }

}
