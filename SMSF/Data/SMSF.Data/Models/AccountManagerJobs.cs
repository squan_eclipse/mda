﻿using DBASystem.SMSF.Contracts.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class AccountManagerJobs : AuditEntity
    {
        public int AccountManagerID { get; set; }

        public int JobID { get; set; }

        public Status StatusID { get; set; }

        public int JobRoleID { get; set; }

        [ForeignKey("AccountManagerID")]
        public virtual AccountManagers AccountManager { get; set; }

        [ForeignKey("JobID")]
        public virtual Jobs Job { get; set; }

        [ForeignKey("JobRoleID")]
        public virtual AccountManagerJobRoles JobRole { get; set; }
    }
}