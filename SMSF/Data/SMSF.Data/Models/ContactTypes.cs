﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace DBASystem.SMSF.Data.Models
{
    public class ContactTypes : BaseEntity
    {

        public ContactTypes()
        {
            this.Contacts = new HashSet<Contacts>();
        }
        [StringLength(50)]
        [Required]
        public string Description { get; set; }
        public Status Status { get; set; }


        public virtual ICollection<Contacts> Contacts { get; set; }
             

    
    }
}
