﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class JobWorkLogs : AuditEntity
    {
        public int JobID { get; set; }
        public int AccountManagerID { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Description { get; set; }

        [ForeignKey("JobID")]
        public virtual Jobs Job { get; set; }
        [ForeignKey("AccountManagerID")]
        public virtual AccountManagers AccountManager { get; set; }
    }
}
