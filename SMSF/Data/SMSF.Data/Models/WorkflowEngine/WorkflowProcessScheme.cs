﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowProcessScheme")]
    public class WorkflowProcessSchemes : WorkflowBase
    {
        [Required]
        [Column(TypeName = "ntext")]
        [MaxLength]
        public string Scheme { get; set; }
        [Required]
        [Column(TypeName = "ntext")]
        [MaxLength]
        public string DefiningParameters { get; set; }
        [Required]
        [StringLength(1024)]
        public string DefiningParametersHash { get; set; }
        [Required]
        public string ProcessName { get; set; }
        public bool IsObsolete { get; set; }
    }
}
