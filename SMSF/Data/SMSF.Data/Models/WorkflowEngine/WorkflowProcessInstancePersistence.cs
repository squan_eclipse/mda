﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowProcessInstancePersistence")]
    public class WorkflowProcessInstancePersistence : WorkflowBase
    {
        public Guid ProcessId { get; set; }
        [Required]
        public string ParameterName { get; set; }
        [Required]
        [Column(TypeName = "ntext")]
        [MaxLength]
        public string Value { get; set; }
    }
}
