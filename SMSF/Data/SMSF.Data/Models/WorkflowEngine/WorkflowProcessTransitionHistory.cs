﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowProcessTransitionHistory")]
    public class WorkflowProcessTransitionHistories : WorkflowBase
    {
        public Guid ProcessId { get; set; }
        public Guid ExecutorIdentityId { get; set; }
        public Guid ActorIdentityId { get; set; }
        [Required]
        public string FromActivityName { get; set; }
        [Required]
        public string ToActivityName { get; set; }
        public string ToStateName { get; set; }
        public DateTime TransitionTime { get; set; }
        [Required]
        public string TransitionClassifier { get; set; }
        public bool IsFinalised { get; set; }
        public string FromStateName { get; set; }
    }
}
