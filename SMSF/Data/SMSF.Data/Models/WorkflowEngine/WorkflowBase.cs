﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class WorkflowBase
    {
        [Key]
        public Guid Id { get; set; }
    }

    public class AuditWorkflow : WorkflowBase
    {
        public DateTime CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
