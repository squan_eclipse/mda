﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowProcessInstanceStatus")]
    public class WorkflowProcessInstanceStatus : WorkflowBase
    {
        public byte Status { get; set; }
        public Guid Lock { get; set; }
    }
}
