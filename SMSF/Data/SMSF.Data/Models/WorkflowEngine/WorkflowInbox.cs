﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models.WorkflowEngine
{
    [Table("WorkflowInbox")]
    public class WorkflowInbox : WorkflowBase
    {
        [Required]
        public Guid ProcessId { get; set; }

        public Guid IdentityId { get; set; }
    }
}