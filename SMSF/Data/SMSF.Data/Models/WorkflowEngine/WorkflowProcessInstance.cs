﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowProcessInstance")]
    public class WorkflowProcessInstance : WorkflowBase
    {
        [Required]
        public string StateName { get; set; }
        [Required]
        public string ActivityName { get; set; }
        public Guid? SchemeId { get; set; }
        public string PreviousState { get; set; }
        public string PreviousStateForDirect { get; set; }
        public string PreviousStateForReverse { get; set; }
        public string PreviousActivity { get; set; }
        public string PreviousActivityForDirect { get; set; }
        public string PreviousActivityForReverse { get; set; }
        public bool IsDeterminingParametersChanged { get; set; }
    }
}
