﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowRuntime")]
    public class WorkflowRuntimes
    {
        [Key]
        public Guid RuntimeId { get; set; }
        [Required]
        public string Timer { get; set; }
    }
}
