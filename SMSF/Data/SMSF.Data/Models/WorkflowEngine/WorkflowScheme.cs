﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowScheme")]
    public class WorkflowSchemes : WorkflowBase
    {
    
        [Required]
        [StringLength(256)]
        public string Code { get; set; }
        [Required]
        public string Scheme { get; set; }
    }
}
