﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Contacts : AuditEntity
    {
        public Contacts()
        {
            this.EntityContacts = new HashSet<EntityContacts>();
            this.AccountManagerContacts = new HashSet<AccountManagerContacts>();
        }
        public int TypeID { get; set; }
        [StringLength(50)]
        public string CountryCode { get; set; }
        [StringLength(50)]
        public string AreaCode { get; set; }
        [Required]
        [StringLength(250)]
        public string Contact { get; set; }
        public Status StatusID { get; set; }

        [ForeignKey("TypeID")]
        public virtual ContactTypes ContactTypes { get; set; }
        public virtual ICollection<EntityContacts> EntityContacts { get; set; }
        public virtual ICollection<AccountManagerContacts> AccountManagerContacts { get; set; }

    }

}
