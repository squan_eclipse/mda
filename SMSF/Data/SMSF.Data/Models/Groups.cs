﻿using DBASystem.SMSF.Contracts.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Groups : BaseEntity
    {
        public Groups()
        {
            this.GroupRoles = new HashSet<GroupRoles>();
            this.Users = new HashSet<Users>();
        }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public Status StatusID { get; set; }

        public bool IsSystem { get; set; }

        public Guid Identifier { get; set; }

        public virtual ICollection<GroupRoles> GroupRoles { get; set; }

        public virtual ICollection<Users> Users { get; set; }
    }
}