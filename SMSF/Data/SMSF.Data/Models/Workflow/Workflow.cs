﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Workflow : BaseEntity
    {
        public Workflow()
        {
            this.Activities = new HashSet<Activities>();
            this.Transitions = new HashSet<Transitions>();
            this.WorkFlowMapping = new HashSet<WorkflowMap>();
        }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public virtual ICollection<Activities> Activities { get; set; }

        public virtual ICollection<Transitions> Transitions { get; set; }

        public virtual ICollection<WorkflowMap> WorkFlowMapping { get; set; }
    }
}