﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class ActivityState : BaseEntity
    {
        public ActivityState()
        {
            this.Activities = new HashSet<Activities>();
        }

        [Required]
        [StringLength(1000)]
        public string Name { get; set; }

        public ICollection<Activities> Activities { get; set; }
    }
}