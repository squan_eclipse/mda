﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Transitions : AuditEntity
    {
        public Transitions()
        {
            this.TransitionActors = new HashSet<TransitionActors>();
        }

        public int WorkflowID { get; set; }
        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        public int FromActivityID { get; set; }
        public int ToActivityID { get; set; }
        public TransitionClassifier Classifier { get; set; }
        public TransitionCommandType CommandType { get; set; }
        public int? CommandID { get; set; }
        public TransitionActionType ActionType { get; set; }
        public int? ActionID { get; set; }
        public int? PreExecutionResult { get; set; }
        public int? EntityId { get; set; }
        public Status StatusID { get; set; }

        [ForeignKey("CommandID")]
        public virtual Commands Command { get; set; }
        public virtual ICollection<TransitionActors> TransitionActors { get; set; }
        [ForeignKey("FromActivityID")]
        public virtual Activities FromActivity { get; set; }
        [ForeignKey("ToActivityID")]
        public virtual Activities ToActivity { get; set; }
        [ForeignKey("ActionID")]
        public virtual Actions Action { get; set; }
        [ForeignKey("WorkflowID")]
        public virtual Workflow Workflow { get; set; }
        [ForeignKey("EntityId")]
        public virtual Entities Entity { get; set; }
    }
}
