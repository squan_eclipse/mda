﻿using DBASystem.SMSF.Contracts.Common;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class TransitionActors : BaseEntity
    {
        public TransitionRestrictionType RestrictionType { get; set; }
        public int TransitionID { get; set; }
        public int ActorID { get; set; }


        [ForeignKey("TransitionID")]
        public virtual Transitions Transition { get; set; }
        [ForeignKey("ActorID")]
        public virtual Actors Actor { get; set; }
    }
}
