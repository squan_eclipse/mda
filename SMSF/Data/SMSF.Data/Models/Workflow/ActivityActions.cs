﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class ActivityActions : BaseEntity
    {
        public bool IsPreExecution { get; set; }
        public int Order { get; set; }
        public int ActivityID { get; set; }
        public int ActionID { get; set; }

        [ForeignKey("ActivityID")]
        public virtual Activities Activity { get; set; }
        [ForeignKey("ActionID")]
        public virtual Actions Action { get; set; }
    }
}
