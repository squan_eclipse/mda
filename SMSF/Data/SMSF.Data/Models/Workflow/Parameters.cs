﻿using DBASystem.SMSF.Contracts.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Parameters : BaseEntity
    {
        public Parameters()
        {
            this.CommandParameters = new HashSet<CommandParameters>();
            this.ActionParameters = new HashSet<ActionParameters>();
        }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        [Required]
        [StringLength(1024)]
        public string Type { get; set; }
        public ParameterPurpose Purpose { get; set; }
        [StringLength(1024)]
        public string SerializedDefaultValue { get; set; }

        public virtual ICollection<CommandParameters> CommandParameters { get; set; }
        public virtual ICollection<ActionParameters> ActionParameters { get; set; }
    }
}
