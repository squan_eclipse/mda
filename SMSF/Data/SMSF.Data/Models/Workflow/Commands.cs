﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Commands : BaseEntity
    {
        public Commands()
        {
            this.CommandParameters = new HashSet<CommandParameters>();
        }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public int? EntityId { get; set; }

        public ICollection<CommandParameters> CommandParameters { get; set; }
        public virtual Entities Entity { get; set; }
    }
}
