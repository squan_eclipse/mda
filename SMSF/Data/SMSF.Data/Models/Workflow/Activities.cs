﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class Activities : BaseEntity
    {
        public Activities()
        {
            this.ActivityActions = new HashSet<ActivityActions>();
            this.FromTransitions = new HashSet<Transitions>();
            this.ToTransitions = new HashSet<Transitions>();
        }

        public int WorkflowID { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public int StateID { get; set; }

        public bool IsInitial { get; set; }

        public bool IsFinal { get; set; }

        public bool IsForSetState { get; set; }

        public bool IsAutoScheme { get; set; }

        public string ActivityView { get; set; }

        [ForeignKey("WorkflowID")]
        public virtual Workflow Workflow { get; set; }

        [ForeignKey("StateID")]
        public virtual ActivityState State { get; set; }

        public virtual ICollection<ActivityActions> ActivityActions { get; set; }

        public virtual ICollection<Transitions> FromTransitions { get; set; }

        public virtual ICollection<Transitions> ToTransitions { get; set; }
    }
}