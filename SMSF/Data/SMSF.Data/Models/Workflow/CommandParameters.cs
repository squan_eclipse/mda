﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class CommandParameters : BaseEntity
    {
        public int CommandID { get; set; }
        public int ParameterID { get; set; }

        [ForeignKey("CommandID")]
        public virtual Commands Command { get; set; }
        [ForeignKey("ParameterID")]
        public virtual Parameters Parameter { get; set; }
    }
}
