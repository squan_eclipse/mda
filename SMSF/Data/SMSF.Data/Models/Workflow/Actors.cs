﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Actors : BaseEntity
    {
        public Actors()
        {
            this.TransitionActors = new HashSet<TransitionActors>();
        }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string MethodName { get; set; }

        public virtual ICollection<TransitionActors> TransitionActors { get; set; }
    }
}
