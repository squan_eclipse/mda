﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowTransitionUsers")]
    public class WorkflowTransitionUsers : WorkflowBase
    {
        public Guid TransactionId { get; set; }
        public Guid UserID { get; set; }

        [ForeignKey("TransactionId")]
        public virtual WorkflowTransactionHistory TransactionHistory { get; set; }
    }
}
