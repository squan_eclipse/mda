﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    [Table("WorkflowTransactionHistory")]
    public class WorkflowTransactionHistory : AuditWorkflow
    {
        public WorkflowTransactionHistory()
        {
            this.TransactionUsers = new HashSet<WorkflowTransitionUsers>();
        }

        public System.Guid ProcessIdentity {get;set;}

        public  System.Guid? UserID {get;set;}

        public string AllowedToEmployeeNames {get;set;}

        public DateTime? TransitionTime {get;set;}

        public int Order {get;set;}

        public DateTime? TransitionTimeForSort {get;set;}

        public string InitialState {get;set;}

        public string DestinationState {get;set;}

        public string Command {get;set;}

        public int WorkflowId { get; set; }

        public virtual ICollection<WorkflowTransitionUsers> TransactionUsers { get; set; }

        [ForeignKey("WorkflowId")]
        public virtual Workflow WorkFlow { get; set; }

    }
}