﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DBASystem.SMSF.Data.Models
{
    public class Actions : BaseEntity
    {
        public Actions()
        {
            this.ActivityActions = new HashSet<ActivityActions>();
        }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }
        [Required]
        [StringLength(1024)]
        public string Type { get; set; }
        [Required]
        [StringLength(250)]
        public string Method { get; set; }
        public bool IsConditional { get; set; }

        public virtual ICollection<ActivityActions> ActivityActions { get; set; }
        public virtual ICollection<ActionParameters> ActionParameters { get; set; }
    }
}