﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class ActionParameters : BaseEntity
    {
        public bool IsOutput { get; set; }
        public int Order { get; set; }
        public int ActionID { get; set; }
        public int ParameterID { get; set; }

        [ForeignKey("ActionID")]
        public virtual Actions Action { get; set; }
        [ForeignKey("ParameterID")]
        public virtual Parameters Parameter { get; set; }
    }
}
