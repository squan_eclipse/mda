﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBASystem.SMSF.Data.Models
{
    public class WorkflowMap : AuditEntity
    {
        public int? EntityId { get; set; }
        public int WorkFlowTypeId { get; set; }
        public Guid WorkFlowId { get; set; }
        [MaxLength(25)]
        public string Name { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("EntityId")]
        public virtual Entities Entity { get; set; }
        [ForeignKey("WorkFlowTypeId")]
        public virtual Workflow WorkFlowType { get; set; }
        [ForeignKey("WorkFlowId")]
        public virtual WorkflowProcessSchemes Workflow { get; set; }
    }
}